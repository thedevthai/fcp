﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master"
  CodeBehind="COVID2.aspx.vb" Inherits=".COVID2" %> 
    <asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">   
    </asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <section class="content-header">
        <h1>แบบฟอร์มติดตามอาการ Long Covid
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
          <li class="active">Login covid</li>
        </ol>
      </section>

      <section class="content">         
        <div class="pull-right">
          <table border="0" cellspacing="2" cellpadding="0" align="right">
            <tr>
              <td class="NameEN">Item ID : </td>
              <td class="NameEN">
                <asp:Label ID="lblCOVIDUID" runat="server"></asp:Label>
              </td>
            </tr>
          </table>
        </div>
        <br />
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">ข้อมูลร้านยาที่ให้บริการ</h3>
            <div class="box-tools pull-right">

              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <table width="100%" border="0" align="left" cellpadding="0" cellspacing="2" class="OptionPanels">
              <tr>
                <td width="50">รหัสร้าน</td>
                <td width="120" class="LocationName">
                  <asp:Label ID="lblLocationID" runat="server"></asp:Label>
                </td>
                <td width="50">ชื่อร้าน</td>
                <td width="300" class="LocationName">
                  <asp:Label ID="lblLocationName" runat="server"></asp:Label>
                </td>
                <td width="100">รหัสหน่วยบริการ</td>
                <td class="LocationName">
                  <asp:Label ID="lblLocationCode" runat="server"></asp:Label>
                </td>
                <td width="50">จังหวัด</td>
                <td class="LocationName">
                  <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>
                </td>
              </tr>
            </table>
          </div>
        </div>
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">ข้อมูลเบื้องต้น</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <label>วันที่ตรวจพบเชื้อ</label>
                  <asp:TextBox ID="txtStartDate" runat="server" placeholder="" CssClass="form-control text-center"
                    autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th"
                    data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>วิธีตรวจพบ</label>
                  <asp:RadioButtonList ID="optTest" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem>ATK</asp:ListItem>
                    <asp:ListItem>RT-PCR</asp:ListItem>
                  </asp:RadioButtonList>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>วันที่หาย/ออกจาก รพ./ครบระยะแยกตัว</label>
                  <asp:TextBox ID="txtEndDate" runat="server" placeholder="" CssClass="form-control text-center"
                    autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th"
                    data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                </div>
              </div>
        
              <div class="col-md-5">
                <div class="form-group">
                  <label>สถานะตอนเป็น covid (ระบุสี)</label>
                  <asp:RadioButtonList ID="optColor" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="G"><span class="badge bg-green">เขียว</span></asp:ListItem>
                    <asp:ListItem Value="Y"><span class="badge bg-yellow">เหลือง</span></asp:ListItem>
                    <asp:ListItem Value="O"><span class="badge bg-orange">ส้ม</span></asp:ListItem>
                    <asp:ListItem Value="R"><span class="badge bg-red">แดง</span></asp:ListItem>
                  </asp:RadioButtonList>
                </div>
              </div>
            </div> 
              <div class="row">                
                 <div class="col-md-12">
          <div class="form-group">
            <label>โรคประจำตัว</label>   
                <table cellpadding="0">
                <tr>
                    <td align="left"><asp:CheckBox ID="chkDisease1" runat="server" Text="เบาหวาน"/>                    </td> 
                    <td align="left"><asp:CheckBox ID="chkDisease2" runat="server" Text="ความดันโลหิตสูง" />                    </td>
                    <td align="left"><asp:CheckBox ID="chkDisease3" runat="server" Text="ไขมันในเลือดสูง" />                    </td>
                </tr>
                <tr>
                    <td align="left"><asp:CheckBox ID="chkDisease4" runat="server" Text="กล้ามเนื้อหัวใจขาดเลือด" />                    </td> 
                    <td align="left"><asp:CheckBox ID="chkDisease5" runat="server" Text="หลอดเลือดสมองอุดตัน" /></td>
                    <td align="left"><asp:CheckBox ID="chkDisease6" runat="server" Text="ซึมเศร้า" /></td>
                </tr>
                <tr>
                    <td align="left"><asp:CheckBox ID="chkDisease7" runat="server" Text="ปอดอุดกั้นเรื้องรัง" />                    </td> 
                    <td align="left"><asp:CheckBox ID="chkDisease8" runat="server" Text="หืด" /></td>
                    <td align="left"><asp:CheckBox ID="chkDisease9" runat="server" Text="ภูมิแพ้" /></td>
                </tr>
                <tr>
                  <td align="left"><asp:CheckBox ID="chkDisease10" runat="server" Text="ตั้งครรภ์/ให้นมบุตร" /></td>
                  <td align="left"><asp:CheckBox ID="chkDisease11" runat="server" Text="ไต" /></td>
                  <td align="left"><asp:CheckBox ID="chkDisease12" runat="server" Text="ตับ" /></td>                 
                </tr>
                <tr>
                  <td align="left"><asp:CheckBox ID="chkDisease0" runat="server" Text="ไม่มีโรคประจำตัว" /></td>                  
                  <td align="left"><asp:CheckBox ID="chkDisease99" runat="server" Text="ไม่ทราบ" /></td>
                  <td colspan="2" align="left" valign="middle"><asp:CheckBox ID="chkDiseaseOther" runat="server" Text="อื่นๆ"  /> 
                    &nbsp;<asp:TextBox ID="txtDiseaseOther" runat="server"  Width="160px"></asp:TextBox></td>
                </tr>
            </table>
             
          </div>
        </div>
  <div class="col-md-3">
          <div class="form-group">
                    <label>ยาที่ใช้ (เลือกได้มากกว่า 1)</label>
               <asp:CheckBoxList ID="chkMed" runat="server" RepeatDirection="Vertical" RepeatColumns="3">
                    <asp:ListItem Value="M1">ฟ้าทะลายโจร</asp:ListItem>
                    <asp:ListItem Value="M2">Favipiravir</asp:ListItem> 
                    <asp:ListItem Value="M3">ยาสมุนไพร</asp:ListItem>      
                   <asp:ListItem Value="M4">Nat Long </asp:ListItem>    
                   <asp:ListItem Value="M5">Ivermectin</asp:ListItem> 
                   <asp:ListItem Value="M6"> ยาลดไข้ แก้ไอ </asp:ListItem> 
                   <asp:ListItem Value="M99">อื่นๆ ระบุ</asp:ListItem>
              </asp:CheckBoxList>

          </div>
        </div>      
                    <div class="col-md-3">
          <div class="form-group">
            <label>ระบุยา</label>
              <asp:TextBox ID="txtMedicineOther" runat="server" placeholder="ระบุยาอื่นๆ" CssClass="form-control"></asp:TextBox>
          </div>
        </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>การรักษา</label>
                  <asp:RadioButtonList ID="optMedical" runat="server" RepeatDirection="Horizontal" RepeatColumns="2" >
                    <asp:ListItem Value="M1">โรงพยาบาล</asp:ListItem>
                    <asp:ListItem Value="M2">โรงพยาบาลสนาม</asp:ListItem>
                    <asp:ListItem Value="M3">Hospitel</asp:ListItem>
                    <asp:ListItem Value="M4">การกักตัวที่บ้าน (Home Isolation)</asp:ListItem>
                    <asp:ListItem Value="M5">การกักตัวที่ชุมชน (Community Isolation)</asp:ListItem>
                    <asp:ListItem Value="M99">อื่นๆ ระบุ</asp:ListItem>
                  </asp:RadioButtonList>

                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>ระบุ</label>
                  <asp:TextBox ID="txtMedicalOther" runat="server" placeholder="ระบุอื่นๆ" CssClass="form-control">
                  </asp:TextBox>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>ท่านสูบบุหรี่หรือไม่</label>
                  <asp:RadioButtonList ID="optSmoke" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1">เคยสูบ แต่หลังจากติดโควิด-19 เลิกสูบแล้ว</asp:ListItem>
                    <asp:ListItem Value="2">เคยสูบ และยังสูบอยู่</asp:ListItem>
                    <asp:ListItem Value="3">ไม่เคยสูบเลย</asp:ListItem>
                  </asp:RadioButtonList>

                </div>
              </div>

            </div>
                <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>ท่านอยากจะลดหรือเลิกสูบบุหรี่หรือไม่</label>
                  <asp:RadioButtonList ID="optSmokingQuit" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1">อยากลด</asp:ListItem>
                    <asp:ListItem Value="2">อยากเลิก</asp:ListItem>
                    <asp:ListItem Value="3">เฉยๆ</asp:ListItem>
                  </asp:RadioButtonList>

                </div>
              </div>

            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>
                    <span class="text-blue text-bold">ปัจจุบัน </span>ท่านมีอาการต่อเนื่อง
                    หรือมีอาการที่เกิดขึ้นหลังการติดเชื้อโควิด-19
                  </label>
                  <asp:RadioButtonList ID="optAfterEffect" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="Y">มี</asp:ListItem>
                    <asp:ListItem Value="N">ไม่มี</asp:ListItem>
                      <asp:ListItem Value="2"> เคยมี  แต่ตอนนี้ไม่มีแล้ว</asp:ListItem>
                  </asp:RadioButtonList>
                </div>
              </div>
                   <div class="col-md-6">
                <div class="form-group">
                  <label>ระบุอาการ</label>
                    <asp:TextBox ID="txtAfterEffextRemark" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                       <h5 class="text-blue text-bold">อาการที่เกิดขึ้นหลังการติดเชื้อโควิด-19 (อาการที่ยังเป็นอยู่ขณะนี้)</h5>
                  <label>น้อย = ไม่กระทบ หรือ ไม่มีผลรบกวนชีวิตปกติ </label><br />
                  <label> ปานกลาง = รู้สึกบางครั้ง รบกวนชีวิตปกติบ้าง แต่ยังทำกิจวัตรต่างๆได้</label><br />
                  <label> มาก = ทำกิจวัตรต่างๆไม่ได้เหมือนปกติ ส่งผลรบกวนชีวิตปกติมาก อยากไปพบแพทย์</label><br /> 
                  <table class="table table-hover">
                    <tr>
                      <td colspan="3" class="text-blue text-bold">
                        อาการทางกาย
                      </td>
                    </tr>
                    <tr>
                      <td width="20"> 1.</td>
                      <td> อ่อนเพลีย ล้า </td>
                      <td>
                        <asp:RadioButtonList ID="optQ1" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>
                      </td>
                    </tr>
                    <tr>
                      <td> 2.</td>
                      <td> ไข้ ไอ </td>
                      <td>
                        <asp:RadioButtonList ID="optQ2" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>

                      </td>
                    </tr>
                    <tr>
                      <td> 3.</td>
                      <td> หายใจลำบาก หอบเหนื่อย </td>
                      <td>
                        <asp:RadioButtonList ID="optQ3" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>

                      </td>
                    </tr>
                    <tr>
                      <td> 4.</td>
                      <td> ใจสั่น เจ็บหน้าอก </td>
                      <td>
                        <asp:RadioButtonList ID="optQ4" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>

                      </td>
                    </tr>
                    <tr>
                      <td> 5.</td>
                      <td> แขน ขา ชา </td>
                      <td>
                        <asp:RadioButtonList ID="optQ5" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>
                      </td>
                    </tr>
                    <tr>
                      <td> 6.</td>
                      <td> จมูกไม่ได้กลิ่น </td>
                      <td>
                        <asp:RadioButtonList ID="optQ6" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>

                      </td>
                    </tr>
                    <tr>
                      <td> 7.</td>
                      <td> ลิ้นไม่รับรส </td>
                      <td>
                        <asp:RadioButtonList ID="optQ7" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>

                      </td>
                    </tr>
                    <tr>
                      <td> 8.</td>
                      <td> กลืนลำบาก </td>
                      <td>
                        <asp:RadioButtonList ID="optQ8" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>

                      </td>
                    </tr>
                    <tr>
                      <td> 9.</td>
                      <td> การออกเสียง การพูด ไม่ชัด ลำบาก </td>
                      <td>
                        <asp:RadioButtonList ID="optQ9" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>

                      </td>
                    </tr>
                    <tr>
                      <td> 10.</td>
                      <td> การใช้ความคิด ความจำ ช้าลงกว่าปกติ </td>
                      <td>
                        <asp:RadioButtonList ID="optQ10" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>

                      </td>
                    </tr>
                    <tr>
                      <td> 11.</td>
                      <td> การเคลื่อนไหวร่างกายผิดปกติ </td>
                      <td>
                        <asp:RadioButtonList ID="optQ11" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>

                      </td>
                    </tr>
                    <tr>
                      <td> 12.</td>
                      <td> ปวดข้อ ปวดกล้ามเนื้อ ปวดตัว </td>
                      <td>
                        <asp:RadioButtonList ID="optQ12" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>

                      </td>
                    </tr>
                    <tr>
                      <td> 13.</td>
                      <td> ปัญหาการกลั้นปัสสาวะ อุจาระ </td>
                      <td>
                        <asp:RadioButtonList ID="optQ13" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>

                      </td>
                    </tr>
                    <tr>
                      <td> 14.</td>
                      <td> อาการผมร่วง </td>
                      <td>
                        <asp:RadioButtonList ID="optQ14" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>

                      </td>
                    </tr>
                    <tr>
                      <td> 15.</td>
                      <td> อาการผื่นคันที่ผิวหนัง </td>
                      <td>
                        <asp:RadioButtonList ID="optQ15" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>

                      </td>
                    </tr>
                    <tr>
                      <td> 16.</td>
                      <td> อื่นๆ ระบุ <asp:TextBox ID="txtPhysicalRemark" runat="server" Width="250"></asp:TextBox> </td>
                      <td>
                        <asp:RadioButtonList ID="optQ16" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>

                      </td>
                    </tr>
                    <tr>
                      <td colspan="3" class="text-blue text-bold"> อาการทางจิตใจ </td>
                    </tr>
                    <tr>
                      <td> 1.</td>
                      <td> มีอาการวิตกกังวล </td>
                      <td>
                        <asp:RadioButtonList ID="optM1" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>

                      </td>
                    </tr>
                    <tr>
                      <td> 2.</td>
                      <td> กินข้าวไม่ได้ </td>
                      <td>
                        <asp:RadioButtonList ID="optM2" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>

                      </td>
                    </tr>
                    <tr>
                      <td> 3.</td>
                      <td> นอนไม่หลับ </td>
                      <td>
                        <asp:RadioButtonList ID="optM3" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="3" class="text-blue text-bold"> อาการทางสังคม </td>
                    </tr>
                    <tr>
                      <td> 1 </td>
                      <td> รู้สึกผิด รู้สึกท้อแท้ ที่ติดเชื้อ </td>
                      <td>
                        <asp:RadioButtonList ID="optS1" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>

                      </td>
                    </tr>
                    <tr>
                      <td> 2 </td>
                      <td>

                        ถูกคนใกล้ชิด ครอบครัว ที่ทำงาน หรือเพื่อน&nbsp;แสดงความรังเกียจ
                      </td>
                      <td>
                        <asp:RadioButtonList ID="optS2" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="3">มาก</asp:ListItem>
                          <asp:ListItem Value="2">ปานกลาง</asp:ListItem>
                          <asp:ListItem Value="1">น้อย</asp:ListItem><asp:ListItem Value="0">ไม่มีอาการ</asp:ListItem>
                        </asp:RadioButtonList>

                      </td>
                    </tr>
                  </table>
                </div>
              </div>
 <div class="col-md-12">
                  <div class="form-group">
                    <label>ปัญหาอื่นๆที่เภสัชกรพบ</label>
                      <asp:TextBox ID="txtProblemRemark" runat="server" CssClass="form-control" TextMode="MultiLine" Height="100"></asp:TextBox>
                  </div>
                </div>
            </div>
          </div>
        </div>

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">การแก้ไขปัญหา</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                  title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
                
              <div class="row">
                     <div class="col-md-12">
                  <div class="form-group">
                    <label>
                        <asp:CheckBox ID="chkRecommend" runat="server" CssClass="text-blue" Text="ให้คำแนะนำ (ระบุสิ่งที่แนะนำ)" /></label> 
                      <asp:TextBox ID="txtRecommend" runat="server" CssClass="form-control" TextMode="MultiLine" Height="100"></asp:TextBox>
 </div>
                </div>                 

                <div class="col-md-6">
                  <div class="form-group">
                    <label>
                        <asp:CheckBox ID="chkRefer" runat="server" CssClass="text-blue" Text="ส่งต่อ" /></label>
                    <asp:RadioButtonList ID="optRefer" runat="server" RepeatDirection="Horizontal">
                      <asp:ListItem Value="1">หน่วยบริการที่ขึ้นทะเบียน</asp:ListItem>
                      <asp:ListItem Value="2">นักสังคมสงเคราะห์</asp:ListItem>
                      <asp:ListItem Value="3">นักกายภาพ</asp:ListItem> 
                      <asp:ListItem Value="9">อื่นๆ</asp:ListItem>
                    </asp:RadioButtonList>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>ระบุ</label>
                    <asp:TextBox ID="txtReferOther" runat="server" placeholder="ระบุส่งต่ออื่นๆ" CssClass="form-control">
                    </asp:TextBox>
                  </div>
                </div>
         </div>
                <div class="row">        
                <div class="col-md-6">
                  <div class="form-group">
                         <h5 class="text-blue">การติดตามผล</h5>
               
                    <asp:RadioButtonList ID="optResult" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="8">ติดตามครั้งแรก</asp:ListItem>
                      <asp:ListItem Value="1">ดีขึ้น</asp:ListItem>
                      <asp:ListItem Value="2">เท่าเดิม</asp:ListItem>
                          <asp:ListItem Value="3">แย่ลง</asp:ListItem>
                      <asp:ListItem Value="0">ติดตามไม่ได้</asp:ListItem>
                                   <asp:ListItem Value="9">อื่นๆ</asp:ListItem>
                    </asp:RadioButtonList>
                  </div>
                </div>
                    <div class="col-md-6">
                  <div class="form-group">
                    <label>ระบุ</label>
                    <asp:TextBox ID="txtResultOther" runat="server" placeholder="ระบุการติดตามอื่นๆ" CssClass="form-control">
                    </asp:TextBox>
                  </div>
                </div>
              </div> 
              <div class="row">
                   <div class="col-md-12">
              <h5 class="text-blue">ระยะเวลาติดตามของเภสัชกร</h5>
                       </div>
                  </div>
                      <div class="row">
                <div class="col-md-3">
                  <div class="form-group">     
                        <table class="table table-hover">
                    <tr>
                      <td class="text-bold text-center" width="50">ครั้งที่</td>
                         <td class="text-bold text-center">ภายในระยะเวลา (วัน)</td>
                    </tr>
                    <tr>
                        <td class="text-center">1.</td>
                        <td ><asp:TextBox ID="txtFollowDay1" runat="server" placeholder="" CssClass="form-control text-center"></asp:TextBox>
                      </td>
                    </tr>
                            <tr>
                        <td class="text-center">2.</td>
                        <td ><asp:TextBox ID="txtFollowDay2" runat="server" placeholder="" CssClass="form-control text-center"></asp:TextBox>
                      </td>
                    </tr>
                            <tr>
                        <td class="text-center">3.</td>
                        <td ><asp:TextBox ID="txtFollowDay3" runat="server" placeholder="" CssClass="form-control text-center"></asp:TextBox>
                      </td>
                    </tr>
                            <tr>
                        <td class="text-center">4.</td>
                        <td ><asp:TextBox ID="txtFollowDay4" runat="server" placeholder="" CssClass="form-control text-center"></asp:TextBox>
                      </td>
                    </tr>
                            <tr>
                        <td class="text-center">5.</td>
                        <td ><asp:TextBox ID="txtFollowDay5" runat="server" placeholder="" CssClass="form-control text-center"></asp:TextBox>
                      </td>
                    </tr>
                    </table>
                  </div>
                </div>
           
                
                             </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>สถานะ</label>
                    <asp:CheckBox ID="chkStatus" runat="server" Text="จบการทำงาน" />
                  </div>
                </div>
              </div>         
            </div>            
          </div>
   
        <div align="center">
          <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="btn btn-primary" Width="100px" />
             &nbsp;<asp:Button ID="cmdDelete" runat="server" Text="ลบ" CssClass="btn btn-danger" Width="100px" />
        </div> <br />
      </section>
    </asp:Content>