﻿
Public Class PaymentConfig2
    Inherits System.Web.UI.Page

    Dim dt As New DataTable

    Dim acc As New UserController
    Dim ctlPay As New PaymentController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If IsNothing(Request.Cookies("FCPCPA")) Then
        '    Response.Redirect("Default.aspx?logout=YES")
        'End If
        If Not IsPostBack Then
            grdData.PageIndex = 0
            txtCode.Text = ""
            lblID.Text = ""
            'LoadProjectToDDL()
            LoadServiceTypeToDDL()
            'LoadProvince()
            LoadPaymentConfig()

        End If

    End Sub
   
    'Private Sub LoadProjectToDDL()
    '    Dim ctlPj As New ProjectController
    '    dt = ctlPj.Project_Get
    '    If dt.Rows.Count > 0 Then
    '        With ddlProject
    '            .DataSource = dt
    '            .DataTextField = "Description"
    '            .DataValueField = "ProjectID"
    '            .DataBind()
    '            .SelectedIndex = 0
    '        End With
    '    End If
    '    dt = Nothing
    'End Sub
    'Private Sub LoadProvince()
    '    dt = ctlPay.LoadProvince
    '    If dt.Rows.Count > 0 Then
    '        With ddlProvince
    '            .DataSource = dt
    '            .DataTextField = "ProvinceName"
    '            .DataValueField = "ProvinceID"
    '            .DataBind()
    '            .SelectedIndex = 0
    '        End With
    '    End If
    '    dt = Nothing
    'End Sub
    Private Sub LoadPaymentConfig()
        If Trim(txtSearch.Text) <> "" Then
            dt = ctlPay.Payment_GetMethodBySearch(Trim(txtSearch.Text))
        Else
            dt = ctlPay.Payment_GetMethod
        End If

        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                Try

                    Dim nrow As Integer = dt.Rows.Count
                    If .PageCount > 1 Then
                        If .PageIndex > 0 Then
                            If (.PageSize) < nrow - (.PageSize * .PageIndex) Then
                                For i = 0 To .PageSize - 1
                                    .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                Next
                            Else
                                For i = 0 To nrow - (.PageSize * .PageIndex) - 1
                                    .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                Next
                            End If
                        Else
                            For i = 0 To .PageSize - 1
                                .Rows(i).Cells(0).Text = i + 1
                            Next
                        End If
                    Else
                        For i = 0 To nrow - 1
                            .Rows(i).Cells(0).Text = i + 1
                        Next
                    End If
                Catch ex As Exception

                End Try

            End With
        End If
        dt = Nothing
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadPaymentConfig()
    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())

                    'lblCode.Text = grdData.Rows(
                Case "imgDel"
                    ctlPay.PaymentMethod_Delete(e.CommandArgument)
                    acc.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_DEL, "PaymentMedthod", "Delete PaymentMethod:" & txtCode.Text & ">>" & ddlServiceType.SelectedItem.Text, "")
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ลบข้อมูลเรียบร้อย');", True)
                    LoadPaymentConfig()
            End Select


        End If
    End Sub


    Private Sub EditData(ByVal pID As String)

        dt = ctlPay.Payment_GetMethodByID(pID)

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                Me.txtCode.Text = DBNull2Str(dt.Rows(0)("PaymentName"))
                txtCode.ReadOnly = True
                lblID.Text = DBNull2Str(dt.Rows(0)("PaymentID"))
                ddlServiceType.SelectedValue = DBNull2Str(dt.Rows(0)("ServiceTypeID"))
                txtRateAmount.Text = DBNull2Str(dt.Rows(0)("Amount"))
                txtEffectiveTo.Text = DisplayStr2ShortDateTH(DBNull2Str(dt.Rows(0)("EffectiveTo")))
                chkActive.Checked = ConvertStatusFlag2Boolean(DBNull2Str(dt.Rows(0)("StatusFlag")))


            End With
        End If
        dt = Nothing
    End Sub

    Private Sub ClearData()
        txtRateAmount.Text = "50"
        txtCode.Text = ""
        ddlServiceType.SelectedIndex = 0
        txtEffectiveTo.Text = ""
        lblID.Text = ""

    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(7).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub


    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        'If ddl.Text = "" Then

        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอกข้อมูลให้ครบถ้วน');", True)
        '    Exit Sub
        'End If

        If txtCode.Text = "" Then

            ctlPay.Payment_Add2(txtCode.Text, ddlServiceType.SelectedValue, StrNull2Zero(txtRateAmount.Text), StrNull2Long(ConvertStrDate2DBString(txtEffectiveTo.Text)), Boolean2StatusFlag(chkActive.Checked))

            acc.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "PaymentMethod", "Add new Payment Method:" & ddlServiceType.SelectedItem.Text, "")

        Else
            ctlPay.Payment_Update2(StrNull2Zero(lblID.Text), txtCode.Text, ddlServiceType.SelectedValue, StrNull2Zero(txtRateAmount.Text), StrNull2Long(ConvertStrDate2DBString(txtEffectiveTo.Text)), Boolean2StatusFlag(chkActive.Checked))

            acc.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "PaymentMethod", "Update Payment Method:" & ddlServiceType.SelectedItem.Text, "")

        End If

        LoadPaymentConfig()
        ClearData()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadPaymentConfig()
    End Sub
    Private Sub LoadServiceTypeToDDL()
        Dim ctlType As New ServiceTypeController
        dt = ctlType.ServiceType_GetAll
        If dt.Rows.Count > 0 Then
            'ddlActivity.Items.Clear()
            'ddlActivity.Items.Add("---ทั้งหมด---")
            'ddlActivity.Items(0).Value = "0"
            For i = 0 To dt.Rows.Count - 1
                With ddlServiceType
                    .Items.Add("" & dt.Rows(i)("ServiceTypeID") & " : " & dt.Rows(i)("ServiceName"))
                    .Items(i).Value = dt.Rows(i)("ServiceTypeID")
                End With
            Next
        End If
        dt = Nothing
    End Sub


End Class

