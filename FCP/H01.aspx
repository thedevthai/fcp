﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="H01.aspx.vb" Inherits=".H01" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>
<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/pagestyles.css">
<link rel="stylesheet" type="text/css" href="css/cpastyles.css">
<link rel="stylesheet" type="text/css" href="css/uidialog.css">
<link rel="stylesheet" type="text/css" href="css/dc_table.css">
    
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">        

     <section class="content-header">
      <h1>บันทึกการเยี่ยมบ้าน
        <small>(Home Visit)</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Home Visit</li>
      </ol>
    </section>

<section class="content">    
<asp:HiddenField ID="HiddenField1" runat="server" />

 <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">ข้อมูลร้านยาที่ให้บริการ</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
 <table width="100%" border="0" align="left" cellpadding="0" cellspacing="2" class="OptionPanels">
      <tr>
        <td width="50">รหัสร้าน</td>
        <td width="120" class="LocationName">
            <asp:Label ID="lblLocationID" runat="server"></asp:Label>          </td>
        <td width="50">ชื่อร้าน</td>
        <td width="300" class="LocationName">
            <asp:Label ID="lblLocationName" runat="server"></asp:Label>          </td>
        <td width="50">จังหวัด</td>
        <td class="LocationName">
            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>          </td>
      </tr>
    </table>

        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">
      <tr>
             <td align="left">ปี</td>
        <td align="left">
            <asp:TextBox ID="txtBYear" runat="server" Width="40px"></asp:TextBox>
             </td>
        <td width="100">วันที่ให้บริการ</td>
        <td>
            <asp:TextBox ID="txtServiceDate" runat="server" Width="80px"></asp:TextBox>
(วว/ดด/ปปปป พ.ศ.) 
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="txtServiceDate" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="\d{2}/\d{2}/\d{4}"></asp:RegularExpressionValidator></td>
        <td width="100">รวมระยะเวลา<img src="images/star.png" width="10" height="10" /></td>
        <td>
            <asp:TextBox ID="txtTime" runat="server" Width="40px"></asp:TextBox>
&nbsp;นาที</td>
        <td>เภสัชกรผู้ให้บริการ</td>
        <td>
            <asp:DropDownList ID="ddlPerson" runat="server" CssClass="OptionControl"></asp:DropDownList>          </td>
      </tr>
    </table>
  <!-- /.box-body -->
        <div class="box-footer">
             <table  border="0" cellspacing="2" cellpadding="0" align="right">
        <tr>
        
          <td class="NameEN">Item ID : </td>
          <td class="NameEN">
              <asp:Label ID="lblHUID" 
                  runat="server"></asp:Label>            </td>
        </tr>
      </table>
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->  
</div>
    <div align="center">
    <h2>ครั้งที่ <asp:Label ID="lblSEQ" runat="server" Text=""></asp:Label></h2>
</div>
<div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">พฤติกรรมสุขภาพ</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
           

 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  
   <tr>
    <td><table border="0" cellpadding="0" cellspacing="2"  align="Center">
     <tr>
        <td>การสูบบุหรี่</td>
        <td>
            <table border="0" cellspacing="2" cellpadding="0">
              <tr>
                <td align="left"><asp:RadioButtonList ID="optSmoke" runat="server" 
                RepeatDirection="Horizontal" AutoPostBack="True" CellPadding="10" CellSpacing="4">
                  <asp:ListItem Value="2">เลิกสูบแล้ว</asp:ListItem>
                  <asp:ListItem Value="1">สูบประจำ</asp:ListItem>
                  <asp:ListItem Selected="True" Value="0">ไม่สูบ</asp:ListItem>
                </asp:RadioButtonList></td>
                <td><asp:TextBox ID="txtCGNo" runat="server" Width="40px"></asp:TextBox>
&nbsp;<asp:Label ID="lblCGday" runat="server" Text="มวน/วัน"></asp:Label>
            &nbsp;<asp:TextBox ID="txtCGYear" runat="server" Width="40px"></asp:TextBox>
&nbsp;<asp:Label ID="lblCGYear" runat="server" Text="ปี"></asp:Label></td>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td>&nbsp; <asp:Label ID="lblCgType" runat="server" Text="ชนิดของบุหรี่ที่สูบ"></asp:Label></td>
                      <td> <asp:RadioButtonList ID="optCigarette" runat="server" 
                RepeatDirection="Horizontal">
                  <asp:ListItem Value="1">มวนเอง</asp:ListItem>
                  <asp:ListItem Selected="True" Value="2">บุหรี่ซอง</asp:ListItem>
                  <asp:ListItem Value="3">บุหรี่ไฟฟ้า</asp:ListItem>
                
                  </asp:RadioButtonList></td>
                    </tr>
                  </table>

                   
                   </td>
                </tr>
            </table>          </td>
      </tr>
      <tr>
        <td>การดื่มเครื่องดื่มที่มีแอลกอฮอล์</td>
        <td align="left"><table border="0" cellspacing="2" cellpadding="0">
              <tr>
                <td align="left">
                    <asp:RadioButtonList ID="optAlcohol" runat="server" 
                RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="3">ดื่มประจำ</asp:ListItem>
                     <asp:ListItem Value="2">ดื่มครั้งคราว</asp:ListItem>
                  <asp:ListItem Value="1">เคยดื่มแต่เลิกแล้ว</asp:ListItem>
                  <asp:ListItem Selected="True" Value="0">ไม่ดื่ม</asp:ListItem>
                </asp:RadioButtonList></td>
                <td align="left"><asp:TextBox ID="txtAlcoholFQ" runat="server" Width="50px"></asp:TextBox>
                  &nbsp;<asp:Label ID="lblAlcohol" runat="server" Text=" ครั้ง/สัปดาห์"></asp:Label>
                   </td>
              </tr>
            </table>
            </td>
      </tr>
 
     
      </table></td> 
  </tr>


   <tr>
    <td align="center">
         &nbsp;</td>
  </tr>
      <tr>
    <td>
        <table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td width="100">สถานะ</td>
        <td>
            <asp:CheckBox ID="chkStatus" runat="server" Text="จบการทำงาน" />
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center">
        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="100px" />
        </td>
  </tr>

  <tr>
    <td align="center">&nbsp;
        <asp:Label ID="lblNotic" runat="server" CssClass="text12_nblue" Text="ท่านต้องบันทึกเอกสารหลักก่อน ถึงจะสามารถบันทึก รายละเอียดได้"></asp:Label>
        </td>
  </tr>
  

</table>
  </div>
     
</div>


  <asp:Panel ID="pnDetail" runat="server">
<h2>Patient Profile
       <small>Patient Screening</small>
   </h2> <br />
<div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">โรคที่เป็น</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">   
          <table align="left" class="table50">
            <tr>
                <td width="150">เพิ่มโรคที่เป็น</td>
                <td>
                    <dx:ASPxComboBox ID="cboDesease" runat="server" CssClass="OptionControl2" width="100%" EnableTheming="True" ToolTip="พิมพ์ค้นหาได้" Theme="MetropolisBlue">
                        <Columns>
                            <dx:ListBoxColumn FieldName="Name">
                            </dx:ListBoxColumn>
                        </Columns>
                        <ItemStyle CssClass="OptionControlText" />
                    </dx:ASPxComboBox>
                </td>
                <td width="80" align="center">
                    &nbsp;</td>
            </tr>
              <tr>
                  <td>ระบุ</td>
                  <td>
                      <asp:TextBox ID="txtDeseaseOther" runat="server" CssClass="OptionControl" Width="100%"></asp:TextBox>
                  </td>
                  <td align="center" width="80">
                      <asp:Button ID="cmdAddDesease" runat="server" CssClass="buttonRedial" Text="เพิ่มโรค" Width="70px" />
                  </td>
              </tr>
              <tr>
                  <td colspan="3">
                      <asp:GridView ID="grdDesease" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%">
                          <RowStyle BackColor="White" VerticalAlign="Top" />
                          <columns>
                              <asp:BoundField HeaderText="No">
                              <HeaderStyle HorizontalAlign="Center" />
                              <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                              </asp:BoundField>
                              <asp:BoundField DataField="DeseaseName" HeaderText="โรคที่เป็น">
                              <headerstyle HorizontalAlign="Left" />
                              <itemstyle HorizontalAlign="Left" />
                              </asp:BoundField>
                              <asp:TemplateField HeaderText="ลบ">
                                  <ItemTemplate>
                                      <asp:ImageButton ID="imgDel_DS" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' CssClass="gridbutton" ImageUrl="images/icon-delete.png" />
                                  </ItemTemplate>
                                  <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                              </asp:TemplateField>
                          </columns>
                          <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                          <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                          <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                          <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                          <EditRowStyle BackColor="#2461BF" />
                          <AlternatingRowStyle BackColor="#F7F7F7" />
                      </asp:GridView>
                  </td>
              </tr>
        </table>
    
      </div>
</div>

<div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Laboratory</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">  
            <table width="100%">            
                <tr>
                    <td valign="top">
                        
                        
                        <asp:GridView ID="grdLab" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" DataKeyNames="LabUID" Width="100%">
                            <RowStyle BackColor="White" VerticalAlign="Top" />
                            <columns>
                                <asp:BoundField DataField="Name" HeaderText="Name" >
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="ผล">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtResultValue" runat="server" CssClass="LabResult" Text='<%# DataBinder.Eval(Container.DataItem, "ResultValue") %>' Width="70px"  ></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="UOM" HeaderText="หน่วย" />
                                <asp:BoundField DataField="NormalRange" HeaderText="ค่าปกติ" />
                            </columns>
                            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <headerstyle   Font-Bold="True" VerticalAlign="Middle" />
                            <EditRowStyle BackColor="#2461BF" />
                            <AlternatingRowStyle BackColor="#F7F7F7" />
                        </asp:GridView>
                     </td>
                    <td  valign="top">
                      
                         <asp:GridView ID="grdLab2" runat="server" AutoGenerateColumns="False" CellPadding="0" DataKeyNames="LabUID" ForeColor="#333333" GridLines="None" Width="100%">
                            <RowStyle BackColor="White" VerticalAlign="Top" />
                            <columns>
                                <asp:BoundField DataField="Name" HeaderText="Name">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="ผล">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtResultValue0" runat="server" CssClass="LabResult" Text='<%# DataBinder.Eval(Container.DataItem, "ResultValue") %>' Width="70px"></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="UOM" HeaderText="UOM" />
                                <asp:BoundField DataField="NormalRange" HeaderText="ค่าปกติ" />
                            </columns>
                            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <headerstyle   Font-Bold="True" VerticalAlign="Middle" />
                            <EditRowStyle BackColor="#2461BF" />
                            <AlternatingRowStyle BackColor="#F7F7F7" />
                        </asp:GridView>
                      
                        


                    </td>
                </tr>
                <tr>
                  
                    <td align="center" colspan="2">
                        <asp:Button ID="cmdAddLab" runat="server" CssClass="buttonRedial" Text="บันทึก" Width="100px" />
                    </td>
                </tr>

            </table>
       </div>
        <div class="box-footer">
          
            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="buttonFind" Height="25px" NavigateUrl="ResultCumulative.aspx" Target="_blank">Cumulative View</asp:HyperLink>
          
        </div>
        <!-- /.box-footer-->
      </div>

<div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">PE</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">  
            <table width="100%">            
                <tr>
                    <td valign="top" width="100">การตรวจเท้า</td>
                    <td  valign="top">
                        <table width="100%">
                            <tr>
                                <td width="180">
                                    <asp:CheckBox ID="chkPitting" runat="server" Text="Pitting edema" />
                                </td>
                                <td width="40">ระดับ</td>
                                <td>
                                    <asp:TextBox ID="txtPitting" runat="server" Width="90%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkWound" runat="server" Text="Wound  " />
                                </td>
                                <td>ระบุ</td>
                                <td>
                                    <asp:TextBox ID="txtWound" runat="server" Width="90%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkPeripheral" runat="server" Text="Peripheral Neuropathy " />
                                </td>
                                <td>ระบุ</td>
                                <td>
                                    <asp:TextBox ID="txtPeripheral" runat="server" Width="90%"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                   <tr>
                    <td valign="top">อาการอื่นๆ</td>
                    <td  valign="top">
                        <asp:TextBox ID="txtPEOther" runat="server" Width="90%"></asp:TextBox>
                       </td>
                </tr>
                <tr>
                  
                    <td align="center" colspan="2">
                        <asp:Button ID="cmdPE_Save" runat="server" CssClass="buttonRedial" Text="บันทึก" Width="100px" />
                    </td>
                </tr>

            </table>
       </div>
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
          

<div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">ปัญหาจากการใช้ยา</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">  
    
  
        <table width="100%">
            <tr>
                <td width="150">Ref. ID</td>
                <td>
                    <asp:Label ID="lblUID_Drug" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>ปัญหาที่พบ</td>
                <td>
                    <dx:ASPxComboBox ID="cboProblemGroup" runat="server" AutoPostBack="True" CssClass="OptionControl2" EnableTheming="True" ToolTip="พิมพ์ค้นหาได้" Theme="MetropolisBlue">
                        <Columns>
                            <dx:ListBoxColumn FieldName="Descriptions">
                            </dx:ListBoxColumn>
                        </Columns>
                    </dx:ASPxComboBox>
                </td>
            </tr>
            <tr>
                <td>ปัญหาย่อย</td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <dx:ASPxComboBox ID="cboProblemItem" runat="server" Theme="MetropolisBlue" CssClass="OptionControl2" EnableTheming="True" ToolTip="พิมพ์ค้นหาได้">
                                <Columns>
                                    <dx:ListBoxColumn FieldName="Descriptions">
                                    </dx:ListBoxColumn>
                                </Columns>
                            </dx:ASPxComboBox>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="cboProblemGroup"  EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
           
            <tr>
                <td>กรณี อื่นๆ ระบุ</td>
                <td>
                    <asp:TextBox ID="txtProblemOtherDrug" runat="server" CssClass="OptionControl2"></asp:TextBox>
                </td>
            </tr>
             <tr>
                 <td>Drug related</td>
                 <td>
                     <dx:ASPxComboBox ID="cboDrug" runat="server" CssClass="OptionControl2" Theme="MetropolisBlue" EnableTheming="True" ToolTip="พิมพ์ค้นหาได้">
                         <Columns>
                             <dx:ListBoxColumn FieldName="Name">
                             </dx:ListBoxColumn>
                         </Columns>
                     </dx:ASPxComboBox>
                 </td>
            </tr>
            <tr>
                <td>Interventions</td>
                <td>
                    <asp:TextBox ID="txtInterventionDrug" runat="server" CssClass="OptionControl2"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td valign="top">สรุป</td>
                <td>
                    <asp:RadioButtonList ID="optResult_D" runat="server"  RepeatDirection="Horizontal" RepeatColumns="3">
                         <asp:ListItem Value="1" Selected="True">แก้ได้เลย (Resolved)</asp:ListItem>
                        <asp:ListItem Value="2">ส่งต่อ (Refer)</asp:ListItem>  
                        <asp:ListItem Value="3">ดีขึ้นแต่ยังเป็นปัญหา ติดตามต่อ</asp:ListItem>
                        <asp:ListItem Value="4">มีปัญหาเพิ่มขึ้น  / ปัญหาหนักขึ้น  ติดตามต่อ</asp:ListItem>
                        <asp:ListItem Value="5">ไม่ดีขึ้น ติดตามต่อ</asp:ListItem> 
                        <asp:ListItem Value="9">อื่นๆ</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:TextBox ID="txtResultOther_D" runat="server" CssClass="OptionControl2"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel ID="pnAlertM" runat="server">
                  <div class="alert alert-danger alert-dismissible">
                
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <p><asp:Label ID="lblAlertM" runat="server"></asp:Label></p>
              </div>
                   </asp:Panel>

                </td>
            </tr>
            <tr>
              <td>
                  
              </td>
              <td align="left">
        <asp:Button ID="cmdAddDrugProblem" runat="server" Text="เพิ่ม" CssClass="buttonRedial" Width="70px" />
                </td>
            </tr>
        </table>
       
    <asp:GridView ID="grdDrugProblem" runat="server" CellPadding="0" ForeColor="#333333"  GridLines="None"  AutoGenerateColumns="False" Width="100%">
            <RowStyle  BackColor="White"  VerticalAlign="Top" />
            <columns>
            <asp:BoundField HeaderText="No" DataField="nRow">
                <HeaderStyle HorizontalAlign="Center" />
              <itemstyle HorizontalAlign="Center" VerticalAlign="Top" />                      </asp:BoundField>
                <asp:BoundField DataField="ProblemDesc" HeaderText="ปัญหาที่พบ" >
                </asp:BoundField>
                <asp:BoundField DataField="DrugName" HeaderText="Drug related problems">
                <headerstyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Interventions" HeaderText="Interventions ">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                 
                <asp:BoundField DataField="ResultTXT" HeaderText="สรุป" />
            <asp:TemplateField HeaderText="Edit">
              <ItemTemplate> 
                     <asp:ImageButton   ID="imgEdit_D" runat="server" ImageUrl="images/icon-edit.png" CssClass="gridbutton"
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID")%>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Del">
              <ItemTemplate> 
                     <asp:ImageButton   ID="imgDel_D" runat="server" CssClass="gridbutton"
                                    ImageUrl="images/icon-delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="#F7F7F7" />
          </asp:GridView>
       
             <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->  
</div> 
      <div align="center">
      <asp:Button ID="cmdSave2" runat="server" CssClass="buttonSave" Text="บันทึก" Width="100px" />
          </div>
  </asp:Panel>
</section>
    
</asp:Content>