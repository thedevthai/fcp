﻿
Public Class vmiStock
    Inherits System.Web.UI.Page

    Dim dt As New DataTable

    Dim acc As New UserController
    Dim ctlbase As New ApplicationBaseClass
    Dim ctlOrder As New SmokingController
    Dim ctlSk As New StockController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("Username") Is Nothing Then
        '    Response.Redirect("Default.aspx?logout=YES")
        'End If
        If Not IsPostBack Then
            grdData.PageIndex = 0
            'lblLocationID.Text = Session("LocationID")
            'lblLocationName.Text = Session("NameOfUser")
            LoadStock()
        End If

    End Sub
    Private Sub LoadStock()


        dt = ctlSk.Stock_GetByLocation(Session("LocationID"))

        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"

                    ctlSk.Stock_Disable(e.CommandArgument, Session("Username"))
                    acc.User_GenLogfile(Session("Username"), ACTTYPE_DEL, "Stock", "Disable Stock MedUID:" & e.CommandArgument(), "")


                    DisplayMessage(Me, "ลบข้อมูลเรียบร้อย")
                    LoadStock()
            End Select


        End If
    End Sub


    Private Sub EditData(ByVal pID As Integer)

        dt = ctlSk.Stock_GetByMedUID(pID, Session("LocationID"))

            If dt.Rows.Count > 0 Then

            isAdd = False
                    lblUID.Text = DBNull2Str(dt.Rows(0)("UID"))
                    Me.lblMedName.Text = DBNull2Str(dt.Rows(0)("itemName"))
                    'lblLocationID.Text = DBNull2Str(dt.Rows(0)("LocationID"))
                    'lblLocationName.Text = DBNull2Str(dt.Rows(0)("LocationName"))
                    'ddlProvince.SelectedValue = DBNull2Str(dt.Rows(0)("ProvinceID"))
                    txtBalance.Text = DBNull2Zero(dt.Rows(0)("OnHand"))
                    txtStock.Text = DBNull2Zero(dt.Rows(0)("Stock"))
                    lblUOM.Text = DBNull2Str(dt.Rows(0)("UOM"))
                    txtROP.Text = DBNull2Zero(dt.Rows(0)("ROP"))
                    lblMedUID.Text = DBNull2Str(dt.Rows(0)("MedUID"))

        End If
            dt = Nothing
    End Sub
    Private Sub ClearData()
        'Me.lblCode.Text = ""
        'lblLocationName.Text = ""
        lblMedName.Text = ""
        'lblLocationID.Text = ""
        lblMedName.Text = ""
        lblMedUID.Text = ""
        txtStock.Text = ""
        txtROP.Text = ""
        txtBalance.Text = ""
        'lblLocationName.Text = ""
        lblUOM.Text = ""
        lblUID.Text = ""
    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(6).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

            If StrNull2Zero(e.Row.Cells(4).Text) <= StrNull2Zero(e.Row.Cells(3).Text) Then
                e.Row.ForeColor = Drawing.Color.Red
            End If


        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub
    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        ctlSk.Stock_Save(Session("LocationID"), lblMedUID.Text, lblUOM.Text, StrNull2Zero(txtStock.Text), StrNull2Zero(txtROP.Text), StrNull2Zero(txtBalance.Text), Session("Username"))

        acc.User_GenLogfile(Session("username"), ACTTYPE_ADD, "VMI_Stock", "Add/Update Stock : " & Session("LocationID") & ">>" & lblMedName.Text & ">>Stock:" & txtStock.Text & ">>ROP:" & txtROP.Text & ">>OH:" & txtBalance.Text, "")

        LoadStock()
        DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อยแล้ว")
        ClearData()
    End Sub



    Protected Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        ClearData()
    End Sub



End Class

