﻿
Public Class vmiStock_Old
    Inherits System.Web.UI.Page

    Dim dt As New DataTable

    Dim ctlSk As New StockController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("Username") Is Nothing Then
        '    Response.Redirect("Default.aspx?logout=YES")
        'End If
        If Not IsPostBack Then
            grdData.PageIndex = 0


            LoadStock()
        End If

    End Sub


    Private Sub LoadStock()
        dt = ctlSk.Stock_GetSearch(txtMedName.Text.Trim(), Session("LocationID"), "0")

        If dt.Rows.Count > 0 Then
            cmdSave.Visible = True
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                Try

                    Dim nrow As Integer = dt.Rows.Count
                    If .PageCount > 1 Then
                        If .PageIndex > 0 Then
                            If (.PageSize) < nrow - (.PageSize * .PageIndex) Then
                                For i = 0 To .PageSize - 1
                                    .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                Next
                            Else
                                For i = 0 To nrow - (.PageSize * .PageIndex) - 1
                                    .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                Next
                            End If
                        Else
                            For i = 0 To .PageSize - 1
                                .Rows(i).Cells(0).Text = i + 1
                            Next
                        End If
                    Else
                        For i = 0 To nrow - 1
                            .Rows(i).Cells(0).Text = i + 1
                        Next
                    End If
                Catch ex As Exception

                End Try

            End With
        Else
            cmdSave.Visible = False
        End If
        dt = Nothing
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadStock()
    End Sub
    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())

                'lblCode.Text = grdData.Rows(
                Case "imgDel"

                    'acc.User_GenLogfile(Session("Username"), ACTTYPE_DEL, "Provinces", "Delete Province:" & lblCode.Text & ">>" & txtName.Text, "")
                    DisplayMessage(Me, "ลบข้อมูลเรียบร้อย")

            End Select


        End If
    End Sub


    Private Sub EditData(ByVal pID As String)

        'dt = ctlbase.Province_GetByID(pID)

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False

                'Me.lblCode.Text = DBNull2Str(dt.Rows(0)("ProvinceID"))
                Me.txtMedName.Text = DBNull2Str(dt.Rows(0)("ProvinceID"))

            End With
        End If
        dt = Nothing
    End Sub


    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim txtR As TextBox
            txtR = e.Row.Cells(4).FindControl("txtROP")
            If StrNull2Zero(e.Row.Cells(3).Text) <= StrNull2Zero(txtR.Text) Then
                e.Row.ForeColor = Drawing.Color.Red
            End If

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub

    Dim acc As New UserController

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        Dim txtR As New TextBox

        For i = 0 To grdData.Rows.Count - 1
            txtR = grdData.Rows(i).Cells(0).FindControl("txtROP")
            ctlSk.Stock_UpdateROPByMedUID(Session("LocationID"), grdData.DataKeys(i).Value, txtR.Text, Session("Username"))
            acc.User_GenLogfile(Session("Username"), ACTTYPE_UPD, "Stock", "Update ROP :ร้าน " & Session("LocationID") & ">>ยา " & grdData.Rows(i).Cells(1).Text & ">>=" & txtR.Text, "")
        Next
        DisplayMessage(Me, "บันทึกข้อมูลเรียบร้อย")
    End Sub

    Protected Sub cmdSearch_Click(sender As Object, e As EventArgs) Handles cmdSearch.Click
        LoadStock()
    End Sub
End Class

