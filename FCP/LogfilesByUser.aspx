﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="LogfilesByUser.aspx.vb" Inherits=".LogfilesByUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
 <link rel="stylesheet" type="text/css" href="css/pagestyles.css"> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>ตรวจสอบประวัติการใช้งานของ User
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
    
 <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลกิจกรรม</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">




 <table width="100%" border="0" cellspacing="2" cellpadding="0">
  <tr>
    <td align="left">
        <asp:Label ID="lblOnline" runat="server" CssClass="GreenAlert" Width="99%"></asp:Label>
      </td>
  </tr>
  <tr>
    <td align="left">&nbsp;</td>
  </tr>
  <tr>
    <td><table border="0" align="center" cellpadding="0" cellspacing="2">
      <tr>
        <td width="100">เลือกผู้ใช้ </td>
<td>
                                               
                                               <asp:DropDownList ID="ddlUserID" 
                runat="server" CssClass="OptionControl" > </asp:DropDownList>
                                             </td>
        <td>
                                              <img src="images/arrow-orange-icons.png" width="14" height="14" /><asp:TextBox 
                                                  ID="txtFind" runat="server"></asp:TextBox>
                                              <asp:Button ID="cmdSearch" runat="server" CssClass="buttonFind" Text="ค้นหา" />
          </td>
      </tr>
      <tr>
        <td>ตั้งแต่</td>
        <td>
            <asp:TextBox ID="txtBeginDate" runat="server"></asp:TextBox>
          </td>
        <td>รูปแบบวันที่ </td>
      </tr>
      <tr>
        <td>ถึง</td>
        <td>
            <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
          </td>
        <td>dd/mm/yyyy ปี พ.ศ.</td>
      </tr>
      <tr>
        <td>ค้นหา</td>
        <td colspan="2">
            <asp:TextBox ID="txtSearch" runat="server" Width="200px"></asp:TextBox>
          </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
            <asp:Button ID="cmdFind" runat="server" CssClass="buttonSave" Text="ดูประวัติการใช้งาน" />
          </td>
        <td>&nbsp;</td>
      </tr>
      </table></td>
  </tr>
  <tr>
    <td><asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" CssClass="txtcontent" 
                             Font-Bold="False" PageSize="20">
                        <RowStyle BackColor="#ffffff" />
                        <columns>
                            <asp:BoundField HeaderText="No." DataField="LogID">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />                            </asp:BoundField>
                            <asp:BoundField DataField="Username" HeaderText="Username">
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        <asp:BoundField DataField="Work_Date" HeaderText="วันที่" />
                            <asp:BoundField DataField="Act_Type" HeaderText="ประเภท" />
                            <asp:BoundField DataField="Descrp" HeaderText="คำอธิบาย" />
                            <asp:BoundField DataField="Remark" HeaderText="หมายเหตุ" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />                            </asp:BoundField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="#f7f7f7" />
                     </asp:GridView>  </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
     </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
 </section>
</asp:Content>
