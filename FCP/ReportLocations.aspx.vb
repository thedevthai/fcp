﻿Public Class ReportLocations
    Inherits System.Web.UI.Page

    Dim ctlLG As New LocationController
    Dim dt As New DataTable
    Dim ds As New DataSet
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then
            grdData.PageIndex = 0
            LoadProjectToDDL()
            LoadProvinceToDDL()
            LoadLocationGroupToDDL()

            If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess Or Convert.ToInt32(Request.Cookies("RoleID").Value) = isProjectManager Then
                ddlProj.SelectedValue = Request.Cookies("PRJMNG").Value
                ddlProj.Enabled = False
            End If

            LoadLocationToGrid()

        End If
    End Sub
    Private Sub LoadProjectToDDL()
        Dim ctlPj As New ProjectController
        dt = ctlPj.Project_GetAll
        If dt.Rows.Count > 0 Then
            With ddlProj
                .DataSource = dt
                .DataTextField = "Description"
                .DataValueField = "ProjectID"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadLocationToGrid()

        dt = ctlLG.Location_GetReport(ddlType.SelectedValue, ddlProvince.SelectedValue, StrNull2Zero(ddlProj.SelectedValue))

        If dt.Rows.Count > 0 Then
            lblCount.Text = "มีร้านยาเข้าร่วมโครงการทั้งหมด " & dt.Rows.Count & " ร้าน"

            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                'For i = 0 To .Rows.Count - 1
                '    .Rows(i).Cells(0).Text = i + 1
                'Next
                Try


                    Dim nrow As Integer = dt.Rows.Count
                    If .PageCount > 1 Then
                        If .PageIndex > 0 Then
                            If (.PageSize) < nrow - (.PageSize * .PageIndex) Then
                                For i = 0 To .PageSize - 1
                                    .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                Next
                            Else
                                For i = 0 To nrow - (.PageSize * .PageIndex) - 1
                                    .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                Next
                            End If
                        Else
                            For i = 0 To .PageSize - 1
                                .Rows(i).Cells(0).Text = i + 1
                            Next
                        End If
                    Else
                        For i = 0 To nrow - 1
                            .Rows(i).Cells(0).Text = i + 1
                        Next
                    End If
                Catch ex As Exception
                    ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','" & ex.Message & "');", True)
                End Try
            End With
        Else
            lblCount.Text = "ไม่มีร้านยาเข้าร่วมโครงการ"
            grdData.DataSource = Nothing
            grdData.Visible = False
        End If
        dt = Nothing
    End Sub

    Private Sub LoadProvinceToDDL()
        Dim ctlbase As New ApplicationBaseClass
        dt = ctlbase.LoadProvince
        ddlProvince.Items.Clear()

        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .Items.Add("---ทั้งหมด---")
                .Items(0).Value = "0"
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)("ProvinceName"))
                    .Items(i + 1).Value = dt.Rows(i)("ProvinceID")
                Next

                '.DataSource = dt
                '.DataTextField = "ProvinceName"
                '.DataValueField = "ProvinceID"
                '.DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadLocationGroupToDDL()

        Dim ctlLG As New LocationGroupController
        dt = ctlLG.LocationGroup_Get
        ddlType.Items.Clear()
        If dt.Rows.Count > 0 Then
            With ddlType
                .Enabled = True
                .Items.Add("---ทั้งหมด---")
                .Items(0).Value = "0"
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)("Name"))
                    .Items(i + 1).Value = dt.Rows(i)("Code")
                Next
                '.DataSource = dt
                '.DataTextField = "Name"
                '.DataValueField = "Code"
                '.DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadLocationToGrid()
    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub

    Protected Sub cmdView_Click(sender As Object, e As EventArgs) Handles cmdView.Click
        grdData.PageIndex = 0
        LoadLocationToGrid()
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlType.SelectedIndexChanged
        grdData.PageIndex = 0
        LoadLocationToGrid()
    End Sub

    Protected Sub ddlProvince_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProvince.SelectedIndexChanged
        grdData.PageIndex = 0
        LoadLocationToGrid()
    End Sub

    Protected Sub cmdPrint_Click(sender As Object, e As EventArgs) Handles cmdPrint.Click
        ReportsName = "ReportLocations"
        Response.Redirect("rptLocations.aspx?p=" & ddlProvince.SelectedValue & "&grp=" & ddlType.SelectedValue & "&pj=" & ddlProj.SelectedValue)
    End Sub

    Protected Sub cmdExcel_Click(sender As Object, e As EventArgs) Handles cmdExcel.Click
        ReportsName = "ReportLocations"
        Response.Redirect("rptLocations.aspx?ex=1&p=" & ddlProvince.SelectedValue & "&grp=" & ddlType.SelectedValue & "&pj=" & ddlProj.SelectedValue)
    End Sub
End Class

