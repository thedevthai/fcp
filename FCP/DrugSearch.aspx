﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DrugSearch.aspx.vb" Inherits=".DrugSearch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>  
    
  <link rel="stylesheet" type="text/css" href="css/rajchasistyles.css"> 
</head>
<body>
     <form id="form1" runat="server">
    <div>
        
  <table width="100%">
                                             <tr>
                                                 <td width="50px">ค้นหา</td>
                                                 <td>
                                                     <table>
                                                         <tr>
                                                             <td>
                                                     <asp:TextBox ID="txtDrugSearch" runat="server" Width="250px"></asp:TextBox>
                                                             </td>
                                                             <td>
                                                     <asp:Button ID="cmdDrugSearch" runat="server" Text="ค้นหา" CssClass="buttonSearch" />
                                                             </td>
                                                         </tr>
                                                     </table>
                                                 </td>
                                             </tr>
                                             <tr>
                                                 <td colspan="2">
        <asp:ListBox ID="lstData" runat="server" CssClass="cssList" Width="100%" Font-Names="Tahoma" Font-Size="9pt" Rows="20">      </asp:ListBox>
                                                 </td>
                                             </tr>
                                             <tr>
                                                 <td colspan="2" align="center">
        <asp:Button ID="cmdOK" runat="server" CssClass="btnCss" Text="OK" Width="100px" />
                                                 </td>
                                             </tr>
                                             </table>
    </div>
    </form>
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {

            $('.btnCss').click(function () {

                var scripts = new Array(); 
                var pages = '<% =String.Concat(pageName) %>';          

                $('.cssList').each(function () {
                    if ($(this).attr("selected", true)) {
                        scripts.push($(this).val());                         
                    }
                });               

                window.opener.getListItems(scripts, pages);                
                window.close();
            });
        });

    </script>
</body>
</html>
