﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="ReportLocations.aspx.vb" Inherits=".ReportLocations" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    </asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>รายงานข้อมูลร้านยา
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
  
  <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลร้านยา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">




    <table width="99%" border="0" align="left" cellpadding="0" cellspacing="2">
<tr>
          <td align="center" valign="top">&nbsp;</td>
      </tr>
       
        <tr>
            <td align="center" valign="top">
             
            
<table border="0" cellPadding="2" cellSpacing="2">
                                                <tr>
                                                    <td align="left" class="texttopic">โครงการ:</td>
                                                    <td width="500" align="left" class="texttopic">
                                                        <asp:DropDownList ID="ddlProj" runat="server" AutoPostBack="True">                                                        </asp:DropDownList>                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="texttopic">ประเภท :</td>
                                                    <td width="500" align="left" class="texttopic">
                                                        <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True" CssClass="OptionControl">                                                        </asp:DropDownList>                                                    </td>
                                                </tr>
                                                <tr>
                                                  <td align="left" class="texttopic">จังหวัด : </td>
                                                  <td align="left" class="texttopic"><asp:DropDownList CssClass="OptionControl" 
                                                            ID="ddlProvince" runat="server" AutoPostBack="True"> </asp:DropDownList></td>
                                                </tr>
                                                <tr>
                                                  <td align="left" class="texttopic">&nbsp;</td>
                                                  <td align="left" class="texttopic">
           <asp:Button ID="cmdView" runat="server" text="ดูรายงาน" cssclass="buttonSave" />
                                                    </td>
                                                </tr>
  </table>      </td>
      </tr>
       <tr>
          <td align="left" valign="top"  id="line_pink_dott">
              <asp:Label ID="lblCount" runat="server"></asp:Label>
&nbsp;</td>
        </tr>
      
        <tr>
          <td align="left" valign="top">
              F : โครงการกิจกรรมการให้ความรู้ คำแนะนำและคำปรึกษา
              <br />
              A : โครงการสนับสนุนการทำกิจกรรมปรับเปลี่ยนพฤติกรรมผู้เสพยาสูบโดยเภสัชกรชุมชน</td>
      </tr>
      
        <tr>
          <td align="center" valign="top">
              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="No.">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />                      </asp:BoundField>
                <asp:BoundField DataField="ProjectName" HeaderText="โครงการ">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
            <asp:BoundField DataField="LocationID" HeaderText="รหัส">
              <headerstyle HorizontalAlign="Center" />          
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />                      </asp:BoundField>
            <asp:BoundField DataField="LocationName" HeaderText="ร้านยา">
              <headerstyle HorizontalAlign="Left" />          
              <itemstyle HorizontalAlign="Left" />                      </asp:BoundField>
                <asp:BoundField DataField="ProvinceName" HeaderText="จังหวัด">
                <ItemStyle HorizontalAlign="Left" />                </asp:BoundField>
                <asp:BoundField DataField="LocationGroupName" HeaderText="ประเภทร้านยา" />
                <asp:BoundField DataField="Co_Name" HeaderText="ผู้ประสานงาน">
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Office_Tel" HeaderText="เบอร์โทร">
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>
                                 </td>
      </tr>
        <tr>
          <td align="center" valign="top">
           <asp:Button ID="cmdPrint" runat="server" text="พิมพ์" cssclass="buttonSave" Width="80px"  />
          <asp:Button ID="cmdExcel" runat="server" text="Export" cssclass="buttonGreen" Width="80px" />     
          </td>
        </tr>
    </table>
                 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

    </section>
</asp:Content>
