﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Subgurim.Controles
'Imports Rajchasi
Public Class COVID3
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlL As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlCovid As New CovidController
    Dim sAlert As String

    Dim isValid As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx")
        End If

        'If Session("patientid") Is Nothing Then
        '    Response.Redirect("PatientSearch.aspx")
        'End If

        If Not IsPostBack Then
            isAdd = True

            pnAlert.Visible = False
            pnSuccess.Visible = False

            If Request("acttype") = "view" Then
                cmdSave.Visible = False
            Else
                cmdSave.Visible = True
            End If

            txtSEQ.Text = "1"
            txtServiceDate.Text = DisplayShortDateTH(ctlCovid.GET_DATE_SERVER)

            LoadLocationData(Request.Cookies("LocationID").Value)
            LoadPharmacist(Request.Cookies("LocationID").Value)

            If Not Request("fid") Is Nothing Then
                LoadCOVIDHeader()
            Else
                If Request("t") = "new" Or (Request("t") Is Nothing) Then
                    pnCOVID.Visible = False
                    pnResult.Visible = False
                    LoadCOVIDHeader()
                End If
            End If
        End If

        'Dim strDR, strDF, strICD As String

        'strDR = "javascript:void(window.open('DrugSearch.aspx?p=drugremain',null,'scrollbars=1,width=650,HEIGHT=550'));"
        'strDF = "javascript:void(window.open('DrugSearch.aspx?p=drugrefill',null,'scrollbars=1,width=650,HEIGHT=550'));"
        'strICD = "javascript:void(window.open('ICD10Search.aspx?p=icd10',null,'scrollbars=1,width=650,HEIGHT=550'));"

        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtTemp.Attributes.Add("OnKeyPress", "return AllowOnlyDouble();")
        txtO2sat.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtSEQ.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")

        txtSmokeQTY.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtBegin.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtEnd.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")

        Dim scriptString As String = "javascript:return confirm(""คุณแน่ใจที่จะลบรายการรับบริการนี้ใช่หรือไม่?"");"
        'cmdDelete.Attributes.Add("onClick", scriptString)
    End Sub


    Private Sub LoadPharmacist(LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With
            End If
        End If
        dtP = Nothing
    End Sub

    Private Sub LoadLocationData(LocationID As String)
        dt = ctlL.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationCode.Text = DBNull2Str(.Item("LocationCode"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("ProvinceName"))
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadCOVIDHeader()
        If Not Request("t") Is Nothing Then
            If Request("t").ToLower() = "new" Then
                dt = ctlCovid.COVID3_Head_Get(Session("patientid"))
            End If
        Else
            dt = ctlCovid.COVID3_Head_GetByUID(StrNull2Zero(Request("fid")))
        End If

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False

                pnCOVID.Visible = True
                pnResult.Visible = True
                'txtBYear.Text = .Item("BYear")
                lblCOVIDUID.Text = .Item("UID")
                'txtSEQ.Text = DBNull2Str(.Item("SEQ"))

                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                Session("patientid") = DBNull2Lng(.Item("PatientID"))

                'lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                'lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                If String.Concat(.Item("TestDate")) <> "" Then
                    txtSwabDate1.Text = DisplayStr2ShortDateTH(String.Concat(.Item("TestDate")))
                Else
                    txtSwabDate1.Text = ""
                End If

                If String.Concat(.Item("ConfirmDate")) <> "" Then
                    txtSwabDate2.Text = DisplayStr2ShortDateTH(String.Concat(.Item("ConfirmDate")))
                Else
                    txtSwabDate2.Text = ""
                End If

                If DBNull2Str(.Item("IsHomeIsolation")) = "Y" Then
                    txtStartDate.Text = DisplayStr2ShortDateTH(String.Concat(.Item("AdmitDate")))
                End If

                If DBNull2Str(.Item("IsCall")) = "Y" Then
                    txtCallDate.Text = DisplayStr2ShortDateTH(String.Concat(.Item("CallDate")))
                End If
                optCall.SelectedValue = DBNull2Str(.Item("IsCall"))

                If DBNull2Str(.Item("IsMedicine")) = "Y" Then
                    Dim sMed As String()
                    sMed = Split(DBNull2Str(.Item("Medicine")), "|")

                    For i = 0 To sMed.Length - 1
                        Select Case sMed(i)
                            Case "M1"
                                chkMed.Items(0).Selected = True
                            Case "M2"
                                chkMed.Items(1).Selected = True
                            Case "M3"
                                chkMed.Items(2).Selected = True
                            Case "M4"
                                chkMed.Items(3).Selected = True
                            Case "M5"
                                chkMed.Items(4).Selected = True
                            Case "M6"
                                chkMed.Items(5).Selected = True
                            Case "M99"
                                chkMed.Items(6).Selected = True
                        End Select
                    Next
                End If

                txtMedOther.Text = String.Concat(.Item("MedicineOther"))

                'txtHospitalTemp.Text = String.Concat(.Item("TempHospitalName"))
                'txtHospitalTemp.Text = String.Concat(.Item("HospitalName"))

                chkDisease0.Checked = False
                chkDisease1.Checked = False
                chkDisease2.Checked = False
                chkDisease3.Checked = False
                chkDisease4.Checked = False
                chkDisease5.Checked = False
                chkDisease6.Checked = False
                chkDisease7.Checked = False
                chkDisease8.Checked = False
                chkDisease9.Checked = False
                chkDisease10.Checked = False
                chkDisease11.Checked = False
                chkDisease12.Checked = False
                chkDisease99.Checked = False
                chkDiseaseOther.Checked = False
                txtDiseaseOther.Text = DBNull2Str(.Item("DiseaseOther"))


                txtSmokeQTY.Text = ""
                txtY.Text = ""
                txtM.Text = ""
                txtD.Text = ""
                optSmoke.SelectedValue = String.Concat(.Item("isSmoke"))
                If optSmoke.SelectedValue = "1" Then
                    txtSmokeQTY.Enabled = True
                    txtSmokeQTY.Text = String.Concat(.Item("SmokeQTY"))
                    txtY.Enabled = False
                    txtM.Enabled = False
                    txtD.Enabled = False
                ElseIf optSmoke.SelectedValue = "2" Then
                    txtSmokeQTY.Enabled = False
                    txtY.Enabled = False
                    txtM.Enabled = False
                    txtD.Enabled = False
                Else
                    txtSmokeQTY.Enabled = False
                    txtY.Enabled = True
                    txtM.Enabled = True
                    txtD.Enabled = True
                    Dim QuitDay As String
                    QuitDay = Replace(Replace(Replace(String.Concat(.Item("QuitDay")), "Y", "|"), "M", "|"), "D", "")
                    Dim str() As String
                    str = Split(QuitDay, "|")
                    txtY.Text = IIf(str(0) = "0", "", str(0))
                    txtM.Text = IIf(str(1) = "0", "", str(1))
                    txtD.Text = IIf(str(2) = "0", "", str(2))
                End If

                chkDisease0.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease0")))
                chkDisease1.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease1")))
                chkDisease2.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease2")))
                chkDisease3.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease3")))
                chkDisease4.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease4")))
                chkDisease5.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease5")))
                chkDisease6.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease6")))
                chkDisease7.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease7")))
                chkDisease8.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease8")))
                chkDisease9.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease9")))
                chkDisease10.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease10")))
                chkDisease11.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease11")))
                chkDisease12.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease12")))
                chkDisease99.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease99")))

                If txtDiseaseOther.Text <> "" Then
                    chkDiseaseOther.Checked = True
                Else
                    chkDiseaseOther.Checked = False
                End If


                LoadPharmacist(lblLocationID.Text)
                'ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))

                chkStatus.Checked = ConvertYesNo2Boolean(DBNull2Str(.Item("CloseStatus")))

                'If DBNull2Str(.Item("CloseStatus")) = "Y" Then
                optFinalResult.SelectedValue = DBNull2Str(.Item("FinalResult"))
                    txtRemarkResult.Text = DBNull2Str(.Item("ResultRemark"))

                    If String.Concat(.Item("ATKDate")) <> "" Then
                        txtATKDate.Text = DisplayStr2ShortDateTH(String.Concat(.Item("ATKDate")))
                    Else
                        txtATKDate.Text = ""
                    End If
                    optATK.SelectedValue = DBNull2Str(.Item("ATKResult"))

                    optSmokeResult.SelectedValue = String.Concat(.Item("SmokeFinal"))
                    txtBegin.Text = String.Concat(.Item("SmokeBegin"))
                    txtEnd.Text = String.Concat(.Item("SmokeEnd"))
                    optIsSmokingService.SelectedValue = String.Concat(.Item("IsA1A5"))
                    txtServiceReason.Text = String.Concat(.Item("ReasonA1A5"))
                    chkA.Checked = ConvertYesNo2Boolean(String.Concat(.Item("IsSmokingFollow")))
                    chkMTM.Checked = ConvertYesNo2Boolean(String.Concat(.Item("IsMTMFollow")))
                    chkF.Checked = ConvertYesNo2Boolean(String.Concat(.Item("IsOtherFollow")))
                    txtFormOther.Text = String.Concat(.Item("FormOther"))

                    'End If


                    If Request("t") = "new" Then
                    lblCOVIDUID.Text = ""
                    txtServiceDate.Text = DisplayShortDateTH(ctlL.GET_DATE_SERVER)
                    txtTime.Text = ""
                    ddlPerson.SelectedIndex = 0
                    pnCOVID.Visible = False
                    pnResult.Visible = False
                Else
                    pnCOVID.Visible = True
                    pnResult.Visible = True
                    LoadCovidDetailToGrid()
                End If

            End With
        Else
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing

    End Sub


    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        isValid = True
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx")
        End If
        If txtSwabDate1.Text = "" Then
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนวันที่ให้บริการก่อน');", True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนวันที่ตรวจ Ag Test ก่อน');", True)
            Exit Sub
        End If
        'If txtSwabDate2.Text = "" Then
        '    ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนวันที่ให้บริการก่อน');", True)
        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนวันที่ตรวจยืนยันผลก่อน');", True)
        '    Exit Sub
        'End If
        If lblLocationID.Text = "" Then
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาตรวจสอบรหัสร้านยาของท่านก่อน แล้วลองใหม่อีกครั้ง")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาตรวจสอบรหัสร้านยาของท่านก่อน แล้วลองใหม่อีกครั้ง');", True)
            Exit Sub
        End If


        Dim objuser As New UserController

        Dim TestDate, CallDate, ATKDate As Integer
        Dim ConfirmDate, AdmitDate As Integer

        Dim isHomeIso, isMed, Medicine As String
        TestDate = 0
        ConfirmDate = 0
        AdmitDate = 0

        Medicine = ""

        If txtSwabDate1.Text <> "" Then
            TestDate = CInt(ConvertStrDate2DBString(txtSwabDate1.Text))
        End If
        If txtSwabDate2.Text <> "" Then
            ConfirmDate = CInt(ConvertStrDate2DBString(txtSwabDate2.Text))
        End If
        If txtStartDate.Text <> "" Then
            AdmitDate = CInt(ConvertStrDate2DBString(txtStartDate.Text))
            isHomeIso = "Y"
        End If

        If txtMedOther.Text <> "" Then
            isMed = "Y"
        End If


        If txtCallDate.Text <> "" Then
            CallDate = CInt(ConvertStrDate2DBString(txtCallDate.Text))
        End If
        If txtATKDate.Text <> "" Then
            atkDate = CInt(ConvertStrDate2DBString(txtATKDate.Text))
        End If

        'If txtHospitalTemp.Text <> "" Then
        '    isTempHos = "Y"
        'End If
        'If txtHospitel.Text <> "" Then
        '    isHospital = "Y"
        'End If

        ' 1 ฟ้าทะลายโจร 2  Favipiravir   3 ยาสมุนไพร   4 Nat Long    5 Ivermectin 6 ยาลดไข้ แก้ไอ  99 อื่นๆ 

        If chkMed.Items(0).Selected Then
            Medicine = "M1M|"
        End If
        If chkMed.Items(1).Selected Then
            Medicine = Medicine & "M2M|"
        End If
        If chkMed.Items(2).Selected Then
            Medicine = Medicine & "M3M|"
        End If
        If chkMed.Items(3).Selected Then
            Medicine = Medicine & "M4M|"
        End If
        If chkMed.Items(4).Selected Then
            Medicine = Medicine & "M5M|"
        End If
        If chkMed.Items(5).Selected Then
            Medicine = Medicine & "M6M|"
        End If
        If chkMed.Items(6).Selected Then
            Medicine = Medicine & "M99"
        End If

        Medicine = Replace(Medicine, "M|M", "|M")
        Medicine = Replace(Medicine, "M|", "")

        If Medicine <> "" Then
            isMed = "Y"
        End If

        If lblCOVIDUID.Text = "" Then 'Add new
            If ctlCovid.COVID3_Head_ChkDupCustomer(Session("patientid")) = True Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านได้บันทึกการให้บริการของผู้รับบริการนี้ ในวันที่ท่านระบุแล้ว');", True)
                Exit Sub
            End If
        End If
        Dim QuitDay As String = ""
        If optSmoke.SelectedValue = "3" Then
            QuitDay = StrNull2Zero(txtY.Text) & "Y" & StrNull2Zero(txtM.Text) & "M" & StrNull2Zero(txtD.Text) & "D"
        End If

        ctlCovid.COVID3_Head_Save(StrNull2Zero(lblCOVIDUID.Text), lblLocationID.Text, Session("patientid"), TestDate, ConfirmDate, isHomeIso, AdmitDate, optCall.SelectedValue, CallDate, isMed, Medicine, optFinalResult.SelectedValue, txtRemarkResult.Text, ATKDate, optATK.SelectedValue, ConvertStatus2YN(chkStatus.Checked), optSmoke.SelectedValue, txtSmokeQTY.Text, QuitDay, ConvertStatus2YN(chkDisease0.Checked), ConvertStatus2YN(chkDisease1.Checked), ConvertStatus2YN(chkDisease2.Checked), ConvertStatus2YN(chkDisease3.Checked), ConvertStatus2YN(chkDisease4.Checked), ConvertStatus2YN(chkDisease5.Checked), ConvertStatus2YN(chkDisease6.Checked), ConvertStatus2YN(chkDisease7.Checked), ConvertStatus2YN(chkDisease8.Checked), ConvertStatus2YN(chkDisease9.Checked), ConvertStatus2YN(chkDisease10.Checked), ConvertStatus2YN(chkDisease11.Checked), ConvertStatus2YN(chkDisease12.Checked), ConvertStatus2YN(chkDisease99.Checked), txtDiseaseOther.Text, optSmokeResult.SelectedValue, txtBegin.Text, txtEnd.Text, optIsSmokingService.SelectedValue, txtServiceReason.Text, ConvertStatus2YN(chkA.Checked), ConvertStatus2YN(chkMTM.Checked), ConvertStatus2YN(chkF.Checked), txtFormOther.Text, txtMedOther.Text, txtMedicineCough.Text)

        objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "CovidHead", "บันทึกเพิ่มแบบฟอร์มการติดตามผู้ป่วย Home Isolation (COVID-19):<<PatientID:" & Session("patientid") & ">>", "COVID")


        If lblCOVIDUID.Text = "" Then
            lblCOVIDUID.Text = ctlCovid.COVID3_Head_GetLastUID(lblLocationID.Text, Session("patientid")).ToString()
        End If

        pnCOVID.Visible = True
        pnResult.Visible = True


        'If isValid = False Then
        '    Exit Sub
        'End If
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

    End Sub
#Region "Edit Data"
    Private Sub EditCovidDetail(UID As Integer)
        dt = ctlCovid.COVID3_Detail_GetByUID(UID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)

                lblUID.Text = dt.Rows(0)("UID")
                txtSEQ.Text = .Item("SEQNO")
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))

                txtServiceDate.Text = DisplayStr2ShortDateTH(String.Concat(.Item("ServiceDate")))
                txtTime.Text = String.Concat(.Item("ServiceTime"))
                If String.Concat(.Item("Temperature")) = "0" Then
                    txtTemp.Text = ""
                Else
                    txtTemp.Text = String.Concat(.Item("Temperature"))
                End If

                txtO2sat.Text = String.Concat(.Item("O2Sat"))

                optCough.SelectedValue = String.Concat(.Item("Cough"))
                optBreath.SelectedValue = String.Concat(.Item("Breathe"))
                optdiarrhea.SelectedValue = String.Concat(.Item("Diarrhea"))
                optSmell.SelectedValue = String.Concat(.Item("Smelless"))
                optTast.SelectedValue = String.Concat(.Item("Tasteless"))
                txtOtherSymp.Text = String.Concat(.Item("OtherFactor"))
                txtProblem.Text = String.Concat(.Item("Problem"))
                txtRemark.Text = String.Concat(.Item("Recommend"))
                cmdSaveVisit.Text = "บันทึกการติดตามอาการ"
            End With
        End If
    End Sub
#End Region

#Region "Load Data to Grid"
    Private Sub LoadCovidDetailToGrid()
        dt = ctlCovid.COVID3_Detail_Get(StrNull2Zero(lblCOVIDUID.Text), StrNull2Zero(Session("patientid")))
        txtSEQ.Text = dt.Rows.Count + 1

        grdData.DataSource = dt
        grdData.DataBind()
    End Sub
#End Region
#Region "Grid Event"
    Protected Sub grdData_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(1).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)
            'e.Row.Cells(0).Text = e.Row.RowIndex + 1
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#ded5f2';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub
    Private Sub grdData_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditCovidDetail(e.CommandArgument())
                Case "imgDel"
                    ctlCovid.COVID3_Detail_Delete(e.CommandArgument())
                    ClearTextBehavior()
                    LoadCovidDetailToGrid()

            End Select
        End If
    End Sub
#End Region

    Dim strD() As String


    Protected Sub cmdSaveVisit_Click(sender As Object, e As EventArgs) Handles cmdSaveVisit.Click
        lblAlert.Visible = True
        lblAlert.Text = ""
        pnSuccess.Visible = False
        Dim ServiceDate As Integer
        ServiceDate = CInt(ConvertStrDate2DBString(txtServiceDate.Text))


        'If txtTime.Text = "" Then
        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุระยะเวลาให้บริการ');", True)
        '    Exit Sub
        'End If

        'If txtTemp.Text = "" Then
        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุอาการไข้');", True)
        '    Exit Sub
        'End If

        If optCough.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุอาการไอ');", True)
            Exit Sub
        End If

        If optBreath.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุอาการการหายใจ');", True)
            Exit Sub
        End If
        If optdiarrhea.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุอาการท้องเสีย');", True)
            Exit Sub
        End If
        If optSmell.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุอาการจมูกไม่ได้กลิ่น');", True)
            Exit Sub
        End If
        If optTast.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุอาการลิ้นไม่รับรส');", True)
            Exit Sub
        End If

        ctlCovid.COVID3_Detail_Save(StrNull2Zero(lblUID.Text), StrNull2Zero(lblCOVIDUID.Text), txtSEQ.Text, ServiceDate, StrNull2Zero(ddlPerson.SelectedValue), Str2Double(txtTime.Text), Str2Double(txtTemp.Text), txtO2sat.Text, optCough.SelectedValue, optBreath.SelectedValue, optdiarrhea.SelectedValue, optSmell.SelectedValue, optTast.SelectedValue, txtOtherSymp.Text, txtProblem.Text, txtRemark.Text, Request.Cookies("UserID").Value)

        ClearTextBehavior()

        LoadCovidDetailToGrid()

        pnAlert.Visible = False
        pnSuccess.Visible = True
    End Sub
    Private Sub ClearTextBehavior()
        lblUID.Text = "0"
        txtServiceDate.Text = ""
        txtTime.Text = ""
        txtTemp.Text = ""
        txtO2sat.Text = ""
        'optCough.SelectedValue, optBreath.SelectedValue, optdiarrhea.SelectedValue, optSmell.SelectedValue, optTast.SelectedValue,
        txtOtherSymp.Text = ""
        txtProblem.Text = ""
        txtRemark.Text = ""
        cmdSaveVisit.Text = "เพิ่มการติดตามอาการ"
    End Sub


    Protected Sub cmdBehaviorCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        ClearTextBehavior()
    End Sub

    Protected Sub optSmoke_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optSmoke.SelectedIndexChanged

        If optSmoke.SelectedValue = "1" Then
            txtSmokeQTY.Enabled = True
            txtY.Enabled = False
            txtM.Enabled = False
            txtD.Enabled = False
            txtY.Text = ""
            txtM.Text = ""
            txtD.Text = ""
        ElseIf optSmoke.SelectedValue = "2" Then
            txtSmokeQTY.Text = ""
            txtY.Text = ""
            txtM.Text = ""
            txtD.Text = ""
            txtSmokeQTY.Enabled = False
            txtY.Enabled = False
            txtM.Enabled = False
            txtD.Enabled = False
        Else
            txtSmokeQTY.Enabled = False
            txtY.Enabled = True
            txtM.Enabled = True
            txtD.Enabled = True
            txtSmokeQTY.Text = ""
        End If

    End Sub
End Class