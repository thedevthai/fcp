﻿Public Class ReportVMIConditionByProvince
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlType As New ServiceTypeController
    Dim objLct As New LocationInfo
    Dim ctlLct As New LocationController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            LoadProvinceToDDL()

            Dim m, y As Integer
            y = Year(Date.Now)
            m = Month(Date.Now)
            If y < 2300 Then
                y = y + 543
            End If

            txtStartDate.Text = "01/" & m & "/" & y 'Date.Now.AddDays(-30).ToString("dd/MM/yyyy", DateFormat_TH)
            txtEndDate.Text = Date.DaysInMonth(y, m) & "/" & m & "/" & y  'Date.Now.ToString("dd/MM/yyyy", DateFormat_TH)

            Select Case Request("ItemType")
                Case "vmi2"
                    lblReportHeader.Text = "VMI Stock Balance Report"
                    cmdExcel.Visible = True
                    lblStart.Visible = False
                    lblTo.Visible = False
                    txtStartDate.Visible = False
                    txtEndDate.Visible = False

                    lblLocation.Visible = False
                    ddlLocation.Visible = False
                    txtFindLocation.Visible = False
                    lnkFindLocation.Visible = False
                    imgArrowLocation.Visible = False
               
            End Select

            'If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            '    lblLocation.Visible = False
            '    ddlLocation.Visible = False
            '    txtFindLocation.Visible = False
            '    lnkFindLocation.Visible = False
            '    imgArrowLocation.Visible = False
            'End If

        End If

    End Sub
    Private Sub LoadProvinceToDDL()
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            dt = ctlLct.Province_GetInGroup(Request.Cookies("RPTGRP").Value)
        Else
            dt = ctlLct.Province_GetInLocation
        End If

        ddlProvince.Items.Clear()
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .Items.Add("---ทั้งหมด---")
                If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
                    .Items(0).Value = Request.Cookies("RPTGRP").Value
                Else
                    .Items(0).Value = "0"
                End If

                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)("ProvinceName"))
                    .Items(i + 1).Value = dt.Rows(i)("ProvinceID")
                Next
                .SelectedIndex = 0

            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadLocationToDDL()

        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            If ddlProvince.SelectedValue <> Request.Cookies("RPTGRP").Value Then
                dt = ctlLct.Location_GetBySearch(ddlProvince.SelectedValue, txtFindLocation.Text)
            Else
                dt = ctlLct.Location_GetByProvinceGroup(Request.Cookies("RPTGRP").Value, txtFindLocation.Text)
            End If

        Else
            dt = ctlLct.Location_GetBySearch(ddlProvince.SelectedValue, txtFindLocation.Text)
        End If

        ddlLocation.Items.Clear()

        If dt.Rows.Count > 0 Then
            With ddlLocation

                .Items.Add("---ทั้งหมด---")
                .Items(0).Value = "0"
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)(objLct.tblField(objLct.fldPos.f01_LocationName).fldName))
                    .Items(i + 1).Value = dt.Rows(i)(objLct.tblField(objLct.fldPos.f00_LocationID).fldName)
                Next
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Protected Sub cmdExcel_Click(sender As Object, e As EventArgs) Handles cmdExcel.Click

        Dim LID As String = "0"
        Dim rptGRP As String = "ALL"
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            LID = Request.Cookies("LocationID").Value
        ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            rptGRP = Request.Cookies("RPTGRP").Value
            LID = ddlLocation.SelectedValue
        Else
            LID = ddlLocation.SelectedValue
        End If


        Select Case Request("ItemType")
            Case "vmi2"
                FagRPT = "VMI2"
                Reportskey = "XLS"
                ReportsName = "StockBalanceReport"

        End Select


        Dim dStart As String = ConvertStrDate2DBString(txtStartDate.Text)
        Dim dEnd As String = ConvertStrDate2DBString(txtEndDate.Text)
        'Dim pLocationID As String

        'If Request.Cookies("LocationID").Value <> "CPA" Then
        '    pLocationID = Request.Cookies("LocationID").Value
        'Else
        '    pLocationID = ""
        'End If

        Response.Redirect("ReportViewer.aspx?b=" & dStart & "&e=" & dEnd & "&y=" & dStart & "_" & dEnd & "&pv=" & ddlProvince.SelectedValue)



    End Sub

    Protected Sub lnkFindLocation_Click(sender As Object, e As EventArgs) Handles lnkFindLocation.Click
        LoadLocationToDDL()
    End Sub

    Protected Sub ddlProvince_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProvince.SelectedIndexChanged
        LoadLocationToDDL()
    End Sub
End Class