﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteNoAjax.Master" CodeBehind="FormEdit.aspx.vb" Inherits=".FormEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
    <script src="js/jquery-1.10.2.js"></script> 

<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
   <script src="js/jquery-1.9.1.js"></script>
  <script src="js/jquery-ui.js"></script>
  
  <script> $(function() {
       $( "#dialog-message" ).dialog({
      autoOpen: false,
      height: 600,
      width: 600,
      modal: true,
      buttons: {
        Close: function() {
          $( this ).dialog( "close" );
        }
      },
      close: function() {
        
      }
    });
 
    $( "#user-login" )
      .button()
      .click(function() {
        $( "#dialog-message" ).dialog( "open" );
      });
  });
  </script>
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>แก้ไขรายการกิจกรรม
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
  <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลกิจกรรม</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
 
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td align="center" valign="top" height="10"></td>
      </tr>
        <tr>
            <td align="left" valign="top">
             
            
<table border="0" align="center" cellPadding="1" cellSpacing="1">
<tr>
  <td align="left" class="texttopic">ปี :</td>
  <td align="left" class="texttopic">
                                                      <asp:DropDownList ID="ddlYear" runat="server" 
                                                          CssClass="OptionControl">
                                                          <asp:ListItem Selected="True" Value="0">-- ทั้งหมด --</asp:ListItem>
                                                          <asp:ListItem>2557</asp:ListItem>
                                                          <asp:ListItem>2558</asp:ListItem>
                                                          <asp:ListItem>2559</asp:ListItem>
                                                          <asp:ListItem>2560</asp:ListItem>
                                                          <asp:ListItem>2561</asp:ListItem>
                                                          <asp:ListItem>2562</asp:ListItem>
                                                          <asp:ListItem>2563</asp:ListItem>
                                                          <asp:ListItem>2564</asp:ListItem>
                                                           <asp:ListItem>2565</asp:ListItem>
                                                           <asp:ListItem>2566</asp:ListItem>
                                                      </asp:DropDownList>                                                    </td>
</tr>
<tr>
  <td align="left" class="texttopic">กิจกรรม :</td>
  <td align="left" class="texttopic">
                                                      <asp:DropDownList ID="ddlActivity" runat="server" 
                                                          Width="400px" CssClass="OptionControl">                                                      </asp:DropDownList>                                                    </td>
</tr>
<tr>
  <td align="left" class="texttopic"><asp:Label ID="lblProv" runat="server" 
          Text="จังหวัด :"></asp:Label>
    </td>
  <td align="left" class="texttopic">
                                                      <asp:DropDownList ID="ddlProvinceID" runat="server" 
                                                          CssClass="OptionControl">                                                      </asp:DropDownList>                                                    </td>
</tr>

<tr>
  <td align="left" class="texttopic">ค้นหา</td>
  <td align="left" class="texttopic">
                  <asp:TextBox ID="txtSearch" runat="server" Width="150px"></asp:TextBox>
      <span class="text9_nblue">* ค้นหาได้จาก ชื่อ / นามสกุล/เลขบัตร ปชช. ผู้รับบริการ</span></td>
</tr>

<tr>
  <td align="left" class="texttopic">&nbsp;</td>
  <td align="left" class="texttopic">
                  <asp:Button ID="cmdFind" runat="server" text="ค้นหา" CssClass="buttonFind" Width="80px" />              </td>
</tr>

  </table>   
  
     </td>
      </tr>
       <tr>
          <td  align="left" valign="top"  class="MenuSt"><table border="0" cellspacing="2" cellpadding="0">
          <tr>
              <td colspan="4">รายการกิจกรรมที่พบทั้งหมด&nbsp; 
                <asp:Label ID="lblCount" runat="server"></asp:Label>
           &nbsp;รายการ</td>
            </tr> 
         </table>         </td>
      </tr>
       <tr>
          <td align="center" valign="top">
              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" 
                  PageSize="20" >
            <RowStyle BackColor="#F7F7F7" />
            <columns>
                <asp:BoundField HeaderText="Seq No." DataField="itemID" >
                <ItemStyle HorizontalAlign="Center" Width="70px" />                </asp:BoundField>
                <asp:BoundField HeaderText="วันที่บริการ" >
                <ItemStyle HorizontalAlign="Center" Width="90px" />                </asp:BoundField>
                <asp:TemplateField HeaderText="ร้านยา">
                 <ItemTemplate>
                        <asp:HyperLink ID="Hyper1" runat="server" 
                            NavigateUrl='<%# NavigateURL("FormDirect.aspx", "acttype", "view", "ServiceTypeID", DataBinder.Eval(Container.DataItem, "ServiceTypeID"), "ItemId", DataBinder.Eval(Container.DataItem, "ItemID"), "ProjID", DataBinder.Eval(Container.DataItem, "ProjectID"))%>' 
                            Target="_blank" 
                            Text='<%# DataBinder.Eval(Container.DataItem,"LocationName") %>' 
                            CssClass="grd_item"></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle Width="200px" CssClass="grd_item" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ชื่อ-สกุล">
                 <ItemTemplate>
                        <asp:HyperLink ID="Hyper2" runat="server" 
                            NavigateUrl='<%# NavigateURL("FormDirect.aspx", "acttype", "view", "ServiceTypeID", DataBinder.Eval(Container.DataItem, "ServiceTypeID"), "ItemId", DataBinder.Eval(Container.DataItem, "ItemID"), "ProjID", DataBinder.Eval(Container.DataItem, "ProjectID"))%>' 
                            Target="_blank" 
                            Text='<%# DataBinder.Eval(Container.DataItem,"CustName") %>' 
                            CssClass="grd_item"></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle Width="200px" HorizontalAlign="Left" CssClass="grd_item" />
                </asp:TemplateField>
                <asp:BoundField DataField="Ages" HeaderText="อายุ" >
                <ItemStyle HorizontalAlign="Center" Width="30px" />                </asp:BoundField>
            <asp:BoundField HeaderText="เพศ">                      
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />                      </asp:BoundField>
                <asp:BoundField DataField="ServiceTypeID" HeaderText="ฟอร์ม" >
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="50px" />                </asp:BoundField>
                <asp:BoundField DataField="FollowSEQ">
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                </asp:BoundField>
            <asp:TemplateField  HeaderText="" >
              <itemstyle HorizontalAlign="center" VerticalAlign="Middle" />          
            </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgEdit" runat="server" 
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "itemID") & "|" & DataBinder.Eval(Container.DataItem, "ProjectID")%>' 
                                        ImageUrl="images/icon-edit.png" />
                                 
                                     <asp:ImageButton ID="imgDel"  cssclass="gridbutton"  runat="server" 
                                       CommandArgument='<%# DataBinder.Eval(Container.DataItem, "itemID") & "|" & DataBinder.Eval(Container.DataItem, "ProjectID")%>'
                                        ImageUrl="images/icon-delete.png" />                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="45px" />
                </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>
              <asp:Label ID="lblNo" runat="server" CssClass="text_red" 
                  
                  Text="ยังไม่มีรายการกิจกรรม"></asp:Label>           </td>
      </tr>
      
         <tr>
           <td align="center" valign="top">&nbsp;</td>
         </tr>
    </table>
     </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    </section>
</asp:Content>
