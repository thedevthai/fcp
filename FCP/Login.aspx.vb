﻿Public Class Login
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim acc As New UserController
    Dim enc As New CryptographyEngine
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request("logout") = "YES" Then
            Session.Contents.RemoveAll()
        End If

        If Not IsPostBack Then
            lblVersion.Text = "2.0.0"
            If Request("logout") = "YES" Then
                Session.Abandon()
                Request.Cookies("username").Value = Nothing
            End If
            txtUserName.Focus()
        End If
    End Sub
    Private Sub CheckUser()
        dt = acc.User_CheckLogin(txtUsername.Value, enc.EncryptString(txtPassword.Value, True))

        If dt.Rows.Count > 0 Then

            If dt.Rows(0).Item("StatusFlag") <> "A" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ID ของท่านไม่สามารถใช้งานได้ชั่วคราว');", True)
                Exit Sub
            End If

            Request.Cookies("UserID").Value = dt.Rows(0).Item("UserID")
            Request.Cookies("username").Value = dt.Rows(0).Item("USERNAME")
            Request.Cookies("Password").Value = enc.DecryptString(dt.Rows(0).Item("PASSWORDs"), True)
            'Session("UserRole") = dt.Rows(0).Item("USERROLE")
            Session("LastLogin") = String.Concat(dt.Rows(0).Item("LastLogin"))
            Request.Cookies("NameOfUser").Value = String.Concat(dt.Rows(0).Item("Name"))  ' & " " & String.Concat(dt.Rows(0).Item("LastName"))
            'Session("Email") = String.Concat(dt.Rows(0).Item("Email"))
            'Session("UserProfileID") = dt.Rows(0).Item("UserProfileID")
            'Session("ProfileID") = dt.Rows(0).Item("ProfileID")

            'Request.Cookies("LocationID").Value = dt.Rows(0).Item("LocationID")

            Session("ROLE_STD") = False
            Session("ROLE_TCH") = False
            Session("ROLE_ADV") = False
            Session("ROLE_PCT") = False
            Session("ROLE_ADM") = False
            Session("ROLE_SPA") = False

            Session("ROLE_CO-1") = False


            If Session("UserProfileID") = 2 Then
                Session("ROLE_TCH") = True
            End If

            'If Session("UserProfileID") <> 1 Then
            '    Dim ctlC As New CourseController
            '    Session("ROLE_ADV") = ctlC.CoursesCoordinator_CheckStatus(Session("ProfileID"))
            'End If

            'Dim ctlCfg As New SystemConfigController()
            'Session("EDUYEAR") = ctlCfg.SystemConfig_GetByCode(CFG_EDUYEAR)
            'Session("ASSMYEAR") = ctlCfg.SystemConfig_GetByCode(CFG_ASSMYEAR)

            'genLastLog()

            'acc.User_GenLogfile(txtUsername.Value, ACTTYPE_LOG, "Users", "", "")

            Dim rd As New UserRoleController

            dt = rd.UserRole_GetActiveRoleByUID(Request.Cookies("UserID").Value)
            If dt.Rows.Count > 0 Then

                For i = 0 To dt.Rows.Count - 1

                    Select Case dt.Rows(i)("RoleID")
                        Case 1 'Student
                            Session("ROLE_STD") = True
                        Case 2  'Faculty member
                            Session("ROLE_TCH") = True
                            Session("ROLE_ADV") = True
                        Case 3 'Preceptor
                            Session("ROLE_PCT") = True
                        Case 4 'Admin แหล่งฝึกร้านยา
                            Session("ROLE_CO-1") = True
                        Case 5 'Administrator
                            Session("ROLE_ADM") = True
                        Case 9
                            Session("ROLE_SPA") = True
                    End Select





                Next

            End If




            'Select Case Session("UserProfileID")
            '    Case 1 'students
            '        Response.Redirect("Default.aspx")
            '    Case 2, 3 'Teacher
            '        Response.Redirect("Default.aspx")
            '    Case 4 'Admin
            Response.Redirect("Home")
            'End Select

        Else
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','Username  หรือ Password ไม่ถูกต้อง');", True)
            Exit Sub
        End If
    End Sub

    Protected Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        txtUsername.Value = "admin"
        txtPassword.Value = "112233"

        If txtUsername.Value = "" Or txtPassword.Value = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'Warning!','กรุณาป้อน Username  หรือ Password ให้ครบถ้วนก่อน');", True)
            txtUsername.Focus()
            Exit Sub
        End If

        Session.Contents.RemoveAll()
        CheckUser()
    End Sub

    Private Sub genLastLog() ' เก็บวันเวลาที่ User เข้าใช้ระบบครั้งล่าสุด
        acc.User_LastLog_Update(txtUsername.Value)
    End Sub
End Class