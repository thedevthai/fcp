﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class Homes
    
    '''<summary>
    '''lblMember1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMember1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblMember2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMember2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblMember3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMember3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''grdVisitor control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdVisitor As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''lblUserOnlineCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblUserOnlineCount As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblOnline control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblOnline As Global.System.Web.UI.WebControls.Label
End Class
