﻿Public Class ucHistoryYear
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim isNowButton As Boolean = False

        If Not IsPostBack Then
            Dim ctlOrder As New OrderController
            Dim dtY As New DataTable
            dtY.Columns.Add("HYear")
            Dim dr As DataRow = dtY.NewRow()
            dr(0) = ""
            dtY.Rows.Add(dr)


            If dtY.Rows.Count > 0 Then
                With grdHistory
                    .Visible = True
                    .DataSource = dtY
                    .DataBind()

                    Dim dts As New DataTable
                    Dim str() As String
                    Dim FORMPATH As String
                    str = Split(HttpContext.Current.Request.Url.AbsolutePath, ".")
                    FORMPATH = Right(str(0), Len(str(0)) - 1)
                    Dim FYear As New Integer

                    FYear = ctlOrder.Service_GetBYear(FORMPATH, Request("fid"))

                    If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
                        If Request("yid") = Year(ctlOrder.GET_DATE_SERVER) Then
                            .Rows(0).Cells(0).Text &= "<a Class='buttonFind' href='" & FORMPATH & ".aspx?yid=" & Year(ctlOrder.GET_DATE_SERVER) & "'>" & Year(ctlOrder.GET_DATE_SERVER) & "</a>&nbsp;"

                        Else

                            If FYear = StrNull2Zero(Right(Year(ctlOrder.GET_DATE_SERVER), 2)) Then
                                .Rows(0).Cells(0).Text &= "<a Class='buttonFind' href='" & FORMPATH & ".aspx?yid=" & Year(ctlOrder.GET_DATE_SERVER) & "'>" & Year(ctlOrder.GET_DATE_SERVER) & "</a>&nbsp;"
                            Else
                                .Rows(0).Cells(0).Text &= "<a Class='buttonSave' href='" & FORMPATH & ".aspx?yid=" & Year(ctlOrder.GET_DATE_SERVER) & "'>" & Year(ctlOrder.GET_DATE_SERVER) & "</a>&nbsp;"
                            End If


                        End If

                        dts = ctlOrder.Service_GetPatinetProfileNotYearNow(FORMPATH, Session("patientid"))

                    Else
                        dts = ctlOrder.Service_GetPatinetProfile(FORMPATH, Session("patientid"))
                    End If

                    If dts.Rows.Count > 0 Then
                        For n = 0 To dts.Rows.Count - 1
                            If (dts.Rows(n)("LocationID") <> Request.Cookies("LocationID").Value) Then
                                If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
                                    .Rows(0).Cells(0).Text &= "<span Class='buttonDisable'>25" & dts.Rows(n)("nYear") & "</span>&nbsp;"
                                Else

                                    If FYear = DBNull2Zero(dts.Rows(n)("nYear")) Then
                                        .Rows(0).Cells(0).Text &= "<a Class='buttonFind' href='" & FORMPATH & ".aspx?yid=25" & dts.Rows(n)("nYear") & "'>25" & dts.Rows(n)("nYear") & "</a>&nbsp;"
                                    Else
                                        .Rows(0).Cells(0).Text &= "<a Class='buttonSave' href='" & FORMPATH & ".aspx?yid=25" & dts.Rows(n)("nYear") & "'>25" & dts.Rows(n)("nYear") & "</a>&nbsp;"
                                    End If

                                End If

                            Else
                                If Request("yid") = ("25" & dts.Rows(n)("nYear")) Then
                                    .Rows(0).Cells(0).Text &= "<a Class='buttonFind' href='" & FORMPATH & ".aspx?yid=25" & dts.Rows(n)("nYear") & "'>25" & dts.Rows(n)("nYear") & "</a>&nbsp;"
                                Else
                                    If FYear = DBNull2Zero(dts.Rows(n)("nYear")) Then
                                        .Rows(0).Cells(0).Text &= "<a Class='buttonFind' href='" & FORMPATH & ".aspx?yid=25" & dts.Rows(n)("nYear") & "'>25" & dts.Rows(n)("nYear") & "</a>&nbsp;"
                                    Else
                                        .Rows(0).Cells(0).Text &= "<a Class='buttonSave' href='" & FORMPATH & ".aspx?yid=25" & dts.Rows(n)("nYear") & "'>25" & dts.Rows(n)("nYear") & "</a>&nbsp;"
                                    End If

                                End If

                            End If

                        Next
                    Else
                        '.Rows(0).Cells(0).Text &= "<a Class='buttonFind' href='" & FORMPATH & ".aspx?yid=" & Year(ctlOrder.GET_DATE_SERVER) & "'>" & Year(ctlOrder.GET_DATE_SERVER) & "</a>&nbsp;"
                    End If
                    dts = Nothing
                End With

            Else
                grdHistory.Visible = False
                grdHistory.DataSource = Nothing
            End If
            dtY = Nothing
        End If

    End Sub

End Class