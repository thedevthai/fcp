﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="rptLocations.aspx.vb" Inherits=".rptLocations" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
 <style type="text/css">
         Body{
	background: #fff;
	margin: 0 0 0 0;
	padding: 0 0 0 0;
	font-family: Geneva, Arial, Helvetica,Tahama, sans-serif;	
	color: #000;
	font-size: 13px;
	line-height: 25px;
}

.Page_Header{
font-size: 16px;
	font-style: normal;
	text-align: center;
	font-weight: bold;
  	color: #000;  
 	padding: 5px 0px 5px 0px;
	text-shadow: 1px 1px 1px rgba(255,255,255,0.8);
	
}
.texttopic{
	 	font-size: 13px;
	color:#555;
	font-weight: bold;
}
.TopicArticle
{
	color: #000;
	font-size: 14px;
	text-transform: uppercase;
	padding: 5px;
}
    

.th{
background-image:url('images/th.gif');
text-indent:5px;
height:20px;
border-bottom: 1px solid #d0d0d0;
border-top: 1px solid #d0d0d0;
/*color: #74a923;*/
color: #666;
font-size: 13px;
} 

.dc_paginationC {padding-bottom:1px}

.dc_pagination {;margin:0px;margin-left:5px;padding:0px}

.dc_pagination{font:12px 'Tahoma';height:100%;list-style-type:none;margin:4px 0;overflow:hidden;padding:0px}
.dc_pagination :first-child{margin-left:0px}
a:link
{
font-family: Geneva, Arial, Helvetica,Tahama, sans-serif;
text-decoration: none;
  color: #3B3B3B;
}


 a, 
 a:link,
 a:visited  {
	
	font-family: Geneva, Arial, Helvetica, sans-serif;	
	color: #246eac;
}

 .dc_paginationC a{background:#FFFFFF;
border:solid 1px #DCDCDC;border-radius:3px;
color:#707070 !important;moz-border-radius:3px;
padding:6px 9px 6px 9px;webkit-border-radius:3px}
.dc_pagination a{color:#000000 !important;display:block;padding:7px 10px 7px 10px;text-decoration:none}

a
{
font-family: Tahoma, "MS Sans Serif", Arial;
text-decoration: none;
  color: #3B3B3B;
}


a {
	text-decoration: none;
}


    </style>
</head>
<body>
    <form id="form1" runat="server">
  
         
  <script language="Javascript">
      function doprint() {
          //save existing user's info
          //  var h = factory.printing.header;
          //  var f = factory.printing.footer;
          //hide the button
          document.all("cmdPrint").style.visibility = 'hidden';

          window.print();
          ////  factory.printing.SetMarginMeasure(2); 
          //  factory.printing.portrait = true;
          //  factory.printing.leftMargin = 1.75;
          //  factory.printing.topMargin = 1.75;
          //  factory.printing.rightMargin = 0.75;
          //  factory.printing.bottomMargin = 1.75;


          ////set header and footer to blank
          //  factory.printing.header = "";
          //  factory.printing.footer = "";
          //  //print page without prompt
          //  factory.DoPrint(false);
          //  //restore user's info
          //  factory.printing.header = h;
          //  factory.printing.footer = f;
          //show the print button
          // document.all("prnButton").style.visibility = 'visible';
          //  document.all("AButton").style.visibility = 'visible';
          //  document.all("BButton").style.visibility = 'visible';
          document.all("cmdPrint").style.visibility = 'visible';

      }
   
    </script>
 <table width="800" border="0" align="center" cellpadding="0" cellspacing="0" >
  <tr>
 <td align="center" class="Page_Header"> รายงานรายชื่อร้านยาที่เข้าร่วมโครงการ</td>
    </tr>
    <tr>
 <td align="center" class="Page_Header"> ประเภท&nbsp;
     <asp:Label  ID="lblTypeName" runat="server"></asp:Label>        &nbsp; &nbsp; จังหวัด 
     <asp:Label ID="lblProvinceName" runat="server"></asp:Label>
        </td>
    </tr>
     <tr><td valign="top" class="texttopic"><table align="left" border="0" cellspacing="2" cellpadding="0">
       <tr>
         <td >ทั้งหมด:&nbsp;&nbsp; </td> 
         <td>
             <asp:Label ID="lblCount" runat="server"></asp:Label>           </td>
         <td>&nbsp;&nbsp; ร้าน</td>        
        
       </tr>
       
     </table> 
       </td>
       </tr>
     <tr><td valign="top" class="texttopic">F : โครงการกิจกรรมการให้ความรู้ คำแนะนำและคำปรึกษา
         <br />
         A : โครงการสนับสนุนการทำกิจกรรมปรับเปลี่ยนพฤติกรรมผู้เสพยาสูบโดยเภสัชกรชุมชน</td>
       </tr>
    <tr>
    <td valign="top">
              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="2" ForeColor="Black" 
                      AutoGenerateColumns="False" Width="100%" BorderColor="#333333">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" VerticalAlign="Top" />
            <columns>
            <asp:BoundField HeaderText="No.">
              <itemstyle HorizontalAlign="Center" />                      </asp:BoundField>
                <asp:BoundField DataField="ProjectName" HeaderText="โครงการ">
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
            <asp:BoundField DataField="LocationID" HeaderText="รหัส">
              <headerstyle HorizontalAlign="Center" />          
              <itemstyle HorizontalAlign="Center" Width="90px" />                      </asp:BoundField>
            <asp:BoundField DataField="LocationName" HeaderText="ร้านยา">
              <headerstyle HorizontalAlign="Left" />          
              <itemstyle HorizontalAlign="Left" />                      </asp:BoundField>
                <asp:BoundField DataField="ProvinceName" HeaderText="จังหวัด">
                <ItemStyle HorizontalAlign="Left" />                </asp:BoundField>
                <asp:BoundField DataField="LocationGroupName" HeaderText="ประเภทร้านยา" >
                <ItemStyle Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="Address" HeaderText="ที่อยู่">
                <ItemStyle HorizontalAlign="Left" Width="150px" />
                </asp:BoundField>
                <asp:BoundField DataField="Co_Name" HeaderText="ผู้ประสานงาน">
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Office_Tel" HeaderText="เบอร์โทร">
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>
                                 </td>
                            
                                   </tr>
                            
                               <td align="center" valign="top"><input  type="button" value="Print" id="cmdPrint"  onclick="doprint();" /></td>                        
    </tr>
  </table>  
    
    </td>
  </tr>
</table>
    </form>
</body>
</html>
