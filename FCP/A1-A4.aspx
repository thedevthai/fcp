﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="A1-A4.aspx.vb" Inherits=".A01" %>
<%@ Register src="ucHistoryYear.ascx" tagname="ucHistoryYear" tagprefix="uc1" %>
<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/pagestyles.css">
<link rel="stylesheet" type="text/css" href="css/cpastyles.css">
<link rel="stylesheet" type="text/css" href="css/uidialog.css">
<link rel="stylesheet" type="text/css" href="css/dc_table.css">  

    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>A1-A4
        <small>โครงการสนับสนุนการทำกิจกรรมปรับเปลี่ยนพฤติกรรมผู้เสพยาสูบโดยเภสัชกรชุมชน</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
   <div align="center">
    <uc1:ucHistoryYear ID="ucHistoryYear1" runat="server" />
  </div> <br />
    <div class="pull-right">
          <table  border="0" cellspacing="2" cellpadding="0">
        <tr>
        
          <td class="NameEN">Item ID : </td>
          <td class="NameEN">
              <asp:Label ID="lblID" 
                  runat="server"></asp:Label>            </td>
         
        </tr>
      </table>
        </div> 

 <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">ข้อมูลร้านยาที่ให้บริการ</h3>
          <div class="box-tools pull-right">
              

            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
 <table width="100%" border="0" align="left" cellpadding="0" cellspacing="2" class="OptionPanels">
      <tr>
        <td width="50">รหัสร้าน</td>
        <td width="120" class="LocationName">
            <asp:Label ID="lblLocationID" runat="server"></asp:Label>          </td>
        <td width="50">ชื่อร้าน</td>
        <td width="300" class="LocationName">
            <asp:Label ID="lblLocationName" runat="server"></asp:Label>          </td>
           <td width="100">รหัสหน่วยบริการ</td>
           <td  class="LocationName">
               <asp:Label ID="lblLocationCode" runat="server"></asp:Label>
          </td>
        <td width="50">จังหวัด</td>
        <td class="LocationName">
            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>          </td>
      </tr>
    </table>

             <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">
      <tr>      
         
        <td>วันที่ให้บริการ</td>
        <td align="left">
             <asp:TextBox ID="txtServiceDate" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox></td>
          <td align="left"> 
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="txtServiceDate" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="(\d{1}|\d{2})/(\d{1}|\d{2})/\d{4}"></asp:RegularExpressionValidator></td>
        <td width="100">รวมระยะเวลา<img src="images/star.png" width="10" height="10" />
                   
                </td> 
          <td>ปี</td>
          <td>
              <asp:TextBox ID="txtYear" runat="server" Width="50px"></asp:TextBox>
          </td>
          <td>ระยะเวลาที่ให้บริการ</td>
        <td>
            <asp:TextBox ID="txtTime" runat="server" Width="50px"></asp:TextBox>
            &nbsp;</td>
          <td>นาที</td>
          <td>เภสัชกรผู้ให้บริการ</td>
        <td>
             <asp:DropDownList  CssClass="form-control select2" ID="ddlPerson" runat="server">            </asp:DropDownList>          </td>
      </tr>
    </table>
            
        
      </div>
      
</div>

  
<div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">แหล่งที่มา/วิธีการให้บริการ</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">         
 
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">
    
      <tr>
         <td>ผู้รับบริการ
             &nbsp;</td>
        <td align="left">

            <asp:RadioButtonList ID="optPatientType" runat="server" RepeatDirection="Horizontal">
                  <asp:ListItem Value="รายเก่า">รายเก่า</asp:ListItem>
                <asp:ListItem  Value="รายใหม่" Selected="True">รายใหม่</asp:ListItem>
            </asp:RadioButtonList>
          </td>
      </tr>
    
      <tr>
         <td>
            แหล่งที่มา</td>
        <td align="left">

            <table>
                <tr>
                    <td>     
            <asp:RadioButtonList ID="optPatientFrom" runat="server" RepeatDirection="Horizontal">
                  <asp:ListItem Value="W" Selected="True">ประชาชนที่มาที่ร้าน</asp:ListItem>
                <asp:ListItem  Value="R">มาจากหน่วยบริการ</asp:ListItem>
                  <asp:ListItem Value="H">มาจากการเยี่ยมบ้าน</asp:ListItem>
                  <asp:ListItem Value="E">มาจากการจัดกิจกรรม/ event ต่างๆ</asp:ListItem>
                  <asp:ListItem Value="T">มาจากโครงการ Telepharmacy</asp:ListItem>
                  <asp:ListItem Value="9">อื่นๆ</asp:ListItem>
            </asp:RadioButtonList>   </td>
                       <td>ระบุ</td>
                       <td>
                           <asp:TextBox ID="txtFromRemark" runat="server" Width="300px" placeholder="ระบุหน่วยบริการ หรือ กรณีอื่นๆ"></asp:TextBox></td>
                </tr>
            </table>
               

          </td>
      </tr>
      <tr>
         <td valign="top">การให้บริการ</td>
        <td align="left">
            <table>
                <tr>
                    <td>
            <asp:RadioButtonList ID="optService" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Value="1">Face to face</asp:ListItem>
                <asp:ListItem Value="2">Telepharmacy</asp:ListItem>
                <asp:ListItem Value="9">อื่นๆ</asp:ListItem>
            </asp:RadioButtonList>
                    </td>
                    <td>
             <asp:TextBox ID="txtServiceRemark" runat="server" Width="300px" placeholder="กรณีอื่นๆ ระบุ"></asp:TextBox>
                    </td>
                </tr>
            </table>
          </td>
      </tr>
      <tr>
         <td valign="top">กรณี Telepharmacy</td>
        <td align="left">
             <table>
                <tr>
                    <td>  <asp:RadioButtonList ID="optTelepharm" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Value="1">ผป.ของร้านเอง</asp:ListItem>
                <asp:ListItem Value="2">ผป.โครงการลดแอัด (ระบุ รพ.แม่ข่าย)</asp:ListItem>
                <asp:ListItem Value="3">ผป.รับยาทาง ปณ.จาก รพ. (ระบุ รพ.)</asp:ListItem>
                <asp:ListItem Value="9">ผป.ในโครงการอื่นๆ (ระบุ)</asp:ListItem>
            </asp:RadioButtonList></td>
                       <td width="60" class="text-right">ระบุ</td>
                       <td>
                           <asp:TextBox ID="txtTelepharmacyRemark" runat="server" Width="300px"></asp:TextBox></td>
                </tr>
            </table>

          
          </td>
      </tr>
      <tr>
         <td valign="top">วิธีการสื่อสาร Telepharmacy</td>
        <td align="left">

             <table>
                <tr>
                    <td>  <asp:RadioButtonList ID="optTelepharmacyMethod" runat="server">
                  <asp:ListItem Value="1" >โทรศัพท์ หรือ  line call  <u><b>ไม่มีการบันทึกทั้งภาพและเสียง</b></u>        </asp:ListItem>
                <asp:ListItem  Value="2">โทรศัพท์  หรือ  Line call <u><b>บันทึกแต่เสียง</b></u></asp:ListItem> 
                  <asp:ListItem Value="3">มี soft ware หรือ program หรือ  Line Call  หรือ อื่นๆ  และ<u><b>มีการบันทึก ภาพและเสียง</b></u></asp:ListItem>
            </asp:RadioButtonList></td>
                       <td> <table class="nav-justified">
                  <tr>
                      <td>ระบุ วิธีการบันทึก</td>
                      <td><asp:TextBox ID="txtRecordMethod" runat="server" Width="300px"></asp:TextBox></td>
                  </tr>
                  <tr>
                      <td>ระบุ  สถานที่เก็บข้อมูล</td>
                      <td> <asp:TextBox ID="txtRecordLocation" runat="server" Width="300px"></asp:TextBox></td>
                  </tr>
              </table></td>

                </tr>
            </table>
            
          
           </td>
          <td valign="top">
             
          </td>
      </tr>
    </table>      

</div>   
</div>  

    <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลกิจกรรม</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">


 <table width="100%" border="0"  class="table table-bordered">
  
  
    <tr>
    <td align="center" class="MenuSt">ข้อมูลทั่วไป</td>
  </tr>
   <tr>
    <td><table border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td align="left">น้ำหนัก</td>
        <td align="left">
            <table>
                <tr>
                    <td>
            <asp:TextBox ID="txtWeight" runat="server" Width="50px">0</asp:TextBox>
                    </td>
                    <td>กก. ส่วนสูง</td>
                    <td>
            <asp:TextBox ID="txtHeigh" runat="server" AutoPostBack="True" Width="50px">0</asp:TextBox>
                    </td>
                    <td>
                        ซม.</td>
                    <td>
                        ความดันโลหิต </td>
                    <td>
            <asp:TextBox ID="txtBP" runat="server" Width="60px"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
          </td>
      </tr>
      <tr>
        <td align="left">อาชีพ</td>
        <td align="left">
            &nbsp;<asp:TextBox ID="txtCareer" runat="server" Width="300px"></asp:TextBox>
          </td>
      </tr>
      </table></td>
  </tr>
   <tr>
    <td align="center" class="MenuSt">โรคประจำตัว/ภาวะผู้สูบ</td>
  </tr>
   <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0">
     
      <tr>
        <td>
            <table cellpadding="0">
                <tr>
                    <td align="left"><asp:CheckBox ID="chkDisease1" runat="server" Text="โรคเบาหวาน"/>
                    </td> 
                    <td align="left"><asp:CheckBox ID="chkDisease2" runat="server" Text="โรคความดันโลหิตสูง" />
                    </td>
                    <td align="left"><asp:CheckBox ID="chkDisease3" runat="server" Text="โรคไขมันในเลือดสูง" />
                    </td>
                </tr>
                <tr>
                    <td align="left"><asp:CheckBox ID="chkDisease4" runat="server" Text="โรคกล้ามเนื้อหัวใจขาดเลือด" />
                    </td> 
                    <td align="left"><asp:CheckBox ID="chkDisease5" runat="server" Text="โรคหลอดเลือดสมองอุดตัน" /></td>
                    <td align="left"><asp:CheckBox ID="chkDisease6" runat="server" Text="โรคซึมเศร้า" /></td>
                </tr>
                <tr>
                    <td align="left"><asp:CheckBox ID="chkDisease7" runat="server" Text="โรคปอดอุดกั้นเรื้องรัง" />
                    </td> 
                    <td align="left"><asp:CheckBox ID="chkDisease8" runat="server" Text="โรคหืด" /></td>
                    <td align="left"><asp:CheckBox ID="chkDisease9" runat="server" Text="โรคภูมิแพ้" /></td>
                </tr>
                <tr>
                  <td align="left"><asp:CheckBox ID="chkDisease10" runat="server" Text="ตั้งครรภ์/ให้นมบุตร" /></td>
                  <td colspan="2" align="left" valign="middle"><asp:CheckBox ID="chkDiseaseOther" runat="server" Text="อื่นๆ" AutoPostBack="True" /> 
                    &nbsp;<asp:TextBox ID="txtDiseaseOther" runat="server" AutoPostBack="True" Visible="False" Width="160px"></asp:TextBox>
                  </td>
                </tr>
                <tr>
                  <td align="left"><asp:CheckBox ID="chkDisease11" runat="server" Text="ไม่มีโรคประจำตัว" /></td>
                  <td align="left"><asp:CheckBox ID="chkDisease12" runat="server" Text="ไม่ทราบ" /></td>
                  <td align="left">&nbsp;</td>
                </tr>
            </table>
          </td>
      </tr>
    </table></td>
  </tr>
   <tr>
    <td align="center" class="MenuSt">พฤติกรรมสุขภาพ</td>
  </tr>
   <tr>
    <td><table border="0" cellpadding="0" cellspacing="2">
      <tr>
        <td>ยาที่ใช้ประจำตัว</td>
        <td align="left">
            <asp:TextBox ID="txtDrugsUsed" runat="server" AutoPostBack="True" Width="200px"></asp:TextBox>
                </td>
      </tr>
      <tr>
        <td>การดื่มเครื่องดื่มที่มีแอลกอฮอล์</td>
        <td align="left"><table border="0" cellspacing="2" cellpadding="0">
              <tr>
                <td align="left">
                    <asp:RadioButtonList ID="optAlcohol" runat="server" 
                RepeatDirection="Horizontal">
                  <asp:ListItem Value="1">ดื่มทุกวัน</asp:ListItem>
                     <asp:ListItem Value="2">ดื่มเป็นครั้งคราว</asp:ListItem>
                  <asp:ListItem Value="3">เคยดื่มแต่เลิกแล้ว</asp:ListItem>
                  <asp:ListItem Value="4">ไม่ดื่ม</asp:ListItem>
                        <asp:ListItem Selected="True" Value="0">ไม่มีข้อมูล</asp:ListItem>
                </asp:RadioButtonList></td>
                <td align="left">&nbsp;</td>
              </tr>
            </table>
            </td>
      </tr>
      <tr>
        <td valign="top">สูบบุหรี่</td>
        <td><table border="0" cellspacing="2" cellpadding="0">
              <tr>
                <td align="left" valign="middle">สูบมานาน เป็นเวลา</td>
                <td colspan="4"  align="left">
                    <table>
                        <tr>
                            <td><asp:TextBox ID="txtSmokeDay" runat="server" Width="40px"></asp:TextBox>
                            </td>
                            <td>
                    <asp:RadioButtonList ID="optSmokeUOM" runat="server" 
                RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="M">เดือน</asp:ListItem>
                  <asp:ListItem Value="Y" Selected="True">ปี</asp:ListItem>
                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                  </td>
                </tr>
              <tr>
                <td align="left" colspan="2">
                    <asp:RadioButtonList ID="optSmokeFQ" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Selected="True" Value="1">ปัจจุบันสูบทุกวัน</asp:ListItem>
                        <asp:ListItem Value="2">ปัจจุบันสูบเป็นครั้งคราว</asp:ListItem>
                        <asp:ListItem Value="3">ปัจจุบันหยุดสูบ โดยหยุดมาแล้ว </asp:ListItem>
                    </asp:RadioButtonList>
                  </td>
                <td>
                    <asp:Label ID="lblCGFQ" runat="server" Text="วันละประมาณ"></asp:Label>
                  </td>
                <td><asp:TextBox ID="txtSmokeQTY" runat="server" Width="40px"></asp:TextBox>
                  </td>
                <td><asp:Label ID="lblCGday" runat="server" Text="มวน"></asp:Label>
                    <asp:RadioButtonList ID="optStopUOM" runat="server" 
                RepeatDirection="Horizontal">
                        <asp:ListItem Value="D">วัน</asp:ListItem>
                  <asp:ListItem Value="M">เดือน</asp:ListItem>
                  <asp:ListItem Value="Y" Selected="True">ปี</asp:ListItem>
                </asp:RadioButtonList></td>
              </tr>
              <tr>
                <td align="left"><asp:Label ID="lblCgType" runat="server" Text="ประเภทของบุหรี่ที่สูบ"></asp:Label></td>
                <td><asp:RadioButtonList ID="optCigarette" runat="server" 
                RepeatDirection="Horizontal">
                  <asp:ListItem Value="1">มวนเอง</asp:ListItem>
                  <asp:ListItem Selected="True" Value="2">บุหรี่ซอง</asp:ListItem>
                  <asp:ListItem Value="3">บุหรี่ไฟฟ้า</asp:ListItem>
                  <asp:ListItem Value="9">อื่นๆ</asp:ListItem>
                </asp:RadioButtonList></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table>          </td>
      </tr>
     
      <tr>
        <td>บุคคลใกล้ชิดที่สูบบุหรี่</td>
        <td align="left"><table border="0" cellspacing="2" cellpadding="0">
          <tr>
                <td><asp:RadioButtonList ID="optIntimate" runat="server" 
                RepeatDirection="Horizontal">
                  <asp:ListItem Value="0" Selected="True">ไม่มี</asp:ListItem>
                  <asp:ListItem Value="1">มี  (ระบุ)</asp:ListItem>
                </asp:RadioButtonList></td>
                <td>
            <asp:TextBox ID="txtCloseupPerson" runat="server" AutoPostBack="True" Width="200px"></asp:TextBox>
                </td>
                </tr>
          </table></td>
      </tr>
      </table></td> 
  </tr>
  <tr>
    <td align="center" class="MenuSt">แบบคัดกรอง</td>
  </tr>
  <tr>
    <td>
    <table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td  valign="top">
          <table width="100%" align="center" class="dc_table_s3">
            <thead>
              </thead>
            <tfoot>
              </tfoot>
            <tbody>
              <tr class="MenuSt">
                <th align="left" scope="row">1.	ในอดีตเคยพยายามเลิกสูบบุหรี่หรือไม่</th>
                </tr>
              <tr >
                <td align="left" scope="row">
                    <table>
                        <tr>
                            <td>
                    <asp:RadioButtonList ID="optQ1A" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="N">ไม่เคย</asp:ListItem>
                        <asp:ListItem Value="Y">เคยเลิก</asp:ListItem>
                    </asp:RadioButtonList>
                            </td>
                            <td>
                                <asp:TextBox ID="txtQ1B" runat="server" Width="50px"></asp:TextBox>
                            </td>
                            <td>ครั้ง เลิกได้นานสุด</td>
                            <td>
                                <asp:TextBox ID="txtQ1C" runat="server" Width="50px"></asp:TextBox>
                            </td>
                            <td>
                    <asp:RadioButtonList ID="optQ1D" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="D">วัน</asp:ListItem>
                        <asp:ListItem Value="M">เดือน</asp:ListItem>
                        <asp:ListItem Value="Y">ปี</asp:ListItem>
                    </asp:RadioButtonList>
                    </td>
                        </tr>
                    </table>
                  </td>
                </tr>
              <tr class="MenuSt">
                <th align="left" scope="row">2.	เคยเลิกโดยวิธี (ตอบได้หลายข้อ)</th>
                </tr>
              <tr>
                <td align="left" scope="row" valign="top"><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td valign="top">
                        <asp:CheckBoxList ID="chkQ2" runat="server" Width="100px">
                            <asp:ListItem>หักดิบ</asp:ListItem>
                            <asp:ListItem>ค่อยๆลด</asp:ListItem>
                            <asp:ListItem>ใช้ยา</asp:ListItem>
                        </asp:CheckBoxList>
                      </td>
                    <td valign="top">
                        <table border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td colspan="2">
                                &nbsp;</td>
                          </tr>
                          <tr>
                            <td colspan="2">&nbsp;</td>
                          </tr>
                          <tr>
                            <td><asp:CheckBoxList ID="chkQ2Med" runat="server" RepeatDirection="Horizontal">
                              <asp:ListItem>Nicotine gum</asp:ListItem>
                              <asp:ListItem>Nicotine patch</asp:ListItem>
                              <asp:ListItem>nortriptyline</asp:ListItem>
                                 <asp:ListItem Value="Bupropion">Bupropion</asp:ListItem>
                              <asp:ListItem>Varenicline</asp:ListItem>
                                <asp:ListItem>สมุนไพรชงหญ้าดอกขาว</asp:ListItem>
                                <asp:ListItem Value="อื่นๆ">อื่นๆ</asp:ListItem>
                            </asp:CheckBoxList></td>
                            <td><asp:TextBox ID="txtQ2Other" runat="server"></asp:TextBox></td>
                          </tr>
                          </table></td>
                  </tr>
                </table></td>
                </tr>
              <tr class="MenuSt">
                <th align="left" scope="row">3.	ปัญหาและอุปสรรคในการเลิกครั้งที่ผ่านมา (เหตุผลที่ทำให้กลับมาสูบ)</th>
                </tr>
                 <tr>
                <td align="left" scope="row" valign="top"><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>
                        <asp:CheckBoxList ID="chkQ3" runat="server">
                            <asp:ListItem Value="1">อาการหงุดหงิดจากการขาดนิโคติน</asp:ListItem>
                            <asp:ListItem Value="2">ถูกกระตุ้นโดยผู้คนรอบข้างหรือสภาพแวดล้อมเดิมๆ </asp:ListItem>
                            <asp:ListItem Value="3">ภาวะเครียด</asp:ListItem>
                            <asp:ListItem Value="4">ยังมีความอยากบุหรี่</asp:ListItem>
                            <asp:ListItem Value="5">อื่นๆ</asp:ListItem>
                            <asp:ListItem Value="0">ไม่มีข้อมูล</asp:ListItem>
                        </asp:CheckBoxList>
                      </td>
                    <td valign="top">
                        <table>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                    </td>
                            </tr>
                            <tr>
                                <td>
                        <asp:TextBox ID="txtQ3" runat="server" Width="250px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                      </td>
                  </tr>
                </table></td>
                </tr>
                 <tr class="MenuSt">
                <th align="left" scope="row">4.	ครั้งนี้มีความตั้งใจเลิกบุหรี่ที่ระดับ</th>
                </tr>
                 <tr>
                <td align="left" scope="row" valign="top">
                    <asp:RadioButtonList ID="optQ4" runat="server">
                        <asp:ListItem Value="1">ยังไม่สนใจที่จะเลิก (Pre-contemplation)</asp:ListItem>
                        <asp:ListItem Value="2">คิดจะเลิกบุหรี่/ยาสูบ แต่ยังไม่มีแผนชัดเจน (Contemplation)</asp:ListItem>
                        <asp:ListItem Value="3">กำลังวางแผนเลิกบุหรี่/ยาสูบภายใน 1 เดือนหรือกำลังเริ่มเข้าสู่กระบวนการเลิกบุหรี่(Preparation)</asp:ListItem>
                        <asp:ListItem Value="4">กำลังเลิกบุหรี่/ยาสูบมาได้ไม่เกิน 6 เดือน (Action)</asp:ListItem>    
                        <asp:ListItem Value="5">เลิกบุหรี่/ยาสูบมาได้เกิน 6 เดือนแต่ไม่เกิน 1 ปี (Maintenance)</asp:ListItem>
                        <asp:ListItem Selected="True" Value="0">ไม่มีข้อมูล</asp:ListItem>
                    </asp:RadioButtonList>
                     </td>
                </tr>
                 <tr class="MenuSt">
                <th align="left" scope="row">5.	คุณสูบบุหรี่มวนแรกหลังตื่นนอนตอนเช้าเมื่อใด</th>
                </tr>
                 <tr>
                <td align="left" scope="row" valign="top">
                    <asp:RadioButtonList ID="optQ5" runat="server" AutoPostBack="True">
                       <asp:ListItem Selected="True" Value="3">สูบทันทีหลังตื่นนอน หรือภายในเวลาไม่เกิน 5 นาที (3 คะแนน)</asp:ListItem>
                       <asp:ListItem Value="2">สูบหลังตื่นนอนเกิน 5 นาที แต่ไม่เกินครึ่งชั่วโมง (2 คะแนน)</asp:ListItem>
                       <asp:ListItem Value="1">สูบหลังตื่นนอนเกินครึ่งชั่วโมงแต่ไม่เกิน 1 ชั่วโมง (1 คะแนน)</asp:ListItem>
                        <asp:ListItem Value="0">สูบหลังตื่นนอนเกิน 1 ชั่วโมง (0 คะแนน)</asp:ListItem>
                    </asp:RadioButtonList>
                     </td>
                </tr>
                    <tr class="MenuSt">
                <th align="left" scope="row">6. คุณรู้สึกอย่างไร หากไม่สามารถสูบบุหรี่ได้ในที่ที่ห้ามสูบบุหรี่เป็นระยะเวลานาน เช่น ในห้องสมุด หรือโรงภาพยนตร์</th>
                </tr> 
                <tr>
                <td align="left" scope="row" valign="top">
                    <asp:RadioButtonList ID="optQ6" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Value="1">หงุดหงิด อึดอัด (1 คะแนน)</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True">เฉยๆ (0 คะแนน)</asp:ListItem>
                    </asp:RadioButtonList>
                     </td>
                </tr>
               <tr class="MenuSt">
                <th align="left" scope="row">7. ในแต่ละวัน บุหรี่มวนใดที่คุณคิดว่าถ้าไม่ได้สูบแล้วจะหงุดหงิดมากที่สุด</th>
                </tr>
                <tr>
                <td align="left" scope="row" valign="top">
                    <asp:RadioButtonList ID="optQ7" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Value="1">มวนแรกที่ สูบในตอนเช้า (1 คะแนน)</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True">มวนไหนๆ ก็เหมือนกัน (0 คะแนน)</asp:ListItem>
                    </asp:RadioButtonList>
                     </td>
                </tr>
             
               
             
                 <tr class="MenuSt">
                <th align="left" scope="row">8.	โดยปกติท่านสูบบุหรี่/ยาสูบวันละกี่มวน</th>
                </tr>
                 <tr>
                <td align="left" scope="row" valign="top">
                    <asp:RadioButtonList ID="optQ8" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Value="3">มากกว่า 31 มวนต่อวัน (3 คะแนน)</asp:ListItem>
                        <asp:ListItem Value="2">21 – 30 มวนต่อวัน (2 คะแนน)</asp:ListItem>
                        <asp:ListItem Value="1">11 – 20 มวนต่อวัน (1 คะแนน)</asp:ListItem>
                        <asp:ListItem Selected="True" Value="0">ไม่เกิน 10 มวนต่อวัน (0 คะแนน)</asp:ListItem>
                    </asp:RadioButtonList>
                     </td>
                </tr>
                
                 <tr class="MenuSt">
                <th align="left" scope="row">9. โดยเฉลี่ยคุณสูบบุหรี่มากที่สุดในช่วง 2-3 ชั่วโมงแรกหลังตื่นนอนมากกว่าช่วงอื่นๆ ของวันใช่หรือไม่ </th>
                </tr>
                 <tr>
                <td align="left" scope="row" valign="top">
                    <asp:RadioButtonList ID="optQ9" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Value="1">ใช่ (1 คะแนน)</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True">ไม่ใช่ (0 คะแนน)</asp:ListItem>
                    </asp:RadioButtonList>
                     </td>
                </tr>
                 <tr class="MenuSt">
                <th align="left" scope="row">10. ขณะเมื่อคุณป่วยต้องนอนอยู่บนเตียงเกือบตลอดเวลา คุณต้องการสูบบุหรี่หรือไม่ </th>
                </tr>
                 <tr>
                <td align="left" scope="row" valign="top">
                    <asp:RadioButtonList ID="optQ10" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Value="1">ต้องการ (1 คะแนน)</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True">ไม่ต้องการ (0 คะแนน)</asp:ListItem>
                    </asp:RadioButtonList>
                     </td>
                </tr>
                 <tr class="MenuSt">
                <th align="left" scope="row">11.	ประเมินภาวะติดนิโคติน โดย Fagerstrom Test for Nicotine Dependence</th>
                </tr>
                 <tr>
                <td align="left" scope="row" valign="top">
                     <asp:RadioButtonList ID="optQ11" runat="server" RepeatDirection="Horizontal">                  
                    <asp:ListItem Value="2">7 – 10 คะแนน ติดนิโคตินสูง</asp:ListItem>
                    <asp:ListItem Value="1">4 - 6 คะแนน ติดนิโคตินปานกลาง</asp:ListItem>
                    <asp:ListItem Value="0" Selected="True">น้อยกว่า 4 คะแนน ติดนิโคตินต่ำ</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                </tr>
                   <tr class="MenuSt">
                <th align="left" scope="row">12. ประเมินภาวะติดด้านร่างกาย</th>
                </tr>
                 <tr>
                <td align="left" scope="row" valign="top">
                    <asp:CheckBoxList ID="chkQ12" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="1">คุณสูบบุหรี่หลังอาหาร</asp:ListItem>
                          <asp:ListItem Value="2">คุณสูบบุหรี่พร้อมดื่มกาแฟหรือหลังดื่มกาแฟ</asp:ListItem>
                          <asp:ListItem Value="3">คุณสูบบุหรี่เวลาดื่มเหล้า</asp:ListItem>
                        </asp:CheckBoxList>
                </td>
                </tr>
                   <tr class="MenuSt">
                <th align="left" scope="row">13.	ประเมินภาวะติดด้านจิตใจ</th>
                </tr>
                 <tr>
                <td align="left" scope="row" valign="top">
                    <asp:CheckBoxList ID="chkQ13" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="1">คุณสูบบุหรี่เวลาว่าง</asp:ListItem>
                          <asp:ListItem Value="2">คุณสูบบุหรี่ขณะกำลังทำงาน</asp:ListItem>
                          <asp:ListItem Value="3">คุณสูบบุหรี่เวลาเครียด</asp:ListItem>
                        </asp:CheckBoxList>
                </td>
                </tr>
                 <tr class="MenuSt">
                <th align="left" scope="row"> 14. แรงจูงใจในการเลิกบุหรี่(ตอบได้มากกว่า1ข้อ)</th>
                </tr>
                 <tr>
                <td align="left" scope="row" valign="top"><table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                        <td><asp:CheckBoxList ID="chkQ14" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem>ครอบครัว</asp:ListItem>
                          <asp:ListItem>สุขภาพ</asp:ListItem>
                          <asp:ListItem>รู้สึกว่าสังคมไม่ยอมรับ</asp:ListItem>
                          <asp:ListItem>เศรษฐกิจ</asp:ListItem>
                          <asp:ListItem>อื่นๆระบุ</asp:ListItem>
                        </asp:CheckBoxList></td>
                        <td valign="bottom"><asp:TextBox ID="txtQ14" runat="server" Width="200px"></asp:TextBox></td>
                      </tr>
                  </table></td>
                </tr>
                 <tr class="MenuSt">
                <th align="left" scope="row">15.อัตราการไหลของอากาศหายใจออกที่สูงที่สุด </th>
                </tr>
                 <tr>
                   <td align="left" scope="row" valign="top"><table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                           <td>PEFR</td>
                           <td><asp:TextBox ID="txtPEFR_Value" runat="server" AutoPostBack="True">0</asp:TextBox></td>
                           <td>ค่า % PEFR</td>
                           <td><asp:TextBox ID="txtPEFR_Rate" runat="server"></asp:TextBox></td>
                         </tr>
                        <tr>
                           <td colspan="2">16.ปริมาณก๊าซคาร์บอนมอนอกไซด์ในลมหายใจ</td>
                           <td><asp:TextBox ID="txtCO_Value" runat="server" Width="60px"></asp:TextBox></td>
                           <td>ppm</td>
                         </tr>
                     </table></td>
                 </tr>
                </tbody>
            </table>      
        </td>
        </tr>        
        <tr>
        <td valign="top">&nbsp;</td>
        </tr>
      
    </table></td>
  </tr>
<tr>
    <td align="center" class="MenuSt">แผนการเลิกบุหรี่</td>
</tr>
<tr>
    <th align="left" class="MenuSt" scope="row">17.แผนการเลิกบุหรี่</th>
</tr>
<tr>
    <td align="left">
        <table>
            <tr>
                <td>
                    <asp:RadioButtonList ID="optStopPlane1" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="1">ปรับพฤติกรรมและสร้างแรงจูงใจ</asp:ListItem>
                        <asp:ListItem Value="2">ใช้ยาช่วยเลิกบุหรี่</asp:ListItem>
                    </asp:RadioButtonList>
                            </td>
                <td>กำหนดวันหยุดบุหรี่ (0 มวน) วันที่</td>
                 <td>
                     <asp:TextBox ID="xDateStopPlan1" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>

                    
                </td>
                <td>
                     <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                ControlToValidate="xDateStopPlan1" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="(\d{1}|\d{2})/(\d{1}|\d{2})/\d{4}"></asp:RegularExpressionValidator>

                    
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr class="MenuSt">
                <th align="left" scope="row">&nbsp;แนวทางการเลิกบุหรี่</th>
                </tr>
              <tr >
                <td align="left" scope="row">
                    <table cellpadding="0" cellspacing="2">
                        <tr>
                            <td valign="top">
                    <asp:RadioButtonList ID="optStopPlane2" runat="server">
                        <asp:ListItem Selected="True" Value="1">หักดิบ</asp:ListItem>
                        <asp:ListItem Value="2">ค่อยๆลด</asp:ListItem>
                    </asp:RadioButtonList>
                            </td>
                            <td valign="top"><table border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td></td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td>จาก <asp:TextBox ID="txtCGTargetRateBegin" runat="server" Width="40px"></asp:TextBox>
                  &nbsp;มวน/วัน เหลือ <asp:TextBox ID="txtCGTargetRateEnd" runat="server" Width="40px"></asp:TextBox>
                  &nbsp;มวน/วัน </td>
                                <td valign="top">
                                    <table border="0" cellspacing="0" cellpadding="4">
                                  <tr>
                                    <td>ภายใน <asp:TextBox ID="txtTargetDay" runat="server" Width="50px"></asp:TextBox>
                            &nbsp;วัน นับจากวันที่มารับบริการ A4 หรือวันที่</td>
                                    <td>
                                        
                                        <asp:TextBox ID="xDateStopPlan2" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                                      </td>
                                      <td>
                     <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                ControlToValidate="xDateStopPlan2" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="(\d{1}|\d{2})/(\d{1}|\d{2})/\d{4}"></asp:RegularExpressionValidator>

                    
                                      </td>
                                    </tr>
                                  </table></td>
                              </tr>
                            </table></td>
                        </tr>
                      </table>
                  </td>
                </tr>
<tr>
    <td align="center" class="MenuSt">ยาช่วยเลิกบุหรี่</td>
</tr>

                 <tr>
                <td align="left" scope="row" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                        <td>
                               <table border="0"   cellspacing="0" cellpadding="0">
                          <tr>
                            <td  width="120" class="MenuSt"><b> 18.ยาที่จ่าย</b></td>
                            <td>
                                 <asp:DropDownList  CssClass="form-control select2" ID="ddlMed" runat="server">
                                </asp:DropDownList>
                              </td>
                            <td>
                                อื่นๆ ระบุ</td>
                            <td colspan="2">
                                <asp:TextBox ID="txtMedName" runat="server" Width="200px"></asp:TextBox>
                              </td>
                          </tr>
                          <tr>
                            <td>ขนาดรับประทาน</td>
                            <td colspan="4">
                                <asp:TextBox ID="txtFrequency" runat="server" Width="100%"></asp:TextBox>
                              </td>
                          </tr>
                          <tr>
                            <td>จำนวน</td>
                            <td>
                                <asp:TextBox ID="txtMedQTY" runat="server" Width="60px"></asp:TextBox>
                              &nbsp;&nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                          </tr>
                          <tr>
                            <td><asp:Label ID="lblBalanceLabel" runat="server" Font-Size="8pt" ForeColor="Blue" Text="Balance = "></asp:Label>
                                </td>
                            <td colspan="4" Class="text10_nblue" >1.กรณี จ่ายยาเป็นแผงให้ระบุจำนวนเม็ด &nbsp;
                                 2.กรณี สมุนไพรชงหญ้าดอกขาว ให้ระบุเป็นซอง ใน 1 ห่อมี 10 ซอง 
                              </td>
                          </tr>
                          <tr>
                            <td>
                                <asp:Label ID="lblBalance" runat="server" Font-Size="8pt" ForeColor="Blue"></asp:Label>
                              </td>
                            <td colspan="3">
                                <dx:ASPxButton ID="cmdAddMed" runat="server" Text="เพิ่มยา/บันทึกชื่อยา" Theme="Material" Width="100px">
                                </dx:ASPxButton>
                              </td>
                          </tr>
                          </table>
                            </td>
                        </tr>
                  <tr>
                    <td>
              <asp:GridView ID="grdMed" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Font-Bold="False" DataKeyNames="itemID" Width="100%">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
                <asp:BoundField HeaderText="No." />
            <asp:BoundField DataField="ItemName" HeaderText="ยาที่จ่าย">
              <headerstyle HorizontalAlign="Left" />          
              <itemstyle HorizontalAlign="Left" Width="300px" />                      </asp:BoundField>
                <asp:BoundField HeaderText="จำนวน" DataField="QTY">
                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                </asp:BoundField>
                <asp:BoundField DataField="FrequencyDescription" HeaderText="ขนาดรับประทาน" />
            <asp:TemplateField HeaderText="ลบ">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgDel" runat="server" 
                                    ImageUrl="images/icon-delete.png" 
                                    CommandArgument='<%# Container.DataItemIndex %>' /></itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView></td>
                    </tr>
                  </table></td>
                </tr>
               
<tr>
    <td align="left">&nbsp;</td>
</tr>
<tr>
    <td align="left"  class="MenuSt">
        <table>
            <tr>
                <td>นัดติดตามอาการถอนนิโคตินและอาการข้างเคียงจากการใช้ยา ในช่วง 14 วันแรก หลังให้บริการ วันที่
</td>
                <td>
                    <asp:TextBox ID="xDateNextService" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                </td>
                <td>
                     <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                ControlToValidate="xDateNextService" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="(\d{1}|\d{2})/(\d{1}|\d{2})/\d{4}"></asp:RegularExpressionValidator>

                    
                </td>
                <td></td>
            </tr>
        </table>
    </td>
</tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="Section_Header">
      <tr>
        <td width="100">สถานะ</td>
        <td>
            <asp:CheckBox ID="chkStatus" runat="server" Text="จบการทำงาน" />
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center">
        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="100px" />
        </td>
  </tr>
</table>


 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
</section>

</asp:Content>