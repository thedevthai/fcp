﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="Home.aspx.vb" Inherits=".Homes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/rajchasistyles.css">

    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server"> 
        <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
      <!-- Main row -->

          
 <h3>จำนวนผู้รับบริการในระบบแยกตามโครงการ</h3> 
<br />
      <!-- for member -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><asp:Label ID="lblMember1" runat="server" Text="0"></asp:Label><sup style="font-size: 20px"></sup></h3>

              <p>F01-F19</p>
            </div>
            <div class="icon">
              <i class="ion ion-happy"></i>
            </div>
          <!--  <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><asp:Label ID="lblMember2" runat="server" Text="0"></asp:Label><sup style="font-size: 20px"></sup></h3>

              <p>บุหรี่</p>
            </div>
            <div class="icon">
              <i class="ion ion-no-smoking"></i>
            </div>
          <!--  <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-maroon">
            <div class="inner">
              <h3><asp:Label ID="lblMember3" runat="server" Text="0"></asp:Label><sup style="font-size: 20px"></sup></h3>

              <p>MTM</p>
            </div>
            <div class="icon">
              <i class="ion ion-medkit"></i>
            </div>
           <!--  <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
          </div>
        </div>
             <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          
        </div>
 <!-- ./col -->
           
      </div>
           <div class="row">
                 <div class="col-md-12">

         <div class="text-center" style="background-color:#45ab69">
             <img src="images/note.jpg" />
</div></div>
    </div>    <br />

      <div class="row">
        <!-- Left col -->
        <section class="col-lg-8 connectedSortable">     
              <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-document"></i>

              <h4 class="box-title">เอกสารประกอบโครงการ ปี 2566</h4>
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
                 
            </div>
 <div class="box-body">
          
          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="table table-hover">    
               <tr>
          <td width="3%" align="center"><img src="images/pdf.png" width="20" height="20" /></td>
          <td align="left"><a href="https://drive.google.com/drive/folders/1byW0WuvSSPB1EKU0SwvhMxVNsMnWjUdQ" target="_blank" class="text12_blue">เอกสารการประชุมวิชาการเรื่อง ทักษะการเลิกบุหรี่ และการจัดการด้านยา</a></td>
        </tr>
         <tr>
          <td width="3%" align="center"><img src="images/youtube.png" width="20" height="20" /></td>
          <td align="left"><a href="https://drive.google.com/drive/folders/1VOOFPIB9eo1BQYUNbN0pfhMyVsxnpdQ2" target="_blank" class="text12_blue">คลิปวิดีโอทักษะการเลิกบุหรี่และการจัดการด้านยา</a></td>
        </tr>
          
               <tr>
          <td width="3%" align="center"><img src="images/pdf.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/การบันทึกรายงานบริการเลิกบุหรี่.pdf" target="_blank" class="text12_blue">การบันทึกรายงานบริการเลิกบุหรี่</a></td>
        </tr>
                  <tr>
          <td width="3%" align="center"><img src="images/pdf.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/การบันทึกรายงานMTM66.pdf" target="_blank" class="text12_blue">การบันทึกรายงานMTM66</a></td>
        </tr>
                <tr>
          <td width="3%" align="center"><img src="images/youtube.png" width="20" height="20" /></td>
          <td align="left"><a href="https://www.youtube.com/watch?v=rTC05_1iCUc" target="_blank" class="text12_blue">การอบรมแนวทางการให้บริการป้องกันและสร้างเสริมสุขภาพ</a></td>
        </tr>

                

        </table>
                                                  
  </div>
            <div class="box-footer clearfix no-border">
          
            </div>
          </div>
<!--
            <div class="box box-primary collapsed-box">
            <div class="box-header">
              <i class="ion ion-document"></i>

              <h4 class="box-title">การอบรมวิชาการ Long covid  3 กุมภาพันธ์ 2565</h4>
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
                 
            </div>
 <div class="box-body">
          
          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="table table-hover">      
          
                <tr>
          <td width="3%" align="center"><img src="images/ppt.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/1Postcovid.SairatNoknoyFeb3.2022.pptx" target="_blank" class="text12_blue">Post COVID conditions</a></td>
        </tr>

                <tr>
          <td width="3%" align="center"><img src="images/ppt.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/2COVID-REHABILITATION-03022022.pptx" target="_blank" class="text12_blue">Screening LONG covid-19 syndrome + REHABILITATION</a></td>
        </tr>
               
                <tr>
          <td width="3%" align="center"><img src="images/pdf.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/3LongCovid.pdf" target="_blank" class="text12_blue">การบันทึกการติดตามผู้ป่วย Long Covid 19</a></td>
        </tr>


        </table>
                                                  
  </div>
            <div class="box-footer clearfix no-border">
          
            </div>
          </div>

<div class="box box-primary collapsed-box">
            <div class="box-header">
              <i class="ion ion-document"></i>

              <h4 class="box-title">การบันทึกติดตาม COVID</h4>
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
                 
            </div>
 <div class="box-body">
          
          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="table table-hover">      
          
                <tr>
          <td width="3%" align="center"><img src="images/pdf.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/วิธีการบันทึกติดตาม COVID.pdf" target="_blank" class="text12_blue">การบันทึกติดตาม COVID</a></td>
        </tr>

                <tr>
          <td width="3%" align="center"><img src="images/youtube.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/fcp-covid.mp4" target="_blank" class="text12_blue">VDO การใช้งานการบันทึกติดตาม COVID</a></td>
        </tr>
               
        </table>
                                                  
  </div>
            <div class="box-footer clearfix no-border">
          
            </div>
          </div>                     
            
<div class="box box-primary collapsed-box">
            <div class="box-header">
              <i class="ion ion-document"></i>

              <h4 class="box-title">เอกสารประกอบการโครงการ ปี 2565</h4>
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
                 
            </div>
 <div class="box-body">
          
          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="table table-hover">
        <tr>
          <td colspan="2" align="left"  class="MenuSt">เอกสารประกอบการประชุม 18/12/2564</td>
          </tr>
          
                <tr>
          <td width="3%" align="center"><img src="images/pdf.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/พัฒนาศักยภาพ18สค64.pdf" target="_blank" class="text12_blue">การบันทึกข้อมูล
การให้บริการสร้างเสริมสุขภาพและบริการเลิกบุหรี่</a></td>
        </tr>

                <tr>
          <td width="3%" align="center"><img src="images/pdf.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/pptการบันทึกFและMTM.pdf" target="_blank" class="text12_blue">การบันทึกงานสร้างเสริมสุขภาพ การคัดกรองฯ ( F1 F15 F17 F18 F19 ) และ MTM</a></td>
        </tr>             
        </table>
                                                  
  </div>
            <div class="box-footer clearfix no-border">
          
            </div>
          </div>
<div class="box box-primary collapsed-box">
            <div class="box-header">
              <i class="ion ion-document"></i>

              <h4 class="box-title">เอกสารประกอบการโครงการ ปี 2563</h4>
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
                 
            </div>
 <div class="box-body">
          
          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="table table-hover">
        <tr>
          <td colspan="2" align="left"  class="MenuSt">การประชุมวิชาการ
เภสัชอาสาพาเลิกบุหรี่และสร้างเสริมสุขภาพในร้านยา 1 สิงหาคม 2563</td>
          </tr>
          
                <tr>
          <td width="3%" align="center"><img src="images/youtube.png" width="20" height="20" /></td>
          <td align="left"><a href="https://youtu.be/J-PIZMYdId8" target="_blank" class="text12_blue">แนะนำโครงการ</a></td>
        </tr>

                <tr>
          <td width="3%" align="center"><img src="images/youtube.png" width="20" height="20" /></td>
          <td align="left"><a href="https://youtu.be/UwUc8VMkVgw" target="_blank" class="text12_blue">ความรู้เรื่องโรค NCDs และแนวทางการคัดกรองความเสี่ยง</a></td>
        </tr>

                <tr>
          <td width="3%" align="center"><img src="images/youtube.png" width="20" height="20" /></td>
          <td align="left"><a href="https://youtu.be/1iIUbg9B428" target="_blank" class="text12_blue">Program FCP </a></td>
        </tr>

                <tr>
          <td width="3%" align="center"><img src="images/youtube.png" width="20" height="20" /></td>
          <td align="left"><a href="https://youtu.be/PDswOBcFM7s" target="_blank" class="text12_blue">MTM</a></td>
        </tr>
                           

        <tr>
          <td width="3%" align="center"><img src="images/youtube.png" width="20" height="20" /></td>
          <td align="left"><a href="https://drive.google.com/drive/folders/1vX9BPS4yNM20rS3KuCXYqu6cshe1yHqK" target="_blank" class="text12_blue">VDO การให้บริการ คัดกรอง  MTM  และเลิกบุหรี</a></td>
        </tr>
        </table>
                                                  
  </div>
            <div class="box-footer clearfix no-border">
          
            </div>
          </div>
<div class="box box-primary collapsed-box">
            <div class="box-header">
              <i class="ion ion-document"></i>

              <h4 class="box-title">เอกสารประกอบการอบรมแผนงานเครือข่ายร้านยาพาเลิกบุหรี่และสร้างเสริมสุขภาพ ปี 2563-2565</h4>
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
                 
            </div>
 <div class="box-body">
          
          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="table table-hover">   
        <tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/cpamanual62.pdf" target="_blank" class="text12_blue">คู่มือการใช้โปรแกรม FCPPROJECT</a></td>
        </tr>
          <tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/Seminar62_Hypertension.pdf" target="_blank" class="text12_blue">Hypertension</a></td>
        </tr>
         
          <tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/Seminar62_Diabetes Mellitus2019.pdf" target="_blank" class="text12_blue">Diabetes Mellitus</a></td>
        </tr>
         
          <tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/Seminar62_Smoking.pdf" target="_blank" class="text12_blue">การสร้างเสริมสุขภาพเพื่อป้องกัน NCD 
การคัดกรองความเสี่ยงและบริการเลิกบุหรี่</a></td>
        </tr>
        </table>
                                                  
  </div>
            <div class="box-footer clearfix no-border">
          
            </div>
          </div>
-->
              <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-paper-plane"></i>

              <h4 class="box-title">แบบฟอร์มโครงการ</h4>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">

                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="table table-hover">
        <tr>
          <td colspan="2" align="left"  class="MenuSt">แบบฟอร์มคัดกรอง</td>
        </tr>
                      <tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/F15.pdf" target="_blank" class="text12_blue">F15</a></td>
        </tr>
          <tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/F19.pdf" target="_blank" class="text12_blue">F19</a></td>
        </tr>
       <!--
        <tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/F01.pdf" target="_blank" class="text12_blue">F1-3</a></td>
        </tr>
        <tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/F13ASU_URI.pdf" target="_blank" class="text12_blue">F13</a></td>
        </tr>
        <tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/F-14ASU.pdf" target="_blank" class="text12_blue">F14</a></td>
        </tr>
          <tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/F15.pdf" target="_blank" class="text12_blue">F15</a></td>
        </tr>
          <tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/F17.pdf" target="_blank" class="text12_blue">F17</a></td>
        </tr> 
          <tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/F18.pdf" target="_blank" class="text12_blue">F18</a></td>
        </tr> 
                   
                   
<tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/F20.pdf" target="_blank" class="text12_blue">F20 (Long Covid)</a></td>
        </tr>

        <tr>
          <td><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/Refer63.pdf" target="_blank" class="text12_blue">ใบส่งต่อ (Refer)</a></td>
        </tr>
         <tr>
          <td><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/PEFR_standard.pdf" target="_blank" class="text12_blue">การวัดค่าและคำนวน PEFR</a></td>
        </tr>
                     <tr>
          <td><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/PEFR_standard_table.pdf" target="_blank" class="text12_blue">ค่ามาตรฐาน PEFR ในแต่ละช่วงอายุ</a></td>
        </tr>
                     <tr>
          <td><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/normal_PEFR_for_Children.pdf" target="_blank" class="text12_blue">ค่ามาตรฐาน PEFR for Under 15 years</a></td>
        </tr>
             
         -->
      </table>  
     
          <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="table table-hover">
        <tr>
          <td colspan="2" align="left"  class="MenuSt">แบบฟอร์มบุหรี่</td>
        </tr>
                <tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/A1-A2.pdf" target="_blank" class="text12_blue">A1-A2</a></td>
        </tr>
        <tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/A1_A4.pdf" target="_blank" class="text12_blue">A1-A4</a></td>
        </tr>
        <tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/A4F.pdf" target="_blank" class="text12_blue">A4F</a></td>
        </tr>
        <tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/A5.pdf" target="_blank" class="text12_blue">A5</a></td>
        </tr>
       
      </table>
      
      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="table table-hover">
        <tr>
          <td colspan="2" align="left"  class="MenuSt">แบบฟอร์ม MTM และ เยี่ยมบ้าน</td>
        </tr>
        <tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/MTM63.pdf" target="_blank" class="text12_blue">แบบฟอร์ม MTM / Home Visit</a></td>
        </tr>
         <tr>
          <td width="3%" align="center"><img src="images/pdf_download.png" width="20" height="20" /></td>
          <td align="left"><a href="Docs/cpamanual.pdf" target="_blank" class="text12_blue">คู่มือแผนงานเครือข่ายร้านยาพาเลิกบุหรี่และสร้างเสริมสุขภาพ</a></td>
        </tr>
       
        
      </table>

            </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

      

        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-4 connectedSortable">
            <!--
               <div class="box box-danger">
            <div class="box-header">
              <i class="fa fa-users"></i>

              <h4 class="box-title"> Admin'Message : ประกาศจากผู้ดูแลระบบ</h4>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body text-center">
            <h4 class="text-red">ขณะนี้ ผู้ดูแลระบบกำลังปรับปรุงระบบอยู่<br />
         เพื่อป้องกันข้อมูลสูญหาย ให้ท่านหลีกเลี่ยงการบันทึกแบบฟอร์มทุกโครงการ <br />แต่ท่านยังสามารถดูข้อมูลหรือรายงานในระบบได้ปกติ</h4><br />
      ท่านสามารถเข้ามาบันทึกข้อมูลได้อีกครั้ง ภายใน 10 นาที หรือ หลังจากข้อความประกาศนี้หายไป <br />
         ขออภัยในความไม่สะดวก

            </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
            -->
  <div class="box box-primary">
            <div class="box-header">
              <i class="ion ion-bookmark"></i>

              <h4 class="box-title">คู่มือการใช้งาน</h4>
             <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
                 
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                
                            <table width="100%" border="0" cellspacing="2" cellpadding="2" class="table table-hover">
                          <tr>
                            <td width="50" align="center"><img src="images/pdf.png" width="20" height="20" /></td>
                            <td><a href="http://www.FCPPROJECT.com/Docs/User_Manual_Shop_Patient.pdf" target="_blank">คู่มือการใช้งานสำหรับร้านยา (เพิ่มเติมโมดูลการทำงานใหม่)</a></td>
                            <td width="50" align="center">&nbsp;</td>
                            <td></td>
                            <td rowspan="2" align="right" valign="top">
                             </td>
                          </tr>
                          <tr>
                            <td align="center"><img src="images/pdf.png" width="20" height="20" /></td>
                            <td><a href="http://www.FCPPROJECT.com/Docs/User_Manual_Shop.pdf" target="_blank">คู่มือการใช้งานสำหรับร้านยา</a></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                          </tr>
                        </table>
                                 
            </div>
            <!-- /.box-body -->
            
          </div>
          <!-- /.box -->

          <!-- Map box -->
          <div class="box box-primary">
            <div class="box-header">
              <!-- tools box -->
              <div class="pull-right box-tools">                
                <button type="button" class="btn btn-box-tool" data-widget="collapse"
                        data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                  <i class="fa fa-minus"></i></button>
              </div>
              <!-- /. tools -->

              <i class="fa fa-map-marker"></i>

              <h4 class="box-title">
                Visitors : จำนวนคนเข้าระบบย้อนหลัง 5 วัน
              </h4>
            </div>
            <div class="box-body">
            <asp:GridView ID="grdVisitor" runat="server" AutoGenerateColumns="False" CellPadding="2"  CssClass="table table-hover" Font-Bold="False" GridLines="None" PageSize="20" Width="99%" CellSpacing="4" ShowHeader="False">
                <columns>
                    <asp:BoundField DataField="VisitDate" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}">
                    </asp:BoundField>
                    <asp:BoundField DataField="nCount" HeaderText="Count">
                    <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:TemplateField>
                        <ItemTemplate><i class="ion ion-man"></i>
                        </ItemTemplate>
                        <ItemStyle Width="10px" />
                    </asp:TemplateField>
                </columns>
                <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" VerticalAlign="Middle" />
            </asp:GridView>                        
                                
            </div>
            <!-- /.box-body-->    
           
          </div>
          <!-- /.box -->
             
          
            <!-- quick email widget -->
          <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-users"></i>

              <h4 class="box-title">User Online : <asp:Label ID="lblUserOnlineCount" runat="server" Text=""></asp:Label></h4>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
            <asp:Label ID="lblOnline" runat="server" Width="99%"></asp:Label>
            </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

              

        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->

  <!-- /.content-wrapper -->
  <div class="control-sidebar-bg"></div>
    
</asp:Content>
