﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="A5F.aspx.vb" Inherits=".A5F" %>
<%@ Register src="ucHistoryYear.ascx" tagname="ucHistoryYear" tagprefix="uc1" %>
<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/pagestyles.css">
<link rel="stylesheet" type="text/css" href="css/cpastyles.css">
<link rel="stylesheet" type="text/css" href="css/uidialog.css">
<link rel="stylesheet" type="text/css" href="css/dc_table.css">  
    <style type="text/css">

.dxbButtonSys /*Bootstrap correction*/
{
    -webkit-box-sizing: content-box;
    -moz-box-sizing: content-box;
    box-sizing: content-box;
}
.dxbButtonSys
{
	cursor: pointer;
	display: inline-block;
	text-align: center;
	white-space: nowrap;
}

.dx-vam, .dx-valm { vertical-align: middle; }
.dx-vam, .dx-vat, .dx-vab { display: inline-block!important; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>โครงการสนับสนุนการทำกิจกรรมปรับเปลี่ยนพฤติกรรมผู้เสพยาสูบโดยเภสัชกรชุมชน
        <small>(ติดตามผล A5)</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    


    
  <div align="center">
    <uc1:ucHistoryYear ID="ucHistoryYear1" runat="server" />
  </div>
    <br />
    <div class="pull-right">
          <table  border="0" cellspacing="2" cellpadding="0">
        <tr>
         
          <td class="NameEN">Item ID : </td>
          <td class="NameEN">
              <asp:Label ID="lblID" 
                  runat="server"></asp:Label>            </td>
             <td class="NameEN">
              Ref.ID :            </td>
          <td class="NameEN">
              <asp:Label ID="lblRefID" runat="server"></asp:Label>
            </td>
        </tr>
      </table>
        </div> 

 <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">ข้อมูลร้านยาที่ให้บริการ</h3>
          <div class="box-tools pull-right">
              

            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
 <table width="100%" border="0" align="left" cellpadding="0" cellspacing="2" class="OptionPanels">
      <tr>
        <td width="50">รหัสร้าน</td>
        <td width="120" class="LocationName">
            <asp:Label ID="lblLocationID" runat="server"></asp:Label>          </td>
        <td width="50">ชื่อร้าน</td>
        <td width="300" class="LocationName">
            <asp:Label ID="lblLocationName" runat="server"></asp:Label>          </td>
           <td width="100">รหัสหน่วยบริการ</td>
           <td  class="LocationName">
               <asp:Label ID="lblLocationCode" runat="server"></asp:Label>
          </td>
        <td width="50">จังหวัด</td>
        <td class="LocationName">
            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>          </td>
      </tr>
    </table>


              
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">
      <tr>       
          <td>ปี</td>
          <td>
              <asp:TextBox ID="txtYear" runat="server" Width="50px"></asp:TextBox>
          </td>
        <td>วันที่ให้บริการ</td>
        <td><asp:TextBox ID="txtServiceDate" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                     </td>
          <td align="left"> 
<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                ControlToValidate="txtServiceDate" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="(\d{1}|\d{2})/(\d{1}|\d{2})/\d{4}"></asp:RegularExpressionValidator></td>
         <td>ระยะเวลาที่ให้บริการ</td>
        <td>
            <asp:TextBox ID="txtTime" runat="server" Width="50px"></asp:TextBox>
&nbsp;นาที</td>
           <td>เภสัชกรผู้ให้บริการ</td>
        <td>
             <asp:DropDownList  CssClass="form-control select2" ID="ddlPerson" runat="server">            </asp:DropDownList>          </td>
      </tr>
    </table>
            
        
      </div>
      
</div>

  
<div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">วิธีการติดตาม</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">         
 
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">
    
      <tr>
         <td>
            แหล่งที่มา</td>
        <td align="left" colspan="5">

            <table>
                <tr>
                    <td>     <asp:RadioButtonList ID="optPatientFrom" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Value="W">Walk in</asp:ListItem>
                <asp:ListItem Value="H">Home visit</asp:ListItem>
                <asp:ListItem Value="R">หน่วยบริการส่งมา</asp:ListItem>
                <asp:ListItem Value="C">โครงการลดความแออัด</asp:ListItem> 
                <asp:ListItem Value="9">อื่นๆ</asp:ListItem> 
            </asp:RadioButtonList>   </td>
                       <td>ระบุ</td>
                       <td>
                           <asp:TextBox ID="txtFromRemark" runat="server" Width="300px"></asp:TextBox></td>
                </tr>
            </table>
               

          </td>
      </tr>
      <tr>
         <td valign="top">การให้บริการ</td>
        <td align="left" colspan="5">
            <table>
                <tr>
                    <td>
            <asp:RadioButtonList ID="optService" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Value="1">Face to face</asp:ListItem>
                <asp:ListItem Value="2">Telepharmacy</asp:ListItem>
                <asp:ListItem Value="9">อื่นๆ</asp:ListItem>
            </asp:RadioButtonList>
                    </td>
                    <td>
             <asp:TextBox ID="txtServiceRemark" runat="server" Width="300px" placeholder="กรณีอื่นๆ ระบุ"></asp:TextBox>
                    </td>
                </tr>
            </table>
          </td>
      </tr>
      <tr>
         <td valign="top">กรณี Telepharmacy</td>
        <td align="left" colspan="5">
             <table>
                <tr>
                    <td>  <asp:RadioButtonList ID="optTelepharm" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Value="1">ผป.ของร้านเอง</asp:ListItem>
                <asp:ListItem Value="2">ผป.โครงการลดแอัด (ระบุ รพ.แม่ข่าย)</asp:ListItem>
                <asp:ListItem Value="3">ผป.รับยาทาง ปณ.จาก รพ. (ระบุ รพ.)</asp:ListItem>
                <asp:ListItem Value="9">ผป.ในโครงการอื่นๆ (ระบุ)</asp:ListItem>
            </asp:RadioButtonList></td>
                       <td width="60" class="text-right">ระบุ</td>
                       <td>
                           <asp:TextBox ID="txtTelepharmacyRemark" runat="server" Width="300px"></asp:TextBox></td>
                </tr>
            </table>

          
          </td>
      </tr>
      <tr>
         <td valign="top">วิธีการสื่อสาร Telepharmacy</td>
        <td align="left" colspan="5">

             <table>
                <tr>
                    <td>  <asp:RadioButtonList ID="optTelepharmacyMethod" runat="server">
                  <asp:ListItem Value="1" >โทรศัพท์ หรือ  line call  <u><b>ไม่มีการบันทึกทั้งภาพและเสียง</b></u>        </asp:ListItem>
                <asp:ListItem  Value="2">โทรศัพท์  หรือ  Line call <u><b>บันทึกแต่เสียง</b></u></asp:ListItem> 
                  <asp:ListItem Value="3">มี soft ware หรือ program หรือ  Line Call  หรือ อื่นๆ  และ<u><b>มีการบันทึก ภาพและเสียง</b></u></asp:ListItem>
            </asp:RadioButtonList></td>
                       <td> <table class="nav-justified">
                  <tr>
                      <td>ระบุ วิธีการบันทึก</td>
                      <td><asp:TextBox ID="txtRecordMethod" runat="server" Width="300px"></asp:TextBox></td>
                  </tr>
                  <tr>
                      <td>ระบุ  สถานที่เก็บข้อมูล</td>
                      <td> <asp:TextBox ID="txtRecordLocation" runat="server" Width="300px"></asp:TextBox></td>
                  </tr>
              </table></td>

                </tr>
            </table>
            
          
           </td>
          <td  colspan="2" valign="top">
             
          </td>
      </tr>
    </table>      

</div>   
</div>  
    <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลกิจกรรม</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">

 <table width="100%" border="0" align="center"> 
  
 
  <tr>
    <td align="center" class="MenuSt">ผลการติดตาม A5 ครั้งที่&nbsp;
        <asp:TextBox ID="lblSEQ" runat="server" Width="50px" CssClass="seqno"></asp:TextBox></td>
  </tr>
  <tr>
    <td>
    <table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td  valign="top">
          <table width="100%" align="center" class="dc_table_s3">
            <thead>
              </thead>
            <tfoot>
              </tfoot>
            <tbody>
            
                  <tr class="MenuSt">
                <th align="left" scope="row">1. วิธีติดตาม</th>
                </tr>
                 <tr>
                <td align="left" scope="row" valign="top"><table border="0" cellspacing="0" cellpadding="2">
                  <tr>
                    <td>วิธีการติดตาม</td>
                    <td><asp:RadioButtonList ID="optFollowChannel" runat="server" 
                RepeatDirection="Horizontal">
                  <asp:ListItem Value="CALL" Selected="True">โทรศัพท์</asp:ListItem>
                  <asp:ListItem Value="WALK">ที่ร้าน</asp:ListItem>
                        <asp:ListItem Value="OTHER">อื่นๆ (ระบุ)</asp:ListItem>
                </asp:RadioButtonList></td>
                    <td>
            <asp:TextBox ID="txtFollowRemark" runat="server" AutoPostBack="True" Width="200px"></asp:TextBox>
                      </td>
                  </tr>
                </table>
                    </td>
                </tr>
             <tr class="MenuSt">
                <th align="left" scope="row">2. การสูบ</th>
                </tr>
                 <tr>
                <td align="left" scope="row" valign="top"><table border="0" cellspacing="0" cellpadding="2">
                  <tr>
                    <td><asp:RadioButtonList ID="optFinalResult" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                      <asp:ListItem Selected="True" Value="1">หยุดสูบได้</asp:ListItem>
                      <asp:ListItem Value="2">ยังคงสูบอยู่</asp:ListItem>
                      </asp:RadioButtonList></td>
                    <td>
                      <asp:TextBox ID="txtStillSmoke" runat="server" AutoPostBack="True" Width="50px"></asp:TextBox>
  &nbsp;<asp:Label ID="lblQ5" runat="server" Text="ตั้งแต่วันที่"></asp:Label>
                      </td>
                    <td>
                         <asp:TextBox ID="dtpFinalStopDate" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox></td>
          <td align="left"> 
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="dtpFinalStopDate" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="(\d{1}|\d{2})/(\d{1}|\d{2})/\d{4}"></asp:RegularExpressionValidator>

                      </td>
                    <td>
                        <asp:Label ID="lblCompareLabel" runat="server" Text="เทียบกับ A1"></asp:Label>
                      </td>
                    <td>
                    <asp:RadioButtonList ID="optChangeRate" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="L" Selected="True">ลดลง</asp:ListItem>
                        <asp:ListItem Value="E">เท่าเดิม</asp:ListItem>
                        <asp:ListItem Value="H">เพิ่มขึ้น</asp:ListItem>
                    </asp:RadioButtonList>
                      </td>
                  </tr>
                    </table></td>
                </tr>
                
              <tr>
                <th align="left" scope="row">
                    <table width="100%">
                        <tr>
                            <td width="50%">
                                <table>
                                    <tr>
                                        <td class="MenuSt">3. อาการถอนนิโคติน</td>
                                        <td>
                    <asp:RadioButtonList ID="optNicotinEffect" runat="server" RepeatDirection="Horizontal">
                      <asp:ListItem Value="1">พบ</asp:ListItem>
                        <asp:ListItem Value="0">ไม่พบ</asp:ListItem>
                      </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                 <table>
                                    <tr>
                                        <td class="MenuSt">4. กรณีที่ใช้ยาช่วยเลิกบุหรี่ อาการข้างเคียงจากยาช่วยเลิกบุหรี่ </td>
                                        <td>
                    <asp:RadioButtonList ID="optMedEffect" runat="server" RepeatDirection="Horizontal">
                      <asp:ListItem Value="1">พบ</asp:ListItem>
                        <asp:ListItem Value="0">ไม่พบ</asp:ListItem>
                      </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                                </td>
                        </tr>
                    </table>
                  </th>
                </tr>
                 <tr>
                <td align="left" valign="top">
                    <table width="100%">
                        <tr>
                            <td width="50%">อาการที่พบและคำแนะนำ</td>
                            <td>อาการที่พบและคำแนะนำ</td>
                        </tr>
                        <tr>
                            <td>
                                    <asp:TextBox ID="txtRecommend1" runat="server" Height="60px" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                </td>
                            <td>
                                    <asp:TextBox ID="txtRecommend2" runat="server" Height="60px" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                </td>
                        </tr>
                    </table>
                     </td>
                </tr>
               <tr>
                <th align="left">
                    <table width="100%">
                        <tr>
                            <td width="50%">
                                <table>
                                    <tr>
                                        <td class="MenuSt">5. ปัญหาเชิงพฤติกรรม สังคมและความเคยชิน หรือ ความเชื่อ </td>
                                        <td>
                    <asp:RadioButtonList ID="optSocietyEffect" runat="server" RepeatDirection="Horizontal">
                      <asp:ListItem Value="1">พบ</asp:ListItem>
                        <asp:ListItem Value="0">ไม่พบ</asp:ListItem>
                      </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                 <table>
                                    <tr>
                                        <td class="MenuSt">6.คำแนะนำอื่นๆ เช่น การเสริมแรง การให้กำลังใจผู้ป่วย</td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                                </td>
                        </tr>
                    </table>
                  </th>
                </tr>
                <tr>
                <td align="left" valign="top">
                    <table width="100%">
                        <tr>
                            <td width="50%">ปัญหาที่พบและคำแนะนำ</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                    <asp:TextBox ID="txtRecommend3" runat="server" Height="60px" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                </td>
                            <td>
                                    <asp:TextBox ID="txtRecommend4" runat="server" Height="60px" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                </td>
                        </tr>
                    </table>
                    </td>
                </tr>
              <tr class="MenuSt">
                <th align="left" scope="row">7. อัตราการไหลของอากาศหายใจออกที่สูงที่สุด </th>
                </tr>
                 <tr>
                   <td align="left" scope="row" valign="top"><table border="0" cellspacing="0" cellpadding="0">
                        <tr>
        <td>น้ำหนัก</td>
        <td>
            <asp:TextBox ID="txtWeight" runat="server" Width="50px"></asp:TextBox>
          </td>
        <td>&nbsp; กก.&nbsp; ส่วนสูง</td>
        <td>
            <asp:TextBox ID="txtHeigh" runat="server" AutoPostBack="True" Width="50px"></asp:TextBox>
&nbsp;ซม.</td>
      </tr>
                        <tr>
                           <td>PEFR</td>
                           <td><asp:TextBox ID="txtPEFR_Value" runat="server" AutoPostBack="True">0</asp:TextBox></td>
                           <td>ค่า % PEFR</td>
                           <td><asp:TextBox ID="txtPEFR_Rate" runat="server"></asp:TextBox></td>
                         </tr>
                        <tr>
                           <td colspan="3" class="MenuSt">8.ปริมาณก๊าซคาร์บอนมอนอกไซด์ในลมหายใจ </td>
                           <td>
            <asp:TextBox ID="txtCo_Value" runat="server" Width="50px"></asp:TextBox>
&nbsp;ppm.</td>
                         </tr>
                     </table></td>
                 </tr>

<tr>
    <td align="center" class="MenuSt">แผนการเลิกบุหรี่</td>
</tr>
<tr>
    <th align="left" scope="row" class="MenuSt">9. แผนการเลิกบุหรี่</th>
</tr>
<tr>
    <td align="left">
        <table>
            <tr>
                <td>
                    <asp:RadioButtonList ID="optStopPlane1" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="1">ปรับพฤติกรรมและสร้างแรงจูงใจ</asp:ListItem>
                        <asp:ListItem Value="2">ใช้ยาช่วยเลิกบุหรี่</asp:ListItem>
                    </asp:RadioButtonList>
                            </td>
                <td>กำหนดวันหยุดบุหรี่ (0 มวน) วันที่</td>
                 <td>
                      <asp:TextBox ID="xDateStopPlan1" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox></td>
          <td align="left"> 
<asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                ControlToValidate="xDateStopPlan1" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="(\d{1}|\d{2})/(\d{1}|\d{2})/\d{4}"></asp:RegularExpressionValidator>

                </td>
            </tr>
        </table>
    </td>
</tr>
<tr class="MenuSt">
                <th align="left" scope="row">10.&nbsp;แนวทางการเลิกบุหรี่</th>
                </tr>
              <tr >
                <td align="left" scope="row">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top">
                    <asp:RadioButtonList ID="optStopPlane2" runat="server" AutoPostBack="True">
                        <asp:ListItem Selected="True" Value="1">หักดิบ</asp:ListItem>
                        <asp:ListItem Value="2">ค่อยๆลด</asp:ListItem>
                    </asp:RadioButtonList>
                            </td>
                            <td valign="top"><table border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td><table border="0" cellspacing="0" cellpadding="2">
                                  <tr>
                                    <td></td>
                                    <td>
                                        &nbsp;</td>
                                  </tr>
                                </table></td>
                                <td>&nbsp;</td>
                              </tr>
                              <tr>
                                <td>จาก <asp:TextBox ID="txtCGTargetRateBegin" runat="server" Width="40px"></asp:TextBox>
                  &nbsp;มวน/วัน เหลือ <asp:TextBox ID="txtCGTargetRateEnd" runat="server" Width="40px"></asp:TextBox>
                  &nbsp;มวน/วัน </td>
                                <td valign="top"><table border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td>ภายใน <asp:TextBox ID="txtTargetDay" runat="server" Width="50px"></asp:TextBox>
                            &nbsp;วัน นับจากวันที่มารับบริการ A4 หรือวันที่</td>
                                    <td>
                                         <asp:TextBox ID="xDateStopPlan2" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox></td>
          <td align="left"> 
<asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
                ControlToValidate="xDateStopPlan2" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="(\d{1}|\d{2})/(\d{1}|\d{2})/\d{4}"></asp:RegularExpressionValidator>


                                      </td>
                                    </tr>
                                  </table></td>
                              </tr>
                            </table></td>
                        </tr>
                      </table>
                  </td>
                </tr>
                <tr>
    <td align="center" class="MenuSt">ยาช่วยเลิกบุหรี่ที่จ่าย</td>
</tr>
                 <tr class="MenuSt">
                <th align="left" scope="row">  11. สรุปยาช่วยเลิกบุหรี่ที่จ่าย (กรณีบรรจุแผงให้นับ เม็ด)</th>
                </tr>
                 <tr>
                <td align="left" scope="row" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                        <td>
                            
                               <table border="0"   cellspacing="0" cellpadding="0">
                          <tr>
                            <td  width="120">ยาที่จ่าย</td>
                            <td>
                                 <asp:DropDownList  CssClass="form-control select2" ID="ddlMed" runat="server">
                                </asp:DropDownList>
                              </td>
                            <td>
                                อื่นๆ ระบุ</td>
                            <td colspan="2">
                                <asp:TextBox ID="txtMed" runat="server" Width="200px"></asp:TextBox>
                              </td>
                          </tr>
                          <tr>
                            <td>ขนาดรับประทาน</td>
                            <td colspan="4">
                                <asp:TextBox ID="txtFrequency" runat="server" Width="100%"></asp:TextBox>
                              </td>
                          </tr>
                          <tr>
                            <td>จำนวน</td>
                            <td>
                                <asp:TextBox ID="txtMedQTY" runat="server" Width="60px"></asp:TextBox>
                              &nbsp;&nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                          </tr>
                          <tr>
                            <td><asp:Label ID="lblBalanceLabel" runat="server" Font-Size="8pt" ForeColor="Blue" Text="Balance = "></asp:Label>
                                </td>
                            <td colspan="4" Class="text10_nblue" >1.กรณี จ่ายยาเป็นแผงให้ระบุจำนวนเม็ด &nbsp;
                                 2.กรณี สมุนไพรชงหญ้าดอกขาว ให้ระบุเป็นซอง ใน 1 ห่อมี 10 ซอง 
                              </td>
                          </tr>
                          <tr>
                            <td>
                                <asp:Label ID="lblBalance" runat="server" Font-Size="8pt" ForeColor="Blue"></asp:Label>
                              </td>
                            <td colspan="3">
                                <dx:ASPxButton ID="cmdAddMed" runat="server" Text="เพิ่มยา/บันทึกชื่อยา" Theme="Material" Width="100px">
                                </dx:ASPxButton>
                              </td>
                          </tr>
                          </table>

                        </td>
                        </tr>
                  <tr>
                    <td>
                        <asp:Label ID="lblNoMed" runat="server" Font-Size="12pt" ForeColor="Red" Text="ไม่มีรายการจ่ายยาช่วยเลิกบุหรี่"></asp:Label>
              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Font-Bold="False" DataKeyNames="itemID">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="No.">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />                      </asp:BoundField>
            <asp:BoundField DataField="ItemName" HeaderText="ยาที่จ่าย">
              <headerstyle HorizontalAlign="Left" />          
              <itemstyle HorizontalAlign="Left" Width="300px" />                      </asp:BoundField>
                <asp:BoundField HeaderText="จำนวน" DataField="QTY">
                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                </asp:BoundField>
                <asp:BoundField DataField="FrequencyDescription" HeaderText="ขนาดรับประทาน" />
            <asp:TemplateField HeaderText="ลบ">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgDel" runat="server" 
                                    ImageUrl="images/icon-delete.png" 
                                    CommandArgument='<%# Container.DataItemIndex %>' /></itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView></td>
                    </tr>
                  </table></td>
                </tr>
               
                </tbody>
            </table>      
        </td>
        </tr>            
    </table></td>
  </tr>
<!--
   <tr>
    <td align="center" class="MenuSt">สรุุปผู้สูบบุหรี่รายนี้</td>
  </tr>
   <tr>
    <td align="left">
        <asp:RadioButtonList ID="optNextStep" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
            <asp:ListItem Selected="True" Value="1">รับบริการต่อในระดับ A5</asp:ListItem>
            <asp:ListItem Value="2">ไม่รับบริการต่อในระดับ A5</asp:ListItem>
        </asp:RadioButtonList>
       </td>
  </tr>
   <tr>
    <td align="left"><table border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td>
            <asp:Label ID="lblProblem" runat="server" Text="เนื่องจาก"></asp:Label>
          </td>
        <td>
            <asp:RadioButtonList ID="optNextCause" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="1">ไม่สะดวกที่จะรับบริการ</asp:ListItem>
                <asp:ListItem Value="2">ไม่อยากเลิก</asp:ListItem>
                <asp:ListItem Value="3">อื่นๆ</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        <td>
          <asp:TextBox ID="txtCause" runat="server" Width="250px"></asp:TextBox>          </td>
        </tr>
      <tr>
        <td align="left" valign="middle">หมายเหตุ</td>
        <td align="left" valign="middle">จำนวนครั้งที่มีการติดต่อผู้สูบเพื่อให้มารับบริการ A5
             <asp:DropDownList  CssClass="form-control select2"  ID="ddlTimeService" runat="server" Width="50px">
                <asp:ListItem Selected="True">0</asp:ListItem>
                <asp:ListItem>1</asp:ListItem>
                <asp:ListItem>2</asp:ListItem>
                <asp:ListItem>3</asp:ListItem>
            </asp:DropDownList>
          </td>
        <td align="left" valign="middle">&nbsp;ครั้ง</td>
        </tr>
      </table></td>
  </tr>
     -->
     <tr>
    <td align="left"  class="MenuSt">
        <table>
            <tr>
                <td>นัดครั้งต่อไป วันที่</td>
                <td>
                     <asp:TextBox ID="xDateNextService" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox></td>
          <td align="left"> 
<asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" 
                ControlToValidate="xDateNextService" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="(\d{1}|\d{2})/(\d{1}|\d{2})/\d{4}"></asp:RegularExpressionValidator>

                </td>
            </tr>
        </table>
    </td>
</tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="Section_Header">
      <tr>
        <td width="100">สถานะ</td>
        <td>
            <asp:CheckBox ID="chkStatus" runat="server" Text="จบการทำงาน" />
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center">
        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="100px" />
        </td>
  </tr>
        <tr>
    <td align="center">&nbsp;</td>
  </tr>
        <tr>
    <td align="center" class="MenuSt">ประวัติการติดตาม A5</td>
  </tr>
   <tr>
        <td valign="top">
              <asp:GridView ID="grdA05" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="ครั้งที่" DataField="FollowSEQ">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />                      </asp:BoundField>
            <asp:BoundField DataField="FollowChannelTXT" HeaderText="วิธีติดตาม">
              <headerstyle HorizontalAlign="Center" />          
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />                      </asp:BoundField>
            <asp:BoundField DataField="FinalResultTXT" HeaderText="พฤติกรรมการสูบ">
              <headerstyle HorizontalAlign="Left" />          
              <itemstyle HorizontalAlign="Center" />                      </asp:BoundField>
                <asp:BoundField DataField="NicotineEffectTXT" HeaderText="อาการถอนนิโคติน" />
                <asp:BoundField HeaderText="อาการข้างเคียง" DataField="MedEffectTXT" />
                <asp:BoundField HeaderText="ปัญหาเชิงพฤติกรรม" DataField="SocietyEffectTXT" />
                <asp:BoundField HeaderText="แผนการเลิก" DataField="ServicePlanTXT" />
            <asp:TemplateField HeaderText="Edit">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ServiceUID")%>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            <asp:TemplateField>
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgDelete" runat="server" 
                                    ImageUrl="images/icon-delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem,"ServiceUID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView></td>
	</tr>
  <tr>
        <td valign="top">&nbsp;</td>
	</tr>
</table>
     </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    </section>
</asp:Content>