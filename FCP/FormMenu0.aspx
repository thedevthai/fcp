﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="FormMenu0.aspx.vb" Inherits=".FormMenu0" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>เลือกบันทึกกิจกรรม
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
     <asp:Panel ID="pnProjectF" runat="server"> 
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-user-circle"></i>

              <h3 class="box-title">โครงการให้คำปรึกษาฯ</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
 
     <div><asp:HyperLink ID="HyperLink1" runat="server" CssClass="buttonMenuB" 
            NavigateUrl="F01.aspx?ActionType=frm&t=new" Width="100%">F01&nbsp;&nbsp;&nbsp;&nbsp;การคัดกรองความเสี่ยงในกลุ่มภาวะโรคเมตาบอลิก (โรคเบาหวาน โรคความดันโลหิตสูง และโรคอ้วน)</asp:HyperLink></div>
   
 
     <div><asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="FormList.aspx?ActionType=frm" Width="100%" CssClass="buttonMenuB">F02,F03&nbsp;&nbsp;&nbsp;&nbsp;กิจกรรมการให้ความรู้ คำแนะนำและคำปรึกษาในกลุ่ม Metabolic Syndrome </asp:HyperLink></div>
   
 
 
     <div><asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="F04.aspx?ActionType=frm" Width="100%" CssClass="buttonMenu" Visible="False">F04&nbsp;&nbsp;&nbsp;&nbsp;การให้ความรู้ คำแนะนำและคำปรึกษาเรื่องการสร้างเสริมภูมิคุ้มกันโรคด้วยวัคซีนในเด็กอายุ 0-14 ปี(EPI Program) </asp:HyperLink></div>
   
 
     <div><asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="F05.aspx?ActionType=frm" Width="100%" CssClass="buttonMenu" Visible="False">F05&nbsp;&nbsp;&nbsp;&nbsp;การให้ความรู้ คำแนะนำและคำปรึกษาในการการสร้างเสริมสุขภาพและป้องกันการเป็นโรคเรื้อรังที่เกี่ยวกับระบบทางเดินหายใจ </asp:HyperLink></div>
   
 
     <div><asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="F06.aspx?ActionType=frm" Width="100%" CssClass="buttonMenu">F06&nbsp;&nbsp;&nbsp;&nbsp;กิจกรรมการให้ความรู้ คำแนะนำและคำปรึกษาในการการสร้างเสริมสุขภาพและป้องกันโรคโดยการฝากครรภ์ </asp:HyperLink></div>
   
 
     <div><asp:HyperLink ID="HyperLink7" runat="server" NavigateUrl="F07.aspx?ActionType=frm" Width="100%" CssClass="buttonMenu">F07&nbsp;&nbsp;&nbsp;&nbsp;กิจกรรมการให้ความรู้ คำแนะนำและคำปรึกษาเรื่องยาคุมกำเนิด ชนิดรับประทาน </asp:HyperLink></div>
   
 
     <div><asp:HyperLink ID="HyperLink8" runat="server" NavigateUrl="F08.aspx?ActionType=frm" Width="100%" CssClass="buttonMenu">F08&nbsp;&nbsp;&nbsp;&nbsp;กิจกรรมการให้ความรู้ คำแนะนำและคำปรึกษาเรื่องยาคุมฉุกเฉิน </asp:HyperLink></div>
    
 
     <div><asp:HyperLink ID="HyperLink9" runat="server" NavigateUrl="F09.aspx?ActionType=frm" Width="100%" CssClass="buttonMenu">F09&nbsp;&nbsp;&nbsp;&nbsp;กิจกรรมการให้ความรู้ คำแนะนำและคำปรึกษาเรื่องการใช้ถุงยางอนามัย </asp:HyperLink></div>
   
 
     <div><asp:HyperLink ID="HyperLink10" runat="server" NavigateUrl="F10.aspx?ActionType=frm" Width="100%" CssClass="buttonMenuB">F10&nbsp;&nbsp;&nbsp;&nbsp;กิจกรรมการให้ความรู้ คำแนะนำและคำปรึกษาเรื่องโรคที่เกิดจากการมีเพศสัมพันธ์
                          STI และ Pre-VCT ในกลุ่มเสี่ยงการติดเชื้อ HIV </asp:HyperLink></div>
 
 
     <div><asp:HyperLink ID="HyperLink11" runat="server" NavigateUrl="F11.aspx?ActionType=frm" Width="100%" CssClass="buttonMenu">F11&nbsp;&nbsp;&nbsp;&nbsp;กิจกรรมการให้ความรู้ คำแนะนำและคำปรึกษาเรื่องการตรวจคัดกรองมะเร็งปากมดลูก
                          โดยวิธี Pap Smear </asp:HyperLink></div>
 
 
     <div><asp:HyperLink ID="HyperLink12" runat="server" NavigateUrl="F12.aspx?ActionType=frm" Width="100%" CssClass="buttonMenu">F12&nbsp;&nbsp;&nbsp;&nbsp;การให้ความรู้ คำแนะนำและคำปรึกษากรณีท้องไม่พร้อม</asp:HyperLink></div>
 
  
     <div><asp:HyperLink ID="HyperLink3" runat="server" 
            NavigateUrl="F13.aspx?ActionType=frm" Width="100%" CssClass="buttonMenu">F13&nbsp;&nbsp;&nbsp;&nbsp;การใช้ยาปฏิชีวนะอย่างสมเหตุสมผล เพื่อป้องกันปัญหาการดื้อยา (ระบบทางเดินหายใจส่วนบน)
</asp:HyperLink></div>
 
  
     <div><asp:HyperLink ID="HyperLink13" runat="server" 
            NavigateUrl="F14.aspx?ActionType=frm" Width="100%" CssClass="buttonMenu">F14&nbsp;&nbsp;&nbsp;&nbsp;การใช้ยาปฏิชีวนะอย่างสมเหตุสมผล เพื่อป้องกันปัญหาการดื้อยา (โรคท้องร่วงเฉียบพลัน)
</asp:HyperLink></div>
    
  
     <div><asp:HyperLink ID="HyperLink15" runat="server" 
            NavigateUrl="F15.aspx?ActionType=frm" Width="100%" CssClass="buttonMenuB" 
            >F15&nbsp;&nbsp;&nbsp;&nbsp;แบบคัดกรองโรคหืดและโรคปอดอุดกั้นเรื้อรัง (F15)</asp:HyperLink></div>
 

     <div><asp:HyperLink ID="HyperLink14" runat="server" 
            NavigateUrl="F16.aspx?ActionType=frm" Width="100%" CssClass="buttonMenu" Visible="False" 
            >F16&nbsp;&nbsp;&nbsp;&nbsp;กิจกรรมการค้นหาปัญหาจากการใช้ยา ( DRP )(F16)</asp:HyperLink></div>
 

     <div><asp:HyperLink ID="HyperLink16" runat="server" 
            NavigateUrl="F17.aspx?ActionType=frm" Width="100%" CssClass="buttonMenuB" 
            >F17&nbsp;&nbsp;&nbsp;&nbsp;แบบประเมินความเสี่ยงเบื้องต้นโรคต่อการเป็นหลอดเลือดสมอง (F17)</asp:HyperLink></div>
 


 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
</asp:Panel>
    
  <asp:Panel ID="pnHomeVisit" runat="server">       
 <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-users"></i>

              <h3 class="box-title">MTM & Home Visit</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
                <div>
  <asp:HyperLink ID="HyperLink17" runat="server" CssClass="buttonMenuC" 
            NavigateUrl="MTM.aspx?ActionType=frm&t=new" Width="100%">&nbsp;&nbsp;&nbsp;&nbsp;การดูแลเรื่องการใช้ยา  (MTM / Home visit)</asp:HyperLink>
                </div>
                
               
               

 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
  </asp:Panel>
          
            <asp:Panel ID="pnProjectA" runat="server">
   <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-stop-circle"></i>

              <h3 class="box-title">โครงการหยุดสูบบุหรี่ ฯ</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
    
     <div>
    <asp:HyperLink ID="SmokeLink1" runat="server" CssClass="buttonMenuA" 
            NavigateUrl="A1-A4.aspx?ActionType=frm&t=new" Width="100%">A1-A4&nbsp;&nbsp;&nbsp;&nbsp;กิจกรรมปรับเปลี่ยนพฤติกรรมผู้เสพยาสูบโดยเภสัชกรชุมชน</asp:HyperLink>
    </div>
  
     <div>
    <asp:HyperLink ID="SmokeLink3" runat="server" CssClass="buttonMenuA" 
            NavigateUrl="A5.aspx?ActionType=frm&t=new" Width="100%">A5&nbsp;&nbsp;&nbsp;&nbsp;ติดตามผลกิจกรรมปรับเปลี่ยนพฤติกรรมผู้เสพยาสูบโดยเภสัชกรชุมชน</asp:HyperLink>
    </div>  
 



 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>     
    
</asp:Panel>
    

    </section>
</asp:Content>
