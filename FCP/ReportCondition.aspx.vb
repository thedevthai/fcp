﻿Public Class ReportCondition
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlType As New ServiceTypeController
    Dim objLct As New LocationInfo
    Dim ctlLct As New LocationController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim m, y As Integer
            y = Year(Date.Now)
            m = Month(Date.Now)
            If y < 2500 Then
                y = y + 543
            End If

            txtStartDate.Text = "01/" & m & "/" & y 'Date.Now.AddDays(-30).ToString("dd/MM/yyyy", DateFormat_TH)
            txtEndDate.Text = Date.DaysInMonth(y, m) & "/" & m & "/" & y  'Date.Now.ToString("dd/MM/yyyy", DateFormat_TH)

            'txtStartDate.Text = Date.Now.AddDays(-30).ToString("dd/MM/yyyy", DateFormat_EN)
            'txtEndDate.Text = Date.Now.ToString("dd/MM/yyyy", DateFormat_EN)

            LoadProjectToDDL()
            Select Case Request("ItemType")
                Case "fnc"
                    lblReportHeader.Text = "รายงานสรุปจำนวนกิจกรรม"
                    lblType.Visible = False
                    ddlType.Visible = False
                    cmdExcel.Visible = True
                    lblProject.Visible = False
                    ddlProject.Visible = False

                    lblLocation.Visible = False
                    ddlLocation.Visible = False
                    txtFindLocation.Visible = False
                    lnkFindLocation.Visible = False
                    imgArrowLocation.Visible = False
                Case "cus"

                    lblLocation.Visible = True
                    ddlLocation.Visible = True
                    txtFindLocation.Visible = True
                    lnkFindLocation.Visible = True
                    imgArrowLocation.Visible = True

                    LoadLocationToDDL()
                    lblReportHeader.Text = "รายงานรายชื่อผู้เข้ารับบริการแยกตามกิจกรรม"
                    lblType.Visible = True
                    ddlType.Visible = True
                    cmdExcel.Visible = True
                    lblProject.Visible = True
                    ddlProject.Visible = True
                    LoadActivityTypeToDDL()
            End Select

            If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
                lblLocation.Visible = False
                ddlLocation.Visible = False
                txtFindLocation.Visible = False
                lnkFindLocation.Visible = False
                imgArrowLocation.Visible = False


                'dt = ctlU.User_GetProjectID( Request.Cookies("UserID").Value)
                'If dt.Rows.Count > 1 Then
                '    lblProject.Visible = True
                '    ddlProject.Visible = True
                'ElseIf dt.Rows.Count = 1 Then
                '    lblProject.Visible = False
                '    ddlProject.Visible = False
                '    Session("projectid") = DBNull2Zero(dt.Rows(0)(0))
                'Else
                '    lblProject.Visible = False
                '    ddlProject.Visible = False
                'End If

                If Request.Cookies("ProjectF").Value = True And Request.Cookies("ProjectA").Value = True Then
                    lblProject.Visible = True
                    ddlProject.Visible = True
                Else
                    lblProject.Visible = False
                    ddlProject.Visible = False
                End If
            Else
                lblProject.Visible = True
                ddlProject.Visible = True
            End If

        End If
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess Or Convert.ToInt32(Request.Cookies("RoleID").Value) = isProjectManager Then
            ddlProject.SelectedValue = Request.Cookies("PRJMNG").Value
            ddlProject.Enabled = False
        End If



        'If Request("ItemType") = "fnc" Then
        '    cmdPrint.Attributes.Add("onClick", "window.open('" + ResolveUrl("rptFinanceSummarize.aspx?b=" & txtStartDate.Text & "&e=" & txtEndDate.Text & "', 'windowname', 'width=800,height=600,scrollbars=yes')"))
        '    cmdExcel.Attributes.Add("onClick", "window.open('" + ResolveUrl("rptFinanceSummarize.aspx?b=" & txtStartDate.Text & "&e=" & txtEndDate.Text & "&ex=1', 'windowname', 'width=800,height=600,scrollbars=yes')"))
        'Else
        '    cmdPrint.Attributes.Add("onClick", "window.open('" + ResolveUrl("ReportViewer.aspx?t=" & ddlType.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text & "', 'windowname', 'width=800,height=600,scrollbars=yes')"))
        'End If


    End Sub
    Private Sub LoadProjectToDDL()
        Dim ctlPj As New ProjectController
        dt = ctlPj.Project_GetAll
        If dt.Rows.Count > 0 Then
            With ddlProject
                .DataSource = dt
                .DataTextField = "Description"
                .DataValueField = "ProjectID"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadLocationToDDL()
        If txtFindLocation.Text = "" Then
            dt = ctlLct.Location_Get
        Else
            dt = ctlLct.Location_GetBySearch("0", txtFindLocation.Text)
        End If

        ddlLocation.Items.Clear()

        If dt.Rows.Count > 0 Then
            With ddlLocation
                .Visible = True
                .Items.Add("---ทั้งหมด---")
                .Items(0).Value = "0"
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)(objLct.tblField(objLct.fldPos.f01_LocationName).fldName))
                    .Items(i + 1).Value = dt.Rows(i)(objLct.tblField(objLct.fldPos.f00_LocationID).fldName)
                Next
                .SelectedIndex = 0
            End With
        End If

    End Sub

    Private Sub LoadActivityTypeToDDL()
        Dim n As Integer = 1
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            dt = ctlType.ServiceType_GetByLocationID(Request.Cookies("LocationID").Value)
        ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isProjectManager Then
            dt = ctlType.ServiceType_GetByProjectID(Request.Cookies("PRJMNG").Value)
        Else
            dt = ctlType.ServiceType_GetAll()
        End If
        If dt.Rows.Count > 0 Then
            ddlType.Items.Clear()
            ddlType.Items.Add("---ทั้งหมด---")
            ddlType.Items(0).Value = "0"
            For i = 0 To dt.Rows.Count - 1
                If dt.Rows(i)("ServiceTypeID") <> "F02" And dt.Rows(i)("ServiceTypeID") <> "F03" Then
                    With ddlType
                        .Items.Add("" & dt.Rows(i)("ServiceTypeID") & " : " & dt.Rows(i)("ServiceName"))
                        .Items(n).Value = dt.Rows(i)("ServiceTypeID")
                        n = n + 1
                    End With
                End If
            Next
        End If
        dt = Nothing
    End Sub

    Protected Sub cmdPrint_Click(sender As Object, e As EventArgs) Handles cmdPrint.Click
        ' Response.Redirect("rptFinanceSummarize.aspx?b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
        FagRPT = ""

        ' Dim fRptView As New ReportViewer
        Dim dStart As String = ConvertStrDate2DBString(txtStartDate.Text)
        Dim dEnd As String = ConvertStrDate2DBString(txtStartDate.Text)
        lblAlert.Text = ""
        Dim LID As String = "0"
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            LID = Request.Cookies("LocationID").Value
        Else
            LID = ddlLocation.SelectedValue
            If ddlProject.SelectedIndex = 0 Then
                lblAlert.Text = "ท่านยังไม่ได้เลือกโครงการ"
                Exit Sub
            End If
        End If

        Reportskey = ""
        If Request("ItemType") = "fnc" Then
            If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
                Response.Redirect("rptFinanceCustomerSummarize.aspx?pj=" & ddlProject.SelectedValue & "&lid=" & LID & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
            Else

                Response.Redirect("rptFinanceSummarize.aspx?pj=" & ddlProject.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
            End If
        Else
            Response.Redirect("rptCustomerByActivity.aspx?pj=" & ddlProject.SelectedValue & "&lid=" & LID & "&t=" & ddlType.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
        End If

    End Sub
    Dim ctlU As New UserController
    Protected Sub cmdExcel_Click(sender As Object, e As EventArgs) Handles cmdExcel.Click

        Dim LID As String = "0"
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            LID = Request.Cookies("LocationID").Value
        Else
            LID = ddlLocation.SelectedValue
        End If
        Reportskey = "XLS"
        If Request("ItemType") = "fnc" Then
            If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
                ReportsName = "rptFinanceCustomerSummarize"
                Response.Redirect("rptFinanceCustomerSummarize.aspx?ex=1&pj=" & ddlProject.SelectedValue & "&lid=" & LID & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
            Else
                ReportsName = "rptFinanceSummarize"
                Response.Redirect("rptFinanceSummarize.aspx?pj=" & ddlProject.SelectedValue & "&ex=1&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
            End If

        Else
            ReportsName = "rptCustomerByActivity"
            Response.Redirect("rptCustomerByActivity.aspx?pj=" & ddlProject.SelectedValue & "&ex=1&lid=" & LID & "&t=" & ddlType.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
        End If

    End Sub

    Protected Sub lnkFindLocation_Click(sender As Object, e As EventArgs) Handles lnkFindLocation.Click
        LoadLocationToDDL()
    End Sub
End Class