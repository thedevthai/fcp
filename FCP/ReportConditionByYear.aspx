﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="ReportConditionByYear.aspx.vb" Inherits=".ReportConditionByYear" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">    
       </asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1><asp:Label ID="lblReportName" runat="server" Text="รายงานสรุปจำนวนผู้รับบริการ"></asp:Label>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
  
    
    <table width="99%" border="0" align="left" cellpadding="0" cellspacing="2">
<tr>
          <td align="center" valign="top">&nbsp;</td>
      </tr>
       
        <tr>
            <td align="center" valign="top">
             
            
<table border="0" cellPadding="2" cellSpacing="2">
                                                <tr>
                                                    <td align="left" class="texttopic">ปี :</td>
                                                    <td width="500" align="left" class="texttopic">
                                                        <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" Width="100px" CssClass="OptionControl">
                                                            <asp:ListItem Selected="True" Value="0">ทั้งหมด</asp:ListItem>
                                                            <asp:ListItem>2557</asp:ListItem>
                                                          <asp:ListItem>2558</asp:ListItem>
                                                          <asp:ListItem>2559</asp:ListItem>
                                                          <asp:ListItem>2560</asp:ListItem>
                                                          <asp:ListItem>2561</asp:ListItem>
                                                          <asp:ListItem>2562</asp:ListItem>
                                                          <asp:ListItem>2563</asp:ListItem>
                                                          <asp:ListItem>2564</asp:ListItem>
                                                           <asp:ListItem>2565</asp:ListItem>
                                                           <asp:ListItem>2566</asp:ListItem>
                                                        </asp:DropDownList>                                                    </td>
                                                </tr>
                                                <tr>
                                                  <td align="left" class="texttopic">&nbsp;</td>
                                                  <td align="left" class="texttopic">
                                                      <asp:Button ID="cmdView" runat="server" CssClass="buttonFind" Text="ดูรายงาน" />
                                                    </td>
                                                </tr>
  </table>      </td>
      </tr>
      
        <tr>
          <td align="center" valign="top">
              &nbsp;</td>
      </tr>
        <tr>
          <td align="center" valign="top">
              &nbsp;</td>
        </tr>
    </table>
    </section>
</asp:Content>
