﻿Imports System.Net
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
Imports System.IO
Imports System.Text
Public Class ResultCumulative
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then

        End If
        If Not IsPostBack Then
            LoadReport()
        End If
    End Sub
    Private Sub LoadReport()

        'System.Threading.Thread.Sleep(1000)
        'UpdateProgress1.Visible = True


        Dim credential As New ReportServerCredentials
        ReportViewer1.Reset()
        ReportViewer1.ServerReport.ReportServerCredentials = credential
        ReportViewer1.ServerReport.ReportServerUrl = credential.ReportServerUrl
        ReportViewer1.ShowPrintButton = True

        'Me.ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout)

        'System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("th-TH")
        Dim xParam As New List(Of ReportParameter)

        'Select Case FagRPT
        '    Case "LocationGroup" 
        ReportViewer1.ServerReport.ReportPath = credential.ReportPath("LabResultCumulative")
        xParam.Add(New ReportParameter("PatientID", Session("patientid").ToString()))
        Reportskey = "PDF"

        'End Select

        ReportViewer1.ServerReport.SetParameters(xParam)

        If Reportskey <> "XLS" Then
            ' Variables
            Dim warnings As Warning()
            Dim streamIds As String()
            Dim mimeType As String = String.Empty
            Dim encoding As String = String.Empty
            Dim extension As String = String.Empty

            ' Setup the report viewer object and get the array of bytes

            Dim bytes As Byte() = ReportViewer1.ServerReport.Render("PDF", Nothing, mimeType, encoding, extension, streamIds, Nothing)

            ' Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
            Response.Buffer = True
            Response.Clear()
            Response.ContentType = mimeType
            'Response.AddHeader("content-disposition", Convert.ToString("attachment; filename=C:\ttt.pdf"))
            Response.BinaryWrite(bytes)
            ' create the file
            'Response.Flush()
            ' send it to the client to download
        Else
            ' Variables
            Dim warnings As Warning()
            Dim streamIds As String()
            Dim mimeType As String = String.Empty
            Dim encoding As String = String.Empty
            Dim extension As String = String.Empty


            ' Setup the report viewer object and get the array of bytes

            Dim bytes As Byte() = ReportViewer1.ServerReport.Render("EXCEL", Nothing, mimeType, encoding, extension, streamIds, Nothing)

            ' Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
            Response.Buffer = True
            Response.Clear()
            Response.ContentType = mimeType
            Response.AddHeader("content-disposition", Convert.ToString("attachment; filename=CPA_" & FagRPT & "_" & Request("y") & "." & extension))
            Response.BinaryWrite(bytes)
            ' create the file
            Response.Flush()
            ' send it to the client to download
        End If

    End Sub

End Class