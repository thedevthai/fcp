﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports Newtonsoft.Json

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="fcpproject.com")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class cpaservice
    Inherits System.Web.Services.WebService
    Dim ctlP As New PatientController
    Dim dtP As New DataTable
    <WebMethod()>
    Public Function getPageInfo(TimeStamp As String) As String
        dtP = ctlP.PatientInfo_GetPageInfo(TimeStamp)
        Dim json = JsonConvert.SerializeObject(dtP, Formatting.Indented)
        Return json
    End Function

    <WebMethod()>
    Public Function getPatientInfo(Page As Integer, TimeStamp As String) As String
        dtP = ctlP.PatientInfo_Get(Page, TimeStamp)
        Dim json = JsonConvert.SerializeObject(dtP, Formatting.Indented)
        Return json
    End Function
    <WebMethod()>
    Public Function getServiceCenterAll() As String
        dtP = ctlP.Location_GetAll()
        Dim json = JsonConvert.SerializeObject(dtP, Formatting.Indented)
        Return json
    End Function

    <WebMethod()>
    Public Function getServiceCenter(old_db_group_id As String) As String
        dtP = ctlP.Location_GetByID(old_db_group_id)
        Dim json = JsonConvert.SerializeObject(dtP, Formatting.Indented)
        Return json
    End Function


End Class