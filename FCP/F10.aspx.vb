﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO



Public Class F10
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlLct As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlOrder As New OrderController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then
            If Request("acttype") = "view" Then
                cmdSave.Visible = False
                cmdClear.Visible = False
            Else
                cmdSave.Visible = True
                cmdClear.Visible = True
            End If
            txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)

            LoadFormData()
             
            LoadPharmacist(Request.Cookies("LocationID").Value)
        End If

        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        ''txtCardID.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")


    End Sub
   Private Sub LoadPharmacist(LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With
            End If
        End If
        dtP = Nothing
    End Sub

    Private Sub LoadLocationData(LocationID As String)
        dt = ctlLct.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblLocationID.Text = .Item("LocationID")
                lblLocationName.Text = .Item("LocationName")
                lblLocationProvince.Text = .Item("ProvinceName")
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadFormData()
        Dim pYear As Integer
        If Not Request("fid") Is Nothing Then
            dt = ctlOrder.GetOrder_ByID(StrNull2Zero(Request("fid")))
        Else
            If Not Request("yid") Is Nothing Then
                pYear = CInt(Request("yid"))
            Else
                pYear = Year(ctlLct.GET_DATE_SERVER)
                If pYear < 2500 Then
                    pYear = pYear + 543
                End If

            End If
            dt = ctlOrder.LocationOrder_ByYear(pYear, FORM_TYPE_ID_F10, Request.Cookies("LocationID").Value, Session("patientid"))
        End If
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)

                lblID.Text = .Item("itemID")
                Session("patientid") = DBNull2Lng(.Item("PatientID"))
                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                txtTime.Text = String.Concat(.Item("ServiceTime"))
                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))
                optFrom.SelectedValue = DBNull2Str(.Item("PatientFrom"))
                'Dim sName(1) As String
                'sName(0) = ""
                'sName(1) = ""
                'sName = Split(DBNull2Str(.Item("CustName")), " ")
                'txtFName.Text = sName(0)
                'If sName.Length > 1 Then
                '    txtLName.Text = sName(1)
                'End If


                'optGender.SelectedValue = DBNull2Str(.Item("Gender"))
                'txtAges.Text = DBNull2Str(.Item("Ages"))
                'txtCardID.Text = DBNull2Str(.Item("CardID"))
                'txtBirthDate.Text = DisplayStr2ShortDateTH(DBNull2Str(.Item("BirthDate")))
                'txtTelephone.Text = DBNull2Str(.Item("Telephone"))
                'txtMobile.Text = DBNull2Str(.Item("Mobile"))
                'optAddressType.SelectedValue = DBNull2Str(.Item("AddressType"))
                'txtAddress.Text = DBNull2Str(.Item("AddressNo"))
                'txtRoad.Text = DBNull2Str(.Item("Road"))
                'txtDistrict.Text = DBNull2Str(.Item("District"))
                'txtCity.Text = DBNull2Str(.Item("City"))
                'ddlProvince.SelectedValue = DBNull2Str(.Item("ProvinceID"))
                'optClaim.SelectedValue = DBNull2Str(.Item("MainClaim"))
                chkClose.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("Status")))

                chkProblem1.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem1")))
                chkProblem2.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem2")))
                chkProblem3.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem3")))
                chkProblem4.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem4")))
                chkProblem5.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem5")))
                chkProblem6.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem6")))
                chkProblem7.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem7")))
                chkProblemOther.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblemOther")))
                txtProblemOther.Text = DBNull2Str(.Item("ProblemRemark"))
                chkEdu1.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate1")))
                chkEdu2.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate2")))
                chkEdu3.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate3")))
                chkEdu4.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate4")))
                chkEdu5.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate5")))
                chkEdu6.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate6")))
                chkEdu7.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate7")))
                chkEdu8.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate8")))
                chkEdu9.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate9")))
                chkEduOther.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducateOther")))
                txtEduOther.Text = DBNull2Str(.Item("EducateRemark"))

                chkNHSO1.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO1")))
                chkNHSO2.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO2")))
                chkNHSO3.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO3")))
                chkNHSO4.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO4")))
                chkNHSO5.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO5")))
                chkNHSO6.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO6")))
                chkNHSO7.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO7")))
                chkNHSO8.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO8")))

                optFollow.SelectedValue = DBNull2Str(.Item("isFollow"))
                txtHospitalName.Text = DBNull2Str(.Item("HospitalName"))
                txtReason.Text = DBNull2Str(.Item("Remark"))

                Dim vct() As String

                If DBNull2Str(.Item("vct_Follow1")) <> "" Then
                    vct = Split(DBNull2Str(.Item("vct_Follow1")), "|")
                    chkFollow1.Checked = False
                    chkFollow2.Checked = False
                    chkFollow3.Checked = False
                    For i = 0 To vct.Length - 1
                        Select Case vct(i)
                            Case "โทร 3 วันหลังส่งต่อ"
                                chkFollow1.Checked = True
                            Case "โทร 7 วันหลังส่งต่อ"
                                chkFollow2.Checked = True
                            Case "โทร 14 วันหลังส่งต่อ"
                                chkFollow3.Checked = True
                        End Select
                    Next
                End If

                If DBNull2Str(.Item("vct_Follow2")) <> "" Then
                    vct = Split(DBNull2Str(.Item("vct_Follow2")), "|")
                    chkFeedBack1.Checked = False
                    chkFeedBack2.Checked = False
                    For i = 0 To vct.Length - 1
                        Select Case vct(i)
                            Case "1"
                                chkFeedBack1.Checked = True
                            Case "2"
                                chkFeedBack2.Checked = True
                        End Select
                    Next
                End If

                txtFeedbaclDate.Text = DBNull2Str(.Item("vct_FollowDate"))
                If DBNull2Zero(.Item("Status")) >= 3 Then
                    cmdSave.Visible = False
                    cmdClear.Visible = False
                Else
                    If Request.Cookies("LocationID").Value <> String.Concat(.Item("LocationID")) Then
                        LoadLocationData(String.Concat(.Item("LocationID")))
                        If Request("t") = "edit" Then
                            cmdSave.Visible = True
                            cmdClear.Visible = True
                        Else
                            cmdSave.Visible = False
                            cmdClear.Visible = False
                        End If
                    Else
                        LoadLocationData(Request.Cookies("LocationID").Value)
                    End If
                End If


            End With
        Else
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing
    End Sub
    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click

        lblID.Text = ""
        txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)
        txtTime.Text = ""
        ddlPerson.SelectedIndex = 0
        'txtFName.Text = ""
        'optGender.SelectedIndex = 0
        'txtAges.Text = ""
        'txtCardID.Text = ""
        'txtBirthDate.Text = ""
        'txtTelephone.Text = ""
        'txtMobile.Text = ""
        'optAddressType.SelectedIndex = 0
        'txtAddress.Text = ""
        'txtRoad.Text = ""
        'txtDistrict.Text = ""
        'txtCity.Text = ""
        'ddlProvince.SelectedValue = "01"
        'optClaim.SelectedIndex = 0
        chkClose.Checked = False




    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If


        If StrNull2Zero(txtTime.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนระยะเวลาในการให้บริการก่อน');", True)
            Exit Sub
        End If


        Dim objuser As New UserController
        Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))
        'Dim BirthDate As String = ConvertStrDate2DBString(txtBirthDate.Text)
        'Dim CustName As String
        Dim vctFollow1, vctFollow2 As String

        If chkFollow1.Checked Then
            vctFollow1 = "โทร 3 วันหลังส่งต่อ|"
        End If
        If chkFollow2.Checked Then
            vctFollow1 &= "โทร 7 วันหลังส่งต่อ|"
        End If
        If chkFollow3.Checked Then
            vctFollow1 &= "โทร 14 วันหลังส่งต่อ|"
        End If

        If chkFeedBack1.Checked Then
            vctFollow2 = "1|"
        End If
        If chkFeedBack2.Checked Then
            vctFollow2 &= "2|"
        End If

        ' CustName = Trim(txtFName.Text) & " " & Trim(txtLName.Text)
        If lblID.Text = "" Then 'Add new

            If ctlOrder.Order_ChkDupCustomer(FORM_TYPE_ID_F10, Session("patientid"), StrNull2Zero(Left(Str(ServiceDate), 4))) = True Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ผู้รับบริการรายนี้เคยได้รับบริการในกิจกรรมนี้แล้ว');", True)
                Exit Sub
            End If

            ctlOrder.F10_Add(lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F10, Convert2Status(chkClose.Checked), Boolean2Decimal(chkProblem1.Checked), Boolean2Decimal(chkProblem2.Checked), Boolean2Decimal(chkProblem3.Checked), Boolean2Decimal(chkProblem4.Checked), Boolean2Decimal(chkProblem5.Checked), Boolean2Decimal(chkProblem6.Checked), Boolean2Decimal(chkProblem7.Checked), Boolean2Decimal(chkProblemOther.Checked), txtProblemOther.Text, Boolean2Decimal(chkEdu1.Checked), Boolean2Decimal(chkEdu2.Checked), Boolean2Decimal(chkEdu3.Checked), Boolean2Decimal(chkEdu4.Checked), Boolean2Decimal(chkEdu5.Checked), Boolean2Decimal(chkEdu6.Checked), Boolean2Decimal(chkEdu7.Checked), Boolean2Decimal(chkEdu8.Checked), Boolean2Decimal(chkEdu9.Checked), Boolean2Decimal(chkEduOther.Checked), txtEduOther.Text, optFollow.SelectedValue, txtHospitalName.Text, txtReason.Text, vctFollow1, vctFollow2, txtFeedbaclDate.Text, Request.Cookies("username").Value, CLng(ConvertDate2DBString(ctlLct.GET_DATE_SERVER)), Boolean2Decimal(chkNHSO1.Checked), Boolean2Decimal(chkNHSO2.Checked), Boolean2Decimal(chkNHSO3.Checked), Boolean2Decimal(chkNHSO4.Checked), Boolean2Decimal(chkNHSO5.Checked), Boolean2Decimal(chkNHSO6.Checked), Boolean2Decimal(chkNHSO7.Checked), Boolean2Decimal(chkNHSO8.Checked), Session("patientid"), Session("sex"), Session("age"), optFrom.SelectedValue.ToString())

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Service_Order", "บันทึกเพิ่มกิจกรรมความรู้และคำแนะนำเรื่องโรคที่เกิดจากการมีเพศสัมพันธ์   STI ,  Pre VCT (F10) :" & Session("patientname"), "F10")
        Else
            ctlOrder.F10_Update(StrNull2Zero(lblID.Text), lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F10, Convert2Status(chkClose.Checked), Boolean2Decimal(chkProblem1.Checked), Boolean2Decimal(chkProblem2.Checked), Boolean2Decimal(chkProblem3.Checked), Boolean2Decimal(chkProblem4.Checked), Boolean2Decimal(chkProblem5.Checked), Boolean2Decimal(chkProblem6.Checked), Boolean2Decimal(chkProblem7.Checked), Boolean2Decimal(chkProblemOther.Checked), txtProblemOther.Text, Boolean2Decimal(chkEdu1.Checked), Boolean2Decimal(chkEdu2.Checked), Boolean2Decimal(chkEdu3.Checked), Boolean2Decimal(chkEdu4.Checked), Boolean2Decimal(chkEdu5.Checked), Boolean2Decimal(chkEdu6.Checked), Boolean2Decimal(chkEdu7.Checked), Boolean2Decimal(chkEdu8.Checked), Boolean2Decimal(chkEdu9.Checked), Boolean2Decimal(chkEduOther.Checked), txtEduOther.Text, optFollow.SelectedValue, txtHospitalName.Text, txtReason.Text, vctFollow1, vctFollow2, txtFeedbaclDate.Text, Request.Cookies("username").Value, Boolean2Decimal(chkNHSO1.Checked), Boolean2Decimal(chkNHSO2.Checked), Boolean2Decimal(chkNHSO3.Checked), Boolean2Decimal(chkNHSO4.Checked), Boolean2Decimal(chkNHSO5.Checked), Boolean2Decimal(chkNHSO6.Checked), Boolean2Decimal(chkNHSO7.Checked), Boolean2Decimal(chkNHSO8.Checked), Session("patientid"), optFrom.SelectedValue.ToString())

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Service_Order", "บันทึกแก้ไขกิจกรรมความรู้และคำแนะนำเรื่องโรคที่เกิดจากการมีเพศสัมพันธ์   STI ,  Pre VCT (F10) :" & Session("patientname"), "F10")
        End If

        Response.Redirect("ResultPage.aspx?p=F10")

        ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย")
    End Sub


End Class