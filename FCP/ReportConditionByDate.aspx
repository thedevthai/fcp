﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="ReportConditionByDate.aspx.vb" Inherits=".ReportConditionByDate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1><asp:Label ID="lblReportHeader" runat="server" Text="รายงาน"></asp:Label>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
     
    <table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td height="350" valign="top">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td  valign="top"> <table border="0" align="center" cellpadding="0" cellspacing="3">
      <tr>
    <td>ตั้งแต่</td>
    <td>
        <asp:TextBox ID="txtStartDate" runat="server">        </asp:TextBox>
        </td>
    <td>ถึง</td>
    <td>
        <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
          </cc1:CalendarExtender>      </td>
  </tr>
    
</table></td>
    </tr>
    <tr>
      <td align="center">
        <asp:Button ID="cmdPrint" runat="server" Text="ดูรายงาน" CssClass="buttonSave" Width="100px" Visible="False" />
        &nbsp;<asp:Button ID="cmdExcel" runat="server" Text="ส่งออก Excel" CssClass="buttonFind" />
        </td>
    </tr>
  </table>
  </td> 
      </tr>
    </table>
    </section>
</asp:Content>
