﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Public Class F15
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlLct As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlOrder As New OrderController
    Dim ctlRv As New RelatedValuesController

    Dim ServiceDate As Long
    'Private _dateDiff As Object

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then
            If Request("acttype") = "view" Then
                cmdSave.Visible = False
                cmdClear.Visible = False
            Else
                cmdSave.Visible = True
                cmdClear.Visible = True
            End If

            LoadPharmacist(Request.Cookies("LocationID").Value)
            txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)

            LoadFormData()

            CalPEFR_Rate()
            CalLung_Score()
            CalCOPD_Score()
            CalAsthma_Score()


        End If

        txtPEFR_Value.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")
        txtWeight.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")
        txtHeigh.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")

        'If HiddenField1.Value <> "" Then
        '    CalBPAVG()
        'End If
        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        ''txtCardID.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")


    End Sub
    Private Sub LoadPharmacist(ByVal LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With
            End If
        End If
        dtP = Nothing
    End Sub

    Private Sub LoadLocationData(LocationID As String)
        dt = ctlLct.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblLocationID.Text = .Item("LocationID")
                lblLocationName.Text = .Item("LocationName")
                lblLocationProvince.Text = .Item("ProvinceName")
            End With
        End If
        dt = Nothing
    End Sub

    'Private Sub LoadProvinceToDDL()
    '    dt = ctlLct.LoadProvince
    '    If dt.Rows.Count > 0 Then
    '        With ddlProvince
    '            .Enabled = True
    '            .DataSource = dt
    '            .DataTextField = "ProvinceName"
    '            .DataValueField = "ProvinceID"
    '            .DataBind()
    '            .SelectedIndex = 1
    '        End With
    '    End If
    '    dt = Nothing
    'End Sub

    'Private Sub BindDayToDDL()
    '    Dim sD As String = ""
    '    For i = 1 To 31
    '        If i < 10 Then
    '            sD = "0" & i
    '        Else
    '            sD = i
    '        End If

    '        With ddlDay
    '            .Items.Add(i)
    '            .Items(i - 1).Value = sD
    '            .SelectedIndex = 0
    '        End With
    '    Next
    'End Sub
    'Private Sub BindMonthToDDL()
    '    Dim sD As String = ""

    '    For i = 1 To 12
    '        If i < 10 Then
    '            sD = "0" & i
    '        Else
    '            sD = i
    '        End If

    '        With ddlMonth
    '            .Items.Add(DisplayNumber2Month(i))
    '            .Items(i - 1).Value = sD
    '            .SelectedIndex = 0
    '        End With
    '    Next
    'End Sub

    'Private Sub BindYearToDDL()
    '    Dim ctlb As New ApplicationBaseClass
    '    Dim sDate As Date
    '    Dim y As Integer

    '    sDate = ctlb.GET_DATE_SERVER()

    '    If sDate.Year < 2300 Then
    '        y = (sDate.Year + 543)
    '    Else
    '        y = sDate.Year
    '    End If
    '    Dim i As Integer = 0
    '    Dim n As Integer = y - 17
    '    For i = 0 To 9
    '        With ddlYear
    '            .Items.Add(n)
    '            .Items(i).Value = n
    '            .SelectedIndex = 0
    '        End With
    '        n = n - 1
    '    Next
    'End Sub

    Private Sub LoadFormData()
        Dim pYear As Integer
        If Not Request("fid") Is Nothing Then
            dt = ctlOrder.GetOrder_ByID(StrNull2Zero(Request("fid")))
        Else
            If Not Request("yid") Is Nothing Then
                pYear = CInt(Request("yid"))
            Else
                pYear = Year(ctlLct.GET_DATE_SERVER)
                If pYear < 2500 Then
                    pYear = pYear + 543
                End If

            End If
            dt = ctlOrder.LocationOrder_ByYear(pYear, FORM_TYPE_ID_F15, Request.Cookies("LocationID").Value, Session("patientid"))
        End If

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Session("patientid") = DBNull2Lng(.Item("PatientID"))
                lblID.Text = .Item("itemID")
                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                txtTime.Text = String.Concat(.Item("ServiceTime"))

                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))

                'Dim sName(1) As String
                'sName(0) = ""
                'sName(1) = ""
                'sName = Split(DBNull2Str(.Item("CustName")), " ")
                'txtFName.Text = sName(0)
                'If sName.Length > 1 Then
                '    txtLName.Text = sName(1)
                'End If

                'optGender.SelectedValue = DBNull2Str(.Item("Gender"))
                'txtAges.Text = DBNull2Str(.Item("Ages"))
                'txtCardID.Text = DBNull2Str(.Item("CardID"))
                'txtBirthDate.Text = DisplayStr2ShortDateTH(DBNull2Str(.Item("BirthDate")))
                'txtTelephone.Text = DBNull2Str(.Item("Telephone"))
                'txtMobile.Text = DBNull2Str(.Item("Mobile"))
                'optAddressType.SelectedValue = DBNull2Str(.Item("AddressType"))
                'txtAddress.Text = DBNull2Str(.Item("AddressNo"))
                'txtRoad.Text = DBNull2Str(.Item("Road"))
                'txtDistrict.Text = DBNull2Str(.Item("District"))
                'txtCity.Text = DBNull2Str(.Item("City"))
                'ddlProvince.SelectedValue = DBNull2Str(.Item("ProvinceID"))
                'optClaim.SelectedValue = DBNull2Str(.Item("MainClaim"))
                chkStatus.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("Status")))

                chkNHSO1.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO1")))
                chkNHSO2.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO2")))
                chkNHSO3.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO3")))
                chkNHSO4.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO4")))
                chkNHSO5.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO5")))
                chkNHSO6.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO6")))
                chkNHSO7.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO7")))
                chkNHSO8.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO8")))

                If DBNull2Zero(.Item("Status")) >= 3 Then
                    cmdSave.Visible = False
                    cmdClear.Visible = False
                Else
                    If Request.Cookies("LocationID").Value <> String.Concat(.Item("LocationID")) Then
                        LoadLocationData(String.Concat(.Item("LocationID")))
                        If Request("t") = "edit" Then
                            cmdSave.Visible = True
                            cmdClear.Visible = True
                        Else
                            cmdSave.Visible = False
                            cmdClear.Visible = False
                        End If
                    Else
                        LoadLocationData(Request.Cookies("LocationID").Value)
                    End If
                End If


                LoadMetabolicRisk(StrNull2Zero(lblID.Text))
            End With

            Dim dtK As New DataTable

            dtK = ctlRv.RelatedValues_GetByCustomer(FORM_TYPE_ID_F15, StrNull2Zero(lblID.Text))

            If dtK.Rows.Count > 0 Then
                optLung_1Age.SelectedValue = DBNull2Str(dtK.Rows(0)("Topic1"))
                optLung_2Sound.SelectedValue = DBNull2Str(dtK.Rows(0)("Topic2"))
                optLung_3.SelectedValue = DBNull2Str(dtK.Rows(0)("Topic3"))
                optLung_4.SelectedValue = DBNull2Str(dtK.Rows(0)("Topic4"))
                optLung_5.SelectedValue = DBNull2Str(dtK.Rows(0)("Topic5"))
                txtLung_Score.Text = DBNull2Str(dtK.Rows(0)("TotalScore1"))

                optCOPD_1Age.SelectedValue = DBNull2Str(dtK.Rows(0)("Topic6"))
                optCOPD_2.SelectedValue = DBNull2Str(dtK.Rows(0)("Topic7"))
                optCOPD_3.SelectedValue = DBNull2Str(dtK.Rows(0)("Topic8"))
                optCOPD_4.SelectedValue = DBNull2Str(dtK.Rows(0)("Topic9"))
                optCOPD_5.SelectedValue = DBNull2Str(dtK.Rows(0)("Topic10"))
                txtCOPD_Score.Text = DBNull2Str(dtK.Rows(0)("TotalScore2"))

                optAsthma_Year.SelectedValue = DBNull2Str(dtK.Rows(0)("Topic11"))
                optAsthma_1.SelectedValue = DBNull2Str(dtK.Rows(0)("Topic12"))
                optAsthma_2.SelectedValue = DBNull2Str(dtK.Rows(0)("Topic13"))
                txtAsthma_Score.Text = DBNull2Str(dtK.Rows(0)("TotalScore3"))

                txtWeight.Text = DBNull2Str(dtK.Rows(0)("ScreenWeight"))
                txtHeigh.Text = DBNull2Str(dtK.Rows(0)("ScreenHeigh"))
                txtPEFR_Value.Text = DBNull2Str(dtK.Rows(0)("PEFR"))
                txtPEFR_Standard.Text = DBNull2Str(dtK.Rows(0)("PEFR_STD"))
                txtPEFR_Rate.Text = DBNull2Str(dtK.Rows(0)("PEFR_RATE"))

                optPEFR_Summary.SelectedValue = DBNull2Str(dtK.Rows(0)("PEFR_Status"))
                optCOPD_Sum.SelectedValue = DBNull2Str(dtK.Rows(0)("Risk_Status1"))
                optAsthma_Sum.SelectedValue = DBNull2Str(dtK.Rows(0)("Risk_Status2"))

            End If
            dtK = Nothing
        Else
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing

    End Sub


    Protected Sub cmdClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdClear.Click

        lblID.Text = ""
        txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)
        txtTime.Text = ""
        ddlPerson.SelectedIndex = 0
        'txtFName.Text = ""
        'txtLName.Text = ""
        'optGender.SelectedIndex = 0
        'txtAges.Text = ""
        'txtCardID.Text = ""
        'txtBirthDate.Text = ""
        'txtTelephone.Text = ""
        'txtMobile.Text = ""
        'optAddressType.SelectedIndex = 0
        'txtAddress.Text = ""
        'txtRoad.Text = ""
        'txtDistrict.Text = ""
        'txtCity.Text = ""
        'ddlProvince.SelectedValue = "01"
        'optClaim.SelectedIndex = 0


    End Sub

    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSave.Click

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        'If Trim(txtFName.Text) = "" Or Trim(txtLName.Text) = "" Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนชื่อ-นามสกุลผู้รับบริการก่อน');", True)
        '    Exit Sub
        'End If
        'If Trim(txtMobile.Text) = "" Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนเบอร์มือถือผู้รับบริการก่อน หากไม่มีให้ใส่เบอร์บ้านในช่องเบอร์มือถือได้")
        '    Exit Sub
        'End If
        If StrNull2Zero(txtTime.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนระยะเวลาในการให้บริการก่อน');", True)
            Exit Sub
        End If
        'If StrNull2Zero(txtAges.Text) = 0 Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนอายุผู้รับบริการก่อน');", True)
        '    Exit Sub
        'End If
        Dim objuser As New UserController
        Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))
        ' 'Dim BirthDate As String = ConvertStrDate2DBString(txtBirthDate.Text)
        'Dim CustName As String
        'CustName = Trim(txtFName.Text) & " " & Trim(txtLName.Text)


        Dim strP As String = ""
        If lblID.Text = "" Then 'Add new

            If ctlOrder.Order_ChkDupCustomer(FORM_TYPE_ID_F15, Session("patientid"), StrNull2Zero(Left(Str(ServiceDate), 4))) = True Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ผู้รับบริการรายนี้เคยได้รับบริการในกิจกรรมนี้แล้ว');", True)
                Exit Sub
            End If

            ctlOrder.ServiceOrder_Main_Add(lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F15, Convert2Status(chkStatus.Checked), 0, 0, Request.Cookies("username").Value, CLng(ConvertDate2DBString(ctlLct.GET_DATE_SERVER)), Boolean2Decimal(chkNHSO1.Checked), Boolean2Decimal(chkNHSO2.Checked), Boolean2Decimal(chkNHSO3.Checked), Boolean2Decimal(chkNHSO4.Checked), Boolean2Decimal(chkNHSO5.Checked), Boolean2Decimal(chkNHSO6.Checked), Boolean2Decimal(chkNHSO7.Checked), Boolean2Decimal(chkNHSO8.Checked), Session("patientid"), Session("sex"), Session("age"))

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Service_Order", "แบบคัดกรองโรคหืดและโรคปอดอุดกั้นเรื้อรัง (F15) :" & Session("patientname"), "F15")
        Else
            ctlOrder.ServiceOrder_Main_Update(StrNull2Zero(lblID.Text), lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F15, Convert2Status(chkStatus.Checked), 0, 0, Request.Cookies("username").Value, Boolean2Decimal(chkNHSO1.Checked), Boolean2Decimal(chkNHSO2.Checked), Boolean2Decimal(chkNHSO3.Checked), Boolean2Decimal(chkNHSO4.Checked), Boolean2Decimal(chkNHSO5.Checked), Boolean2Decimal(chkNHSO6.Checked), Boolean2Decimal(chkNHSO7.Checked), Boolean2Decimal(chkNHSO8.Checked), Session("patientid"))

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Service_Order", "แบบคัดกรองโรคหืดและโรคปอดอุดกั้นเรื้อรัง (F15) :" & Session("patientname"), "F15")
        End If

        Dim ServiceID As Integer = ctlOrder.ServiceOrder_GetID(lblLocationID.Text, FORM_TYPE_ID_F15, Session("patientid"), ServiceDate)

        ctlOrder.ServiceOrder_SaveRelatedValues(ServiceID, FORM_TYPE_ID_F15, "แบบคัดกรองโรคหืดและโรคปอดอุดกั้นเรื้อรัง", ServiceDate, StrNull2Zero(txtWeight.Text), StrNull2Zero(txtHeigh.Text), Str2Double(txtPEFR_Value.Text), Str2Double(txtPEFR_Standard.Text), Str2Double(txtPEFR_Rate.Text), Request.Cookies("username").Value, optLung_1Age.SelectedValue, optLung_2Sound.SelectedValue, optLung_3.SelectedValue, optLung_4.SelectedValue, optLung_5.SelectedValue, StrNull2Zero(txtLung_Score.Text), optCOPD_1Age.SelectedValue, optCOPD_2.SelectedValue, optCOPD_3.SelectedValue, optCOPD_4.SelectedValue, optCOPD_5.SelectedValue, StrNull2Zero(txtCOPD_Score.Text), optAsthma_Year.SelectedValue, optAsthma_1.SelectedValue, optAsthma_2.SelectedValue, StrNull2Zero(txtAsthma_Score.Text), Convert2Status(chkStatus.Checked), optPEFR_Summary.SelectedValue, optCOPD_Sum.SelectedValue, optAsthma_Sum.SelectedValue)

        Response.Redirect("ResultPage.aspx?p=F15")
    End Sub
    Private Sub CalPEFR_Rate()
        Dim PEF As Double
        Dim A As Integer = StrNull2Zero(Session("age"))
        Dim H As Double = StrNull2Zero(txtHeigh.Text)
        If H > 0 Then
            If StrNull2Zero(Session("age")) >= 15 Then
                If Session("sex") = "M" Then
                    PEF = (((((-16.859 + (0.307 * A)) + (0.141 * H)) - (0.0018 * (A * A))) - (0.001 * A * H)) * 60)
                Else
                    PEF = ((((((-31.355 + (0.162 * A)) + (0.391 * H)) - (0.00084 * (A * A))) - (0.00099 * (H * H))) - (0.00072 * A * H)) * 60)
                End If
            Else
                PEF = (StrNull2Zero(txtHeigh.Text) * 5) - 400
            End If

            txtPEFR_Standard.Text = Math.Round(PEF)
            txtPEFR_Rate.Text = Math.Round((StrNull2Zero(txtPEFR_Value.Text) / Math.Round(PEF)) * 100)

            If StrNull2Zero(txtPEFR_Rate.Text) < 70 Then
                optPEFR_Summary.SelectedValue = "L"
            Else
                optPEFR_Summary.SelectedValue = "G"
            End If
        End If
    End Sub

    Protected Sub txtScreenWeight_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtWeight.TextChanged
        'CalBMI()
        txtHeigh.Focus()
    End Sub

    Protected Sub txtScreenHeigh_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtHeigh.TextChanged
        CalPEFR_Rate()
        CalPEFR_Rate()
    End Sub

    'Private Sub CalAges()
    '    Try

    '        Session("patientages") = DateDiff(DateInterval.Year, CDate(txtBirthDate.Text), ctlLct.GET_DATE_SERVER.Date)


    '    Catch ex As Exception

    '    End Try
    '    Dim iAge As Integer = StrNull2Zero(txtAges.Text)

    'End Sub


    Private Sub LoadMetabolicRisk(pID As Integer)
        Dim dtM As New DataTable

        dtM = ctlOrder.MetabolicRisk_ByOrderID(pID)
        If dtM.Rows.Count > 0 Then
            With dtM.Rows(0)
                Session("age") = String.Concat(.Item("ScreenAge"))
                txtWeight.Text = String.Concat(.Item("ScreenWeight"))
                txtHeigh.Text = String.Concat(.Item("ScreenHeight"))
            End With
            CalPEFR_Rate()

        End If
        dtM = Nothing
    End Sub

    'Private Sub ClearData()
    '    txtAges.Text = 0
    '    txtCardID.Text = ""
    '    txtBirthDate.Text = ""
    '    txtTelephone.Text = ""
    '    txtMobile.Text = ""
    '    txtAddress.Text = ""
    '    txtRoad.Text = ""
    '    txtDistrict.Text = ""
    '    txtCity.Text = ""

    'End Sub

    Public Function GetNumbericValue(ByVal str As String) As Integer
        If str = "" Then
            Return 0
        Else
            Return StrNull2Zero(Right(str, 1))
        End If
    End Function

    Private Sub CalLung_Score()
        txtLung_Score.Text = GetNumbericValue(optLung_1Age.SelectedValue) + GetNumbericValue(optLung_2Sound.SelectedValue) + GetNumbericValue(optLung_3.SelectedValue) + GetNumbericValue(optLung_4.SelectedValue) + GetNumbericValue(optLung_5.SelectedValue)
    End Sub
    Private Sub CalCOPD_Score()
        txtCOPD_Score.Text = GetNumbericValue(optCOPD_1Age.SelectedValue) + GetNumbericValue(optCOPD_2.SelectedValue) + GetNumbericValue(optCOPD_3.SelectedValue) + GetNumbericValue(optCOPD_4.SelectedValue) + GetNumbericValue(optCOPD_5.SelectedValue)

        If CInt(txtCOPD_Score.Text) < 5 Then
            optCOPD_Sum.SelectedValue = "L"
        ElseIf CInt(txtCOPD_Score.Text) >= 5 Then
            optCOPD_Sum.SelectedValue = "H"
        Else
            optCOPD_Sum.SelectedValue = "S"
        End If
    End Sub
    Private Sub CalAsthma_Score()
        txtAsthma_Score.Text = GetNumbericValue(optAsthma_1.SelectedValue) + GetNumbericValue(optAsthma_2.SelectedValue)

        If CInt(txtAsthma_Score.Text) < 2 Then
            optAsthma_Sum.SelectedValue = "L"
        ElseIf CInt(txtCOPD_Score.Text) >= 2 Then
            optAsthma_Sum.SelectedValue = "H"
        Else
            optAsthma_Sum.SelectedValue = "S"
        End If

    End Sub

    Protected Sub txtPEFR_Value_TextChanged(sender As Object, e As EventArgs) Handles txtPEFR_Value.TextChanged
        CalPEFR_Rate()
        CalPEFR_Rate()
    End Sub

    Protected Sub optLung_1Age_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optLung_1Age.SelectedIndexChanged
        CalLung_Score()
    End Sub

    Protected Sub optLung_2Sound_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optLung_2Sound.SelectedIndexChanged
        CalLung_Score()
    End Sub

    Protected Sub optLung_3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optLung_3.SelectedIndexChanged
        CalLung_Score()
    End Sub

    Protected Sub optLung_4_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optLung_4.SelectedIndexChanged
        CalLung_Score()
    End Sub

    Protected Sub optLung_5_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optLung_5.SelectedIndexChanged
        CalLung_Score()
    End Sub

    Protected Sub optCOPD_1Age_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optCOPD_1Age.SelectedIndexChanged
        CalCOPD_Score()
    End Sub

    Protected Sub optCOPD_2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optCOPD_2.SelectedIndexChanged
        CalCOPD_Score()
    End Sub

    Protected Sub optCOPD_3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optCOPD_3.SelectedIndexChanged
        CalCOPD_Score()
    End Sub

    Protected Sub optCOPD_4_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optCOPD_4.SelectedIndexChanged
        CalCOPD_Score()
    End Sub

    Protected Sub optCOPD_5_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optCOPD_5.SelectedIndexChanged
        CalCOPD_Score()
    End Sub

    Protected Sub optAsthma_1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optAsthma_1.SelectedIndexChanged
        CalAsthma_Score()
    End Sub

    Protected Sub optAsthma_2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optAsthma_2.SelectedIndexChanged
        CalAsthma_Score()
    End Sub

    Protected Sub optAsthma_Year_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optAsthma_Year.SelectedIndexChanged
        If optAsthma_Year.SelectedIndex = 1 Then
            optAsthma_1.SelectedIndex = 1
            optAsthma_2.SelectedIndex = 1
        End If
    End Sub
End Class