﻿Public Class NewsAll
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim ctlNews As New NewsController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            ' ModalPopupExtender1.Hide()

            If Request("logout") = "YES" Then
                Session.Abandon()
                Request.Cookies("username").Value = Nothing
            End If

            LoadNewsToGrid()
            Dim ctlS As New SystemConfigController
            lblVersion.Text = ctlS.SystemConfig_GetByCode("Version")
        End If

    End Sub
    Private Sub LoadNewsToGrid()

        dt = ctlNews.News_GetByStatus

        With grdData
            .Visible = True
            .DataSource = dt
            .DataBind()

            For i = 0 To .Rows.Count - 1
                Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                Dim hlnkN As HyperLink = .Rows(i).Cells(1).FindControl("hlnkNews")
                hlnkN.Text = dt.Rows(i)("Title")

                Select Case dt.Rows(i)("NewsType")
                    Case "URL"
                        img1.ImageUrl = "images/www.png"
                        hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                    Case "UPL"
                        img1.ImageUrl = "images/pdf.png"
                        hlnkN.NavigateUrl = tmpUpload & "/" & String.Concat(dt.Rows(i)("LinkPath"))
                    Case "CON"
                        img1.ImageUrl = "images/comms.png"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    Case Else
                        img1.ImageUrl = "images/comms.png"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                End Select





                Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                img2.Visible = False
                If DateDiff(DateInterval.Day, CDate(dt.Rows(i)("NewsDate")), Today.Date) <= 5 Then
                    img2.Visible = True
                End If



                .Rows(i).Cells(2).Text = "<span class='Newsdate'>Update เมื่อ" & DisplayDateTH(dt.Rows(i)("NewsDate")) & "</span>"
            Next

        End With

    End Sub

    Private Sub grdData_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub
End Class
