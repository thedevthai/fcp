﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class ReportLocations
    
    '''<summary>
    '''ddlProj control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlProj As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''ddlType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlType As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''ddlProvince control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlProvince As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''cmdView control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdView As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''lblCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCount As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''grdData control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdData As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''cmdPrint control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdPrint As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''cmdExcel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdExcel As Global.System.Web.UI.WebControls.Button
End Class
