﻿
Public Class ReferReg
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim ctlR As New ReferController
    Dim acc As New UserController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then
            txtDate.Text = DisplayShortDateTH(ctlR.GET_DATE_SERVER)
        End If

    End Sub

    Private Sub ClearData()
        Me.txtDate.Text = ""
        txtDestination.Text = ""
        txtRemark.Text = ""
    End Sub



    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If txtDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุวันที่ Refer ให้ถูกต้อง');", True)
            Exit Sub
        End If

        If txtDestination.Text = "" Then
            txtDestination.Text = "TCDB"
        End If
        If txtRemark.Text = "" Then
            txtRemark.Text = "ฐานข้อมูลส่วนกลาง"
        End If
        Dim sLocationID As String = ""

        If Request.Cookies("LocationID").Value = "CPA" Then
            sLocationID = ctlR.Refer_GetLocation(Session("PatientID"))
        Else
            sLocationID = Request.Cookies("LocationID").Value
        End If


        If Not ctlR.Refer_CheckDup(Session("PatientID"), "S") Then

            ctlR.Refer_Add(txtDate.Text, "S", Session("PatientID"), txtDestination.Text, sLocationID, txtRemark.Text, Request.Cookies("username").Value)

            acc.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Refer", "Refer to TCDB:" & Session("PatientID") & "|" & txtDestination.Text, txtRemark.Text)

        Else
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ผู้รับบริการรายนี้ไ   ด้ทำการส่งต่อแล้ว');", True)
            Exit Sub

        End If

        Response.Redirect("PatientServiceList.aspx?r=y")
        'ClearData()
        'DisplayMessage(Me, "บันทึกข้อมูลการส่งต่อเรียบร้อย")

    End Sub
End Class

