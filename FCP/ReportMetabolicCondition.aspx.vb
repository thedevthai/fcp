﻿Public Class ReportMetabolicCondition
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlType As New ServiceTypeController
    Dim objLct As New LocationInfo
    Dim ctlLct As New LocationController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            LoadProvinceToDDL()

            Dim m, y As Integer
            y = Year(Date.Now)
            m = Month(Date.Now)
            If y < 2300 Then
                y = y + 543
            End If

            txtStartDate.Text = "01/" & m & "/" & y 'Date.Now.AddDays(-30).ToString("dd/MM/yyyy", DateFormat_TH)
            txtEndDate.Text = Date.DaysInMonth(y, m) & "/" & m & "/" & y  'Date.Now.ToString("dd/MM/yyyy", DateFormat_TH)


            lblCon1.Visible = False
            ddlBP.Visible = False
            ddlAge.Visible = False
            Select Case Request("r")
                Case "d" 'เบาหวาน
                    ReportsName = "rptMetabolicByDiabete"
                Case "f" 'อ้วน
                    ReportsName = "rptMetabolicByFat"
                    If Request("ItemType") <> "smz" Then
                        ddlAge.Visible = True
                        lblCon1.Visible = True
                        lblCon1.Text = "อายุ"
                    End If

                Case "b" 'ความดัน
                    ReportsName = "rptMetabolicByBlood"
                    If Request("ItemType") <> "smz" Then
                        lblCon1.Visible = True
                        ddlBP.Visible = True 
                        lblCon1.Text = "BP เฉลี่ย"
                    End If
                Case Else
                    ReportsName = "rptServiceOrder" & ConvertTimeToString(Now())
            End Select

        End If


    End Sub
    Private Sub LoadProvinceToDDL()
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            dt = ctlLct.Province_GetInGroup(Request.Cookies("RPTGRP").Value)
        Else
            dt = ctlLct.Province_GetInLocation
        End If

        ddlProvince.Items.Clear()
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .Items.Add("---ทั้งหมด---")
                If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
                    .Items(0).Value = Request.Cookies("RPTGRP").Value
                Else
                    .Items(0).Value = "0"
                End If

                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)("ProvinceName"))
                    .Items(i + 1).Value = dt.Rows(i)("ProvinceID")
                Next
                .SelectedIndex = 0

            End With
        End If
        dt = Nothing
    End Sub

    Protected Sub cmdPrint_Click(sender As Object, e As EventArgs) Handles cmdPrint.Click

        FagRPT = ""

        Dim dStart As String = ConvertStrDate2DBString(txtStartDate.Text)
        Dim dEnd As String = ConvertStrDate2DBString(txtStartDate.Text)


        Dim rptGRP As String = "ALL"

        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            rptGRP = Request.Cookies("RPTGRP").Value
        End If

        Select Case Request("ItemType")
            Case "smz"
                If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
                    Response.Redirect("reports/" & ReportsName & "RiskSummary.aspx?grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)

                Else
                    Response.Redirect("reports/" & ReportsName & "RiskSummary.aspx?grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)

                End If
            Case "cnt"
                Response.Redirect("reports/rptServiceCountSummaryByProvince.aspx?grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
            Case "soc"
                Response.Redirect("reports/rptServiceOfCustomer.aspx?grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)

            Case "nhso"
                Response.Redirect("reports/rptMetabolicByShopGroupSummary.aspx?grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
            Case "3d"
                FagRPT = "MetabolicRisk"
                Response.Redirect("ReportViewer.aspx?grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)

            Case Else
                Response.Redirect("reports/" & ReportsName & "RiskCustomer.aspx?grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text & "&bp=" & ddlBP.SelectedValue & "&ag=" & ddlAge.SelectedValue)

        End Select
    End Sub

    Protected Sub cmdExcel_Click(sender As Object, e As EventArgs) Handles cmdExcel.Click

        FagRPT = ""

        Dim dStart As String = ConvertStrDate2DBString(txtStartDate.Text)
        Dim dEnd As String = ConvertStrDate2DBString(txtStartDate.Text)


        Dim rptGRP As String = "ALL"

        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            rptGRP = Request.Cookies("RPTGRP").Value
        End If

        Select Case Request("ItemType")
            Case "smz"
                If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
                    Response.Redirect("reports/" & ReportsName & "RiskSummary.aspx?ex=1&grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)

                Else
                    Response.Redirect("reports/" & ReportsName & "RiskSummary.aspx?ex=1&grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)

                End If
            Case "cnt"
                Response.Redirect("reports/rptServiceCountSummaryByProvince.aspx?ex=1&grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
            Case "soc"
                Response.Redirect("reports/rptServiceOfCustomer.aspx?ex=1&grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
            Case "nhso"
                Response.Redirect("reports/rptMetabolicByShopGroupSummary.aspx?ex=1&grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
            Case "3d"
                FagRPT = "MetabolicRisk"
                Response.Redirect("ReportViewer.aspx?grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)

            Case Else
                Response.Redirect("reports/" & ReportsName & "RiskCustomer.aspx?ex=1&grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text & "&bp=" & ddlBP.SelectedValue & "&ag=" & ddlAge.SelectedValue)

        End Select


    End Sub

End Class