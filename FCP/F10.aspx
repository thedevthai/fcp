﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="F10.aspx.vb" Inherits=".F10" %>
<%@ Register src="ucHistoryYear.ascx" tagname="ucHistoryYear" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

<link rel="stylesheet" type="text/css" href="css/dc_table.css">
<link rel="stylesheet" type="text/css" href="css/rajchasistyles.css">
<link rel="stylesheet" type="text/css" href="css/uidialog.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>F10
        <small>กิจกรรมการให้ความรู้ คำแนะนำและคำปรึกษาเรื่องโรคที่เกิดจากการมีเพศสัมพันธ์ <br />
                          (Sexually Transmitted Infections =STI) และ Pre-Voluntary Counseling and 
                           Testing (Pre-VCT) ในกลุ่มเสี่ยงการติดเชื้อ HIV</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">   
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลกิจกรรม</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
                 
 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="10"> <uc1:ucHistoryYear ID="ucHistoryYear1" runat="server" /></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="OptionPanels">
      <tr>
        <td width="100" class="texttopic">รหัสร้าน</td>
        <td width="120" class="LocationName">
            <asp:Label ID="lblLocationID" runat="server"></asp:Label>
          </td>
        <td width="50" class="texttopic">ชื่อร้าน</td>
        <td width="300" class="LocationName">
            <asp:Label ID="lblLocationName" runat="server"></asp:Label>          </td>
        <td width="50" class="texttopic">จังหวัด</td>
        <td class="LocationName">
            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
   <tr>
    <td align="center"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">
      <tr>
        <td width="100">วันที่ให้บริการ</td>
        <td>
            <asp:TextBox ID="txtServiceDate" runat="server"></asp:TextBox>
(วว/ดด/ปปปป พ.ศ.) 
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="txtServiceDate" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="\d{2}/\d{2}/\d{4}"></asp:RegularExpressionValidator></td>
        <td width="100">รวมระยะเวลา<img src="images/star.png" width="10" height="10" /></td>
        <td>
            <asp:TextBox ID="txtTime" runat="server" Width="50px"></asp:TextBox>
&nbsp;นาที</td>
        <td>เภสัชกรผู้ให้บริการ</td>
        <td>
            <asp:DropDownList ID="ddlPerson" runat="server">            </asp:DropDownList>          </td>
      </tr>
    </table></td>
  </tr> 
   <tr>
    <td>
      <table width="100%" border="0" cellspacing="2" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
          <td width="100" align="right"><span class="NameEN">Ref.ID :</span></td>
          <td width="50"><span class="NameEN">
              <asp:Label ID="lblID" 
                  runat="server"></asp:Label></span>
            </td>
        </tr>
      </table></td>
  </tr>
     <tr>
    <td class="MenuSt">ผู้รับบริการมาจาก</td>
  </tr>
      <tr>
    <td>
        <asp:RadioButtonList ID="optFrom" runat="server" RepeatDirection="Horizontal">
            <asp:ListItem Selected="True">ประชาชนทั่วไป</asp:ListItem>
            <asp:ListItem>มาจาก P2H</asp:ListItem>
        </asp:RadioButtonList>
          </td>
  </tr>
  <tr>
    <td class="MenuSt">ข้อมูลการให้ความรู้และคำแนะนำปรึกษา</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="dc_table_s3">
      <thead>
        <tr>
          <th scope="col" width="50%" valign="top" class="texttopic">ปัญหาของผู้รับบริการ</th>
          <th scope="col" colspan="3" valign="top">การให้ความรู้และคำแนะนำปรึกษา</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th valign="top" class="texttopic" align="left">
           <asp:CheckBox ID="chkProblem1" runat="server" Font-Bold="False" Text="ติดเชื้อกลุ่มโรคที่เกิดจากการมีเพศสัมพันธ์" />    
              <br />
              <asp:CheckBox ID="chkProblem2" runat="server" Font-Bold="False" Text="มีเพศสัมพันธ์โดยไม่ได้สวมถุงยางอนามัย" />      
              <br />
              <asp:CheckBox ID="chkProblem3" runat="server" Font-Bold="False" Text="ขาดความรู้ความเข้าใจเกี่ยวกับโรคที่เกิดจากการมีเพศสัมพันธ์" />      
              <br />
              <asp:CheckBox ID="chkProblem4" runat="server" Font-Bold="False" Text="ขาดความรู้ความเข้าใจเกี่ยวกับช่องทางการติดต่อของโรคและการถ่ายทอดเชื้อ" />    
            <br />
            <asp:CheckBox ID="chkProblem5" runat="server" Font-Bold="False" Text="ขาดความรู้ความเข้าใจเกี่ยวกับอันตรายของโรคที่เกิดจากการมีเพศสัมพันธ์" />
            <br />
            <asp:CheckBox ID="chkProblem6" runat="server" Font-Bold="False" Text="ขาดความรู้ความเข้าใจเกี่ยวกับการใช้ถุงยางอนามัย" />
            <br />
            <asp:CheckBox ID="chkProblem7" runat="server" Font-Bold="False" Text=" มีเพศสัมพันธ์เพศเดียวกัน ( MSM )" />
            <br />
            <asp:CheckBox ID="chkProblemOther" runat="server" Font-Bold="False" Text="อื่นๆ" />
            &nbsp;
            <asp:TextBox ID="txtProblemOther" runat="server" Width="200px"></asp:TextBox>
          </th>
          <th colspan="3" valign="top" align="left">       
            <asp:CheckBox ID="chkEdu1" runat="server" Font-Bold="False" Text="วิธีรับประทานยา ที่ถูกต้อง" />    
              <br />
              <asp:CheckBox ID="chkEdu2" runat="server" Font-Bold="False" Text="อันตรายของโรคที่กำลังป่วยอยู่" />      
              <br />
              <asp:CheckBox ID="chkEdu3" runat="server" Font-Bold="False" Text="โรคแทรกซ้อน รวมทั้งโอกาสที่จะติดเชื้อHIV 
       และโรคติดต่อทางเพศสัมพันธ์อื่นๆ " />      
              <br />
              <asp:CheckBox ID="chkEdu4" runat="server" Font-Bold="False" Text="ช่องทางการติดต่อของโรคและการถ่ายทอดเชื้อ ไปสู่คู่เพศสัมพันธ์ และทารกในครรภ์" />      
              <br />
              <asp:CheckBox ID="chkEdu5" runat="server" Font-Bold="False" Text="ความจำเป็นของการรักษาอย่างครบถ้วน รวมถึงการรักษา คู่นอน" />      
              <br />
              <asp:CheckBox ID="chkEdu6" runat="server" Font-Bold="False" Text="งดมีเพศสัมพันธ์ระหว่างการรักษา  หากงดไม่ได้
       ควรใช้ถุงยางอนามัยทุกครั้ง และทุกช่องทาง" />      
              <br />
              <asp:CheckBox ID="chkEdu7" runat="server" Font-Bold="False" Text="งดเครื่องดื่มที่มีแอลกอฮอล์ " />      
              <br />
              <asp:CheckBox ID="chkEdu8" runat="server" Font-Bold="False" Text=" มีคู่เพศสัมพันธ์คนเดียว งดเว้นการเปลี่ยนคู่" />      
              <br />
              <asp:CheckBox ID="chkEdu9" runat="server" Font-Bold="False" Text="ใช้ถุงยางอนามัยทุกครั้งในทุกช่องทางที่ใช้ในการมีเพศสัมพันธ์  ไม่ว่าคู่เพศสัมพันธ์จะเป็นผู้ให้บริการทางเพศหรือไม่ใช่ก็ตาม  " />                    
              <br />
              <asp:CheckBox ID="chkEduOther" runat="server" Font-Bold="False" Text="อื่นๆ" />      
            &nbsp;
            <asp:TextBox 
                 ID="txtEduOther" runat="server" Width="200px"></asp:TextBox>
            <br />
          </th>
        </tr>
      </tbody>
    </table></td>
  </tr>
 
  <tr>
    <td class="MenuSt">เชิญชวนตรวจ Pre-VCT บริการส่งต่อหน่วยบริการ และติดตาม</td>
  </tr>
  <tr>
    <td align="left"><table border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td rowspan="2">
            <asp:RadioButtonList ID="optFollow" runat="server">        
             <asp:ListItem Selected="True" Value="2">สามารถไปรับบริการได้ทุก สถานพยาบาลในระบบหลักประกันสุขภาพแห่งชาติ</asp:ListItem>
                <asp:ListItem Value="1">จะไปใช้บริการ สถานพยาบาล</asp:ListItem>
                <asp:ListItem Value="0">ไม่ไปใช้บริการ</asp:ListItem>
                
         </asp:RadioButtonList>
          </td>
        <td><asp:Label ID="lblHospital" runat="server" Text="สถานพยาบาลชื่อ"></asp:Label></td>
        <td>
            <asp:TextBox ID="txtHospitalName" runat="server" Width="300px"></asp:TextBox>
          </td>
      </tr>
      <tr>
        <td><asp:Label ID="lblReason" runat="server" Text="เพราะ"></asp:Label>
          </td>
        <td>
            <asp:TextBox ID="txtReason" runat="server" Width="300px"></asp:TextBox>
          </td>
      </tr>
    </table></td>
  </tr>
   <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td width="50%" class="text11b_pink" align="left"> การติดตามเพื่อกระตุ้นไปรับบริการ</td>
        <td class="text11b_pink" align="left">การติดตามเพื่อขอทราบการไปรับบริการ</td>
      </tr>
      <tr>
        <td align="left">
        <asp:CheckBox ID="chkFollow1" runat="server" Font-Bold="False" Text="โทร 3 วันหลังส่งต่อ " />    
              <br />
              <asp:CheckBox ID="chkFollow2" runat="server" Font-Bold="False" Text="โทร 7 วันหลังส่งต่อ" />      
              <br />
              <asp:CheckBox ID="chkFollow3" runat="server" Font-Bold="False" Text="โทร 14 วันหลังส่งต่อ" />      
              
        </td>
        <td align="left">
         <asp:CheckBox ID="chkFeedBack1" runat="server" Font-Bold="False" Text="ผู้รับบริการแจ้งกลับ" />
         &nbsp;วันที่ 
            <asp:TextBox ID="txtFeedbaclDate" runat="server"></asp:TextBox>
         <br />
              <asp:CheckBox ID="chkFeedBack2" runat="server" Font-Bold="False" Text="โทร ติดต่อหน่วยบริการ" />      
              </td>
      </tr>
    </table></td>
  </tr>

  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="dc_table_s3">
     <thead>
      <tr>
        <th scope="col" width="50%" valign="top" class="texttopic">แจ้งสิทธิประโยช์  สปสช.</th>
      </tr>
       </thead>
      <tr>
        <td align="left">
              <table border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td><asp:CheckBox ID="chkNHSO1" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวจคัดกรองความเสี่ยงDM/HT" /></td>
                  <td><asp:CheckBox ID="chkNHSO2" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวจคัดกรองความเสี่ยงมะเร็งปากมดลูก/มะเร็งเต้านม" /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO3" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับวัคซีนเสริมสร้างภูมิคุ้มกันโรค" /></td>
                  <td><asp:CheckBox ID="chkNHSO4" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับคำแนะนำปรึกษาการวางแผนครอบครัว / คุมกำเนิด" /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO5" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการดูแล/หญิงมีครรภ์ " /></td>
                  <td><asp:CheckBox ID="chkNHSO6" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวดเอดส์ ฟรี ปีละ 2 ครั้ง " /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO7" runat="server" Font-Bold="False" Text="การแจ้งย้ายสิทธิ " /></td>
                  <td><asp:CheckBox ID="chkNHSO8" runat="server" Font-Bold="False" Text="สายด่วน 1330" /></td>
                </tr>
              </table></td>
      </tr>
    </table></td>
  </tr>  
 
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="Section_Header">
      <tr>
        <td width="100">สถานะ</td>
        <td>
            <asp:CheckBox ID="chkClose" runat="server" Text="จบการทำงาน" Checked="True" />
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center">
           <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="70px" />
        &nbsp;<asp:Button ID="cmdClear" runat="server" Text="ยกเลิก" CssClass="buttonCancle" Width="70px" />
      </td>
  </tr>
</table>
    
 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    </section>
</asp:Content>
 

