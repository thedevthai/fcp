﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="F17.aspx.vb" Inherits=".F17" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>
<%@ Register src="ucHistoryYear.ascx" tagname="ucHistoryYear" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/pagestyles.css">
<link rel="stylesheet" type="text/css" href="css/rajchasistyles.css">
<link rel="stylesheet" type="text/css" href="css/uidialog.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>F17
        <small>แบบประเมินความเสี่ยงเบื้องต้นโรคต่อการเป็นหลอดเลือดสมอง</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">   
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลกิจกรรม</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">


 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="10"> <uc1:ucHistoryYear ID="ucHistoryYear1" runat="server" /></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="OptionPanels">
      <tr>
        <td width="100" class="texttopic">รหัสร้าน</td>
        <td width="120" class="LocationName">
            <asp:Label ID="lblLocationID" runat="server"></asp:Label>
          </td>
        <td width="50" class="texttopic">ชื่อร้าน</td>
        <td width="300" class="LocationName">
            <asp:Label ID="lblLocationName" runat="server"></asp:Label>          </td>
        <td width="50" class="texttopic">จังหวัด</td>
        <td class="LocationName">
            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
   <tr>
    <td align="center"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">
      <tr>
        <td width="100">วันที่ให้บริการ</td>
        <td>
            <asp:TextBox ID="txtServiceDate" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
(วว/ดด/ปปปป พ.ศ.) 
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="txtServiceDate" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="\d{2}/\d{2}/\d{4}"></asp:RegularExpressionValidator></td>
        <td width="100">รวมระยะเวลา<img src="images/star.png" width="10" height="10" /></td>
        <td>
            <asp:TextBox ID="txtTime" runat="server" Width="50px"></asp:TextBox>
&nbsp;นาที</td>
        <td>เภสัชกรผู้ให้บริการ</td>
        <td>
            <asp:DropDownList ID="ddlPerson" runat="server">            </asp:DropDownList>          </td>
      </tr>
    </table></td>
  </tr>

   <tr>
    <td>
      <table width="100%" border="0" cellspacing="2" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
          <td width="100" align="right"><span class="NameEN">Ref.ID :</span></td>
          <td width="50"><span class="NameEN">
              <asp:Label ID="lblID" 
                  runat="server"></asp:Label></span>
            </td>
        </tr>
      </table></td>
  </tr>
  
   <tr>
    <td class="MenuSt">แบบประเมินความเสี่ยงเบื้องต้นโรคต่อการเป็นหลอดเลือดสมอง </td>
  </tr>
  <tr>
    <td>
          <table width="100%" align="center" class="dc_table_s3">
            <thead>
              </thead>
            <tfoot>
              </tfoot>
            <tbody>
              <tr class="odd">
                <th align="left" scope="row">1.	ท่านมีญาติสายตรง ( พ่อ หรือแม่ หรือพี่ หรือน้อง ) เป็นโรคหัวใจขาดเลือดหรืออัมพาตใช่หรือไม่</th>
                </tr>
              <tr >
                <td align="left" scope="row">
                    <asp:RadioButtonList ID="optQ1" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Value="1">ใช่ [1]</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True">ไม่ใช่ [0]</asp:ListItem>
                    </asp:RadioButtonList>
                  </td>
                </tr>
              <tr class="odd">
                <th align="left" scope="row">2.	ในระยะ 6 เดือนที่ผ่านมา จนถึงปัจจุบัน  ท่านสูบบุหรี่ใช่หรือไม่</th>
                </tr>
              <tr>
                <td align="left" scope="row" valign="top">
                    <asp:RadioButtonList ID="optQ2" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Value="1">ใช่ [1]</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True">ไม่ใช่ [0]</asp:ListItem>
                        <asp:ListItem Value="2">ไม่ทราบ [0]</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                </tr>
              <tr class="odd">
                <th align="left" scope="row">3.	ท่านมีระดับความดันโลหิตที่วัดได้ มากว่า หรือเท่ากับ  140 /90    mmHg   หรือเคยได้รับการวินิจฉัยว่าเป็นโรคความดันโลหิตสูงหรือไหม่</th>
                </tr>
                 <tr>
                <td align="left" scope="row" valign="top">
                    <asp:RadioButtonList ID="optQ3" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Value="1">ใช่ [1]</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True">ไม่ใช่ [0]</asp:ListItem>
                        <asp:ListItem Value="2">ไม่ทราบ [0]</asp:ListItem>
                    </asp:RadioButtonList>
                     </td>
                </tr>
                 <tr class="odd">
                <th align="left" scope="row">4.	ท่านมีระดับน้ำตาลในเลือดจากหลอดเลือดฝอย   มากกว่า120  มิลลิกรัมเปอร์เซ็นต์ หรือเคยได้รับการวินิจฉัยว่าเป็นเบาหวาน</th>
                </tr>
                 <tr>
                <td align="left" scope="row" valign="top">
                    <asp:RadioButtonList ID="optQ4" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Value="1">ใช่ [1]</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True">ไม่ใช่ [0]</asp:ListItem>
                        <asp:ListItem Value="2">ไม่ทราบ [0]</asp:ListItem>
                    </asp:RadioButtonList>
                     </td>
                </tr>
                 <tr class="odd">
                <th align="left" scope="row">5.	ท่านเคยได้รับการบอกจากแพทย์หรือพยาบาลว่ามีไขมันในเลือดผิดปกติ</th>
                </tr>
                 <tr>
                <td align="left" scope="row" valign="top">
                    <asp:RadioButtonList ID="optQ5" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Value="1">ใช่ [1]</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True">ไม่ใช่ [0]</asp:ListItem>
                    </asp:RadioButtonList>
                     </td>
                </tr>
                    <tr class="odd">
                <th align="left" scope="row">6. ท่านมีดัชนีมวลการ ( BMI ) มากกว่า 25 kg/m2    หรือขนาดรอบเอวที่วัดได้  ชายมากกว่า 90 ซม.  หญิง มากกว่า 80 ซม.</th>
                </tr> 
                <tr>
                <td align="left" scope="row" valign="top">
                    <asp:RadioButtonList ID="optQ6" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Value="1">ใช่ [1]</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True">ไม่ใช่ [0]</asp:ListItem>
                        <asp:ListItem Value="2">ไม่ทราบ [0]</asp:ListItem>
                    </asp:RadioButtonList>
                     </td>
                </tr>
               <tr class="odd">
                <th align="left" scope="row">7. ท่านเป็นโรคหลอดเลือดสมองหรือไม่</th>
                </tr>
                <tr>
                <td align="left" scope="row" valign="top">
                    <asp:RadioButtonList ID="optQ7" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Value="1">ใช่ [1]</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True">ไม่ใช่ [0]</asp:ListItem>
                    </asp:RadioButtonList>
                     </td>
                </tr>
             
               
             
                 <tr class="odd">
                <th align="left" scope="row">8.	ท่านเป็นโรคหัวใจหรือไม่</th>
                </tr>
                 <tr>
                <td align="left" scope="row" valign="top">
                    <asp:RadioButtonList ID="optQ8" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                        <asp:ListItem Value="1">ใช่ [1]</asp:ListItem>
                        <asp:ListItem Value="0" Selected="True">ไม่ใช่ [0]</asp:ListItem>
                    </asp:RadioButtonList>
                     </td>
                </tr>
               
                </tbody>
            </table>     

    </td>
  </tr>
  <tr>
    <td class="MenuSt">สรุปผลการประเมินโอกาสเสี่ยงต่อการเป็นอัมพาต </td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td align="center">
                    <asp:RadioButtonList ID="optAbnormal" runat="server" RepeatDirection="Horizontal">
                        <asp:ListItem Value="L">ต่ำ  ( 0 คะแนน )</asp:ListItem>
                        <asp:ListItem Value="M" Selected="True">ปานกลาง  ( 1 – 6 คะแนน )          </asp:ListItem>
                        <asp:ListItem Value="H">เสี่ยสูง   ( 7-8 คะแนน )</asp:ListItem>
                    </asp:RadioButtonList>
                  </td>
      </tr>
       </table></td>
  </tr>
      <tr>
    <td class="MenuSt">การให้ความรู้</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td align="left" width="35%">
            &nbsp;</td>
        <td align="left">

            <asp:CheckBox ID="chkEdu1" runat="server" Font-Bold="False" Text="สอน Stroke  Application  เน้น 270 นาทีเพื่อชีวิต" />    
              <br />
              <asp:CheckBox ID="chkEdu2" runat="server" Font-Bold="False" Text="อธิบายอาการที่ต้องรีบไป รพ. : Facial Palsy ; Arm drip ; Speech  ; Time" />      
              <br />
              <asp:CheckBox ID="chkEdu3" runat="server" Font-Bold="False" Text="MTM" />      
              <br />
            
            <asp:CheckBox ID="chkEdu4" runat="server" Font-Bold="False" Text="ส่งต่อ" />
            
            <br />
            <asp:CheckBox ID="chkEdu5" runat="server" Font-Bold="False" Text="ปรับพฤติกรรม" />
<br />
             <asp:CheckBox ID="chkEduOther" runat="server" Font-Bold="False" Text="อื่นๆ" />
            &nbsp;
            <asp:TextBox 
                 ID="txtEduOther" runat="server" Width="350px"></asp:TextBox>

        </td>
      </tr>
       </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="dc_table_s3">
     <thead>
      <tr>
        <th scope="col" width="50%" valign="top"  class="MenuSt">แจ้งสิทธิประโยช์  สปสช.</th>
      </tr>
       </thead>
      <tr>
        <td align="left">
              <table border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td><asp:CheckBox ID="chkNHSO1" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวจคัดกรองความเสี่ยงDM/HT" /></td>
                  <td><asp:CheckBox ID="chkNHSO2" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวจคัดกรองความเสี่ยงมะเร็งปากมดลูก/มะเร็งเต้านม" /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO3" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับวัคซีนเสริมสร้างภูมิคุ้มกันโรค" /></td>
                  <td><asp:CheckBox ID="chkNHSO4" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับคำแนะนำปรึกษาการวางแผนครอบครัว / คุมกำเนิด" /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO5" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการดูแล/หญิงมีครรภ์ " /></td>
                  <td><asp:CheckBox ID="chkNHSO6" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวดเอดส์ ฟรี ปีละ 2 ครั้ง " /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO7" runat="server" Font-Bold="False" Text="การแจ้งย้ายสิทธิ " /></td>
                  <td><asp:CheckBox ID="chkNHSO8" runat="server" Font-Bold="False" Text="สายด่วน 1330" /></td>
                </tr>
              </table></td>
      </tr>
    </table></td>
  </tr>  
 
 
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="Section_Header">
      <tr>
        <td width="100">สถานะ</td>
        <td>
            <asp:CheckBox ID="chkClose" runat="server" Text="จบการทำงาน" Checked="True" />
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center">
           <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="100px" />
        &nbsp;</td>
  </tr>
</table>


 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    </section>
</asp:Content>
 

