﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="F14.aspx.vb" Inherits=".F14" %>
<%@ Register src="ucHistoryYear.ascx" tagname="ucHistoryYear" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/pagestyles.css">
<link rel="stylesheet" type="text/css" href="css/rajchasistyles.css">
<link rel="stylesheet" type="text/css" href="css/uidialog.css">
<link rel="stylesheet" type="text/css" href="css/dc_table.css">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>F14
        <small>กิจกรรมการให้ความรู้ คำแนะนำในการใช้ยาปฏิชีวนะอย่างสมเหตุสมผล ()
                                <br />
        เพื่อป้องกันปัญหาการดื้อยา (โรคท้องร่วงเฉียบพลัน)</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">     

<asp:HiddenField ID="HiddenField1" runat="server" />
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลกิจกรรม</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">



 

 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
   <td height="10"> <uc1:ucHistoryYear ID="ucHistoryYear1" runat="server" /></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="OptionPanels">
      <tr>
        <td width="100" class="texttopic">รหัสร้าน</td>
        <td width="120" class="LocationName">
            <asp:Label ID="lblLocationID" runat="server"></asp:Label>          </td>
        <td width="50" class="texttopic">ชื่อร้าน</td>
        <td width="300" class="LocationName">
            <asp:Label ID="lblLocationName" runat="server"></asp:Label>          </td>
        <td width="50" class="texttopic">จังหวัด</td>
        <td class="LocationName">
            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
   <tr>
    <td align="center"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">
      <tr>
        <td width="100">วันที่ให้บริการ</td>
        <td>
            <asp:TextBox ID="txtServiceDate" runat="server"></asp:TextBox>
(วว/ดด/ปปปป พ.ศ.) 
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="txtServiceDate" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="\d{2}/\d{2}/\d{4}"></asp:RegularExpressionValidator></td>
        <td width="100">รวมระยะเวลา<img src="images/star.png" width="10" height="10" /></td>
        <td>
            <asp:TextBox ID="txtTime" runat="server" Width="50px"></asp:TextBox>
&nbsp;นาที</td>
        <td>เภสัชกรผู้ให้บริการ</td>
        <td>
            <asp:DropDownList ID="ddlPerson" runat="server">            </asp:DropDownList>          </td>
      </tr>
    </table></td>
  </tr>
 
   <tr>
    <td>
      <table width="100%" border="0" cellspacing="2" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
          <td width="100" align="right"><span class="NameEN">Ref.ID :</span></td>
          <td width="50"><span class="NameEN">
              <asp:Label ID="lblID" 
                  runat="server"></asp:Label></span>            </td>
        </tr>
      </table></td>
  </tr>
   <tr>
    <td align="center" class="MenuSt">ข้อมูลสุขภาพ</td>
  </tr>
   <tr>
     <td align="left"><table border="0" cellspacing="2" cellpadding="0">
       <tr>
         <td width="100" class="texttopic">ประวัติแพ้ยา</td>
         <td><asp:TextBox ID="txtAllergy" runat="server" Width="300px"></asp:TextBox></td>
         <td width="150" align="right" class="texttopic">โรคประจำตัว</td>
         <td><asp:TextBox ID="txtDisease" runat="server" Width="300px"></asp:TextBox></td>
       </tr>
       </table></td>
   </tr>
 
   <tr>
    <td align="left"><table border="0" cellpadding="0" cellspacing="2">
      <tr>
        <td width="100" class="texttopic">ยาที่ใช้ปัจจุบัน</td>
        <td align="left"><asp:TextBox ID="txtMedicineUsage" runat="server" Width="500px"></asp:TextBox></td>
        <td width="150" align="right" class="texttopic">มีอาการก่อนมาร้านยา</td>
        <td align="left"><asp:TextBox ID="txtUsageDay" runat="server" Width="50px"></asp:TextBox> 
        วัน</td>
      </tr>
      <tr>
        <td colspan="2" class="texttopic">รักษาด้วยยาต้านจุลชีพจากที่อื่นมาก่อน</td>
        <td colspan="2" align="left">
          <asp:RadioButtonList ID="optConsult" runat="server" 
                RepeatDirection="Horizontal">
            <asp:ListItem Selected="True" Value="1">ใช่</asp:ListItem>
            <asp:ListItem Value="0">ไม่ใช่</asp:ListItem>
          </asp:RadioButtonList>          </td>
        </tr>
     
    </table></td> 
  </tr> 
   <tr>
    <td align="center" class="MenuSt">ข้อมูลการคัดกรอง
      </td>
  </tr>
  
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="2" Text='7b. จ่ายยาต้านจุลชีพ first-line empiric therapy นาน 5-7 วัน' runat="server">
       
          <tr>
            <td colspan="8" align="center" valign="top"  class="block_Green">ถ่ายเหลวเป็นน้ำ &gt;= 3 ครั้งใน 24 ชม. หรือ ถ่ายเป็นมูก หรือมูกปนเลือดตั้งแต่ 1 ครั้งขึ้นไปใน 24 ชม.</td>
          </tr>
          <tr>
            <td  align="center" valign="middle"  class="block_Green">> 3 สัปดาห์ หรือเป็นๆ หายๆ </td>
            <td colspan="7" align="center"  valign="middle"  class="block_Green">&lt; 3 สัปดาห์ : ประเมินการขาดน้ำ </td>
          </tr>
          <tr>
            <td align="left" valign="top" class="block_white" > <p>
              <asp:CheckBox ID="chkD1" runat="server" Font-Bold="False" Text="โรคท้องเสียเรื้อรัง" /></p>              </td>
            <td   valign="middle" align="center" class="block_Green">ท้องเสียรุนแรง</span><br />            </td>
           <td align="center" colspan="2" valign="middle"  class="block_white">อาเจียนเป็นอาการเด่น</td>
            <td align="center" colspan="4" valign="middle"  class="block_Green">อาการท้องเสียเป็นอาการเด่น</td>
          </tr>
          <tr>
            <td rowspan="3" valign="top" class="block_white">.<br /></td>
            <td rowspan="3" align="center"   valign="top" class="block_white"><asp:CheckBox ID="chkD3" runat="server" Font-Bold="False" Text="ตาโบ๋ ปากแห้ง กระหายน้ำ หรือมีช๊อก" />                          <br /></td>
            <td rowspan="2"   align="left"   valign="top" class="block_white"><asp:CheckBox ID="chkD6" runat="server" Font-Bold="False" Text='เด็ก <3 ปีหรือผู้ป่วยอยู่ในสถานเลี้ยงเด็ก โรงเรียน หรือ บ้านพักคนชรา' />                          <br />
              <asp:CheckBox ID="chkD7" runat="server" Font-Bold="False" Text="อาจมีอาการไข้หวัดร่วม" />
              <br />
              <asp:CheckBox ID="chkD8" runat="server" Font-Bold="False" Text="ไข้สูงไม่เกิน 7 วัน" />
            <br />
            <asp:CheckBox ID="chkD9" runat="server" Font-Bold="False" Text='อาจมีอาการปวดท้อง' />
            <br />
            <asp:CheckBox ID="chkD10" runat="server" Font-Bold="False" Text='ถ่ายเป็นน้ำบ่อย อุจจาระไม่มีมูก เลือด อุจจาระเป็นฟอง เหม็นเปรี้ยว' />
            <br />
            <asp:CheckBox ID="chkD11" runat="server" Font-Bold="False" Text='อาจมีภาวะขาดน้ำรุนแรงได้' />            
           <br /></td>
             <td rowspan="2"   align="left"   valign="top" class="block_white"><asp:CheckBox ID="chkD13" runat="server" Font-Bold="False" Text='ประวัติทานอาหารปนเปื้อนสารพิษ แบคทีเรีย อาจจะปรุงสุกหรือไม่สุก' />                            <br />
               <asp:CheckBox ID="chkD14" runat="server" Font-Bold="False" Text="อาการเกิดใน 1-18 ชม. หลังทาน" />
               <br />
               <asp:CheckBox ID="chkD15" runat="server" Font-Bold="False" Text="มักไม่มีไข้" />
             <br />
             <asp:CheckBox ID="chkD16" runat="server" Font-Bold="False" Text='มีอาการปวดบิดเป็นพักๆ' />
             <br />
             <asp:CheckBox ID="chkD17" runat="server" Font-Bold="False" Text='ถ่ายเป็นน้ำบ่อย อุจจระไม่มีมูกเลือด' />
             <br />
             <asp:CheckBox ID="chkD18" runat="server" Font-Bold="False" Text='มักหายเองใน 1-2 วัน' />
             <br />
             <asp:CheckBox ID="chkD19" runat="server" Font-Bold="False" Text='อาจมีภาวะขาดน้ำรุนแรงได้' />
             <br />
            <br /></td>
              <td colspan="3"   align="center"   valign="top" class="block_white"><asp:CheckBox ID="chkD20" runat="server" Font-Bold="False" Text="ถ่ายเป็นมูก/มูกปนเลือดมีอาการปวดเบ่ง อยากถ่าย ถ่ายบ่อย แต่มีเนื้ออุจจระน้อย" /></td>
               <td rowspan="2"   valign="top" class="block_white"><asp:CheckBox ID="chkD39" runat="server" Font-Bold="False" Text="ถ่ายเป็นน้ำจำนวนมาก" />                                <br />
                 <asp:CheckBox ID="chkD40" runat="server" Font-Bold="False" Text="ประวัติรับประทานอาหารทะเลดิบ " />               
                 <br />
                 <asp:CheckBox ID="chkD41" runat="server" Font-Bold="False" Text='ถ่ายเป็นน้ำรุนแรง อุจจาระไหลพุ่ง ไม่มีกลิ่น ถ่ายวันละหลายสิบครั้ง' />
              <br />
              <asp:CheckBox ID="chkD42" runat="server" Font-Bold="False" Text='ระยะแรกอุจจาระมีเนื้อปน ต่อมาน้ำล้วนๆ เหมือนน้ำซาวข้าว' />              
              <br />
              <asp:CheckBox ID="chkD43" runat="server" Font-Bold="False" Text='ส่วนน้อยจะปวดบิด' />              
              <br />
              <asp:CheckBox ID="chkD44" runat="server" Font-Bold="False" Text='อาเจียน' />              
              <br />
              <asp:CheckBox ID="chkD45" runat="server" Font-Bold="False" Text='อาจมีภาวะขาดน้ำรุนแรงได้' />              
             </td>
          </tr>
          <tr>
            <td align="left"   valign="top" class="block_white"><asp:CheckBox ID="chkD21" runat="server" Font-Bold="False" Text="ปวดท้องรุนแรง" />                          <br />
              <asp:CheckBox ID="chkD22" runat="server" Font-Bold="False" Text="กดเจ็บท้องมาก หน้าท้องเกร็งแข็ง" />
              <br />
            <asp:CheckBox ID="chkD23" runat="server" Font-Bold="False" Text='อาเจียนเป็นพักๆ' />
            <br />
            <asp:CheckBox ID="chkD24" runat="server" Font-Bold="False" Text='อาจมีภาวะขาดน้ำรุนแรงได้' />            
           </td>
            <td align="left"   valign="top" class="block_white"><asp:CheckBox ID="chkD26" runat="server" Font-Bold="False" Text="ประวัติดื่มน้ำดื่มหรือรักษา shigella ไม่ดีขึ้น" />            
              <br />
              <asp:CheckBox ID="chkD27" runat="server" Font-Bold="False" Text='ปวดบิด ลมในท้องมาก ท้องอืด' />
              <br />
            <asp:CheckBox ID="chkD28" runat="server" Text='อุจจาระเหม็นเหมือนหัวกุ้งเน่า'  />
                <br />
            <asp:CheckBox ID="chkD29" runat="server" Font-Bold="False" Text='ถ่ายอุจจาระวันละ 3-5 ครั้ง อาจถึง 20 ครั้ง' />
            <br />
            <asp:CheckBox ID="chkD30" runat="server" Font-Bold="False" Text='อาการทุเลาได้ แต่กำเริบใหม่ได้' />
            <br />
            <asp:CheckBox ID="chkD31" runat="server" Font-Bold="False" Text='อาจมีภาวะขาดน้ำรุนแรงได้' />            
          <br /></td>
            <td align="left"   valign="top" class="block_white"><asp:CheckBox ID="chkD33" runat="server" Font-Bold="False" Text="ประวัติสถานรับเลี้ยงเด็ก/คนชรา" />                          <br />
              <asp:CheckBox ID="chkD34" runat="server" Font-Bold="False" Text='ไข้สูง (อาจไม่มีไข้ หรือไข้ต่ำ)' />            
              <br />
              <asp:CheckBox ID="chkD35" runat="server" Font-Bold="False" Text='ปวดบิดในท้อง คลื่นไส้ อาเจียน' />              
              <br />
              <asp:CheckBox ID="chkD36" runat="server" Font-Bold="False" Text='ถ่ายเป็นน้ำ ช่วงแรก ต่อมาถ่ายเป็นมูก ปวดเบ่งที่ก้นวันละ 10-30 ครั้ง ไม่มีกลิ่นเหม็น' />      
              <br />
              <asp:CheckBox ID="chkD37" runat="server" Font-Bold="False" Text='อาจมีภาวะขาดน้ำรุนแรงได้' />              
          </td>
          </tr>
          <tr>
            <td align="center"   valign="middle" class="block_Green texttopic">
                <asp:CheckBox ID="chkS1" runat="server" Text="ท้องเสียจากไวรัส" />
                </td>
            <td align="center"   valign="middle" class="block_Green texttopic">
                <asp:CheckBox ID="chkS2" runat="server" Text="ท้องเสียจากอาหาร<br />
            ปนเปื้อนสารพิษ"/>
                </td>
            <td align="left"   valign="top" class="block_Green">&nbsp;</td>
            <td align="center"   valign="middle" class="block_Green texttopic">
                <asp:CheckBox ID="chkS3" runat="server" Text="บิดมีตัว/บิดอะมีบา" />
               </td>
            <td align="center"   valign="middle" class="block_Green texttopic">
                <asp:CheckBox ID="chkS4" runat="server"  Text="บิดไม่มีตัว/ปิด shigella"/>
               </td>
            <td align="center"   valign="middle" class="block_Green texttopic">
                <asp:CheckBox ID="chkS5" runat="server" Text="อหิวาต์"/>
                </td>
          </tr>
          <tr>
            <td valign="top" class="block_white"><asp:CheckBox ID="chkD2" runat="server" Font-Bold="False" Text="Refer เพื่อหาสาเหตุ " /></td>
            <td align="center"   valign="top" class="block_white"><asp:CheckBox ID="chkD4" runat="server" Font-Bold="False" Text="Refer เพิ่อรับน้ำเกลือทางหลอดเลือด" /></td>
            <td colspan="2" align="left"   valign="top" class="block_Green"><asp:CheckBox ID="chkD12" runat="server" Font-Bold="False" Text='Refer ถ้าอาการไม่ดีขึ้นหลังได้รับยา 48 ชม. หรือมีอาการรุนแรง' /></td>
            <td align="left"   valign="top" class="block_Green"><asp:CheckBox ID="chkD25" runat="server" Font-Bold="False" Text='Refer' /></td>
            <td align="left"   valign="top" class="block_Green"><asp:CheckBox ID="chkD32" runat="server" Font-Bold="False" Text='Refer ถ้าอาการไม่ดีขึ้นใน 5 วัน' /></td>
            <td align="left"   valign="top" class="block_Green"><asp:CheckBox ID="chkD38" runat="server" Font-Bold="False" Text='Refer ถ้าอาการไม่ดีขึ้นใน 5 วันหรือ ปวดท้องรุนแรง' /></td>
            <td align="left"   valign="top" class="block_Green"><asp:CheckBox ID="chkD46" runat="server" Font-Bold="False" Text='Refer ถ้าอาการไม่ดีขึ้น หลังได้รับยา 48 ชม. หรือมีอาการรุนแรง' /></td>
          </tr>
          
          
      </table></td>
  </tr>
 
 
   <tr>
     <td><table width="100%" border="0" cellspacing="2" cellpadding="0">
       <tr>
         <td width="50%" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
           
           <tr>
             <td align="center" class="MenuSt"   colspan="2">ยาที่ร้านยาจ่ายให้ (ระบุ ชื่อยา ขนาด วิธีรับประทาน จำนวนเม็ดยา)</td>
             </tr>
           <tr>
             <td align="right">1.</td>
             <td>
               <asp:DropDownList ID="ddlMedicineGroup1" runat="server">
                 <asp:ListItem>ORS</asp:ListItem>
                 <asp:ListItem>ยาปฏิชีวนะ</asp:ListItem>
                 <asp:ListItem>คาร์บอน</asp:ListItem>
                 <asp:ListItem>อื่นๆ</asp:ListItem>
                 </asp:DropDownList>
               <asp:TextBox ID="txtMed1" runat="server" Width="250px"></asp:TextBox></td>
             </tr>
           <tr>
             <td align="right">2.</td>
             <td>
               <asp:DropDownList ID="ddlMedicineGroup2" runat="server">
                 <asp:ListItem>ORS</asp:ListItem>
                 <asp:ListItem>ยาปฏิชีวนะ</asp:ListItem>
                 <asp:ListItem>คาร์บอน</asp:ListItem>
                 <asp:ListItem>อื่นๆ</asp:ListItem>
                 </asp:DropDownList>
               <asp:TextBox ID="txtMed2" runat="server" Width="250px"></asp:TextBox></td>
             </tr>
           <tr>
             <td align="right">3.</td>
             <td>
               <asp:DropDownList ID="ddlMedicineGroup3" runat="server">
                 <asp:ListItem>ORS</asp:ListItem>
                 <asp:ListItem>ยาปฏิชีวนะ</asp:ListItem>
                 <asp:ListItem>คาร์บอน</asp:ListItem>
                 <asp:ListItem>อื่นๆ</asp:ListItem>
                 </asp:DropDownList>
               <asp:TextBox ID="txtMed3" runat="server" Width="250px"></asp:TextBox></td>
             </tr>
           <tr>
             <td align="right">4.</td>
             <td>
               <asp:DropDownList ID="ddlMedicineGroup4" runat="server">
                 <asp:ListItem>ORS</asp:ListItem>
                 <asp:ListItem>ยาปฏิชีวนะ</asp:ListItem>
                 <asp:ListItem>คาร์บอน</asp:ListItem>
                 <asp:ListItem>อื่นๆ</asp:ListItem>
                 </asp:DropDownList>
               <asp:TextBox ID="txtMed4" runat="server" Width="250px"></asp:TextBox></td>
             </tr>
           <tr>
             <td align="right">5.</td>
             <td>
               <asp:DropDownList ID="ddlMedicineGroup5" runat="server">
                 <asp:ListItem>ORS</asp:ListItem>
                 <asp:ListItem>ยาปฏิชีวนะ</asp:ListItem>
                 <asp:ListItem>คาร์บอน</asp:ListItem>
                 <asp:ListItem>อื่นๆ</asp:ListItem>
                 </asp:DropDownList>
               <asp:TextBox ID="txtMed5" runat="server" Width="250px"></asp:TextBox></td>
             </tr>
           <tr>
             <td align="right">&nbsp;</td>
             <td>&nbsp;</td>
             </tr>
         </table></td>
         </tr>
       <tr>
         <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="2">
           <tr>
             <td align="center" class="MenuSt" colspan="2">ผลการติดตาม</td>
             </tr>
           <tr>
             <td>ติดตามโดย</td>
             <td align="left"><asp:RadioButtonList ID="optChannel" runat="server" 
                 RepeatDirection="Horizontal">
               <asp:ListItem Selected="True" Value="CALL">โทรติดตาม</asp:ListItem>
               <asp:ListItem Value="WALKIN">ผู้รับบริการกลับมาร้าน</asp:ListItem>
               </asp:RadioButtonList></td>
             </tr>
           <tr>
             <td valign="top">ระยะเวลา</td>
             <td align="left"><asp:RadioButtonList ID="optChannelTime" runat="server">
               <asp:ListItem Selected="True" Value="3">3 วันหลังจากมารับบริการที่ร้าน</asp:ListItem>
               <asp:ListItem Value="5">5 วันหลังจากมารับบริการที่ร้าน</asp:ListItem>
               <asp:ListItem Value="7">7 วันหลังจากมารับบริการที่ร้าน</asp:ListItem>
               </asp:RadioButtonList></td>
             </tr>
           <tr>
             <td>ติดตามวันที่</td>
             <td align="left"><asp:TextBox ID="txtFollowDate" runat="server"></asp:TextBox></td>
             </tr>
           <tr>
             <td valign="top">ผลการรักษา</td>
             <td align="left"><asp:RadioButtonList ID="optFollowup" runat="server" AutoPostBack="True">
               <asp:ListItem Selected="True" Value="0">หาย  โดยไม่ได้มีการรักษาเพิ่มเติม</asp:ListItem>
               <asp:ListItem Value="1">ไม่หาย กลับมารักษาที่ร้านยาต่อ</asp:ListItem>
               <asp:ListItem Value="2">หาย  จากการไปรักษาต่อ</asp:ListItem>
                 <asp:ListItem Value="3">ไม่หาย  จากการไปรักษาต่อ</asp:ListItem>
               </asp:RadioButtonList></td>
             </tr>
           <tr>
             <td valign="top">
                 <asp:Label ID="lblCause" runat="server" Text="เพราะ"></asp:Label>
               </td>
             <td align="left"><table border="0" cellspacing="0" cellpadding="0">
               <tr>
                 <td><asp:RadioButtonList ID="optCause" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                     <asp:ListItem Selected="True" Value="0">ไม่ได้ปฏิบัติตัวตามคำแนะนำ</asp:ListItem>
                     <asp:ListItem Value="1">ไม่ได้รับประทานยาตามคำแนะนำ</asp:ListItem>
                     <asp:ListItem Value="2">อื่นๆ</asp:ListItem>
                   </asp:RadioButtonList></td>
                 <td><asp:TextBox ID="txtCauseRemark" runat="server" Width="200px"></asp:TextBox></td>
               </tr>
             </table></td>
           </tr>
           <tr>
             <td>สถานพยาบาล</td>
             <td align="left"><asp:RadioButtonList ID="optPlace" runat="server" RepeatDirection="Horizontal">
               <asp:ListItem Selected="True" Value="1">โรงพยาบาล</asp:ListItem>
               <asp:ListItem Value="2">คลินืก</asp:ListItem>
               <asp:ListItem Value="3">ร้านยาอื่น</asp:ListItem>
               </asp:RadioButtonList></td>
             </tr>
           <tr>
             <td>ระบุชื่อสถานพยาบาล</td>
             <td align="left"><asp:TextBox ID="txtHospitalName" runat="server" Width="250px"></asp:TextBox></td>
             </tr>
           <tr>
             <td>ระบุชื่อยาจากสถานพยาบาล</td>
             <td align="left"><asp:TextBox ID="txtMedicineFromHospital" runat="server" Width="250px"></asp:TextBox></td>
             </tr>
         </table></td>
         </tr>
     </table>
     </td>
   </tr>
  
 <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="dc_table_s3">
     <thead>
      <tr>
        <th scope="col" width="50%" valign="top" class="texttopic">แจ้งสิทธิประโยช์  สปสช.</th>
      </tr>
       </thead>
      <tr>
        <td align="left">
              <table border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td><asp:CheckBox ID="chkNHSO1" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวจคัดกรองความเสี่ยงDM/HT" /></td>
                  <td><asp:CheckBox ID="chkNHSO2" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวจคัดกรองความเสี่ยงมะเร็งปากมดลูก/มะเร็งเต้านม" /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO3" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับวัคซีนเสริมสร้างภูมิคุ้มกันโรค" /></td>
                  <td><asp:CheckBox ID="chkNHSO4" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับคำแนะนำปรึกษาการวางแผนครอบครัว / คุมกำเนิด" /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO5" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการดูแล/หญิงมีครรภ์ " /></td>
                  <td><asp:CheckBox ID="chkNHSO6" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวดเอดส์ ฟรี ปีละ 2 ครั้ง " /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO7" runat="server" Font-Bold="False" Text="การแจ้งย้ายสิทธิ " /></td>
                  <td><asp:CheckBox ID="chkNHSO8" runat="server" Font-Bold="False" Text="สายด่วน 1330" /></td>
                </tr>
              </table></td>
      </tr>
    </table></td>
  </tr>  
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="Section_Header">
      <tr>
        <td width="100">สถานะ</td>
        <td>
            <asp:CheckBox ID="chkStatus" runat="server" Text="จบการทำงาน" />          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center">
        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="70px" />
        &nbsp;<asp:Button ID="cmdClear" runat="server" Text="ยกเลิก" CssClass="buttonCancle" Width="70px" />
        </td>
  </tr>
</table>
</div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    </section>
</asp:Content>