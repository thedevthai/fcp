﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="A1.aspx.vb" Inherits=".A1" %>

<%@ Register Src="ucHistoryYear.ascx" TagName="ucHistoryYear" TagPrefix="uc1" %>
<%@ Register Assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/pagestyles.css" />
    <link rel="stylesheet" type="text/css" href="css/cpastyles.css" />
    <link rel="stylesheet" type="text/css" href="css/uidialog.css" />
    <link rel="stylesheet" type="text/css" href="css/dc_table.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <section class="content-header">
        <h1>A1-A4
        <small>แผนงานเครือข่ายวิชาชีพเภสัชกรรมบริการเลิกบุหรี่และพัฒนาระบบบริการเลิกบุหรี่</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"></li>
        </ol>
    </section>

    <section class="content">
        <div align="center">
            <uc1:ucHistoryYear ID="ucHistoryYear1" runat="server" />
        </div>
        <br />
        <div class="pull-right">
            <table border="0" cellspacing="2" cellpadding="0">
                <tr>

                    <td class="NameEN">Item ID : </td>
                    <td class="NameEN">
                        <asp:Label ID="lblID"
                            runat="server"></asp:Label>
                    </td>

                </tr>
            </table>
        </div> 

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">ข้อมูลร้านยาที่ให้บริการ</h3>
                <div class="box-tools pull-right">


                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <table width="100%" border="0" align="left" cellpadding="0" cellspacing="2" class="table table-hover Section_Date">
                    <tr>
                        <td width="80">รหัสร้าน</td>
                        <td width="120">
                            <asp:Label ID="lblLocationID" runat="server"></asp:Label>
                        </td>
                        <td >ชื่อร้าน</td>
                        <td class="text-blue">
                            <asp:Label ID="lblLocationName" runat="server"></asp:Label>
                        </td>
                        <td>รหัสหน่วยบริการ</td>
                        <td width="60">
                            <asp:Label ID="lblLocationCode" runat="server"></asp:Label>
                        </td>
                        <td>จังหวัด</td>
                        <td>
                            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>
                        </td>
                    </tr>
   <tr> 
                        <td>ปี</td>
                        <td>
                            <asp:TextBox ID="txtYear" runat="server" CssClass="text-center" Width="60px"></asp:TextBox>
                        </td>

                        <td>วันที่ให้บริการ</td>
                        <td align="left">
                            <asp:TextBox ID="txtServiceDate" runat="server" CssClass="form-control text-center" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox></td>
                                   
                      
                        <td width="190">ระยะเวลาที่ให้บริการ (นาที)<img src="images/star.png" width="10" height="10" /></td>
                        <td>
                            <asp:TextBox ID="txtTime" runat="server" CssClass="text-center" Width="60px"></asp:TextBox>
                            </td>
                  
                        <td>เภสัชกรผู้ให้บริการ</td>
                        <td>
                            <asp:DropDownList CssClass="form-control select2" ID="ddlPerson" runat="server"></asp:DropDownList>
                        </td>
                    </tr>                   
                </table>            

            </div>
            <div class="box-footer text-center">
 <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                ControlToValidate="txtServiceDate" CssClass="text_red"
                                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="(\d{1}|\d{2})/(\d{1}|\d{2})/\d{4}"></asp:RegularExpressionValidator>
            </div>
        </div>


        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">แหล่งที่มา/วิธีการให้บริการ</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">

                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="table table-hover">

                    <tr>
                        <td>ผู้รับบริการ
             &nbsp;</td>
                        <td align="left">

                            <asp:RadioButtonList ID="optPatientType" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="รายเก่า">รายเก่า</asp:ListItem>
                                <asp:ListItem Value="รายใหม่" Selected="True">รายใหม่</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>

                    <tr>
                        <td>แหล่งที่มา</td>
                        <td align="left">

                            <table>
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="optPatientFrom" runat="server" RepeatDirection="Horizontal" RepeatColumns="3">
                                            <asp:ListItem Value="W" Selected="True">ประชาชนที่มาที่ร้าน</asp:ListItem>
                                            <asp:ListItem Value="R">มาจากหน่วยบริการ</asp:ListItem>
                                            <asp:ListItem Value="H">มาจากการเยี่ยมบ้าน</asp:ListItem>
                                            <asp:ListItem Value="E">มาจากการจัดกิจกรรม/ event ต่างๆ</asp:ListItem>
                                            <asp:ListItem Value="T">มาจากโครงการ Telepharmacy</asp:ListItem>
                                            <asp:ListItem Value="9">อื่นๆ</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td valign="bottom" width="30">ระบุ</td>
                                    <td valign="bottom">
                                        <asp:TextBox ID="txtFromRemark" runat="server" Width="300px" placeholder="ระบุหน่วยบริการ หรือ กรณีอื่นๆ"></asp:TextBox></td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                    <tr>
                        <td valign="top">การให้บริการ</td>
                        <td align="left">
                            <table>
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="optService" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="1">Face to face</asp:ListItem>
                                            <asp:ListItem Value="2">Telepharmacy</asp:ListItem>
                                            <asp:ListItem Value="9">อื่นๆ</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtServiceRemark" runat="server" Width="300px" placeholder="กรณีอื่นๆ ระบุ"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">กรณี Telepharmacy</td>
                        <td align="left">
                            <table>
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="optTelepharm" runat="server" RepeatDirection="Horizontal" RepeatColumns="2">
                                            <asp:ListItem Value="1">ผป.ของร้านเอง</asp:ListItem>
                                            <asp:ListItem Value="2">ผป.โครงการลดแอัด (ระบุ รพ.แม่ข่าย)</asp:ListItem>
                                            <asp:ListItem Value="3">ผป.รับยาทาง ปณ.จาก รพ. (ระบุ รพ.)</asp:ListItem>
                                            <asp:ListItem Value="9">ผป.ในโครงการอื่นๆ (ระบุ)</asp:ListItem>
                                        </asp:RadioButtonList></td>
                                    <td width="60" class="text-right">ระบุ</td>
                                    <td>
                                        <asp:TextBox ID="txtTelepharmacyRemark" runat="server" Width="300px"></asp:TextBox></td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                    <tr>
                        <td valign="top">วิธีการสื่อสาร Telepharmacy</td>
                        <td align="left">

                            <table>
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="optTelepharmacyMethod" runat="server">
                                            <asp:ListItem Value="1">โทรศัพท์ หรือ  line call  <u><b>ไม่มีการบันทึกทั้งภาพและเสียง</b></u>        </asp:ListItem>
                                            <asp:ListItem Value="2">โทรศัพท์  หรือ  Line call <u><b>บันทึกแต่เสียง</b></u></asp:ListItem>
                                            <asp:ListItem Value="3">มี soft ware หรือ program หรือ  Line Call  หรือ อื่นๆ  และ<u><b>มีการบันทึก ภาพและเสียง</b></u></asp:ListItem>
                                        </asp:RadioButtonList></td>
                                    <td>
                                        <table class="nav-justified">
                                            <tr>
                                                <td>ระบุ วิธีการบันทึก</td>
                                                <td>
                                                    <asp:TextBox ID="txtRecordMethod" runat="server" Width="300px"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td>ระบุ  สถานที่เก็บข้อมูล</td>
                                                <td>
                                                    <asp:TextBox ID="txtRecordLocation" runat="server" Width="300px"></asp:TextBox></td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                            </table>


                        </td>
                        <td valign="top"></td>
                    </tr>
                </table>

            </div>
        </div>

        <div class="box box-success">
            <div class="box-header">     
                <h3 class="box-title">ข้อมูลสุขภาพและการเลิกบุหรี่</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>


            </div>
            <div class="box-body">
                <table width="100%" border="0" class="table table-bordered">
                   
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="MenuSt">พฤติกรรมสุขภาพ</td>
                    </tr>
                    <tr>
                        <td>
                            <table class="table table-hover">
                                <tr>
                                    <td>การดื่มเครื่องดื่มที่มีแอลกอฮอล์</td>
                                    <td align="left">
                                        <table border="0" cellspacing="2" cellpadding="0">
                                            <tr>
                                                <td align="left">
                                                    <asp:RadioButtonList ID="optAlcohol" runat="server"
                                                        RepeatDirection="Horizontal">
                                                        <asp:ListItem Value="1">ดื่มทุกวัน</asp:ListItem>
                                                        <asp:ListItem Value="2">ดื่มเป็นครั้งคราว</asp:ListItem>
                                                        <asp:ListItem Value="3">เคยดื่มแต่เลิกแล้ว</asp:ListItem>
                                                        <asp:ListItem Value="4">ไม่ดื่ม</asp:ListItem>
                                                        <asp:ListItem Selected="True" Value="0">ไม่มีข้อมูล</asp:ListItem>
                                                    </asp:RadioButtonList></td>
                                                <td align="left">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">สูบบุหรี่</td>
                                    <td>
                                        <table border="0" cellspacing="2" cellpadding="0">
                                            <tr>
                                                <td align="left" valign="middle">สูบมานาน เป็นเวลา</td>
                                                <td colspan="4" align="left">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtSmokeDay" CssClass="text-center" runat="server" Width="40px"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:RadioButtonList ID="optSmokeUOM" runat="server"
                                                                    RepeatDirection="Horizontal" AutoPostBack="True">
                                                                    <asp:ListItem Value="M">เดือน</asp:ListItem>
                                                                    <asp:ListItem Value="Y" Selected="True">ปี</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="3">
                                                    <asp:RadioButtonList ID="optSmokeFQ" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                        <asp:ListItem Selected="True" Value="1">ปัจจุบันสูบทุกวัน</asp:ListItem>
                                                        <asp:ListItem Value="2">ปัจจุบันสูบเป็นครั้งคราว</asp:ListItem>
                                                        <asp:ListItem Value="3">ปัจจุบันหยุดสูบ โดยหยุดมาแล้ว </asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCGFQ" runat="server" Text="วันละประมาณ"></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtSmokeQTY" runat="server" CssClass="text-center" Width="60px"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblCGday" runat="server" Text="มวน"></asp:Label>
                                                    <asp:RadioButtonList ID="optStopUOM" runat="server"
                                                        RepeatDirection="Horizontal">
                                                        <asp:ListItem Value="D">วัน</asp:ListItem>
                                                        <asp:ListItem Value="M">เดือน</asp:ListItem>
                                                        <asp:ListItem Value="Y" Selected="True">ปี</asp:ListItem>
                                                    </asp:RadioButtonList></td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <asp:Label ID="lblCgType" runat="server" Text="ประเภทของบุหรี่ที่สูบ"></asp:Label></td>
                                                <td>
                                                    <asp:RadioButtonList ID="optCigarette" runat="server"
                                                        RepeatDirection="Horizontal">
                                                        <asp:ListItem Value="1">มวนเอง</asp:ListItem>
                                                        <asp:ListItem Selected="True" Value="2">บุหรี่ซอง</asp:ListItem>
                                                        <asp:ListItem Value="3">บุหรี่ไฟฟ้า</asp:ListItem>
                                                        <asp:ListItem Value="9">อื่นๆ</asp:ListItem>
                                                    </asp:RadioButtonList></td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td><u>กรณีบุหรี่ไฟฟ้า</u>  ระบุความเข้มข้นของนิโคติน</td>
                                                <td>
                                                    <asp:TextBox ID="txtNicotinRate" runat="server" CssClass="text-center" Width="80px"></asp:TextBox></td>
                                                <td>จำนวนครั้งที่สูบต่อวัน</td>
                                                <td>
                                                    <asp:TextBox ID="txtOften" runat="server" CssClass="text-center" Width="50px"></asp:TextBox>ครั้ง/วัน</td>
                                                <td>&nbsp;</td>
                                                <td>ในแต่ละครั้งที่สูบบุหรี่ไฟฟ้าสูบนาน</td>
                                                <td>
                                                    <asp:TextBox ID="txtMinutePerTime" runat="server" CssClass="text-center" Width="80px"></asp:TextBox></td>
                                                <td>&nbsp;นาที/ครั้ง</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td>บุคคลใกล้ชิดที่สูบบุหรี่</td>
                                    <td align="left">
                                        <table border="0" cellspacing="2" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <asp:RadioButtonList ID="optIntimate" runat="server"
                                                        RepeatDirection="Horizontal">
                                                        <asp:ListItem Value="0" Selected="True">ไม่มี</asp:ListItem>
                                                        <asp:ListItem Value="1">มี  (ระบุ)</asp:ListItem>
                                                    </asp:RadioButtonList></td>
                                                <td>
                                                    <asp:TextBox ID="txtCloseupPerson" runat="server" AutoPostBack="True" Width="200px"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="MenuSt">ประวัติการเลิกบุหรี่</td>
                    </tr>
                    <tr>
                        <td align="left" scope="row">ในอดีตเคยพยายามเลิกสูบบุหรี่หรือไม่</td>
                    </tr>
                    <tr>
                        <td align="left" scope="row">
                            <table>
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="optQ1A" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True" Value="N">ไม่เคยเลิก</asp:ListItem>
                                            <asp:ListItem Value="Y">เคยเลิก</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtQ1B" runat="server" CssClass="text-center" Width="50px"></asp:TextBox>
                                    </td>
                                    <td>ครั้ง เลิกได้นานสุด</td>
                                    <td>
                                        <asp:TextBox ID="txtQ1C" runat="server" CssClass="text-center" Width="50px"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="optQ1D" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True" Value="D">วัน</asp:ListItem>
                                            <asp:ListItem Value="M">เดือน</asp:ListItem>
                                            <asp:ListItem Value="Y">ปี</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" scope="row">เคยเลิกโดยวิธี (ตอบได้หลายข้อ)</td>
                    </tr>
                    <tr>
                        <td align="left" scope="row" valign="top">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td valign="top">
                                        <asp:CheckBoxList ID="chkQ2" runat="server" Width="100px">
                                            <asp:ListItem>หักดิบ</asp:ListItem>
                                            <asp:ListItem>ค่อยๆลด</asp:ListItem>
                                            <asp:ListItem>ใช้ยา</asp:ListItem>
                                        </asp:CheckBoxList>
                                    </td>
                                    <td valign="top">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBoxList ID="chkQ2Med" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem>Nicotine gum</asp:ListItem>
                                                        <asp:ListItem>Nicotine patch</asp:ListItem>
                                                        <asp:ListItem>nortriptyline</asp:ListItem>
                                                        <asp:ListItem Value="Bupropion">Bupropion</asp:ListItem>
                                                        <asp:ListItem>Varenicline</asp:ListItem>
                                                        <asp:ListItem>สมุนไพรชงหญ้าดอกขาว</asp:ListItem>
                                                        <asp:ListItem Value="อื่นๆ">อื่นๆ</asp:ListItem>
                                                    </asp:CheckBoxList></td>
                                                <td>
                                                    <asp:TextBox ID="txtQ2Other" runat="server"></asp:TextBox></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>


            </div>
            <div class="box-footer clearfix">
            </div>
        </div>

        <div class="box box-success">
            <div class="box-header">
                <i class="fa fa-check"></i>

                <h3 class="box-title">แบบประเมิน Fagerstrom Test for Nicotine Dependence (FTND/eFTND) เพื่อประเมินการติดทางกาย</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>


            </div>
            <div class="box-body">

                <table width="100%" border="0" class="table table-bordered">
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                <tr>
                                    <td valign="top">
                                        <table width="100%" align="center" class="dc_table_s3">
                                            <thead>
                                            </thead>
                                            <tfoot>
                                            </tfoot>
                                            <tbody>

                                                <tr class="MenuSt">
                                                    <th align="left" scope="row">1.	หลังตื่นนอนตอนเช้า คุณสูบบุหรี่มวนแรกหรือบุหรี่ไฟฟ้าเมื่อไร</th>
                                                </tr>
                                                <tr>
                                                    <td align="left" scope="row" valign="top">
                                                        <asp:RadioButtonList ID="optQ5" runat="server" AutoPostBack="True">
                                                            <asp:ListItem Value="3">สูบทันทีหลังตื่นนอน หรือภายในเวลาไม่เกิน 5 นาที (3 คะแนน)</asp:ListItem>
                                                            <asp:ListItem Value="2">สูบหลังตื่นนอนเกิน 6-30 นาที (2 คะแนน)</asp:ListItem>
                                                            <asp:ListItem Value="1">สูบหลังตื่นนอน 31-60 นาที (1 คะแนน)</asp:ListItem>
                                                            <asp:ListItem Value="0" Selected="True">สูบหลังตื่นนอน หลัง 60 นาที  (0 คะแนน)</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr class="MenuSt">
                                                    <th align="left" scope="row">2. คุณรู้สึกอึดอัด กระวนกระวายหรือลำบากใจหรือไม่ ที่ต้องอยู่ในเขตปลอดบุหรี่ เช่น โรงภาพยนตร์ ห้องสมุด</th>
                                                </tr>
                                                <tr>
                                                    <td align="left" scope="row" valign="top">
                                                        <asp:RadioButtonList ID="optQ6" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                            <asp:ListItem Value="1">ใช่ (1 คะแนน)</asp:ListItem>
                                                            <asp:ListItem Value="0" Selected="True">ไม่ใช่ (0 คะแนน)</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr class="MenuSt">
                                                    <th align="left" scope="row">3. บุหรี่มวนไหนที่ท่านคิดว่าเลิกยากที่สุด</th>
                                                </tr>
                                                <tr>
                                                    <td align="left" scope="row" valign="top">
                                                        <asp:RadioButtonList ID="optQ7" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                            <asp:ListItem Value="1">มวนแรกที่สูบในตอนเช้า (1 คะแนน)</asp:ListItem>
                                                            <asp:ListItem Value="0" Selected="True">มวนไหนๆ ก็เหมือนกัน (0 คะแนน)</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>



                                                <tr class="MenuSt">
                                                    <th align="left" scope="row">4.	โดยปกติคุณสูบบุหรี่ วันละกี่มวน หรือบุหรี่ไฟฟ้า วันละกี่ครั้ง (โดยสูบบุหรี่ไฟฟ้า 1 ครั้งประมาณ 15 สูด หรือสูบนาน 10 นาที)</th>
                                                </tr>
                                                <tr>
                                                    <td align="left" scope="row" valign="top">
                                                        <asp:RadioButtonList ID="optQ8" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                            <asp:ListItem Value="3">มากกว่าหรือเท่ากับ 31 มวน (3 คะแนน)</asp:ListItem>
                                                            <asp:ListItem Value="2">21 – 30 มวน (2 คะแนน)</asp:ListItem>
                                                            <asp:ListItem Value="1">11 – 20 มว (1 คะแนน)</asp:ListItem>
                                                            <asp:ListItem Selected="True" Value="0">ไม่เกิน 10 มวน (0 คะแนน)</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>

                                                <tr class="MenuSt">
                                                    <th align="left" scope="row">5. คุณสูบบุหรี่ หรือบุหรี่ไฟฟ้าจัด ในชั่วโมงแรกหลังตื่นนอน โดยสูบมากกว่าเวลาอื่นของวัน</th>
                                                </tr>
                                                <tr>
                                                    <td align="left" scope="row" valign="top">
                                                        <asp:RadioButtonList ID="optQ9" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                            <asp:ListItem Value="1">ใช่ (1 คะแนน)</asp:ListItem>
                                                            <asp:ListItem Value="0" Selected="True">ไม่ใช่ (0 คะแนน)</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr class="MenuSt">
                                                    <th align="left" scope="row">6.คุณยังต้องสูบบุหรี่ หรือบุหรี่ไฟฟ้าอยู่ แม้จะเจ็บป่วยนอนพักตลอดบนเตียงนอน</th>
                                                </tr>
                                                <tr>
                                                    <td align="left" scope="row" valign="top">
                                                        <asp:RadioButtonList ID="optQ10" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                                                            <asp:ListItem Value="1">ต้องการ (1 คะแนน)</asp:ListItem>
                                                            <asp:ListItem Value="0" Selected="True">ไม่ต้องการ (0 คะแนน)</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <th align="left" scope="row">
                                                        <table>
                                                            <tr>
                                                                <td class="text-blue">การแปลผล FTND/eFTND</td>
                                                                <td>
                                                                    <asp:TextBox ID="txtFTND" runat="server" CssClass="text-center" Width="80px" Text="0"></asp:TextBox></td>
                                                                <td>คะแนน</td> 
                                                    <td align="left" class="small">(7 – 10 คะแนน ติดนิโคตินสูง ,4 - 6 คะแนน ติดนิโคตินปานกลาง ,น้อยกว่า 4 คะแนน ติดนิโคตินต่ำ)                                                  
                                                    </td>
                                                </tr>
                                                        </table>
                                                    </th>
                                                </tr>

                                              
                                                <tr class="MenuSt">
                                                    <th align="left" scope="row">7. การติดจากความเคยชิน/ สังคม</th>
                                                </tr>
                                                <tr>
                                                    <td align="left" scope="row" valign="top">
                                                        <asp:RadioButtonList ID="optQ12" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0" Selected="True">ไม่มี</asp:ListItem>
                                                            <asp:ListItem Value="1">มี</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr class="MenuSt">
                                                    <th align="left" scope="row">8.การติดด้านจิตใจ</th>
                                                </tr>
                                                <tr>
                                                    <td align="left" scope="row" valign="top">
                                                        <asp:RadioButtonList ID="optQ13" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0" Selected="True">ไม่มี</asp:ListItem>
                                                            <asp:ListItem Value="1">มี</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                    </td>
                                                </tr>
                                                <tr class="MenuSt">
                                                    <th align="left" scope="row">9. แรงจูงใจในการเลิกบุหรี่(ตอบได้มากกว่า1ข้อ)</th>
                                                </tr>
                                                <tr>
                                                    <td align="left" scope="row" valign="top">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBoxList ID="chkQ14" runat="server" RepeatDirection="Horizontal">
                                                                        <asp:ListItem>ครอบครัว</asp:ListItem>
                                                                        <asp:ListItem>สุขภาพ</asp:ListItem>
                                                                        <asp:ListItem>รู้สึกว่าสังคมไม่ยอมรับ</asp:ListItem>
                                                                        <asp:ListItem>เศรษฐกิจ</asp:ListItem>
                                                                        <asp:ListItem>อื่นๆระบุ</asp:ListItem>
                                                                    </asp:CheckBoxList></td>
                                                                <td valign="bottom">
                                                                    <asp:TextBox ID="txtQ14" runat="server" Width="200px"></asp:TextBox></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr class="MenuSt">
                                                    <th align="left" scope="row">10.ผลตรวจร่างกาย</th>
                                                </tr>

                                                <tr>
                                                    <td align="left" scope="row" valign="top">
                                                        <table>
                                                            <tr>
                                                                <td align="left">
                                                                    <table>
                                                                        <tr>
                                                                            <td align="left">น้ำหนัก</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtWeight" runat="server" CssClass="text-center" Width="50px"></asp:TextBox>
                                                                            </td>
                                                                            <td>กก. ส่วนสูง</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtHeigh" runat="server" AutoPostBack="True" CssClass="text-center" Width="50px"></asp:TextBox>
                                                                            </td>
                                                                            <td>ซม.</td>
                                                                            <td>ความดันโลหิต </td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtBP" runat="server" CssClass="text-center" Width="80px"></asp:TextBox>
                                                                            </td>
                                                                            <td>&nbsp;mmHg</td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                    <table>
                                                                        <tr>
                                                                            <td>PEFR</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtPEFR_Value" runat="server" CssClass="text-center" AutoPostBack="True" Width="80px"></asp:TextBox></td>
                                                                            <td>L/MIN</td>
                                                                            <td>ค่า % PEFR</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtPEFR_Rate" CssClass="text-center" runat="server" Width="80px"></asp:TextBox></td>

                                                                            <td colspan="2">ปริมาณก๊าซคาร์บอนมอนอกไซด์ในลมหายใจ</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtCO_Value" runat="server" CssClass="text-center" Width="60px"></asp:TextBox></td>
                                                                            <td>ppm</td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">&nbsp;</td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <th align="left" class="MenuSt" scope="row">11.วิธีการเลิกบุหรี่</th>
                    </tr>
                    <tr>
                        <td align="left">
                            <table>
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="optStopPlane1" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True" Value="1">ปรับพฤติกรรมและสร้างแรงจูงใจ</asp:ListItem>
                                            <asp:ListItem Value="2">ปรับพฤติกรรมและสร้างแรงจูงใจ + ใช้ยาช่วยเลิกบุหรี่</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>

                                </tr>
                            </table>
                        </td>
                    </tr>                
                    <tr>
                        <td align="left" class="MenuSt">ยาช่วยเลิกบุหรี่</td>
                    </tr>

                    <tr>
                        <td align="left" scope="row" valign="top">
                            <table width="100%" border="0" cellpadding="2" cellspacing="0">
                                <tr>
                                    <td>
                                        <table border="0" cellspacing="0" cellpadding="2">
                                            <tr>
                                                <td width="120"><b>ยาที่จ่าย</b></td>
                                                <td>
                                                    <asp:DropDownList CssClass="form-control select2" ID="ddlMed" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>อื่นๆ ระบุ</td>
                                                <td colspan="2">
                                                    <asp:TextBox ID="txtMedName" runat="server" Width="200px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>ขนาดรับประทาน</td>
                                                <td colspan="4">
                                                    <asp:TextBox ID="txtFrequency" runat="server" Width="100%"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>จำนวน</td>
                                                <td>
                                                    <asp:TextBox ID="txtMedQTY" runat="server" Width="60px" CssClass="text-center"></asp:TextBox>
                                                    &nbsp;&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblBalanceLabel" runat="server" Font-Size="8pt" ForeColor="Blue" Text="Balance = "></asp:Label>
                                                </td>
                                                <td colspan="4" class="small text-blue">1.กรณี จ่ายยาเป็นแผงให้ระบุจำนวนเม็ด &nbsp;
                                 2.กรณี สมุนไพรชงหญ้าดอกขาว ให้ระบุเป็นซอง ใน 1 ห่อมี 10 ซอง 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblBalance" runat="server" Font-Size="8pt" ForeColor="Blue"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <dx:ASPxButton ID="cmdAddMed" runat="server" Text="เพิ่มยา/บันทึกชื่อยา" Theme="Material" Width="100px">
                                                    </dx:ASPxButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="grdMed"
                                            runat="server" CellPadding="0" ForeColor="#333333"
                                            GridLines="None" CssClass="table table-hover"
                                            AutoGenerateColumns="False" Font-Bold="False" DataKeyNames="itemID" Width="100%">
                                            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:BoundField HeaderText="No." />
                                                <asp:BoundField DataField="ItemName" HeaderText="ยาที่จ่าย">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" Width="300px" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="จำนวน" DataField="QTY">
                                                    <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FrequencyDescription" HeaderText="ขนาดรับประทาน" />
                                                <asp:TemplateField HeaderText="ลบ">
                                                    <ItemTemplate>
                                                        <asp:ImageButton CssClass="gridbutton" ID="imgDel" runat="server"
                                                            ImageUrl="images/icon-delete.png"
                                                            CommandArgument='<%# Container.DataItemIndex %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle HorizontalAlign="Center"
                                                CssClass="dc_pagination dc_paginationC dc_paginationC11" />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <HeaderStyle Font-Bold="True" HorizontalAlign="Center"
                                                VerticalAlign="Middle" />
                                            <EditRowStyle BackColor="#2461BF" />
                                            <AlternatingRowStyle BackColor="White" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>

                            <table>
                                <tr>

                                    <td class="text-blue text-bold">12.วันกำหนดหยุดสูบ (Quit date) วันที่ </td>
                                    <td class="ml-2">
                                        <asp:TextBox ID="QuitPlanDate" runat="server" CssClass="form-control text-center" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>


                                    </td>
                                    <td>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                            ControlToValidate="QuitPlanDate" CssClass="text_red"
                                            ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="(\d{1}|\d{2})/(\d{1}|\d{2})/\d{4}"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="MenuSt">
                            <table>
                                <tr>
                                    <td>13.นัดติดตามอาการถอนนิโคตินและอาการข้างเคียงจากการใช้ยา ในช่วง 14 วันแรก หลังให้บริการ วันที่
                                    </td>
                                    <td>
                                        <asp:TextBox ID="xDateNextService" runat="server" CssClass="form-control text-center" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                            ControlToValidate="xDateNextService" CssClass="text_red"
                                            ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="(\d{1}|\d{2})/(\d{1}|\d{2})/\d{4}"></asp:RegularExpressionValidator>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="alert alert-success">
                        <td>
                            <table>
                                <tr class="text-bold text-blue">
                                    <td>สถานะ</td>
                                    <td>
                                        <asp:CheckBox ID="chkStatus" runat="server" Text="จบการทำงาน" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>                  
                </table>
            </div>
            <div class="box-footer clearfix">
            </div>
        </div>

        <div class="row text-center">
<asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="100px" />
        </div>
    </section>

</asp:Content>
