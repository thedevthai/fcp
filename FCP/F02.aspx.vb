﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO



Public Class F02
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlLct As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlOrder As New OrderController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If IsNothing(Request.Cookies("FCPCPA")) Then
        '    Response.Redirect("Default.aspx?logout=YES")
        'End If
        If Not IsPostBack Then
            'LoadProvinceToDDL()
            pnEating.Visible = False
            pnFat.Visible = False

            txtBegin.Enabled = True
            txtEnd.Enabled = True
            pnBehavior.Visible = True
            pnAlertB.Visible = False
            If Request("acttype") = "view" Then
                cmdSave.Visible = False
                cmdClear.Visible = False
            Else
                cmdSave.Visible = True
                cmdClear.Visible = True
            End If
            LoadProblemRelate()
            BindFinalResultToRadioList()
            LoadPharmacist(Request.Cookies("LocationID").Value)
            LoadFormData()

            ' LockControls()

        End If

        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        ''txtCardID.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
      
    End Sub
    Private Sub LoadPharmacist(LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With
            End If
        End If
        dtP = Nothing
    End Sub

    Private Sub LoadLocationData(LocationID As String)
        dt = ctlLct.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblLocationID.Text = .Item("LocationID")
                lblLocationName.Text = .Item("LocationName")
                lblLocationProvince.Text = .Item("ProvinceName")
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadFormData()

        Dim pYear As Integer
        If Not Request("fid") Is Nothing Then
            dt = ctlOrder.GetOrder_ByID(StrNull2Zero(Request("fid")))
        Else
            If Not Request("yid") Is Nothing Then
                pYear = CInt(Request("yid"))
            Else
                pYear = Year(ctlLct.GET_DATE_SERVER)
                If pYear < 2500 Then
                    pYear = pYear + 543
                End If

            End If
            dt = ctlOrder.LocationOrder_ByYear(pYear, FORM_TYPE_ID_F02, Request.Cookies("LocationID").Value, Session("patientid"))
        End If


        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                'pnBehavior.Visible = True
                If Request("t") = "new" Then
                    'pnBehavior.Visible = False
                    lblID.Text = ""
                    lblRefID.Text = String.Concat(.Item("itemID"))
                ElseIf Request("t") = "edit" Then
                    lblID.Text = String.Concat(.Item("itemID"))
                    lblRefID.Text = String.Concat(.Item("RefID"))
                ElseIf Request("t") Is Nothing Then
                    lblID.Text = ""
                    lblRefID.Text = String.Concat(.Item("itemID"))
                End If
                Session("patientid") = DBNull2Lng(.Item("PatientID"))
                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                txtTime.Text = String.Concat(.Item("ServiceTime"))
                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))

                chkClose.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("Status")))

                chkProblem1.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem1")))
                chkProblem2.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem2")))
                chkProblem3.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem3")))
                chkProblem4.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem4")))
                chkProblem5.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem5")))
                chkProblem6.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem6")))
                chkProblem7.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblemOther")))
                txtProblemOther.Text = DBNull2Str(.Item("ProblemRemark"))
                chkEdu1.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate1")))
                chkEdu2.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate2")))
                chkEdu3.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate3")))
                chkEdu4.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate4")))
                chkEdu5.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate5")))
                chkEdu6.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate6")))
                chkEdu7.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducateOther")))
                txtEduOther.Text = DBNull2Str(.Item("EducateRemark"))


                'chkNHSO1.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO1")))
                'chkNHSO2.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO2")))
                'chkNHSO3.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO3")))
                'chkNHSO4.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO4")))
                'chkNHSO5.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO5")))
                'chkNHSO6.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO6")))
                'chkNHSO7.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO7")))
                'chkNHSO8.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO8")))

                lblServicePlan.Text = ""

                Dim sPlan() As String
                sPlan = Split(String.Concat(.Item("ServicePlan")), "|")
                For i = 0 To sPlan.Length - 1
                    Select Case sPlan(i)
                        Case "1"
                            lblServicePlan.Text = "ให้ความรู้เรื่องโรคและพฤติกรรมความเสี่ยงครั้งที่ 1"
                        Case "2"
                            lblServicePlan.Text &= " , ให้ความรู้เรื่องโรคและพฤติกรรมความเสี่ยงครั้งที่ 2"
                        Case "3"
                            lblServicePlan.Text &= " , Refer เพื่อConfirm โดยแพทย์"
                    End Select
                Next


                lblHospitalName.Text = String.Concat(.Item("HospitalName"))

                'lblTimeNo.Text = "2"

                If DBNull2Zero(.Item("Status")) >= 3 Then
                    cmdSave.Visible = False
                    cmdClear.Visible = False
                Else
                    If Request.Cookies("LocationID").Value <> String.Concat(.Item("LocationID")) Then
                        LoadLocationData(String.Concat(.Item("LocationID")))
                        If Request("t") = "edit" Then
                            cmdSave.Visible = True
                            cmdClear.Visible = True
                        Else
                            cmdSave.Visible = False
                            cmdClear.Visible = False
                        End If
                    Else
                        LoadLocationData(Request.Cookies("LocationID").Value)
                    End If
                End If

            End With
            LoadBehaviorProblemToGrid()
        Else
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing

    End Sub

    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click

        lblID.Text = ""
        txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)
        txtTime.Text = ""
        ddlPerson.SelectedIndex = 0
       
        chkClose.Checked = False
        chkProblem1.Checked = False
        chkProblem2.Checked = False
        chkProblem3.Checked = False
        chkProblem4.Checked = False
        chkProblem5.Checked = False
        chkProblem6.Checked = False
        chkProblem7.Checked = False
        txtProblemOther.Text = ""
        chkEdu1.Checked = False
        chkEdu2.Checked = False
        chkEdu3.Checked = False
        chkEdu4.Checked = False
        chkEdu5.Checked = False
        chkEdu6.Checked = False
        chkEdu7.Checked = False
        txtEduOther.Text = ""

        'chkNHSO1.Checked = False
        'chkNHSO2.Checked = False
        'chkNHSO3.Checked = False
        'chkNHSO4.Checked = False
        'chkNHSO5.Checked = False
        'chkNHSO6.Checked = False
        'chkNHSO7.Checked = False
        'chkNHSO8.Checked = False


    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        'If Trim(txtMobile.Text) = "" Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนเบอร์มือถือผู้รับบริการก่อน หากไม่มีให้ใส่เบอร์บ้านในช่องเบอร์มือถือได้")
        '    Exit Sub
        'End If
        If StrNull2Zero(txtTime.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนระยะเวลาในการให้บริการก่อน');", True)
            Exit Sub
        End If
        'If StrNull2Zero(txtAges.Text) = 0 Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนอายุผู้รับบริการก่อน');", True)
        '    Exit Sub
        'End If
        Dim objuser As New UserController
        Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))
        ''Dim BirthDate As String = ConvertStrDate2DBString(txtBirthDate.Text)

        'Dim Fname, Lname As String
        'Dim sName(1) As String
        'sName(0) = ""
        'sName(1) = ""
        'sName = Split(txtName.Text, " ")
        'Fname = sName(0)
        'Lname = ""
        'If sName.Length > 1 Then
        '    Lname = sName(1)
        'End If


        If lblID.Text = "" Then 'Add new

            If ctlOrder.Order_ChkDupCustomer(FORM_TYPE_ID_F02, Session("patientid"), StrNull2Zero(Left(Str(ServiceDate), 4))) = True Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ผู้รับบริการรายนี้เคยได้รับบริการในกิจกรรมนี้แล้ว');", True)
                Exit Sub
            End If

            ctlOrder.F02_Add(lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F02, StrNull2Zero(lblRefID.Text), 1, 1, Boolean2Decimal(chkProblem1.Checked), Boolean2Decimal(chkProblem2.Checked), Boolean2Decimal(chkProblem3.Checked), Boolean2Decimal(chkProblem4.Checked), Boolean2Decimal(chkProblem5.Checked), Boolean2Decimal(chkProblem6.Checked), Boolean2Decimal(chkProblem7.Checked), txtProblemOther.Text, Boolean2Decimal(chkEdu1.Checked), Boolean2Decimal(chkEdu2.Checked), Boolean2Decimal(chkEdu3.Checked), Boolean2Decimal(chkEdu4.Checked), Boolean2Decimal(chkEdu5.Checked), Boolean2Decimal(chkEdu6.Checked), Boolean2Decimal(chkEdu7.Checked), txtEduOther.Text, Convert2Status(chkClose.Checked), Request.Cookies("username").Value, CLng(ConvertDate2DBString(ctlLct.GET_DATE_SERVER)), 0, 0, 0, 0, 0, 0, 0, 0, Session("patientid"), Session("sex"), Session("age"), "", 0, "")


            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Service_Order,Metabolic_Risk", "การให้ความรู้และคำแนะนำปรึกษาในกลุ่ม Metabolic Syndrome (F02) :<<PatientID:" & Session("patientid") & ">> ID:" & lblID.Text, "F02")

        Else
            ctlOrder.F02_Update(lblID.Text, lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F02, StrNull2Zero(lblRefID.Text), 1, 1, Boolean2Decimal(chkProblem1.Checked), Boolean2Decimal(chkProblem2.Checked), Boolean2Decimal(chkProblem3.Checked), Boolean2Decimal(chkProblem4.Checked), Boolean2Decimal(chkProblem5.Checked), Boolean2Decimal(chkProblem6.Checked), Boolean2Decimal(chkProblem7.Checked), txtProblemOther.Text, Boolean2Decimal(chkEdu1.Checked), Boolean2Decimal(chkEdu2.Checked), Boolean2Decimal(chkEdu3.Checked), Boolean2Decimal(chkEdu4.Checked), Boolean2Decimal(chkEdu5.Checked), Boolean2Decimal(chkEdu6.Checked), Boolean2Decimal(chkEdu7.Checked), txtEduOther.Text, Convert2Status(chkClose.Checked), Request.Cookies("username").Value, 0, 0, 0, 0, 0, 0, 0, 0, Session("patientid"), "", 0, "")
            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Service_Order,Metabolic_Risk", "การให้ความรู้และคำแนะนำปรึกษาในกลุ่ม Metabolic Syndrome (F02) :<<PatientID:" & Session("patientid") & ">>ID:" & lblID.Text, "F02")
        End If
        ctlOrder.F01_UpdateEducateCount(StrNull2Zero(lblRefID.Text), 1)
        ctlOrder.F01_UpdateCloseStatus(StrNull2Zero(lblRefID.Text), Convert2Status(chkClose.Checked), Request.Cookies("username").Value)

        Response.Redirect("ResultPage.aspx")

        ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย")
    End Sub
#Region "ปัญหาพฤติกรรม"

    Private Sub LoadProblemRelate()
        Dim dtPG As New DataTable
        Dim ctlP As New ProblemController
        ddlBehavior.Items.Clear()
        dtPG = ctlP.BehaviorProblem_Get
        If dtPG.Rows.Count > 0 Then
            ddlBehavior.DataSource = dtPG
            ddlBehavior.DataTextField = "Descriptions"
            ddlBehavior.DataValueField = "UID"
            ddlBehavior.DataBind()
        End If
        dtPG = Nothing
    End Sub

    Private Sub BindFinalResultToRadioList()
        If chkNotFollow.Checked Then
            'optFat.Enabled = False
            optResultB.Enabled = False
            txtResultOtherB.Enabled = False
            txtBegin.Enabled = False
            txtEnd.Enabled = False
        Else
            optFat.Enabled = True
            optResultB.Enabled = True
            txtResultOtherB.Enabled = True
            txtBegin.Enabled = True
            txtEnd.Enabled = True
            optResultB.Items.Clear()
            pnFat.Visible = False
            pnEating.Visible = False
            Select Case ddlBehavior.SelectedValue
                Case "1" 'การสูบบุหรี่
                    optResultB.Items.Add("สูบลดลง")
                    optResultB.Items.Add("สูบเท่าเดิม")
                    optResultB.Items.Add("สูบเพิ่มขึ้น")
                    optResultB.Items.Add("เลิกสูบแล้ว")
                    optResultB.Items.Add("อื่นๆ")
                Case "2" 'การดื่มเหล้า
                    optResultB.Items.Add("ดื่มลดลง")
                    optResultB.Items.Add("ดื่มเพิ่มขึ้น")
                    optResultB.Items.Add("คงเดิม")
                    optResultB.Items.Add("อื่นๆ")
                Case "3" 'ความอ้วน
                    pnFat.Visible = True
                    optResultB.Items.Add("ลดลง")
                    optResultB.Items.Add("เพิ่มขึ้น")
                    optResultB.Items.Add("คงเดิม")
                    optResultB.Items.Add("อื่นๆ")
                Case "4" 'การออกกำลังกาย
                    optResultB.Items.Add("ไม่ได้ออกกำลังกายเพิ่ม(เหมือนเดิม)")
                    optResultB.Items.Add("ออกกำลังกายเพิ่มขึ้น")
                    optResultB.Items.Add("ออกกำลังกายลดลง")
                    optResultB.Items.Add("จำนวนครั้งเท่าเดิม แต่เพิ่มเวลาในแต่ละครั้ง")
                    optResultB.Items.Add("อื่นๆ")
                Case "5" 'การรับประทานอาหาร

                    pnEating.Visible = True
                    optResultB.Items.Add("ดีขึ้น")
                    optResultB.Items.Add("แย่ลง")
                    optResultB.Items.Add("เท่าเดิม")
                    optResultB.Items.Add("อื่นๆ")

                    txtBegin.Enabled = False
                    txtEnd.Enabled = False
                Case "6" 'ความเครียด
                    optResultB.Items.Add("ลดลง")
                    optResultB.Items.Add("เพิ่มขึ้น")
                    optResultB.Items.Add("คงเดิม")
                    optResultB.Items.Add("อื่นๆ")
                Case Else
                    optResultB.Items.Add("ลดลง")
                    optResultB.Items.Add("เพิ่มขึ้น")
                    optResultB.Items.Add("คงเดิม")
                    optResultB.Items.Add("อื่นๆ")
            End Select
        End If
    End Sub

    Protected Sub ddlBehavior_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBehavior.SelectedIndexChanged
        BindFinalResultToRadioList()
    End Sub

    Protected Sub cmdAddBehavior_Click(sender As Object, e As EventArgs) Handles cmdAddBehavior.Click

        lblAlertB.Visible = True
        lblAlertB.Text = ""

        If ddlBehavior.SelectedValue = "99" Then ' อื่นๆ
            If txtProblemBehaviorOther.Text.Trim = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุปัญหาเรื่องอื่นๆก่อน');", True)
                Exit Sub
            End If
        End If


        If chkNotFollow.Checked Then
            'optFat.Enabled = False
            optResultB.Enabled = False
            txtResultOtherB.Enabled = False
            txtBegin.Enabled = False
            txtEnd.Enabled = False
        Else
            optFat.Enabled = True
            optResultB.Enabled = True
            txtResultOtherB.Enabled = True
            txtBegin.Enabled = True
            txtEnd.Enabled = True
            'optResultB.Items.Clear()
            pnFat.Visible = False
            pnEating.Visible = False

            optResultB.Visible = True


            Select Case ddlBehavior.SelectedValue
                Case "1" 'การสูบบุหรี่

                Case "2" 'การดื่มเหล้า

                Case "3" 'ความอ้วน
                    pnFat.Visible = True
                    If optFat.SelectedIndex = -1 Then
                        lblAlertB.Text = "ท่านยังไม่ได้เลือกสิ่งที่พบ"
                        pnAlertB.Visible = True
                        Exit Sub
                    End If

                Case "4" 'การออกกำลังกาย

                Case "5" 'การรับประทานอาหาร
                    pnEating.Visible = True
                    If optTast.SelectedIndex = -1 Then
                        lblAlertB.Text = "ท่านยังไม่ได้เลือกรส"
                        pnAlertB.Visible = True
                        Exit Sub
                    End If




                Case "6" 'ความเครียด

                Case Else

            End Select

            If optResultB.SelectedIndex = -1 Then
                lblAlertB.Text = "ท่านยังไม่ได้เลือกผลการติดตาม"
                pnAlertB.Visible = True
                Exit Sub
            End If

        End If


        If txtInterventionB.Text = "" Then
            lblAlertB.Text = "ท่านยังไม่ได้ป้อน Interventions"
            lblAlertB.Visible = True
            pnAlertB.Visible = True
            Exit Sub
        End If

        Dim isFollow, ResultB, ResultOtherB, ResultBegin, ResultEnd As String
        If chkNotFollow.Checked Then ' not follow
            isFollow = "N"
            ResultB = ""
            ResultOtherB = ""
            ResultBegin = ""
            ResultEnd = ""
        Else
            isFollow = "Y"
            ResultB = optResultB.SelectedValue
            ResultOtherB = txtResultOtherB.Text
            ResultBegin = txtBegin.Text
            ResultEnd = txtEnd.Text
        End If

        ctlOrder.BehaviorRelate_Save(StrNull2Zero(lblID.Text), StrNull2Zero(lblUID_B.Text), ddlBehavior.SelectedValue, txtProblemBehaviorOther.Text, txtInterventionB.Text, ResultB, ResultOtherB, ResultBegin, ResultEnd, optFat.SelectedValue, ResultOtherB, isFollow, Request.Cookies("UserID").Value, FORM_TYPE_ID_F02, StrNull2Zero(Session("patientid")))

        LoadBehaviorProblemToGrid()
        ClearTextBehavior()

        pnAlertB.Visible = False
    End Sub
    Private Sub LoadBehaviorProblemToGrid()
        dt = ctlOrder.BehaviorRelate_Get(StrNull2Zero(Session("patientid")), FORM_TYPE_ID_F02)
        grdBehavior.DataSource = dt
        grdBehavior.DataBind()

        For i = 0 To dt.Rows.Count - 1
            If String.Concat(dt.Rows(i)("ServiceDate")) = "" Then
                grdBehavior.Rows(i).Cells(1).Text = txtServiceDate.Text
            End If
        Next

    End Sub

    Private Sub ClearTextBehavior()
        lblUID_B.Text = "0"
        ddlBehavior.SelectedIndex = 0
        BindFinalResultToRadioList()

        txtProblemBehaviorOther.Text = ""
        txtInterventionB.Text = ""
        txtResultOtherB.Text = ""
        txtBegin.Text = ""
        txtEnd.Text = ""
        chkNotFollow.Checked = False
        cmdAddBehavior.Text = "เพิ่ม"
    End Sub
    Private Sub grdBehavior_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdBehavior.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit_B"
                    EditBehaviorProblem(e.CommandArgument())
                Case "imgDel_B"
                    ctlOrder.BehaviorRelate_Delete(e.CommandArgument())
                    LoadBehaviorProblemToGrid()
                    ClearTextBehavior()
            End Select
        End If
    End Sub
    Protected Sub grdBehavior_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdBehavior.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(6).FindControl("imgDel_B")
            imgD.Attributes.Add("onClick", scriptString)

            e.Row.Cells(0).Text = e.Row.RowIndex + 1
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub
    Private Sub EditBehaviorProblem(UID As Integer)
        dt = ctlOrder.BehaviorRelate_GetByUID(UID)
        If dt.Rows.Count > 0 Then

            lblUID_B.Text = dt.Rows(0)("UID")
            ddlBehavior.SelectedValue = DBNull2Str(dt.Rows(0)("ProblemUID"))
            BindFinalResultToRadioList()
            txtProblemBehaviorOther.Text = DBNull2Str(dt.Rows(0)("ProblemOther"))
            txtInterventionB.Text = DBNull2Str(dt.Rows(0)("Interventions"))
            txtBegin.Text = DBNull2Str(dt.Rows(0)("ResultBegin"))
            txtEnd.Text = DBNull2Str(dt.Rows(0)("ResultEnd"))
            Select Case ddlBehavior.SelectedValue
                'Case "1" 'การสูบบุหรี่

                'Case "2" 'การดื่มเหล้า

                Case "3" 'ความอ้วน
                    pnFat.Visible = True
                    optFat.SelectedValue = DBNull2Str(dt.Rows(0)("FatFollow"))
                Case "4" 'การรับประทานอาหาร
                    txtResultOtherB.Text = DBNull2Str(dt.Rows(0)("Remark"))
                    txtBegin.Enabled = False
                    txtEnd.Enabled = False
                    'Case "5" 'การออกกำลังกาย

                    'Case "6" 'ความเครียด

            End Select

            If DBNull2Str(dt.Rows(0)("isFollow")) = "N" Then
                chkNotFollow.Checked = True
            Else
                chkNotFollow.Checked = False
                If DBNull2Str(dt.Rows(0)("FinalResult")) <> "" Then
                    optResultB.SelectedValue = DBNull2Str(dt.Rows(0)("FinalResult"))
                End If
                txtResultOtherB.Text = DBNull2Str(dt.Rows(0)("FinalResultOther"))
            End If

            cmdAddBehavior.Text = "บันทึก"
        End If
    End Sub

    Protected Sub chkNotFollow_CheckedChanged(sender As Object, e As EventArgs) Handles chkNotFollow.CheckedChanged
        BindFinalResultToRadioList()
    End Sub
#End Region

End Class