<%@ Page Title="" Language="vb" AutoEventWireup="false"  MasterPageFile="~/SiteCPA.Master" CodeBehind="RegisterDetail.aspx.vb" Inherits=".RegisterDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

       <section class="content-header">
      <h1>Register Detail
        <small>(�����ż��ŧ����¹����ç���)</small>
      </h1> 
    </section>
       
<section class="content">  
    
  <div class="box box-primary">
            <div class="box-header">
              <i class="lnr-store"></i>

              <h3 class="box-title">������ŧ����¹</h3>
             
                 <div class="box-tools pull-right"> 
                            <asp:Label ID="lblNewCode" runat="server" CssClass="small"></asp:Label>            
              </div>

                 
                <asp:HiddenField ID="hdUID" runat="server" />

                 
            </div>
            <div class="box-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="p-2">
                                     <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>
                                                    <asp:Label ID="lblLicensTxt" runat="server" Text="�Ţ��� ��.5/�Ţ���㺻�Сͺ"></asp:Label></label>
                                                <asp:TextBox ID="txtLicenseNo" runat="server" CssClass="form-control text-center text-primary"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>������ҹ��/���Ѫ������/���� þ.</label><asp:TextBox ID="txtLocationName" runat="server" CssClass="form-control" placeholder=""></asp:TextBox>
                                            </div>
                                        </div>

                                          <div id="pnSpecName" runat="server" class="col-md-3">
                                            <div class="form-group">
                                                <label>�к�</label>
                                                <asp:TextBox ID="txtSpecName" runat="server" CssClass="form-control text-center text-primary"  placeholder="�к� þ. ���� ���� " ></asp:TextBox>
                                            </div>
                                        </div>

                                         <div class="col-md-2">
                                            <div class="form-group">
                                                <label>����˹��º�ԡ�� ʻʪ.</label>
                                                <asp:TextBox ID="txtNHSOCode" runat="server" CssClass="form-control text-center text-primary"  placeholder="DXXXX" ></asp:TextBox>
                                            </div>
                                        </div>
                                         
                                    </div>
                                      <div class="row">   
                                          <div class="col-md-6">
                                            <div class="form-group">
                                                <label>��������ҹ��</label>
                                                <asp:RadioButtonList ID="optLocationType" runat="server" RepeatDirection="Horizontal"></asp:RadioButtonList>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>�������ҹ��</label>
                                                <asp:DropDownList ID="ddlGroup" runat="server" CssClass="form-control select2"></asp:DropDownList>
                                            </div>
                                        </div>
                                     
                                    </div>      
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>ʶҹ������Ţ���</label>
                                                <asp:TextBox ID="txtAddressNo" runat="server" CssClass="form-control" TextMode="MultiLine" Height="60" placeholder="�������"></asp:TextBox>
                                            </div>
                                        </div>                                       
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>�ѧ��Ѵ</label>
                                                <asp:DropDownList CssClass="form-control select2" ID="ddlProvince" runat="server"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>������ɳ���</label>
                                                <asp:TextBox ID="txtZipCode" runat="server" CssClass="form-control" MaxLength="5"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>���������͡�û�Ъ�����ѹ����ҹ</label>
                                                <asp:TextBox ID="txtTel" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>E-mail</label>
                                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control special-letter"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Line ID</label>
                                                <asp:TextBox ID="txtLineID" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>�еԨٴ,�ͧ�Ԩٴ (��ٻẺͧ�ҷȹ�����ҹ�� ��Ф�蹪ش�����Ŵ��¤����� (,)) </label>
                                                <asp:TextBox ID="txtLat" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>���ҷӡ����ҹ</label>
                                                <asp:TextBox ID="txtWorkTime" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>�� �.�.�����Ѥ�����ç���</label>
                                                <asp:TextBox ID="txtQAYear" runat="server" CssClass="form-control text-center" Text="2566" placeholder=""></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>                                
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>������Ңͧ(����ѭ��)</label>
                                                <asp:TextBox ID="txtCo_Name" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>        
                                          <div class="col-md-3">
                                            <div class="form-group">
                                                <label>�Ţ���㺻�Сͺ�ԪҪվ</label>
                                                <asp:TextBox ID="txtCo_LicenseNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>        
                                         <div class="col-md-3">
                                            <div class="form-group">
                                                <label>������</label>
                                                <asp:TextBox ID="txtCo_Tel" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>E-mail</label>
                                                <asp:TextBox ID="txtCo_Mail" runat="server" CssClass="form-control special-letter"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                                              
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <div class="row">
        <!-- Left col -->
        <section class="col-lg-6 connectedSortable">     
      <div class="box box-success">
            <div class="box-header">
              <i class="lnr-store"></i>
              <h3 class="box-title">��˹��Է����ç���</h3>                      
            </div>
            <div class="box-body alert alert-success">
                 <asp:CheckBoxList ID="chkProject" runat="server"  RepeatColumns="2">
                                                    <asp:ListItem Value="1">F : �ç��������й� ��֡���</asp:ListItem>
                                                    <asp:ListItem Value="2">A : �ç��ú�����</asp:ListItem>
                                                    <asp:ListItem Value="3">M : �ç�����������ҹ ��� MTM</asp:ListItem>
                                                    <asp:ListItem Value="4">H : Home Isolation (Covid-19)</asp:ListItem>

                                                </asp:CheckBoxList>
               </div>
          </div>

            </section>
                <section class="col-lg-6 connectedSortable">     
      <div class="box box-success">
            <div class="box-header">
              <i class="lnr-store"></i>
              <h3 class="box-title">�����ż����ҹ</h3>                      
            </div>
            <div class="box-body">                
                <table class="table">
                    <tr>
                        <td class="text-right">Username</td>
                         <td><asp:TextBox ID="txtLocationID" runat="server" CssClass="form-control text-center text-primary" ReadOnly="true" placeholder="���ʷ������ѧ�ҡ͹��ѵ�" ></asp:TextBox></td>
                         <td class="text-right">Password</td>
                         <td><asp:TextBox ID="txtPassword" runat="server" CssClass="form-control text-center text-primary" ReadOnly="true"></asp:TextBox></td>
                    </tr>
                </table>
               </div>
         
          </div>

            </section>
            </div>


        <div class="row text-center">
                                        
                                        <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-success" Text="͹��ѵ����ҧ User ����" />
             <asp:Button ID="cmdReject" runat="server" CssClass="btn btn-warning" Text="���͹��ѵ�" Width="100" />
             <asp:Button ID="cmdDelete" runat="server" CssClass="btn btn-danger" Text="ź" Width="100" />
                                   <asp:Button ID="cmdSendMail" runat="server" CssClass="btn btn-primary" Text="���������� Username" />
                                              </div>
    
         </section>

      
</asp:Content>
