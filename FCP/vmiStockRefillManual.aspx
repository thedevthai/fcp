﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="vmiStockRefillManual.aspx.vb" Inherits=".vmiStockRefillManual" %>


<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
   
 
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>Stock Refill 
        <small>: เติมยาให้ร้านยา</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
  <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">เติมยาให้ร้านยา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">




    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td align="left" valign="top">
              <dx:ASPxPageControl ID="tabPageSearch" runat="server" ActiveTabIndex="0" BackColor="#DDE0E3" EnableTheming="True" Theme="MetropolisBlue" Width="100%" Font-Size="12pt">
                  <TabPages>
                      <dx:TabPage Name="pSearch" Text="ค้นหา / Search">
                          <ContentCollection>
                              <dx:ContentControl runat="server">
                                  <table border="0" cellpadding="1" cellspacing="1">
                                      <tr>
                                          <td align="left" class="text12_gray">ชื่อยา : </td>
                                          <td align="left" class="text12_gray" >
                                              <asp:TextBox ID="txtMedName" runat="server" Width="200px" CssClass="text12_gray"></asp:TextBox>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td align="left" class="text12_gray">ร้านยา :</td>
                                          <td align="left" class="text12_gray">
                                              <asp:TextBox ID="txtLocationName" runat="server" CssClass="text12_gray" Width="200px"></asp:TextBox>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td align="left" class="text12_gray">จังหวัด</td>
                                          <td align="left" class="text12_gray">
                                              <asp:DropDownList ID="ddlProvince" runat="server" Width="210px" CssClass="text12_gray">
                                              </asp:DropDownList>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td align="center" class="text12_gray" colspan="2">
                                              <asp:Button ID="cmdSearch" runat="server" CssClass="buttonFind" Text="Search" Width="100px" />
                                          </td>
                                      </tr>
                                  </table>
                              </dx:ContentControl>
                          </ContentCollection>
                      </dx:TabPage>
                      <dx:TabPage Name="pNew" Text="เติมยา / Refill">
                          <ContentCollection>
                              <dx:ContentControl runat="server">
                                  <table border="0" cellspacing="2" cellpadding="2">
                                      <tr>
                                        <td width="100" class="text12_gray">Ref.ID</td>
                                        <td width="250" class="text12_gray">
                                            <asp:Label ID="lblUID" runat="server"></asp:Label>
                                          </td>
                                        <td width="70" class="text12_gray">&nbsp;</td>
                                        <td class="text12_gray">
                                            &nbsp;</td>
                                      </tr>
                                      <tr>
                                          <td class="text12_gray" width="100">รหัสร้านยา</td>
                                          <td class="text12_gray" width="250">
                                              <asp:Label ID="lblLocationID" runat="server"></asp:Label>
                                          </td>
                                          <td class="text12_gray" width="100">ชื่อร้านยา</td>
                                          <td class="text12_gray">
                                              <asp:Label ID="lblLocationName" runat="server"></asp:Label>
                                          </td>
                                      </tr>
                                      <tr>
                                        <td class="text12_gray">ยา</td>
                                        <td class="text12_gray">
                                            [<asp:Label ID="lblMedUID" runat="server"></asp:Label>
                                            ]
                                            <asp:Label ID="lblMedName" runat="server"></asp:Label>
                                          </td>
                                        <td class="text12_gray">หน่วย</td>
                                        <td class="text12_gray">
                                            <asp:Label ID="lblUOM" runat="server"></asp:Label>
                                          </td>
                                      </tr>
                                      <tr>
                                        <td class="text12_gray">Stock</td>
                                        <td class="text12_gray">
                                            <asp:Label ID="lblStock" runat="server"></asp:Label>
                                          </td>
                                        <td class="text12_gray">ROP</td>
                                        <td class="text12_gray">
                                            <asp:Label ID="lblROP" runat="server"></asp:Label>
                                          </td>
                                      </tr>
                                      <tr>
                                        <td class="text12_gray">On hand</td>
                                        <td class="text12_gray">
                                            <asp:Label ID="lblOnHand" runat="server"></asp:Label>
                                          </td>
                                        <td class="text12_blue">จำนวนเติมยา</td>
                                        <td>
                                            <asp:TextBox ID="txtQTY" runat="server" Font-Bold="True" Font-Size="12pt" ForeColor="Blue" Height="25px" MaxLength="6" Width="100px"></asp:TextBox>
                                          </td>
                                      </tr>
                                      <tr>
                                        <td colspan="4" align="center">
                                            <asp:Button ID="cmdSave" runat="server" CssClass="buttonRedial " Text="Save" Width="100px" />
                                            &nbsp;<asp:Button ID="cmdCancel" runat="server" CssClass="buttonRedial " Text="Cancel" Width="100px" />
                                          </td>
                                      </tr>
                                    </table>
                              </dx:ContentControl>
                          </ContentCollection>
                      </dx:TabPage>
                  </TabPages>
                  <Paddings Padding="0px" />
                  <TabStyle Font-Bold="True" Font-Size="11pt">
                  </TabStyle>
                  <Border BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" />
              </dx:ASPxPageControl>
            </td>
      </tr>
       <tr>
          <td align="left" valign="top" class="MenuSt"> &nbsp;&nbsp;&nbsp;&nbsp;รายการสต๊อกยา</td>
      </tr>
        <tr>
          <td align="center" valign="top">
              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="No.">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />                      </asp:BoundField>
            <asp:BoundField DataField="LocationName" HeaderText="ร้านยา">
              <headerstyle HorizontalAlign="Center" />          
              <itemstyle VerticalAlign="Middle" />                      </asp:BoundField>
            <asp:BoundField DataField="ProvinceName" HeaderText="จังหวัด">
              <headerstyle HorizontalAlign="Left" />          
              <itemstyle HorizontalAlign="Left" />                      </asp:BoundField>
                <asp:BoundField DataField="itemName" HeaderText="ชื่อยา" >
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Stock" HeaderText="Stock" />
                <asp:BoundField DataField="ROP" HeaderText="ROP" />
                <asp:BoundField DataField="OnHand" HeaderText="On hand" />
            <asp:TemplateField HeaderText="Refill">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgEdit" runat="server" ImageUrl="images/med_icon.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID")%>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC12" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView></td>
      </tr>
       
    </table>
     </div>
            <div class="box-footer clearfix">
           
            </div>
          </div> 
    </section>
</asp:Content>
