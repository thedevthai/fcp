﻿Public Class ReportConditionSummary
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlType As New ServiceTypeController
    Dim objLct As New LocationInfo
    Dim ctlLct As New LocationController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            LoadProvinceToDDL()

            Dim m, y As Integer
            y = Year(Date.Now)
            m = Month(Date.Now)
            If y < 2300 Then
                y = y + 543
            End If

            txtStartDate.Text = "01/" & m & "/" & y 'Date.Now.AddDays(-30).ToString("dd/MM/yyyy", DateFormat_TH)
            txtEndDate.Text = Date.DaysInMonth(y, m) & "/" & m & "/" & y  'Date.Now.ToString("dd/MM/yyyy", DateFormat_TH)

        End If



        'If Request("ItemType") = "fnc" Then
        '    cmdPrint.Attributes.Add("onClick", "window.open('" + ResolveUrl("rptFinanceSummarize.aspx?b=" & txtStartDate.Text & "&e=" & txtEndDate.Text & "', 'windowname', 'width=800,height=600,scrollbars=yes')"))
        '    cmdExcel.Attributes.Add("onClick", "window.open('" + ResolveUrl("rptFinanceSummarize.aspx?b=" & txtStartDate.Text & "&e=" & txtEndDate.Text & "&ex=1', 'windowname', 'width=800,height=600,scrollbars=yes')"))
        'Else
        '    cmdPrint.Attributes.Add("onClick", "window.open('" + ResolveUrl("ReportViewer.aspx?t=" & ddlType.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text & "', 'windowname', 'width=800,height=600,scrollbars=yes')"))
        'End If


    End Sub
    Private Sub LoadProvinceToDDL()
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            dt = ctlLct.Province_GetInGroup(Request.Cookies("RPTGRP").Value)
        Else
            dt = ctlLct.Province_GetInLocation
        End If

        ddlProvince.Items.Clear()
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .Items.Add("---ทั้งหมด---")
                If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
                    .Items(0).Value = Request.Cookies("RPTGRP").Value
                Else
                    .Items(0).Value = "0"
                End If

                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)("ProvinceName"))
                    .Items(i + 1).Value = dt.Rows(i)("ProvinceID")
                Next
                .SelectedIndex = 0

            End With
        End If
        dt = Nothing
    End Sub

    Protected Sub cmdPrint_Click(sender As Object, e As EventArgs) Handles cmdPrint.Click

        FagRPT = ""

        Dim dStart As String = ConvertStrDate2DBString(txtStartDate.Text)
        Dim dEnd As String = ConvertStrDate2DBString(txtStartDate.Text)

        Dim rptGRP As String = "ALL"
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            rptGRP = Request.Cookies("RPTGRP").Value
        End If

        Response.Redirect("Reports/rptFinalSumaryByCustomer.aspx?grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)

    End Sub

    Protected Sub cmdExcel_Click(sender As Object, e As EventArgs) Handles cmdExcel.Click

        Dim rptGRP As String = "ALL"
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            rptGRP = Request.Cookies("RPTGRP").Value
        End If

        Response.Redirect("Reports/rptFinalSumaryByCustomer.aspx?ex=1&grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
    End Sub

End Class