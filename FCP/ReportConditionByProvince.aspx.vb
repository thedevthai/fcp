﻿Public Class ReportConditionByProvince
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlType As New ServiceTypeController
    Dim objLct As New LocationInfo
    Dim ctlLct As New LocationController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            LoadProvinceToDDL()

            Dim m, y As Integer
            y = Year(Date.Now)
            m = Month(Date.Now)
            If y < 2300 Then
                y = y + 543
            End If

            txtStartDate.Text = "01/" & m & "/" & y 'Date.Now.AddDays(-30).ToString("dd/MM/yyyy", DateFormat_TH)
            txtEndDate.Text = Date.DaysInMonth(y, m) & "/" & m & "/" & y  'Date.Now.ToString("dd/MM/yyyy", DateFormat_TH)

            Select Case Request("ItemType")
                Case "fncp"
                    lblReportHeader.Text = "รายงานสรุปจำนวนกิจกรรม"
                    lblType.Visible = False
                    ddlType.Visible = False
                    cmdExcel.Visible = True

                    lblLocation.Visible = False
                    ddlLocation.Visible = False
                    txtFindLocation.Visible = False
                    lnkFindLocation.Visible = False
                    imgArrowLocation.Visible = False
                Case "cusp"
                    lblLocation.Visible = True
                    ddlLocation.Visible = True
                    txtFindLocation.Visible = True
                    lnkFindLocation.Visible = True
                    imgArrowLocation.Visible = True

                    LoadLocationToDDL()
                    lblReportHeader.Text = "รายงานรายชื่อผู้เข้ารับบริการแยกตามกิจกรรม"
                    lblType.Visible = True
                    ddlType.Visible = True
                    cmdExcel.Visible = True
                    LoadActivityTypeToDDL()
                Case "mtm"
                    lblReportHeader.Text = "รายงาน MTM & Home Visit"
                    ReportsName = "MTMFinanceServiceCount"
                    lblType.Visible = False
                    ddlType.Visible = False
                    cmdExcel.Visible = True

                    lblLocation.Visible = False
                    ddlLocation.Visible = False
                    txtFindLocation.Visible = False
                    lnkFindLocation.Visible = False
                    imgArrowLocation.Visible = False

                    ddlProvince.Visible = False
                    lblProv.Visible = False

                    FagRPT = "MTM"
                Case "dr"
                    lblReportHeader.Text = "รายงานยาเหลือใช้"
                    ReportsName = "MTMDrugRemain"
                    lblType.Visible = False
                    ddlType.Visible = False
                    cmdExcel.Visible = True

                    lblLocation.Visible = False
                    ddlLocation.Visible = False
                    txtFindLocation.Visible = False
                    lnkFindLocation.Visible = False
                    imgArrowLocation.Visible = False

                    ddlProvince.Visible = True
                    lblProv.Visible = True

                    FagRPT = "DR"
                Case "mcount"
                    lblReportHeader.Text = "รายงานสรุปจำนวนการบริการ MTM & Home Visit"
                    ReportsName = "MTMCustomerServiceCount"
                    lblType.Visible = False
                    ddlType.Visible = False
                    cmdExcel.Visible = True


                    ddlProvince.Visible = False
                    lblProv.Visible = False

                    lblLocation.Visible = False
                    ddlLocation.Visible = False
                    txtFindLocation.Visible = False
                    lnkFindLocation.Visible = False
                    imgArrowLocation.Visible = False

                    FagRPT = "MTM"

                Case "pcount"
                    lblReportHeader.Text = "รายงานสรุปปัญหา MTM & Home Visit รายบุคคล"
                    ReportsName = "MTMCustomerProblemMatrix"
                    lblType.Visible = False
                    ddlType.Visible = False
                    cmdExcel.Visible = True

                    ddlProvince.Visible = False
                    lblProv.Visible = False

                    lblLocation.Visible = False
                    ddlLocation.Visible = False
                    txtFindLocation.Visible = False
                    lnkFindLocation.Visible = False
                    imgArrowLocation.Visible = False

                    FagRPT = "MTM"

                Case "mtmcount", "hvcount"
                    lblReportHeader.Text = "รายงานสรุปจำนวนการบริการ MTM & Home Visit"
                    ReportsName = "MTMCustomerServiceCountByType"
                    lblType.Visible = False
                    ddlType.Visible = False
                    cmdExcel.Visible = True

                    ddlProvince.Visible = False
                    lblProv.Visible = False

                    lblLocation.Visible = False
                    ddlLocation.Visible = False
                    txtFindLocation.Visible = False
                    lnkFindLocation.Visible = False
                    imgArrowLocation.Visible = False

                    FagRPT = "HV"
                Case "cov1"
                    lblReportHeader.Text = "รายงานสรุปจำนวนผู้ป่วยในโครงการแยกตามร้านยา"
                    ReportsName = "CovidSummaryServiceCount"
                    lblType.Visible = False
                    ddlType.Visible = False
                    cmdExcel.Visible = True

                    ddlProvince.Visible = False
                    lblProv.Visible = False

                    lblLocation.Visible = False
                    ddlLocation.Visible = False
                    txtFindLocation.Visible = False
                    lnkFindLocation.Visible = False
                    imgArrowLocation.Visible = False

                    FagRPT = "HV"
            End Select

            If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
                lblLocation.Visible = False
                ddlLocation.Visible = False
                txtFindLocation.Visible = False
                lnkFindLocation.Visible = False
                imgArrowLocation.Visible = False
            End If

        End If



        'If Request("ItemType") = "fnc" Then
        '    cmdPrint.Attributes.Add("onClick", "window.open('" + ResolveUrl("rptFinanceSummarize.aspx?b=" & txtStartDate.Text & "&e=" & txtEndDate.Text & "', 'windowname', 'width=800,height=600,scrollbars=yes')"))
        '    cmdExcel.Attributes.Add("onClick", "window.open('" + ResolveUrl("rptFinanceSummarize.aspx?b=" & txtStartDate.Text & "&e=" & txtEndDate.Text & "&ex=1', 'windowname', 'width=800,height=600,scrollbars=yes')"))
        'Else
        '    cmdPrint.Attributes.Add("onClick", "window.open('" + ResolveUrl("ReportViewer.aspx?t=" & ddlType.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text & "', 'windowname', 'width=800,height=600,scrollbars=yes')"))
        'End If


    End Sub
    Private Sub LoadProvinceToDDL()
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            dt = ctlLct.Province_GetInGroup(Request.Cookies("RPTGRP").Value)
        Else
            dt = ctlLct.Province_GetInLocation
        End If

        ddlProvince.Items.Clear()
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .Items.Add("---ทั้งหมด---")
                If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
                    .Items(0).Value = Request.Cookies("RPTGRP").Value
                Else
                    .Items(0).Value = "0"
                End If

                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)("ProvinceName"))
                    .Items(i + 1).Value = dt.Rows(i)("ProvinceID")
                Next
                .SelectedIndex = 0

            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadLocationToDDL()

        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            If ddlProvince.SelectedValue <> Request.Cookies("RPTGRP").Value Then
                dt = ctlLct.Location_GetBySearch(ddlProvince.SelectedValue, txtFindLocation.Text)
            Else
                dt = ctlLct.Location_GetByProvinceGroup(Request.Cookies("RPTGRP").Value, txtFindLocation.Text)
            End If

        Else
            dt = ctlLct.Location_GetBySearch(ddlProvince.SelectedValue, txtFindLocation.Text)
        End If

        ddlLocation.Items.Clear()

        If dt.Rows.Count > 0 Then
            With ddlLocation

                .Items.Add("---ทั้งหมด---")
                .Items(0).Value = "0"
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)(objLct.tblField(objLct.fldPos.f01_LocationName).fldName))
                    .Items(i + 1).Value = dt.Rows(i)(objLct.tblField(objLct.fldPos.f00_LocationID).fldName)
                Next
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadActivityTypeToDDL()
        Dim n As Integer = 1

        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            dt = ctlType.ServiceType_GetByLocationID(Request.Cookies("LocationID").Value)
        ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isProjectManager Then
            dt = ctlType.ServiceType_GetByProjectID(Request.Cookies("PRJMNG").Value)
        Else
            dt = ctlType.ServiceType_GetAll()
        End If

        If dt.Rows.Count > 0 Then
            ddlType.Items.Clear()
            ddlType.Items.Add("---ทั้งหมด---")
            ddlType.Items(0).Value = "0"
            For i = 0 To dt.Rows.Count - 1
                If dt.Rows(i)("ServiceTypeID") <> "F02" And dt.Rows(i)("ServiceTypeID") <> "F03" Then
                    With ddlType
                        .Items.Add("" & dt.Rows(i)("ServiceTypeID") & " : " & dt.Rows(i)("ServiceName"))
                        .Items(n).Value = dt.Rows(i)("ServiceTypeID")
                        n = n + 1
                    End With
                End If
            Next
        End If
        dt = Nothing
    End Sub

    Protected Sub cmdPrint_Click(sender As Object, e As EventArgs) Handles cmdPrint.Click
        ' Response.Redirect("rptFinanceSummarize.aspx?b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
        
        ' Dim fRptView As New ReportViewer
        Dim dStart As String = ConvertStrDate2DBString(txtStartDate.Text)
        Dim dEnd As String = ConvertStrDate2DBString(txtStartDate.Text)

        'ReDim ReportParameter(2)
        'ReportParameter(0) = dStart
        'ReportParameter(1) = dEnd
        'fRptView.SelectionFomula = "{Service_Order.ServiceDate} in " & ConvertDate2DBString(txtStartDate.Text) & "  to  " & ConvertDate2DBString(txtEndDate.Text)

        'Select Case Request("ItemType")
        '    Case "cus"
        '        If ddlType.SelectedValue = FORM_TYPE_ID_F01 Then
        '            fRptView.FileName = "Reports/rptCustomer_F01.rpt"
        '        ElseIf ddlType.SelectedValue = FORM_TYPE_ID_F04 Then
        '            fRptView.FileName = "Reports/rptCustomer_F04.rpt"
        '        Else
        '            fRptView.FileName = "Reports/rptCustomer_F99.rpt"
        '            fRptView.SelectionFomula &= " AND {Service_Order.ServiceTypeID}='" & ddlType.SelectedValue & "'"

        '        End If 

        'End Select

        Dim LID As String = "0"
        Dim rptGRP As String = "ALL"
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            LID = Request.Cookies("LocationID").Value
        ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            rptGRP = Request.Cookies("RPTGRP").Value
            LID = ddlLocation.SelectedValue
        Else
            LID = ddlLocation.SelectedValue
        End If
        Select Case Request("ItemType")
            Case "fncp"
                If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
                    Response.Redirect("rptFinanceCustomerSummarize.aspx?lid=" & LID & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
                Else
                    Response.Redirect("rptFinanceSummarizeByProvince.aspx?grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
                End If
            Case "mcount", "mtmcount", "hvcount", "pcount"
                Response.Redirect("ReportViewer.aspx?rpt=" & Request("ItemType") & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
            Case "mtm", "dr"
                Response.Redirect("ReportViewer.aspx?rpt=mtm&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
            Case Else
                Response.Redirect("rptCustomerByActivityAndProvince.aspx?grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&lid=" & LID & "&t=" & ddlType.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
        End Select
    End Sub

    Protected Sub cmdExcel_Click(sender As Object, e As EventArgs) Handles cmdExcel.Click

        Dim LID As String = "0"
        Dim rptGRP As String = "ALL"
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            LID = Request.Cookies("LocationID").Value
        ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            rptGRP = Request.Cookies("RPTGRP").Value
            LID = ddlLocation.SelectedValue
        Else
            LID = ddlLocation.SelectedValue
        End If
        Select Case Request("ItemType")
            Case "fncp"
                If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
                    ReportsName = "rptFinanceCustomerSummarize"
                    Response.Redirect("rptFinanceCustomerSummarize.aspx?ex=1&lid=" & LID & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
                Else
                    ReportsName = "rptFinanceSummarize"
                    Response.Redirect("rptFinanceSummarizeByProvince.aspx?ex=1&grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
                End If
            Case "mcount", "mtmcount", "hvcount", "pcount"
                Response.Redirect("ReportViewer.aspx?ex=1&rpt=" & Request("ItemType") & "&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
            Case "mtm", "dr"
                Response.Redirect("ReportViewer.aspx?ex=1&rpt=mtm&p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
            Case Else
                ReportsName = "rptCustomerByActivity"
                Response.Redirect("rptCustomerByActivityAndProvince.aspx?ex=1&grp=" & rptGRP & "&p=" & ddlProvince.SelectedValue & "&lid=" & LID & "&t=" & ddlType.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
        End Select


    End Sub

    Protected Sub lnkFindLocation_Click(sender As Object, e As EventArgs) Handles lnkFindLocation.Click
        LoadLocationToDDL()
    End Sub

    Protected Sub ddlProvince_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProvince.SelectedIndexChanged
        LoadLocationToDDL()
    End Sub
End Class