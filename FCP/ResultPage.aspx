﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="ResultPage.aspx.vb" Inherits=".ResultPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/uidialog.css">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
     <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td align="center"><h3>
          <asp:Label ID="lblMsg" runat="server" Text="บันทึกข้อมูลเรียบร้อย"></asp:Label></h3>
        </td>
    </tr>
   <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td align="center">
          <asp:HyperLink ID="hlnkBack" runat="server" CssClass="buttonSave" Height="35">กลับไปบันทึกกิจกรรมเพิ่มอีกครั้ง</asp:HyperLink>
          &nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
    </section>
</asp:Content>
