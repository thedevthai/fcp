﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="F04.aspx.vb" Inherits=".F04" %>
<%@ Register src="ucHistoryYear.ascx" tagname="ucHistoryYear" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/pagestyles.css">
<link rel="stylesheet" type="text/css" href="css/rajchasistyles.css">
<link rel="stylesheet" type="text/css" href="css/uidialog.css">
<link rel="stylesheet" type="text/css" href="css/dc_table.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>F04
        <small>กิจกรรมการให้ความรู้ คำแนะนำและคำปรึกษาเรื่องการสร้างเสริมภูมิคุ้มกันโรคด้วย
                          วัคซีนในเด็กอายุ 0-14 ปี <br /> ตามแผนการสร้างเสริมภูมิคุ้มกันโรคของกระทรวงสาธารณสุข (EPI Program)</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">     
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลกิจกรรม</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">





 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
   <td height="10"> <uc1:ucHistoryYear ID="ucHistoryYear1" runat="server" /></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="OptionPanels">
      <tr>
        <td width="100" class="texttopic">รหัสร้าน</td>
        <td width="120" class="LocationName">
            <asp:Label ID="lblLocationID" runat="server"></asp:Label>
          </td>
        <td width="50" class="texttopic">ชื่อร้าน</td>
        <td width="300" class="LocationName">
            <asp:Label ID="lblLocationName" runat="server"></asp:Label>          </td>
        <td width="50" class="texttopic">จังหวัด</td>
        <td class="LocationName">
            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
   <tr>
    <td align="center"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">
      <tr>
        <td width="100">วันที่ให้บริการ</td>
        <td>
            <asp:TextBox ID="txtServiceDate" runat="server"></asp:TextBox>
(วว/ดด/ปปปป พ.ศ.) 
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="txtServiceDate" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="\d{2}/\d{2}/\d{4}"></asp:RegularExpressionValidator></td>
        <td width="100">รวมระยะเวลา<img src="images/star.png" width="10" height="10" /></td>
        <td>
            <asp:TextBox ID="txtTime" runat="server" Width="50px"></asp:TextBox>
&nbsp;นาที</td>
        <td>เภสัชกรผู้ให้บริการ</td>
        <td>
            <asp:DropDownList ID="ddlPerson" runat="server">            </asp:DropDownList>          </td>
      </tr>
    </table></td>
  </tr>
  
   <tr>
    <td >
      <table width="100%" border="0" cellspacing="2" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
          <td width="100" align="right"><span class="NameEN">Ref.ID :</span></td>
          <td width="50"><span class="NameEN">
              <asp:Label ID="lblID" 
                  runat="server"></asp:Label></span>
            </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td class="MenuSt">ข้อมูลการให้ความรู้และคำแนะนำปรึกษา</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td colspan="2" valign="top" class="texttopic"><table width="100%" border="0" cellspacing="2" cellpadding="0">
          <tr>
            <td width="150" valign="top" class="texttopic">ชื่อ-นามสกุล(ด.ช./ด.ญ.)</td>
            <td width="220" align="left" valign="top">
                <asp:TextBox ID="txtChildName" runat="server" 
                Width="200px"></asp:TextBox></td>
            <td align="left" valign="top">
                เพศ</td>
            <td align="left" valign="top">
                <asp:RadioButtonList ID="optChildGender" runat="server" RepeatDirection="Horizontal">
          <asp:ListItem Selected="True" Value="M">ชาย</asp:ListItem>
          <asp:ListItem Value="F">หญิง</asp:ListItem>
        </asp:RadioButtonList></td>
            <td valign="top" class="texttopic">วัน/เดือน/ปีเกิด</td>
            <td align="left" valign="top">
                <asp:TextBox ID="txtChildBirthDate" runat="server" 
                Width="80px" AutoPostBack="True"></asp:TextBox>
              &nbsp;(วว/ดด/ปปปป{พ.ศ.})
              <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                ControlToValidate="txtChildBirthDate" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" 
                    ValidationExpression="\d{2}/\d{2}/\d{4}"></asp:RegularExpressionValidator></td>
            <td valign="top" class="texttopic">อายุ</td>
            <td valign="top"><asp:TextBox ID="txtChildAges" runat="server" Width="50px"></asp:TextBox>
              &nbsp;ปี</td>
          </tr>
        </table></td>
        </tr>
      <tr>
        <td width="50%" valign="top">
          <table width="100%" align="center" class="dc_table_s3">
   
    <thead>
      <tr>
        <th scope="col">อายุ</th>
        <th scope="col">ชนิดวัคซีน</th>
        </tr>
    </thead>
    <tfoot>
    </tfoot>
    <tbody>
      <tr >
        <th align="center" scope="row">แรกเกิด</th>
        <th align="center">BCG,HB1</th>
        </tr>
      <tr class="odd">
        <th align="center" scope="row">2 เดือน</th>
        <th align="center">OPV1,DTP-HB1</th>
        </tr>
      <tr >
        <th align="center" scope="row">4 เดือน</th>
        <th align="center">OPV2,DTP-HB2</th>
        </tr>
      <tr >
        <th align="center" scope="row">6 เดือน</th>
        <th align="center">OPV3,DTP-HB3</th>
      </tr>
      <tr >
        <th align="center" scope="row">9 เดือน</th>
        <th align="center">MMR</th>
      </tr>
      <tr >
        <th align="center" scope="row">18 เดือน</th>
        <th align="center">OPV4,DTP4,JE1,JE2</th>
      </tr>
      <tr >
        <th align="center" scope="row">6 เดือน - 2 ปี</th>
        <th align="center">วัคซีนไข้หวัดใหญ่ตามฤดูกาล (ปีละครั้ง)</th>
      </tr>
      <tr >
        <th align="center" scope="row">2 <span class="text9">1/2</span> ปี</th>
        <th align="center">JE3</th>
      </tr>
      <tr >
        <th align="center" scope="row">4 ปี</th>
        <th align="center">OPV5, DTP5</th>
      </tr>
      <tr >
        <th align="center" scope="row">7 ปี</th>
        <th align="center">MMR2<br />
          BCG (ไม่มีหลักฐานฉีดตอนแรกเกิด หรือไม่มีแผลเป็น)<br />
          dT, OPV (กรณีได้รับไม่ครบ 5 ครั้ง)</th>
      </tr>
      <tr >
        <th align="center" scope="row">12 ปี</th>
        <th align="center">dT</th>
      </tr>
    </tbody>
  </table>
        
        </td>
        <td align="left" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="2" class="dc_table_s1">
          <thead>
      <tr>
        <th scope="col">ความครบถ้วนของวัคซีน</th>
        
        </tr>
    </thead>
    <tfoot>
    </tfoot>
    <tbody>
          <tr>
            <td align="left">
                <asp:RadioButtonList ID="optVaccine" runat="server" 
                    RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Value="Y">ฉีดครบ</asp:ListItem>
                    <asp:ListItem Value="N">ฉีดไม่ครบ</asp:ListItem>
                    <asp:ListItem Value="-">ไม่ทราบ/ไม่แน่ใจ</asp:ListItem>
                </asp:RadioButtonList>              </td>
          </tr>
          <tr>
            <td align="left"><table border="0" cellspacing="2" cellpadding="0">
                <tr>
                  <td>เพิ่มวัคซีนที่ได้รับไม่ครบ </td>
                  <td>
                      <asp:TextBox ID="txtVaccine" runat="server"></asp:TextBox>
                    </td>
                  <td>
                      <asp:LinkButton ID="lnkAdd" runat="server" CssClass="buttonRedial">เพิ่มวัคซีน</asp:LinkButton>&nbsp; <span class="text10_nblue">(เพิ่มทีละตัวจนครบ)</span></td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td>
              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" Font-Bold="False" 
                    ShowHeader="False">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="No.">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />                      </asp:BoundField>
            <asp:BoundField DataField="VaccineName" HeaderText="วัคซีนที่ไม่ได้รับ">
              <headerstyle HorizontalAlign="Left" />          
              <itemstyle HorizontalAlign="Left" />                      </asp:BoundField>
            <asp:TemplateField HeaderText="ลบ">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgDel" runat="server" 
                                    ImageUrl="images/icon-delete.png" 
                                    CommandArgument='<%# Container.DataItemIndex %>' /></itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView></td>
          </tr>
          </tbody>
        </table></td>
        </tr>
      
    </table></td>
  </tr>
  
   <tr>
    <td class="MenuSt" align="center">การให้ความรู้และคำแนะนำปรึกษา</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="dc_table_s3">
     <thead>  <tr>
        <th scope="col" width="50%" valign="top" class="texttopic">ปัญหาของผู้รับบริการ</th>
        <th scope="col" colspan="3" valign="top">การให้ความรู้และคำแนะนำปรึกษา</th>
        </tr> </thead>
       <tbody><tr>
        <th valign="top" class="texttopic" align="left">
              <asp:CheckBox ID="chkProblem1" runat="server" Font-Bold="False" Text="ความรู้เรื่องการสร้างภูมิคุมกันโรคด้วยวัคซีน" /> <br />
               <asp:CheckBox ID="chkProblem2" runat="server" Font-Bold="False" Text="ได้รับวัคซีนไม่ครบ" />                  <br />
                <asp:CheckBox ID="chkProblem3" runat="server" Font-Bold="False" Text="ได้รับวัคซีนครบแต่ไม่เป็นไปตามกำหนด" /> <br />
              <asp:CheckBox ID="chkProblem4" runat="server" Font-Bold="False" Text="ไม่รู้ว่าได้ครบหรือไม่ครบ" /> <br />
                <asp:CheckBox ID="chkProblem5" runat="server" Font-Bold="False" Text="ปัญหาอื่นๆ" />            
           &nbsp;<asp:TextBox ID="txtProblemOther" runat="server" Width="200px"></asp:TextBox>           </th>
        <th colspan="3" valign="top" align="left">
             <asp:CheckBox ID="chkEdu1" runat="server" Font-Bold="False" Text="ให้ความรู้เรื่องการสร้างภูมิคุมกันโรคด้วยวัคซีน" /> <br />
  <asp:CheckBox ID="chkEdu2" runat="server" Font-Bold="False" Text="แนะนำให้ไปรับการฉีดจนครบ" /> <br />
  <asp:CheckBox ID="chkEdu3" runat="server" Font-Bold="False" Text="แนะนำตามประเภทวัคซีนที่มีปัญหา" /> <br />
  <asp:CheckBox ID="chkEdu4" runat="server" Font-Bold="False" Text="แนะนำให้ไปตรวจทบทวนในสมุดวัคซีนของเด็ก" /> <br />
  <asp:CheckBox ID="chkEdu5" runat="server" Font-Bold="False" Text="อื่นๆ" /> &nbsp;<asp:TextBox 
                 ID="txtEduOther" runat="server" Width="200px"></asp:TextBox>
             <br />            </th>
      </tr>
       </tbody>
    </table></td>
  </tr>
   <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="dc_table_s3">
     <thead>
      <tr>
        <th scope="col" width="50%" valign="top" class="texttopic">แจ้งสิทธิประโยช์  สปสช.</th>
      </tr>
       </thead>
      <tr>
        <td align="left">
              <table border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td><asp:CheckBox ID="chkNHSO1" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวจคัดกรองความเสี่ยงDM/HT" /></td>
                  <td><asp:CheckBox ID="chkNHSO2" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวจคัดกรองความเสี่ยงมะเร็งปากมดลูก/มะเร็งเต้านม" /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO3" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับวัคซีนเสริมสร้างภูมิคุ้มกันโรค" /></td>
                  <td><asp:CheckBox ID="chkNHSO4" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับคำแนะนำปรึกษาการวางแผนครอบครัว / คุมกำเนิด" /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO5" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการดูแล/หญิงมีครรภ์ " /></td>
                  <td><asp:CheckBox ID="chkNHSO6" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวดเอดส์ ฟรี ปีละ 2 ครั้ง " /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO7" runat="server" Font-Bold="False" Text="การแจ้งย้ายสิทธิ " /></td>
                  <td><asp:CheckBox ID="chkNHSO8" runat="server" Font-Bold="False" Text="สายด่วน 1330" /></td>
                </tr>
              </table></td>
      </tr>
    </table></td>
  </tr>  
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="Section_Header">
      <tr>
        <td width="100">สถานะ</td>
        <td>
            <asp:CheckBox ID="chkClose" runat="server" Text="จบการทำงาน" Checked="True" />
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center">
           <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="70px" />
        &nbsp;<asp:Button ID="cmdClear" runat="server" Text="ยกเลิก" CssClass="buttonCancle" Width="70px" />
      </td>
  </tr>
</table>
  </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
       </section>
</asp:Content>