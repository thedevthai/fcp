﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="Login" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title> Login</title> 
  <link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Modal -->
  <link href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round" rel="stylesheet">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <style type="text/css">
    body {
      font-family: 'Varela Round', sans-serif;
    }

    .modal-confirm {
      color: #434e65;
      width: 525px;
    }

    .modal-confirm .modal-content {
      padding: 20px;
      font-size: 16px;
      border-radius: 5px;
      border: none;
    }

    .modal-confirm .modal-header {
      background: #e85e6c;
      border-bottom: none;
      position: relative;
      text-align: center;
      margin: -20px -20px 0;
      border-radius: 5px 5px 0 0;
      padding: 35px;
    }

    .modal-confirm h4 {
      text-align: center;
      font-size: 36px;
      margin: 10px 0;
    }

    .modal-confirm .form-control,
    .modal-confirm .btn {
      min-height: 40px;
      border-radius: 3px;
    }

    .modal-confirm .close {
      position: absolute;
      top: 15px;
      right: 15px;
      color: #fff;
      text-shadow: none;
      opacity: 0.5;
    }

    .modal-confirm .close:hover {
      opacity: 0.8;
    }

    .modal-confirm .icon-box {
      color: #fff;
      width: 95px;
      height: 95px;
      display: inline-block;
      border-radius: 50%;
      z-index: 9;
      border: 5px solid #fff;
      padding: 15px;
      text-align: center;
    }

    .modal-confirm .icon-box i {
      font-size: 58px;
      margin: -2px 0 0 -2px;
    }

    .modal-confirm.modal-dialog {
      margin-top: 80px;
    }

    .modal-confirm .btn {
      color: #fff;
      border-radius: 4px;
      background: #eeb711;
      text-decoration: none;
      transition: all 0.4s;
      line-height: normal;
      border-radius: 30px;
      margin-top: 10px;
      padding: 6px 20px;
      min-width: 150px;
      border: none;
    }

    .modal-confirm .btn:hover,
    .modal-confirm .btn:focus {
      background: #eda645;
      outline: none;
    }

    .trigger-btn {
      display: inline-block;
      margin: 100px auto;
    }
  </style>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <script type="text/javascript">
    function openModals(sender, title, message) {
      $("#spnTitle").text(title);
      $("#spnMsg").text(message);
      $('#modalPopUp').modal('show');
      $('#btnConfirm').attr('onclick', "$('#modalPopUp').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
      return false;
    }

  </script>

  <!-- End modal -->
</head>

<body class="bg-gradient-primary">
  <form id="form1"  class="user" runat="server">
    <div>
    </div>

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Member Login</h1>
                  </div>
               
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" id="txtUsername" runat="server" placeholder="Username">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control form-control-user" id="txtPassword" runat="server"
                        placeholder="Password">
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                      </div>
                    </div>

                    <asp:Button ID="btnLogin" runat="server" CssClass="btn btn-primary"
                      Text="Login" />
                     
                  <hr>
                  <div class="text-center">
                    <a class="small" href="forgot-password.html">Forgot Password?</a>
                  </div>
                  <div class="text-center">
                    <a class="small" href="register.html">Create an Account!</a>
                  </div>

                  <div class="text-center">
                    <br>
                    <br>
                    <br>
                    <div class="text-right">
                      <asp:Label ID="lblVersion" runat="server" Text=""></asp:Label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>


    <!-- Modal HTML -->
    <div id="modalPopUp" class="modal fade" role="dialog" data-backdrop="static">
      <div class="modal-dialog modal-successss">
        <div class="modal-content">
          <div class="modal-header">
            <div class="icon-box">
              <i class="material-icons">&#xE5CD;</i>
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body text-center">
            <h4><span id="spnTitle"></span></h4>
            <p><span id="spnMsg"></span>.</p>
            <button class="btn btn-success" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!--- End Modal --->

  </div>
 </form>
</body>

</html>