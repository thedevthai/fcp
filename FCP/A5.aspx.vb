﻿Public Class A5
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlLct As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlOrder As New SmokingController
    Dim ctlSK As New StockController
    Dim acc As New UserController
    Dim ServiceDate As Long
    Private _dateDiff As Object

    'Dim dtPOS As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Redirect("CuttomErrorPage.aspx")
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Session("patientid") Is Nothing Then
            Response.Redirect("PatientSearch.aspx")
        End If

        If Not IsPostBack Then
            If ctlLct.GET_DATE_SERVER.Year < 2555 Then
                txtYear.Text = ctlLct.GET_DATE_SERVER.Year + 543
            Else
                txtYear.Text = ctlLct.GET_DATE_SERVER.Year
            End If


            ClearData()
            LoadMedicine()
            'txtSmokeQTY.Visible = False
            'lblCGday.Visible = False
            'txtSmokeDay.Visible = False
            'lblCgType.Visible = False
            'optCigarette.Visible = False
            'lblProblem.Visible = False
            'optNextCause.Visible = False
            'txtCause.Visible = False

            'lblUnit1.Visible = True
            'lblUnit2.Visible = False

            lblBalance.Visible = False
            lblBalanceLabel.Visible = False

            If Request("acttype") = "view" Then
                cmdSave.Visible = False
            Else
                cmdSave.Visible = True
            End If

            'dtPOS.Columns.Add("MedTime")
            'dtPOS.Columns.Add("itemID")
            'dtPOS.Columns.Add("itemName")
            'dtPOS.Columns.Add("QTY")
            'Session("dtposA5") = dtPOS

            LoadPharmacist(Request.Cookies("LocationID").Value)
            txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)

            LoadFormData()

            'LoadBalanceStock()

            LoadA05ToGrid()

            'If grdA05.Rows.Count >= 10 Then
            '    'cmdSave.Visible = False
            '    lblProblem.Visible = False
            '    optNextCause.Visible = False
            '    txtCause.Visible = False

            '    optNextStep.Visible = False

            'Else
            '    cmdSave.Visible = True
            'End If

            'If StrNull2Zero(lblSEQ.Text) < 10 Then
            '    lblProblem.Visible = True
            '    optNextCause.Visible = True
            '    txtCause.Visible = True

            '    optNextStep.Visible = True
            'Else
            '    lblProblem.Visible = False
            '    optNextCause.Visible = False
            '    txtCause.Visible = False

            '    optNextStep.Visible = False
            'End If

            If Request("t") = "new" Then
                ClearData()
            End If

        End If



        'txtScreenBP1.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")
        'txtScreenBP2.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")
        'txtScreenBP3.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")

        'If HiddenField1.Value <> "" Then
        '    CalBPAVG()
        'End If
        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        lblSEQ.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtYear.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")


    End Sub
    Private Sub LoadA05ToGrid()
        'Dim iRef As Long
        'iRef = StrNull2Long(hdA1UID.value)
        'If iRef = 0 Then
        dt = ctlOrder.Smoking_Order_GetA05(Session("patientid"), lblLocationID.Text, StrNull2Zero(txtYear.Text))
        'Else
        '    dt = ctlOrder.Smoking_Order_GetA05(iRef)
        'End If

        If dt.Rows.Count > 0 Then
            With grdA05
                .Visible = True
                .DataSource = dt
                .DataBind()
            End With

            'If dt.Rows.Count < 5 Then
            '    lblSEQ.Text = dt.Rows.Count + 1
            'Else
            '    lblSEQ.Text = ""
            'End If

            If hdServiceUID.Value = "" Then
                lblSEQ.Text = dt.Rows.Count + 1
                LoadFinalStopDate(FORM_TYPE_ID_A5, Session("patientid"), lblLocationID.Text, dt.Rows.Count)
            End If

            If Request("t") = "new" Then
                lblSEQ.Text = dt.Rows.Count + 1
            End If
        Else
            lblSEQ.Text = "1"
            grdA05.Visible = False

            LoadFinalStopDate(FORM_TYPE_ID_A4F, Session("patientid"), lblLocationID.Text, 0)
        End If
        dt = Nothing
    End Sub
    Private Sub ClearData()

        'For t = 0 To dtPOS.Rows.Count - 1
        '    dtPOS.Rows(t).Delete()
        'Next

        'Session("dtposA5") = Nothing
        'Session("dtposA5") = dtPOS
        txtTime.Text = ""
        txtNicotineRemark.Text = ""
        txtMedEffRemark.Text = ""
        'txtRecommend3.Text = ""
        'txtRecommend4.Text = ""
        'txtFollowRemark.Text = ""
        txtQuitDay.Text = ""
        dtpFinalStopDate.Text = ""
        'txtCause.Text = ""
        txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)
    End Sub

    Private Sub LoadPharmacist(LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With
            End If
        End If
        dtP = Nothing
    End Sub

    Private Sub LoadMedicine()
        Dim dtM As New DataTable
        dtM = ctlOrder.Medicine_Get
        If dtM.Rows.Count > 0 Then
            ddlMed.DataSource = dtM
            ddlMed.DataTextField = "itemName"
            ddlMed.DataValueField = "itemID"
            ddlMed.DataBind()
        End If
        dtM = Nothing
    End Sub
    Private Sub LoadLocationData(LocationID As String)
        dt = ctlLct.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblLocationID.Text = .Item("LocationID")
                lblLocationName.Text = .Item("LocationName")
                lblLocationProvince.Text = .Item("ProvinceName")
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadFormData()

        Dim pYear As Integer
        ctlOrder.MedOrder_DeleteAll(0, FORM_TYPE_ID_A5, lblLocationID.Text)
        If Not Request("fid") Is Nothing Then
            dt = ctlOrder.CPA_SmokingOrder_ByID(StrNull2Zero(Request("fid")))
        Else
            If Not Request("yid") Is Nothing Then
                pYear = CInt(Request("yid"))
            Else
                pYear = Year(ctlLct.GET_DATE_SERVER)
                If pYear < 2500 Then
                    pYear = pYear + 543
                End If

            End If
            dt = ctlOrder.CPA_Smoking_OrderByYear(pYear, FORM_TYPE_ID_A5, Session("patientid"))
        End If


        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                If Request("t") = "new" Then
                    hdServiceUID.Value = ""
                    hdA1UID.Value = String.Concat(.Item("RefID"))
                ElseIf Request("t") = "edit" Then
                    hdServiceUID.Value = String.Concat(.Item("ServiceUID"))
                    hdA1UID.Value = String.Concat(.Item("RefID"))
                    txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                    lblSEQ.Text = String.Concat(.Item("FollowSEQ"))

                ElseIf Request("t") Is Nothing Then
                    hdServiceUID.Value = ""
                    hdA1UID.Value = String.Concat(.Item("ServiceUID"))
                End If

                txtYear.Text = String.Concat(.Item("BYear"))
                If hdServiceUID.Value <> "" Then
                    lblSEQ.Text = String.Concat(.Item("FollowSEQ"))
                End If

                '-------------------
                optPatientFrom.SelectedValue = DBNull2Str(.Item("PFROM"))
                txtFromRemark.Text = DBNull2Str(.Item("PFROM_DESC"))

                optService.SelectedValue = DBNull2Str(.Item("MTMService"))
                txtServiceRemark.Text = DBNull2Str(.Item("ServiceRemark"))

                If DBNull2Str(.Item("MTMService")) = "2" Then
                    optTelepharm.SelectedValue = DBNull2Str(.Item("ServiceRef"))
                Else
                    optTelepharm.ClearSelection()
                End If

                optTelepharmacyMethod.SelectedValue = DBNull2Str(.Item("TelepharmacyMethod"))
                txtRecordMethod.Text = DBNull2Str(.Item("RecordMethod"))
                txtRecordLocation.Text = DBNull2Str(.Item("RecordLocation"))

                txtTelepharmacyRemark.Text = DBNull2Str(.Item("TelepharmacyRemark"))

                '---------------------------------

                txtSmokeQTY.Text = String.Concat(.Item("SmokeQTY"))
                txtNicotinRate.Text = String.Concat(.Item("Nicotine"))
                txtOften.Text = String.Concat(.Item("Often"))
                txtMinutePerTime.Text = String.Concat(.Item("SmokeTime"))


                Session("patientid") = DBNull2Lng(.Item("PatientID"))
                If DBNull2Str(.Item("FollowDate")) <> "" Then
                    txtFollowDate.Text = DisplayShortDateTH(.Item("FollowDate"))
                Else
                    txtFollowDate.Text = ""
                End If
                optFollowChannel.SelectedValue = String.Concat(.Item("FollowChannel"))
                'txtFollowRemark.Text = String.Concat(.Item("FollowRemark"))

                'optFinalResult.SelectedValue = String.Concat(.Item("FinalResult"))
                txtQuitDay.Text = String.Concat(.Item("QuitDay"))
                optChangeRate.SelectedValue = DBNull2Str(.Item("ChangeRate"))
                'VisibleChangRate()
                If Not IsDBNull(.Item("FinalStopDate")) Then
                    dtpFinalStopDate.Text = .Item("FinalStopDate")
                Else
                    dtpFinalStopDate.Text = ""
                End If

                optNicotineEffect.SelectedValue = String.Concat(.Item("NicotineEffect"))
                optMedEffect.SelectedValue = String.Concat(.Item("MedEffect"))
                'optSocietyEffect.SelectedValue = String.Concat(.Item("SocietyEffect"))
                txtNicotineRemark.Text = String.Concat(.Item("Recommend1"))
                txtMedEffRemark.Text = String.Concat(.Item("Recommend2"))
                'txtRecommend3.Text = String.Concat(.Item("Recommend3"))
                'txtRecommend4.Text = String.Concat(.Item("Recommend4"))

                txtWeight.Text = DBNull2Str(.Item("PWeight"))
                txtHeigh.Text = DBNull2Str(.Item("PHeigh"))
                txtPEFR_Value.Text = DBNull2Str(.Item("PEFR_Value"))
                txtPEFR_Rate.Text = DBNull2Str(.Item("PEFR_Rate"))
                txtCo_Value.Text = DBNull2Str(.Item("CO_Value"))

                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))

                txtTime.Text = String.Concat(.Item("ServiceTime"))
                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))


                optStopPlane1.SelectedValue = DBNull2Str(.Item("ServicePlan"))
                txtStopPlaneOther.Text = String.Concat(.Item("StopPlanRemark"))
                'xDateStopPlan1.Text = DBNull2Str(.Item("ServicePlanDate"))

                'optStopPlane2.SelectedValue = DBNull2Str(.Item("Guidelines"))
                'If optStopPlane2.SelectedIndex = 1 Then
                '    txtCGTargetRateBegin.Text = DBNull2Str(.Item("GuidelinesCGFrom"))
                '    txtCGTargetRateEnd.Text = DBNull2Str(.Item("GuidelinesCGTo"))
                '    txtTargetDay.Text = DBNull2Str(.Item("GuidelinesDay"))
                '    xDateStopPlan2.Text = DBNull2Str(.Item("GuidelinesDate"))
                '    If DBNull2Str(.Item("GuidelinesDate")) <> "" Then
                '        xDateStopPlan2.Text = DisplayShortDateTH(.Item("GuidelinesDate"))
                '    Else
                '        xDateStopPlan2.Text = ""
                '    End If

                'End If


                'If DBNull2Str(.Item("ServicePlanDate")) <> "" Then
                '    xDateStopPlan1.Text = DisplayShortDateTH(.Item("ServicePlanDate"))
                'Else
                '    xDateStopPlan1.Text = ""
                'End If

                If DBNull2Str(.Item("ServiceNextDate")) <> "" Then
                    xDateNextService.Text = DisplayShortDateTH(.Item("ServiceNextDate"))
                Else
                    xDateNextService.Text = ""
                End If




                'optNextStep.SelectedValue = DBNull2Str(.Item("ServicePlan"))
                'optNextCause.SelectedValue = DBNull2Str(.Item("ServicePlanProblem"))
                'txtCause.Text = String.Concat(.Item("ServicePlanRemark"))
                'ddlTimeService.SelectedValue = DBNull2Str(.Item("ContactTime"))

                chkStatus.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("StatusFlag")))

                'ctlOrder.MedOrder_DeleteAll(StrNull2Long(hdServiceUID.value), FORM_TYPE_ID_A4F, Request.Cookies("LocationID").Value)
                LoadMedOrder()

                If DBNull2Zero(.Item("StatusFlag")) >= 3 Then  'Year(ctlLct.GET_DATE_SERVER.Date) <> DBNull2Zero(.Item("BYear")) 
                    cmdSave.Visible = False
                Else
                    If Request.Cookies("LocationID").Value <> String.Concat(.Item("LocationID")) Then
                        LoadLocationData(String.Concat(.Item("LocationID")))
                        If Request("t") = "edit" Then
                            cmdSave.Visible = True
                        Else
                            cmdSave.Visible = False
                        End If
                    Else
                        LoadLocationData(Request.Cookies("LocationID").Value)
                    End If
                End If


                'If optNextStep.SelectedValue = "2" Then
                '    lblProblem.Visible = True
                '    optNextCause.Visible = True
                '    txtCause.Visible = True
                'Else
                '    lblProblem.Visible = False
                '    optNextCause.Visible = False
                '    txtCause.Visible = False
                'End If



            End With
        Else
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing

    End Sub

    Private Sub EditData(ServiceUID As Long)

        ctlOrder.MedOrder_DeleteAll(0, FORM_TYPE_ID_A5, Request.Cookies("LocationID").Value)
        dt = ctlOrder.Smoking_Order_GetByID(ServiceUID)

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False

                hdServiceUID.Value = String.Concat(.Item("ServiceUID"))
                hdA1UID.Value = String.Concat(.Item("RefID"))
                txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                txtTime.Text = String.Concat(.Item("ServiceTime"))


                lblSEQ.Text = String.Concat(.Item("FollowSEQ"))

                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))


                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))

                If DBNull2Str(.Item("FollowDate")) <> "" Then
                    txtFollowDate.Text = DisplayShortDateTH(.Item("FollowDate"))
                Else
                    txtFollowDate.Text = ""
                End If

                optFollowChannel.SelectedValue = String.Concat(.Item("FollowChannel"))
                'txtFollowRemark.Text = String.Concat(.Item("FollowRemark"))

                txtSmokeQTY.Text = String.Concat(.Item("SmokeQTY"))
                txtNicotinRate.Text = String.Concat(.Item("Nicotine"))
                txtOften.Text = String.Concat(.Item("Often"))
                txtMinutePerTime.Text = String.Concat(.Item("SmokeTime"))

                'optFinalResult.SelectedValue = String.Concat(.Item("FinalResult"))
                txtQuitDay.Text = String.Concat(.Item("QuitDay"))
                optChangeRate.SelectedValue = DBNull2Str(.Item("ChangeRate"))
                'VisibleChangRate()
                If Not IsDBNull(.Item("FinalStopDate")) Then
                    dtpFinalStopDate.Text = .Item("FinalStopDate")
                Else
                    dtpFinalStopDate.Text = ""
                End If

                optNicotineEffect.SelectedValue = String.Concat(.Item("NicotineEffect"))
                optMedEffect.SelectedValue = String.Concat(.Item("MedEffect"))
                'optSocietyEffect.SelectedValue = String.Concat(.Item("SocietyEffect")
                txtNicotineRemark.Text = String.Concat(.Item("Recommend1"))
                txtMedEffRemark.Text = String.Concat(.Item("Recommend2"))
                'txtRecommend3.Text = String.Concat(.Item("Recommend3"))
                'txtRecommend4.Text = String.Concat(.Item("Recommend4"))

                txtWeight.Text = DBNull2Str(.Item("PWeight"))
                txtHeigh.Text = DBNull2Str(.Item("PHeigh"))
                txtPEFR_Value.Text = DBNull2Str(.Item("PEFR_Value"))
                txtPEFR_Rate.Text = DBNull2Str(.Item("PEFR_Rate"))
                txtCo_Value.Text = DBNull2Str(.Item("CO_Value"))

                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))

                txtTime.Text = String.Concat(.Item("ServiceTime"))
                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))
                optStopPlane1.SelectedValue = DBNull2Str(.Item("ServicePlan"))
                txtStopPlaneOther.Text = String.Concat(.Item("StopPlaneRemark"))

                'optStopPlane2.SelectedValue = DBNull2Str(.Item("Guidelines"))
                'txtCGTargetRateBegin.Text = DBNull2Str(.Item("GuidelinesCGFrom"))
                'txtCGTargetRateEnd.Text = DBNull2Str(.Item("GuidelinesCGTo"))
                'txtTargetDay.Text = DBNull2Str(.Item("GuidelinesDay"))


                'If DBNull2Str(.Item("ServicePlanDate")) <> "" Then
                '    xDateStopPlan1.Text = DisplayShortDateTH(.Item("ServicePlanDate"))
                'Else
                '    xDateStopPlan1.Text = ""
                'End If
                'If DBNull2Str(.Item("GuidelinesDate")) <> "" Then
                '    xDateStopPlan2.Text = DisplayShortDateTH(.Item("GuidelinesDate"))
                'Else
                '    xDateStopPlan2.Text = ""
                'End If
                If DBNull2Str(.Item("ServiceNextDate")) <> "" Then
                    xDateNextService.Text = DisplayShortDateTH(.Item("ServiceNextDate"))
                Else
                    xDateNextService.Text = ""
                End If

                'chkStatus.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("StatusFlag")))

                'ctlOrder.MedOrder_DeleteAll(StrNull2Long(hdServiceUID.value), FORM_TYPE_ID_A4F, Request.Cookies("LocationID").Value)
                LoadMedOrder()

                If DBNull2Zero(.Item("StatusFlag")) >= 3 Then  'Year(ctlLct.GET_DATE_SERVER.Date) <> DBNull2Zero(.Item("BYear")) 
                    cmdSave.Visible = False
                Else
                    If Request.Cookies("LocationID").Value <> String.Concat(.Item("LocationID")) Then
                        LoadLocationData(String.Concat(.Item("LocationID")))
                        If Request("t") = "edit" Then
                            cmdSave.Visible = True
                        Else
                            cmdSave.Visible = False
                        End If
                    Else
                        LoadLocationData(Request.Cookies("LocationID").Value)
                    End If
                End If


                chkStatus.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("StatusFlag")))

                'VisibleChangRate()

                'If DBNull2Zero(.Item("FollowSEQ")) >= 5 Then
                '    optNextStep.Visible = False
                '    optNextCause.Visible = False
                '    lblProblem.Visible = False
                '    txtCause.Visible = False
                'Else
                '    optNextStep.Visible = True
                '    optNextCause.Visible = True
                '    lblProblem.Visible = True
                '    txtCause.Visible = True

                '    If optNextStep.SelectedValue = "2" Then
                '        lblProblem.Visible = True
                '        optNextCause.Visible = True
                '        txtCause.Visible = True
                '    Else
                '        lblProblem.Visible = False
                '        optNextCause.Visible = False
                '        txtCause.Visible = False
                '    End If

                'End If

                LoadMedOrder()

                If DBNull2Zero(.Item("StatusFlag")) >= 3 Then  'Year(ctlLct.GET_DATE_SERVER.Date) <> DBNull2Zero(.Item("BYear")) 
                    cmdSave.Visible = False
                Else
                    If Request.Cookies("LocationID").Value <> String.Concat(.Item("LocationID")) Then
                        LoadLocationData(String.Concat(.Item("LocationID")))
                        If Request("t") = "edit" Then
                            cmdSave.Visible = True
                        Else
                            cmdSave.Visible = False
                        End If
                    Else
                        'LoadLocationData(Request.Cookies("LocationID").Value)
                    End If
                End If

                ' CompareDataFormA01()

            End With
        Else
            ' LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing

    End Sub

    Private Sub LoadMedOrderItem(ServiceUID As Long)
        Dim dtM2 As New DataTable
        dtM2 = ctlOrder.CPA_MedOrder_GetMedItemA5(FORM_TYPE_ID_A5, lblLocationID.Text, Session("patientid"), txtYear.Text, StrNull2Zero(lblSEQ.Text))
        If dtM2.Rows.Count > 0 Then
            lblNoMed.Visible = False
            With grdData
                .Visible = True
                .DataSource = dtM2
                .DataBind()
                'For i = 0 To .Rows.Count - 1
                '    .Rows(i).Cells(0).Text = i + 1
                'Next
            End With
            'Session("dtposA4") = dtM
        Else
            lblNoMed.Visible = True
            grdData.Visible = False
            grdData.DataSource = dtM2
            grdData.DataBind()

        End If
        dtM2 = Nothing
    End Sub

    Private Sub LoadFinalStopDate(Type As String, PatientID As Integer, LocationID As String, SEQNO As Integer)
        Dim dtFn As New DataTable
        dtFn = ctlOrder.Smoking_GetFinalStopDate(Type, PatientID, LocationID, SEQNO)

        If dtFn.Rows.Count > 0 Then
            dtpFinalStopDate.Text = String.Concat(dtFn.Rows(0).Item("FinalStopDate"))
            'optFinalResult.SelectedValue = String.Concat(dtFn.Rows(0)("FinalResult"))
        End If



    End Sub
    Private Sub LoadMedOrder()
        Dim dtM As New DataTable
        dtM = ctlOrder.CPA_MedOrder_GetMedA5(FORM_TYPE_ID_A5, StrNull2Zero(lblSEQ.Text), lblLocationID.Text, Session("patientid"), StrNull2Zero(txtYear.Text))
        If dtM.Rows.Count > 0 Then
            lblNoMed.Visible = False
            With grdData
                .Visible = True
                .DataSource = dtM
                .DataBind()

                For i = 0 To .Rows.Count - 1
                    .Rows(i).Cells(0).Text = i + 1
                Next

            End With
            ' Session("dtposA5") = dtM
        Else
            lblNoMed.Visible = True
            grdData.Visible = False
            grdData.DataSource = dtM
            grdData.DataBind()
        End If
        dtM = Nothing
    End Sub

    'Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click

    '    isAdd = True

    '    ctlOrder.MedOrder_DeleteStatusQ(StrNull2Long(hdServiceUID.value), FORM_TYPE_ID_A5, lblLocationID.Text)

    '    'For t = 0 To dtPOS.Rows.Count - 1
    '    '    dtPOS.Rows(t).Delete()
    '    'Next

    '    'Session("dtposA4") = Nothing
    '    'Session("dtposA4") = dtPOS

    '    'hdServiceUID.value = ""
    '    txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)
    '    'txtTime.Text = ""
    '    ddlPerson.SelectedIndex = 0
    '    txtWeight.Text = ""
    '    txtHeigh.Text = ""
    '    'txtAlcoholFQ.Text = ""
    '    txtCause.Text = ""
    '    txtCGTargetRateEnd.Text = ""
    '    txtTargetDay.Text = ""
    '    txtFollowRemark.Text = ""

    '    txtPEFR_Rate.Text = ""
    '    txtPEFR_Value.Text = ""
    '    txtTime.Text = ""
    '    txtStopDate.Text = ""
    '    txtRecommend1.Text = ""
    '    txtRecommend2.Text = ""
    '    txtRecommend3.Text = ""
    '    txtRecommend4.Text = ""
    '    LoadMedOrder(StrNull2Long(hdServiceUID.value))
    'End Sub

    Protected Sub txtWeight_TextChanged(sender As Object, e As EventArgs) Handles txtWeight.TextChanged
        CalPEFR_Rate()
        txtHeigh.Focus()
    End Sub

    Protected Sub txtHeigh_TextChanged(sender As Object, e As EventArgs) Handles txtHeigh.TextChanged
        CalPEFR_Rate()
    End Sub
    Private Sub CalPEFR_Rate()
        Dim PEF, PEFR_Standard As Double

        Dim A As Integer = StrNull2Zero(Session("age"))
        Dim H As Double = StrNull2Zero(txtHeigh.Text)
        If H > 0 Then
            If StrNull2Zero(Session("age")) >= 15 Then
                If Session("sex") = "M" Then
                    PEF = (((((-16.859 + (0.307 * A)) + (0.141 * H)) - (0.0018 * (A * A))) - (0.001 * A * H)) * 60)
                Else
                    PEF = ((((((-31.355 + (0.162 * A)) + (0.391 * H)) - (0.00084 * (A * A))) - (0.00099 * (H * H))) - (0.00072 * A * H)) * 60)
                End If
            Else
                PEF = (StrNull2Zero(txtHeigh.Text) * 5) - 400
            End If

            PEFR_Standard = Math.Round(PEF)
            If StrNull2Zero(txtPEFR_Value.Text) <> 0 Then
                txtPEFR_Rate.Text = Math.Round((StrNull2Zero(txtPEFR_Value.Text) / PEFR_Standard) * 100)
            Else
                txtPEFR_Rate.Text = "0"
            End If

            'If Not IsNumeric(txtPEFR_Rate.Text) Then
            '    txtPEFR_Rate.Text = "0"
            'End If
        End If
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        'If Trim(txtFName.Text) = "" Or Trim(txtLName.Text) = "" Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาป้อนชื่อ-นามสกุลผู้รับบริการก่อน');", True)
        '    Exit Sub
        'End If
        'If Trim(txtMobile.Text) = "" Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาป้อนเบอร์มือถือผู้รับบริการก่อน หากไม่มีให้ใส่เบอร์บ้านในช่องเบอร์มือถือได้');", True)
        '    Exit Sub
        'End If
        If Trim(txtTime.Text) <> "" Then
            If IsNumeric(txtTime.Text) = False Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาตรวจสอบระยะเวลาในการให้บริการ ควรป้อนเฉพาะตัวเลขเท่านั้น หากไม่มีข้อมูลให้ใส่ 0');", True)

                Exit Sub
            End If
        End If


        'If StrNull2Zero(txtAges.Text) = 0 Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาป้อนอายุผู้รับบริการก่อน');", True)
        '    Exit Sub
        'End If
        'If optFinalResult.SelectedIndex = 0 Then
        '    If dtpFinalStopDate.Text = "" Then
        '        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาป้อนวันที่หยุดสูบได้');", True)
        '        Exit Sub
        '    End If
        'End If



        '--------------------------------
        If optPatientFrom.SelectedIndex = -1 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทแหล่งที่มาก่อน');", True)
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทแหล่งที่มาก่อน ")
            Exit Sub
        End If

        If optPatientFrom.SelectedValue = "9" And txtFromRemark.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุประเภทแหล่งที่มาก่อน');", True)
            Exit Sub
        End If


        If optService.SelectedIndex = -1 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกการให้บริการมาก่อน');", True)
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกการให้บริการมาก่อน ")
            Exit Sub
        End If

        If optService.SelectedIndex = 1 Then
            If optTelepharm.SelectedIndex = -1 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทการให้บริการ Telepharmacy ก่อน ');", True)
                Exit Sub
            Else
                If optTelepharm.SelectedValue <> "1" Then
                    If txtTelepharmacyRemark.Text.Trim() = "" Then
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุชื่อ รพ. หรือ ชื่อโครงการอื่นๆ กรณี Telepharmacy ก่อน');", True)
                        Exit Sub
                    End If
                End If
            End If
        End If

        '-----------------------------------------------------
        If optPatientFrom.SelectedValue = "9" Then
            If txtFromRemark.Text.Trim() = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุแหล่งที่มาก่อน');", True)
                Exit Sub
            End If

        End If

        If optService.SelectedValue = "9" Then
            If txtServiceRemark.Text.Trim() = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุการให้บริการก่อน');", True)
                Exit Sub
            End If

        End If

        If optService.SelectedIndex = 1 Then 'case Telepharmacy
            If optTelepharm.SelectedIndex = -1 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทการให้บริการ Telepharmacy ก่อน');", True)
                Exit Sub
            End If

            If optTelepharmacyMethod.SelectedIndex = -1 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกวิธีการให้บริการ Telepharmacy ก่อน');", True)
                Exit Sub
            End If

            If optTelepharmacyMethod.SelectedIndex <> 0 Then 'case Telepharmacy
                If txtRecordMethod.Text.Trim() = "" Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุวิธีการบันทึก');", True)
                    Exit Sub
                End If
                If txtRecordLocation.Text.Trim() = "" Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุสถานที่เก็บข้อมูล');", True)
                    Exit Sub
                End If
            End If
        End If

        '------------------------------------------------------


        Dim objuser As New UserController
        Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))

        ''Dim BirthDate As String = ConvertStrDate2DBString(txtBirthDate.Text)


        Dim dStopPlan, dFollowDate, dNextService, dFinalStopDate As String
        'dStopPlan1 = ConvertStrDate2InformDateString(xDateStopPlan1.Text)
        dFollowDate = ConvertStrDate2InformDateString(txtFollowDate.Text)
        dNextService = ConvertStrDate2InformDateString(xDateNextService.Text)
        dFinalStopDate = ConvertStrDate2InformDateString(dtpFinalStopDate.Text)

        Dim CGTargetRateBegin, CGTargetRateEnd, TargetDay As Integer

        'If optStopPlane2.SelectedIndex = 1 Then
        '    CGTargetRateBegin = StrNull2Zero(txtCGTargetRateBegin.Text)
        '    CGTargetRateEnd = StrNull2Zero(txtCGTargetRateEnd.Text)
        '    TargetDay = StrNull2Zero(txtTargetDay.Text)
        'Else
        '    CGTargetRateBegin = 0
        '    CGTargetRateEnd = 0
        '    TargetDay = 0
        'End If

        If hdServiceUID.Value = "" Then 'Add new
            If ctlOrder.Smoking_Order_ChkDup(FORM_TYPE_ID_A5, Session("patientid"), StrNull2Zero(Left(Trim(ServiceDate), 4)), StrNull2Zero(lblSEQ.Text)) = True Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','A5-" & lblSEQ.Text & "มีอยู่ในระบบแล้ว');", True)
                Exit Sub
            End If
            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Smoking", "บันทึกเพิ่มกิจกรรมปรับเปลี่ยนพฤติกรรมผู้เสพยาสูบโดยเภสัชกรชุมชน (A5):<<PatientID:" & Session("patientid") & ">>", "A5")
        Else
            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Smoking", "บันทึกเพิ่มกิจกรรมปรับเปลี่ยนพฤติกรรมผู้เสพยาสูบโดยเภสัชกรชุมชน (A5) :<<PatientID:" & Session("patientid") & ">>", "A5")
        End If

        ctlOrder.A5_Save(StrNull2Long(hdA1UID.Value), StrNull2Long(hdServiceUID.Value), StrNull2Zero(txtYear.Text), FORM_TYPE_ID_A5,
                                 ServiceDate,
                                 Str2Double(txtTime.Text),
                                 lblLocationID.Text,
                               StrNull2Long(Session("patientid")),
                                 StrNull2Zero(ddlPerson.SelectedValue), optPatientFrom.SelectedValue, txtFromRemark.Text, optService.SelectedValue, txtServiceRemark.Text, optTelepharm.SelectedValue, optTelepharmacyMethod.SelectedValue, txtRecordMethod.Text, txtRecordLocation.Text, txtTelepharmacyRemark.Text, optFollowChannel.SelectedValue, StrNull2Zero(lblSEQ.Text), dFollowDate, optNicotineEffect.SelectedValue,
                                 txtNicotineRemark.Text,
                                 optMedEffect.SelectedValue,
                                 txtMedEffRemark.Text, StrNull2Zero(txtSmokeQTY.Text), txtNicotinRate.Text, StrNull2Zero(txtOften.Text), StrNull2Zero(txtMinutePerTime.Text), optChangeRate.SelectedValue, dFinalStopDate, StrNull2Zero(txtQuitDay.Text),
                                 Str2Double(txtWeight.Text),
                                 Str2Double(txtHeigh.Text), txtBP.Text,
                                 Str2Double(txtPEFR_Value.Text),
                                 Str2Double(txtPEFR_Rate.Text),
                                 Str2Double(txtCo_Value.Text), optStopPlane1.SelectedValue, txtStopPlaneOther.Text, dNextService, Convert2Status(chkStatus.Checked), Request.Cookies("username").Value)


        Dim ServiceID As Integer = ctlOrder.Smoking_Order_GetID(StrNull2Zero(txtYear.Text), lblLocationID.Text, FORM_TYPE_ID_A5, Session("patientid"), StrNull2Zero(lblSEQ.Text))

        ctlOrder.CPA_MedOrder_DeleteByStatus(FORM_TYPE_ID_A5, lblLocationID.Text, Session("patientid"), "D")

        If grdData.Rows.Count > 0 Then
            Dim UOM As String = ""
            Dim BALANCE As Integer = 0

            Dim ctlSK As New StockController
            For i = 0 To grdData.Rows.Count - 1
                UOM = ctlSK.Stock_GetUOM(StrNull2Zero(grdData.DataKeys(i).Value), lblLocationID.Text)
                BALANCE = ctlSK.Stock_GetBalance(StrNull2Zero(grdData.DataKeys(i).Value), lblLocationID.Text)

                ctlOrder.CPA_MedOrder_Add(StrNull2Zero(lblSEQ.Text), StrNull2Zero(grdData.DataKeys(i).Value), grdData.Rows(i).Cells(1).Text, grdData.Rows(i).Cells(3).Text, StrNull2Zero(grdData.Rows(i).Cells(2).Text), Request.Cookies("username").Value, FORM_TYPE_ID_A5, lblLocationID.Text, "A", StrNull2Long(Session("patientid")), StrNull2Zero(txtYear.Text))

                ctlSK.Stock_Inventory_ISS(StrNull2Zero(grdData.DataKeys(i).Value), StrNull2Zero(grdData.Rows(i).Cells(2).Text), Request.Cookies("username").Value, FORM_TYPE_ID_A5 & "-" & lblSEQ.Text, lblLocationID.Text, "A", UOM, BALANCE, "ISSPTN", ServiceID, ParseDateToSQL(txtServiceDate.Text, "/"))

            Next

        End If

        ctlOrder.A01_UpdateA05Count(StrNull2Zero(hdA1UID.Value), 1)
        ctlOrder.A01_UpdateCloseStatus(StrNull2Zero(hdA1UID.Value), Convert2Status(chkStatus.Checked), Request.Cookies("username").Value)
        LoadA05ToGrid()
        If hdServiceUID.Value = "" Then 'Add new
            Response.Redirect("ResultPage.aspx")
        Else
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalSuccess(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
        End If

    End Sub

    Private Sub txtPEFR_Value_TextChanged(sender As Object, e As EventArgs) Handles txtPEFR_Value.TextChanged
        CalPEFR_Rate()
    End Sub

    'Protected Sub optNextStep_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optNextStep.SelectedIndexChanged
    '    If optNextStep.SelectedValue = "2" Then
    '        lblProblem.Visible = True
    '        optNextCause.Visible = True
    '        txtCause.Visible = True
    '    Else
    '        lblProblem.Visible = False
    '        optNextCause.Visible = False
    '        txtCause.Visible = False
    '    End If
    'End Sub

    Protected Sub cmdAddMed_Click(sender As Object, e As EventArgs) Handles cmdAddMed.Click
        If ddlMed.SelectedValue = "12" Then
            If txtMed.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','ท่านยังไม่ได้ป้อนชื่อยา');", True)
                Exit Sub
            End If
            'Else
            '    If StrNull2Zero(txtMedQTY.Text) > StrNull2Zero(lblBalance.Text) Then
            '         ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','ยาในสต๊อกมีไม่พอ")
            '        Exit Sub
            '    End If

        End If

        'If StrNull2Zero(txtMedQTY.Text) = 0 Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','ท่านยังไม่ได้ป้อนจำนวนที่จ่าย")
        '    Exit Sub
        'End If

        'dtPOS = Session("dtposA5")
        'If dtPOS Is Nothing Then
        '    dtPOS.Columns.Add("MedTime")
        '    dtPOS.Columns.Add("itemID")
        '    dtPOS.Columns.Add("itemName")
        '    dtPOS.Columns.Add("QTY")
        'End If

        'Dim dr As DataRow = dtPOS.NewRow()
        'dr(0) = txtMedTime.Text
        'dr(1) = ddlMed.SelectedValue
        'dr(2) = ddlMed.SelectedItem.Text
        'dr(3) = txtMedQTY.Text
        'dtPOS.Rows.Add(dr)

        'With grdData
        '    .Visible = True
        '    .DataSource = dtPOS
        '    .DataBind()
        'End With

        'Session("dtposA5") = Nothing
        'Session("dtposA5") = dtPOS


        Dim MedName As String = ""

        If ddlMed.SelectedValue = "12" Then
            MedName = txtMed.Text
        Else
            MedName = ddlMed.SelectedItem.Text
        End If

        ctlOrder.CPA_MedOrder_Add(StrNull2Zero(lblSEQ.Text), ddlMed.SelectedValue, MedName, txtFrequency.Text, StrNull2Zero(txtMedQTY.Text), Request.Cookies("username").Value, FORM_TYPE_ID_A5, lblLocationID.Text, "Q", StrNull2Long(Session("patientid")), StrNull2Zero(txtYear.Text))

        LoadMedOrderItem(StrNull2Long(hdServiceUID.Value))

        txtMed.Text = ""
        txtMedQTY.Text = ""
    End Sub
    'Private Sub LoadBalanceStock()
    '    Dim ctlSK As New StockController
    '    lblBalance.Text = ctlSK.Stock_GetBalance(ddlMed.SelectedValue, lblLocationID.Text)
    '    lblUOM.Text = ctlSK.Stock_GetUOM(ddlMed.SelectedValue, lblLocationID.Text)
    'End Sub
    'Protected Sub ddlMed_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMed.SelectedIndexChanged
    '    lblUnit2.Visible = False
    '    txtMed.Visible = False
    '    'lblBalance.Visible = True
    '    'lblBalanceLabel.Visible = True

    '    'LoadBalanceStock()

    '    Select Case ddlMed.SelectedValue
    '        Case "11"
    '            lblUnit1.Visible = False
    '            lblUnit2.Visible = True
    '        Case "12"
    '            txtMed.Visible = True
    '            lblUnit1.Visible = True

    '            'lblBalance.Visible = False
    '            'lblBalanceLabel.Visible = False
    '    End Select
    'End Sub

    'Private Sub VisibleChangRate()

    '    'If optFinalResult.SelectedIndex = 0 Then
    '    '    dtpFinalStopDate.Visible = True
    '    '    lblQ5.Visible = True
    '    '    lblQ5.Text = "วันที่"
    '    '    txtStillSmoke.Text = "0"
    '    '    optChangeRate.SelectedIndex = 0
    '    '    txtStillSmoke.Visible = False
    '    '    optChangeRate.Visible = False
    '    '    lblCompareLabel.Visible = False
    '    'Else
    '    '    dtpFinalStopDate.Visible = False
    '    '    lblQ5.Visible = True
    '    '    lblQ5.Text = "มวน/วัน"
    '    '    txtStillSmoke.Visible = True
    '    '    optChangeRate.Visible = True
    '    '    lblCompareLabel.Visible = True
    '    'End If
    'End Sub

    Private Sub grdData_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel"

                    'dtPOS = Session("dtposA5")
                    'dtPOS.Rows(e.CommandArgument).Delete()
                    'Session("dtposA5") = Nothing
                    'Session("dtposA5") = dtPOS

                    ctlOrder.MedOrder_DisableByItemA5(StrNull2Long(Session("patientid")), FORM_TYPE_ID_A5, StrNull2Zero(grdData.DataKeys(e.CommandArgument).Value), StrNull2Zero(lblSEQ.Text), Request.Cookies("LocationID").Value)

                    ctlSK.Stock_Inventory_Delete(StrNull2Zero(grdData.DataKeys(e.CommandArgument).Value), StrNull2Zero(grdData.Rows(e.CommandArgument).Cells(2).Text), FORM_TYPE_ID_A5 & "-" & lblSEQ.Text, lblLocationID.Text, "ISSPTN", hdServiceUID.Value)

                    grdData.DataSource = Nothing
                    LoadMedOrderItem(StrNull2Long(hdServiceUID.Value))

                    'grdData.DataSource = dtPOS
                    'grdData.DataBind()
                    txtMedQTY.Focus()

            End Select

        End If
    End Sub
    Private Sub grdA05_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdA05.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDelete"
                    ctlOrder.Smoking_Order_Delete(e.CommandArgument)
                    acc.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_DEL, "Smoking", "Delete A05:ID=" & hdServiceUID.Value & "|SEQ=" & lblSEQ.Text, "")
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','ลบข้อมูลเรียบร้อย');", True)
                    LoadA05ToGrid()
            End Select


        End If
    End Sub


    Private Sub grdA05_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdA05.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(7).FindControl("imgDelete")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub



    Private Sub optFollowChannel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optFollowChannel.SelectedIndexChanged
        If optFollowChannel.SelectedValue = "ONLINE" Then
            optOnline.Enabled = True
        Else
            optOnline.Enabled = False
            optOnline.ClearSelection()
        End If

    End Sub
End Class