﻿Imports System.IO
Imports Newtonsoft.Json
Public Class Site
    Inherits System.Web.UI.MasterPage

    Dim dt As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Request.Cookies("RoleID") = isShopAccess Then
        '    hlnkUserName.NavigateUrl = "LocationsEdit.aspx?id=" & Request.Cookies("LocationID").Value
        'Else
        '    hlnkUserName.NavigateUrl = "#"
        'End If

        Dim ctlS As New SystemConfigController

        If ctlS.SystemConfig_GetByCode("ALIVE") = "N" Then
            Response.Redirect("404.aspx")
        End If

        If Not IsPostBack Then
            'lblVersion.Text = ctlS.SystemConfig_GetByCode("Version")
        End If
        hlnkUserName.Text = Request.Cookies("NameOfUser").Value
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            hlnkUserName.NavigateUrl = "LocationsEdit.aspx?id=" & Request.Cookies("LocationID").Value
        Else
            hlnkUserName.NavigateUrl = "#"
        End If

        'If Request.Cookies("Username").Value Is Nothing Then
        '    Response.Redirect("Default.aspx?logout=YES")
        'End If

        'If Not IsPostBack Then

        '    Dim ctlStd As New StudentController
        '    Dim ctlPsn As New PersonController

        '    Dim picPaths As String


        '    If Request.Cookies("UserProfileID").Value = 1 Then
        '        dt = ctlStd.GetStudent_ByID(Request.Cookies("ProfileID").Value)
        '        picPaths = stdPic
        '        lblLocationName.Text = "คณะเภสัชศาสตร์"
        '    Else
        '        dt = ctlPsn.Person_GetByID(StrNull2Zero(Request.Cookies("ProfileID").Value))
        '        picPaths = personPic

        '        lblLocationName.Visible = True
        '        lblLocationName.Text = String.Concat(dt.Rows(0).Item("LocationName"))

        '    End If

        '    If dt.Rows.Count > 0 Then
        '        If DBNull2Str(dt.Rows(0).Item("PicturePath")) <> "" Then

        '            Dim objfile As FileInfo = New FileInfo(Server.MapPath("~/" & picPaths & "/" & dt.Rows(0).Item("PicturePath")))

        '            If objfile.Exists Then
        '                imgUser.ImageUrl = "~/" & picPaths & "/" & dt.Rows(0).Item("PicturePath")
        '            Else
        '                imgUser.ImageUrl = "~/" & picPaths & "/nopic" & dt.Rows(0).Item("Gender") & ".jpg"

        '            End If

        '        End If
        '        hlnkUserName.Text = String.Concat(dt.Rows(0).Item("FirstName")) & " " & dt.Rows(0).Item("LastName") & " (" & dt.Rows(0).Item("NickName") & ")"


        '    End If

        '    dt = Nothing

        'End If
    End Sub
End Class