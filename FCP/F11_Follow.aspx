﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteSimple.Master" CodeBehind="F11_Follow.aspx.vb" Inherits=".F11_Follow" %>
<%@ Register src="ucHistoryYear.ascx" tagname="ucHistoryYear" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/pagestyles.css">
<link rel="stylesheet" type="text/css" href="css/rajchasistyles.css">
<link rel="stylesheet" type="text/css" href="css/uidialog.css">
<link rel="stylesheet" type="text/css" href="css/dc_table.css">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>บันทึกการติดตาม กิจกรรมการให้ความรู้ คำแนะนำและคำปรึกษาเรื่องการตรวจคัดกรองมะเร็งปากมดลูก โดยวิธี Pap Smear
        <small>Pap Smear</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content"> 
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลกิจกรรม</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">





 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="10"> <uc1:ucHistoryYear ID="ucHistoryYear1" runat="server" /></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="OptionPanels">
      <tr>
        <td width="100" class="texttopic">รหัสร้าน</td>
        <td width="120" class="LocationName">
            <asp:Label ID="lblLocationID" runat="server"></asp:Label>          </td>
        <td width="50" class="texttopic">ชื่อร้าน</td>
        <td width="300" class="LocationName">
            <asp:Label ID="lblLocationName" runat="server"></asp:Label>          </td>
        <td width="50" class="texttopic">จังหวัด</td>
        <td class="LocationName">
            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
   <tr>
    <td align="center"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">
      <tr>
        <td width="100">วันที่ให้บริการ</td>
        <td>
            <asp:TextBox ID="txtServiceDate" runat="server"></asp:TextBox>
(วว/ดด/ปปปป พ.ศ.) 
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="txtServiceDate" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="\d{2}/\d{2}/\d{4}"></asp:RegularExpressionValidator></td>
        <td width="100">รวมระยะเวลา<img src="images/star.png" width="10" height="10" /></td>
        <td>
            <asp:TextBox ID="txtTime" runat="server" Width="50px"></asp:TextBox>
&nbsp;นาที</td>
        <td>เภสัชกรผู้ให้บริการ</td>
        <td>
            <asp:DropDownList ID="ddlPerson" runat="server">            </asp:DropDownList>          </td>
      </tr>
    </table></td>
  </tr>
 
   <tr>
    <td>
      <table width="100%" border="0" cellspacing="2" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
          <td width="50" align="right"><span class="NameEN">ID :</span></td>
          <td width="50"><span class="NameEN">
              <asp:Label ID="lblID" 
                  runat="server"></asp:Label></span>            </td>
             <td width="50" align="right"><span class="NameEN">Ref.ID :</span></td>
          <td width="50"><span class="NameEN">
              <asp:Label ID="lblRefID" 
                  runat="server"></asp:Label></span>            </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td class="MenuSt">สรุปการติดตามผลการตรวจ Pap Smear</td>
  </tr>
   
  <tr>
    <td><table border="0" cellspacing="2" cellpadding="0">
     <tr>
        <td>การติดตามผล</td>
        <td colspan="3">
            <asp:RadioButtonList ID="optChannel" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="CALL">โทรถาม</asp:ListItem>
                <asp:ListItem Value="WALKIN">มีผลมาให้ที่ร้าน</asp:ListItem>
            </asp:RadioButtonList>
         </td>
        </tr>
     <tr>
        <td>สถานที่ไปตรวจ</td>
        <td colspan="3">
            <asp:RadioButtonList ID="optIsClaim" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="1">ตรงตามสิทธิ์</asp:ListItem>
                <asp:ListItem Value="0">ไม่ตรงตามสิทธิ์</asp:ListItem>
            </asp:RadioButtonList>
         </td>
        </tr>
     <tr>
        <td>ระบุสถานที่ไปตรวจ</td>
        <td colspan="3">
            <asp:RadioButtonList ID="optHospitalType" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="1">หน่วยบริการที่ขึ้นทะเบียน</asp:ListItem>
                <asp:ListItem Value="2">ศูนย์บริการ กทม. อื่นๆ</asp:ListItem>
                <asp:ListItem Value="3">รพ.เอกชน</asp:ListItem>
                <asp:ListItem Value="4">คลีนิก</asp:ListItem>
                <asp:ListItem Value="0">อื่นๆ</asp:ListItem>
            </asp:RadioButtonList>
         </td>
        </tr>
      <tr>
        <td>ระบุสถานที่</td>
        <td><asp:TextBox ID="txtHospitalName" runat="server" 
                Width="200px"></asp:TextBox></td>
        <td>วันที่ที่ไปรับการตรวจ</td>
        <td>&nbsp;<asp:TextBox ID="txtCheckDate" runat="server" Width="200px"></asp:TextBox></td>
      </tr>
     
     
    </table></td>
  </tr>
    <tr>
    <td><table border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td>ผลการตรวจ</td>
        <td>
            <asp:RadioButtonList ID="optChkResult" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="0">ปกติ</asp:ListItem>
                <asp:ListItem Value="1">ไม่ปกติ  ระบุ</asp:ListItem>
            </asp:RadioButtonList>          </td>
        <td><asp:TextBox ID="txtRemark" runat="server" 
                Width="350px"></asp:TextBox></td>
      </tr>
    </table></td>
  </tr>
   <tr>
        <td valign="top"><table border="0" cellspacing="2" cellpadding="0" align="left">
          <tr>
            <td>&nbsp;</td>
            <td colspan="4" align="left">แนบเอกสารหลักฐาน (ชื่อไฟล์ต้องเป็นภาษาอังกฤษเท่านั้น)</td>
          </tr>
          <tr>
            <td align="right">1.</td>
            <td  align="left"><asp:TextBox ID="txtMed1" runat="server" Width="300px"></asp:TextBox>            </td>
            <td width="130" align="right">แนบไฟล์หลักฐาน 1</td>
            <td width="310"><asp:FileUpload ID="FileUpload1" runat="server" Width="300px" />            </td>
            <td>
                <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank"></asp:HyperLink>
              </td>
          </tr>
          <tr>
            <td align="right">2.</td>
            <td  align="left"><asp:TextBox ID="txtMed2" runat="server" Width="300px"></asp:TextBox>            </td>
            <td align="right">แนบไฟล์หลักฐาน 2</td>
            <td><asp:FileUpload ID="FileUpload2" runat="server" Width="300px" />            </td>
            <td>
                <asp:HyperLink ID="HyperLink2" runat="server" Target="_blank"></asp:HyperLink>
              </td>
          </tr>
          <tr>
            <td align="right">3.</td>
            <td  align="left"><asp:TextBox 
                      ID="txtMed3" runat="server" Width="300px"></asp:TextBox>            </td>
            <td align="right">แนบไฟล์หลักฐาน 3</td>
            <td><asp:FileUpload ID="FileUpload3" runat="server" Width="300px" />            </td>
            <td>
                <asp:HyperLink ID="HyperLink3" runat="server" Target="_blank"></asp:HyperLink>
              </td>
          </tr>
          <tr>
            <td align="right">4.</td>
            <td  align="left"><asp:TextBox 
                      ID="txtMed4" runat="server" Width="300px"></asp:TextBox>            </td>
            <td width="130" align="right">แนบไฟล์หลักฐาน 4</td>
            <td width="310"><asp:FileUpload ID="FileUpload4" runat="server" Width="300px" />            </td>
            <td>
                <asp:HyperLink ID="HyperLink4" runat="server" Target="_blank"></asp:HyperLink>
              </td>
          </tr>
          <tr>
            <td align="right">5.</td>
            <td  align="left"><asp:TextBox 
                      ID="txtMed5" runat="server" Width="300px"></asp:TextBox>            </td>
            <td width="130" align="right">แนบไฟล์หลักฐาน 5</td>
            <td width="310"><asp:FileUpload ID="FileUpload5" runat="server" Width="300px" />            </td>
            <td>
                <asp:HyperLink ID="HyperLink5" runat="server" Target="_blank"></asp:HyperLink>
              </td>
          </tr>
          <tr>
            <td align="right">&nbsp;</td>
            <td  align="left">&nbsp;</td>
            <td colspan="3" align="left">&nbsp;</td>
          </tr>
      </table></td>
 </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="Section_Header">
      <tr>
        <td width="100">สถานะ</td>
        <td>
            <asp:CheckBox ID="chkClose" runat="server" Text="จบการทำงาน" Checked="True" />          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center">
           <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="70px" />
        &nbsp;<asp:Button ID="cmdClear" runat="server" Text="ยกเลิก" CssClass="buttonCancle" Width="70px" />
      </td>
  </tr>
</table>
     </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    </section>
</asp:Content>