﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="F13.aspx.vb" Inherits=".F13" %>
<%@ Register src="ucHistoryYear.ascx" tagname="ucHistoryYear" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/pagestyles.css">
<link rel="stylesheet" type="text/css" href="css/rajchasistyles.css">
<link rel="stylesheet" type="text/css" href="css/uidialog.css">
<link rel="stylesheet" type="text/css" href="css/dc_table.css">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>F13
        <small>กิจกรรมการให้ความรู้ คำแนะนำในการใช้ยาปฏิชีวนะอย่างสมเหตุผล
        <br />
        เพื่อป้องกันปัญหาการดื้อยา (ระบบทางเดินหายใจส่วนบน)</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">     
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลกิจกรรม</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">




<asp:HiddenField ID="HiddenField1" runat="server" />
 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
   <td height="10"> <uc1:ucHistoryYear ID="ucHistoryYear1" runat="server" /></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="OptionPanels">
      <tr>
        <td width="100" class="texttopic">รหัสร้าน</td>
        <td width="120" class="LocationName">
            <asp:Label ID="lblLocationID" runat="server"></asp:Label>          </td>
        <td width="50" class="texttopic">ชื่อร้าน</td>
        <td width="300" class="LocationName">
            <asp:Label ID="lblLocationName" runat="server"></asp:Label>          </td>
        <td width="50" class="texttopic">จังหวัด</td>
        <td class="LocationName">
            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
   <tr>
    <td align="center"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">
      <tr>
        <td width="100">วันที่ให้บริการ</td>
        <td>
            <asp:TextBox ID="txtServiceDate" runat="server"></asp:TextBox>
(วว/ดด/ปปปป พ.ศ.) 
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="txtServiceDate" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="\d{2}/\d{2}/\d{4}"></asp:RegularExpressionValidator></td>
        <td width="100">รวมระยะเวลา<img src="images/star.png" width="10" height="10" /></td>
        <td>
            <asp:TextBox ID="txtTime" runat="server" Width="50px"></asp:TextBox>
&nbsp;นาที</td>
        <td>เภสัชกรผู้ให้บริการ</td>
        <td>
            <asp:DropDownList ID="ddlPerson" runat="server">            </asp:DropDownList>          </td>
      </tr>
    </table></td>
  </tr>
   <tr>
    <td>
      <table width="100%" border="0" cellspacing="2" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
          <td width="100" align="right"><span class="NameEN">Ref.ID :</span></td>
          <td width="50"><span class="NameEN">
              <asp:Label ID="lblID" 
                  runat="server"></asp:Label></span>            </td>
        </tr>
      </table></td>
  </tr>
   <tr>
    <td align="center" class="MenuSt">ข้อมูลสุขภาพ</td>
  </tr>
   <tr>
     <td align="left"><table border="0" align="left" cellpadding="0" cellspacing="2">
       <tr>
         <td width="100">ประวัติแพ้ยา</td>
         <td><asp:TextBox ID="txtAllergy" runat="server" Width="200px"></asp:TextBox></td>
         <td width="150" align="right">โรคประจำตัว</td>
         <td><asp:TextBox ID="txtDisease" runat="server" Width="200px"></asp:TextBox></td>
       </tr>
       </table></td>
   </tr>
 
   <tr>
    <td align="left"><table border="0" align="left" cellpadding="0" cellspacing="2">
      <tr>
        <td width="100" >ยาที่ใช้ปัจจุบัน</td>
        <td align="left"><asp:TextBox ID="txtMedicineUsage" runat="server" Width="200px"></asp:TextBox></td>
        <td width="150" align="right">มีอาการก่อนมาร้านยา</td>
        <td align="left"><asp:TextBox ID="txtUsageDay" runat="server" Width="50px"></asp:TextBox> 
        &nbsp;วัน</td>
      </tr>
      <tr>
        <td colspan="2">รักษาด้วยยาต้านจุลชีพจากที่อื่นมาก่อน</td>
        <td colspan="2">
          <asp:RadioButtonList ID="optConsult" runat="server" 
                RepeatDirection="Horizontal">
            <asp:ListItem Selected="True" Value="1">ใช่</asp:ListItem>
            <asp:ListItem Value="0">ไม่ใช่</asp:ListItem>
          </asp:RadioButtonList>          </td>
        </tr>
     
    </table></td> 
  </tr>
 
   <tr>
    <td align="center" class="MenuSt">ข้อมูลการคัดกรอง
      </td>
  </tr>
  
  <tr>
    <td>
    
    <table width="100%" cellpadding="2" cellspacing="0" >
     <tr>
     <td colspan="2" align="center"  class="block_Green">มีอาการไข้หวัดมาก่อน <10 วัน</td>
     </tr>
        <tr>
          <td align="left" valign="top" class="block_white">
          
         <asp:CheckBox ID="chkC1" runat="server" Font-Bold="False" Text="ไข้ต่ำๆ หนาวๆ ร้อนๆ ปวดเมื่อยตามตัว อ่อนเพลีย Nasal symptoms เด่น" />              
                <br />
              <asp:CheckBox ID="chkC2" runat="server" Font-Bold="False" Text="น้ำมูกไหล น้ำมูกไส น้ำมูกมาก" />
              <br />
              <asp:CheckBox ID="chkC3" runat="server" Font-Bold="False" Text="เสียงแหบ" />
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<asp:CheckBox ID="chkC4" runat="server" Font-Bold="False" Text="จาม" />
              <br />
              <asp:CheckBox ID="chkC5" runat="server" Font-Bold="False" Text="ตาแดง แสบตา" />
              <asp:CheckBox ID="chkC6" runat="server" Font-Bold="False" Text="แผลในปาก" />
              <br />
              <asp:CheckBox ID="chkC7" runat="server" Font-Bold="False" Text="อาการไอ" />
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<asp:CheckBox ID="chkC8" runat="server" Font-Bold="False" Text="ท้องเสีย" />                </td>
          <td colspan="3" rowspan="3" valign="top" ><table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td colspan="4" align="center"  class="block_Green">เจ็บคอ (โดยทั่วไปมักเจ็บคอแบบเฉียบพลัน) เจ็บเวลากลืน (IDSA,2012)</td>
              </tr>
            <tr>
              <td colspan="4" align="left" class="block_white"><em>Nasal symptoms</em>: ไม่มีอาการแสดง<br />
                <asp:CheckBox ID="chkP1" runat="server" Font-Bold="False" Text="ไข้ > 38c (1) (ASU) หรือมีประวัติไข้(CDC,2012)" />
                <br />
                <asp:CheckBox ID="chkP2" runat="server" Font-Bold="False" Text="ใม่มีอาการไอ (1)" />                
                <br />
                <asp:CheckBox ID="chkP3" runat="server" Font-Bold="False" Text="มีจุดหนอง/ผ้าขาวที่ทอนซิล มีจุดเลือดออกหลังเช็ด/ลิ้นไก่บวมแดง (1)" />                
                <br />
                <asp:CheckBox ID="chkP4" runat="server" Font-Bold="False" Text="ต่อมน้ำเหลืองบริเวณลำคอโตและกดเจ็บ (1)" />
                <br />
                 <asp:CheckBox ID="chkP5" runat="server" Font-Bold="False" Text="อายุ 3-14 ปี (1)" />                 &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkP6" runat="server" Font-Bold="False" Text="อายุ 15-44 ปี (0)" /> &nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkP7" runat="server" Font-Bold="False" Text="อายุ >= 45 ปี (-1)" />  </td>
              </tr>
            <tr>
              <td align="center" class="block_Green">Viral Pharyngitis 
                  <br />
                  (GAS score &lt;=2)</td>
              <td align="center" class="block_Green">Bacterial Pharyngitis 
                  <br />
                  (GAS score =3)</td>
              <td colspan="2" align="center" class="block_Green">GAS score >=4</td>
              </tr>
            <tr>
              <td width="25%" valign="top" class="block_white"><asp:CheckBox ID="chkC8v" runat="server" Font-Bold="False" Text="ไม่จ่ายยาต้านจุชชีพ รักษาอาการ antihistamine ชนิดง่วงนอน" /></td>
              <td width="25%" valign="top" class="block_white"><asp:CheckBox ID="chkC9b" runat="server" Font-Bold="False" Text='ไม่แนะนำยาต้านจุชชีพ ขึ้นอยู่กับดุลพินิจเภสัชกร' /></td>
              <td width="25%" valign="top" class="block_white"><asp:CheckBox ID="chkC10" runat="server" Font-Bold="False" Text='จ่ายยาต้านจุชชีพ 10 วัน' /></td>
              <td width="25%" valign="top" class="block_white"><asp:CheckBox ID="chkC11" runat="server" Font-Bold="False" Text='จ่ายยาต้านจุชชีพครั้งที่ 2 ' />
                <br />
                มีอาการกลับเป็นซ้ำ<br />
                ในช่วงหลายสัปดาห์หรือเดือน</td>
            </tr>
          </table>
           </td>
        </tr>
        <tr>
          <td valign="top" class="block_Green" align="center">Common Cold</td>
          </tr>
        <tr>
          <td valign="top" align="left"><asp:CheckBox ID="chkC9" runat="server" Text="ไม่จ่ายยาต้านจุชชีพ รักษาอาการ antihistamine ชนิดง่วงนอน"  /></td>
        </tr>
    </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="2">
       
          <tr>
            <td colspan="2" align="center" valign="top"  class="block_Green">มีอาการไข้หวัดมาก่อน > 10 วัน แต่น้อยกว่า 4 สัปดาห์</td>
            <td colspan="2" align="center"  valign="top" class="block_Green">มักพบว่ามีไข้ เจ็บคอมาก่อน</td>
          </tr>
       
          <tr>
            <td width="25%" align="left" valign="top" class="block_white" > <p>
              <asp:CheckBox ID="chkV1" runat="server" Font-Bold="False" Text="มีไข้ และหายไน 24-48 ชม." />              
              <br />
              <asp:CheckBox ID="chkV2" runat="server" Font-Bold="False" Text="น้ำมูกขุ่นมักพบในวันที่ 4-5 ของการเจ็บป่วย" />              
              <br />
              <asp:CheckBox ID="chkV3" runat="server" Font-Bold="False" Text="มีอาการ < 10 วัน" />              
             <br />
                <asp:CheckBox ID="chkV4" runat="server" Font-Bold="False" Text="ไม่มีอาการไซนัส แย่ลง" />
                <br />
              <asp:CheckBox ID="chkV5" runat="server" Font-Bold="False" Text="อื่นๆ" />                            
              &nbsp;
    <asp:TextBox ID="txtV5" runat="server" Width="200px"></asp:TextBox>
                          </p>              </td>
            <td   valign="top" align="left" class="block_white"> <asp:CheckBox ID="chkB1" runat="server" Font-Bold="False" Text="ผู้ที่มีหนองไหลออกจากจมูก >= 10 วัน" />      
                <br />
                <asp:CheckBox ID="chkB2" runat="server" Font-Bold="False" Text="มีอาการไซนัสแย่ลงหลังจาก 5-6 วัน" />        
                <br />
                <asp:CheckBox ID="chkB3" runat="server" Font-Bold="False" Text="มีอาการรุนแรง" />        
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <asp:CheckBox ID="chkB3_1" runat="server" Font-Bold="False" Text="ไข้ > 39 C " />
                &nbsp;&nbsp;&nbsp;
              <asp:CheckBox ID="chkB3_2" runat="server" Font-Bold="False" Text="น้ำมูกเขียวเหลืองติดต่อกัน 3-4 วันแรก" />
                <br />
                <asp:CheckBox ID="chkB4" runat="server" Font-Bold="False" Text="อื่นๆ" />
              <asp:TextBox ID="txtB4" runat="server" Width="200px"></asp:TextBox>              <br />            </td>
            <td colspan="2" align="left"   valign="top" class="block_white">Octic symptoms<br />
              <asp:CheckBox ID="chkO1" runat="server" Font-Bold="False" Text="ปวดหู หูอื้อ กระสับกระส่าย จับใบหูหรือเอานิ้วแหย่รูหูเป็นระยะ" />            
              <br />
              <asp:CheckBox ID="chkO2" runat="server" Font-Bold="False" Text="มีหนองไหลจากหู" />              
              <br />
              <asp:CheckBox ID="chkO3" runat="server" Font-Bold="False" Text="ไข้" />              
              <br />
              <asp:CheckBox ID="chkO4" runat="server" Font-Bold="False" Text="เบื่ออาหาร" /></td>
          </tr>
          <tr>
            <td valign="top" class="block_Green" align="center">Acute virus rhinosinusitis</td>
            <td align="center"   valign="top" class="block_Green">Acute bacteria fhinosinusitis</td>
            <td colspan="2" align="center"   valign="top" class="block_Green">Acute otitis media (มักมีอาการแสดงใน 48 ชม.)</td>
          </tr>
          <tr>
            <td valign="top" class="block_white" align="left"><asp:CheckBox ID="chkV6" runat="server" Font-Bold="False" Text="ไม่จ่ายยาต้านจุลชีพ" />
            <br />
            ให้ยาตามอาการ</td>
            <td align="left"   valign="top" class="block_white">เคยได้ยาใน 4-6 สัปดาห์ 
              <br />
              <asp:CheckBox ID="chkB5" runat="server" Font-Bold="False" Text="จ่ายยาต้านจุลชีพ second-line นาน 7-10 วัน (ผู้ใหญ่)" />
              <br />
              <asp:CheckBox ID="chkB6" runat="server" Font-Bold="False" Text="ไม่ดีขึ้นใน 72 ชม.หลังได้ยา ---> refer" />
            <br /></td>
            <td align="left"   valign="top" class="block_white"><asp:CheckBox ID="chkO5_1" runat="server" Font-Bold="False" Text="ในผู้อายุ > 6 เดือน" />
            <br />
            ไม่มีไข้ &gt;= 39 C หรือไม่ปวดหูมาก<br />
            หรือไม่ปวดหูทั้งสองข้าง</td>
            <td align="left"   valign="top" class="block_white"><asp:CheckBox ID="chkO5_2a" runat="server" Font-Bold="False" Text="ในเด็ก <= 6 เดือน" />
            <br />
            กรณี ไม่มีไข้หรือไม่ปวดหูมาก หรือไม่ปวดหูทั้งสองข้าง<br />
            <strong>*กรณีมีไข้ &gt;= 39 C หรือ ปวดหูมกา หรือ ปวดหูทั้งสองข้าง</strong><br />
            <asp:CheckBox ID="chkO5_3a" runat="server" Font-Bold="False" Text="ผุ้มีอาการรุนแรง หรือ" />
            <br />
            <asp:CheckBox ID="chkO5_4" runat="server" Font-Bold="False" Text="อายุ <= 2 ปี หรือ" />
            <br />
            <asp:CheckBox ID="chkO5_5" runat="server" Font-Bold="False" Text="ภูมิคุ้มกันบกพร่อง" /></td>
          </tr>
          <tr>
            <td valign="top" class="block_white" align="left"><asp:CheckBox ID="chkV7" runat="server" Font-Bold="False" Text="จ่ายยาต้านจุลชีพ " />
            <br />
            กรณีอาการไม่ดีขึ้นใน 48-72 ชม.</td>
            <td align="left"   valign="top" class="block_white"><p>กรณี ไม่เคยได้รับยาและไม่มีปัจจัยเสี่ยง<br />
              <asp:CheckBox ID="chkB7" runat="server" Font-Bold="False" Text="จ่ายยาต้านจุลชีพ first-line empiric therapy นาน 5-7 วัน" />
                <br />
            (ผู้ใหญ่) และ 10-14 วัน (เด็ก) ถ้าอาการไม่ดีขึ้นใน 72 ชม.หลังให้เปลี่ยนชนิดยา</p>
            </td>
            <td align="left"   valign="top" class="block_white"><asp:CheckBox ID="chkO5_2" runat="server" Font-Bold="False" Text="ไม่จ่ายยาต้านจุลชีพ " />
              <br />
              <asp:CheckBox ID="chkO5_3" runat="server" Font-Bold="False" Text="จ่ายยาต้านจุลชีพ 5-7 วัน" />
              <br />
              กรณีอาการไม่ดีขึ้นใน 48-72 ชม.</td>
            <td align="left"   valign="top" class="block_white"><asp:CheckBox ID="chkO5_6" runat="server" Font-Bold="False" Text="จ่ายยาต้านจุลชีพ 10 วัน" /></td>
          </tr>
      </table></td>
  </tr>
 
 
   <tr>
     <td><table width="100%" border="0" cellspacing="2" cellpadding="0">
       <tr>
         <td width="50%" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
           
           <tr>
             <td align="center" class="MenuSt"   colspan="2">ยาที่ร้านยาจ่ายให้ (ระบุ ชื่อยา ขนาด วิธีรับประทาน จำนวนเม็ดยา)</td>
             </tr>
           <tr>
             <td align="right">1.</td>
             <td>
               <asp:DropDownList ID="ddlMedicineGroup1" runat="server">
                 <asp:ListItem>NSAIDs</asp:ListItem>
                 <asp:ListItem>ยาละลายเสมหะ/แก้ไอ</asp:ListItem>
                 <asp:ListItem>ยาปฏิชีวนะ</asp:ListItem>
                 <asp:ListItem>อื่นๆ</asp:ListItem>
                 </asp:DropDownList>
               <asp:TextBox ID="txtMed1" runat="server" Width="250px"></asp:TextBox></td>
             </tr>
           <tr>
             <td align="right">2.</td>
             <td>
               <asp:DropDownList ID="ddlMedicineGroup2" runat="server">
                 <asp:ListItem>NSAIDs</asp:ListItem>
                 <asp:ListItem>ยาละลายเสมหะ/แก้ไอ</asp:ListItem>
                 <asp:ListItem>ยาปฏิชีวนะ</asp:ListItem>
                 <asp:ListItem>อื่นๆ</asp:ListItem>
                 </asp:DropDownList>
               <asp:TextBox ID="txtMed2" runat="server" Width="250px"></asp:TextBox></td>
             </tr>
           <tr>
             <td align="right">3.</td>
             <td>
               <asp:DropDownList ID="ddlMedicineGroup3" runat="server">
                 <asp:ListItem>NSAIDs</asp:ListItem>
                 <asp:ListItem>ยาละลายเสมหะ/แก้ไอ</asp:ListItem>
                 <asp:ListItem>ยาปฏิชีวนะ</asp:ListItem>
                 <asp:ListItem>อื่นๆ</asp:ListItem>
                 </asp:DropDownList>
               <asp:TextBox ID="txtMed3" runat="server" Width="250px"></asp:TextBox></td>
             </tr>
           <tr>
             <td align="right">4.</td>
             <td>
               <asp:DropDownList ID="ddlMedicineGroup4" runat="server">
                 <asp:ListItem>NSAIDs</asp:ListItem>
                 <asp:ListItem>ยาละลายเสมหะ/แก้ไอ</asp:ListItem>
                 <asp:ListItem>ยาปฏิชีวนะ</asp:ListItem>
                 <asp:ListItem>อื่นๆ</asp:ListItem>
                 </asp:DropDownList>
               <asp:TextBox ID="txtMed4" runat="server" Width="250px"></asp:TextBox></td>
             </tr>
           <tr>
             <td align="right">5.</td>
             <td>
               <asp:DropDownList ID="ddlMedicineGroup5" runat="server">
                 <asp:ListItem>NSAIDs</asp:ListItem>
                 <asp:ListItem>ยาละลายเสมหะ/แก้ไอ</asp:ListItem>
                 <asp:ListItem>ยาปฏิชีวนะ</asp:ListItem>
                 <asp:ListItem>อื่นๆ</asp:ListItem>
                 </asp:DropDownList>
               <asp:TextBox ID="txtMed5" runat="server" Width="250px"></asp:TextBox></td>
             </tr>
           <tr>
             <td align="right">&nbsp;</td>
             <td>&nbsp;</td>
             </tr>
           </table></td>
         </tr>
       <tr>
         <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="2">
           <tr>
             <td align="center" class="MenuSt" colspan="2">ผลการติดตาม</td>
             </tr>
           <tr>
             <td class="texttopic">ติดตามโดย</td>
             <td align="left"><asp:RadioButtonList ID="optChannel" runat="server" 
                 RepeatDirection="Horizontal">
               <asp:ListItem Selected="True" Value="CALL">โทรติดตาม</asp:ListItem>
               <asp:ListItem Value="WALKIN">ผู้รับบริการกลับมาร้านยา</asp:ListItem>
               </asp:RadioButtonList></td>
             </tr>
           <tr>
             <td class="texttopic">ระยะเวลา</td>
             <td align="left"><asp:RadioButtonList ID="optChannelTime" runat="server" RepeatDirection="Horizontal">
               <asp:ListItem Selected="True" Value="3">3 วันหลังจากมารับบริการที่ร้าน</asp:ListItem>
               <asp:ListItem Value="5">5 วันหลังจากมารับบริการที่ร้าน</asp:ListItem>
               <asp:ListItem Value="7">7 วันหลังจากมารับบริการที่ร้าน</asp:ListItem>
               </asp:RadioButtonList></td>
             </tr>
           <tr>
             <td class="texttopic">ติดตามวันที่</td>
             <td align="left"><asp:TextBox ID="txtFollowDate" runat="server"></asp:TextBox></td>
             </tr>
           <tr>
             <td valign="top" class="texttopic">ผลการรักษา</td>
             <td align="left"><asp:RadioButtonList ID="optFollowup" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
               <asp:ListItem Selected="True" Value="0">หาย  โดยไม่ได้มีการรักษาเพิ่มเติม</asp:ListItem>
               <asp:ListItem Value="1">ไม่หาย กลับมารักษาที่ร้านยาต่อ</asp:ListItem>
               <asp:ListItem Value="2">หาย  จากการไปรักษาต่อ</asp:ListItem>
                 <asp:ListItem Value="3">ไม่หาย  จากการไปรักษาต่อ</asp:ListItem>
               </asp:RadioButtonList></td>
             </tr>
           <tr>
             <td align="right" valign="top" class="texttopic">
                 <asp:Label ID="lblCause" runat="server" Text="เพราะ"></asp:Label>
               </td>
             <td align="left"><table border="0" cellspacing="0" cellpadding="0">
                 <tr>
                   <td><asp:RadioButtonList ID="optCause" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                     <asp:ListItem Selected="True" Value="0">ไม่ได้ปฏิบัติตัวตามคำแนะนำ</asp:ListItem>
                     <asp:ListItem Value="1">ไม่ได้รับประทานยาตามคำแนะนำ</asp:ListItem>
                     <asp:ListItem Value="2">อื่นๆ</asp:ListItem>
                   </asp:RadioButtonList></td>
                   <td><asp:TextBox ID="txtCauseRemark" runat="server" Width="200px"></asp:TextBox></td>
                 </tr>
               </table></td>
             </tr>
           <tr>
             <td class="texttopic">สถานพยาบาล</td>
             <td align="left"><asp:RadioButtonList ID="optPlace" runat="server" RepeatDirection="Horizontal">
               <asp:ListItem Selected="True" Value="1">โรงพยาบาล</asp:ListItem>
               <asp:ListItem Value="2">คลินิก</asp:ListItem>
               <asp:ListItem Value="3">ร้านยาอื่น</asp:ListItem>
               </asp:RadioButtonList></td>
             </tr>
           <tr>
             <td class="texttopic">ระบุชื่อสถานพยาบาล</td>
             <td align="left"><asp:TextBox ID="txtHospitalName" runat="server" Width="250px"></asp:TextBox></td>
             </tr>
           <tr>
             <td class="texttopic">ระบุชื่อยาจากสถานพยาบาล</td>
             <td align="left"><asp:TextBox ID="txtMedicineFromHospital" runat="server" Width="250px"></asp:TextBox></td>
             </tr>
         </table></td>
         </tr>
     </table>
     </td>
   </tr>
  
    <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="dc_table_s3">
     <thead>
      <tr>
        <th scope="col" width="50%" valign="top" class="texttopic">แจ้งสิทธิประโยช์  สปสช.</th>
      </tr>
       </thead>
      <tr>
        <td align="left">
              <table border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td><asp:CheckBox ID="chkNHSO1" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวจคัดกรองความเสี่ยงDM/HT" /></td>
                  <td><asp:CheckBox ID="chkNHSO2" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวจคัดกรองความเสี่ยงมะเร็งปากมดลูก/มะเร็งเต้านม" /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO3" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับวัคซีนเสริมสร้างภูมิคุ้มกันโรค" /></td>
                  <td><asp:CheckBox ID="chkNHSO4" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับคำแนะนำปรึกษาการวางแผนครอบครัว / คุมกำเนิด" /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO5" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการดูแล/หญิงมีครรภ์ " /></td>
                  <td><asp:CheckBox ID="chkNHSO6" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวดเอดส์ ฟรี ปีละ 2 ครั้ง " /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO7" runat="server" Font-Bold="False" Text="การแจ้งย้ายสิทธิ " /></td>
                  <td><asp:CheckBox ID="chkNHSO8" runat="server" Font-Bold="False" Text="สายด่วน 1330" /></td>
                </tr>
              </table></td>
      </tr>
    </table></td>
  </tr>  
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="Section_Header">
      <tr>
        <td width="100">สถานะ</td>
        <td>
            <asp:CheckBox ID="chkStatus" runat="server" Text="จบการทำงาน" />          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center">
        <asp:Button ID="cmdSave" runat="server" CssClass="buttonSave" Text="บันทึก" Width="70px" />
&nbsp;<asp:Button ID="cmdClear" runat="server" CssClass="buttonCancle" Text="ยกเลิก" Width="70px" />
      </td>
  </tr>
</table>
    
 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    </section>
</asp:Content>