﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="FormMenu.aspx.vb" Inherits=".FormMenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>บันทึกกิจกรรม
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">  
     <asp:Panel ID="pnProjectF" runat="server">
     <h4 class="text-blue text-bold">โครงการให้คำปรึกษาฯ</h4> 
<br />
           <!--    
     <div class="row">


        <div class="col-lg-6 col-xs-6">
          
          <div class="small-box bg-teal">
            <div class="inner">
              <h3>F01</h3>

              <p>การคัดกรองความเสี่ยงในกลุ่มภาวะโรคเมตาบอลิก (โรคเบาหวาน โรคความดันโลหิตสูง และโรคอ้วน)</p>
            </div>
            <div class="icon">
              <i class="ion ion-man"></i>
            </div>
            <a href="F01.aspx?ActionType=frm&t=new" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        <div class="col-lg-6 col-xs-6">
          
          <div class="small-box bg-green">
            <div class="inner">
              <h3>F02-F03<sup style="font-size: 20px"></sup></h3>

              <p>กิจกรรมการให้ความรู้ คำแนะนำและคำปรึกษาในกลุ่ม Metabolic Syndrome</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-contact"></i>
            </div>
            <a href="FormList.aspx?ActionType=frm" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div> 
         
     </div>

       
        <div class="col-lg-3 col-xs-6">
  
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>F04</h3>

              <p>การให้ความรู้ คำแนะนำและคำปรึกษาเรื่องการสร้างเสริมภูมิคุ้มกันโรคด้วยวัคซีนในเด็กอายุ 0-14 ปี(EPI Program)</p>
            </div>
            <div class="icon">
              <i class="ion ion-social-reddit-outline"></i>
            </div>
            <a href="F04.aspx?ActionType=frm" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
    
        <div class="col-lg-3 col-xs-6">
  
          <div class="small-box bg-red">
            <div class="inner">
              <h3>F5</h3>

              <p>การให้ความรู้ การสร้างเสริมสุขภาพและป้องกันการเป็นโรคเรื้อรังที่เกี่ยวกับระบบทางเดินหายใจ</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-locate"></i>
            </div>
            <a href="F05.aspx?ActionType=frm" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
      </div>
     
        <div class="row">
        <div class="col-lg-3 col-xs-6">
          
          <div class="small-box bg-maroon">
            <div class="inner">
              <h3>F06</h3>

              <p>กิจกรรมการให้ความรู้ คำแนะนำและคำปรึกษาในการการสร้างเสริมสุขภาพและป้องกันโรคโดยการฝากครรภ์</p>
            </div>
            <div class="icon">
              <i class="ion ion-woman"></i>
            </div>
            <a href="F06.aspx?ActionType=frm&t=new" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        <div class="col-lg-3 col-xs-6">
          
          <div class="small-box bg-purple">
            <div class="inner">
              <h3>F07<sup style="font-size: 20px"></sup></h3>

              <p>กิจกรรมการให้ความรู้ คำแนะนำและคำปรึกษาเรื่องยาคุมกำเนิด ชนิดรับประทาน</p>
            </div>
            <div class="icon">
              <i class="ion ion-female"></i>
            </div>
            <a href="F07.aspx?ActionType=frm" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        <div class="col-lg-3 col-xs-6">
          
          <div class="small-box bg-primary">
            <div class="inner">
              <h3>F08</h3>

              <p>กิจกรรมการให้ความรู้ คำแนะนำและคำปรึกษาเรื่องยาคุมฉุกเฉิน</p>
            </div>
            <div class="icon">
              <i class="ion ion-flash-off"></i>
            </div>
            <a href="F08.aspx?ActionType=frm" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        <div class="col-lg-3 col-xs-6">
          
          <div class="small-box bg-teal">
            <div class="inner">
              <h3>F09</h3>

              <p>กิจกรรมการให้ความรู้ คำแนะนำและคำปรึกษาเรื่องการใช้ถุงยางอนามัย</p>
            </div>
            <div class="icon">
              <i class="ion ion-happy"></i>
            </div>
            <a href="F09.aspx?ActionType=frm" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
      </div>
      
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>F10</h3>
              <p>คำแนะนำและคำปรึกษาเรื่องโรคที่เกิดจากการมีเพศสัมพันธ์ STI และ Pre-VCT ในกลุ่มเสี่ยงการติดเชื้อ HIV</p>
            </div>
            <div class="icon">
              <i class="ion ion-umbrella"></i>
            </div>
            <a href="F10.aspx?ActionType=frm&t=new" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        <div class="col-lg-3 col-xs-6">
          
          <div class="small-box bg-olive">
            <div class="inner">
              <h3>F11<sup style="font-size: 20px"></sup></h3>

              <p>กิจกรรมการให้ความรู้ คำแนะนำและคำปรึกษาเรื่องการตรวจคัดกรองมะเร็งปากมดลูก โดยวิธี Pap Smear<br /></p>
            </div>
            <div class="icon">
              <i class="ion ion-social-pinterest"></i>
            </div>
            <a href="F11.aspx?ActionType=frm" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        <div class="col-lg-3 col-xs-6">
          
          <div class="small-box bg-fuchsia">
            <div class="inner">
              <h3>F12</h3>

              <p>การให้ความรู้ คำแนะนำและคำปรึกษากรณีท้องไม่พร้อม<br /><br /></p>
            </div>
            <div class="icon">
              <i class="ion ion-social-github"></i>
            </div>
            <a href="F12.aspx?ActionType=frm" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        <div class="col-lg-3 col-xs-6">
          
          <div class="small-box bg-navy">
            <div class="inner">
              <h3>F13</h3>

              <p>การใช้ยาปฏิชีวนะอย่างสมเหตุสมผล เพื่อป้องกันปัญหาการดื้อยา (ระบบทางเดินหายใจส่วนบน)</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-gear-outline"></i>
            </div>
            <a href="F13.aspx?ActionType=frm" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
      </div>
      

      <div class="row">
        <div class="col-lg-3 col-xs-6">
          
          <div class="small-box bg-red">
            <div class="inner">
              <h3>F14</h3>

              <p>การใช้ยาปฏิชีวนะอย่างสมเหตุสมผล เพื่อป้องกันปัญหาการดื้อยา (โรคท้องร่วงเฉียบพลัน)</p>
            </div>
            <div class="icon">
              <i class="ion ion-load-a"></i>
            </div>
            <a href="F14.aspx?ActionType=frm&t=new" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
       --> 

 <div class="row">
        <div class="col-lg-6 col-xs-6">
          
          <div class="small-box bg-purple">
            <div class="inner">
              <h3>F15<sup style="font-size: 20px"></sup></h3>

              <p>แบบคัดกรองโรคหืดและโรคปอดอุดกั้นเรื้อรัง</p>
            </div>
            <div class="icon">
              <i class="ion ion-ios-sunny"></i>
            </div>
            <a href="F15.aspx?ActionType=frm" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
         <!--    
        
        <div class="col-lg-6 col-xs-6">
          
          <div class="small-box bg-red">
            <div class="inner">
              <h3>F17</h3>

              <p>แบบประเมินความเสี่ยงเบื้องต้นโรคต่อการเป็นหลอดเลือดสมอง</p>
            </div>
            <div class="icon">
              <i class="ion ion-fork-repo"></i>
            </div>
            <a href="F17.aspx?ActionType=frm" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        -->
  <div class="col-lg-6 col-xs-6">
          
          <div class="small-box bg-maroon">
            <div class="inner">
              <h3>F19<sup style="font-size: 20px"></sup></h3>

              <p>แบบคัดกรอง/ประเมิน ปัญหาการดื่มสุรา</p>
            </div>
            <div class="icon">
              <i class="ion ion-beer"></i>
            </div>
            <a href="F19.aspx?ActionType=frm" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

      </div>
      
      <div class="row">
            <!--    
        <div class="col-lg-6 col-xs-6">
          
          <div class="small-box bg-purple">
            <div class="inner">
              <h3>F18</h3>

              <p>แบบคัดกรอง/แบบประเมิน โรคซึมเศร้าและความเครียด</p>
            </div>
            <div class="icon">
              <i class="ion ion-sad"></i>
            </div>
            <a href="F18.aspx?ActionType=frm&t=new" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        -->
      
        
       
      </div>
      

     </asp:Panel>
<asp:Panel ID="pnHomeVisit" runat="server"> 
      <h4 class="text-maroon text-bold">โครงการ MTM/Home Visit</h4>

      <div class="row">
        <div class="col-lg-12 col-xs-6">
          
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>MTM</h3>

              <p>การดูแลเรื่องการใช้ยา</p>
            </div>
            <div class="icon">
              <i class="ion ion-home"></i>
            </div>
            <a href="MTM.aspx?ActionType=frm&t=new" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
              
      </div>
      
 </asp:Panel>
 <asp:Panel ID="pnProjectA" runat="server">
    <h4 class="text-success text-bold">โครงการหยุดสูบบุหรี่</h4>

      <div class="row">
        <div class="col-lg-6 col-xs-6">
          
          <div class="small-box bg-green">
            <div class="inner">
              <h3>A1-A4</h3>

              <p>กิจกรรมปรับเปลี่ยนพฤติกรรมผู้เสพยาสูบโดยเภสัชกรชุมชน</p>
            </div>
            <div class="icon">
              <i class="ion ion-no-smoking"></i>
            </div>
            <a href="A1.aspx?ActionType=frm&t=new" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
        <div class="col-lg-6 col-xs-6">
          
          <div class="small-box bg-olive">
            <div class="inner">
              <h3>A5<sup style="font-size: 20px"></sup></h3>

              <p>ติดตามผลกิจกรรมปรับเปลี่ยนพฤติกรรมผู้เสพยาสูบโดยเภสัชกรชุมชน</p>
            </div>
            <div class="icon">
              <i class="ion ion-no-smoking"></i>
            </div>
            <a href="A5.aspx?ActionType=frm&t=new" class="small-box-footer">บันทึกกิจกรรม <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        
       
      </div>
      
</asp:Panel>

    <asp:Panel ID="pnCovid" runat="server">
    <h4 class="text-red text-bold">โครงการ COVID-19</h4>

      <div class="row">
        <div class="col-lg-6 col-xs-6">
          
          <div class="small-box bg-red">
            <div class="inner">
              <h3>Home Isolation COVID</h3>

              <p>แบบฟอร์มการติดตามผู้ป่วย Home Isolation (COVID-19)</p>
            </div>
            <div class="icon">
              <i class="ion ion-load-a"></i>
            </div>
            <a href="Covid.aspx?ActionType=cov&t=new" class="small-box-footer">บันทึกกิจกรรม<i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

          <div class="col-lg-6 col-xs-6">
          
          <div class="small-box bg-purple">
            <div class="inner">
              <h3>Long COVID</h3>

              <p>แบบฟอร์มติดตามอาการ Long Covid</p>
            </div>
            <div class="icon">
              <i class="ion ion-clock"></i>
            </div>
            <a href="Covid2.aspx?ActionType=cov2&t=new" class="small-box-footer">บันทึกกิจกรรม<i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
         <div class="col-lg-6 col-xs-6">          
          <div class="small-box bg-primary">
            <div class="inner">
              <h3>Self Isolation Care</h3>
              <p>แบบฟอร์มการดูแลผู้ป่วย COVID 19 โดยเภสัชกรชุมชน (Self Isolation Care  by Community Pharmacist)</p>
            </div>
            <div class="icon">
              <i class="ion ion-clock"></i>
            </div>
            <a href="Covid3.aspx?ActionType=cov3&t=new" class="small-box-footer">บันทึกกิจกรรม<i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
       
      </div>
      
</asp:Panel>



    </section>
</asp:Content>
