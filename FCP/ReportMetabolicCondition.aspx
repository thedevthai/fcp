﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="ReportMetabolicCondition.aspx.vb" Inherits=".ReportMetabolicCondition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1><asp:Label ID="lblReportHeader" runat="server" Text="รายงาน"></asp:Label>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
 
    <table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td height="350" valign="top">
  <table width="100%" border="0" cellspacing="3" cellpadding="0">
    <tr>
      <td  valign="top"> <table border="0" align="center" cellpadding="0" cellspacing="3">
  <tr>
    <td>วันที่ให้บริการตั้งแต่</td>
    <td>
        <asp:TextBox ID="txtStartDate" runat="server">        </asp:TextBox>
      </td>
    <td>ถึง</td>
    <td>
        <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>   </td>
  </tr>
  <tr>
    <td>จังหวัด</td>
    <td colspan="3" align="left"><asp:DropDownList ID="ddlProvince" runat="server" CssClass="OptionControl"> </asp:DropDownList></td>
  </tr>
  <tr>
    <td>
        <asp:Label ID="lblCon1" runat="server" Text="BP เฉลี่ย"></asp:Label>
      </td>
    <td colspan="3" align="left">
        <asp:DropDownList ID="ddlBP" runat="server" CssClass="OptionControl">
            <asp:ListItem Selected="True" Value="0">--- ทั้งหมด ---</asp:ListItem>
            <asp:ListItem Value="0-120">&lt;=120</asp:ListItem>
            <asp:ListItem Value="121-140">121-140</asp:ListItem>
            <asp:ListItem Value="141-200">&gt;140</asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="ddlAge" runat="server" CssClass="OptionControl">
            <asp:ListItem Selected="True" Value="0">--- ทั้งหมด ---</asp:ListItem>
            <asp:ListItem>15-20</asp:ListItem>
            <asp:ListItem>21-30</asp:ListItem>
            <asp:ListItem>31-40</asp:ListItem>
            <asp:ListItem>41-50</asp:ListItem>
            <asp:ListItem>51-60</asp:ListItem>
            <asp:ListItem Value="61-200"> &gt; 60</asp:ListItem>
        </asp:DropDownList>
      </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="3" align="left">&nbsp;</td>
  </tr>
          
</table></td>
    </tr>
    <tr>
      <td align="center">
          <asp:Button ID="cmdPrint" runat="server" text="ดูรายงาน" cssclass="buttonSave" />
          <asp:Button ID="cmdExcel" runat="server" text="Export" cssclass="buttonGreen" />        </td>
    </tr>
  </table>
  </td> 
      </tr>
    </table>
    </section>
</asp:Content>
