﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="F18.aspx.vb" Inherits=".F18" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>
<%@ Register src="ucHistoryYear.ascx" tagname="ucHistoryYear" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/pagestyles.css">
<link rel="stylesheet" type="text/css" href="css/rajchasistyles.css">
<link rel="stylesheet" type="text/css" href="css/uidialog.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>F18
        <small>แบบคัดกรอง/แบบประเมิน โรคซึมเศร้าและความเครียด</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">   


    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลกิจกรรม</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body"> 
        <table class="table table-hover">
      <tr>
        <td width="100" class="texttopic">รหัสร้าน</td>
        <td width="120" class="LocationName">
            <asp:Label ID="lblLocationID" runat="server"></asp:Label>
          </td>
        <td width="50" class="texttopic">ชื่อร้าน</td>
        <td width="300" class="LocationName">
            <asp:Label ID="lblLocationName" runat="server"></asp:Label>          </td>
        <td width="50" class="texttopic">จังหวัด</td>
        <td class="LocationName">
            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>
          </td>
           <td  align="left"><span class="NameEN">Ref.ID :<asp:Label ID="lblID" 
                  runat="server"></asp:Label></span></td>
      </tr>
    </table>
                  
        
        <table  class="table table-hover">
      <tr>        
        
        <td width="100">วันที่ให้บริการ</td>
        <td>
            <asp:TextBox ID="txtServiceDate" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
(วว/ดด/ปปปป พ.ศ.) 
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="txtServiceDate" CssClass="text-red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="\d{2}/\d{2}/\d{4}"></asp:RegularExpressionValidator></td>
        <td><span class="pull-right">รวมระยะเวลา (นาที)<img src="images/star.png" width="10" height="10" /></span></td>
        <td  Width="70px">
            <asp:TextBox ID="txtTime" runat="server" Width="60px"></asp:TextBox></td>
        <td><span class="pull-right">เภสัชกรผู้ให้บริการ</span> </td>
        <td>
            <asp:DropDownList ID="ddlPerson" runat="server" CssClass="form-control select2">            </asp:DropDownList>          </td>
      </tr>
    </table>




 </div>
           
          </div>
   
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">แบบคัดกรองโรคซึมเศร้า (2Q)
        </h3>
         <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <div class="row">

        <div class="col-md-4">
          <div class="form-group">
            <label>1. ใน 2 สัปดาห์ที่ผ่านมา รวมวันนี้ ท่านรู้สึก หดหู่ เศร้า หรือท้อแท้สิ้นหวัง หรือไม่</label>
            <asp:RadioButtonList ID="optQ1" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
 <asp:ListItem Value="Y">มี</asp:ListItem>
              <asp:ListItem Selected="True" Value="N">ไม่มี</asp:ListItem>
             
            </asp:RadioButtonList>
          </div>
        </div>

      


      </div>
      <div class="row">

        <div class="col-md-4">
          <div class="form-group">
            <label>2. ใน 2 สัปดาห์ที่ผ่านมา รวมวันนี้ ท่านรู้สึกเบื่อทำอะไรไม่เพลิดเพลิน หรือไม่</label>
            <asp:RadioButtonList ID="optQ2" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
 <asp:ListItem Value="Y">มี</asp:ListItem>
              <asp:ListItem Selected="True" Value="N">ไม่มี</asp:ListItem>
             
            </asp:RadioButtonList>

          </div>
        </div>


      </div>
      


    </div>
        <div class="box-footer clearfix">
          <span class="text-primary text-bold"> การแปลผล</span> <br />
            - ถ้าคำตอบ : ไม่มีทั้ง 2 คำถาม คือ ปกติ ไม่เป็นโรคซึมเศร้า <br />
            - ถ้าคำตอบ : มี ข้อใดข้อหนึ่ง หรือ ทั้ง 2 ข้อ (มีอาการใด ๆ ใน คำถามที่ 1 และ  2 หมายถึง "เป็นผู้มีความเสี่ยง" หรือ "มีแนวโน้มที่จะเป็นโรคซึมเศร้า" ให้ประเมินต่อด้วยแบบประเมิน โรคซึมเศร้า 9Q
            </div>
  </div>

    
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">แบบประเมินโรคซึมเศร้า (9Q)
      </h3>
         <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table class="table table-hover">
        <tr>
          <th width="50%">ในช่วง 2 สัปดาห์ที่ผ่านมา รวมทั้งวันนี้ท่านมีอาการเหล่านี้บ่อยแค่ไหน</th>
          <th></th>
        </tr>
        <tr>
          <td>1. เบื่อไม่สนใจอยากทำอะไร</td>
          <td>
            <asp:RadioButtonList ID="opt9Q1" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
              <asp:ListItem Value="0">ไม่มีเลย</asp:ListItem>
              <asp:ListItem Value="1">เป็นบางวัน 1-7 วัน</asp:ListItem>
              <asp:ListItem Value="2">เป็นบ่อย &gt;7 วัน</asp:ListItem>
              <asp:ListItem Value="3">เป็นทุกวัน</asp:ListItem>              
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>
          <td>2. ไม่สบายใจ ซึมเศร้า ท้อแท้</td>

          <td>
            <asp:RadioButtonList ID="opt9Q2" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
             <asp:ListItem Value="0">ไม่มีเลย</asp:ListItem>
              <asp:ListItem Value="1">เป็นบางวัน 1-7 วัน</asp:ListItem>
              <asp:ListItem Value="2">เป็นบ่อย &gt;7 วัน</asp:ListItem>
              <asp:ListItem Value="3">เป็นทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
            </td>

        </tr>
        <tr>
          <td>3. หลับยาก หรือหลับๆ ตื่นๆ หรือหลับมากไป</td>
          <td>
            <asp:RadioButtonList ID="opt9Q3" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
              <asp:ListItem Value="0">ไม่มีเลย</asp:ListItem>
              <asp:ListItem Value="1">เป็นบางวัน 1-7 วัน</asp:ListItem>
              <asp:ListItem Value="2">เป็นบ่อย &gt;7 วัน</asp:ListItem>
              <asp:ListItem Value="3">เป็นทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
          <td>4. เหนื่อยง่ายหรือไม่ค่อยมีแรง</td>
          <td>
            <asp:RadioButtonList ID="opt9Q4" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
              <asp:ListItem Value="0">ไม่มีเลย</asp:ListItem>
              <asp:ListItem Value="1">เป็นบางวัน 1-7 วัน</asp:ListItem>
              <asp:ListItem Value="2">เป็นบ่อย &gt;7 วัน</asp:ListItem>
              <asp:ListItem Value="3">เป็นทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
          <td>5. เบื่ออาหารหรือกินมากเกินไป</td>
          <td>
            <asp:RadioButtonList ID="opt9Q5" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
              <asp:ListItem Value="0">ไม่มีเลย</asp:ListItem>
              <asp:ListItem Value="1">เป็นบางวัน 1-7 วัน</asp:ListItem>
              <asp:ListItem Value="2">เป็นบ่อย &gt;7 วัน</asp:ListItem>
              <asp:ListItem Value="3">เป็นทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
          <td>6. รู้สึกไม่ดีกับตัวเอง คิดว่าตัวเองล้มเหลวหรือครอบครัวผิดหวัง</td>
          <td>
            <asp:RadioButtonList ID="opt9Q6" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
              <asp:ListItem Value="0">ไม่มีเลย</asp:ListItem>
              <asp:ListItem Value="1">เป็นบางวัน 1-7 วัน</asp:ListItem>
              <asp:ListItem Value="2">เป็นบ่อย &gt;7 วัน</asp:ListItem>
              <asp:ListItem Value="3">เป็นทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
          <td>7. สมาธิไม่ดีเวลาทำอะไร เช่น ดูโทรทัศน์ ฟังวิทยุ หรือทำงานที่ต้องใช้ความตั้งใจ   </td>
          <td>
            <asp:RadioButtonList ID="opt9Q7" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
              <asp:ListItem Value="0">ไม่มีเลย</asp:ListItem>
              <asp:ListItem Value="1">เป็นบางวัน 1-7 วัน</asp:ListItem>
              <asp:ListItem Value="2">เป็นบ่อย &gt;7 วัน</asp:ListItem>
              <asp:ListItem Value="3">เป็นทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
          <td>8. พูดช้า ทำอะไรช้าลงจนคนอื่นสังเกตุเห็นได้</td>
          <td>
            <asp:RadioButtonList ID="opt9Q8" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
              <asp:ListItem Value="0">ไม่มีเลย</asp:ListItem>
              <asp:ListItem Value="1">เป็นบางวัน 1-7 วัน</asp:ListItem>
              <asp:ListItem Value="2">เป็นบ่อย &gt;7 วัน</asp:ListItem>
              <asp:ListItem Value="3">เป็นทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
            </td>
        </tr>
           <tr>
          <td>9. คิดทำร้ายตนเอง หรือคิดว่าถ้าตายไปคงจะดี</td>
          <td>
            <asp:RadioButtonList ID="opt9Q9" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
              <asp:ListItem Value="0">ไม่มีเลย</asp:ListItem>
              <asp:ListItem Value="1">เป็นบางวัน 1-7 วัน</asp:ListItem>
              <asp:ListItem Value="2">เป็นบ่อย &gt;7 วัน</asp:ListItem>
              <asp:ListItem Value="3">เป็นทุกวัน</asp:ListItem>
            </asp:RadioButtonList>
               </td>
        </tr>

           <tr>
          <td> <span class="text-primary text-bold pull-right">คะแนนรวมทั้งหมด</span></td>
          <td>
              <asp:Label ID="lbl9QTotal" runat="server" cssclass="text-primary text-bold"></asp:Label>
               &nbsp;<asp:Label ID="lbl9QResultTxt" runat="server" Visible="False"  class="text-red text-bold"></asp:Label>
       
               </td>
        </tr>

      </table>
    </div>
       <div class="box-footer clearfix text-center">
       
           <asp:Label ID="lbl9QAlert" runat="server" Text="คะแนน 9Q มากกว่าหรือเท่ากับ 7 เภสัชกรแนะนำให้ปรึกษาแพทย์" Visible="False"  class="text-red text-bold"></asp:Label>
       
  </div>
      
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">แบบประเมินความเครียด (ST5)
      </h3>
         <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table class="table table-hover">
        <tr>
          <th  width="50%">อาการหรือความรู้สึกที่เกิดในระยะ 2-4 สัปดาห์</th>
          <th>คะแนน</th>
        </tr>
        <tr>
          <td>1. มีปัญหาการนอน นอนไม่หลับหรือนอนมาก</td>
          <td>
            <asp:RadioButtonList ID="optST1" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                <asp:ListItem>0</asp:ListItem>
              <asp:ListItem>1</asp:ListItem>
              <asp:ListItem>2</asp:ListItem>
              <asp:ListItem>3</asp:ListItem> 
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>
          <td>2. มีสมาธิน้อยลง</td>

          <td>
            <asp:RadioButtonList ID="optST2" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                <asp:ListItem>0</asp:ListItem>
              <asp:ListItem>1</asp:ListItem>
              <asp:ListItem>2</asp:ListItem>
              <asp:ListItem>3</asp:ListItem>
            </asp:RadioButtonList>
          </td>

        </tr>
        <tr>
          <td>3. หงุดหงิด/กระวนกระวาย/ว้าวุ่นใจ</td>
          <td>
            <asp:RadioButtonList ID="optST3" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                <asp:ListItem>0</asp:ListItem>
              <asp:ListItem>1</asp:ListItem>
              <asp:ListItem>2</asp:ListItem>
              <asp:ListItem>3</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>
          <td>4. รู้สึกเบื่อ/เซ็ง</td>
          <td>
            <asp:RadioButtonList ID="optST4" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                <asp:ListItem>0</asp:ListItem>
              <asp:ListItem>1</asp:ListItem>
              <asp:ListItem>2</asp:ListItem>
              <asp:ListItem>3</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>
          <td>5. ไม่อยากพบปะผู้คน</td>
          <td>
            <asp:RadioButtonList ID="optST5" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                <asp:ListItem>0</asp:ListItem>
              <asp:ListItem>1</asp:ListItem>
              <asp:ListItem>2</asp:ListItem>
              <asp:ListItem>3</asp:ListItem>
            </asp:RadioButtonList>
          </td>
        </tr>
        <tr>
          <td> <span class="text-primary text-bold pull-right">คะแนนรวม</span></td>
          <td>
            
              <asp:Label ID="lblSTTotal" runat="server" CssClass="text-primary text-bold"></asp:Label>
            
          &nbsp;<asp:Label ID="lblSTResultTxt" runat="server" Visible="False"  class="text-red text-bold"></asp:Label>
       
          </td>
        </tr>


      </table>
    </div>
  </div>

      
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">สรุปผล
      </h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table class="table table-hover">
       
        <tr>
          <td>
              <asp:CheckBox ID="chkEducate1" runat="server" Text="เภสัชกรแนะนำให้พบแพทย์" />
              </td>
        </tr>
        <tr>
          <td> <asp:CheckBox ID="chkEducate2" runat="server" Text="เภสัชกรให้คำแนะนำเพื่อจัดการความเครียดเบื้องต้น ได้แก่" /><br />
              1. แนะนำให้ขอความช่วยเหลือ<br />
              2. ใช้เทคนิคจัดการความเครียดเบื้องต้น<br />
              3. ไม่กล่าวโทษตนเอง<br />
              4. คิดแก้ปัญหา<br />
              5. มองบวก<br />
          </td>

        </tr>
          
        <tr>
          <td class="MenuSt text-center">คำยินยอม</td>
        </tr>
           <tr>
          <td> 
              <asp:CheckBox ID="chkAgree" runat="server" Text="ข้าพเจ้าได้รับทราบรายละเอียดของแผนงานฯและสมัครใจเข้ารับบริการ" CssClass="text-primary text-bold" /> โดยยินยอมให้เภสัชกรผู้ให้บริการตามแผนงานฯนี้เข้าถึงข้อมูลสุขภาพของข้าพเจ้าและอนุญาตให้ผู้ควบคุมข้อมูลของแผนงานฯ เก็บรวบรวมและบันทึกข้อมูลสุขภาพของข้าพเจ้าไว้เพื่อวัตถุประสงค์ในการสร้างเสริมสุขภาพของข้าพเจ้าเองและเพื่อประโยชน์ส่วนรวมของการพัฒนางานสร้างเสริมสุขภาพและการพัฒนาวิชาชีพเภสัชกรรมชุมชน โดยจะต้องเก็บข้อมูลของข้าพเจ้าเป็นความลับและอนุญตให้เปิดเผยหรือใช้ข้อมูลของข้าพเจ้าในรูปแบบที่ไม่ระบุตัวตนของข้าพเจ้าเท่านั้น
               </td>

        </tr>
           <tr>
          <td> <asp:CheckBox ID="chkClose" runat="server" Text="จบการทำงาน" Checked="True" /></td>
        </tr>      
      </table>
    </div>
  </div> 

      <div class="text-center"><asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="100px" /></div>
    </section>
</asp:Content>
 

