﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class F03
    
    '''<summary>
    '''ucHistoryYear1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ucHistoryYear1 As Global.ucHistoryYear
    
    '''<summary>
    '''lblLocationID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLocationID As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblLocationName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLocationName As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblLocationProvince control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLocationProvince As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''txtServiceDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtServiceDate As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''RegularExpressionValidator1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator1 As Global.System.Web.UI.WebControls.RegularExpressionValidator
    
    '''<summary>
    '''txtTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTime As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''ddlPerson control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPerson As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lblID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblID As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblRefID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRefID As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblServicePlan control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblServicePlan As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblHospitalName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblHospitalName As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''optChannal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optChannal As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''txtRefDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRefDate As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtRefTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRefTime As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''chkProblem1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkProblem1 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''chkProblem2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkProblem2 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''chkProblem3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkProblem3 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''chkProblem4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkProblem4 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''chkProblem5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkProblem5 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''chkProblem6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkProblem6 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''chkProblem7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkProblem7 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''txtProblemOther control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtProblemOther As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''chkEdu1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkEdu1 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''chkEdu2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkEdu2 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''chkEdu3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkEdu3 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''chkEdu4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkEdu4 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''chkEdu5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkEdu5 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''chkEdu6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkEdu6 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''chkEdu7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkEdu7 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''txtEduOther control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEduOther As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtBP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBP As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtBP2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBP2 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtFBS control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFBS As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtRandom control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRandom As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtHourAfter control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtHourAfter As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''chkNotResponse control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkNotResponse As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''optCause control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optCause As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''txtOtherCause control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtOtherCause As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''optResultProcess control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optResultProcess As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''chkDiseases1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkDiseases1 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''chkDiseases2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkDiseases2 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''chkDiseases3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkDiseases3 As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''txtResultRemark control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtResultRemark As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''optRefer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optRefer As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''txtRemarkRefer control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRemarkRefer As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''optIsMed control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optIsMed As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''txtMed1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMed1 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''FileUpload1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FileUpload1 As Global.System.Web.UI.WebControls.FileUpload
    
    '''<summary>
    '''HyperLink1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HyperLink1 As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''txtMed2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMed2 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''FileUpload2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FileUpload2 As Global.System.Web.UI.WebControls.FileUpload
    
    '''<summary>
    '''HyperLink2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HyperLink2 As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''txtMed3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMed3 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''FileUpload3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FileUpload3 As Global.System.Web.UI.WebControls.FileUpload
    
    '''<summary>
    '''HyperLink3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HyperLink3 As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''txtMed4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMed4 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''FileUpload4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FileUpload4 As Global.System.Web.UI.WebControls.FileUpload
    
    '''<summary>
    '''HyperLink4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HyperLink4 As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''txtMed5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMed5 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''FileUpload5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents FileUpload5 As Global.System.Web.UI.WebControls.FileUpload
    
    '''<summary>
    '''HyperLink5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents HyperLink5 As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''pnBehavior control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnBehavior As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''ddlBehavior control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlBehavior As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''txtProblemBehaviorOther control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtProblemBehaviorOther As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtInterventionB control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtInterventionB As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''chkNotFollow control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkNotFollow As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''pnFat control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnFat As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''optFat control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optFat As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''pnEating control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnEating As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''optTast control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optTast As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''optResultB control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optResultB As Global.System.Web.UI.WebControls.RadioButtonList
    
    '''<summary>
    '''txtResultOtherB control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtResultOtherB As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtBegin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBegin As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''txtEnd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtEnd As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''pnAlertB control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnAlertB As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblAlertB control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblAlertB As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblUID_B control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblUID_B As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''cmdAddBehavior control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdAddBehavior As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''grdBehavior control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdBehavior As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''chkClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkClose As Global.System.Web.UI.WebControls.CheckBox
    
    '''<summary>
    '''cmdSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdSave As Global.System.Web.UI.WebControls.Button
End Class
