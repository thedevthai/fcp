﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="vmiStockMain.aspx.vb" Inherits=".vmiStockMain" %>


<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">    
     
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1> Main Stock Item
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
  
 <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">รายการสต๊อก</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">




    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td align="left" valign="top">
              <dx:ASPxPageControl ID="tabPageSearch" runat="server" ActiveTabIndex="0" BackColor="#DDE0E3" EnableTheming="True" Theme="MetropolisBlue" Width="99.9%">
                  <TabPages>
                      <dx:TabPage Name="pSearch" Text="ค้นหา / Search">
                          <ContentCollection>
                              <dx:ContentControl runat="server">
                                  <table border="0" cellpadding="1" cellspacing="1">
                                      <tr>
                                          <td align="left" class="auto-style1">ชื่อยา : </td>
                                          <td align="left" class="auto-style2">
                                              <asp:TextBox ID="txtMedName" runat="server" Width="200px"></asp:TextBox>
                                          </td>
                                          <td>
                                              <asp:Button ID="cmdSearch" runat="server" CssClass="buttonFind" Text="Search" Width="100px" />
                                          </td>
                                      </tr>
                                  </table>
                              </dx:ContentControl>
                          </ContentCollection>
                      </dx:TabPage>
                  </TabPages>
                  <Paddings Padding="0px" />
                  <Border BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" />
              </dx:ASPxPageControl>
            </td>
      </tr>
       <tr>
          <td align="left" valign="top" class="MenuSt"> รายการสต๊อกยา</td>
      </tr>
        <tr>
          <td align="center" valign="top">
              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" DataKeyNames="MedUID">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="No.">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />                      </asp:BoundField>
                <asp:BoundField DataField="StoreName" HeaderText="Store" />
                <asp:BoundField DataField="itemName" HeaderText="Item Name" >
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="itemUOM" HeaderText="UOM" />
                <asp:BoundField DataField="Stock" HeaderText="Total Balance" DataFormatString="{0:#,###}" />
                <asp:BoundField DataField="AviableBalance" HeaderText="Aviable Balance" DataFormatString="{0:#,###}" />
                <asp:BoundField DataField="PendingTransfer" HeaderText="Pending Transfer" DataFormatString="{0:#,###}" />
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationA dc_paginationA07" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView></td>
      </tr>
       
        <tr>
          <td align="center" valign="top">
              &nbsp;</td>
      </tr>
       
        <tr>
          <td align="center" valign="top">
              &nbsp;</td>
      </tr>
       
    </table>

                 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

    </section>
</asp:Content>
