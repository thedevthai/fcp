﻿Imports System.Data
Imports System.Data.SqlClient
Public Class FormList_HomeVisit
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlType As New ServiceTypeController
    Dim objUser As New UserController
    Dim ctlMTM As New MTMController
    Dim svTypeID As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then

            lblNo.Visible = False
            lblNo2.Visible = False
            ddlYear.SelectedValue = DisplayYear(Today.Date)

            LoadProvinceToDDL()

            LoadActivityTypeToDDL()

            grdData.PageIndex = 0
            grdDataPaymented.PageIndex = 0

            LoadActivityListToGrid()
            LoadActivityPaymentListToGrid()

            If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
                grdData.Columns(2).Visible = False
                grdDataPaymented.Columns(2).Visible = False


                lblProv.Visible = False
                ddlProvinceID.Visible = False

            Else
                grdData.Columns(2).Visible = True
                grdDataPaymented.Columns(2).Visible = True


                lblProv.Visible = True
                ddlProvinceID.Visible = True

            End If
            SumCount()
        End If
    End Sub
    Protected Function DateText(ByVal input As Date) As String
        Dim dStr As String
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/0544", "", input.ToString("dd/MM/yyyy"))
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/0001", "", input.ToString("dd/MM/yyyy"))
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/0544", "", input.ToString("dd/MM/yyyy"))
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/2443", "", input.ToString("dd/MM/yyyy"))
        Return dStr
    End Function
    Private Sub LoadProvinceToDDL()
        Dim ctlOrder As New OrderController
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            dt = ctlOrder.Province_GetInGroup(Request.Cookies("RPTGRP").Value)
        Else
            dt = ctlOrder.Province_GetInLocation
        End If


        ddlProvinceID.Items.Clear()
        If dt.Rows.Count > 0 Then
            With ddlProvinceID
                .Enabled = True
                .Items.Add("---ทั้งหมด---")
                If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
                    .Items(0).Value = Request.Cookies("RPTGRP").Value
                Else
                    .Items(0).Value = "0"
                End If

                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(String.Concat(dt.Rows(i)("ProvinceName")))
                    .Items(i + 1).Value =String.Concat( dt.Rows(i)("ProvinceID"))
                Next
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadActivityListToGrid()

        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            dt = ctlMTM.MTM_Master_GetByStatus(Request.Cookies("LocationID").Value, ddlForm.SelectedValue, Trim(txtSearch.Text), 1, "0", CInt(ddlYear.SelectedValue))
        ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            dt = ctlMTM.MTM_Master_GetByProvinceGroup(Request.Cookies("RPTGRP").Value, ddlForm.SelectedValue, Trim(txtSearch.Text), 1, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
            'ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isProjectManager Then
            '    dt = ctlMTM.MTM_Master_GetByProjectID(Request.Cookies("PRJMNG").Value, ddlForm.SelectedValue, Trim(txtSearch.Text), 1, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
        Else
            dt = ctlMTM.MTM_Master_GetByStatus("", ddlForm.SelectedValue, Trim(txtSearch.Text), 1, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
        End If

        If dt.Rows.Count > 0 Then
            lblCountForm.Text = dt.Rows.Count
            lblNo.Visible = False
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

            End With
        Else
            lblCountForm.Text = 0
            lblNo.Visible = True
            grdData.Visible = False
            grdData.DataSource = Nothing
        End If

        dt = Nothing
    End Sub

    Private Sub LoadActivityPaymentListToGrid()

        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            dt = ctlMTM.MTM_Master_GetByStatus(Request.Cookies("LocationID").Value, ddlForm.SelectedValue, Trim(txtSearch.Text), 3, "0", CInt(ddlYear.SelectedValue))
        ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            dt = ctlMTM.MTM_Master_GetByProvinceGroup(Request.Cookies("RPTGRP").Value, ddlForm.SelectedValue, Trim(txtSearch.Text), 3, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
            'ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isProjectManager Then
            '    dt = ctlMTM.MTM_Master_GetByProjectID(PROJECT_MTM, ddlForm.SelectedValue, Trim(txtSearch.Text), 3, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
        Else
            dt = ctlMTM.MTM_Master_GetByStatus("", ddlForm.SelectedValue, Trim(txtSearch.Text), 3, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
        End If


        If dt.Rows.Count > 0 Then
            lblPayCount.Text = dt.Rows.Count
            lblNo2.Visible = False
            With grdDataPaymented
                .Visible = True
                .DataSource = dt
                .DataBind()


            End With
        Else
            lblPayCount.Text = 0
            lblNo2.Visible = True
            grdDataPaymented.Visible = False
            grdDataPaymented.DataSource = Nothing
        End If

        dt = Nothing
    End Sub

    Private Sub LoadActivityTypeToDDL()

        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            dt = ctlType.ServiceType_GetByLocationID(Request.Cookies("LocationID").Value, PROJECT_MTM)
        ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isProjectManager Then
            dt = ctlType.ServiceType_GetByProjectID(PROJECT_MTM)
        Else
            dt = ctlType.ServiceType_GetByProjectID(PROJECT_MTM)
        End If

        If dt.Rows.Count > 0 Then
            Panel2.Visible = True


            ddlForm.Items.Clear()
            ddlForm.Items.Add("---ทั้งหมด---")
            ddlForm.Items(0).Value = "0"
            For i = 0 To dt.Rows.Count - 1
                With ddlForm
                    .Items.Add("" & dt.Rows(i)("ServiceTypeID") & " : " & dt.Rows(i)("ServiceName"))
                    .Items(i + 1).Value = dt.Rows(i)("ServiceTypeID")
                End With
            Next
        Else
            Panel2.Visible = False
        End If
        dt = Nothing
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadActivityListToGrid()
    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand

        Dim sArg(2), sStr() As String
        Dim iSID As Long
        Dim iPID As Integer
        sArg(0) = ""
        sArg(1) = ""
        sArg(2) = ""
        sStr = Split(DBNull2Str(e.CommandArgument), "|")
        For i = 0 To sStr.Length - 1
            sArg(i) = sStr(i)
        Next
        iSID = StrNull2Long(sArg(0))
        iPID = StrNull2Zero(sArg(1))
        Session("patientid") = ctlMTM.MTM_Master_GetPatientIDByItemID(iSID)

        If TypeOf e.CommandSource Is WebControls.LinkButton Then
            Dim ButtonPressed As WebControls.LinkButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "lnkAdd1"
                    If sStr(2) = FORM_TYPE_ID_MTM Then
                        Response.Redirect("MTM.aspx?t=new&fid=" & iSID)
                    Else
                        Response.Redirect("H01.aspx?t=new&fid=" & iSID)
                    End If


            End Select
        End If
        svTypeID = sArg(2) 'ctlMTM.MTM_Master_GetTypeIDByItemID(iSID, iPID)
        'svSeqNo = ctlOrder.Order_GetSEQByItemID(isid)

        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"

                    Select Case svTypeID
                        Case FORM_TYPE_ID_MTM
                            Response.Redirect("MTM.aspx?t=edit&fid=" & iSID)
                        Case FORM_TYPE_ID_H01
                            Response.Redirect("H01.aspx?t=edit&fid=" & iSID)
                        Case Else
                            Response.Redirect("" & svTypeID & ".aspx?t=edit&fid=" & iSID)
                    End Select

                Case "imgDel"
                    Select Case svTypeID
                        Case FORM_TYPE_ID_MTM
                            ctlMTM.MTM_Master_Delete(iSID)
                        Case FORM_TYPE_ID_H01
                            Dim ctlH As New HomeVisitController
                            ctlH.H01_Delete(iSID)
                    End Select

                    objUser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_DEL, svTypeID, "ลบรายการกิจกรรม :" & svTypeID, iSID)
                    'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ลบข้อมูลเรียบร้อย');", True)
                    DisplayMessage(Me.Page, "ลบข้อมูลเรียบร้อย")
                    grdData.PageIndex = 0
                    LoadActivityListToGrid()

            End Select
        End If

    End Sub

    Private Sub grdData_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            'e.Row.Cells(0).Text = e.Row.RowIndex + 1
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(9).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub
    Private Sub SumCount()
        lblCount.Text = StrNull2Zero(lblCountForm.Text) + StrNull2Zero(lblPayCount.Text)
    End Sub

    Protected Sub ddlActivity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlForm.SelectedIndexChanged
        grdData.PageIndex = 0
        LoadActivityListToGrid()

        grdDataPaymented.PageIndex = 0
        LoadActivityPaymentListToGrid()

        SumCount()

    End Sub



    Private Sub grdDataStep2_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdDataPaymented.PageIndexChanging
        grdDataPaymented.PageIndex = e.NewPageIndex
        LoadActivityPaymentListToGrid()
    End Sub
    Private Sub grdDataPaymented_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdDataPaymented.RowCommand

        Dim sArg(2), sStr() As String
        Dim iSID As Long
        Dim iPID As Integer
        sArg(0) = ""
        sArg(1) = ""
        sArg(2) = ""
        sStr = Split(DBNull2Str(e.CommandArgument), "|")
        For i = 0 To sStr.Length - 1
            sArg(i) = sStr(i)
        Next
        iSID = StrNull2Long(sArg(0))
        iPID = StrNull2Zero(sArg(1))
        Session("patientid") = ctlMTM.MTM_Master_GetPatientIDByItemID(iSID)

        If TypeOf e.CommandSource Is WebControls.LinkButton Then
            Dim ButtonPressed As WebControls.LinkButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "lnkAdd2"
                    If sStr(2) = FORM_TYPE_ID_MTM Then
                        Response.Redirect("MTM.aspx?t=new&fid=" & iSID)
                    Else
                        Response.Redirect("H01.aspx?t=new&fid=" & iSID)
                    End If
            End Select
        End If

        svTypeID = sArg(2)


        'svTypeID = ctlMTM.MTM_Master_GetTypeIDByItemID(iSID, iPID)
        ' svSeqNo = ctlOrder.Order_GetSEQByItemID(isid)


        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit2"
                    Select Case svTypeID
                        Case FORM_TYPE_ID_MTM
                            Response.Redirect("MTM.aspx?t=edit&fid=" & iSID)
                        Case FORM_TYPE_ID_H01
                            Response.Redirect("H01.aspx?t=edit&fid=" & iSID)
                        Case Else
                            Response.Redirect("" & svTypeID & ".aspx?t=edit&fid=" & iSID)
                    End Select

                Case "imgDel2"

                    Select Case svTypeID
                        Case FORM_TYPE_ID_MTM
                            ctlMTM.MTM_Master_Delete(iSID)
                        Case FORM_TYPE_ID_H01
                            Dim ctlH As New HomeVisitController
                            ctlH.H01_Delete(iSID)
                    End Select

                    objUser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_DEL, svTypeID, "ลบรายการกิจกรรม :" & svTypeID, iSID)
                    'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ลบข้อมูลเรียบร้อย');", True)
                    DisplayMessage(Me.Page, "ลบข้อมูลเรียบร้อย")
                    grdDataPaymented.PageIndex = 0
                    LoadActivityPaymentListToGrid()

            End Select
        End If
    End Sub
    Private Sub grdDataStep2_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdDataPaymented.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            'e.Row.Cells(0).Text = e.Row.RowIndex + 1
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(9).FindControl("imgDel2")
            imgD.Attributes.Add("onClick", scriptString)

        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Protected Sub ddlProvinceID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProvinceID.SelectedIndexChanged
        grdData.PageIndex = 0
        LoadActivityListToGrid()

        grdDataPaymented.PageIndex = 0
        LoadActivityPaymentListToGrid()

        SumCount()
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        grdData.PageIndex = 0
        LoadActivityListToGrid()

        grdDataPaymented.PageIndex = 0
        LoadActivityPaymentListToGrid()

        SumCount()
    End Sub

    Protected Sub cmdSearch_Click(sender As Object, e As EventArgs) Handles cmdSearch.Click
        grdData.PageIndex = 0
        LoadActivityListToGrid()

        grdDataPaymented.PageIndex = 0
        LoadActivityPaymentListToGrid()

        SumCount()
    End Sub
End Class

