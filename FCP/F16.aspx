﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="F16.aspx.vb" Inherits=".F16" %>
<%@ Register src="ucHistoryYear.ascx" tagname="ucHistoryYear" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/pagestyles.css">
<link rel="stylesheet" type="text/css" href="css/rajchasistyles.css">
<link rel="stylesheet" type="text/css" href="css/uidialog.css">
<link rel="stylesheet" type="text/css" href="css/dc_table.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>F16
        <small>กิจกรรมการค้นหาปัญหาจากการใช้ยา ( DRP )</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">     
       <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลกิจกรรม</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
   <td height="10"> <uc1:ucHistoryYear ID="ucHistoryYear1" runat="server" /></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="OptionPanels">
      <tr>
        <td width="100" class="texttopic">รหัสร้าน</td>
        <td width="120" class="LocationName">
            <asp:Label ID="lblLocationID" runat="server"></asp:Label>          </td>
        <td width="50" class="texttopic">ชื่อร้าน</td>
        <td width="300" class="LocationName">
            <asp:Label ID="lblLocationName" runat="server"></asp:Label>          </td>
        <td width="50" class="texttopic">จังหวัด</td>
        <td class="LocationName">
            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
   <tr>
    <td align="center"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">
      <tr>
        <td width="100">วันที่ให้บริการ</td>
        <td>
            <asp:TextBox ID="txtServiceDate" runat="server"></asp:TextBox>
(วว/ดด/ปปปป พ.ศ.) 
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="txtServiceDate" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="\d{2}/\d{2}/\d{4}"></asp:RegularExpressionValidator></td>
        <td width="100">รวมระยะเวลา<img src="images/star.png" width="10" height="10" /></td>
        <td>
            <asp:TextBox ID="txtTime" runat="server" Width="50px"></asp:TextBox>
&nbsp;นาที</td>
        <td>เภสัชกรผู้ให้บริการ</td>
        <td>
            <asp:DropDownList ID="ddlPerson" runat="server">            </asp:DropDownList>          </td>
      </tr>
    </table></td>
  </tr>
 
   <tr>
    <td>
      <table width="100%" border="0" cellspacing="2" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
          <td width="100" align="right"><span class="NameEN">Ref.ID :</span></td>
          <td width="50"><span class="NameEN">
              <asp:Label ID="lblID" 
                  runat="server"></asp:Label></span>            </td>
        </tr>
      </table></td>
  </tr>
 <tr>
    <td align="center" class="MenuSt">โรคประจำตัว</td>
  </tr>
   <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0">
     
      <tr>
        <td>
            <table cellpadding="0">
                <tr>
                    <td><asp:CheckBox ID="chkDisease1" runat="server" Text="เบาหวาน" 
                            AutoPostBack="True" />
                    </td> 
                    <td><asp:CheckBox ID="chkDisease2" runat="server" Text="ความดันโลหิตสูง" /></td>
                    <td><asp:CheckBox ID="chkDisease3" runat="server" Text="ไขมันในหลอดเลือดผิดปกติ" /></td>
                    <td><asp:CheckBox ID="chkDisease4" runat="server" Text="หัวใจ" /></td>
                     <td><asp:CheckBox ID="chkDisease8" runat="server" Text="ไม่มี" /></td>
                     <td><asp:CheckBox ID="chkDisease9" runat="server" Text="ไม่ทราบ" /></td>
                     <td><asp:CheckBox ID="chkDisease10" runat="server" Text="อื่นๆ" AutoPostBack="True" /></td>
                      <td> 
            <asp:TextBox ID="txtDiseaseOther" runat="server" AutoPostBack="True" Visible="False" Width="160px"></asp:TextBox>
                    </td>
                </tr>
            </table>
          </td>
      </tr>
  <tr>
    <td align="center" class="MenuSt">ข้อมูลการให้ความรู้และคำแนะนำปรึกษา</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="dc_table_s3">
      <thead>
        <tr>
          <th scope="col" width="50%" valign="top" class="texttopic">ปัญหาของผู้รับบริการ</th>
          <th scope="col" colspan="3" valign="top">ข้อมูลการให้ความรู้และคำแนะนำปรึกษา</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th valign="top" class="texttopic" align="left">
          	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="50%">1.Appropriate drug <br />
          	  <asp:CheckBox ID="chkProblem1" runat="server" Font-Bold="False" Text="ไม่ได้รับยาที่ควรได้รับ" />    
             
              <br />
              <asp:CheckBox ID="chkProblem2" runat="server" Font-Bold="False" Text="ได้รับยาที่ไม่มีข้อบ่งใช้" />      
              <br />
              <asp:CheckBox ID="chkProblem3" runat="server" Font-Bold="False" Text="ได้รับยาไม่เหมาะสม" />      
              <br />
              2.Efficacy
              <br />
              <asp:CheckBox ID="chkProblem4" runat="server" Font-Bold="False" Text="ขนาดยาน้อยเกินไป" />      
              <br />
              <asp:CheckBox ID="chkProblem5" runat="server" Font-Bold="False" Text="Drug interaction" />      
              <br />
              <asp:CheckBox ID="chkProblem6" runat="server" Font-Bold="False" Text="ผู้ป่วยไม่ได้ใช้ยาตามสั่ง" />    
               <br />
               3.Safety <br />
              <asp:CheckBox ID="chkProblem7" runat="server" Font-Bold="False" Text="ขนาดยามากเกินไป" />    
               <br />
              <asp:CheckBox ID="chkProblem8" runat="server" Font-Bold="False" Text="Drug interaction" />    
               <br />
              <asp:CheckBox ID="chkProblem9" runat="server" Font-Bold="False" Text="ADR (ประเมินใน Naranjo's Algorithm)" />              </td>
                <td valign="top">4.Accessibility <br /><asp:CheckBox ID="chkProblem10" 
                        runat="server" Font-Bold="False" Text="ผู้ป่วยไม่ได้รับยา" />    <br />
                5.Continuity <br />
                <asp:CheckBox ID="chkProblem11" runat="server" Font-Bold="False" 
                        Text="ผู้ป่วยไม่ได้รับยาอย่างต่อเนื่อง" />
                <br />
                6.Seamless care <br />
                <asp:CheckBox ID="chkProblem12" runat="server" Font-Bold="False" 
                        Text="ผู้ป่วยได้รับยาซ้ำซ้อน(ยาคนละชนิด รักษาอาการเดียวกัน)" />  
            <br />
            <asp:CheckBox ID="chkProblem13" runat="server" Font-Bold="False" 
                        Text="ผู้ป่วยได้รับยาที่เกิดปฏิกิริยาระหว่างยา" />
            <br />
            
            <asp:CheckBox ID="chkProblem14" runat="server" Font-Bold="False" 
                        Text="ผู้ป่วยได้รับยาชนิดเดียวกัน แต่รูปแบบหรือลักษณะเม็ดยาแตกต่างกัน ทำให้ได้รับยาเกินขนาดเพราะเข้าใจว่าเป็นยาคนละชนิดกัน" />
            <br />
              <asp:CheckBox ID="chkProblem15" runat="server" Font-Bold="False" 
                        Text="ผู้ป่วยได้รับยาไม่ครบจากที่เคยได้รับ" />      
            &nbsp;</td>
              </tr>
            </table>   	          </th>
          <th colspan="3" valign="top" align="left">
          	<asp:CheckBox ID="chkEdu1" runat="server" Font-Bold="False" Text="ให้ความรู้และคำแนะนำเกี่ยวกับการใช้ยา"/>    
              <br />
              <asp:CheckBox ID="chkEdu2" runat="server" Font-Bold="False" Text="ให้ความรู้และคำแนะนำเพื่อป้องกันและวิธีป้องกันปัญหา" />      
              <br />
              <asp:CheckBox ID="chkEdu3" runat="server" Font-Bold="False" Text="ประสานงานแพทย์" />      
              <br />
              <asp:CheckBox ID="chkEdu4" runat="server" Font-Bold="False" Text="ให้หยุดใช้ยา......" />      
              <br />
            <asp:CheckBox ID="chkEdu5" runat="server" Font-Bold="False" Text="จ่ายยารักษาอาการ......"  />
            <br />
            <asp:CheckBox ID="chkEduOther" runat="server" Font-Bold="False" Text="อื่นๆ" />
            &nbsp;
            <asp:TextBox 
                 ID="txtEduOther" runat="server" Width="200px"></asp:TextBox>
            <br />          </th>
        </tr>
        <tr>
          <th valign="top" class="texttopic" align="left">ระบุปัญหาอื่นๆ   <br />    
            <asp:TextBox ID="txtProblemRemark" runat="server" Width="80%" Height="63px" 
                  TextMode="MultiLine"></asp:TextBox>
                 </th>
          <th colspan="3" valign="top" align="left">คำแนะนำอื่นๆ<br />    
            <asp:TextBox ID="txtEduRemark" runat="server" Width="80%" Height="63px" 
                  TextMode="MultiLine"></asp:TextBox></th>
        </tr>
      </tbody>
    </table></td>
  </tr>
    <tr>
    <td align="left"><table border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td width="150">ยาที่อาจจะทำให้เกิดปัญหา</td>
        <td><asp:TextBox  ID="txtProMed" runat="server" Width="400px"></asp:TextBox></td>
      </tr>
    </table></td>
  </tr>
    <tr>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td align="center" class="MenuSt">การติดตาม/นัดครั้งต่อไป</td>
  </tr>
  
  <tr>
    <td><table border="0" cellspacing="2" cellpadding="0">
     <tr>
        <td>มีการติดตามผล</td>
        <td align="left">
            
            <asp:RadioButtonList ID="optFollow" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Value="1">มี</asp:ListItem>
                <asp:ListItem Selected="True" Value="0">ไม่มี</asp:ListItem>
            </asp:RadioButtonList>
            
         </td>
        </tr>
     <tr>
        <td>วันที่นัดติดตามครั้งต่อไป</td>
        <td>
            <asp:TextBox ID="txtNextTime" runat="server" 
                Width="200px"></asp:TextBox>
         </td>
        </tr>
     <tr>
        <td>วันที่บันทึกผลการติดตาม</td>
        <td>
            <asp:TextBox ID="txtCheckDate" runat="server" Width="200px"></asp:TextBox>
         </td>
        </tr>
           
     
    </table></td>
  </tr>
    <tr>
    <td><table border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td>ผลการติดตาม</td>
        <td>
            <asp:RadioButtonList ID="optChkResult" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="0">ปกติ/หาย</asp:ListItem>
                <asp:ListItem Value="1">ไม่ปกติ  ระบุ</asp:ListItem>
            </asp:RadioButtonList>          </td>
        <td><asp:TextBox ID="txtRemark" runat="server" 
                Width="350px"></asp:TextBox></td>
      </tr>
    </table></td>
  </tr>
   
      <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="dc_table_s3">
     <thead>
      <tr>
        <th scope="col" width="50%" valign="top" class="texttopic">แจ้งสิทธิประโยช์  สปสช.</th>
      </tr>
       </thead>
      <tr>
        <td align="left">
              <table border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td><asp:CheckBox ID="chkNHSO1" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวจคัดกรองความเสี่ยงDM/HT" /></td>
                  <td><asp:CheckBox ID="chkNHSO2" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวจคัดกรองความเสี่ยงมะเร็งปากมดลูก/มะเร็งเต้านม" /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO3" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับวัคซีนเสริมสร้างภูมิคุ้มกันโรค" /></td>
                  <td><asp:CheckBox ID="chkNHSO4" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับคำแนะนำปรึกษาการวางแผนครอบครัว / คุมกำเนิด" /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO5" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการดูแล/หญิงมีครรภ์ " /></td>
                  <td><asp:CheckBox ID="chkNHSO6" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวดเอดส์ ฟรี ปีละ 2 ครั้ง " /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO7" runat="server" Font-Bold="False" Text="การแจ้งย้ายสิทธิ " /></td>
                  <td><asp:CheckBox ID="chkNHSO8" runat="server" Font-Bold="False" Text="สายด่วน 1330" /></td>
                </tr>
              </table></td>
      </tr>
    </table></td>
  </tr>  

  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="Section_Header">
      <tr>
        <td width="100">สถานะ</td>
        <td>
            <asp:CheckBox ID="chkClose" runat="server" Text="จบการทำงาน" />          &nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center">
        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="70px" />
        &nbsp;<asp:Button ID="cmdClear" runat="server" Text="ยกเลิก" CssClass="buttonCancle" Width="70px" />
        </td>
  </tr>
</table>

     



 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

        </section>
</asp:Content>