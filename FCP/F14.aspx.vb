﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Public Class F14
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlLct As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlOrder As New OrderController
    Dim ServiceDate As Long
    Public F01_DocFile, F01_sFile As String
    Private _dateDiff As Object

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then

            If Request("acttype") = "view" Then
                cmdSave.Visible = False
                cmdClear.Visible = False
            Else
                cmdSave.Visible = True
                cmdClear.Visible = True
            End If

            LoadPharmacist(Request.Cookies("LocationID").Value)
            txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)
            LoadFormData()
        End If

        'txtScreenBP1.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")
        'txtScreenBP2.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")
        'txtScreenBP3.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")

        'If HiddenField1.Value <> "" Then
        '    CalBPAVG()
        'End If
        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        'txtCardID.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")

    End Sub
    Private Sub LoadPharmacist(ByVal LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With
            End If
        End If
        dtP = Nothing
    End Sub

    Private Sub LoadLocationData(LocationID As String)
        dt = ctlLct.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblLocationID.Text = .Item("LocationID")
                lblLocationName.Text = .Item("LocationName")
                lblLocationProvince.Text = .Item("ProvinceName")
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadFormData()
        Dim pYear As Integer
        If Not Request("fid") Is Nothing Then
            dt = ctlOrder.GetOrder_ByID(StrNull2Zero(Request("fid")))
        Else
            If Not Request("yid") Is Nothing Then
                pYear = CInt(Request("yid"))
            Else
                pYear = Year(ctlLct.GET_DATE_SERVER)
                If pYear < 2500 Then
                    pYear = pYear + 543
                End If

            End If
            dt = ctlOrder.LocationOrder_ByYear(pYear, FORM_TYPE_ID_F14, Request.Cookies("LocationID").Value, Session("patientid"))
        End If
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Session("patientid") = DBNull2Lng(.Item("PatientID"))
                lblID.Text = .Item("itemID")
                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                txtTime.Text = String.Concat(.Item("ServiceTime"))

                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))

                chkStatus.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("Status")))


                txtAllergy.Text = String.Concat(.Item("MedicinceDesc"))
                txtDisease.Text = String.Concat(.Item("ProblemRemark"))
                txtMedicineUsage.Text = String.Concat(.Item("EducateRemark"))
                txtUsageDay.Text = String.Concat(.Item("EducateCount"))
                optConsult.SelectedValue = String.Concat(.Item("isEducate"))


                Dim arMed(1), sStr() As String
                arMed(0) = ""
                arMed(1) = ""
                sStr = Split(String.Concat(.Item("ProbMed1")), "|")
                For i = 0 To sStr.Length - 1
                    arMed(i) = sStr(i)
                Next
                ddlMedicineGroup1.SelectedValue = arMed(0)
                txtMed1.Text = arMed(1)

                arMed(0) = ""
                arMed(1) = ""
                sStr = Split(String.Concat(.Item("ProbMed2")), "|")
                For i = 0 To sStr.Length - 1
                    arMed(i) = sStr(i)
                Next
                ddlMedicineGroup2.SelectedValue = arMed(0)
                txtMed2.Text = arMed(1)

                arMed(0) = ""
                arMed(1) = ""
                sStr = Split(String.Concat(.Item("ProbMed3")), "|")
                For i = 0 To sStr.Length - 1
                    arMed(i) = sStr(i)
                Next
                ddlMedicineGroup3.SelectedValue = arMed(0)
                txtMed3.Text = arMed(1)

                arMed(0) = ""
                arMed(1) = ""
                sStr = Split(String.Concat(.Item("EduMed1")), "|")
                For i = 0 To sStr.Length - 1
                    arMed(i) = sStr(i)
                Next
                ddlMedicineGroup4.SelectedValue = arMed(0)
                txtMed4.Text = arMed(1)

                arMed(0) = ""
                arMed(1) = ""
                sStr = Split(String.Concat(.Item("EduMed2")), "|")
                For i = 0 To sStr.Length - 1
                    arMed(i) = sStr(i)
                Next
                ddlMedicineGroup5.SelectedValue = arMed(0)
                txtMed5.Text = arMed(1)



                'txtMed2.Text = String.Concat(.Item("ProbMed2"))
                'txtMed3.Text = String.Concat(.Item("ProbMed3"))
                'txtMed4.Text = String.Concat(.Item("EduMed1"))
                'txtMed5.Text = String.Concat(.Item("EduMed2"))

                txtFollowDate.Text = String.Concat(.Item("DateCheck"))
                optChannel.SelectedValue = String.Concat(.Item("Follow_Channel"))
                optChannelTime.SelectedValue = String.Concat(.Item("isProblemOther"))
                optFollowup.SelectedValue = String.Concat(.Item("ServicePlan"))

                optCause.SelectedValue = String.Concat(.Item("EduPro1"))
                txtCauseRemark.Text = String.Concat(.Item("EduPro2"))

                If String.Concat(.Item("ServicePlan")) = "2" Then
                    optPlace.SelectedValue = String.Concat(.Item("Hospital_Type"))
                    txtHospitalName.Text = String.Concat(.Item("HospitalName"))
                    txtMedicineFromHospital.Text = String.Concat(.Item("Remark"))
                End If

               

                chkNHSO1.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO1")))
                chkNHSO2.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO2")))
                chkNHSO3.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO3")))
                chkNHSO4.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO4")))
                chkNHSO5.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO5")))
                chkNHSO6.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO6")))
                chkNHSO7.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO7")))
                chkNHSO8.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO8")))

                If DBNull2Zero(.Item("Status")) >= 3 Then
                    cmdSave.Visible = False
                    cmdClear.Visible = False
                Else
                    If Request.Cookies("LocationID").Value <> String.Concat(.Item("LocationID")) Then
                        LoadLocationData(String.Concat(.Item("LocationID")))
                        If Request("t") = "edit" Then
                            cmdSave.Visible = True
                            cmdClear.Visible = True
                        Else
                            cmdSave.Visible = False
                            cmdClear.Visible = False
                        End If
                    Else
                        LoadLocationData(Request.Cookies("LocationID").Value)
                    End If
                End If


                ShowDiagnosisResult(String.Concat(.Item("AbNormalRemark")))


            End With
        Else
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing

    End Sub

    Protected Sub cmdClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdClear.Click
        lblID.Text = ""
        txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)
        txtTime.Text = ""
        ddlPerson.SelectedIndex = 0
        'txtFName.Text = ""
        'txtLName.Text = ""
        'optGender.SelectedIndex = 0
        'txtAges.Text = ""
        'txtCardID.Text = ""
        'txtBirthDate.Text = ""
        'txtTelephone.Text = ""
        'txtMobile.Text = ""
        'optAddressType.SelectedIndex = 0
        'txtAddress.Text = ""
        'txtRoad.Text = ""
        'txtDistrict.Text = ""
        'txtCity.Text = ""
        'ddlProvince.SelectedValue = "01"
        'optClaim.SelectedIndex = 0
        txtMed1.Text = ""
        txtMed2.Text = ""
        txtMed3.Text = ""
        txtMed4.Text = ""
        txtMed5.Text = ""
        txtHospitalName.Text = ""
        txtAllergy.Text = ""
        txtDisease.Text = ""
        txtUsageDay.Text = ""
        txtMedicineUsage.Text = ""
        txtFollowDate.Text = ""
    End Sub

    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSave.Click

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If StrNull2Zero(txtTime.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนระยะเวลาในการให้บริการก่อน');", True)
            Exit Sub
        End If

        Dim objuser As New UserController
        Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))
        'Dim BirthDate As String = ConvertStrDate2DBString(txtBirthDate.Text)
        'Dim CustName As String
        'CustName = Trim(txtFName.Text) & " " & Trim(txtLName.Text)


        Dim strP As String = ""
        Dim AbNormalRemark As String = ""

        AbNormalRemark = GetDiagnosisResult()

        Dim Med1, Med2, Med3, Med4, Med5 As String

        If Trim(txtMed1.Text) <> "" Then
            Med1 = ddlMedicineGroup1.SelectedValue & "|" & txtMed1.Text
        Else
            Med1 = ""
        End If

        If Trim(txtMed2.Text) <> "" Then
            Med2 = ddlMedicineGroup2.SelectedValue & "|" & txtMed2.Text
        Else
            Med2 = ""
        End If

        If Trim(txtMed3.Text) <> "" Then
            Med3 = ddlMedicineGroup3.SelectedValue & "|" & txtMed3.Text
        Else
            Med3 = ""
        End If

        If Trim(txtMed4.Text) <> "" Then
            Med4 = ddlMedicineGroup4.SelectedValue & "|" & txtMed4.Text
        Else
            Med4 = ""
        End If

        If Trim(txtMed5.Text) <> "" Then
            Med5 = ddlMedicineGroup5.SelectedValue & "|" & txtMed5.Text
        Else
            Med5 = ""
        End If


        If lblID.Text = "" Then 'Add new

            If ctlOrder.Order_ChkDupCustomer(FORM_TYPE_ID_F14, Session("patientid"), StrNull2Zero(Left(Str(ServiceDate), 4))) = True Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ผู้รับบริการรายนี้เคยได้รับบริการในกิจกรรมนี้แล้ว');", True)
                Exit Sub
            End If

            ctlOrder.F13_Add(lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F14, Convert2Status(chkStatus.Checked),
            txtAllergy.Text _
          , txtDisease.Text _
          , txtMedicineUsage.Text _
          , StrNull2Zero(txtUsageDay.Text) _
          , optConsult.SelectedValue _
          , Med1 _
          , Med2 _
          , Med3 _
          , Med4 _
          , Med5 _
          , 1 _
          , optChannel.SelectedValue _
          , optChannelTime.SelectedValue _
          , txtFollowDate.Text _
          , optFollowup.SelectedValue _
          , optCause.SelectedValue _
          , txtCauseRemark.Text _
          , optPlace.SelectedValue _
          , txtHospitalName.Text _
          , txtMedicineFromHospital.Text _
          , AbNormalRemark _
          , "" _
          , "" _
          , Request.Cookies("username").Value, CLng(ConvertDate2DBString(ctlLct.GET_DATE_SERVER)), Boolean2Decimal(chkNHSO1.Checked), Boolean2Decimal(chkNHSO2.Checked), Boolean2Decimal(chkNHSO3.Checked), Boolean2Decimal(chkNHSO4.Checked), Boolean2Decimal(chkNHSO5.Checked), Boolean2Decimal(chkNHSO6.Checked), Boolean2Decimal(chkNHSO7.Checked), Boolean2Decimal(chkNHSO8.Checked), Session("patientid"), Session("sex"), Session("age"))


            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Service_Order", "บันทึกเพิ่มแบบประเมินเพื่อคัดกรองการใช้ยาปฏิชีวนะโรคท้องเสียอย่างสมเหตุผล (F14):" & Session("patientname"), "F14")
        Else
            ctlOrder.F13_Update(StrNull2Zero(lblID.Text), lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F14, Convert2Status(chkStatus.Checked),
            txtAllergy.Text _
          , txtDisease.Text _
          , txtMedicineUsage.Text _
          , StrNull2Zero(txtUsageDay.Text) _
          , optConsult.SelectedValue _
          , Med1 _
          , Med2 _
          , Med3 _
          , Med4 _
          , Med5 _
          , 1 _
          , optChannel.SelectedValue _
          , optChannelTime.SelectedValue _
          , txtFollowDate.Text _
          , optFollowup.SelectedValue _
          , optCause.SelectedValue _
          , txtCauseRemark.Text _
          , optPlace.SelectedValue _
          , txtHospitalName.Text _
          , txtMedicineFromHospital.Text _
          , AbNormalRemark _
          , "" _
          , "" _
          , Request.Cookies("username").Value, CLng(ConvertDate2DBString(ctlLct.GET_DATE_SERVER)), Boolean2Decimal(chkNHSO1.Checked), Boolean2Decimal(chkNHSO2.Checked), Boolean2Decimal(chkNHSO3.Checked), Boolean2Decimal(chkNHSO4.Checked), Boolean2Decimal(chkNHSO5.Checked), Boolean2Decimal(chkNHSO6.Checked), Boolean2Decimal(chkNHSO7.Checked), Boolean2Decimal(chkNHSO8.Checked), Session("patientid"))

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Service_Order", "บันทึกแบบประเมินเพื่อคัดกรองการใช้ยาปฏิชีวนะโรคท้องเสียอย่างสมเหตุผล (F14):" & Session("patientname"), "F14")
        End If

        Response.Redirect("ResultPage.aspx?p=F14")


    End Sub

    Function GetDiagnosisResult() As String
        Dim result As String = ""

        If chkD1.Checked Then
            result &= "D1|"
        End If

        If chkD2.Checked Then
            result &= "D2|"
        End If
        If chkD3.Checked Then
            result &= "D3|"
        End If
        If chkD4.Checked Then
            result &= "D4|"
        End If


        If chkD6.Checked Then
            result &= "D6|"
        End If
        If chkD7.Checked Then
            result &= "D7|"
        End If
        If chkD8.Checked Then
            result &= "D8|"
        End If
        If chkD9.Checked Then
            result &= "D9|"
        End If

        If chkD10.Checked Then
            result &= "D10|"
        End If
        If chkD11.Checked Then
            result &= "D11|"
        End If
        If chkD12.Checked Then
            result &= "D12|"
        End If
        If chkD13.Checked Then
            result &= "D13|"
        End If
        If chkD14.Checked Then
            result &= "D14|"
        End If
        If chkD15.Checked Then
            result &= "D15|"
        End If
        If chkD16.Checked Then
            result &= "D16|"
        End If


        If chkD17.Checked Then
            result &= "D17|"
        End If
        If chkD18.Checked Then
            result &= "D18|"
        End If

        If chkD19.Checked Then
            result &= "D19|"
        End If
        If chkD20.Checked Then
            result &= "D20|"
        End If


        If chkD21.Checked Then
            result &= "D21|"
        End If
        If chkD22.Checked Then
            result &= "D22|"
        End If
        If chkD23.Checked Then
            result &= "D23|"
        End If
        If chkD24.Checked Then
            result &= "D24|"
        End If
        If chkD25.Checked Then
            result &= "D25|"
        End If

        If chkD26.Checked Then
            result &= "D26|"
        End If
        If chkD27.Checked Then
            result &= "D27|"
        End If
        If chkD28.Checked Then
            result &= "D28|"
        End If
        If chkD29.Checked Then
            result &= "D29|"
        End If
        If chkD30.Checked Then
            result &= "D30|"
        End If

        If chkD31.Checked Then
            result &= "D31|"
        End If


        If chkD32.Checked Then
            result &= "D32|"
        End If
        If chkD33.Checked Then
            result &= "D33|"
        End If
        If chkD34.Checked Then
            result &= "D34|"
        End If
        If chkD35.Checked Then
            result &= "D35|"
        End If

        If chkD36.Checked Then
            result &= "D36|"
        End If
        If chkD37.Checked Then
            result &= "D37|"
        End If

        If chkD38.Checked Then
            result &= "D38|"
        End If
        If chkD39.Checked Then
            result &= "D39|"
        End If
        If chkD40.Checked Then
            result &= "D40|"
        End If

        If chkD41.Checked Then
            result &= "D41|"
        End If
        If chkD42.Checked Then
            result &= "D42|"
        End If
        If chkD43.Checked Then
            result &= "D43|"
        End If
        If chkD44.Checked Then
            result &= "D44|"
        End If
        If chkD45.Checked Then
            result &= "D45|"
        End If

        If chkD46.Checked Then
            result &= "D46|"
        End If

        If chkS1.Checked Then
            result &= "S1|"
        End If

        If chkS2.Checked Then
            result &= "S2|"
        End If

        If chkS3.Checked Then
            result &= "S3|"
        End If

        If chkS4.Checked Then
            result &= "S4|"
        End If

        If chkS5.Checked Then
            result &= "S5|"
        End If


        If Right(result, 1) = "|" Then
            result = Left(result, Len(result) - 1)
        End If

        Return result
    End Function

    Protected Sub optFollowup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optFollowup.SelectedIndexChanged
        If optFollowup.SelectedValue = "2" Then
            optPlace.Enabled = True
            txtHospitalName.Enabled = True
        Else
            optPlace.Enabled = False
            txtHospitalName.Enabled = False
        End If
    End Sub


    Private Sub ShowDiagnosisResult(sResult As String)
        Dim pstr() As String
        pstr = Split(sResult, "|")

        For i = 0 To pstr.Length - 1

            Select Case pstr(i)
                Case "D1"
                    chkD1.Checked = True
                Case "D2"
                    chkD2.Checked = True
                Case "D3"
                    chkD3.Checked = True
                Case "D4"
                    chkD4.Checked = True
                    'Case "D5"
                    '    chkD5.Checked = True
                Case "D6"
                    chkD6.Checked = True
                Case "D7"
                    chkD7.Checked = True
                Case "D8"
                    chkD8.Checked = True
                Case "D9"
                    chkD9.Checked = True
                Case "D10"
                    chkD10.Checked = True
                Case "D11"
                    chkD11.Checked = True
                Case "D12"
                    chkD12.Checked = True
                Case "D13"
                    chkD13.Checked = True
                Case "D14"
                    chkD14.Checked = True
                Case "D15"
                    chkD15.Checked = True
                Case "D16"
                    chkD16.Checked = True
                Case "D17"
                    chkD17.Checked = True
                Case "D18"
                    chkD18.Checked = True
                Case "D19"
                    chkD19.Checked = True
                Case "D20"
                    chkD20.Checked = True
                Case "D21"
                    chkD21.Checked = True
                Case "D22"
                    chkD22.Checked = True
                Case "D23"
                    chkD23.Checked = True
                Case "D24"
                    chkD24.Checked = True
                Case "D25"
                    chkD25.Checked = True
                Case "D26"
                    chkD26.Checked = True
                Case "D27"
                    chkD27.Checked = True
                Case "D28"
                    chkD28.Checked = True
                Case "D29"
                    chkD29.Checked = True
                Case "D30"
                    chkD30.Checked = True
                Case "D31"
                    chkD31.Checked = True
                Case "D32"
                    chkD32.Checked = True
                Case "D33"
                    chkD33.Checked = True
                Case "D34"
                    chkD34.Checked = True
                Case "D35"
                    chkD35.Checked = True
                Case "D36"
                    chkD36.Checked = True
                Case "D37"
                    chkD37.Checked = True
                Case "D38"
                    chkD38.Checked = True
                Case "D39"
                    chkD39.Checked = True
                Case "D40"
                    chkD40.Checked = True
                Case "D41"
                    chkD41.Checked = True
                Case "D42"
                    chkD42.Checked = True
                Case "D43"
                    chkD43.Checked = True
                Case "D44"
                    chkD44.Checked = True
                Case "D45"
                    chkD45.Checked = True
                Case "D46"
                    chkD45.Checked = True
                Case "S1"
                    chkS1.Checked = True
                Case "S2"
                    chkS2.Checked = True
                Case "S3"
                    chkS3.Checked = True
                Case "S4"
                    chkS4.Checked = True
                Case "S5"
                    chkS5.Checked = True


            End Select

        Next

    End Sub
 
End Class