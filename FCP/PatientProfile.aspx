﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="PatientProfile.aspx.vb" Inherits=".PatientProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>ประวัติการรับบริการ
        <small>Service Profile</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">      

      <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-users"></i>

              <h3 class="box-title">ประวัติการรับบริการ</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">



          


<table width="100%" border="0" cellspacing="2" cellpadding="0">
  <tr>
    <td>
              <asp:GridView ID="grdData"  CssClass="table table-hover"
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" 
                  PageSize="15" DataKeyNames="ServiceTypeID">
          
            <columns>
                <asp:BoundField DataField="ServiceTypeID" HeaderText="ฟอร์ม">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="ServiceName" HeaderText="กิจกรรม">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="ประวัติกิจกรรม">
                    <HeaderStyle HorizontalAlign="Center" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />           
             
          </asp:GridView>
              </td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    </tr>
</table> 

                  </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    </section>
</asp:Content>
