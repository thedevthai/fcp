﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteNoAjax.Master" CodeBehind="PatientServiceList.aspx.vb" Inherits=".PatientServiceList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
     
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>รายการผู้รับบริการกิจกรรมโครงการบุหรี่
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">รายการผู้รับบริการกิจกรรมโครงการบุหรี่</li>
      </ol>
    </section>

<section class="content">  

    <asp:Panel ID="pnResult" runat="server">
              <div class="alert alert-success alert-success">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <i class="icon fa fa-check"></i>
                  <h4>Sussess!!</h4>
                 บันทึกข้อมูลการส่งต่อเรียบร้อย
              </div>       
           </asp:Panel> 


       <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-search"></i>

              <h3 class="box-title">ค้นหา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">  
                <table border="0">
<tr>
  <td align="left" class="texttopic">ปี :</td>
  <td align="left" class="texttopic">
                                                      <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" 
                                                          CssClass="OptionControl">
                                                          <asp:ListItem Selected="True" Value="0">-- ทั้งหมด --</asp:ListItem>
                                                          <asp:ListItem>2557</asp:ListItem>
                                                          <asp:ListItem>2558</asp:ListItem>
                                                          <asp:ListItem>2559</asp:ListItem>
                                                          <asp:ListItem>2560</asp:ListItem>
                                                          <asp:ListItem>2561</asp:ListItem>
                                                          <asp:ListItem>2562</asp:ListItem>
                                                          <asp:ListItem>2563</asp:ListItem>
                                                          <asp:ListItem>2564</asp:ListItem>
                                                           <asp:ListItem>2565</asp:ListItem>
                                                           <asp:ListItem>2566</asp:ListItem>
                                                      </asp:DropDownList>                                                    </td>
</tr>
<tr>
  <td align="left" class="texttopic"><asp:Label ID="lblProv" runat="server" 
          Text="จังหวัด :"></asp:Label>
    </td>
  <td align="left" class="texttopic">
                                                      <asp:DropDownList ID="ddlProvinceID" runat="server" AutoPostBack="True" 
                                                          CssClass="OptionControl">                                                      </asp:DropDownList>                                                    </td>
</tr>

<tr>
  <td align="left" class="texttopic">ค้นหา :</td>
  <td align="left" class="texttopic">
                  <asp:TextBox ID="txtSearch" runat="server" Width="250px"></asp:TextBox>                                                    </td>
</tr>

<tr>
  <td align="left" class="texttopic">&nbsp;</td>
  <td align="left" class="texttopic">
                                                      <asp:Button ID="cmdSearch" runat="server" CssClass="buttonLogin" Text="ค้นหา" Width="100px" />
    </td>
</tr>

  </table>  
 </div>
          
          </div>
     <h3>รายการกิจกรรมที่พบทั้งหมด&nbsp;<asp:Label ID="lblCount" runat="server"></asp:Label>
           &nbsp;&nbsp;รายการ</h3> 
    <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-edit"></i>

              <h3 class="box-title">รายการผู้รับบริการ และสถานะการ Refer </h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">


              <asp:GridView ID="grdData" CssClass="table table-hover"
                             runat="server" CellPadding="4" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" >
            <RowStyle BackColor="#F7F7F7" />
            <columns>
                <asp:BoundField HeaderText="No." DataField="nRow" >
                <ItemStyle HorizontalAlign="Center" Width="30px" />                </asp:BoundField>
                <asp:BoundField HeaderText="วันที่รับบริการครั้งล่าสุด" DataField="ServiceDateTXT" >
                <ItemStyle HorizontalAlign="Center" />                </asp:BoundField>
                <asp:BoundField DataField="LocationName" HeaderText="ร้านยา">
                <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="ชื่อ-สกุล">
                 <ItemTemplate>
                        <asp:HyperLink ID="Hyper2" runat="server" 
                            NavigateUrl='<%# NavigateURL("PatientProfile.aspx", "acttype", "view", "PatientID", DataBinder.Eval(Container.DataItem, "PatientID"))%>' 
                            Target="_blank" 
                            Text='<%# DataBinder.Eval(Container.DataItem, "CustName") %>' 
                            CssClass="grd_item"></asp:HyperLink>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" CssClass="grd_item" />
                </asp:TemplateField>
                <asp:BoundField DataField="Ages" HeaderText="อายุ" >
                <ItemStyle HorizontalAlign="Center" Width="30px" />                </asp:BoundField>
            <asp:BoundField HeaderText="เพศ" DataField="Gender">                      
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />                      </asp:BoundField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Image ID="imgRefer" runat="server" ImageUrl="images/refer.png"   Visible='<%# DataBinder.Eval(Container.DataItem, "isSendRefer") %>'/>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="50px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Refer">
                    <ItemTemplate>
                        <asp:Button ID="btnRefer" runat="server" CssClass="buttonPurple" Text="Refer" Visible='<%# ConvertValue(DataBinder.Eval(Container.DataItem, "isSendRefer")) %>'  CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PatientID") %>'/>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC01" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView>


 </div>
            <div class="box-footer clearfix">
                <asp:Panel ID="pnNo" runat="server">
              <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <i class="icon fa fa-ban"></i>
                  <h4>Not found!!</h4>
                   ยังไม่มีรายการกิจกรรมที่ท่านค้นหา
              </div>       
           </asp:Panel> 
            </div>
          </div>
     
    </section>
</asp:Content>
