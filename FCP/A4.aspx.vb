﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Public Class A04
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlLct As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlOrder As New SmokingController
    Dim ctlSK As New StockController
    Dim ServiceDate As Long
    Private _dateDiff As Object

    'Dim dtPOS As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Response.Redirect("CuttomErrorPage.aspx")

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Session("patientid") Is Nothing Then
            Response.Redirect("PatientSearch.aspx")
        End If

        If Not IsPostBack Then

            If ctlLct.GET_DATE_SERVER.Year < 2555 Then
                txtYear.Text = ctlLct.GET_DATE_SERVER.Year + 543
            Else
                txtYear.Text = ctlLct.GET_DATE_SERVER.Year
            End If

            LoadMedicine()
            'txtSmokeQTY.Visible = False
            'lblCGday.Visible = False
            'txtSmokeDay.Visible = False
            'lblCgType.Visible = False
            'optCigarette.Visible = False
            lblProblem.Visible = False
            optNextCause.Visible = False
            txtCause.Visible = False

            'lblUnit1.Visible = True
            'lblUnit2.Visible = False
            If Request("acttype") = "view" Then
                cmdSave.Visible = False
            Else
                cmdSave.Visible = True
            End If

            'Session("dtposA4") = Nothing

            'dtPOS.Columns.Add("MedTime")
            'dtPOS.Columns.Add("itemID")
            'dtPOS.Columns.Add("itemName")
            'dtPOS.Columns.Add("QTY")
            'Session("dtposA4") = dtPOS

            LoadPharmacist(Request.Cookies("LocationID").Value)
            txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)
            LoadFormData()
            'LoadBalanceStock()
            If Request("t") = "new" Then
                txtTime.Text = ""
            End If


            lblBalance.Visible = False
            lblBalanceLabel.Visible = False

        End If


        'txtScreenBP1.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")
        'txtScreenBP2.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")
        'txtScreenBP3.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")

        'If HiddenField1.Value <> "" Then
        '    CalBPAVG()
        'End If

        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtYear.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")


    End Sub
    Private Sub LoadPharmacist(LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With
            End If
        End If
        dtP = Nothing
    End Sub

    Private Sub LoadMedicine()
        Dim dtM As New DataTable
        dtM = ctlOrder.Medicine_Get
        If dtM.Rows.Count > 0 Then
            ddlMed.DataSource = dtM
            ddlMed.DataTextField = "itemName"
            ddlMed.DataValueField = "itemID"
            ddlMed.DataBind()
        End If
        dtM = Nothing
    End Sub
    Private Sub LoadLocationData(LocationID As String)
        dt = ctlLct.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblLocationID.Text = .Item("LocationID")
                lblLocationName.Text = .Item("LocationName")
                lblLocationProvince.Text = .Item("ProvinceName")
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadFormData()
        Dim pYear As Integer


        If Not Request("fid") Is Nothing Then
            dt = ctlOrder.CPA_SmokingOrder_ByID(StrNull2Zero(Request("fid")))
        Else
            If Not Request("yid") Is Nothing Then
                pYear = CInt(Request("yid"))
            Else
                pYear = Year(ctlLct.GET_DATE_SERVER)
                If pYear < 2500 Then
                    pYear = pYear + 543
                End If

            End If
            dt = ctlOrder.CPA_Smoking_OrderByYear(pYear, FORM_TYPE_ID_A4F, Session("patientid"))
        End If

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                If Request("t") = "new" Then
                    lblID.Text = ""
                    lblRefID.Text = String.Concat(.Item("ServiceUID"))
                ElseIf Request("t") = "edit" Then
                    lblID.Text = String.Concat(.Item("ServiceUID"))
                    lblRefID.Text = String.Concat(.Item("RefID"))
                    txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                ElseIf Request("t") Is Nothing Then
                    lblID.Text = ""
                    lblRefID.Text = String.Concat(.Item("ServiceUID"))
                End If
                Session("patientid") = DBNull2Lng(.Item("PatientID"))
                txtYear.Text = String.Concat(.Item("BYear"))

                '-------------------
                optPatientFrom.SelectedValue = DBNull2Str(.Item("PFROM"))
                txtFromRemark.Text = DBNull2Str(.Item("PFROM_DESC"))

                optService.SelectedValue = DBNull2Str(.Item("MTMService"))
                txtServiceRemark.Text = DBNull2Str(.Item("ServiceRemark"))

                If DBNull2Str(.Item("MTMService")) = "2" Then
                    optTelepharm.SelectedValue = DBNull2Str(.Item("ServiceRef"))
                Else
                    optTelepharm.ClearSelection()
                End If

                optTelepharmacyMethod.SelectedValue = DBNull2Str(.Item("TelepharmacyMethod"))
                txtRecordMethod.Text = DBNull2Str(.Item("RecordMethod"))
                txtRecordLocation.Text = DBNull2Str(.Item("RecordLocation"))

                txtTelepharmacyRemark.Text = DBNull2Str(.Item("TelepharmacyRemark"))

                '---------------------------------


                optFinalResult.SelectedValue = String.Concat(.Item("FinalResult"))
                txtStillSmoke.Text = String.Concat(.Item("StillSmoke"))
                optChangeRate.SelectedValue = DBNull2Str(.Item("ChangeRate"))
                VisibleChangRate()
                If Not IsDBNull(.Item("FinalStopDate")) Then
                    dtpFinalStopDate.Text = .Item("FinalStopDate")
                End If

                optNicotinEffect.SelectedValue = String.Concat(.Item("NicotineEffect"))
                optAdverse.SelectedValue = String.Concat(.Item("MedEffect"))
                optBelieve.SelectedValue = String.Concat(.Item("SocietyEffect"))
                txtRecommend1.Text = String.Concat(.Item("Recommend1"))
                txtRecommend2.Text = String.Concat(.Item("Recommend2"))
                txtRecommend3.Text = String.Concat(.Item("Recommend3"))
                txtRecommend4.Text = String.Concat(.Item("Recommend4"))

                txtWeight.Text = DBNull2Str(.Item("PWeight"))
                txtHeigh.Text = DBNull2Str(.Item("PHeigh"))
                txtPEFR_Value.Text = DBNull2Str(.Item("PEFR_Value"))
                txtPEFR_Rate.Text = DBNull2Str(.Item("PEFR_Rate"))
                txtCO_Value.Text = DBNull2Str(.Item("CO_Value"))

                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))

                txtTime.Text = String.Concat(.Item("ServiceTime"))
                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))


                optStopPlane1.SelectedValue = DBNull2Str(.Item("ServicePlan"))


                optStopPlane2.SelectedValue = DBNull2Str(.Item("Guidelines"))
                If optStopPlane2.SelectedIndex = 1 Then
                    txtCGTargetRateBegin.Text = DBNull2Str(.Item("GuidelinesCGFrom"))
                    txtCGTargetRateEnd.Text = DBNull2Str(.Item("GuidelinesCGTo"))
                    txtTargetDay.Text = DBNull2Str(.Item("GuidelinesDay"))
                    If DBNull2Str(.Item("GuidelinesDate")) <> "" Then
                        xDateStopPlan2.Text = DisplayShortDateTH(.Item("GuidelinesDate"))
                    Else
                        xDateStopPlan2.Text = ""
                    End If
                End If

                If DBNull2Str(.Item("ServicePlanDate")) <> "" Then
                    xDateStopPlan1.Text = DisplayShortDateTH(.Item("ServicePlanDate"))
                Else
                    xDateStopPlan1.Text = ""
                End If

                If DBNull2Str(.Item("ServiceNextDate")) <> "" Then
                    xDateNextService.Text = DisplayShortDateTH(.Item("ServiceNextDate"))
                Else
                    xDateNextService.Text = ""
                End If


                'optNextStep.SelectedValue = DBNull2Str(.Item("ServicePlan"))
                'optNextCause.SelectedValue = DBNull2Str(.Item("ServicePlanProblem"))
                'txtCause.Text = String.Concat(.Item("ServicePlanRemark"))
                'ddlTimeService.SelectedValue = DBNull2Str(.Item("ContactTime"))

                chkStatus.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("StatusFlag")))

                'ctlOrder.MedOrder_DeleteAll(StrNull2Long(lblID.Text), FORM_TYPE_ID_A4F, Request.Cookies("LocationID").Value)
                LoadMedOrder(StrNull2Long(lblID.Text))

                If DBNull2Zero(.Item("StatusFlag")) >= 3 Then  'Year(ctlLct.GET_DATE_SERVER.Date) <> DBNull2Zero(.Item("BYear")) 
                    'cmdSave.Visible = False
                Else
                    If Request.Cookies("LocationID").Value <> String.Concat(.Item("LocationID")) Then
                        LoadLocationData(String.Concat(.Item("LocationID")))
                        If Request("t") = "edit" Then
                            cmdSave.Visible = True
                        Else
                            cmdSave.Visible = False
                        End If
                    Else
                        LoadLocationData(Request.Cookies("LocationID").Value)
                    End If
                End If


                If optNextStep.SelectedValue = "2" Then
                    lblProblem.Visible = True
                    optNextCause.Visible = True
                    txtCause.Visible = True
                Else
                    lblProblem.Visible = False
                    optNextCause.Visible = False
                    txtCause.Visible = False
                End If


                CompareDataFormA01()

            End With
        Else
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing

    End Sub

    Private Sub LoadMedOrderItem(ServiceUID As Long)
        Dim dtM2 As New DataTable
        dtM2 = ctlOrder.CPA_MedOrder_GetMedItem(FORM_TYPE_ID_A4F, lblLocationID.Text, Session("patientid"), txtYear.Text)
        If dtM2.Rows.Count > 0 Then
            lblNoMed.Visible = False
            With grdData
                .Visible = True
                .DataSource = dtM2
                .DataBind()
                'For i = 0 To .Rows.Count - 1
                '    .Rows(i).Cells(0).Text = i + 1
                'Next
            End With
            'Session("dtposA4") = dtM
        Else
            lblNoMed.Visible = True
            grdData.Visible = False
            grdData.DataSource = dtM2
            grdData.DataBind()

        End If
        dtM2 = Nothing
    End Sub

    Private Sub LoadMedOrder(ServiceUID As Long)
        Dim dtM As New DataTable

        ctlOrder.MedOrder_DeleteByStatus(FORM_TYPE_ID_A4F, lblLocationID.Text, StrNull2Long(Session("patientid")), "Q")

        dtM = ctlOrder.MedOrder_GetMedA4(ServiceUID, FORM_TYPE_ID_A4F, lblLocationID.Text, Session("patientid"), txtYear.Text)
        If dtM.Rows.Count > 0 Then
            lblNoMed.Visible = False
            With grdData
                .Visible = True
                .DataSource = dtM
                .DataBind()
            End With
            'Session("dtposA4") = dtM
        Else
            lblNoMed.Visible = True
            grdData.Visible = False
            grdData.DataSource = dtM
            grdData.DataBind()
        End If
        dtM = Nothing
    End Sub
    Private Sub CompareDataFormA01()
        dt = ctlOrder.Smoking_Order_GetByID(StrNull2Long(lblRefID.Text))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                txtWeight.Text = String.Concat(.Item("PWeight"))
                txtHeigh.Text = String.Concat(.Item("PHeigh"))
                CalPEFR_Rate()
                CalSmokingChangRate(DBNull2Zero(.Item("SmokeQTY")))

            End With
        End If
        dt = Nothing
    End Sub
    Private Sub CalSmokingChangRate(CGNumber As Integer)
        Dim nA04, nA01 As Integer
        nA01 = CGNumber
        nA04 = StrNull2Zero(txtStillSmoke.Text)

        If nA04 < nA01 Then
            optChangeRate.SelectedValue = "L"
        ElseIf nA01 = nA04 Then
            optChangeRate.SelectedValue = "E"
        Else
            optChangeRate.SelectedValue = "H"
        End If

    End Sub

    'Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click

    '    isAdd = True

    '    ctlOrder.MedOrder_DeleteStatusQ(StrNull2Long(lblID.Text), FORM_TYPE_ID_A4F, lblLocationID.Text)

    '    'For t = 0 To dtPOS.Rows.Count - 1
    '    '    dtPOS.Rows(t).Delete()
    '    'Next

    '    'Session("dtposA4") = Nothing
    '    'Session("dtposA4") = dtPOS

    '    'lblID.Text = ""
    '    dtpServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)
    '    'txtTime.Text = ""
    '    ddlPerson.SelectedIndex = 0
    '    txtWeight.Text = ""
    '    txtHeigh.Text = ""
    '    'txtAlcoholFQ.Text = ""
    '    txtCause.Text = ""
    '    txtCGTargetRateEnd.Text = ""
    '    txtTargetDay.Text = ""
    '    txtFollowRemark.Text = ""

    '    txtPEFR_Rate.Text = ""
    '    txtPEFR_Value.Text = ""
    '    txtTime.Text = ""
    '    txtStopDate.Text = ""
    '    txtRecommend1.Text = ""
    '    txtRecommend2.Text = ""
    '    txtRecommend3.Text = ""
    '    txtRecommend4.Text = ""
    '    LoadMedOrder(StrNull2Long(lblID.Text))
    'End Sub

    Protected Sub txtHeigh_TextChanged(sender As Object, e As EventArgs) Handles txtHeigh.TextChanged
        CalPEFR_Rate()
    End Sub
    Private Sub CalPEFR_Rate()
        Dim PEF, PEFR_Standard As Double

        Dim A As Integer = StrNull2Zero(Session("age"))
        Dim H As Double = StrNull2Zero(txtHeigh.Text)
        If H > 0 Then
            If StrNull2Zero(Session("age")) >= 15 Then
                If Session("sex") = "M" Then
                    PEF = (((((-16.859 + (0.307 * A)) + (0.141 * H)) - (0.0018 * (A * A))) - (0.001 * A * H)) * 60)
                Else
                    PEF = ((((((-31.355 + (0.162 * A)) + (0.391 * H)) - (0.00084 * (A * A))) - (0.00099 * (H * H))) - (0.00072 * A * H)) * 60)
                End If
            Else
                PEF = (StrNull2Zero(txtHeigh.Text) * 5) - 400
            End If

            PEFR_Standard = Math.Round(PEF)
            If StrNull2Zero(txtPEFR_Value.Text) <> 0 Then
                txtPEFR_Rate.Text = Math.Round((StrNull2Zero(txtPEFR_Value.Text) / PEFR_Standard) * 100)
            Else
                txtPEFR_Rate.Text = "0"
            End If

            'If Not IsNumeric(txtPEFR_Rate.Text) Then
            '    txtPEFR_Rate.Text = "0"
            'End If
        End If
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        'If Trim(txtFName.Text) = "" Or Trim(txtLName.Text) = "" Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาป้อนชื่อ-นามสกุลผู้รับบริการก่อน');", True)
        '    Exit Sub
        'End If
        'If Trim(txtMobile.Text) = "" Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาป้อนเบอร์มือถือผู้รับบริการก่อน หากไม่มีให้ใส่เบอร์บ้านในช่องเบอร์มือถือได้")
        '    Exit Sub
        'End If
        If Trim(txtTime.Text) <> "" Then
            If IsNumeric(txtTime.Text) = False Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาตรวจสอบระยะเวลาในการให้บริการ ควรป้อนเฉพาะตัวเลขเท่านั้น หากไม่มีข้อมูลให้ใส่ 0');", True)
                Exit Sub
            End If
        End If

        If optFinalResult.SelectedIndex = 0 Then
            If dtpFinalStopDate.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาป้อนวันที่หยุดสูบได้');", True)
                Exit Sub
            End If
        End If

        'If StrNull2Zero(txtAges.Text) = 0 Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาป้อนอายุผู้รับบริการก่อน');", True)
        '    Exit Sub
        'End If


        '--------------------------------
        If optPatientFrom.SelectedIndex = -1 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทแหล่งที่มาก่อน');", True)
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทแหล่งที่มาก่อน ")
            Exit Sub
        End If

        If optPatientFrom.SelectedValue = "9" And txtFromRemark.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุประเภทแหล่งที่มาก่อน');", True)
            Exit Sub
        End If


        If optService.SelectedIndex = -1 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกการให้บริการมาก่อน');", True)
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกการให้บริการมาก่อน ")
            Exit Sub
        End If


        If optService.SelectedIndex = 1 Then
            If optTelepharm.SelectedIndex = -1 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทการให้บริการ Telepharmacy ก่อน ');", True)
                Exit Sub
            Else
                If optTelepharm.SelectedValue <> "1" Then
                    If txtTelepharmacyRemark.Text.Trim() = "" Then
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุชื่อ รพ. หรือ ชื่อโครงการอื่นๆ กรณี Telepharmacy ก่อน');", True)
                        Exit Sub
                    End If
                End If
            End If
        End If




        '-----------------------------------------------------
        If optPatientFrom.SelectedValue = "9" Then
            If txtFromRemark.Text.Trim() = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุแหล่งที่มาก่อน');", True)
                Exit Sub
            End If

        End If

        If optService.SelectedValue = "9" Then
            If txtServiceRemark.Text.Trim() = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุการให้บริการก่อน');", True)
                Exit Sub
            End If

        End If

        If optService.SelectedIndex = 1 Then 'case Telepharmacy
            If optTelepharm.SelectedIndex = -1 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทการให้บริการ Telepharmacy ก่อน');", True)
                Exit Sub
            End If

            If optTelepharmacyMethod.SelectedIndex = -1 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกวิธีการให้บริการ Telepharmacy ก่อน');", True)
                Exit Sub
            End If

            If optTelepharmacyMethod.SelectedIndex <> 0 Then 'case Telepharmacy
                If txtRecordMethod.Text.Trim() = "" Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุวิธีการบันทึก');", True)
                    Exit Sub
                End If
                If txtRecordLocation.Text.Trim() = "" Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุสถานที่เก็บข้อมูล');", True)
                    Exit Sub
                End If
            End If
        End If

        '------------------------------------------------------


        Dim objuser As New UserController
        Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))

        ''Dim BirthDate As String = ConvertStrDate2DBString(txtBirthDate.Text)

        Dim dStopPlan1, dStopPlan2, dNextService, dFinalStopDate As String
        dStopPlan1 = ConvertStrDate2InformDateString(xDateStopPlan1.Text)
        dStopPlan2 = ConvertStrDate2InformDateString(xDateStopPlan2.Text)
        dNextService = ConvertStrDate2InformDateString(xDateNextService.Text)
        dFinalStopDate = ConvertStrDate2InformDateString(dtpFinalStopDate.Text)

        Dim CGTargetRateBegin, CGTargetRateEnd, TargetDay As Integer

        If optStopPlane2.SelectedIndex = 1 Then
            CGTargetRateBegin = StrNull2Zero(txtCGTargetRateBegin.Text)
            CGTargetRateEnd = StrNull2Zero(txtCGTargetRateEnd.Text)
            TargetDay = StrNull2Zero(txtTargetDay.Text)
        Else
            CGTargetRateBegin = 0
            CGTargetRateEnd = 0
            TargetDay = 0
        End If



        If lblID.Text = "" Then 'Add new

            If ctlOrder.Smoking_ChkDupCustomer(FORM_TYPE_ID_A4F, Session("patientid"), StrNull2Zero(txtYear.Text)) = True Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','ผู้รับบริการรายนี้เคยได้รับบริการในกิจกรรมนี้แล้ว');", True)
                Exit Sub
            End If

            ctlOrder.CPA_A04_Add(StrNull2Long(lblRefID.Text), StrNull2Zero(txtYear.Text), FORM_TYPE_ID_A4F,
                                 ServiceDate,
                                 Str2Double(txtTime.Text),
                                 lblLocationID.Text,
                                 Session("patientid"),
                                 StrNull2Zero(ddlPerson.SelectedValue),
                                 Str2Double(txtWeight.Text),
                                 Str2Double(txtHeigh.Text),
                                 Str2Double(txtPEFR_Value.Text),
                                 Str2Double(txtPEFR_Rate.Text),
                                 Str2Double(txtCO_Value.Text),
                                 optFinalResult.SelectedValue,
                                 StrNull2Zero(txtStillSmoke.Text),
                                 dFinalStopDate,
                                 optChangeRate.SelectedValue,
                                 optNicotinEffect.SelectedValue,
                                 txtRecommend1.Text,
                                 optAdverse.SelectedValue,
                                 txtRecommend2.Text,
                                 optBelieve.SelectedValue,
                                 txtRecommend3.Text,
                                 txtRecommend4.Text,
                               optStopPlane1.SelectedValue, dStopPlan1,
              StrNull2Zero(optStopPlane2.SelectedValue),
             CGTargetRateBegin, CGTargetRateEnd, TargetDay, dStopPlan2,
              dNextService, Convert2Status(chkStatus.Checked), Request.Cookies("username").Value, optPatientFrom.SelectedValue, txtFromRemark.Text, optService.SelectedValue, txtServiceRemark.Text, optTelepharm.SelectedValue, optTelepharmacyMethod.SelectedValue, txtRecordMethod.Text, txtRecordLocation.Text, txtTelepharmacyRemark.Text)

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Smoking", "บันทึกเพิ่มกิจกรรมปรับเปลี่ยนพฤติกรรมผู้เสพยาสูบโดยเภสัชกรชุมชน (A04):<<PatientID:" & Session("patientid") & ">>", "A4")

        Else

            ctlOrder.CPA_A04_Update(StrNull2Long(lblID.Text),
                                    StrNull2Zero(txtYear.Text),
                                 FORM_TYPE_ID_A4F,
                                 ServiceDate,
                                 Str2Double(txtTime.Text),
                                 lblLocationID.Text,
                                 Session("patientid"),
                                 StrNull2Zero(ddlPerson.SelectedValue),
                                 Str2Double(txtWeight.Text),
                                 Str2Double(txtHeigh.Text),
                                 Str2Double(txtPEFR_Value.Text),
                                 Str2Double(txtPEFR_Rate.Text),
                                 Str2Double(txtCO_Value.Text),
                                 optFinalResult.SelectedValue,
                                 StrNull2Zero(txtStillSmoke.Text),
                                 dFinalStopDate,
                                 optChangeRate.SelectedValue,
                                 optNicotinEffect.SelectedValue,
                                 txtRecommend1.Text,
                                 optAdverse.SelectedValue,
                                 txtRecommend2.Text,
                                 optBelieve.SelectedValue,
                                 txtRecommend3.Text,
                                 txtRecommend4.Text,
                               optStopPlane1.SelectedValue, dStopPlan1,
              StrNull2Zero(optStopPlane2.SelectedValue), CGTargetRateBegin, CGTargetRateEnd, TargetDay, dStopPlan2,
             dNextService, Convert2Status(chkStatus.Checked), Request.Cookies("username").Value, optPatientFrom.SelectedValue, txtFromRemark.Text, optService.SelectedValue, txtServiceRemark.Text, optTelepharm.SelectedValue, optTelepharmacyMethod.SelectedValue, txtRecordMethod.Text, txtRecordLocation.Text, txtTelepharmacyRemark.Text)

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Smoking", "บันทึกเพิ่มกิจกรรมปรับเปลี่ยนพฤติกรรมผู้เสพยาสูบโดยเภสัชกรชุมชน (A4) :<<PatientID:" & Session("patientid") & ">>", "A4")
        End If

        ctlOrder.CPA_MedOrder_DeleteByStatus(FORM_TYPE_ID_A4F, lblLocationID.Text, Session("patientid"), "D")

        Dim ServiceID As Integer = ctlOrder.Smoking_Order_GetID(StrNull2Zero(txtYear.Text), lblLocationID.Text, FORM_TYPE_ID_A4F, Session("patientid"), 0)


        If grdData.Rows.Count > 0 Then
            Dim UOM As String = ""
            Dim BALANCE As Integer = 0
            Dim ctlSK As New StockController
            For i = 0 To grdData.Rows.Count - 1

                UOM = ctlOrder.Medicine_GetUOM(StrNull2Zero(grdData.DataKeys(i).Value))

                BALANCE = ctlSK.Stock_GetBalance(StrNull2Zero(grdData.DataKeys(i).Value), lblLocationID.Text)
                ctlOrder.CPA_MedOrder_Add(0, StrNull2Zero(grdData.DataKeys(i).Value), grdData.Rows(i).Cells(1).Text, grdData.Rows(i).Cells(3).Text, StrNull2Zero(grdData.Rows(i).Cells(2).Text), Request.Cookies("username").Value, FORM_TYPE_ID_A4F, lblLocationID.Text, "A", StrNull2Long(Session("patientid")), StrNull2Zero(txtYear.Text))

                ctlSK.Stock_Inventory_ISS(StrNull2Zero(grdData.DataKeys(i).Value), StrNull2Zero(grdData.Rows(i).Cells(2).Text), Request.Cookies("username").Value, FORM_TYPE_ID_A4F, lblLocationID.Text, "A", UOM, BALANCE, "ISSPTN", ServiceID, ParseDateToSQL(txtServiceDate.Text, "/"))
            Next
        End If

        ctlOrder.A01_UpdateA04Count(StrNull2Zero(lblRefID.Text), 1)
        ctlOrder.A01_UpdateCloseStatus(StrNull2Zero(lblRefID.Text), Convert2Status(chkStatus.Checked), Request.Cookies("username").Value)

        Response.Redirect("ResultPage.aspx?xa=4")
    End Sub

    Private Sub txtPEFR_Value_TextChanged(sender As Object, e As EventArgs) Handles txtPEFR_Value.TextChanged
        CalPEFR_Rate()
    End Sub

    Protected Sub optNextStep_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optNextStep.SelectedIndexChanged
        If optNextStep.SelectedValue = "2" Then
            lblProblem.Visible = True
            optNextCause.Visible = True
            txtCause.Visible = True
        Else
            lblProblem.Visible = False
            optNextCause.Visible = False
            txtCause.Visible = False
        End If
    End Sub

    Protected Sub cmdAddMed_Click(sender As Object, e As EventArgs) Handles cmdAddMed.Click

        If ddlMed.SelectedValue = "12" Then
            If txtMed.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','ท่านยังไม่ได้ป้อนชื่อยา');", True)
                Exit Sub
            End If
            'Else
            '    If StrNull2Zero(txtMedQTY.Text) > StrNull2Zero(lblBalance.Text) Then
            '         ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','ยาในสต๊อกมีไม่พอ")
            '        Exit Sub
            '    End If

        End If

        'If StrNull2Zero(txtMedQTY.Text) = 0 Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','ท่านยังไม่ได้ป้อนจำนวนที่จ่าย")
        '    Exit Sub
        'End If

        'dtPOS = Session("dtposA4")
        'If dtPOS Is Nothing Then
        '    dtPOS.Columns.Add("MedTime")
        '    dtPOS.Columns.Add("itemID")
        '    dtPOS.Columns.Add("itemName")
        '    dtPOS.Columns.Add("QTY")
        'End If

        'Dim dr As DataRow = dtPOS.NewRow()
        'dr(0) = txtMedTime.Text
        'dr(1) = ddlMed.SelectedValue
        'dr(2) = ddlMed.SelectedItem.Text
        'dr(3) = txtMedQTY.Text
        'dtPOS.Rows.Add(dr)

        'With grdData
        '    .Visible = True
        '    .DataSource = dtPOS
        '    .DataBind()
        'End With

        'Session("dtposA4") = Nothing
        'Session("dtposA4") = dtPOS


        Dim MedName As String = ""

        If ddlMed.SelectedValue = "12" Then
            MedName = txtMed.Text
        Else
            MedName = ddlMed.SelectedItem.Text
        End If

        ctlOrder.CPA_MedOrder_Add(0, ddlMed.SelectedValue, MedName, txtFrequency.Text, StrNull2Zero(txtMedQTY.Text), Request.Cookies("username").Value, FORM_TYPE_ID_A4F, lblLocationID.Text, "Q", StrNull2Long(Session("patientid")), StrNull2Zero(txtYear.Text))

        LoadMedOrderItem(StrNull2Long(lblID.Text))

        txtMed.Text = ""
        txtMedQTY.Text = ""
    End Sub
    'Private Sub LoadBalanceStock()
    '    Dim ctlSK As New StockController
    '    lblBalance.Text = ctlSK.Stock_GetBalance(ddlMed.SelectedValue, lblLocationID.Text)
    '    lblUOM.Text = ctlSK.Stock_GetUOM(ddlMed.SelectedValue, lblLocationID.Text)
    '    lblBalance.Visible = True
    '    lblBalanceLabel.Visible = True
    'End Sub
    'Protected Sub ddlMed_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMed.SelectedIndexChanged
    '    lblUnit2.Visible = False
    '    txtMed.Visible = False

    '    'lblBalance.Visible = True
    '    'lblBalanceLabel.Visible = True

    '    'LoadBalanceStock()

    '    Select Case ddlMed.SelectedValue
    '        Case "11"
    '            lblUnit1.Visible = False
    '            lblUnit2.Visible = True
    '        Case "12"
    '            txtMed.Visible = True
    '            lblUnit1.Visible = True

    '            lblBalance.Visible = False
    '            lblBalanceLabel.Visible = False
    '    End Select
    'End Sub

    Protected Sub optFinalResult_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optFinalResult.SelectedIndexChanged
        VisibleChangRate()
    End Sub
    Private Sub VisibleChangRate()

        If optFinalResult.SelectedIndex = 0 Then
            dtpFinalStopDate.Visible = True
            lblQ5.Visible = True
            lblQ5.Text = "วันที่"
            txtStillSmoke.Text = "0"
            optChangeRate.SelectedIndex = 0
            txtStillSmoke.Visible = False
            optChangeRate.Visible = False
            lblCompareLabel.Visible = False
        Else
            dtpFinalStopDate.Visible = False
            lblQ5.Visible = True
            lblQ5.Text = "มวน/วัน"
            txtStillSmoke.Visible = True
            optChangeRate.Visible = True
            lblCompareLabel.Visible = True
        End If
    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel"

                    'dtPOS = Session("dtposA4")
                    'dtPOS.Rows(e.CommandArgument).Delete()
                    'Session("dtposA4") = Nothing
                    'Session("dtposA4") = dtPOS

                    'grdData.DataSource = Nothing
                    'grdData.DataSource = dtPOS
                    'grdData.DataBind()

                    ctlOrder.MedOrder_DisableByItemA4(StrNull2Long(Session("patientid")), FORM_TYPE_ID_A4F, StrNull2Zero(grdData.DataKeys(e.CommandArgument).Value), lblLocationID.Text)

                    ctlSK.Stock_Inventory_Delete(StrNull2Zero(grdData.DataKeys(e.CommandArgument).Value), StrNull2Zero(grdData.Rows(e.CommandArgument).Cells(2).Text), FORM_TYPE_ID_A4F, lblLocationID.Text, "ISSPTN", lblID.Text)
                    grdData.DataSource = Nothing
                    LoadMedOrderItem(StrNull2Long(lblID.Text))

                    ddlMed.Focus()

            End Select


        End If
    End Sub

    Private Sub txtStillSmoke_TextChanged(sender As Object, e As EventArgs) Handles txtStillSmoke.TextChanged
        CompareDataFormA01()
    End Sub

    Private Sub grdData_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            e.Row.Cells(0).Text = e.Row.RowIndex + 1
            'Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            'Dim imgD As Image = e.Row.Cells(2).FindControl("imgDel")
            'imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub
     
End Class