﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="ReportLocationsGroup.aspx.vb" Inherits=".ReportLocationsGroup" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
        </asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>รายงานข้อมูลร้านยา
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
  
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">รายงานข้อมูลร้านยา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">

                 
             
            
<table border="0" cellPadding="2" cellSpacing="2">
                                                <tr>
                                                    <td align="left" class="texttopic">โครงการ :</td>
                                                    <td width="500" align="left" class="texttopic">
                                                        <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True" CssClass="OptionControl">                                                        </asp:DropDownList>                                                    </td>
                                                </tr>
                                                <tr>
                                                  <td align="left" class="texttopic">&nbsp;</td>
                                                  <td align="left" class="texttopic">
                                                      <asp:Button ID="cmdView" runat="server" CssClass="buttonFind" Text="ดูรายงาน" />
                                                    </td>
                                                </tr>
  </table>     
                </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>

    </section>
</asp:Content>
