﻿Public Class vmiStockMain
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlSk As New StockController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            grdData.PageIndex = 0
            LoadStock()
        End If

    End Sub


    Private Sub LoadStock()
        dt = ctlSk.StockMain_GetSearch(txtMedName.Text.Trim(), Session("LocationID"), "0")

        If dt.Rows.Count > 0 Then

            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To .Rows.Count - 1
                    .Rows(i).Cells(0).Text = i + 1
                Next

            End With
        Else

        End If
        dt = Nothing
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadStock()
    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub

    Protected Sub cmdSearch_Click(sender As Object, e As EventArgs) Handles cmdSearch.Click
        LoadStock()
    End Sub
End Class

