﻿Imports System.Data
Imports System.Data.SqlClient
Public Class rptServiceCountSummaryByProvince
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlOrder As New OrderController
    Dim dtSummary As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Request("p") = "0" Then
                lblProvinceName.Text = "ทุกจังหวัด"
            Else
                If Request("p") <> Request("grp") Then
                    lblProvinceName.Text = "จังหวัด " & ctlOrder.Province_GetNameByID(Request("p"))
                Else
                    lblProvinceName.Text = ctlOrder.ProvinceGroupName_GetByID(Request("grp"))
                End If
            End If

            dtSummary.Columns.Add("ProvinceID")
            dtSummary.Columns.Add("ProvinceName")
            dtSummary.Columns.Add("TotalCount")
            Session("dtsum") = dtSummary


        End If

        lblStartDate.Text = Request("b")
        lblEndDate.Text = Request("e")

        LoadDataToGrid(Request("p"), Request("b"), Request("e"))

        If Request("ex") = 1 Then
            Response.AppendHeader("content-disposition", "attachment;filename=" & ReportsName & ".xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
        End If

    End Sub

    Private Sub LoadDataToGrid(ByVal ProvID As String, ByVal Bdate As String, ByVal EDate As String)

        Dim StartDate As String = "0"
        Dim EndDate As String = "0"
        Dim sumCount As Integer

        If Bdate = "" And EDate = "" Then
            StartDate = 0
            EndDate = 0
        Else
            StartDate = ConvertStrDate2DBString(Bdate)
            EndDate = ConvertStrDate2DBString(EDate)
        End If


        dtSummary = Session("dtsum")
        ' 1. Bind Province to dtSummary
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            If Request("p") <> Request("grp") Then 'By Province
                dt = ctlOrder.Province_GetByID(ProvID)
            Else
                'By Section
                dt = ctlOrder.Province_GetInGroup(Request("grp"))
            End If

        Else
            If Request("p") <> "0" Then
                dt = ctlOrder.Province_GetByID(ProvID)
            Else
                'All
                dt = ctlOrder.RPT_Order_GetF01Province(StrNull2Zero(StartDate), StrNull2Zero(EndDate))
            End If

        End If


        Dim ProvinceID, ProvinceName As String
        Dim TotalCount As Integer


        If dt.Rows.Count > 0 Then

            For i = 0 To dt.Rows.Count - 1

                ProvinceID = dt.Rows(i)(0)
                ProvinceName = dt.Rows(i)(1)
                TotalCount = ctlOrder.RPT_Order_GetServiceCountByProvince(dt.Rows(i)(0), StrNull2Zero(StartDate), StrNull2Zero(EndDate))

                sumCount = sumCount + TotalCount


                Dim drSum As DataRow = dtSummary.NewRow()

                drSum("ProvinceID") = ProvinceID
                drSum("ProvinceName") = ProvinceName
                drSum("TotalCount") = TotalCount

                dtSummary.Rows.Add(drSum)

            Next



            '2. หา Risk Count
            '4 Sum total

            Dim dr As DataRow = dtSummary.NewRow()

            dr("ProvinceID") = ""
            dr("ProvinceName") = "รวมทั้งหมด"
            dr("TotalCount") = sumCount

            dtSummary.Rows.Add(dr)

            With grdSummary
                .Visible = True
                .DataSource = dtSummary
                .DataBind()

                .Rows(grdSummary.Rows.Count - 1).ForeColor = Drawing.Color.Blue
                .Rows(grdSummary.Rows.Count - 1).Font.Bold = True
            End With

        Else

            grdSummary.Visible = False
            grdSummary.DataSource = Nothing

        End If

        dt = Nothing
    End Sub

End Class