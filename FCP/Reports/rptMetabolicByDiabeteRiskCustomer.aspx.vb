﻿Imports System.Data
Imports System.Data.SqlClient
Public Class rptMetabolicByDiabeteRiskCustomer
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlOrder As New OrderController
    Dim dtF3 As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Request("p") = "0" Then
                lblProvinceName.Text = "ทุกจังหวัด"
            Else
                If Request("p") <> Request("grp") Then
                    lblProvinceName.Text = "จังหวัด " & ctlOrder.Province_GetNameByID(Request("p"))
                Else
                    lblProvinceName.Text = ctlOrder.ProvinceGroupName_GetByID(Request("grp"))
                End If
            End If


        End If

        lblStartDate.Text = Request("b")
        lblEndDate.Text = Request("e")

        LoadDataToGrid(Request("p"), Request("b"), Request("e"))

        If Request("ex") = 1 Then
            Response.AppendHeader("content-disposition", "attachment;filename=" & ReportsName & ".xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
        End If

    End Sub

    Private Sub LoadDataToGrid(ByVal ProvID As String, ByVal Bdate As String, ByVal EDate As String)

        Dim StartDate As String = "0"
        Dim EndDate As String = "0"
        Dim sumRefer, sumRisk, sumIsDiabete As Integer
        sumRisk = 0
        sumRefer = 0 
        sumIsDiabete = 0

        If Bdate = "" And EDate = "" Then
            StartDate = 0
            EndDate = 0
        Else
            StartDate = ConvertStrDate2DBString(Bdate)
            EndDate = ConvertStrDate2DBString(EDate)
        End If


        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            If Request("p") <> Request("grp") Then
                dt = ctlOrder.RPT_Order_GetF01CustomerByProvince(Request("p"), StrNull2Zero(StartDate), StrNull2Zero(EndDate))
            Else
                dt = ctlOrder.RPT_Order_GetF01CustomerByProvinceGroup(Request("grp"), StrNull2Zero(StartDate), StrNull2Zero(EndDate))
            End If

        Else
            dt = ctlOrder.RPT_Order_GetF01CustomerByProvince(Request("p"), StrNull2Zero(StartDate), StrNull2Zero(EndDate))
        End If

        If dt.Rows.Count > 0 Then

            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To dt.Rows.Count - 1
                    .Rows(i).Cells(0).Text = i + 1
                    .Rows(i).Cells(2).Text = DisplayGenderName(dt.Rows(i)("Gender"))

                    Select Case DBNull2Zero(dt.Rows(i)("isRiskDiabetes"))
                        Case 2
                            .Rows(i).Cells(14).Text = "เสี่ยงต่ำ"
                        Case 5
                            .Rows(i).Cells(14).Text = "เสี่ยงปานกลาง"
                        Case 8
                            .Rows(i).Cells(14).Text = "เสี่ยงสูง"
                        Case 9
                            .Rows(i).Cells(14).Text = "เสี่ยงสูงมาก"
                    End Select

                    dtF3 = ctlOrder.RPT_Order_GetF03ByRefID(dt.Rows(i)("itemID"))
                    If dtF3.Rows.Count > 0 Then
                        .Rows(i).Cells(12).Text = dtF3.Rows(0).Item("FBS")
                        .Rows(i).Cells(13).Text = dtF3.Rows(0).Item("Postprandial")

                        If Mid(DBNull2Str(dtF3.Rows(0)("Diseases")), 1, 1) = "Y" Then
                            .Rows(i).Cells(15).Text = "เป็น"
                        Else
                            .Rows(i).Cells(15).Text = "ไม่เป็น"
                        End If

                        'Select Case dtF3.Rows(0).Item("Diagnoses")
                        '    Case 0
                        '        .Rows(i).Cells(11).Text = "ไม่เป็น"
                        '    Case 1
                        '        .Rows(i).Cells(11).Text = "เป็น"
                        'End Select

                    End If

                    dtF3 = Nothing
                Next
            End With
        Else

            grdData.Visible = False
            grdData.DataSource = Nothing

        End If

        dt = Nothing
    End Sub

End Class