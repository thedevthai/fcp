﻿Imports System.Data
Imports System.Data.SqlClient
Public Class rptFinalSumaryByCustomer
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlOrder As New OrderController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Request("p") = "0" Then
                lblProvinceName.Text = "ทุกจังหวัด"
            Else
                If Request("p") <> Request("grp") Then
                    lblProvinceName.Text = "จังหวัด " & ctlOrder.Province_GetNameByID(Request("p"))
                Else
                    lblProvinceName.Text = ctlOrder.ProvinceGroupName_GetByID(Request("grp"))
                End If
            End If


        End If

        lblStartDate.Text = Request("b")
        lblEndDate.Text = Request("e")
        ReportsName = "ReportFinalSummary"
        LoadDataToGrid(Request("p"), Request("b"), Request("e"))

        If Request("ex") = 1 Then
            Response.AppendHeader("content-disposition", "attachment;filename=" & ReportsName & ".xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
        End If

    End Sub

    Private Sub LoadDataToGrid(ByVal ProvID As String, ByVal Bdate As String, ByVal EDate As String)

        Dim StartDate As String = "0"
        Dim EndDate As String = "0"
        Dim dtF1 As New DataTable
        Dim dtF3 As New DataTable
        Dim sumRefer, sumRisk, sumIsDiabete As Integer
        sumRisk = 0
        sumRefer = 0
        sumIsDiabete = 0

        If Bdate = "" And EDate = "" Then
            StartDate = 0
            EndDate = 0
        Else
            StartDate = ConvertStrDate2DBString(Bdate)
            EndDate = ConvertStrDate2DBString(EDate)
        End If


        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            If Request("p") <> Request("grp") Then
                dt = ctlOrder.RPT_GetCustomerByProvince(Request("p"), StrNull2Zero(StartDate), StrNull2Zero(EndDate))
            Else
                dt = ctlOrder.RPT_GetCustomerByProvinceGroup(Request("grp"), StrNull2Zero(StartDate), StrNull2Zero(EndDate))
            End If

        Else
            dt = ctlOrder.RPT_GetCustomerByProvince(Request("p"), StrNull2Zero(StartDate), StrNull2Zero(EndDate))
        End If


        If dt.Rows.Count > 0 Then

            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To dt.Rows.Count - 1
                    .Rows(i).Cells(0).Text = i + 1
                    .Rows(i).Cells(2).Text = DisplayGenderName(String.Concat(dt.Rows(i)("Gender")))


                    dtF1 = ctlOrder.Order_GetByCustomer(String.Concat(dt.Rows(i)("PatientID")), String.Concat(dt.Rows(i)("LocationID")), StrNull2Long(StartDate), StrNull2Long(EndDate))
                    If dtF1.Rows.Count > 0 Then

                        For n = 0 To dtF1.Rows.Count - 1
                            Select Case DBNull2Str(dtF1.Rows(n)("ServiceTypeID"))
                                Case FORM_TYPE_ID_F01
                                    .Rows(i).Cells(7).Text = "มี"
                                Case FORM_TYPE_ID_F02
                                    .Rows(i).Cells(8).Text = "มี"
                                Case FORM_TYPE_ID_F03
                                    .Rows(i).Cells(9).Text = "มี"
                                Case FORM_TYPE_ID_F04
                                    .Rows(i).Cells(10).Text = "มี"
                                Case FORM_TYPE_ID_F05
                                    .Rows(i).Cells(11).Text = "มี"
                                Case FORM_TYPE_ID_F06
                                    .Rows(i).Cells(12).Text = "มี"
                                Case FORM_TYPE_ID_F07
                                    .Rows(i).Cells(13).Text = "มี"
                                Case FORM_TYPE_ID_F08
                                    .Rows(i).Cells(14).Text = "มี"
                                Case FORM_TYPE_ID_F09
                                    .Rows(i).Cells(15).Text = "มี"
                                Case FORM_TYPE_ID_F10
                                    .Rows(i).Cells(16).Text = "มี"
                                Case FORM_TYPE_ID_F11
                                    .Rows(i).Cells(17).Text = "มี"
                                Case FORM_TYPE_ID_F11F
                                    .Rows(i).Cells(18).Text = "มี"
                                Case FORM_TYPE_ID_F12
                                    .Rows(i).Cells(19).Text = "มี"
                            End Select
                             
                        Next
                         
                    End If
                    dtF1 = Nothing

                    dtF1 = ctlOrder.RPT_GetF01ByCustomer(String.Concat(dt.Rows(i)("CustName")))
                    If dtF1.Rows.Count > 0 Then

                        Select Case DBNull2Zero(dtF1.Rows(0)("isRiskDiabetes"))
                            Case 2
                                .Rows(i).Cells(20).Text = "เสี่ยงต่ำ"
                            Case 5
                                .Rows(i).Cells(20).Text = "เสี่ยงปานกลาง"
                            Case 8
                                .Rows(i).Cells(20).Text = "เสี่ยงสูง"
                            Case 9
                                .Rows(i).Cells(20).Text = "เสี่ยงสูงมาก"
                        End Select

                        Select Case DBNull2Zero(dtF1.Rows(0)("isRiskFat"))
                            Case 0
                                .Rows(i).Cells(22).Text = "ไม่เสี่ยง"
                            Case 1
                                .Rows(i).Cells(22).Text = "เสี่ยง"
                        End Select


                        Select Case DBNull2Zero(dtF1.Rows(0)("isRiskBloodPressure"))
                            Case 0
                                .Rows(i).Cells(23).Text = "เสี่ยงต่ำ"
                            Case 2
                                .Rows(i).Cells(23).Text = "เสี่ยง"
                        End Select


                        dtF3 = ctlOrder.RPT_Order_GetF03ByRefID(dtF1.Rows(0)("itemID"))
                        If dtf3.Rows.Count > 0 Then

                            Select Case dtF3.Rows(0).Item("Diagnoses")
                                Case 0
                                    .Rows(i).Cells(21).Text = "ไม่เป็น"
                                Case 1
                                    .Rows(i).Cells(21).Text = "เป็น"

                            End Select
                        Else
                            .Rows(i).Cells(21).Text = "ติดตามไม่ได้"
                        End If
                    End If
                    dtF1 = Nothing
                    dtF3 = Nothing
                Next
            End With
        Else

            grdData.Visible = False
            grdData.DataSource = Nothing

        End If

        dt = Nothing
    End Sub

End Class