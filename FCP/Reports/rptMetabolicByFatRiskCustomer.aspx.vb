﻿Imports System.Data
Imports System.Data.SqlClient
Public Class rptMetabolicByFatRiskCustomer
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlOrder As New OrderController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Request("p") = "0" Then
                lblProvinceName.Text = "ทุกจังหวัด"
            Else
                If Request("p") <> Request("grp") Then
                    lblProvinceName.Text = "จังหวัด " & ctlOrder.Province_GetNameByID(Request("p"))
                Else
                    lblProvinceName.Text = ctlOrder.ProvinceGroupName_GetByID(Request("grp"))
                End If
            End If


        End If

        lblStartDate.Text = Request("b")
        lblEndDate.Text = Request("e")

        LoadDataToGrid(Request("p"), Request("b"), Request("e"))

        If Request("ex") = 1 Then
            Response.AppendHeader("content-disposition", "attachment;filename=" & ReportsName & ".xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
        End If

    End Sub

    Private Sub LoadDataToGrid(ByVal ProvID As String, ByVal Bdate As String, ByVal EDate As String)

        Dim StartDate As String = "0"
        Dim EndDate As String = "0"
        Dim sumRefer, sumRisk, sumIsDiabete As Integer
        sumRisk = 0
        sumRefer = 0
        sumIsDiabete = 0


        Dim AG1, AG2 As Integer
        If Request("ag") <> "0" Then
            Dim BP() As String = Split(Request("ag"), "-")
            AG1 = StrNull2Zero(BP(0))
            AG2 = StrNull2Zero(BP(1))
        Else
            AG1 = 0
            AG2 = 0
        End If


        If Bdate = "" And EDate = "" Then
            StartDate = 0
            EndDate = 0
        Else
            StartDate = ConvertStrDate2DBString(Bdate)
            EndDate = ConvertStrDate2DBString(EDate)
        End If


        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            If Request("p") <> Request("grp") Then
                dt = ctlOrder.RPT_Order_GetF01FatCustomerByProvince(Request("p"), AG1, AG2, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
            Else
                dt = ctlOrder.RPT_Order_GetF01FatCustomerByProvinceGroup(Request("grp"), AG1, AG2, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
            End If

        Else

            dt = ctlOrder.RPT_Order_GetF01FatCustomerByProvince(Request("p"), AG1, AG2, StrNull2Zero(StartDate), StrNull2Zero(EndDate))

        End If


        If dt.Rows.Count > 0 Then

            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To dt.Rows.Count - 1
                    .Rows(i).Cells(0).Text = i + 1
                    .Rows(i).Cells(2).Text = DisplayGenderName(dt.Rows(i)("Gender"))

                    Select Case DBNull2Zero(dt.Rows(i)("isRiskFat"))
                        Case 0
                            .Rows(i).Cells(10).Text = "ไม่เสี่ยง"
                        Case 1
                            .Rows(i).Cells(10).Text = "เสี่ยง"

                    End Select

                Next
            End With
        Else

            grdData.Visible = False
            grdData.DataSource = Nothing

        End If

        dt = Nothing
    End Sub

End Class