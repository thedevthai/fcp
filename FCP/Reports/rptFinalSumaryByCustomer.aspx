﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="rptFinalSumaryByCustomer.aspx.vb" Inherits=".rptFinalSumaryByCustomer" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
 <style type="text/css">
         Body{
	background: #fff;
	margin: 0 0 0 0;
	padding: 0 0 0 0;
	font-family: Geneva, Arial, Helvetica,Tahama, sans-serif;	
	color: #000;
	font-size: 13px;
	line-height: 25px;
}

.Page_Header{
font-size: 16px;
	font-style: normal;
	text-align: center;
	font-weight: bold;
  	color: #000;  
 	padding: 5px 0px 5px 0px;
	text-shadow: 1px 1px 1px rgba(255,255,255,0.8);
	
}
.texttopic{
	 	font-size: 13px;
	color:#555;
	font-weight: bold;
}
.TopicArticle
{
	color: #000;
	font-size: 14px;
	text-transform: uppercase;
	padding: 5px;
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
  
         
  <script language="Javascript">
      function doprint() {
          //save existing user's info
          //  var h = factory.printing.header;
          //  var f = factory.printing.footer;
          //hide the button
          document.all("cmdPrint").style.visibility = 'hidden';

          window.print();
          ////  factory.printing.SetMarginMeasure(2); 
          //  factory.printing.portrait = true;
          //  factory.printing.leftMargin = 1.75;
          //  factory.printing.topMargin = 1.75;
          //  factory.printing.rightMargin = 0.75;
          //  factory.printing.bottomMargin = 1.75;


          ////set header and footer to blank
          //  factory.printing.header = "";
          //  factory.printing.footer = "";
          //  //print page without prompt
          //  factory.DoPrint(false);
          //  //restore user's info
          //  factory.printing.header = h;
          //  factory.printing.footer = f;
          //show the print button
          // document.all("prnButton").style.visibility = 'visible';
          //  document.all("AButton").style.visibility = 'visible';
          //  document.all("BButton").style.visibility = 'visible';
          document.all("cmdPrint").style.visibility = 'visible';

      }
   
    </script>
 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" >
  <tr>
 <td align="center" class="Page_Header"> รายงานสรุปผลการให้บริการรายบุคคล</td>
    </tr>
    <tr>
 <td align="center" class="Page_Header">  
     <asp:Label ID="lblProvinceName" runat="server"></asp:Label>        </td>
    </tr>
    <tr>
    <td align="left" valign="top"><table border="0" align="center" cellpadding="0" cellspacing="2">
      <tr>
        <td width="45" align="center"><strong>ตั้งแต่</strong></td>
        <td>  <asp:Label ID="lblStartDate"        runat="server"></asp:Label>          </td>
        <td width="30" align="center"><strong>ถึง</strong></td>
        <td>  <asp:Label ID="lblEndDate"        runat="server"></asp:Label>          </td>
</tr>
    </table></td>
    </tr>    
    <tr>
    <td valign="top">
                                 <asp:GridView ID="grdData" runat="server" CellPadding="2" 
                      AutoGenerateColumns="False" 
                                     HorizontalAlign="Center" Width="100%">
                                     <RowStyle BackColor="#F7F7F7" VerticalAlign="Top" HorizontalAlign="Center" />
                                     <columns>
                                         <asp:BoundField HeaderText="No." >
                                         <ItemStyle HorizontalAlign="Center" />                                         </asp:BoundField>
                                         <asp:BoundField DataField="CustName" HeaderText="ชื่อ - สกุล" >
                                         <ItemStyle HorizontalAlign="Left" />                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="เพศ" DataField="Gender" />
                                         <asp:BoundField HeaderText="อายุ" DataField="Ages">
                                         <ItemStyle HorizontalAlign="Center" />                                         </asp:BoundField>
                                         <asp:BoundField DataField="Mobile" HeaderText="เบอร์โทร">
                                         <ItemStyle HorizontalAlign="Left" />
                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="ร้านยา" DataField="LocationName" />
                                         <asp:BoundField HeaderText="จังหวัด" DataField="LocationProvinceName" />
                                         <asp:BoundField HeaderText="F01">                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="F02" />
                                         <asp:BoundField HeaderText="F03" >
                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="F04" />
                                         <asp:BoundField HeaderText="F05" />
                                         <asp:BoundField HeaderText="F06" />
                                         <asp:BoundField HeaderText="F07">
                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="F08" />
                                         <asp:BoundField HeaderText="F09" />
                                         <asp:BoundField HeaderText="F10" />
                                         <asp:BoundField HeaderText="F11" />
                                         <asp:BoundField HeaderText="F11-F" />
                                         <asp:BoundField HeaderText="F12" />
                                         <asp:BoundField HeaderText="ผลการคัดกรองเบาหวาน" />
                                         <asp:BoundField HeaderText="ติดตามผลเบาหวาน" />
                                         <asp:BoundField HeaderText="ผลการคัดกรองความเสี่ยงอ้วน" />
                                         <asp:BoundField HeaderText="ผลการคัดกรองความเสี่ยงความดันโลหิตสูง" />
                                     </columns>
                                     <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                     <pagerstyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                     <SelectedRowStyle BackColor="#FF8B00" Font-Bold="True" ForeColor="#333333" />
                                     <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                                         VerticalAlign="Middle"  />
                                     <EditRowStyle BackColor="#2461BF" />
                                     <AlternatingRowStyle BackColor="White" VerticalAlign="Top" />
                                 </asp:GridView>
        </td>
        </tr>
        <tr>
                               <td align="center" valign="top">&nbsp;</td>                        
    </tr>   
   
        <tr>
                               <td align="center" valign="top"><input  type="button" value="Print" id="cmdPrint"  onclick="doprint();" /></td>                        
    </tr>   
   
</table>
</form>
</body>
</html>
