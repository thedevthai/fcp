﻿Imports System.Data
Imports System.Data.SqlClient
Public Class rptMetabolicByDiabeteRiskSummary
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlOrder As New OrderController
    Dim dtSummary As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Request("p") = "0" Then
                lblProvinceName.Text = "ทุกจังหวัด"
            Else
                If Request("p") <> Request("grp") Then
                    lblProvinceName.Text = "จังหวัด " & ctlOrder.Province_GetNameByID(Request("p"))
                Else
                    lblProvinceName.Text = ctlOrder.ProvinceGroupName_GetByID(Request("grp"))
                End If
            End If

            dtSummary.Columns.Add("LocationID")
            dtSummary.Columns.Add("LocationName")
            dtSummary.Columns.Add("ProvinceID")
            dtSummary.Columns.Add("ProvinceName")
            dtSummary.Columns.Add("TotalCount")
            dtSummary.Columns.Add("LowCount")
            dtSummary.Columns.Add("MidCount")
            dtSummary.Columns.Add("HighCount")
            dtSummary.Columns.Add("TopCount")
            dtSummary.Columns.Add("NotCount")
            dtSummary.Columns.Add("YesCount")
            dtSummary.Columns.Add("ReplyCount")
            dtSummary.Columns.Add("SumReferCount")
            Session("dtsum") = dtSummary


        End If

        lblStartDate.Text = Request("b")
        lblEndDate.Text = Request("e")

        LoadDataToGrid(Request("p"), Request("b"), Request("e"))

        If Request("ex") = 1 Then
            Response.AppendHeader("content-disposition", "attachment;filename=" & ReportsName & ".xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
        End If

    End Sub

    Private Sub LoadDataToGrid(ByVal ProvID As String, ByVal Bdate As String, ByVal EDate As String)

        Dim StartDate As String = "0"
        Dim EndDate As String = "0"
        Dim sumCount(9) As Integer

        If Bdate = "" And EDate = "" Then
            StartDate = 0
            EndDate = 0
        Else
            StartDate = ConvertStrDate2DBString(Bdate)
            EndDate = ConvertStrDate2DBString(EDate)
        End If


        dtSummary = Session("dtsum")
        ' 1. Bind Province to dtSummary
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            If Request("p") <> Request("grp") Then 'By Province
                dt = ctlOrder.Province_GetByID(ProvID)
            Else
                'By Section
                dt = ctlOrder.Province_GetInGroup(Request("grp"))
            End If

        Else
            If Request("p") <> "0" Then
                dt = ctlOrder.Province_GetByID(ProvID)
            Else
                'All
                '  dt = ctlOrder.RPT_Order_GetF01Province(StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                dt = ctlOrder.RPT_Order_GetF01Province(StrNull2Zero(StartDate), StrNull2Zero(EndDate))
            End If

        End If


        Dim ProvinceID, ProvinceName As String
        Dim TotalCount, LowCount, MidCount, HighCount, TopCount, NotCount, YesCount, ReplyCount, SumReferCount As Integer


        If dt.Rows.Count > 0 Then

            For i = 0 To dt.Rows.Count - 1

                ProvinceID = dt.Rows(i)(0)
                ProvinceName = dt.Rows(i)(1)
                TotalCount = ctlOrder.RPT_Order_GetF01Count(ProvinceID, StrNull2Zero(StartDate), StrNull2Zero(EndDate))


                LowCount = ctlOrder.RPT_Order_GetF01DiabeteRiskCount(dt.Rows(i)(0), 2, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                MidCount = ctlOrder.RPT_Order_GetF01DiabeteRiskCount(dt.Rows(i)(0), 5, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                HighCount = ctlOrder.RPT_Order_GetF01DiabeteRiskCount(dt.Rows(i)(0), 8, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                TopCount = ctlOrder.RPT_Order_GetF01DiabeteRiskCount(dt.Rows(i)(0), 9, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                YesCount = ctlOrder.RPT_Order_GetF03DiagnoseCountByDisease(dt.Rows(i)(0), 1, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                NotCount = TotalCount - YesCount
                ReplyCount = ctlOrder.RPT_Order_GetF03ReplyCount(dt.Rows(i)(0), StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                SumReferCount = ctlOrder.RPT_Order_GetF01ReferCount(dt.Rows(i)(0), StrNull2Zero(StartDate), StrNull2Zero(EndDate), 1)



                sumCount(0) = sumCount(0) + TotalCount
                sumCount(1) = sumCount(1) + LowCount
                sumCount(2) = sumCount(2) + MidCount
                sumCount(3) = sumCount(3) + HighCount
                sumCount(4) = sumCount(4) + TopCount
                sumCount(5) = sumCount(5) + NotCount
                sumCount(6) = sumCount(6) + YesCount
                sumCount(7) = sumCount(7) + ReplyCount
                sumCount(8) = sumCount(8) + SumReferCount



                Dim drSum As DataRow = dtSummary.NewRow()

                drSum("ProvinceID") = ProvinceID
                drSum("ProvinceName") = ProvinceName
                drSum("TotalCount") = TotalCount
                drSum("LowCount") = LowCount
                drSum("MidCount") = MidCount
                drSum("HighCount") = HighCount
                drSum("TopCount") = TopCount
                drSum("NotCount") = NotCount
                drSum("YesCount") = YesCount
                drSum("ReplyCount") = ReplyCount
                drSum("SumReferCount") = SumReferCount


                dtSummary.Rows.Add(drSum)

            Next



            '2. หา Risk Count
            '4 Sum total

            Dim dr As DataRow = dtSummary.NewRow()

            dr("ProvinceID") = ""
            dr("ProvinceName") = "รวมทั้งหมด"
            dr("TotalCount") = sumCount(0)
            dr("LowCount") = sumCount(1)
            dr("MidCount") = sumCount(2)
            dr("HighCount") = sumCount(3)
            dr("TopCount") = sumCount(4)
            dr("NotCount") = sumCount(5)
            dr("YesCount") = sumCount(6)
            dr("ReplyCount") = sumCount(7)
            dr("SumReferCount") = sumCount(8)

            dtSummary.Rows.Add(dr)

            With grdSummary
                .Visible = True
                .DataSource = dtSummary
                .DataBind()

                .Rows(grdSummary.Rows.Count - 1).ForeColor = Drawing.Color.Blue
                .Rows(grdSummary.Rows.Count - 1).Font.Bold = True
            End With

        Else

            grdSummary.Visible = False
            grdSummary.DataSource = Nothing

        End If

        dt = Nothing
    End Sub

End Class