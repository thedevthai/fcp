﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="rptMetabolicByBloodRiskSummary.aspx.vb" Inherits=".rptMetabolicByBloodRiskSummary" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
 <style type="text/css">
         Body{
	background: #fff;
	margin: 0 0 0 0;
	padding: 0 0 0 0;
	font-family: Geneva, Arial, Helvetica,Tahama, sans-serif;	
	color: #000;
	font-size: 13px;
	line-height: 25px;
}

.Page_Header{
font-size: 16px;
	font-style: normal;
	text-align: center;
	font-weight: bold;
  	color: #000;  
 	padding: 5px 0px 5px 0px;
	text-shadow: 1px 1px 1px rgba(255,255,255,0.8);
	
}
.texttopic{
	 	font-size: 13px;
	color:#555;
	font-weight: bold;
}
.TopicArticle
{
	color: #000;
	font-size: 14px;
	text-transform: uppercase;
	padding: 5px;
}
td.brd_rpt{
	border-top: 1px solid #5d5d5d;
	border-left: 1px solid #5d5d5d;
}
td.brd_rpt_r{
	border-top: 1px solid #5d5d5d;
	border-left: 1px solid #5d5d5d;
	border-right: 1px solid #5d5d5d;
}
 
 
     #cmdPrint
     {
         width: 66px;
     }
 
    </style>
</head>
<body>
    <form id="form1" runat="server">
  
         
  <script language="Javascript">
      function doprint() {
          //save existing user's info
          //  var h = factory.printing.header;
          //  var f = factory.printing.footer;
          //hide the button
          document.all("cmdPrint").style.visibility = 'hidden';

          window.print();
          ////  factory.printing.SetMarginMeasure(2); 
          //  factory.printing.portrait = true;
          //  factory.printing.leftMargin = 1.75;
          //  factory.printing.topMargin = 1.75;
          //  factory.printing.rightMargin = 0.75;
          //  factory.printing.bottomMargin = 1.75;


          ////set header and footer to blank
          //  factory.printing.header = "";
          //  factory.printing.footer = "";
          //  //print page without prompt
          //  factory.DoPrint(false);
          //  //restore user's info
          //  factory.printing.header = h;
          //  factory.printing.footer = f;
          //show the print button
          // document.all("prnButton").style.visibility = 'visible';
          //  document.all("AButton").style.visibility = 'visible';
          //  document.all("BButton").style.visibility = 'visible';
          document.all("cmdPrint").style.visibility = 'visible';

      }
   
    </script>
 <table width="800" border="0" align="center" cellpadding="0" cellspacing="0" >
  <tr>
 <td align="center" class="Page_Header"> รายงานสรุปผลการคัดกรองความเสี่ยงความดันโลหิตสูง</td>
    </tr>
    <tr>
 <td align="center" class="Page_Header">  
     <asp:Label ID="lblProvinceName" runat="server"></asp:Label>        </td>
    </tr>
    <tr>
    <td align="left" valign="top"><table border="0" align="center" cellpadding="0" cellspacing="2">
      <tr>
        <td width="45" align="center"><strong>ตั้งแต่</strong></td>
        <td>  <asp:Label ID="lblStartDate"        runat="server"></asp:Label>          </td>
        <td width="30" align="center"><strong>ถึง</strong></td>
        <td>  <asp:Label ID="lblEndDate"        runat="server"></asp:Label>          </td>
</tr>
    </table></td>
    </tr>    
     <tr>
                               <td align="center" valign="top">
                                <table width="100%" align="right" cellpadding="0" cellspacing="0"  >
<tr>
                                         <td align="center"></td>
                         <td width="58" align="center"></td>
            <td width="60" align="center"></td>
            <td width="60" align="center"></td>
                                  <td width="51" align="center">&nbsp;</td>
            <td width="55" align="center">&nbsp;</td>
                     <td width="38" align="center">&nbsp;</td>
            <td width="45" align="center">&nbsp;</td>
            <td width="45" align="center">&nbsp;</td>
            <td width="45" align="center">&nbsp;</td>
            <td width="45" align="center">&nbsp;</td>
            <td width="45" align="center">&nbsp;</td>
                         <td width="44" align="center">&nbsp;</td>
            <td width="50" align="center">&nbsp;</td>
            <td width="50" align="center">&nbsp;</td>
              <td width="55" align="center">&nbsp;</td>
                       </tr>
                       <tr>
                         <td align="center">&nbsp;</td>
                         <td colspan="3" align="center" class="brd_rpt"><strong>ผลการคัดกรอง</strong></td>
                         <td colspan="2" align="center" class="brd_rpt"><strong>แยกตามเพศ</strong></td>
                         <td colspan="6" align="center" class="brd_rpt"><strong>แยกตามช่วงอายุ (ปี)</strong></td>
                         <td colspan="3" align="center" class="brd_rpt" ><strong>แยกตาม BP เฉลี่ย</strong></td>
                           <td align="center" class="brd_rpt_r" ><strong>สรุปผล</strong></td>
                         </tr>
                                 </table>
       </td>                        
    </tr>   
    <tr>
    <td align="right">                                
<asp:GridView ID="grdSummary" runat="server" CellPadding="2" 
                      AutoGenerateColumns="False" 
                                     HorizontalAlign="Center" Width="100%">
                                     <RowStyle BackColor="#F7F7F7" VerticalAlign="Middle" HorizontalAlign="Center" />
                                     <columns>
                                         <asp:BoundField HeaderText="จังหวัด" DataField="ProvinceName">                                         
                                         <ItemStyle HorizontalAlign="Center" />
                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="รับบริการ" DataField="TotalCount">
                                         <ItemStyle HorizontalAlign="Center" Width="70px" />                                         </asp:BoundField>
                                         <asp:BoundField DataField="NotCount" HeaderText="เสี่ยงต่ำ">
                                         <ItemStyle HorizontalAlign="Center" Width="50px" />                                         </asp:BoundField>
                                         <asp:BoundField DataField="YesCount" HeaderText="เสี่ยง" >
                                         <ItemStyle HorizontalAlign="Center" Width="50px" />                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="ชาย" DataField="MCount" >                                         
                                         <ItemStyle HorizontalAlign="Center" Width="50px" />                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="หญิง" DataField="FCount" >
                                         <ItemStyle HorizontalAlign="Center" Width="50px" />                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="15-20" DataField="A1Count" >
                                         <ItemStyle HorizontalAlign="Center" Width="40px" />                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="21-30" DataField="A2Count" >
                                         <ItemStyle HorizontalAlign="Center" Width="40px" />                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="31-40" DataField="A3Count" >
                                         <ItemStyle HorizontalAlign="Center" Width="40px" />                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="41-50" DataField="A4Count" >
                                         <ItemStyle Width="40px" />
                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="51-60" DataField="A5Count" >
                                         <ItemStyle Width="40px" />
                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="&gt;60" DataField="A6Count" >
                                         <ItemStyle Width="40px" />
                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="&lt;=120" DataField="BP1Count">
                                         <ItemStyle Width="40px" />
                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="121-140" DataField="BP2Count">
                                         <ItemStyle Width="50px" />
                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="&gt;140" DataField="BP3Count">
                                         <ItemStyle Width="40px" />
                                         </asp:BoundField>
                                         <asp:BoundField HeaderText="เป็นโรค" DataField="SumYesCount">
                                         <ItemStyle Width="50px" HorizontalAlign="Center" />
                                         </asp:BoundField>
                                         <asp:BoundField DataField="SumReferCount" HeaderText="Refer" />
                                     </columns>
                                     <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                     <pagerstyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                     <SelectedRowStyle BackColor="#FF8B00" Font-Bold="True" ForeColor="#333333" />
                                     <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                                         VerticalAlign="Middle"  />
                                     <EditRowStyle BackColor="#2461BF" />
                                     <AlternatingRowStyle BackColor="White" VerticalAlign="Top" />
                                 </asp:GridView>
        </td>
    </tr>
        <tr>
                               <td align="center" valign="top">&nbsp;</td>                        
    </tr>   
   
        <tr>
                               <td align="center" valign="top"><input  type="button" value="Print" id="cmdPrint"  onclick="doprint();" /></td>                        
    </tr>   
   
</table>
</form>
</body>
</html>
