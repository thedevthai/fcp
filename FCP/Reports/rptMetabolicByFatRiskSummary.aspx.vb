﻿Imports System.Data
Imports System.Data.SqlClient
Public Class rptMetabolicByFatRiskSummary
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlOrder As New OrderController
    Dim dtSummary As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Request("p") = "0" Then
                lblProvinceName.Text = "ทุกจังหวัด"
            Else
                If Request("p") <> Request("grp") Then
                    lblProvinceName.Text = "จังหวัด " & ctlOrder.Province_GetNameByID(Request("p"))
                Else
                    lblProvinceName.Text = ctlOrder.ProvinceGroupName_GetByID(Request("grp"))
                End If
            End If

            dtSummary.Columns.Add("ProvinceID")
            dtSummary.Columns.Add("ProvinceName")
            dtSummary.Columns.Add("TotalCount")
            dtSummary.Columns.Add("NotCount")
            dtSummary.Columns.Add("YesCount")
            dtSummary.Columns.Add("MCount")
            dtSummary.Columns.Add("FCount")
            dtSummary.Columns.Add("A1Count")
            dtSummary.Columns.Add("A2Count")
            dtSummary.Columns.Add("A3Count")
            dtSummary.Columns.Add("A4Count")
            dtSummary.Columns.Add("A5Count")
            dtSummary.Columns.Add("A6Count")

            Session("dtsum") = dtSummary


        End If

        lblStartDate.Text = Request("b")
        lblEndDate.Text = Request("e")

        LoadDataToGrid(Request("p"), Request("b"), Request("e"))

        If Request("ex") = 1 Then
            Response.AppendHeader("content-disposition", "attachment;filename=" & ReportsName & ".xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
        End If

    End Sub

    Private Sub LoadDataToGrid(ByVal ProvID As String, ByVal Bdate As String, ByVal EDate As String)

        Dim StartDate As String = "0"
        Dim EndDate As String = "0"
        Dim sumCount(11) As Integer

        If Bdate = "" And EDate = "" Then
            StartDate = 0
            EndDate = 0
        Else
            StartDate = ConvertStrDate2DBString(Bdate)
            EndDate = ConvertStrDate2DBString(EDate)
        End If


        dtSummary = Session("dtsum")
        ' 1. Bind Province to dtSummary
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            If Request("p") <> Request("grp") Then 'By Province
                dt = ctlOrder.Province_GetByID(ProvID)
            Else
                'By Section
                dt = ctlOrder.Province_GetInGroup(Request("grp"))
            End If

        Else
            If Request("p") <> "0" Then
                dt = ctlOrder.Province_GetByID(ProvID)
            Else
                'All
                dt = ctlOrder.RPT_Order_GetF01Province(StrNull2Zero(StartDate), StrNull2Zero(EndDate))
            End If
        End If

        Dim ProvinceID, ProvinceName As String
        Dim TotalCount, NotCount, YesCount, MCount, FCount, A1Count, A2Count, A3Count, A4Count, A5Count, A6Count As Integer


        If dt.Rows.Count > 0 Then

            For i = 0 To dt.Rows.Count - 1
                '2. หา Risk Count
                ProvinceID = dt.Rows(i)(0)
                ProvinceName = dt.Rows(i)(1)

                TotalCount = ctlOrder.RPT_Order_GetF01Count(ProvinceID, StrNull2Zero(StartDate), StrNull2Zero(EndDate))

                NotCount = ctlOrder.RPT_Order_GetF01FatRiskCount(dt.Rows(i)(0), 0, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                YesCount = TotalCount - NotCount

                MCount = ctlOrder.RPT_Order_GetF01FatRiskGenderCount(dt.Rows(i)(0), "M", StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                FCount = YesCount - MCount

                A1Count = ctlOrder.RPT_Order_GetF01FatRiskAgeCount(dt.Rows(i)(0), 15, 20, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                A2Count = ctlOrder.RPT_Order_GetF01FatRiskAgeCount(dt.Rows(i)(0), 21, 30, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                A3Count = ctlOrder.RPT_Order_GetF01FatRiskAgeCount(dt.Rows(i)(0), 31, 40, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                A4Count = ctlOrder.RPT_Order_GetF01FatRiskAgeCount(dt.Rows(i)(0), 41, 50, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                A5Count = ctlOrder.RPT_Order_GetF01FatRiskAgeCount(dt.Rows(i)(0), 51, 60, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                A6Count = ctlOrder.RPT_Order_GetF01FatRiskAgeCount(dt.Rows(i)(0), 60, 200, StrNull2Zero(StartDate), StrNull2Zero(EndDate))


                sumCount(0) = sumCount(0) + TotalCount
                sumCount(1) = sumCount(1) + NotCount
                sumCount(2) = sumCount(2) + YesCount
                sumCount(3) = sumCount(3) + MCount
                sumCount(4) = sumCount(4) + FCount
                sumCount(5) = sumCount(5) + A1Count
                sumCount(6) = sumCount(6) + A2Count
                sumCount(7) = sumCount(7) + A3Count
                sumCount(8) = sumCount(8) + A4Count
                sumCount(9) = sumCount(9) + A5Count
                sumCount(10) = sumCount(10) + A6Count


                Dim drSum As DataRow = dtSummary.NewRow()

                drSum("ProvinceID") = ProvinceID
                drSum("ProvinceName") = ProvinceName
                drSum("TotalCount") = TotalCount
                drSum("NotCount") = NotCount
                drSum("YesCount") = YesCount
                drSum("MCount") = MCount
                drSum("FCount") = FCount
                drSum("A1Count") = A1Count
                drSum("A2Count") = A2Count
                drSum("A3Count") = A3Count
                drSum("A4Count") = A4Count
                drSum("A5Count") = A5Count
                drSum("A6Count") = A6Count

                dtSummary.Rows.Add(drSum)

            Next


            '4 Sum total

            Dim dr As DataRow = dtSummary.NewRow()

            dr("ProvinceID") = ""
            dr("ProvinceName") = "รวมทั้งหมด"
            dr("TotalCount") = sumCount(0)
            dr("NotCount") = sumCount(1)
            dr("YesCount") = sumCount(2)
            dr("MCount") = sumCount(3)
            dr("FCount") = sumCount(4)
            dr("A1Count") = sumCount(5)
            dr("A2Count") = sumCount(6)
            dr("A3Count") = sumCount(7)
            dr("A4Count") = sumCount(8)
            dr("A5Count") = sumCount(9)
            dr("A6Count") = sumCount(10)


            dtSummary.Rows.Add(dr)

            With grdSummary
                .Visible = True
                .DataSource = dtSummary
                .DataBind()

                .Rows(grdSummary.Rows.Count - 1).ForeColor = Drawing.Color.Blue
                .Rows(grdSummary.Rows.Count - 1).Font.Bold = True
            End With

        Else

            grdSummary.Visible = False
            grdSummary.DataSource = Nothing

        End If

        dt = Nothing
    End Sub

End Class