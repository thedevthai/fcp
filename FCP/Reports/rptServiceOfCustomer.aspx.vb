﻿Imports System.Data
Imports System.Data.SqlClient
Public Class rptServiceOfCustomer
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlOrder As New OrderController
    Dim dtF3 As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Request("p") = "0" Then
                lblProvinceName.Text = "ทุกจังหวัด"
            Else
                If Request("p") <> Request("grp") Then
                    lblProvinceName.Text = "จังหวัด " & ctlOrder.Province_GetNameByID(Request("p"))
                Else
                    lblProvinceName.Text = ctlOrder.ProvinceGroupName_GetByID(Request("grp"))
                End If
            End If


        End If

        lblStartDate.Text = Request("b")
        lblEndDate.Text = Request("e")

        LoadDataToGrid(Request("p"), Request("b"), Request("e"))

        If Request("ex") = 1 Then
            Response.AppendHeader("content-disposition", "attachment;filename=" & ReportsName & ".xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
        End If

    End Sub

    Private Sub LoadDataToGrid(ByVal ProvID As String, ByVal Bdate As String, ByVal EDate As String)

        Dim StartDate As String = "0"
        Dim EndDate As String = "0"
        Dim sumRefer, sumRisk, sumIsDiabete As Integer
        sumRisk = 0
        sumRefer = 0
        sumIsDiabete = 0

        If Bdate = "" And EDate = "" Then
            StartDate = 0
            EndDate = 0
        Else
            StartDate = ConvertStrDate2DBString(Bdate)
            EndDate = ConvertStrDate2DBString(EDate)
        End If

        dt = ctlOrder.RPT_Order_GetCustomerByProvince(Request("p"), StrNull2Zero(StartDate), StrNull2Zero(EndDate))

        If dt.Rows.Count > 0 Then

            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To dt.Rows.Count - 1
                    .Rows(i).Cells(0).Text = i + 1
                    .Rows(i).Cells(2).Text = DisplayGenderName(dt.Rows(i)("Gender"))

                    dtF3 = ctlOrder.RPT_Order_GetServiceOfCustomer(StrNull2Long(.DataKeys(i).Value))
                    If dtF3.Rows.Count > 0 Then
                        For n = 0 To dtF3.Rows.Count - 1
                            If n <> dtF3.Rows.Count - 1 Then
                                .Rows(i).Cells(5).Text &= dtF3.Rows(n)(0) & " , "
                            Else
                                .Rows(i).Cells(5).Text &= dtF3.Rows(n)(0)
                            End If

                        Next

                    End If

                    dtF3 = Nothing
                Next
            End With
        Else

            grdData.Visible = False
            grdData.DataSource = Nothing

        End If

        dt = Nothing
    End Sub

End Class