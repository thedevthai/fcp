﻿Imports System.Data
Imports System.Data.SqlClient
Public Class rptMetabolicByShopGroupSummary
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlOrder As New OrderController
    Dim dtSummary As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Request("p") = "0" Then
                lblProvinceName.Text = "ทุกจังหวัด"
            Else
                If Request("p") <> Request("grp") Then
                    lblProvinceName.Text = "จังหวัด " & ctlOrder.Province_GetNameByID(Request("p"))
                Else
                    lblProvinceName.Text = ctlOrder.ProvinceGroupName_GetByID(Request("grp"))
                End If
            End If

            dtSummary.Columns.Add("ProvinceID")
            dtSummary.Columns.Add("ProvinceName")
            dtSummary.Columns.Add("TotalCount")
            dtSummary.Columns.Add("ACount")
            dtSummary.Columns.Add("BCount")
            dtSummary.Columns.Add("WCount")
            dtSummary.Columns.Add("XCount")
            dtSummary.Columns.Add("PCount")
            dtSummary.Columns.Add("UCount")
            dtSummary.Columns.Add("D1Count")
            dtSummary.Columns.Add("D2Count")
            dtSummary.Columns.Add("D3Count")
            dtSummary.Columns.Add("SumYesCount")
            dtSummary.Columns.Add("ACountYes")
            dtSummary.Columns.Add("BCountYes")
            dtSummary.Columns.Add("WCountYes")
            dtSummary.Columns.Add("XCountYes")
            dtSummary.Columns.Add("PCountYes")
            dtSummary.Columns.Add("UCountYes")
            dtSummary.Columns.Add("DiseaseCount")

            Session("dtsum") = dtSummary


        End If

        lblStartDate.Text = Request("b")
        lblEndDate.Text = Request("e")

        LoadDataToGrid(Request("p"), Request("b"), Request("e"))

        If Request("ex") = 1 Then
            ReportsName = "ReportNHSO"
            Response.AppendHeader("content-disposition", "attachment;filename=" & ReportsName & ".xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
        End If

    End Sub

    Private Sub LoadDataToGrid(ByVal ProvID As String, ByVal Bdate As String, ByVal EDate As String)

        Dim StartDate As String = "0"
        Dim EndDate As String = "0"
        Dim sumCount(18) As Integer

        If Bdate = "" And EDate = "" Then
            StartDate = 0
            EndDate = 0
        Else
            StartDate = ConvertStrDate2DBString(Bdate)
            EndDate = ConvertStrDate2DBString(EDate)
        End If


        dtSummary = Session("dtsum")
        ' 1. Bind Province to dtSummary
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            If Request("p") <> Request("grp") Then 'By Province
                dt = ctlOrder.Province_GetByID(ProvID)
            Else
                'By Section
                dt = ctlOrder.Province_GetInGroup(Request("grp"))
            End If

        Else
            If Request("p") <> "0" Then
                dt = ctlOrder.Province_GetByID(ProvID)
            Else
                'All
                dt = ctlOrder.RPT_Order_GetF01Province(StrNull2Zero(StartDate), StrNull2Zero(EndDate))
            End If
        End If

        Dim ProvinceID, ProvinceName As String
        Dim TotalCount, ACount, BCount, WCount, XCount, PCount, UCount, D1Count, D2Count, D3Count, SumYesCount, ACountYes, BCountYes, WCountYes, XCountYes, PCountYes, UCountYes, DiseaseCount As Integer


        If dt.Rows.Count > 0 Then

            For i = 0 To dt.Rows.Count - 1
                '2. หา Risk Count
                ProvinceID = dt.Rows(i)(0)
                ProvinceName = dt.Rows(i)(1)

                TotalCount = ctlOrder.RPT_Order_GetF01Count(ProvinceID, StrNull2Zero(StartDate), StrNull2Zero(EndDate))

                ACount = ctlOrder.RPT_Order_GetF01CountByShopGroup(dt.Rows(i)(0), "A", StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                BCount = ctlOrder.RPT_Order_GetF01CountByShopGroup(dt.Rows(i)(0), "B", StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                WCount = ctlOrder.RPT_Order_GetF01CountByShopGroup(dt.Rows(i)(0), "W", StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                XCount = ctlOrder.RPT_Order_GetF01CountByShopGroup(dt.Rows(i)(0), "X", StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                PCount = ctlOrder.RPT_Order_GetF01CountByShopGroup(dt.Rows(i)(0), "P", StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                UCount = ctlOrder.RPT_Order_GetF01CountByShopGroup(dt.Rows(i)(0), "U", StrNull2Zero(StartDate), StrNull2Zero(EndDate))

                D1Count = ctlOrder.RPT_Order_GetF03DiagnoseCountByDisease(dt.Rows(i)(0), 1, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                D2Count = ctlOrder.RPT_Order_GetF03DiagnoseCountByDisease(dt.Rows(i)(0), 3, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                D3Count = ctlOrder.RPT_Order_GetF03DiagnoseCountByDisease(dt.Rows(i)(0), 5, StrNull2Zero(StartDate), StrNull2Zero(EndDate))

                SumYesCount = ctlOrder.RPT_Order_GetF03AbnormalCount(dt.Rows(i)(0), StrNull2Zero(StartDate), StrNull2Zero(EndDate))

                ACountYes = ctlOrder.RPT_Order_GetF03YesCountByShopGroup(dt.Rows(i)(0), "A", StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                BCountYes = ctlOrder.RPT_Order_GetF03YesCountByShopGroup(dt.Rows(i)(0), "B", StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                WCountYes = ctlOrder.RPT_Order_GetF03YesCountByShopGroup(dt.Rows(i)(0), "W", StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                XCountYes = ctlOrder.RPT_Order_GetF03YesCountByShopGroup(dt.Rows(i)(0), "X", StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                PCountYes = ctlOrder.RPT_Order_GetF03YesCountByShopGroup(dt.Rows(i)(0), "P", StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                UCountYes = ctlOrder.RPT_Order_GetF03YesCountByShopGroup(dt.Rows(i)(0), "U", StrNull2Zero(StartDate), StrNull2Zero(EndDate))

                DiseaseCount = ctlOrder.RPT_Order_GetF03DiseaseAbnormalCount(dt.Rows(i)(0), 2, StrNull2Zero(StartDate), StrNull2Zero(EndDate))

                sumCount(0) = sumCount(0) + TotalCount
                sumCount(1) = sumCount(1) + ACount
                sumCount(2) = sumCount(2) + BCount
                sumCount(3) = sumCount(3) + WCount
                sumCount(4) = sumCount(4) + XCount
                sumCount(5) = sumCount(5) + PCount
                sumCount(6) = sumCount(6) + UCount
                sumCount(7) = sumCount(7) + D1Count
                sumCount(8) = sumCount(8) + D2Count
                sumCount(9) = sumCount(9) + D3Count
                sumCount(10) = sumCount(10) + SumYesCount
                sumCount(11) = sumCount(11) + ACountYes
                sumCount(12) = sumCount(12) + BCountYes
                sumCount(13) = sumCount(13) + WCountYes
                sumCount(14) = sumCount(14) + XCountYes
                sumCount(15) = sumCount(15) + PCountYes
                sumCount(16) = sumCount(16) + UCountYes
                sumCount(17) = sumCount(17) + DiseaseCount
                Dim drSum As DataRow = dtSummary.NewRow()

                drSum("ProvinceID") = ProvinceID
                drSum("ProvinceName") = ProvinceName
                drSum("TotalCount") = TotalCount
                drSum("ACount") = ACount
                drSum("BCount") = BCount
                drSum("WCount") = WCount
                drSum("XCount") = XCount
                drSum("PCount") = PCount
                drSum("UCount") = UCount
                drSum("D1Count") = D1Count
                drSum("D2Count") = D2Count
                drSum("D3Count") = D3Count
                drSum("SumYesCount") = SumYesCount
                drSum("ACountYes") = ACountYes
                drSum("BCountYes") = BCountYes
                drSum("WCountYes") = WCountYes
                drSum("XCountYes") = XCountYes
                drSum("PCountYes") = PCountYes
                drSum("UCountYes") = UCountYes
                drSum("DiseaseCount") = DiseaseCount
                dtSummary.Rows.Add(drSum)

            Next


            '4 Sum total

            Dim dr As DataRow = dtSummary.NewRow()

            dr("ProvinceID") = ""
            dr("ProvinceName") = "รวมทั้งหมด"
            dr("TotalCount") = sumCount(0)
            dr("ACount") = sumCount(1)
            dr("BCount") = sumCount(2)
            dr("WCount") = sumCount(3)
            dr("XCount") = sumCount(4)
            dr("PCount") = sumCount(5)
            dr("UCount") = sumCount(6)
            dr("D1Count") = sumCount(7)
            dr("D2Count") = sumCount(8)
            dr("D3Count") = sumCount(9)
            dr("SumYesCount") = sumCount(10)
            dr("ACountYes") = sumCount(11)
            dr("BCountYes") = sumCount(12)
            dr("WCountYes") = sumCount(13)
            dr("XCountYes") = sumCount(14)
            dr("PCountYes") = sumCount(15)
            dr("UCountYes") = sumCount(16)
            dr("DiseaseCount") = sumCount(17)

            dtSummary.Rows.Add(dr)

            With grdSummary
                .Visible = True
                .DataSource = dtSummary
                .DataBind()

                .Rows(grdSummary.Rows.Count - 1).ForeColor = Drawing.Color.Blue
                .Rows(grdSummary.Rows.Count - 1).Font.Bold = True
            End With

        Else

            grdSummary.Visible = False
            grdSummary.DataSource = Nothing

        End If

        dt = Nothing
    End Sub

End Class