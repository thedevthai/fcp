﻿Imports System.Data
Imports System.Data.SqlClient
Public Class rptMetabolicByBloodRiskCustomer
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlOrder As New OrderController
    Dim dtF3 As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Request("p") = "0" Then
                lblProvinceName.Text = "ทุกจังหวัด"
            Else
                If Request("p") <> Request("grp") Then
                    lblProvinceName.Text = "จังหวัด " & ctlOrder.Province_GetNameByID(Request("p"))
                Else
                    lblProvinceName.Text = ctlOrder.ProvinceGroupName_GetByID(Request("grp"))
                End If
            End If


        End If

        lblStartDate.Text = Request("b")
        lblEndDate.Text = Request("e")

        LoadDataToGrid(Request("p"), Request("b"), Request("e"))

        If Request("ex") = 1 Then
            Response.AppendHeader("content-disposition", "attachment;filename=" & ReportsName & ".xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
        End If

    End Sub

    Private Sub LoadDataToGrid(ByVal ProvID As String, ByVal Bdate As String, ByVal EDate As String)

        Dim StartDate As String = "0"
        Dim EndDate As String = "0"
        Dim BP1, BP2 As Integer
        If Request("bp") <> "0" Then
            Dim BP() As String = Split(Request("bp"), "-")
            BP1 = StrNull2Zero(BP(0))
            BP2 = StrNull2Zero(BP(1))
        Else
            BP1 = 0
            BP2 = 0
        End If


        If Bdate = "" And EDate = "" Then
            StartDate = 0
            EndDate = 0
        Else
            StartDate = ConvertStrDate2DBString(Bdate)
            EndDate = ConvertStrDate2DBString(EDate)
        End If


        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            If Request("p") <> Request("grp") Then
                dt = ctlOrder.RPT_Order_GetF01BloodCustomerByProvince(Request("p"), BP1, BP2, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
            Else
                dt = ctlOrder.RPT_Order_GetF01BloodCustomerByProvinceGroup(Request("grp"), BP1, BP2, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
            End If

        Else
            dt = ctlOrder.RPT_Order_GetF01BloodCustomerByProvince(Request("p"), BP1, BP2, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
        End If


        If dt.Rows.Count > 0 Then

            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To dt.Rows.Count - 1
                    .Rows(i).Cells(0).Text = i + 1
                    .Rows(i).Cells(2).Text = DisplayGenderName(dt.Rows(i)("Gender"))

                    Select Case DBNull2Zero(dt.Rows(i)("isRiskBloodPressure"))
                        Case 0
                            .Rows(i).Cells(10).Text = "เสี่ยงต่ำ"
                        Case 2
                            .Rows(i).Cells(10).Text = "เสี่ยง"
                    End Select

                    .Rows(i).Cells(11).Text = DBNull2Zero(dt.Rows(i)("BPAVG")) & "/" & DBNull2Zero(dt.Rows(i)("BPAVG_2"))


                    dtF3 = ctlOrder.RPT_Order_GetF03ByRefID(dt.Rows(i)("itemID"))
                    If dtF3.Rows.Count > 0 Then

                        If Mid(DBNull2Str(dtF3.Rows(0)("Diseases")), 3, 1) = "Y" Then
                            .Rows(i).Cells(12).Text = "เป็น"
                        Else
                            .Rows(i).Cells(12).Text = "ไม่เป็น"
                        End If

                        'If DBNull2Zero(dtF3.Rows(0)("Diagnoses")) <> 0 Then
                        '    .Rows(i).Cells(8).Text = "เป็น"
                        'ElseIf DBNull2Zero(dtF3.Rows(0)("Diagnoses")) = 0 Then
                        '    .Rows(i).Cells(8).Text = "ไม่เป็น"
                        'End If
                    End If

                    dtF3 = Nothing

                Next
            End With
        Else

            grdData.Visible = False
            grdData.DataSource = Nothing

        End If

        dt = Nothing
    End Sub

End Class