﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="vmiStock_Old.aspx.vb" Inherits=".vmiStock_Old" %>


<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
<script src="js/jquery-1.10.2.js"></script> 

<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
   <script src="js/jquery-1.9.1.js"></script>
  <script src="js/jquery-ui.js"></script>
  
  <script> $(function() {
       $( "#dialog-message" ).dialog({
      autoOpen: false,
      height: 600,
      width: 600,
      modal: true,
      buttons: {
        Close: function() {
          $( this ).dialog( "close" );
        }
      },
      close: function() {
        
      }
    });
 
    $( "#user-login" )
      .button()
      .click(function() {
        $( "#dialog-message" ).dialog( "open" );
      });
  });
  </script>



    <style type="text/css">
        .auto-style1 {
            font-size: 13px;
            color: #555;
            font-weight: bold;
            height: 28px;
        }
        .auto-style2 {
            height: 28px;
        }
        .auto-style3 {
            height: 25px;
            color: #FFFFFF;
            font-size: 12px;
            font-weight: bold;
            border-bottom: 1px dotted #0157d8;
            padding-left: 10;
            padding-right: 0;
            padding-top: 5;
            padding-bottom: 0;
            background-color: #1e82cd;
        }
    </style>
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
  
    <div class="Page_Header">
        Stock Setup</div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td align="left" valign="top">
              <dx:ASPxPageControl ID="tabPageSearch" runat="server" ActiveTabIndex="0" BackColor="#DDE0E3" EnableTheming="True" Theme="MetropolisBlue" Width="99.9%">
                  <TabPages>
                      <dx:TabPage Name="pSearch" Text="ค้นหา / Search">
                          <ContentCollection>
                              <dx:ContentControl runat="server">
                                  <table border="0" cellpadding="1" cellspacing="1">
                                      <tr>
                                          <td align="left" class="auto-style1">ชื่อยา : </td>
                                          <td align="left" class="auto-style2">
                                              <asp:TextBox ID="txtMedName" runat="server" Width="200px"></asp:TextBox>
                                          </td>
                                          <td>
                                              <asp:Button ID="cmdSearch" runat="server" CssClass="buttonFind" Text="Search" Width="100px" />
                                          </td>
                                      </tr>
                                  </table>
                              </dx:ContentControl>
                          </ContentCollection>
                      </dx:TabPage>
                  </TabPages>
                  <Paddings Padding="0px" />
                  <Border BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" />
              </dx:ASPxPageControl>
            </td>
      </tr>
       <tr>
          <td align="left" valign="top" class="auto-style3"> รายการสต๊อกยา</td>
      </tr>
        <tr>
          <td align="center" valign="top">
              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" DataKeyNames="MedUID">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="No.">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />                      </asp:BoundField>
                <asp:BoundField DataField="itemName" HeaderText="Item Name" >
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Stock" HeaderText="Stock Point" />
                <asp:BoundField DataField="OnHand" HeaderText="On hand" />
                <asp:TemplateField HeaderText="ROP">
                    <ItemTemplate>
                        <asp:TextBox ID="txtROP" runat="server" Width="50px" Text='<%# DataBinder.Eval(Container.DataItem, "ROP") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="UOM" HeaderText="UOM" />
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationA dc_paginationA07" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView></td>
      </tr>
       
        <tr>
          <td align="center" valign="top">
              &nbsp;</td>
      </tr>
       
        <tr>
          <td align="center" valign="top">
              <asp:Button runat="server" Text="Save" CssClass="buttonRedial" Width="100px" ID="cmdSave"></asp:Button>
            </td>
      </tr>
       
    </table>
    
</asp:Content>
