﻿
<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="PatientSearch.aspx.vb" Inherits=".PatientSearch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <link rel="stylesheet" type="text/css" href="css/rajchasistyles.css"/>

     <link href="java/css/vtip.css" rel="stylesheet" type="text/css" />
    <!--  <script src="java/jquery.js" type="text/javascript"></script> -->
    <script src="java/vtip.js" type="text/javascript"></script>
    <script src="java/vtip-min.js" type="text/javascript"></script>
   

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>Patient List : รายชื่อผู้รับบริการ
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">รายชื่อผู้รับบริการ</li>
      </ol>
    </section>

<section class="content">   
     <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-users"></i>

              <h3 class="box-title">รายชื่อผู้รับบริการ</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">  
<table width="100%" border="0" cellspacing="2" cellpadding="0">
  <tr>
    <td><table border="0" align="center" cellpadding="0" cellspacing="2">
      <tr>
        <td>เลขบัตรประชาชน/ชื่อ-สกุล :</td>
        <td>
            <asp:TextBox ID="txtSearch" runat="server" Width="250px"></asp:TextBox>
          </td>
        <td>
            <asp:Button ID="cmdSearch" runat="server" Text="ค้นหา" CssClass="buttonFind" />
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="400" valign="top">
              <asp:GridView ID="grdData"  CssClass="table table-hover"
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" 
                  PageSize="15" DataKeyNames="PatientID">
          
            <columns>
                <asp:TemplateField HeaderText="Patient ID">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkPatientID" runat="server" CssClass="grd_item" NavigateUrl='<%# NavigateURL("PatientProfile.aspx", "acttype", "view", "PatientID", DataBinder.Eval(Container.DataItem, "PatientID"))%>' Target="_blank" Text='<%# DataBinder.Eval(Container.DataItem, "PatientID")%>'></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle Width="100px" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ชื่อ-สกุล">
                 <ItemTemplate>
                        <asp:HyperLink ID="lnkName" runat="server" 
                           NavigateUrl='<%# NavigateURL("PatientProfile.aspx", "acttype", "view", "PatientID", DataBinder.Eval(Container.DataItem, "PatientID")) %>' 
                            Target="_blank" 
                            Text='<%# DataBinder.Eval(Container.DataItem,"CustName") %>' 
                            CssClass="grd_item"></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle Width="200px" HorizontalAlign="Left" CssClass="grd_item" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="เลขบัตรประชาชน">
                 <ItemTemplate>
                        <asp:HyperLink ID="lnkCardID" runat="server" 
                            NavigateUrl='<%# NavigateURL("PatientProfile.aspx", "acttype", "view", "PatientID", DataBinder.Eval(Container.DataItem, "PatientID")) %>' 
                            Target="_blank" 
                            Text='<%# FormatCardID(DBNull2Str(DataBinder.Eval(Container.DataItem, "CardID")))%>' 
                            CssClass="grd_item"></asp:HyperLink>
                    </ItemTemplate>
                    <ItemStyle Width="200px" CssClass="grd_item" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:BoundField DataField="Ages" HeaderText="อายุ" >
                <ItemStyle HorizontalAlign="Center" Width="50px" />                </asp:BoundField>
            <asp:BoundField HeaderText="เพศ" DataField="Gender">                      
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="100px" />                      </asp:BoundField>
                <asp:TemplateField HeaderText="รับบริการครั้งล่าสุด">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server"  Text='<%# DateText(String.Concat(DataBinder.Eval(Container.DataItem, "LastServiceDate")))%>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate> 
                        <table cellspacing="2">
                            <tr>
                                <td>
                                     <% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then%>
                                     <div class='vtip' title='เพิ่มกิจกรรม'>   
                                                         
                      <asp:ImageButton ID="imgNew" runat="server"  CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PatientID")%>' 
                                        ImageUrl="images/plus.png"    Height="22px" /></div>

                                      <% End If%>
                                </td>
                                <td> <div class='vtip' title='ประวัติการรับบริการ'> 
                             <asp:ImageButton ID="imgPt" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PatientID")%>' Height="22px" ImageUrl="images/history.png" AlternateText="ประวัติการรับบริการ"  />
                        </div>  </td>
                                <td> <div class='vtip' title='ข้อมูลส่วนตัว'> 
                                 <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PatientID")%>' ImageUrl="images/user.png"  AlternateText="ข้อมูลส่วนตัว" />
                        </div> </td>
                                <td> <div class='vtip' title='ลบ'> 
                                       <asp:ImageButton ID="imgDel"  cssclass="gridbutton"  runat="server" 
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PatientID")%>' 
                                        ImageUrl="images/icon-delete.png" AlternateText="ลบ" />  </div></td>
                            </tr>
                        </table>
                                               
                             
                                                     </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
            </columns>         
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
          </asp:GridView>
        <asp:Panel ID="pnNew" runat="server">
              <table width="50%" border="0" align="center" cellpadding="0" cellspacing="2">
                <tr>
                  <td align="center" class="text13b_blue">ไม่พบผู้รับบริการที่ท่านค้นหา</td>
                </tr>
                <tr>
                  <td align="center">
                      <asp:Button ID="cmdAddNew"  CssClass="buttonSave" runat="server" Text="เพิ่มผู้รับบริการใหม่" />
                      </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                </tr>
              </table></asp:Panel>

    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>

 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    </section>
</asp:Content>
