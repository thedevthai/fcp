﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class A5F

    '''<summary>
    '''ucHistoryYear1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ucHistoryYear1 As Global.ucHistoryYear

    '''<summary>
    '''lblID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblID As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblRefID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRefID As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblLocationID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLocationID As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblLocationName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLocationName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblLocationCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLocationCode As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblLocationProvince control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLocationProvince As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtYear control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtYear As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtServiceDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtServiceDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''RegularExpressionValidator2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator2 As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''txtTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTime As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlPerson control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPerson As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''optPatientFrom control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optPatientFrom As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtFromRemark control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFromRemark As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''optService control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optService As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtServiceRemark control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtServiceRemark As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''optTelepharm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optTelepharm As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtTelepharmacyRemark control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTelepharmacyRemark As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''optTelepharmacyMethod control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optTelepharmacyMethod As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtRecordMethod control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRecordMethod As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtRecordLocation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRecordLocation As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblSEQ control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSEQ As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''optFollowChannel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optFollowChannel As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtFollowRemark control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFollowRemark As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''optFinalResult control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optFinalResult As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtStillSmoke control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtStillSmoke As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblQ5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblQ5 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''dtpFinalStopDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents dtpFinalStopDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''RegularExpressionValidator1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator1 As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''lblCompareLabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCompareLabel As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''optChangeRate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optChangeRate As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''optNicotinEffect control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optNicotinEffect As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''optMedEffect control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optMedEffect As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtRecommend1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRecommend1 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtRecommend2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRecommend2 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''optSocietyEffect control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optSocietyEffect As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtRecommend3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRecommend3 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtRecommend4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRecommend4 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtWeight control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtWeight As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtHeigh control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtHeigh As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtPEFR_Value control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPEFR_Value As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtPEFR_Rate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPEFR_Rate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCo_Value control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCo_Value As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''optStopPlane1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optStopPlane1 As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''xDateStopPlan1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents xDateStopPlan1 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''RegularExpressionValidator3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator3 As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''optStopPlane2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optStopPlane2 As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtCGTargetRateBegin control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCGTargetRateBegin As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCGTargetRateEnd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCGTargetRateEnd As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtTargetDay control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTargetDay As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''xDateStopPlan2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents xDateStopPlan2 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''RegularExpressionValidator4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator4 As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''ddlMed control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlMed As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtMed control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMed As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtFrequency control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFrequency As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtMedQTY control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMedQTY As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblBalanceLabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBalanceLabel As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblBalance control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBalance As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cmdAddMed control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdAddMed As Global.DevExpress.Web.ASPxButton

    '''<summary>
    '''lblNoMed control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNoMed As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''grdData control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdData As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''optNextStep control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optNextStep As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''lblProblem control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProblem As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''optNextCause control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optNextCause As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtCause control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCause As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlTimeService control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlTimeService As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''xDateNextService control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents xDateNextService As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''RegularExpressionValidator5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator5 As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''chkStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkStatus As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''cmdSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdSave As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''grdA05 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdA05 As Global.System.Web.UI.WebControls.GridView
End Class
