﻿Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms

Partial Public Class ReportReceivedCondition
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlO As New OrderController
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then
            LoadProvinceToDDL()

            Dim m, y As Integer
            y = Year(Date.Now)
            m = Month(Date.Now)
            If y < 2500 Then
                y = y + 543
            End If

            txtStartDate.Text = "01/" & m & "/" & y 'Date.Now.AddDays(-30).ToString("dd/MM/yyyy", DateFormat_TH)
            txtEndDate.Text = Date.DaysInMonth(y, m) & "/" & m & "/" & y  'Date.Now.ToString("dd/MM/yyyy", DateFormat_TH)


        End If
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        Response.Redirect("rptReceivedView.aspx?p=" & ddlProvince.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text)
    End Sub
    Private Sub LoadProvinceToDDL()


        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            dt = ctlO.Province_GetInGroup(Request.Cookies("RPTGRP").Value)
        Else
            dt = ctlO.Province_GetInLocation
        End If
         

        ddlProvince.Items.Clear()
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .Items.Add("---ทั้งหมด---")
                If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
                    .Items(0).Value = Request.Cookies("RPTGRP").Value
                Else
                    .Items(0).Value = "0"
                End If
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)("ProvinceName"))
                    .Items(i + 1).Value = dt.Rows(i)("ProvinceID")
                Next
                .SelectedIndex = 0

            End With
        End If
        dt = Nothing
    End Sub
End Class