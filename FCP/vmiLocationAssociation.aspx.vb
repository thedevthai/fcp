﻿
Public Class vmiLocationAssociation
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim ctlUser As New UserController 
    Dim ctlStk As New StoreController

    Dim ctlLct As New LocationController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then
            grdData.PageIndex = 0
            LoadStore()

            LoadLocationAssociation()
            LoadLocationToDDL()
          

        End If
    End Sub

    Private Sub LoadStore()
        dt = ctlStk.store_getall
        With ddlStore
            .Visible = True
            .DataSource = dt
            .DataTextField = "StoreName"
            .DataValueField = "StoreCode"
            .DataBind()
            .SelectedIndex = 0
        End With

        With ddlStoreFind
            .Visible = True
            .DataSource = dt
            .DataTextField = "StoreName"
            .DataValueField = "StoreCode"
            .DataBind()
            .SelectedIndex = 0
        End With

    End Sub

    Private Sub LoadLocationToDDL()
        If txtFindLocation.Text = "" Then
            dt = ctlLct.Location_Get
        Else
            dt = ctlLct.Location_GetBySearch("0", txtFindLocation.Text)
        End If

        ddlLocation.Items.Clear()

        If dt.Rows.Count > 0 Then
            With ddlLocation
                .Visible = True
                .Items.Add("---เลือกร้านยา---")
                .Items(0).Value = 0
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)("LocationName"))
                    .Items(i + 1).Value = dt.Rows(i)("LocationID")
                Next
                .SelectedIndex = 0
            End With
        End If

    End Sub

    Private Sub LoadLocationAssociation()

        dt = ctlStk.LocationAssociation_GetSearch(ddlStoreFind.SelectedValue, txtSearch.Text)

        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()
            End With
        Else
            grdData.DataSource = Nothing
            grdData.Visible = False
        End If
        dt = Nothing

    End Sub

    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSave.Click

        Dim item As Integer 

        Dim status As Integer = 0



        item = ctlStk.LocationAssociation_Save(ddlStore.SelectedValue, txtFindLocation.Text, Session("Username"))

        ctlUser.User_GenLogfile(Session("Username"), ACTTYPE_ADD, "LocationAssociation", "Modifile location association :" & txtFindLocation.Text, ">>Location:" & ddlStore.SelectedValue)


        DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อย")
        LoadLocationAssociation()

    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadLocationAssociation()
    End Sub
    Private Sub grdData_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand

        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"
                    If ctlUser.User_Delete(e.CommandArgument) Then

                        ctlUser.User_GenLogfile(Session("Username"), "DEL", "LocationAssociation", "ลบ store :" & ddlStore.SelectedValue & ">>location:" & txtFindLocation.Text, "")

                        DisplayMessage(Me, "ลบข้อมูลเรียบร้อย")
                    Else
                        DisplayMessage(Me, "ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง")
                    End If

                    LoadLocationAssociation()

            End Select

        End If

    End Sub
    Private Sub EditData(ByVal pID As String)

        dt = ctlStk.LocationAssociation_GetByUID(pID)

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                ddlStore.SelectedValue = .Item("StoreCode")
                txtFindLocation.Text = .Item("LocationID")

                ddlLocation.Enabled = False
                lnkFindLocation.Enabled = False
            End With
        Else
            ddlLocation.Enabled = True
            lnkFindLocation.Enabled = True
        End If

        dt = Nothing
    End Sub


    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            'e.Row.Cells(0).Text = e.Row.RowIndex + 1
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(7).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If


    End Sub

    Protected Sub lnkFindLocation_Click(sender As Object, e As EventArgs) Handles lnkFindLocation.Click
        LoadLocationToDDL()
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadLocationAssociation()
    End Sub

    Protected Sub ddlGroupFind_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlStoreFind.SelectedIndexChanged
        grdData.PageIndex = 0
        LoadLocationAssociation()
    End Sub

End Class

