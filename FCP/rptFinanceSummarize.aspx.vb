﻿Public Class rptFinanceSummarize
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim dtL As New DataTable

    Dim grp As New OrderController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblStartDate.Text = Request("b")
        lblEndDate.Text = Request("e")
        FagRPT = "SummaryServiceCount"

        Response.Redirect("ReportViewer.aspx?b=" & Request("b") & "&e=" & Request("e") & "&pj=" & Request("pj"))
        'LoadDataToGrid(Request("b"), Request("e"))

        'If Request("ex") = 1 Then
        '    Response.AppendHeader("content-disposition", "attachment;filename=" & ReportsName & ".xls")
        '    Response.Charset = ""
        '    Response.ContentType = "application/vnd.ms-excel"
        'End If

    End Sub
    Private Sub LoadDataToGrid(ByVal Bdate As String, ByVal EDate As String)

        Dim StartDate As String = "0"
        Dim EndDate As String = "0"
        Dim sumCount As Integer = 0

        If Bdate = "" And EDate = "" Then
            StartDate = 0
            EndDate = 0
        Else
            StartDate = ConvertStrDate2DBString(Bdate)
            EndDate = ConvertStrDate2DBString(EDate)
        End If
        'dtL = grp.Order_GetLocationByCondition(StartDate, EndDate)
        dtL = grp.Order_GetLocationByDate(StartDate, EndDate)

        If dtL.Rows.Count > 0 Then

            Dim dr As DataRow = dtL.NewRow()
            dtL.Rows.Add(dr)

            Dim EndRow As Integer
            EndRow = dtL.Rows.Count - 1
            lblCount.Text = dtL.Rows.Count - 1
            With grdData
                .Visible = True
                .DataSource = dtL
                .DataBind()

                For i = 0 To dtL.Rows.Count - 2

                    .Rows(i).Cells(0).Text = i + 1
                    sumCount = 0
                    dt = grp.Order_GetByCondition(String.Concat(dtL.Rows(i)("LocationID")), StrNull2Long(StartDate), StrNull2Long(EndDate))
                    If dt.Rows.Count > 0 Then
                        .Rows(i).Cells(2).Text = DBNull2Str(dt.Rows(0)("LocationName"))
                        .Rows(EndRow).Cells(2).Text = "รวมทั้งหมด"
                        For n = 0 To dt.Rows.Count - 1
                            Select Case DBNull2Str(dt.Rows(n)("ServiceTypeID"))
                                Case FORM_TYPE_ID_F01
                                    .Rows(i).Cells(3).Text = DBNull2Zero(dt.Rows(n)("nCount"))
                                    .Rows(EndRow).Cells(3).Text = StrNull2Zero(.Rows(EndRow).Cells(3).Text) + DBNull2Zero(dt.Rows(n)("nCount"))
                                Case FORM_TYPE_ID_F02
                                    .Rows(i).Cells(4).Text = DBNull2Zero(dt.Rows(n)("nCount"))
                                    .Rows(EndRow).Cells(4).Text = StrNull2Zero(.Rows(EndRow).Cells(4).Text) + DBNull2Zero(dt.Rows(n)("nCount"))
                                Case FORM_TYPE_ID_F03
                                    .Rows(i).Cells(5).Text = DBNull2Zero(dt.Rows(n)("nCount"))
                                    .Rows(EndRow).Cells(5).Text = StrNull2Zero(.Rows(EndRow).Cells(5).Text) + DBNull2Zero(dt.Rows(n)("nCount"))
                                Case FORM_TYPE_ID_F04
                                    .Rows(i).Cells(6).Text = DBNull2Zero(dt.Rows(n)("nCount"))
                                    .Rows(EndRow).Cells(6).Text = StrNull2Zero(.Rows(EndRow).Cells(6).Text) + DBNull2Zero(dt.Rows(n)("nCount"))
                                Case FORM_TYPE_ID_F05
                                    .Rows(i).Cells(7).Text = DBNull2Zero(dt.Rows(n)("nCount"))
                                    .Rows(EndRow).Cells(7).Text = StrNull2Zero(.Rows(EndRow).Cells(7).Text) + DBNull2Zero(dt.Rows(n)("nCount"))
                                Case FORM_TYPE_ID_F06
                                    .Rows(i).Cells(8).Text = DBNull2Zero(dt.Rows(n)("nCount"))
                                    .Rows(EndRow).Cells(8).Text = StrNull2Zero(.Rows(EndRow).Cells(8).Text) + DBNull2Zero(dt.Rows(n)("nCount"))
                                Case FORM_TYPE_ID_F07
                                    .Rows(i).Cells(9).Text = DBNull2Zero(dt.Rows(n)("nCount"))
                                    .Rows(EndRow).Cells(9).Text = StrNull2Zero(.Rows(EndRow).Cells(9).Text) + DBNull2Zero(dt.Rows(n)("nCount"))
                                Case FORM_TYPE_ID_F08
                                    .Rows(i).Cells(10).Text = DBNull2Zero(dt.Rows(n)("nCount"))
                                    .Rows(EndRow).Cells(10).Text = StrNull2Zero(.Rows(EndRow).Cells(10).Text) + DBNull2Zero(dt.Rows(n)("nCount"))
                                Case FORM_TYPE_ID_F09
                                    .Rows(i).Cells(11).Text = DBNull2Zero(dt.Rows(n)("nCount"))
                                    .Rows(EndRow).Cells(11).Text = StrNull2Zero(.Rows(EndRow).Cells(11).Text) + DBNull2Zero(dt.Rows(n)("nCount"))
                                Case FORM_TYPE_ID_F10
                                    .Rows(i).Cells(12).Text = DBNull2Zero(dt.Rows(n)("nCount"))
                                    .Rows(EndRow).Cells(12).Text = StrNull2Zero(.Rows(EndRow).Cells(12).Text) + DBNull2Zero(dt.Rows(n)("nCount"))
                                Case FORM_TYPE_ID_F11
                                    .Rows(i).Cells(13).Text = DBNull2Zero(dt.Rows(n)("nCount"))
                                    .Rows(EndRow).Cells(13).Text = StrNull2Zero(.Rows(EndRow).Cells(13).Text) + DBNull2Zero(dt.Rows(n)("nCount"))
                                Case FORM_TYPE_ID_F11F
                                    .Rows(i).Cells(14).Text = StrNull2Zero(.Rows(i).Cells(14).Text) + DBNull2Zero(dt.Rows(n)("nCount"))
                                    .Rows(EndRow).Cells(14).Text = StrNull2Zero(.Rows(EndRow).Cells(14).Text) + DBNull2Zero(dt.Rows(n)("nCount"))
                                Case FORM_TYPE_ID_F12
                                    .Rows(i).Cells(15).Text = DBNull2Zero(dt.Rows(n)("nCount"))
                                    .Rows(EndRow).Cells(15).Text = StrNull2Zero(.Rows(EndRow).Cells(15).Text) + DBNull2Zero(dt.Rows(n)("nCount"))
                                Case FORM_TYPE_ID_F13
                                    .Rows(i).Cells(16).Text = DBNull2Zero(dt.Rows(n)("nCount"))
                                    .Rows(EndRow).Cells(16).Text = StrNull2Zero(.Rows(EndRow).Cells(16).Text) + DBNull2Zero(dt.Rows(n)("nCount"))
                                Case FORM_TYPE_ID_F14
                                    .Rows(i).Cells(17).Text = DBNull2Zero(dt.Rows(n)("nCount"))
                                    .Rows(EndRow).Cells(17).Text = StrNull2Zero(.Rows(EndRow).Cells(17).Text) + DBNull2Zero(dt.Rows(n)("nCount"))
                                Case FORM_TYPE_ID_F15
                                    .Rows(i).Cells(18).Text = DBNull2Zero(dt.Rows(n)("nCount"))
                                    .Rows(EndRow).Cells(18).Text = StrNull2Zero(.Rows(EndRow).Cells(18).Text) + DBNull2Zero(dt.Rows(n)("nCount"))
                                    'Case FORM_TYPE_ID_F12
                                    '    .Rows(i).Cells(15).Text = DBNull2Zero(dt.Rows(n)("nCount"))
                                    '    .Rows(EndRow).Cells(15).Text = StrNull2Zero(.Rows(EndRow).Cells(15).Text) + DBNull2Zero(dt.Rows(n)("nCount"))

                                Case FORM_TYPE_ID_F16
                                    .Rows(i).Cells(19).Text = DBNull2Zero(dt.Rows(n)("nCount"))
                                    .Rows(EndRow).Cells(19).Text = StrNull2Zero(.Rows(EndRow).Cells(19).Text) + DBNull2Zero(dt.Rows(n)("nCount"))

                                Case FORM_TYPE_ID_F17
                                    .Rows(i).Cells(20).Text = DBNull2Zero(dt.Rows(n)("nCount"))
                                    .Rows(EndRow).Cells(20).Text = StrNull2Zero(.Rows(EndRow).Cells(20).Text) + DBNull2Zero(dt.Rows(n)("nCount"))

                            End Select

                            sumCount = sumCount + DBNull2Zero(dt.Rows(n)("nCount"))
                        Next
                        .Rows(i).Cells(21).Text = sumCount
                        .Rows(EndRow).Cells(21).Text = StrNull2Zero(.Rows(EndRow).Cells(21).Text) + sumCount

                        Dim PayAmount As Double
                        Dim ctlP As New PaymentController
                        PayAmount = ctlP.Payment_GetAmountByLocation(DBNull2Str(dt.Rows(0)("LocationID")), StartDate, 1)

                        .Rows(i).Cells(22).Text = (sumCount * PayAmount).ToString("#,###")
                        .Rows(EndRow).Cells(22).Text = (StrNull2Zero(.Rows(EndRow).Cells(22).Text) + (sumCount * PayAmount)).ToString("#,###")

                    End If
                    dt = Nothing

                Next
                grdData.Rows(EndRow).Font.Bold = True
                grdData.Rows(EndRow).ForeColor = Drawing.Color.Blue
                grdData.Rows(EndRow).BackColor = Drawing.Color.AliceBlue
                grdData.Rows(EndRow).Cells(2).HorizontalAlign = HorizontalAlign.Right
            End With
        Else
            grdData.DataSource = Nothing
            grdData.Visible = False
        End If

        dtL = Nothing
    End Sub

End Class