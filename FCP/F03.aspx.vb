﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO



Public Class F03
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlLct As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlOrder As New OrderController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then
            pnBehavior.Visible = True
            pnAlertB.Visible = False
            If Request("acttype") = "view" Then
                cmdSave.Visible = False
            Else
                cmdSave.Visible = True
            End If
            LoadPharmacist(Request.Cookies("LocationID").Value)
            LoadProblemRelate()
            BindFinalResultToRadioList()
            LoadFormData()
            ' LockControls()

        End If

        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")



    End Sub
   Private Sub LoadPharmacist(LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With
            End If
        End If
        dtP = Nothing
    End Sub

    Private Sub LoadLocationData(LocationID As String)
        dt = ctlLct.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblLocationID.Text = .Item("LocationID")
                lblLocationName.Text = .Item("LocationName")
                lblLocationProvince.Text = .Item("ProvinceName")
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadFormData()
        Dim pYear As Integer
        If Not Request("fid") Is Nothing Then
            dt = ctlOrder.GetOrder_ByID(StrNull2Zero(Request("fid")))
        Else
            If Not Request("yid") Is Nothing Then
                pYear = CInt(Request("yid"))
            Else
                pYear = Year(ctlLct.GET_DATE_SERVER)
                If pYear < 2500 Then
                    pYear = pYear + 543
                End If

            End If
            dt = ctlOrder.LocationOrder_ByYear(pYear, FORM_TYPE_ID_F03, Request.Cookies("LocationID").Value, Session("patientid"))
        End If
        If dt.Rows.Count > 0 Then

            pnBehavior.Visible = True
            With dt.Rows(0)

                If Request("t") = "new" Then
                    lblID.Text = ""
                    lblRefID.Text = String.Concat(.Item("itemID"))
                ElseIf Request("t") = "edit" Then
                    lblID.Text = String.Concat(.Item("itemID"))
                    lblRefID.Text = String.Concat(.Item("RefID"))
                ElseIf Request("t") Is Nothing Then
                    lblID.Text = ""
                    lblRefID.Text = String.Concat(.Item("itemID"))
                End If
                Session("patientid") = String.Concat(.Item("PatientID"))
                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                txtTime.Text = String.Concat(.Item("ServiceTime"))
                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))
                'txtName.Text = DBNull2Str(.Item("CustName"))
                'optGender.SelectedValue = DBNull2Str(.Item("Gender"))
                'txtAges.Text = DBNull2Str(.Item("Ages"))
                'txtCardID.Text = DBNull2Str(.Item("CardID"))
                'txtBirthDate.Text = DisplayStr2ShortDateTH(DBNull2Str(.Item("BirthDate")))
                'txtTelephone.Text = DBNull2Str(.Item("Telephone"))
                'txtMobile.Text = DBNull2Str(.Item("Mobile"))
                'optAddressType.SelectedValue = DBNull2Str(.Item("AddressType"))
                'txtAddress.Text = DBNull2Str(.Item("AddressNo"))
                'txtRoad.Text = DBNull2Str(.Item("Road"))
                'txtDistrict.Text = DBNull2Str(.Item("District"))
                'txtCity.Text = DBNull2Str(.Item("City"))
                'ddlProvince.SelectedValue = DBNull2Str(.Item("ProvinceID"))
                'optClaim.SelectedValue = DBNull2Str(.Item("MainClaim"))
                chkClose.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("Status")))


                chkNotResponse.Checked = ConvertYesNo2Boolean(DBNull2Str(.Item("isNotResponse")))
                optCause.SelectedValue = DBNull2Zero(.Item("CauseResponse")).ToString()
                txtOtherCause.Text = DBNull2Str(.Item("OtherCause"))


                chkProblem1.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem1")))
                chkProblem2.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem2")))
                chkProblem3.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem3")))
                chkProblem4.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem4")))
                chkProblem5.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem5")))
                chkProblem6.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem6")))
                chkProblem7.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblemOther")))
                txtProblemOther.Text = DBNull2Str(.Item("ProblemRemark"))
                chkEdu1.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate1")))
                chkEdu2.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate2")))
                chkEdu3.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate3")))
                chkEdu4.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate4")))
                chkEdu5.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate5")))
                chkEdu6.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate6")))
                chkEdu7.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducateOther")))
                txtEduOther.Text = DBNull2Str(.Item("EducateRemark"))

                lblServicePlan.Text = ""

                Dim sPlan() As String
                sPlan = Split(String.Concat(.Item("ServicePlan")), "|")
                For i = 0 To sPlan.Length - 1
                    Select Case sPlan(i)
                        Case "1"
                            lblServicePlan.Text = "ให้ความรู้เรื่องโรคและพฤติกรรมความเสี่ยงครั้งที่ 1"
                        Case "2"
                            lblServicePlan.Text &= " , ให้ความรู้เรื่องโรคและพฤติกรรมความเสี่ยงครั้งที่ 2"
                        Case "3"
                            lblServicePlan.Text &= " , Refer เพื่อConfirm โดยแพทย์"
                    End Select
                Next


                lblHospitalName.Text = String.Concat(.Item("HospitalName"))
                If DBNull2Zero(.Item("Status")) >= 3 Then
                    cmdSave.Visible = False
                Else
                    If Request.Cookies("LocationID").Value <> String.Concat(.Item("LocationID")) Then
                        LoadLocationData(String.Concat(.Item("LocationID")))
                        If Request("t") = "edit" Then
                            cmdSave.Visible = True
                        Else
                            cmdSave.Visible = False

                        End If
                    Else
                        LoadLocationData(Request.Cookies("LocationID").Value)
                    End If
                End If


                ' lblTimeNo.Text = Request("seq")
                LoadMetabolic(String.Concat(.Item("itemID")))

            End With
            LoadBehaviorProblemToGrid()
        Else
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing

    End Sub

    Private Sub LoadMetabolic(pid As Integer)

        dt = ctlOrder.MetabolicRisk_ByOrderID(StrNull2Zero(pid))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)

                ' lblRefID.Text = DBNull2Str(.Item("itemID"))
                txtBP.Text = DBNull2Str(.Item("BPAVG"))
                txtBP2.Text = DBNull2Str(.Item("BPAVG_2"))
                txtFBS.Text = DBNull2Str(.Item("FBS"))
                txtRandom.Text = DBNull2Str(.Item("Postprandial"))
                txtHourAfter.Text = String.Concat(.Item("TimeAfter"))
                txtRefDate.Text = DBNull2Str(.Item("EducateDate"))
                txtRefTime.Text = DBNull2Str(.Item("EducateTime"))
                optChannal.SelectedValue = DBNull2Str(.Item("ServiceChannel"))

                If DBNull2Str(.Item("Diagnoses")) = "0" Then
                    optResultProcess.SelectedValue = "0"
                ElseIf DBNull2Str(.Item("Diagnoses")) = "1" Then
                    optResultProcess.SelectedValue = "1"

                    Dim sDiseases(3) As String
                    sDiseases(0) = ""
                    sDiseases(1) = ""
                    sDiseases(2) = ""
                    chkDiseases1.Checked = False
                    chkDiseases2.Checked = False
                    chkDiseases3.Checked = False

                    If DBNull2Str(.Item("Diseases")) <> "" Then
                        sDiseases = Split(DBNull2Str(.Item("Diseases")), "|")
                        chkDiseases1.Checked = ConvertYesNo2Boolean(sDiseases(0))
                        chkDiseases2.Checked = ConvertYesNo2Boolean(sDiseases(1))
                        chkDiseases3.Checked = ConvertYesNo2Boolean(sDiseases(2))
                    End If
                    Select Case DBNull2Str(.Item("Diagnoses"))
                        Case "1"
                            chkDiseases1.Checked = True
                        Case "2"
                            chkDiseases2.Checked = True
                        Case "3"
                            chkDiseases3.Checked = True
                    End Select
                Else
                    optResultProcess.SelectedValue = Nothing
                End If
                txtResultRemark.Text = DBNull2Str(.Item("DiagnosesOther"))


                optRefer.SelectedValue = DBNull2Str(.Item("isRefer"))
                txtRemarkRefer.Text = DBNull2Str(.Item("RemarkRefer"))


                If DBNull2Str(.Item("isMedicine")) = "0" Then
                    optIsMed.SelectedValue = "0"
                ElseIf DBNull2Str(.Item("isMedicine")) = "1" Then
                    optIsMed.SelectedValue = "1"
                Else
                    optIsMed.SelectedValue = Nothing
                End If


                Dim sStr(4) As String
                sStr(0) = ""
                sStr(1) = ""
                sStr(2) = ""
                sStr(3) = ""
                sStr(4) = ""

                If DBNull2Str(.Item("MedicineDesc")) <> "" Then
                    sStr = Split(DBNull2Str(.Item("MedicineDesc")), "|")

                    For k = 0 To sStr.Length - 1
                        Select Case k
                            Case 0
                                txtMed1.Text = sStr(0)
                            Case 1
                                txtMed2.Text = sStr(1)
                            Case 2
                                txtMed3.Text = sStr(2)
                            Case 3
                                txtMed4.Text = sStr(3)
                            Case 4
                                txtMed5.Text = sStr(4)
                        End Select
                    Next

                End If
                ReDim sStr(4)
                If DBNull2Str(.Item("DocFile1")) <> "" Then
                    sStr(0) = DBNull2Str(.Item("DocFile1"))
                    sStr(1) = DBNull2Str(.Item("DocFile2"))
                    sStr(2) = DBNull2Str(.Item("DocFile3"))
                    sStr(3) = DBNull2Str(.Item("DocFile4"))
                    sStr(4) = DBNull2Str(.Item("DocFile5"))

                    'If sStr(0) <> "" Then
                    '    If txtMed1.Text <> "" Then
                    '        HyperLink1.NavigateUrl = "~/" & tmpUpload & "/" & sStr(0)
                    '        HyperLink1.Text = sStr(0)
                    '    End If
                    'End If

                    If chkFileExist(Server.MapPath("~/" & tmpUpload & "/" & sStr(0))) Then
                        HyperLink1.NavigateUrl = "~/" & tmpUpload & "/" & sStr(0)
                        HyperLink1.Text = sStr(0)
                    Else
                        HyperLink1.Visible = False
                        HyperLink1.Text = ""
                    End If


                    'If sStr(1) <> "" Then
                    '    If txtMed2.Text <> "" Then
                    '        HyperLink2.NavigateUrl = "~/" & tmpUpload & "/" & sStr(1)
                    '        HyperLink2.Text = sStr(1)
                    '    End If

                    'End If

                    If chkFileExist(Server.MapPath("~/" & tmpUpload & "/" & sStr(1))) Then
                        HyperLink2.NavigateUrl = "~/" & tmpUpload & "/" & sStr(1)
                        HyperLink2.Text = sStr(1)
                    Else
                        HyperLink2.Visible = False
                        HyperLink2.Text = ""
                    End If


                    'If sStr(2) <> "" Then
                    '    If txtMed3.Text <> "" Then
                    '        HyperLink3.NavigateUrl = "~/" & tmpUpload & "/" & sStr(2)
                    '        HyperLink3.Text = sStr(2)
                    '    End If

                    'End If
                    If chkFileExist(Server.MapPath("~/" & tmpUpload & "/" & sStr(2))) Then
                        HyperLink3.NavigateUrl = "~/" & tmpUpload & "/" & sStr(2)
                        HyperLink3.Text = sStr(2)
                    Else
                        HyperLink3.Visible = False
                        HyperLink3.Text = ""
                    End If

                    'If sStr(3) <> "" Then
                    '    If txtMed4.Text <> "" Then
                    '        HyperLink4.NavigateUrl = "~/" & tmpUpload & "/" & sStr(3)
                    '        HyperLink4.Text = sStr(3)
                    '    End If

                    'End If
                    If chkFileExist(Server.MapPath("~/" & tmpUpload & "/" & sStr(3))) Then
                        HyperLink4.NavigateUrl = "~/" & tmpUpload & "/" & sStr(3)
                        HyperLink4.Text = sStr(3)
                    Else
                        HyperLink4.Visible = False
                        HyperLink4.Text = ""
                    End If

                    'If sStr(4) <> "" Then
                    '    If txtMed5.Text <> "" Then
                    '        HyperLink5.NavigateUrl = "~/" & tmpUpload & "/" & sStr(4)
                    '        HyperLink5.Text = sStr(4)
                    '    End If
                    'End If

                    If chkFileExist(Server.MapPath("~/" & tmpUpload & "/" & sStr(4))) Then
                        HyperLink5.NavigateUrl = "~/" & tmpUpload & "/" & sStr(4)
                        HyperLink5.Text = sStr(4)
                    Else
                        HyperLink5.Visible = False
                        HyperLink5.Text = ""
                    End If


                End If

            End With

        End If

        dt = Nothing
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        'If Trim(txtMobile.Text) = "" Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนเบอร์มือถือผู้รับบริการก่อน หากไม่มีให้ใส่เบอร์บ้านในช่องเบอร์มือถือได้")
        '    Exit Sub
        'End If

        If StrNull2Zero(txtTime.Text) = 0 Then
            DisplayMessage(Me.Page, "กรุณาป้อนระยะเวลาในการให้บริการก่อน")
            'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนระยะเวลาในการให้บริการก่อน');", True)
            Exit Sub
        End If


        Dim objuser As New UserController
        Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))
        ' 'Dim BirthDate As String = ConvertStrDate2DBString(txtBirthDate.Text)


        If lblID.Text = "" Then

            If ctlOrder.Order_ChkDupCustomer(FORM_TYPE_ID_F03, Session("patientid"), StrNull2Zero(Left(Str(ServiceDate), 4))) = True Then
                DisplayMessage(Me.Page, "ผู้รับบริการรายนี้เคยได้รับบริการในกิจกรรมนี้แล้ว")
                'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','');", True)
                Exit Sub
            End If

            ctlOrder.F02_Add(lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F03, StrNull2Zero(lblRefID.Text), 2, 2, Boolean2Decimal(chkProblem1.Checked), Boolean2Decimal(chkProblem2.Checked), Boolean2Decimal(chkProblem3.Checked), Boolean2Decimal(chkProblem4.Checked), Boolean2Decimal(chkProblem5.Checked), Boolean2Decimal(chkProblem6.Checked), Boolean2Decimal(chkProblem7.Checked), txtProblemOther.Text, Boolean2Decimal(chkEdu1.Checked), Boolean2Decimal(chkEdu2.Checked), Boolean2Decimal(chkEdu3.Checked), Boolean2Decimal(chkEdu4.Checked), Boolean2Decimal(chkEdu5.Checked), Boolean2Decimal(chkEdu6.Checked), Boolean2Decimal(chkEdu7.Checked), txtEduOther.Text, Convert2Status(chkClose.Checked), Request.Cookies("username").Value, CLng(ConvertDate2DBString(ctlLct.GET_DATE_SERVER)), 0, 0, 0, 0, 0, 0, 0, 0, Session("patientid"), Session("sex"), Session("age"), ConvertStatus2YN(chkNotResponse.Checked), optCause.SelectedValue, txtOtherCause.Text)

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Service_Order,Metabolic_Risk", "การให้ความรู้และคำแนะนำปรึกษาในกลุ่ม Metabolic Syndrome (F03) :" & Session("patientname") & "ID:" & lblID.Text, "F03")

            ctlOrder.F01_UpdateEducateCount(StrNull2Zero(lblID.Text), 2)

        Else
            ctlOrder.F02_Update(StrNull2Zero(lblID.Text), lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F03, StrNull2Zero(lblRefID.Text), 2, 2, Boolean2Decimal(chkProblem1.Checked), Boolean2Decimal(chkProblem2.Checked), Boolean2Decimal(chkProblem3.Checked), Boolean2Decimal(chkProblem4.Checked), Boolean2Decimal(chkProblem5.Checked), Boolean2Decimal(chkProblem6.Checked), Boolean2Decimal(chkProblem7.Checked), txtProblemOther.Text, Boolean2Decimal(chkEdu1.Checked), Boolean2Decimal(chkEdu2.Checked), Boolean2Decimal(chkEdu3.Checked), Boolean2Decimal(chkEdu4.Checked), Boolean2Decimal(chkEdu5.Checked), Boolean2Decimal(chkEdu6.Checked), Boolean2Decimal(chkEdu7.Checked), txtEduOther.Text, Convert2Status(chkClose.Checked), Request.Cookies("username").Value, 0, 0, 0, 0, 0, 0, 0, 0, Session("patientid"), ConvertStatus2YN(chkNotResponse.Checked), optCause.SelectedValue, txtOtherCause.Text)

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Service_Order,Metabolic_Risk", "การให้ความรู้และคำแนะนำปรึกษาในกลุ่ม Metabolic Syndrome (F03) :" & Session("patientname") & "ID:" & lblID.Text, "F03")
        End If


        Dim ServiceID As Integer = ctlOrder.F02_GetID(lblLocationID.Text, FORM_TYPE_ID_F03, Session("patientid"), ServiceDate, 2)

        Dim DocFile1, DocFile2, DocFile3, DocFile4, DocFile5, MedDesc As String 'sFile

        DocFile1 = FORM_TYPE_ID_F03 & "_" & ServiceDate & "_" & ConvertTimeToString(Now()) & "_" & ServiceID
        DocFile2 = DocFile1
        DocFile3 = DocFile1
        DocFile4 = DocFile1
        DocFile5 = DocFile1
        If FileUpload1.HasFile Then
            DocFile1 = DocFile1 & "_1" & Path.GetExtension(FileUpload1.PostedFile.FileName)
        Else
            DocFile1 = HyperLink1.Text
        End If

        If FileUpload2.HasFile Then
            DocFile2 = DocFile2 & "_2" & Path.GetExtension(FileUpload2.PostedFile.FileName)
        Else
            DocFile2 = HyperLink2.Text
        End If

        If FileUpload3.HasFile Then
            DocFile3 = DocFile3 & "_3" & Path.GetExtension(FileUpload3.PostedFile.FileName)
        Else
            DocFile3 = HyperLink3.Text
        End If

        If FileUpload4.HasFile Then
            DocFile4 = DocFile4 & "_4" & Path.GetExtension(FileUpload4.PostedFile.FileName)
        Else
            DocFile4 = HyperLink4.Text
        End If

        If FileUpload5.HasFile Then
            DocFile5 = DocFile5 & "_5" & Path.GetExtension(FileUpload5.PostedFile.FileName)
        Else
            DocFile5 = HyperLink5.Text
        End If

        MedDesc = ""
        If Trim(txtMed1.Text) <> "" Then
            MedDesc = txtMed1.Text & "|"
        Else
            DocFile1 = ""
        End If

        If Trim(txtMed2.Text) <> "" Then
            MedDesc &= txtMed2.Text & "|"
        Else
            DocFile2 = ""
        End If

        If Trim(txtMed3.Text) <> "" Then
            MedDesc &= txtMed3.Text & "|"
        Else
            DocFile3 = ""
        End If

        If Trim(txtMed4.Text) <> "" Then
            MedDesc &= txtMed4.Text & "|"
        Else
            DocFile4 = ""
        End If

        If Trim(txtMed5.Text) <> "" Then
            MedDesc &= txtMed5.Text
        Else
            DocFile5 = ""
        End If
        Dim Diseases As String = ""

        If optResultProcess.SelectedValue = "1" Then
            If chkDiseases1.Checked Then
                Diseases = "Y|"
            Else
                Diseases = "N|"
            End If
            If chkDiseases2.Checked Then
                Diseases &= "Y|"
            Else
                Diseases &= "N|"
            End If
            If chkDiseases3.Checked Then
                Diseases &= "Y"
            Else
                Diseases &= "N"
            End If
        End If

        ctlOrder.F03_SaveMetabolicRisk(ServiceID, StrNull2Zero(lblRefID.Text), FORM_TYPE_ID_F03, StrNull2Zero(txtBP.Text), StrNull2Zero(txtBP2.Text), Str2Double(txtFBS.Text), Str2Double(txtRandom.Text), Str2Double(txtHourAfter.Text), txtRefDate.Text, txtRefTime.Text, optChannal.SelectedValue, optResultProcess.SelectedValue, txtResultRemark.Text, optIsMed.SelectedValue, MedDesc, DocFile1, DocFile2, DocFile3, DocFile4, DocFile5, 2, Request.Cookies("username").Value, Diseases, optRefer.SelectedValue, txtRemarkRefer.Text)

        UploadFile(FileUpload1, DocFile1)
        UploadFile(FileUpload2, DocFile2)
        UploadFile(FileUpload3, DocFile3)
        UploadFile(FileUpload4, DocFile4)
        UploadFile(FileUpload5, DocFile5)

        ctlOrder.F01_UpdateEducateCount(StrNull2Zero(lblRefID.Text), 2)
        ctlOrder.F01_UpdateCloseStatus(StrNull2Zero(lblRefID.Text), Convert2Status(chkClose.Checked), Request.Cookies("username").Value)

        Response.Redirect("ResultPage.aspx")

        ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย")
    End Sub
    Sub UploadFile(ByVal Fileupload As Object, sName As String)
        Dim FileFullName As String = Fileupload.PostedFile.FileName
        Dim FileNameInfo As String = Path.GetFileName(FileFullName)
        Dim filename As String = Path.GetFileName(Fileupload.PostedFile.FileName)
        Dim objfile As FileInfo = New FileInfo(Server.MapPath(tmpUpload))
        If FileNameInfo <> "" Then
            'Fileupload.PostedFile.SaveAs(objfile.DirectoryName & "\" & filename)
            'sFile = Path.GetExtension(FileFullName)
            Fileupload.PostedFile.SaveAs(objfile.DirectoryName & "\" & tmpUpload & "\" & sName)
        End If
        objfile = Nothing
    End Sub

    'Private Sub txtBirthDate_TextChanged(sender As Object, e As System.EventArgs) Handles txtBirthDate.TextChanged

    '    Try
    '        txtAges.Text = DateDiff(DateInterval.Year, CDate(txtBirthDate.Text), ctlLct.GET_DATE_SERVER.Date)
    '        txtCardID.Focus()
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Protected Sub optResultProcess_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optResultProcess.SelectedIndexChanged
        If optResultProcess.SelectedIndex = 0 Then
            chkDiseases1.Visible = False
            chkDiseases2.Visible = False
            chkDiseases3.Visible = False
            txtResultRemark.Visible = False
        Else
            chkDiseases1.Visible = True
            chkDiseases2.Visible = True
            chkDiseases3.Visible = True
            txtResultRemark.Visible = True
        End If
    End Sub


#Region "ปัญหาพฤติกรรม"

    Private Sub LoadProblemRelate()
        Dim dtPG As New DataTable
        Dim ctlP As New ProblemController
        ddlBehavior.Items.Clear()
        dtPG = ctlP.BehaviorProblem_Get
        If dtPG.Rows.Count > 0 Then
            ddlBehavior.DataSource = dtPG
            ddlBehavior.DataTextField = "Descriptions"
            ddlBehavior.DataValueField = "UID"
            ddlBehavior.DataBind()
        End If
        dtPG = Nothing
    End Sub

    Private Sub BindFinalResultToRadioList()
        If chkNotFollow.Checked Then
            'optFat.Enabled = False
            optResultB.Enabled = False
            txtResultOtherB.Enabled = False
            txtBegin.Enabled = False
            txtEnd.Enabled = False
        Else
            optFat.Enabled = True
            optResultB.Enabled = True
            txtResultOtherB.Enabled = True
            txtBegin.Enabled = True
            txtEnd.Enabled = True
            optResultB.Items.Clear()
            pnFat.Visible = False
            pnEating.Visible = False
            Select Case ddlBehavior.SelectedValue
                Case "1" 'การสูบบุหรี่
                    optResultB.Items.Add("สูบลดลง")
                    optResultB.Items.Add("สูบเท่าเดิม")
                    optResultB.Items.Add("สูบเพิ่มขึ้น")
                    optResultB.Items.Add("เลิกสูบแล้ว")
                    optResultB.Items.Add("อื่นๆ")
                Case "2" 'การดื่มเหล้า
                    optResultB.Items.Add("ดื่มลดลง")
                    optResultB.Items.Add("ดื่มเพิ่มขึ้น")
                    optResultB.Items.Add("คงเดิม")
                    optResultB.Items.Add("อื่นๆ")
                Case "3" 'ความอ้วน
                    pnFat.Visible = True
                    optResultB.Items.Add("ลดลง")
                    optResultB.Items.Add("เพิ่มขึ้น")
                    optResultB.Items.Add("คงเดิม")
                    optResultB.Items.Add("อื่นๆ")
                Case "4" 'การออกกำลังกาย
                    optResultB.Items.Add("ไม่ได้ออกกำลังกายเพิ่ม(เหมือนเดิม)")
                    optResultB.Items.Add("ออกกำลังกายเพิ่มขึ้น")
                    optResultB.Items.Add("ออกกำลังกายลดลง")
                    optResultB.Items.Add("จำนวนครั้งเท่าเดิม แต่เพิ่มเวลาในแต่ละครั้ง")
                    optResultB.Items.Add("อื่นๆ")
                Case "5" 'การรับประทานอาหาร

                    pnEating.Visible = True
                    optResultB.Items.Add("ดีขึ้น")
                    optResultB.Items.Add("แย่ลง")
                    optResultB.Items.Add("เท่าเดิม")
                    optResultB.Items.Add("อื่นๆ")

                    txtBegin.Enabled = False
                    txtEnd.Enabled = False
                Case "6" 'ความเครียด
                    optResultB.Items.Add("ลดลง")
                    optResultB.Items.Add("เพิ่มขึ้น")
                    optResultB.Items.Add("คงเดิม")
                    optResultB.Items.Add("อื่นๆ")
                Case Else
                    optResultB.Items.Add("ลดลง")
                    optResultB.Items.Add("เพิ่มขึ้น")
                    optResultB.Items.Add("คงเดิม")
                    optResultB.Items.Add("อื่นๆ")
            End Select
        End If
    End Sub

    Protected Sub ddlBehavior_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBehavior.SelectedIndexChanged
        BindFinalResultToRadioList()
    End Sub

    Protected Sub cmdAddBehavior_Click(sender As Object, e As EventArgs) Handles cmdAddBehavior.Click
        If txtInterventionB.Text = "" Then
            lblAlertB.Text = "ท่านยังไม่ได้ป้อน Interventions"
            lblAlertB.Visible = True
            pnAlertB.Visible = True
            Exit Sub
        End If

        Dim isFollow, ResultB, ResultOtherB, ResultBegin, ResultEnd As String
        If chkNotFollow.Checked Then ' not follow
            isFollow = "N"
            ResultB = ""
            ResultOtherB = ""
            ResultBegin = ""
            ResultEnd = ""
        Else
            isFollow = "Y"
            ResultB = optResultB.SelectedValue
            ResultOtherB = txtResultOtherB.Text
            ResultBegin = txtBegin.Text
            ResultEnd = txtEnd.Text
        End If

        ctlOrder.BehaviorRelate_Save(StrNull2Zero(lblID.Text), StrNull2Zero(lblUID_B.Text), ddlBehavior.SelectedValue, txtProblemBehaviorOther.Text, txtInterventionB.Text, ResultB, ResultOtherB, ResultBegin, ResultEnd, optFat.SelectedValue, ResultOtherB, isFollow, Request.Cookies("UserID").Value, FORM_TYPE_ID_F03, StrNull2Zero(Session("patientid")))

        LoadBehaviorProblemToGrid()
        ClearTextBehavior()

        pnAlertB.Visible = False
    End Sub
    Private Sub LoadBehaviorProblemToGrid()
        dt = ctlOrder.BehaviorRelate_Get(StrNull2Zero(Session("patientid")), FORM_TYPE_ID_F03)
        grdBehavior.DataSource = dt
        grdBehavior.DataBind()

        For i = 0 To dt.Rows.Count - 1
            If String.Concat(dt.Rows(i)("ServiceDate")) = "" Then
                grdBehavior.Rows(i).Cells(1).Text = txtServiceDate.Text
            End If
        Next
    End Sub

    Private Sub ClearTextBehavior()
        lblUID_B.Text = "0"
        ddlBehavior.SelectedIndex = 0
        BindFinalResultToRadioList()

        txtProblemBehaviorOther.Text = ""
        txtInterventionB.Text = ""
        txtResultOtherB.Text = ""
        txtBegin.Text = ""
        txtEnd.Text = ""
        chkNotFollow.Checked = False
        cmdAddBehavior.Text = "เพิ่ม"
    End Sub
    Private Sub grdBehavior_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdBehavior.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit_B"
                    EditBehaviorProblem(e.CommandArgument())
                Case "imgDel_B"
                    ctlOrder.BehaviorRelate_Delete(e.CommandArgument())
                    LoadBehaviorProblemToGrid()
                    ClearTextBehavior()
            End Select
        End If
    End Sub
    Protected Sub grdBehavior_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdBehavior.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(6).FindControl("imgDel_B")
            imgD.Attributes.Add("onClick", scriptString)

            'e.Row.Cells(0).Text = e.Row.RowIndex + 1
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub
    Private Sub EditBehaviorProblem(UID As Integer)
        dt = ctlOrder.BehaviorRelate_GetByUID(UID)
        If dt.Rows.Count > 0 Then

            lblUID_B.Text = dt.Rows(0)("UID")
            ddlBehavior.SelectedValue = DBNull2Str(dt.Rows(0)("ProblemUID"))
            BindFinalResultToRadioList()
            txtProblemBehaviorOther.Text = DBNull2Str(dt.Rows(0)("ProblemOther"))
            txtInterventionB.Text = DBNull2Str(dt.Rows(0)("Interventions"))
            txtBegin.Text = DBNull2Str(dt.Rows(0)("ResultBegin"))
            txtEnd.Text = DBNull2Str(dt.Rows(0)("ResultEnd"))
            Select Case ddlBehavior.SelectedValue
                'Case "1" 'การสูบบุหรี่

                'Case "2" 'การดื่มเหล้า

                Case "3" 'ความอ้วน
                    pnFat.Visible = True
                    optFat.SelectedValue = DBNull2Str(dt.Rows(0)("FatFollow"))
                Case "4" 'การรับประทานอาหาร
                    txtResultOtherB.Text = DBNull2Str(dt.Rows(0)("Remark"))
                    txtBegin.Enabled = False
                    txtEnd.Enabled = False
                    'Case "5" 'การออกกำลังกาย

                    'Case "6" 'ความเครียด

            End Select

            If DBNull2Str(dt.Rows(0)("isFollow")) = "N" Then
                chkNotFollow.Checked = True
            Else
                chkNotFollow.Checked = False
                If DBNull2Str(dt.Rows(0)("FinalResult")) <> "" Then
                    optResultB.SelectedValue = DBNull2Str(dt.Rows(0)("FinalResult"))
                End If
                txtResultOtherB.Text = DBNull2Str(dt.Rows(0)("FinalResultOther"))
            End If

            cmdAddBehavior.Text = "บันทึก"
        End If
    End Sub

    Protected Sub chkNotFollow_CheckedChanged(sender As Object, e As EventArgs) Handles chkNotFollow.CheckedChanged
        BindFinalResultToRadioList()
    End Sub
#End Region



End Class