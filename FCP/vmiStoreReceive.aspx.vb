﻿
Public Class vmiStoreReceive
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim ctlUser As New UserController
    Dim ctlStk As New StoreController
    Dim ctlDtb As New DistributionTypeController
    Dim ctlInv As New InventoryController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then
            grdData.PageIndex = 0
            dtpDate.Value = Now.Date()
            LoadStore()
            LoadMedicineToDDL()
            LoadDistributionTypeToDDL()
            LoadTransactionLine()
        End If
    End Sub

    Private Sub LoadStore()
        dt = ctlStk.Store_GetAll
        With ddlStore
            .Visible = True
            .DataSource = dt
            .DataTextField = "StoreName"
            .DataValueField = "StoreCode"
            .DataBind()
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub LoadDistributionTypeToDDL()
        dt = ctlDtb.DistributionType_GetByDomainUID(1)

        ddlType.Items.Clear()

        If dt.Rows.Count > 0 Then
            With ddlType
                .Visible = True
                .Items.Add("")
                .Items(0).Value = ""
            End With

            For i = 0 To dt.Rows.Count - 1
                ddlType.Items.Add(dt.Rows(i)("Name"))
                ddlType.Items(i + 1).Value = dt.Rows(i)("Code")
            Next
            ddlType.SelectedIndex = 0
        End If

    End Sub

    Private Sub LoadMedicineToDDL()
        Dim ctlMed As New SmokingController
        Dim dtM As New DataTable

        dtM = ctlMed.Medicine_Get4VMI

        ddlMedicine.Items.Clear()

        If dtM.Rows.Count > 0 Then
            With ddlMedicine
                .Visible = True
                .Items.Add("")
                .Items(0).Value = "0"

                For i = 0 To dtM.Rows.Count - 1
                    .Items.Add(dtM.Rows(i)("itemName"))
                    .Items(i + 1).Value = dtM.Rows(i)("itemID")

                Next
                .SelectedIndex = 0
            End With
        End If
        dtM = Nothing
    End Sub


    Private Sub LoadTransactionLine()

        dt = ctlInv.Store_Inventory_GetLine(ddlStore.SelectedValue, ddlType.Text, ddlMedicine.SelectedValue)

        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()
            End With
        Else
            grdData.DataSource = Nothing
            grdData.Visible = False
        End If
        dt = Nothing

    End Sub

    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSave.Click

        If StrNull2Zero(txtQTY.Text) = 0 Then
            DisplayMessage(Me.Page, "กรุณาระบุ QTY")
            Exit Sub
        End If

        If ddlType.SelectedIndex = 0 Then
            DisplayMessage(Me.Page, "ท่านต้องเลือก Distribution Type ก่อน")
            Exit Sub
        End If


        Dim item As Integer
        Dim invDate As String
        invDate = ConvertFormateDate(dtpDate.Value)

        item = ctlInv.Store_Inventory_Add(ddlStore.SelectedValue, ddlMedicine.SelectedValue, invDate, ddlType.SelectedValue, txtQTY.Text, Session("Username"))

        ctlUser.User_GenLogfile(Session("Username"), ACTTYPE_ADD, "LocationAssociation", "Modifile location association :" & txtQTY.Text, ">>Location:" & ddlStore.SelectedValue)


        DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อย")
        LoadTransactionLine()

    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadTransactionLine()
    End Sub
    Private Sub grdData_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand

        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"
                    If ctlInv.Store_Inventory_Delete(e.CommandArgument, Session("Username")) Then

                        ctlUser.User_GenLogfile(Session("Username"), "DEL", "StoreInventory", "ลบ store inventory :" & ddlStore.SelectedValue & ">>Med:" & ddlMedicine.SelectedValue & ":" & ddlMedicine.SelectedItem.Text, "")

                        DisplayMessage(Me, "ลบข้อมูลเรียบร้อย")
                    Else
                        DisplayMessage(Me, "ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง")
                    End If

                    LoadTransactionLine()

            End Select

        End If

    End Sub
    Private Sub EditData(ByVal pID As String)

        dt = ctlStk.LocationAssociation_GetByUID(pID)

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                ddlStore.SelectedValue = .Item("StoreCode")
                txtQTY.Text = .Item("LocationID")
            End With
        End If

        dt = Nothing
    End Sub


    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            'e.Row.Cells(0).Text = e.Row.RowIndex + 1
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(6).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If


    End Sub
 

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadTransactionLine()
    End Sub
     

End Class

