﻿Imports System.Net.Mail
Imports System.Net

Public Class RegisterDetail
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlM As New MasterController
    Dim ctlL As New LocationController
    Dim enc As New CryptographyEngine
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If IsNothing(Request.Cookies("CPAQA")) Then
        '    Response.Redirect("Default.aspx")
        'End If
        If Not IsPostBack Then
            hdUID.Value = 0
            cmdSendMail.Visible = False
            LoadProvinceToDDL()
            LoadLocationGroupToDDL()
            LoadLocationTypeToCheckList()

            If Not Request("id") = Nothing Then
                LoadRegisterData()
            End If
        End If
        txtQAYear.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        'txtArea1.Attributes.Add("OnKeyPress", "return AllowOnlyDouble();")
        'txtArea2.Attributes.Add("OnKeyPress", "return AllowOnlyDouble();")
        txtZipCode.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtEmail.Attributes.Add("OnKeyPress", "return NotAllowThai();")

        Dim scriptString As String = "javascript:return confirm(""คุณแน่ใจที่จะลบรายการนี้ใช่หรือไม่?"");"
        cmdDelete.Attributes.Add("onClick", scriptString)

    End Sub
    Function checkField(tD As DataTable, ColumnName As String) As String
        If dt.Columns(ColumnName) IsNot Nothing Then
            Return String.Concat(dt.Rows(0)(ColumnName))
        Else
            Return ""
        End If
    End Function

    Private Sub LoadProvinceToDDL()
        dt = ctlM.Province_Get
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .DataSource = dt
                .DataTextField = "ProvinceName"
                .DataValueField = "ProvinceID"
                .DataBind()
                '.SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadLocationGroupToDDL()
        dt = ctlL.LocationGroup_Get
        If dt.Rows.Count > 0 Then
            With ddlGroup
                .Enabled = True
                .DataSource = dt
                .DataTextField = "GroupName"
                .DataValueField = "Code"
                .DataBind()
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadLocationTypeToCheckList()
        dt = ctlL.LocationType_Get
        If dt.Rows.Count > 0 Then
            optLocationType.DataSource = dt
            optLocationType.DataTextField = "Name"
            optLocationType.DataValueField = "UID"
            optLocationType.DataBind()
        End If
        dt = Nothing
    End Sub

    Private Sub LoadRegisterData()
        Dim dtE As New DataTable
        dtE = ctlL.Register_GetByUID(Request("id"))
        If dtE.Rows.Count > 0 Then
            With dtE.Rows(0)
                hdUID.Value = Request("id")
                txtLicenseNo.Text = String.Concat(.Item("LicenseNo"))
                txtNHSOCode.Text = String.Concat(.Item("NHSOCode"))
                txtLocationName.Text = DBNull2Str(.Item("LocationName"))
                txtSpecName.Text = DBNull2Str(.Item("LocationName2"))
                ddlGroup.SelectedValue = DBNull2Str(.Item("LocationGroupID"))
                optLocationType.SelectedValue = DBNull2Str(.Item("LocationType"))
                txtQAYear.Text = DBNull2Str(.Item("RegisYear"))
                txtAddressNo.Text = DBNull2Str(.Item("Address"))
                ddlProvince.SelectedValue = DBNull2Str(.Item("ProvinceID"))
                txtZipCode.Text = DBNull2Str(.Item("ZipCode"))
                txtTel.Text = DBNull2Str(.Item("Office_Tel"))
                txtLineID.Text = DBNull2Str(.Item("LineID"))
                txtEmail.Text = DBNull2Str(.Item("Office_Mail"))
                txtCo_Name.Text = DBNull2Str(.Item("Co_Name"))
                txtCo_LicenseNo.Text = DBNull2Str(.Item("Co_LicenseNo"))
                txtCo_Mail.Text = DBNull2Str(.Item("Co_Mail"))
                txtCo_Tel.Text = DBNull2Str(.Item("Co_Tel"))

                txtWorkTime.Text = DBNull2Str(.Item("Office_Hour"))

                If String.Concat(.Item("Lng")) <> "" Then
                    txtLat.Text = String.Concat(.Item("Lat")) & "," & String.Concat(.Item("Lng"))
                Else
                    txtLat.Text = String.Concat(.Item("Lat"))
                End If

                If String.Concat(.Item("LocationID")) <> "" Then
                    txtLocationID.Text = String.Concat(.Item("LocationID"))
                    txtPassword.Text = enc.DecryptString(String.Concat(.Item("Passwords")), True)
                    cmdSendMail.Visible = True
                    cmdSave.Visible = False


                    chkProject.ClearSelection()
                    Dim dtLP As New DataTable
                    Dim ctlUser As New UserController
                    dtLP = ctlUser.LocationProject_GetByLocationID(txtLocationID.Text)

                    If dtLP.Rows.Count > 0 Then
                        For i = 0 To dtLP.Rows.Count - 1
                            Select Case dtLP.Rows(i)("ProjectID")
                                Case "1"
                                    chkProject.Items(0).Selected = True
                                Case "2"
                                    chkProject.Items(1).Selected = True
                                Case "3"
                                    chkProject.Items(2).Selected = True
                                Case "4"
                                    chkProject.Items(3).Selected = True
                            End Select
                        Next
                    End If
                    dtLP = Nothing
                End If
            End With
        End If
        dtE = Nothing
    End Sub
    Function Check_Email(str As String) As Boolean
        Return Regex.IsMatch(str, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
    End Function
    Function GenLocationNumber() As String
        Return ddlGroup.SelectedValue & ddlProvince.SelectedValue & ctlM.genRunningNumber(ddlGroup.SelectedValue, ddlProvince.SelectedValue)
    End Function
    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If txtLicenseNo.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรอกเลขที่เลขที่ใบอนุญาต (ขย.) /เลข ภ. ก่อน');", True)
            Exit Sub
        End If
        If StrNull2Zero(optLocationType.SelectedValue) <= 3 Then
            If ctlL.Location_SearchByLicense(txtLicenseNo.Text) > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','เลขที่ใบอนุญาตนี้มี User ในระบบแล้ว กรุณา Login ด้วยรหัสผู้ใช้งานของท่าน');", True)
                Exit Sub
            End If

            If IsNumeric(Left(txtLicenseNo.Text, 1)) Or IsNumeric(Mid(txtLicenseNo.Text, 2, 1)) Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุเลขที่ใบอนุญาตขายยา (ขย.5) 2 ตัวแรกด้วยรหัสจังหวัด');", True)
                Exit Sub
            End If

            If Len(Trim(txtLicenseNo.Text)) < 8 Or Len(Trim(txtLicenseNo.Text)) > 12 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุเลขที่ใบอนุญาตขายยา (ขย.5) ให้ถูกต้อง');", True)
                Exit Sub
            End If
        End If

        If txtLocationName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุชื่อร้านยา');", True)
            Exit Sub
        End If
        If txtAddressNo.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุบ้านเลขที่/เลขที่ตั้ง');", True)
            Exit Sub
        End If
        If txtZipCode.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุรหัสไปรษณีย์');", True)
            Exit Sub
        End If
        If txtTel.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุเบอร์โทร');", True)
            Exit Sub
        End If
        If txtEmail.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุอีเมล');", True)
            Exit Sub
        End If
        If Check_Email(txtEmail.Text) = False Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','รูปแบบอีเมลไม่ถูกต้อง กรุณาตรวจสอบ');", True)
            Exit Sub
        End If

        If txtCo_Name.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุชื่อเจ้าของ(คู่สัญญา)');", True)
            Exit Sub
        End If

        If chkProject.Items(0).Selected = False And chkProject.Items(1).Selected = False And chkProject.Items(2).Selected = False And chkProject.Items(3).Selected = False Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','เลือกโครงการที่จะให้สิทธิ์ก่อน');", True)
            Exit Sub
        End If

        Dim sLat, sLng, sL() As String
        sLat = ""
        sLng = ""
        If txtLat.Text <> "" Then
            sL = Split(Replace(txtLat.Text, " ", ""), ",")
            sLat = sL(0)
            If sL.Length > 1 Then
                sLng = sL(1)
            End If

            If IsNumeric(sLat) = False Or IsNumeric(sLng) = False Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุละติจูด/ลองติจูด เป็นรูปแบบองศาทศนิยมเท่านั้น ');", True)
                Exit Sub
            End If
        End If

        Dim Generator As System.Random = New System.Random()
        Dim sPassword As String = Generator.Next(1000, 9999).ToString()
        Dim enc As New CryptographyEngine
        Dim ctlU As New UserController
        Dim k As Integer
        Dim sLocationID As String = ""
        sLocationID = GenLocationNumber()

        ctlL.Register_Approve(StrNull2Zero(hdUID.Value), sLocationID, txtLocationName.Text, txtSpecName.Text, ddlGroup.SelectedValue, optLocationType.SelectedValue, txtAddressNo.Text, ddlProvince.SelectedValue, ddlProvince.SelectedItem.Text, txtZipCode.Text, txtTel.Text, txtEmail.Text, txtLineID.Text, txtCo_Name.Text, txtCo_Mail.Text, txtCo_Tel.Text, txtQAYear.Text, txtLicenseNo.Text, txtNHSOCode.Text, sLat, sLng, Request.Cookies("username").Value)

        'ctlL.Register_UpdateStatus(hdUID.Value, 2)

        ctlU.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Locations", "เพิ่มใหม่ ร้านยา/เภสัชกร/รพ.:" & txtLocationName.Text, "")

        k = ctlU.User_Add(sLocationID, enc.EncryptString(sPassword, True), txtLocationName.Text, ddlProvince.SelectedItem.Text, 0, 1, 1, sLocationID, "", 0, txtCo_Mail.Text)

        ctlU.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Users", "add new user :" & sLocationID, "Result:" & k)

        ctlU.RunningNumber_Update(ddlGroup.SelectedValue, ddlProvince.SelectedValue)


        For i = 0 To chkProject.Items.Count - 1
            ctlU.User_AddLocationProject(i + 1, sLocationID, Boolean2StatusFlag(chkProject.Items(i).Selected))
        Next

        ctlU.User_GenLogfile(0, ACTTYPE_ADD, "Locations", "สร้าง User ใหม่จากการลงทะเบียน", "[LicenseNo=" & txtLicenseNo.Text & "]")
        SendEmail(txtLocationName.Text, sLocationID, sPassword, txtCo_Mail.Text)

        cmdSendMail.Visible = True
        cmdSave.Visible = False
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalSuccess(this,'Success','บันทึกอนุมัติเรียบร้อย');", True)

    End Sub

    Private Sub SendEmail(PersonName As String, Username As String, Password As String, sTo As String)
        Dim CC As String = ""
        If sTo = "" Then
            sTo = txtEmail.Text
        Else
            CC = txtEmail.Text
        End If

        If sTo = "" Then
            Exit Sub
        End If

        Dim SenderDisplayName As String = "มูลนิธิเภสัชกรรมชุมชน"
        Dim MySubject As String = "ยืนยันการลงทะเบียนเข้าใช้งาน FCPPROJECT :" & txtLocationName.Text
        Dim MyMessageBody As String = ""

        MyMessageBody = " <font size='4'>
    
  <p> เรียน คุณ " & PersonName & "</p>
  <p> เรื่อง แจ้งอนุมัติการลงทะเบียนเข้าใช้งานระบบฐานข้อมูลกิจกรรมบริการสร้างเสริมสุขภาพและการดูแลการใช้ยาฯโดยเภสัชกรชุมชน (FCPPROJECT)</p> 
     <p>ท่านสามารถเข้าใช้งานระบบได้ที่ <a href='https://www.fcpproject.com'>https://www.fcpproject.com</p> 
  <p>ข้อมูลผู้ใช้งานของท่าน</p>
        ชื่อผู้ใช้ (Username) : " & Username & " <br />
        รหัสผ่าน (Password) : " & Password & "  <br /> <br />

       <font color='#ff0000'> 
**อีเมล์นี้เป็นระบบอัตโนมัติ ห้ามตอบกลับใดๆทั้งสิ้น** </font> <br />
        หากท่านต้องการติดต่อสอบถามข้อมูลเพิ่มเติมสามารถติดต่อได้ตามช่องทางต่อไปนี้ <br />
       
        <a href='https://compharmfund.or.th/site/page/contact_us'>มูลนิธิเภสัชกรรมชุมชน</a> <br />
        อีเมลล์ <a href='mailto:support@compharmfund.or.th'>support@compharmfund.or.th</a> <br />       

        <br /><br />
 
        ขอแสดงความนับถือ <br />
       
มูลนิธิเภสัชกรรมชุมชน<br />
40 ซอย สุขุมวิท 38 แขวงคลองเตย เขตคลองเตย กรุงเทพมหานคร 10110<br /> 
    </font>"


        Dim mailMessage As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage()
        mailMessage.From = New MailAddress("papcinfo@gmail.com", SenderDisplayName)
        mailMessage.Subject = MySubject
        mailMessage.Body = MyMessageBody
        mailMessage.IsBodyHtml = True
        mailMessage.[To].Add(New MailAddress(sTo))
        If CC <> "" Then
            mailMessage.CC.Add(New MailAddress(CC))
        End If

        Dim smtp As SmtpClient = New SmtpClient()
        smtp.Host = "smtp.gmail.com"
        smtp.EnableSsl = True
        Dim NetworkCred As NetworkCredential = New NetworkCredential()
        NetworkCred.UserName = mailMessage.From.Address
        NetworkCred.Password = "qktzlmsamzknnizq"
        smtp.UseDefaultCredentials = True
        smtp.Credentials = NetworkCred
        smtp.Port = 587
        smtp.Send(mailMessage)
    End Sub

    Protected Sub cmdReject_Click(sender As Object, e As EventArgs) Handles cmdReject.Click
        ctlL.Register_Reject(hdUID.Value)
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalSuccess(this,'Success','บันทึกไม่อนุมัติเรียบร้อย');", True)
    End Sub

    Protected Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click
        ctlL.Register_Delete(hdUID.Value)
        hdUID.Value = 0
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalSuccess(this,'Success','ลบเรียบร้อย');", True)
    End Sub

    Protected Sub cmdSendMail_Click(sender As Object, e As EventArgs) Handles cmdSendMail.Click
        Dim dtU As New DataTable

        Dim ctlU As New UserController
        dtU = ctlU.User_GetByUsername(txtLocationID.Text)

        If dtU.Rows.Count > 0 Then
            SendEmailUsername(txtLocationName.Text, txtLocationID.Text, enc.DecryptString(dtU.Rows(0)("Password"), True), txtCo_Mail.Text)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalSuccess(this,'Success','ส่งอีเมล์แจ้ง Username & Password เรียบร้อย');", True)
        Else
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','ไม่พบอีเมล์ที่จะส่ง');", True)
        End If
    End Sub

    Private Sub SendEmailUsername(PersonName As String, Username As String, Password As String, sTo As String)
        Dim CC As String = ""
        If sTo = "" Then
            sTo = txtEmail.Text
        Else
            CC = txtEmail.Text
        End If

        If sTo = "" Then
            Exit Sub
        End If

        Dim SenderDisplayName As String = "มูลนิธิเภสัชกรรมชุมชน"
        Dim MySubject As String = "แจ้งข้อมูลการเข้าใช้งาน FCPPROJECT :" & txtLocationName.Text
        Dim MyMessageBody As String = ""

        MyMessageBody = " <font size='4'>
    
  <p> เรียน คุณ " & PersonName & "</p>
  <p> เรื่อง แจ้งข้อมูลการเข้าใช้งานระบบฐานข้อมูลกิจกรรมบริการสร้างเสริมสุขภาพและการดูแลการใช้ยาฯโดยเภสัชกรชุมชน (FCPPROJECT)</p> 
     <p>ท่านสามารถเข้าใช้งานระบบได้ที่ <a href='https://www.fcpproject.com'>https://www.fcpproject.com</p> 
  <p>ข้อมูลผู้ใช้งานของท่าน</p>
        ชื่อผู้ใช้ (Username) : " & Username & " <br />
        รหัสผ่าน (Password) : " & Password & "  <br /> <br />

        <font color='#ff0000'> 
**อีเมล์นี้เป็นระบบอัตโนมัติ ห้ามตอบกลับใดๆทั้งสิ้น** </font><br />
        หากท่านต้องการติดต่อสอบถามข้อมูลเพิ่มเติมสามารถติดต่อได้ตามช่องทางต่อไปนี้ <br />
       
        <a href='https://compharmfund.or.th/site/page/contact_us'>มูลนิธิเภสัชกรรมชุมชน</a> <br />
        อีเมลล์ <a href='mailto:support@compharmfund.or.th'>support@compharmfund.or.th</a> <br />       

        <br /><br />
 
        ขอแสดงความนับถือ <br />
       
มูลนิธิเภสัชกรรมชุมชน<br />
40 ซอย สุขุมวิท 38 แขวงคลองเตย เขตคลองเตย กรุงเทพมหานคร 10110<br /> 
    </font>"


        Dim mailMessage As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage()
        mailMessage.From = New MailAddress("papcinfo@gmail.com", SenderDisplayName)
        mailMessage.Subject = MySubject
        mailMessage.Body = MyMessageBody
        mailMessage.IsBodyHtml = True
        mailMessage.[To].Add(New MailAddress(sTo))
        If CC <> "" Then
            mailMessage.CC.Add(New MailAddress(CC))
        End If

        Dim smtp As SmtpClient = New SmtpClient()
        smtp.Host = "smtp.gmail.com"
        smtp.EnableSsl = True
        Dim NetworkCred As NetworkCredential = New NetworkCredential()
        NetworkCred.UserName = mailMessage.From.Address
        NetworkCred.Password = "qktzlmsamzknnizq"
        smtp.UseDefaultCredentials = True
        smtp.Credentials = NetworkCred
        smtp.Port = 587
        smtp.Send(mailMessage)
    End Sub
End Class