﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO



Public Class F18
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlLct As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlOrder As New OrderController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then
            If Request("acttype") = "view" Then
                cmdSave.Visible = False
            Else
                cmdSave.Visible = True
            End If
            txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)
            CalScore_Q()

            If Request("t") <> "new" Then
                If Request("fid") <> "0" Then
                    LoadFormData()
                End If
            End If

            LoadPharmacist(Request.Cookies("LocationID").Value)
        End If

        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")


    End Sub
    Private Sub LoadPharmacist(LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With
            End If
        End If
        dtP = Nothing
    End Sub
    Private Sub LoadLocationData(LocationID As String)
        dt = ctlLct.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblLocationID.Text = .Item("LocationID")
                lblLocationName.Text = .Item("LocationName")
                lblLocationProvince.Text = .Item("ProvinceName")
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub CalScore_Q()
        Dim iScore As Integer = 0
        iScore = StrNull2Zero(opt9Q1.SelectedValue) +
                 StrNull2Zero(opt9Q2.SelectedValue) +
                 StrNull2Zero(opt9Q3.SelectedValue) +
                 StrNull2Zero(opt9Q4.SelectedValue) +
                 StrNull2Zero(opt9Q5.SelectedValue) +
                 StrNull2Zero(opt9Q6.SelectedValue) +
                 StrNull2Zero(opt9Q7.SelectedValue) +
                 StrNull2Zero(opt9Q8.SelectedValue) +
                 StrNull2Zero(opt9Q9.SelectedValue)

        lbl9QTotal.Text = iScore.ToString()
        lbl9QAlert.Visible = True
        lbl9QResultTxt.Text = ""
        lbl9QResultTxt.Visible = True

        If iScore < 7 Then
            lbl9QAlert.Visible = False
            lbl9QResultTxt.Text = "(ไม่มีอาการของโรคซึมเศร้าหรือมีอาการของโรคซึมเศร้าระดับน้อยมาก)"
        ElseIf iScore >= 7 And iScore <= 12 Then
            lbl9QResultTxt.Text = "(มีอาการของโรคซึมเศร้าระดับน้อย)"
        ElseIf iScore >= 13 And iScore <= 18 Then
            lbl9QResultTxt.Text = "(มีอาการของโรคซึมเศร้าระดับปานกลาง)"
        ElseIf iScore >= 19 Then
            lbl9QResultTxt.Text = "(มีอาการของโรคซึมเศร้าระดับรุนแรง)"
        End If

    End Sub

    Private Sub CalScore_ST()
        Dim iScore As Integer = 0
        iScore = StrNull2Zero(optST1.SelectedValue) +
                 StrNull2Zero(optST2.SelectedValue) +
                 StrNull2Zero(optST3.SelectedValue) +
                 StrNull2Zero(optST4.SelectedValue) +
                 StrNull2Zero(optST5.SelectedValue)

        lblSTTotal.Text = iScore.ToString()
        lblSTResultTxt.Text = ""
        lblSTResultTxt.Visible = True

        If iScore <= 4 Then
            lblSTResultTxt.Text = "(เครียดน้อย)"
        ElseIf iScore > 4 And iScore <= 7 Then
            lblSTResultTxt.Text = "(เครียดปานกลาง)"
        ElseIf iScore > 7 And iScore <= 9 Then
            lblSTResultTxt.Text = "(เครียดมาก)"
        ElseIf iScore >= 10 Then
            lblSTResultTxt.Text = "(เครียดมากที่สุด แนะนำให้พบแพทย์)"
        End If


    End Sub

    Private Sub LoadFormData()
        'Dim pYear As Integer
        'If Not Request("fid") Is Nothing Then
        '    dt = ctlOrder.GetOrder_ByID(StrNull2Zero(Request("fid")))
        'Else
        '    If Not Request("yid") Is Nothing Then
        '        pYear = CInt(Request("yid"))
        '    Else
        '        pYear = Year(ctlLct.GET_DATE_SERVER)
        '        If pYear < 2500 Then
        '            pYear = pYear + 543
        '        End If

        '    End If
        'dt = ctlOrder.LocationOrder_ByYear(pYear, FORM_TYPE_ID_F18, Request.Cookies("LocationID").Value, Session("patientid"))
        'End If

        dt = ctlOrder.F18_GetByUID(Request("fid"))

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Session("patientid") = DBNull2Lng(.Item("PatientID"))
                lblID.Text = .Item("UID")
                'Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                txtTime.Text = String.Concat(.Item("ServiceTime"))
                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))

                chkClose.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("CloseStatus")))

                optQ1.SelectedValue = DBNull2Str(.Item("P1"))
                optQ2.SelectedValue = DBNull2Str(.Item("P2"))

                opt9Q1.SelectedValue = DBNull2Str(.Item("Q1"))
                opt9Q2.SelectedValue = DBNull2Str(.Item("Q2"))
                opt9Q3.SelectedValue = DBNull2Str(.Item("Q3"))
                opt9Q4.SelectedValue = DBNull2Str(.Item("Q4"))
                opt9Q5.SelectedValue = DBNull2Str(.Item("Q5"))
                opt9Q6.SelectedValue = DBNull2Str(.Item("Q6"))
                opt9Q7.SelectedValue = DBNull2Str(.Item("Q7"))
                opt9Q8.SelectedValue = DBNull2Str(.Item("Q8"))
                opt9Q9.SelectedValue = DBNull2Str(.Item("Q9"))

                lbl9QTotal.Text = DBNull2Str(.Item("Total9Q"))

                optST1.SelectedValue = DBNull2Str(.Item("ST1"))
                optST2.SelectedValue = DBNull2Str(.Item("ST2"))
                optST3.SelectedValue = DBNull2Str(.Item("ST3"))
                optST4.SelectedValue = DBNull2Str(.Item("ST4"))
                optST5.SelectedValue = DBNull2Str(.Item("ST5"))
                lblSTTotal.Text = DBNull2Str(.Item("TotalST5"))
                CalScore_Q()
                CalScore_ST()

                chkEducate1.Checked = ConvertYesNo2Boolean(.Item("isEducate1"))
                chkEducate2.Checked = ConvertYesNo2Boolean(.Item("isEducate2"))

                chkAgree.Checked = ConvertYesNo2Boolean(.Item("isAgree"))


                If DBNull2Zero(.Item("CloseStatus")) >= 3 Then
                    cmdSave.Visible = False
                Else
                    If Request.Cookies("LocationID").Value <> String.Concat(.Item("LocationID")) Then
                        LoadLocationData(String.Concat(.Item("LocationID")))
                        If Request("t") = "edit" Then
                            cmdSave.Visible = True
                        Else
                            cmdSave.Visible = False
                        End If
                    Else
                        LoadLocationData(Request.Cookies("LocationID").Value)
                    End If
                End If
            End With
        Else
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing

    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        Dim Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9 As Integer
        Q1 = 0
        Q2 = 0
        Q3 = 0
        Q4 = 0
        Q5 = 0
        Q6 = 0
        Q7 = 0
        Q8 = 0
        Q9 = 0

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If StrNull2Zero(txtTime.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนระยะเวลาในการให้บริการก่อน');", True)
            Exit Sub
        End If

        If optQ1.SelectedValue = "N" And optQ2.SelectedValue = "N" Then
            Q1 = 0
            Q2 = 0
            Q3 = 0
            Q4 = 0
            Q5 = 0
            Q6 = 0
            Q7 = 0
            Q8 = 0
            Q9 = 0
        Else
            If opt9Q1.SelectedValue = Nothing Or opt9Q2.SelectedValue = Nothing Or opt9Q3.SelectedValue = Nothing Or opt9Q4.SelectedValue = Nothing Or opt9Q5.SelectedValue = Nothing Or opt9Q6.SelectedValue = Nothing Or opt9Q7.SelectedValue = Nothing Or opt9Q8.SelectedValue = Nothing Or opt9Q9.SelectedValue = Nothing Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาประเมิน 9Q ให้ครบทุกข้อ');", True)
                Exit Sub
            End If
            Q1 = opt9Q1.SelectedValue
            Q2 = opt9Q2.SelectedValue
            Q3 = opt9Q3.SelectedValue
            Q4 = opt9Q4.SelectedValue
            Q5 = opt9Q5.SelectedValue
            Q6 = opt9Q6.SelectedValue
            Q7 = opt9Q7.SelectedValue
            Q8 = opt9Q8.SelectedValue
            Q9 = opt9Q9.SelectedValue
        End If

        If optST1.SelectedValue = Nothing Or optST2.SelectedValue = Nothing Or optST3.SelectedValue = Nothing Or optST4.SelectedValue = Nothing Or optST5.SelectedValue = Nothing Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาประเมิน ST5 ให้ครบทุกข้อ');", True)
                Exit Sub
        End If

        If (chkEducate1.Checked = False) And (chkEducate2.Checked = False) Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกสรุปผลของเภสัชกรก่อน');", True)
            Exit Sub
        End If

        If chkAgree.Checked = False Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกให้คำยินยอมก่อน');", True)
            Exit Sub
        End If


        Dim objuser As New UserController
        Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))

        If lblID.Text = "" Then 'Add new
            If ctlOrder.Order_ChkDupCustomer(FORM_TYPE_ID_F18, Session("patientid"), StrNull2Zero(Left(Str(ServiceDate), 4))) = True Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ผู้รับบริการรายนี้เคยได้รับบริการในกิจกรรมนี้แล้ว');", True)
                Exit Sub
            End If
            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Service_Order", "แบบคัดกรอง/แบบประเมิน โรคซึมเศร้าและความเครียด (F18):" & Session("patientname"), "F18")
        Else
            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Service_Order", "แบบคัดกรอง/แบบประเมิน โรคซึมเศร้าและความเครียด (F18):" & Session("patientname"), "F18")
        End If
        ctlOrder.F18_Save(StrNull2Long(lblID.Text), lblLocationID.Text, Session("patientid"), ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), optQ1.SelectedValue, optQ2.SelectedValue, Q1, Q2, Q3, Q4, Q5, Q6, Q7, Q8, Q9, lbl9QTotal.Text, optST1.SelectedValue, optST2.SelectedValue, optST3.SelectedValue, optST4.SelectedValue, optST5.SelectedValue, lblSTTotal.Text, ConvertStatus2YN(chkEducate1.Checked), ConvertStatus2YN(chkEducate2.Checked), ConvertStatus2YN(chkAgree.Checked), Convert2Status(chkClose.Checked), Request.Cookies("username").Value)

        Response.Redirect("ResultPage.aspx?p=F18")

    End Sub

    Protected Sub opt9Q1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles opt9Q1.SelectedIndexChanged
        CalScore_Q()
    End Sub

    Protected Sub opt9Q2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles opt9Q2.SelectedIndexChanged
        CalScore_Q()
    End Sub

    Protected Sub opt9Q3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles opt9Q3.SelectedIndexChanged
        CalScore_Q()
    End Sub

    Protected Sub opt9Q4_SelectedIndexChanged(sender As Object, e As EventArgs) Handles opt9Q4.SelectedIndexChanged
        CalScore_Q()
    End Sub

    Protected Sub opt9Q5_SelectedIndexChanged(sender As Object, e As EventArgs) Handles opt9Q5.SelectedIndexChanged
        CalScore_Q()
    End Sub

    Protected Sub opt9Q6_SelectedIndexChanged(sender As Object, e As EventArgs) Handles opt9Q6.SelectedIndexChanged
        CalScore_Q()
    End Sub

    Protected Sub opt9Q7_SelectedIndexChanged(sender As Object, e As EventArgs) Handles opt9Q7.SelectedIndexChanged
        CalScore_Q()
    End Sub

    Protected Sub opt9Q8_SelectedIndexChanged(sender As Object, e As EventArgs) Handles opt9Q8.SelectedIndexChanged
        CalScore_Q()
    End Sub

    Protected Sub opt9Q9_SelectedIndexChanged(sender As Object, e As EventArgs) Handles opt9Q9.SelectedIndexChanged
        CalScore_Q()
    End Sub

    Protected Sub optST1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optST1.SelectedIndexChanged
        CalScore_ST()
    End Sub

    Protected Sub optST2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optST2.SelectedIndexChanged
        CalScore_ST()
    End Sub

    Protected Sub optST3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optST3.SelectedIndexChanged
        CalScore_ST()
    End Sub

    Protected Sub optST4_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optST4.SelectedIndexChanged
        CalScore_ST()
    End Sub

    Protected Sub optST5_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optST5.SelectedIndexChanged
        CalScore_ST()
    End Sub

    Protected Sub optQ1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ1.SelectedIndexChanged
        CalScore_Q()
    End Sub

    Protected Sub optQ2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ2.SelectedIndexChanged
        CalScore_Q()
    End Sub
End Class