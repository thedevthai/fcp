﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="ReportMenu.aspx.vb" Inherits=".ReportMenu" %>

<%@ Register Assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
       
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<section class="content-header">
      <h1>REPORT
        <small>รายงาน</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">รายงาน</li>
      </ol>
</section>

<section class="content">  

    <dx:ASPxPageControl ID="ASPxPageControl1" runat="server" ActiveTabIndex="9" EnableTheming="True" Theme="Material" Width="100%" BackColor="White">
        <TabPages>
            <dx:TabPage Text="สรุปกิจกรรม">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                          <table width="100%" border="0" cellspacing="4" cellpadding="0"  class="table table-hover">
  <tr>
    <td width="50%"> <asp:HyperLink ID="sum1" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportCondition.aspx?ActionType=rpt&ItemType=fnc" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;สรุปจำนวนกิจกรรม</asp:HyperLink></td>
    <td></td>
  </tr>
  <tr>
    <td> <asp:HyperLink ID="HyperLink15" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportMetabolicCondition.aspx?ActionType=rpt&ItemType=soc" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;กิจกรรมแยกแต่ละบุคคล</asp:HyperLink></td>
    <td><asp:HyperLink ID="HyperLink16" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionSummary.aspx?ActionType=rpt&ItemType=fsum" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;สรุปผลการให้บริการรายบุคคล</asp:HyperLink></td>
  </tr>
</table>

                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="ผู้รับบริการ">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                             <table width="100%" border="0" cellspacing="4" cellpadding="0" class="table table-hover">
  <tr>
    <td width="50%">  <asp:HyperLink ID="Pat1" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportCondition.aspx?ActionType=rpt&ItemType=cus" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;รายชื่อผู้เข้ารับบริการแยกตามกิจกรรม</asp:HyperLink></td>
    <td>  <asp:HyperLink ID="Pat2" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=cusp" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;รายชื่อผู้เข้ารับบริการแยกตามจังหวัด</asp:HyperLink></td>
  </tr>
<% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isAdminAccess Or Convert.ToInt32(Request.Cookies("RoleID").Value) = isSuperAdminAccess Or Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess Then%> 
  <tr>
    <td><asp:HyperLink ID="HyperLink19" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionByYear.aspx?ActionType=rpt&ItemType=3" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;รายชื่อผู้รับบริการ</asp:HyperLink></td>
    <td><asp:HyperLink ID="HyperLink18" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionByYear.aspx?ActionType=rpt&ItemType=2" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;สรุปจำนวนผู้รับบริการ</asp:HyperLink></td>
  </tr>
<% End If%>
  <tr>   
    <td> <asp:HyperLink ID="HyperLink13" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportMetabolicCondition.aspx?ActionType=rpt&ItemType=cnt" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;สรุปจำนวนผู้รับบริการ</asp:HyperLink></td>
       <td>&nbsp;</td>
  </tr>

  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="ผลการคัดกรอง">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                          <table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td  valign="top"><table border="0" align="center" cellpadding="0" cellspacing="4" class="table table-hover">
          <tr>
            <td class="rpttopic">คัดกรองความเสี่ยงเบาหวาน</td>
          </tr>
          <tr>
        
            <td><asp:HyperLink ID="HyperLink1" runat="server"  CssClass="button_rpt_Menu"
            NavigateUrl="ReportMetabolicCondition.aspx?ActionType=rpt&ItemType=smz&r=d" Width="100%"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;ผลการคัดกรองความเสี่ยงเบาหวาน (สรุป)</asp:HyperLink></td>
          </tr>
          <tr>
           
            <td><asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="ReportMetabolicCondition.aspx?ActionType=rpt&ItemType=scn&r=d" Width="100%"  CssClass="button_rpt_Menu"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;ผลการคัดกรองความเสี่ยงเบาหวาน (รายชื่อ)</asp:HyperLink></td>
          </tr>
        </table></td>
        <td valign="top">
  <table border="0" align="center" cellpadding="0" cellspacing="4" class="table table-hover">
  <tr> <td class="rpttopic">คัดกรองความเสี่ยงอ้วน</td>
    </tr>
 
  <tr>  
    <td><asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="ReportMetabolicCondition.aspx?ActionType=rpt&ItemType=smz&r=f" Width="100%"  CssClass="button_rpt_Menu"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;ผลการคัดกรองความเสี่ยงอ้วน (สรุป)</asp:HyperLink></td>
  </tr>
  <tr> 
    <td><asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="ReportMetabolicCondition.aspx?ActionType=rpt&amp;ItemType=scn&r=f" Width="100%"  CssClass="button_rpt_Menu"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;ผลการคัดกรองความเสี่ยงอ้วน (รายชื่อ)</asp:HyperLink></td>
  </tr>
</table>	</td>
        <td valign="top"><table border="0" align="center" cellpadding="0" cellspacing="4" class="table table-hover">
          <tr>
            <td class="rpttopic">คัดกรองความเสี่ยงความดันโลหิตสูง</td>
          </tr>
          <tr>
           <td>
               <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="ReportMetabolicCondition.aspx?ActionType=rpt&amp;ItemType=smz&r=b" Width="100%"  CssClass="button_rpt_Menu"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;ผลการคัดกรองความเสี่ยงความดันโลหิตสูง (สรปุ)</asp:HyperLink></td>
          </tr>
          <tr>
           
            <td><asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="ReportMetabolicCondition.aspx?ActionType=rpt&amp;ItemType=scn&r=b" Width="100%"  CssClass="button_rpt_Menu"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;ผลการคัดกรองความเสี่ยงความดันโลหิตสูง (รายชื่อ)</asp:HyperLink></td>
          </tr>
        </table></td>
      </tr>
                              <tr>
                                  <td colspan="3"><asp:HyperLink ID="HyperLink26" runat="server" CssClass="button_rpt_Menu" NavigateUrl="ReportMetabolicCondition.aspx?ActionType=rpt&amp;ItemType=3d&amp;r=3d" Width="100%"><img src="images/healthbook.png" width="20" height="20"  align="absmiddle"  />&nbsp;ผลการคัดกรองความเสี่ยง 3 โรค</asp:HyperLink>
                                  </td>
                              </tr>
    </table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage  Text="ร้านยา">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                          <table width="100%" border="0" cellspacing="4" cellpadding="0" class="table table-hover">
  <tr>
    <td width="50%"><asp:HyperLink ID="HyperLink8" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportLocationsGroup.aspx?ActionType=rpt&ItemType=1" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;ร้านยาในโครงการ</asp:HyperLink></td>
    <td>  </td>
  </tr>
  <tr>
    <td></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage  Text="สปสช.">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                         <table width="100%" border="0" cellspacing="4" cellpadding="0" class="table table-hover">
  <tr>
    <td width="50%">  <asp:HyperLink ID="HyperLink9" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportMetabolicCondition.aspx?ActionType=rpt&ItemType=nhso" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;NHSO</asp:HyperLink></td>
    <td>  <asp:HyperLink ID="HyperLink10" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionByDate.aspx?ActionType=rpt&ItemType=nhso2" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;จำนวนกิจกรรมตามเขต สปสช.</asp:HyperLink></td>
  </tr>
  <tr>
    <td><asp:HyperLink ID="HyperLink11" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionByNHSO.aspx?ActionType=rpt&ItemType=nhso3" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;การแจ้งสิทธิประโยชน์ สปสช.</asp:HyperLink></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage  Text="VMI">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                          <table width="100%" border="0" cellspacing="4" cellpadding="0" class="table table-hover">
  <tr>
    <td width="50%">  <asp:HyperLink ID="HyperLink23" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionByDate.aspx?ActionType=rpt&ItemType=vmi1" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;รายงานการใช้ยา</asp:HyperLink></td>
    <td>  <asp:HyperLink ID="HyperLink24" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionByDate.aspx?ActionType=rpt&ItemType=vmi2" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;รายงานสต๊อก</asp:HyperLink></td>
  </tr>
  <tr>
    <td> </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage  Text="โครงการบุหรี่">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                           <table width="100%" border="0" cellspacing="4" cellpadding="0" class="table table-hover">
  <tr>
    
     <td><asp:HyperLink ID="HyperLink12" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportSmokingCondition.aspx?ActionType=rpt&ItemType=sm2" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;ข้อมูลการให้บริการ A1-A4</asp:HyperLink></td>
      <td width="50%"> <asp:HyperLink ID="HyperLink14" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportSmokingCondition.aspx?ActionType=rpt&ItemType=smsum1" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;สรุปข้อมูลการให้บริการ</asp:HyperLink></td>
  </tr>
  <tr>
 
  
    <td><asp:HyperLink ID="HyperLink21" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportSmokingCondition.aspx?ActionType=rpt&ItemType=sm3" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;ข้อมูลการให้บริการติดตาม A4</asp:HyperLink></td>
         <td>
              <asp:HyperLink ID="hlnkSmkFn" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportSmokingCondition.aspx?ActionType=rpt&ItemType=smfn1" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;สรุปข้อมูลการให้บริการ+ค่าตอบแทน</asp:HyperLink>
         </td>
  </tr>
  <tr>
  
    <td><asp:HyperLink ID="HyperLink20" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportSmokingCondition.aspx?ActionType=rpt&ItemType=sm4" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;ข้อมูลการให้บริการ A5</asp:HyperLink></td>
         <td>
               <asp:HyperLink ID="hlnkSmkFnstopday" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportSmokingCondition.aspx?ActionType=rpt&ItemType=smstop"  ><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;สรุปข้อมูลการหยุดสูบ (30/60/90/120/... วัน)</asp:HyperLink>
         </td>
  </tr>
  <tr>
    <td><asp:HyperLink ID="HyperLink38" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportSmokingCondition.aspx?ActionType=rpt&ItemType=smpq" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;ข้อมูลผู้ที่ต้องการเลิกบุหรี่</asp:HyperLink></td>
    <td><asp:HyperLink ID="HyperLink32" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportSmokingCondition.aspx?ActionType=rpt&ItemType=smstopdate" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;รายงานวันที่หยุดสูบ</asp:HyperLink>

    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage  Text="อื่น  ๆ" >
                <ContentCollection>
                    <dx:ContentControl runat="server">
    <% If Convert.ToInt32(Request.Cookies("RoleID").Value) <> isShopAccess And Convert.ToInt32(Request.Cookies("RoleID").Value) <> isReportViewerAccess Then%> 
                          <table width="100%" border="0" cellspacing="4" cellpadding="0" class="table table-hover">
  <tr>
    <td width="50%">  <asp:HyperLink ID="HyperLink17" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="rptReceivedView.aspx?ActionType=rpt&ItemType=rcv" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;รายงานการชำระเงิน(คำปรึกษา)</asp:HyperLink></td>
    <td> <asp:HyperLink ID="HyperLink22" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="rptReceivedView.aspx?ActionType=rpt&ItemType=smk" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;รายงานการชำระเงิน(บุหรี่)</asp:HyperLink> </td>
  </tr>

  <tr>
    <td><asp:HyperLink ID="HyperLink25" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="rptReceivedView.aspx?ActionType=rpt&ItemType=smk2" Width="250"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;รายงานการจ่ายเงินหยุดสูบบุหรี่</asp:HyperLink> </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
    <% End If %>
                         </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="MTM &amp; Home visit">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                          <table width="100%" border="0" cellspacing="4" cellpadding="0" class="table table-hover">
  <tr>
    <td width="50%">  <asp:HyperLink ID="HyperLink27" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=mtm" Width="250"><img src="images/med_icon.png" width="20" height="20"  align="absmiddle"  />&nbsp;รายงาน MTM & Home visit</asp:HyperLink></td>
    <td><asp:HyperLink ID="HyperLink28" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=mcount"><img src="images/chart-pie.png" width="20" height="20"  align="absmiddle"  />&nbsp;รายงานสรุปจำนวนบริการ MTM & Home visit</asp:HyperLink></td>
  </tr>

  <tr><td><i class="fa fa-odnoklassniki-square"></i><asp:HyperLink ID="HyperLink29" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=mtmcount">&nbsp;รายงานสรุปจำนวนบริการ MTM</asp:HyperLink></td>
    <td><i class="fa fa-h-square"></i><asp:HyperLink ID="HyperLink30" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=hvcount">&nbsp;รายงานสรุปจำนวนบริการ Home visit</asp:HyperLink></td>
    
  </tr>
  <tr>
    <td><i class="fa fa-bar-chart text-green"></i><asp:HyperLink ID="HyperLink31" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=pcount">&nbsp;รายงานสรุปปัญหารายบุคคล</asp:HyperLink></td>
    <td><img src="images/med_icon.png" width="20" height="20"  align="absmiddle"  /><asp:HyperLink ID="HyperLink33" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=dr">&nbsp;รายงานยาเหลือใช้</asp:HyperLink>&nbsp;</td>
  </tr>
  <tr>
     <td></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
            <dx:TabPage Text="Home Isolation">
                <ContentCollection>
                    <dx:ContentControl runat="server">
                     
                         <table width="100%" border="0" cellspacing="4" cellpadding="0" class="table table-hover">
  <tr>
    <td width="50%">  
        <asp:HyperLink ID="HyperLink7" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=cov1"><img src="images/chart-pie.png" width="20" height="20"  align="absmiddle"  />&nbsp;รายงานสรุปจำนวนผู้ป่วยในโครงการแยกตามร้านยา</asp:HyperLink>

    </td>
    <td>
        <asp:HyperLink ID="HyperLink34" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=cov2"><img src="images/chart-strick.png" width="20" height="20"  align="absmiddle"  />&nbsp;รายงานการติดตามอาการผู้ป่วยรายคน</asp:HyperLink>

    </td>
  </tr>

  <tr><td><i class="fa fa-odnoklassniki-square"></i><asp:HyperLink ID="HyperLink35" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=cov3"><img src="images/med_icon.png" width="20" height="20"  align="absmiddle"  />&nbsp;รายงานการจ่ายยาให้ผู้ป่วยในโครงการ</asp:HyperLink></td>

    <td><i class="fa fa-h-square"></i><asp:HyperLink ID="HyperLink36" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=cov4"><img src="images/pie-chart_graph.gif" width="20" height="20"  align="absmiddle"  />&nbsp;รายงานปัญหากาติดตามอาการ</asp:HyperLink></td>
    
  </tr>
  <tr>
    <td><i class="fa fa-bar-chart text-green"></i><asp:HyperLink ID="HyperLink37" runat="server" CssClass="button_rpt_Menu"   NavigateUrl="ReportConditionByProvince.aspx?ActionType=rpt&ItemType=cov5"><img src="images/history.png" width="20" height="20"  align="absmiddle"  />&nbsp;รายงานสรุปผลการติดตามอาการหลังครบ 14 วันหรือหลังสิ้นสุดวันให้การรักษา</asp:HyperLink></td>
    <td>&nbsp;</td>
  </tr> 
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>


                    </dx:ContentControl>
                </ContentCollection>
            </dx:TabPage>
        </TabPages>
    </dx:ASPxPageControl>
 </section> 

</asp:Content>
