﻿
Public Class LocationsEdit
    Inherits System.Web.UI.Page

    Dim ctlLG As New LocationController
    Dim dt As New DataTable
    Dim acc As New UserController
    Dim ctlPs As New PersonController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then
            lblID.Text = ""
            'LoadBankToDDL()
            EditData()
            LoadPharmacist()
        End If

        txtZipCode.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
    End Sub

    'Private Sub LoadBankToDDL()
    '    Dim ctlbase As New ApplicationBaseClass
    '    dt = ctlbase.LoadBank
    '    If dt.Rows.Count > 0 Then
    '        With ddlBank
    '            .Enabled = True
    '            .DataSource = dt
    '            .DataTextField = "BankName"
    '            .DataValueField = "BankID"
    '            .DataBind()
    '            .SelectedIndex = 0
    '        End With
    '    End If
    '    dt = Nothing
    'End Sub
    Private Sub LoadPharmacist()
        If lblID.Text <> "" Then
            dt = ctlPs.GetPerson_ByLocation(lblID.Text)
        End If

        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To dt.Rows.Count - 1
                    .Rows(i).Cells(0).Text = i + 1
                Next

            End With
        Else
            grdData.Visible = False
        End If
        dt = Nothing
    End Sub

    Private Sub EditData()
        Dim dtE As New DataTable
        Dim pID As String

        If Request("id") Is Nothing Then
            pID = Request.Cookies("LocationID").Value
        Else
            pID = Request("id")
        End If

        dtE = ctlLG.Location_GetByID(pID)
        Dim objList As New LocationInfo
        If dtE.Rows.Count > 0 Then
            With dtE.Rows(0)
                isAdd = False

                txtLicenseNo.Text = String.Concat(.Item("LicenseNo"))
                txtNHSOCode.Text = String.Concat(.Item("LocationCode"))

                Me.lblID.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f00_LocationID).fldName))
                txtName.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f01_LocationName).fldName))

                lblTypeName.Text = DBNull2Str(dtE.Rows(0)("LocationGroupName"))

                ddlTypeShop.SelectedValue = DBNull2Str(.Item("LocationType"))
                txtTypeName.Text = DBNull2Str(.Item("TypeOther"))
                txtYear.Text = DBNull2Str(.Item("RegisYear"))

                txtAddress.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f06_Address).fldName))

                lblProvinceName.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f08_ProvinceName).fldName))
                Me.txtZipCode.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f09_ZipCode).fldName))

                txtTel.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f10_Office_Tel).fldName))
                txtFax.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f11_Office_Fax).fldName))
                txtMail.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f24_Office_Mail).fldName))

                txtCoName.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f12_Co_Name).fldName))
                txtCoMail.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f14_Co_Mail).fldName))
                txtCoTel.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f15_Co_Tel).fldName))
                'txtAccNo.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f16_AccNo).fldName))
                'txtAccName.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f17_AccName).fldName))
                'ddlBank.SelectedValue = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f18_BankID).fldName))
                'txtBrunch.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f19_BankBrunch).fldName))
                'optBankType.SelectedValue = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f20_BankType).fldName))

                If String.Concat(.Item("Lng")) <> "" Then
                    txtLat.Text = String.Concat(.Item("Lat")) & "," & String.Concat(.Item("Lng"))
                Else
                    txtLat.Text = String.Concat(.Item("Lat"))
                End If
            End With
        End If
        dtE = Nothing
        objList = Nothing
    End Sub

    Protected Function validateData() As Boolean
        Dim result As Boolean = True
        lblValidate.Text = ""

        If txtName.Text = "" Then
            result = False
            lblValidate.Text &= "- กรุณาระบุชื่อร้านยา  <br />"
            lblValidate.Visible = True

        End If


        'Dim n As Integer = 0
        'For i = 0 To optGroup.Items.Count - 1
        '    If optGroup.Items(i).Selected Then
        '        n = n + 1
        '    End If
        'Next

        'If n = 0 Then
        '    lblValidate.Text = "กรุณาเลือกประเภทร้านยา"
        '    lblValidate.Visible = True
        'End If

        Return result
    End Function

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If validateData() Then

            lblValidate.Visible = False

            Dim item As Integer
            Dim sLat, sLng, sL() As String
            sLat = ""
            sLng = ""
            If txtLat.Text <> "" Then
                    sL = Split(Replace(txtLat.Text, " ", ""), ",")
                    sLat = sL(0)
                    If sL.Length > 1 Then
                        sLng = sL(1)
                    End If

                    If IsNumeric(sLat) = False Or IsNumeric(sLng) = False Then
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุละติจูด/ลองติจูด เป็นรูปแบบองศาทศนิยมเท่านั้น ');", True)
                        Exit Sub
                    End If
                End If

            item = ctlLG.Location_UpdateByUser(lblID.Text, txtName.Text, ddlTypeShop.SelectedValue, txtTypeName.Text, txtAddress.Text, txtZipCode.Text, txtTel.Text, txtFax.Text, txtCoName.Text, txtCoMail.Text, txtCoTel.Text, Request.Cookies("username").Value, txtMail.Text, txtYear.Text, txtLicenseNo.Text, txtNHSOCode.Text, sLat, sLng)

            acc.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Locations", "แก้ไข ร้านยา:" & txtName.Text, "")

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

        Else

        End If

    End Sub


    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        EditData()
    End Sub

End Class

