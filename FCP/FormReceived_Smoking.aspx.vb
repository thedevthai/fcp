﻿
Public Class FormReceived_Smoking
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlType As New ServiceTypeController
    ' Dim objUser As New UserController
    Dim ctlOrder As New SmokingController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Convert.ToInt32(Request.Cookies("RoleID").Value) <> isSuperAdminAccess Then
            Response.Redirect("Home.aspx?aut=no")
        End If


        If Not IsPostBack Then
            LoadProvinceToDDL()

            Dim m, y As Integer
            y = Year(Date.Now)
            m = Month(Date.Now)
            If y < 2500 Then
                y = y + 543
            End If

            txtStartDate.Text = "01/" & m & "/" & y 'Date.Now.AddDays(-30).ToString("dd/MM/yyyy", DateFormat_TH)
            txtEndDate.Text = Date.DaysInMonth(y, m) & "/" & m & "/" & y  'Date.Now.ToString("dd/MM/yyyy", DateFormat_TH)
            txtPayDate.Text = Date.Now.ToString("dd/MM/yyyy", DateFormat_TH)

            lblNo.Visible = False
            lblNo.Visible = False
            LoadActivityTypeToDDL()

            grdComplete.PageIndex = 0
            LoadActivityListReceivedToGrid()

            genInvoicNo()

        End If
    End Sub

    Private Sub genInvoicNo()
        txtInvoiceNo.Text = "smk" & ctlOrder.genRunningNumber("SMK", ddlProvince.SelectedValue) & "-" & ConvertStrDate2DBString(txtPayDate.Text) & "-" & ddlProvince.SelectedValue
    End Sub

    Private Sub LoadProvinceToDDL()
        dt = ctlOrder.Province_GetInLocation

        ddlProvince.Items.Clear()
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .Items.Add("---ทั้งหมด---")
                .Items(0).Value = "0"
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)("ProvinceName"))
                    .Items(i + 1).Value = dt.Rows(i)("ProvinceID")
                Next
                .SelectedIndex = 0

                '.DataSource = dt
                '.DataTextField = "ProvinceName"
                '.DataValueField = "ProvinceID"
                '.DataBind()
                '.SelectedIndex = 1
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadActivityListReceivedToGrid()

        System.Threading.Thread.Sleep(1000)
        UpdateProgress1.Visible = True

        Dim StartDate As String = "0"
        Dim EndDate As String = "0"
        Dim sumCount As Integer = 0


        'StartDate = ConvertStrDate2DateQueryString(txtStartDate.Text)


        StartDate = ConvertStrDate2DBString(txtStartDate.Text)


        EndDate = CDate(txtEndDate.Text).AddDays(1)
        'EndDate = ConvertStrDate2DateQueryString(EndDate)
        EndDate = ConvertStrDate2DBString(EndDate)

        dt = ctlOrder.Smoking_Get4ReceivedByCloseDate(ddlProvince.SelectedValue, ddlActivity.SelectedValue, StartDate, EndDate)

        If dt.Rows.Count > 0 Then
            lblCount.Text = dt.Rows.Count
            lblNo.Visible = False
            With grdComplete
                .Visible = True
                .DataSource = dt
                .DataBind()

                Try

                    Dim nrow As Integer = dt.Rows.Count
                    If .PageCount > 1 Then
                        If .PageIndex > 0 Then
                            If (.PageSize) < nrow - (.PageSize * .PageIndex) Then
                                For i = 0 To .PageSize - 1
                                    ' .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                    .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows((.PageSize * .PageIndex) + i)("ServiceDate"))
                                    .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows((.PageSize * .PageIndex) + i)("Gender"))


                                Next
                            Else
                                For i = 0 To nrow - (.PageSize * .PageIndex) - 1
                                    '.Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                    .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows((.PageSize * .PageIndex) + i)("ServiceDate"))
                                    .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows((.PageSize * .PageIndex) + i)("Gender"))


                                Next
                            End If
                        Else
                            For i = 0 To .PageSize - 1
                                ' .Rows(i).Cells(0).Text = i + 1
                                .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows(i)("ServiceDate"))
                                .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows(i)("Gender"))
                            Next
                        End If
                    Else
                        For i = 0 To nrow - 1
                            ' .Rows(i).Cells(0).Text = i + 1
                            .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows(i)("ServiceDate"))
                            .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows(i)("Gender"))
                        Next
                    End If

                Catch ex As Exception

                End Try

                For i = 0 To grdComplete.Rows.Count - 1

                Next
            End With
        Else
            lblCount.Text = 0
            lblNo.Visible = True
            grdComplete.Visible = False
            grdComplete.DataSource = Nothing
        End If
        UpdateProgress1.Visible = False
        dt = Nothing
    End Sub
    Protected Function DateText(ByVal input As Date) As String
        Dim dStr As String
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01-01-0544", "", input.ToString("dd/MM/yyyy"))
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01-01-0001", "", input.ToString("dd/MM/yyyy"))
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01-01-0544", "", input.ToString("dd/MM/yyyy"))
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/2443", "", input.ToString("dd/MM/yyyy"))
        Return dStr
    End Function

    Private Sub LoadActivityTypeToDDL()
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            dt = ctlType.ServiceType_GetByLocationID(Request.Cookies("LocationID").Value)
        ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isProjectManager Then
            dt = ctlType.ServiceType_GetByProjectID(Request.Cookies("PRJMNG").Value)
        Else
            dt = ctlType.ServiceType_GetByProjectID(PROJECT_SMOKING)
        End If

        If dt.Rows.Count > 0 Then
            ddlActivity.Items.Clear()
            ddlActivity.Items.Add("---ทั้งหมด---")
            ddlActivity.Items(0).Value = "0"
            For i = 0 To dt.Rows.Count - 1
                With ddlActivity
                    .Items.Add("" & dt.Rows(i)("ServiceTypeID") & " : " & dt.Rows(i)("ServiceName"))
                    .Items(i + 1).Value = dt.Rows(i)("ServiceTypeID")
                End With
            Next
        End If
        dt = Nothing
    End Sub

    Dim svTypeID As String
    Dim svSeqNo As Integer

    Protected Sub ddlActivity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlActivity.SelectedIndexChanged
        grdComplete.PageIndex = 0
        genInvoicNo()
        LoadActivityListReceivedToGrid()
    End Sub

    Private Sub grdComplete_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdComplete.PageIndexChanging
        grdComplete.PageIndex = e.NewPageIndex
        LoadActivityListReceivedToGrid()
    End Sub

    Private Sub grdComplete_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdComplete.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        Dim Bdate, Edate, Pdate As String

        'Bdate = ConvertStrDate2DateQueryString(txtStartDate.Text)
        'Edate = CDate(txtEndDate.Text).AddDays(1)
        'Edate = ConvertStrDate2DateQueryString(Edate)

        Bdate = ConvertStrDate2DBString(txtStartDate.Text)


        Edate = CDate(txtEndDate.Text).AddDays(1)
        Edate = ConvertStrDate2DBString(Edate)

        Pdate = ConvertStrDate2DBString(txtPayDate.Text)
        If Pdate = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาตรวจสอบวันที่ดูข้อมูล หรือ วันที่บันทึกข้อมูล');", True)
            Exit Sub
        End If

        ctlOrder.Smoking_UpdateStatusReceived(ddlProvince.SelectedValue, ddlActivity.SelectedValue, CLng(Bdate), CLng(Edate), CLng(Pdate), txtInvoiceNo.Text, Request.Cookies("username").Value)
        ctlOrder.RunningNumber_Update("SMK", ddlProvince.SelectedValue)


        grdComplete.PageIndex = 0
        LoadActivityListReceivedToGrid()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกเรียบร้อย');", True)

    End Sub

    Protected Sub cmdExcel_Click(sender As Object, e As EventArgs) Handles cmdExcel.Click
        If txtPayDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาตรวจสอบวันที่บันทึกข้อมูล');", True)
            Exit Sub
        End If
        ReportsName = "rptFinanceReceived"
        'Response.Redirect("rptFinanceReceived.aspx?ex=1&t=" & ddlActivity.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text & "&p=" & txtPayDate.Text & "&pv=" & ddlProvince.SelectedValue)

        Response.Redirect("ReportView.aspx?ex=1&t=" & ddlActivity.SelectedValue & "&b=" & txtStartDate.Text & "&e=" & txtEndDate.Text & "&p=" & txtPayDate.Text & "&pv=" & ddlProvince.SelectedValue)

    End Sub

    Protected Sub ddlProvince_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProvince.SelectedIndexChanged
        grdComplete.PageIndex = 0
        LoadActivityListReceivedToGrid()
        genInvoicNo()
    End Sub

    Protected Sub txtPayDate_TextChanged(sender As Object, e As EventArgs) Handles txtPayDate.TextChanged
        genInvoicNo()
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdComplete.PageIndex = 0
        genInvoicNo()
        LoadActivityListReceivedToGrid()
    End Sub
End Class

