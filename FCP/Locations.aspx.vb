﻿
Imports System.Net
Imports System.Net.Mail

Public Class Locations
    Inherits System.Web.UI.Page
    Dim ctlU As New UserController
    Dim ctlLG As New LocationController
    Dim dt As New DataTable
    Dim ds As New DataSet
    Dim enc As New CryptographyEngine
    Dim ctlPs As New PersonController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then
            grdData.PageIndex = 0
            hdID.Value = ""
            hdNewID.Value = ""
            cmdSendMail.Visible = False
            LoadLocationToGrid()

            LoadProvinceToDDL()
            'LoadBankToDDL()
            LoadLocationGroupToDDL()

            GenLocationNumber()
        End If

        txtZipCode.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
    End Sub
    Private Sub GenLocationNumber()
        If hdID.Value = "" Then
            txtLocationID.Text = ddlType.SelectedValue & ddlProvince.SelectedValue & ctlU.genRunningNumber(ddlType.SelectedValue, ddlProvince.SelectedValue)
        Else
            hdNewID.Value = txtLocationID.Text
        End If
    End Sub
    Private Sub LoadLocationToGrid()
        If Trim(txtSearch.Text) <> "" Then
            dt = ctlLG.Location_GetBySearchAll("0", txtSearch.Text)
        Else
            dt = ctlLG.Location_GetAll
        End If

        With grdData
            .Visible = True
            .DataSource = dt
            .DataBind()

            'For i = 0 To .Rows.Count - 1
            '    .Rows(i).Cells(0).Text = i + 1
            'Next
            Try


                Dim nrow As Integer = dt.Rows.Count
                If .PageCount > 1 Then
                    If .PageIndex > 0 Then
                        If (.PageSize) < nrow - (.PageSize * .PageIndex) Then
                            For i = 0 To .PageSize - 1
                                .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                            Next
                        Else
                            For i = 0 To nrow - (.PageSize * .PageIndex) - 1
                                .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                            Next
                        End If
                    Else
                        For i = 0 To .PageSize - 1
                            .Rows(i).Cells(0).Text = i + 1
                        Next
                    End If
                Else
                    For i = 0 To nrow - 1
                        .Rows(i).Cells(0).Text = i + 1
                    Next
                End If
            Catch ex As Exception

            End Try
        End With

    End Sub
    Private Sub LoadPharmacist()
        Dim dtP As New DataTable

        If hdID.Value <> "" Then
            dtP = ctlPs.GetPerson_ByLocation(hdID.Value)
        End If

        If dtP.Rows.Count > 0 Then
            With grdPharmacist
                .Visible = True
                .DataSource = dtP
                .DataBind()

                'For i = 0 To dtP.Rows.Count - 1
                '    .Rows(i).Cells(0).Text = i + 1
                'Next

            End With
        Else
            grdPharmacist.Visible = False
        End If
        dtP = Nothing
    End Sub
    Private Sub LoadProvinceToDDL()
        Dim ctlbase As New ApplicationBaseClass
        dt = ctlbase.LoadProvince
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .DataSource = dt
                .DataTextField = "ProvinceName"
                .DataValueField = "ProvinceID"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    'Private Sub LoadBankToDDL()
    '    Dim ctlbase As New ApplicationBaseClass
    '    dt = ctlbase.LoadBank
    '    If dt.Rows.Count > 0 Then
    '        With ddlBank
    '            .Enabled = True
    '            .DataSource = dt
    '            .DataTextField = "BankName"
    '            .DataValueField = "BankID"
    '            .DataBind()
    '            .SelectedIndex = 0
    '        End With
    '    End If
    '    dt = Nothing
    'End Sub

    Private Sub LoadLocationGroupToDDL()

        Dim ctlLG As New LocationGroupController
        dt = ctlLG.LocationGroup_Get
        If dt.Rows.Count > 0 Then
            With ddlType
                .Enabled = True
                .DataSource = dt
                .DataTextField = "Name"
                .DataValueField = "Code"
                .DataBind()
                .SelectedValue = "A"
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadLocationToGrid()
    End Sub


    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"
                    If ctlLG.Location_Delete(e.CommandArgument) Then

                        ctlU.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_DEL, "Location", "ลบร้านยา:" & e.CommandArgument, "")

                        ctlU.User_DeleteByUsername(e.CommandArgument)
                        ctlU.User_GenLogfile(Request.Cookies("username").Value, "DEL", "User", "ลบ user :" & e.CommandArgument, "")
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ลบข้อมูลเรียบร้อย');", True)

                        LoadLocationToGrid()
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    End If

                Case "imgMail"
                    Dim dtU As New DataTable
                    Dim ctlU As New UserController
                    dtU = ctlU.User_GetByUsername(e.CommandArgument)
                    If dtU.Rows.Count > 0 Then
                        SendEmailUsername(String.Concat(dtU.Rows(0)("FirstName")) & " " & String.Concat(dtU.Rows(0)("LastName")), String.Concat(dtU.Rows(0)("Username")), enc.DecryptString(dtU.Rows(0)("Password"), True), String.Concat(dtU.Rows(0)("Email")))
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalSuccess(this,'Success','ส่งอีเมล์แจ้ง Username & Password เรียบร้อย');", True)
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','ไม่พบอีเมล์ที่จะส่ง');", True)
                    End If
            End Select


        End If
    End Sub


    Private Sub EditData(ByVal pID As String)
        Dim dtE As New DataTable

        dtE = ctlLG.Location_GetByID(pID)
        Dim objList As New LocationInfo
        If dtE.Rows.Count > 0 Then
            With dtE.Rows(0)
                isAdd = False
                cmdSendMail.Visible = True
                txtLicenseNo.Text = String.Concat(.Item("LicenseNo"))
                txtNHSOCode.Text = String.Concat(.Item("LocationCode"))
                txtLocationID.Text = DBNull2Str(dtE.Rows(0)("LocationID"))
                Me.hdID.Value = DBNull2Str(dtE.Rows(0)("LocationID"))
                txtLocationName.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f01_LocationName).fldName))
                txtLocationName2.Text = DBNull2Str(dtE.Rows(0)("LocationName2"))

                ddlType.SelectedValue = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f02_LocationGroupID).fldName))

                ddlTypeShop.SelectedValue = DBNull2Str(.Item("LocationType"))
                txtTypeName.Text = DBNull2Str(.Item("TypeOther"))
                txtYear.Text = DBNull2Str(.Item("RegisYear"))

                txtAddress.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f06_Address).fldName))
                ddlProvince.SelectedValue = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f07_ProvinceID).fldName))
                Me.txtZipCode.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f09_ZipCode).fldName))

                txtTel.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f10_Office_Tel).fldName))
                txtLineID.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f11_Office_Fax).fldName))
                txtCoName.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f12_Co_Name).fldName))

                txtCoMail.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f14_Co_Mail).fldName))
                txtCoTel.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f15_Co_Tel).fldName))

                'txtAccNo.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f16_AccNo).fldName))
                'txtAccName.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f17_AccName).fldName))
                'ddlBank.SelectedValue = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f18_BankID).fldName))
                'txtBrunch.Text = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f19_BankBrunch).fldName))
                'optBankType.SelectedValue = DBNull2Str(dtE.Rows(0)(objList.tblField(objList.fldPos.f20_BankType).fldName))

                'txtCardID.Text = DBNull2Str(dtE.Rows(0)("CardID"))

                If String.Concat(.Item("Lng")) <> "" Then
                    txtLat.Text = String.Concat(.Item("Lat")) & "," & String.Concat(.Item("Lng"))
                Else
                    txtLat.Text = String.Concat(.Item("Lat"))
                End If

                chkStatus.Checked = CBool(dtE.Rows(0)(objList.tblField(objList.fldPos.f23_isPublic).fldName))

                LoadPharmacist()
            End With

            chkProject.ClearSelection()
            Dim dtLP As New DataTable
            dtLP = ctlU.LocationProject_GetByLocationID(hdID.Value)

            If dtLP.Rows.Count > 0 Then
                For i = 0 To dtLP.Rows.Count - 1
                    Select Case dtLP.Rows(i)("ProjectID")
                        Case "1"
                            chkProject.Items(0).Selected = True
                        Case "2"
                            chkProject.Items(1).Selected = True
                        Case "3"
                            chkProject.Items(2).Selected = True
                    End Select
                Next
            End If
            dtLP = Nothing
        Else
            cmdSendMail.Visible = False
        End If
        dtE = Nothing
        objList = Nothing
    End Sub

    Protected Function validateData() As Boolean
        Dim result As Boolean = True
        lblValidate.Text = ""


        If hdID.Value = "" Then
            If ddlType.SelectedIndex = -1 Then
                result = False
                lblValidate.Text &= "- กรุณาเลือกประเภทร้านยา  <br />"
                lblValidate.Visible = True
            End If
        End If

        If txtLocationName.Text = "" Or txtLocationName2.Text = "" Then
            result = False
            lblValidate.Text &= "- กรุณาระบุชื่อร้านยาให้ครบถ้วน <br />"
            lblValidate.Visible = True
        End If
        If chkProject.Items(0).Selected = False And chkProject.Items(1).Selected = False And chkProject.Items(2).Selected = False And chkProject.Items(3).Selected = False Then
            result = False
            lblValidate.Text &= "- เลือกโครงการที่จะให้สิทธิ์ก่อน <br />"
            lblValidate.Visible = True

        End If

        'Dim n As Integer = 0
        'For i = 0 To optGroup.Items.Count - 1
        '    If optGroup.Items(i).Selected Then
        '        n = n + 1
        '    End If
        'Next

        'If n = 0 Then
        '    lblValidate.Text = "กรุณาเลือกประเภทร้านยา"
        '    lblValidate.Visible = True
        'End If

        Return result
    End Function


    Private Sub ClearData()
        Me.hdID.Value = ""
        hdNewID.Value = ""
        cmdSendMail.Visible = False
        txtLocationName.Text = ""
        txtLocationName2.Text = ""
        txtAddress.Text = ""
        ddlProvince.SelectedIndex = 1
        Me.txtZipCode.Text = ""
        chkStatus.Checked = True
        txtCoName.Text = ""
        txtCoMail.Text = ""
        'txtAccNo.Text = ""
        'txtAccName.Text = ""
        'txtBrunch.Text = ""
        txtCoTel.Text = ""
        txtTel.Text = ""
        txtLineID.Text = ""
        txtSearch.Text = ""
        txtLicenseNo.Text = ""
        txtNHSOCode.Text = ""
        GenLocationNumber()
    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(7).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If


    End Sub


    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click


        If validateData() Then

            lblValidate.Visible = False

            Dim LocationID As String = ""
            Dim item As Integer
            Dim sLat, sLng, sL() As String
            sLat = ""
            sLng = ""
            If txtLat.Text <> "" Then
                sL = Split(Replace(txtLat.Text, " ", ""), ",")
                sLat = sL(0)
                If sL.Length > 1 Then
                    sLng = sL(1)
                End If

                If IsNumeric(sLat) = False Or IsNumeric(sLng) = False Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุละติจูด/ลองติจูด เป็นรูปแบบองศาทศนิยมเท่านั้น ');", True)
                    Exit Sub
                End If
            End If

            If hdID.Value = "" Then

                LocationID = txtLocationID.Text

                item = ctlLG.Location_Add(LocationID, txtLocationName.Text, txtLocationName2.Text, ddlType.SelectedValue, ddlTypeShop.SelectedValue, txtTypeName.Text, txtAddress.Text, ddlProvince.SelectedValue, ddlProvince.SelectedItem.Text, txtZipCode.Text, txtTel.Text, txtLineID.Text, txtCoName.Text, txtCoMail.Text, txtCoTel.Text, Request.Cookies("username").Value, Boolean2Decimal(chkStatus.Checked), txtMail.Text, txtYear.Text, txtLicenseNo.Text, txtNHSOCode.Text, sLat, sLng)

                ctlU.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Locations", "เพิ่มใหม่ ร้านยา:" & txtLocationName.Text, "")

                item = ctlU.User_Add(txtLocationID.Text, enc.EncryptString("1234", True), txtLocationName.Text, ddlProvince.SelectedItem.Text, 0, 1, 1, txtLocationID.Text, "", 0, txtCoMail.Text)

                ctlU.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Users", "add new user :" & txtLocationID.Text, "Result:" & item)

                ctlU.RunningNumber_Update(ddlType.SelectedValue, ddlProvince.SelectedValue)
            Else
                LocationID = hdID.Value

                item = ctlLG.Location_Update(LocationID, txtLocationName.Text, txtLocationName2.Text, ddlType.SelectedValue, ddlTypeShop.SelectedValue, txtTypeName.Text, txtAddress.Text, ddlProvince.SelectedValue, ddlProvince.SelectedItem.Text, txtZipCode.Text, txtTel.Text, txtLineID.Text, txtCoName.Text, txtCoMail.Text, txtCoTel.Text, Request.Cookies("username").Value, Boolean2Decimal(chkStatus.Checked), txtMail.Text, txtYear.Text, txtLicenseNo.Text, txtNHSOCode.Text, sLat, sLng)

                ctlU.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Locations", "แก้ไข ร้านยา:" & txtLocationName.Text, "")

            End If


            For i = 0 To chkProject.Items.Count - 1
                ctlU.User_AddLocationProject(i + 1, LocationID, Boolean2StatusFlag(chkProject.Items(i).Selected))
            Next

            LoadLocationToGrid()
            ClearData()
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
        End If
    End Sub

    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadLocationToGrid()
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlType.SelectedIndexChanged
        GenLocationNumber()
    End Sub

    Protected Sub ddlProvince_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProvince.SelectedIndexChanged
        GenLocationNumber()
    End Sub

    Private Sub cmdSendMail_Click(sender As Object, e As EventArgs) Handles cmdSendMail.Click
        Dim dtU As New DataTable
        Dim ctlU As New UserController
        dtU = ctlU.User_GetByUsername(txtLocationID.Text)

        If dtU.Rows.Count > 0 Then
            SendEmailUsername(txtLocationName.Text, txtLocationID.Text, enc.DecryptString(dtU.Rows(0)("Password"), True), txtCoMail.Text)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalSuccess(this,'Success','ส่งอีเมล์แจ้ง Username & Password เรียบร้อย');", True)
        Else
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','ไม่พบอีเมล์ที่จะส่ง');", True)
        End If
    End Sub
    Private Sub SendEmailUsername(PersonName As String, Username As String, Password As String, sTo As String)
        Dim CC As String = ""
        If sTo = "" Then
            sTo = txtMail.Text
        Else
            CC = txtMail.Text
        End If

        If sTo = "" Then
            Exit Sub
        End If

        Dim SenderDisplayName As String = "มูลนิธิเภสัชกรรมชุมชน"
        Dim MySubject As String = "แจ้งข้อมูลการเข้าใช้งาน FCPPROJECT : " & PersonName
        Dim MyMessageBody As String = ""

        MyMessageBody = " <font size='4'>
    
  <p> เรียน คุณ " & PersonName & "</p>
  <p> เรื่อง แจ้งข้อมูลการเข้าใช้งานระบบฐานข้อมูลกิจกรรมบริการสร้างเสริมสุขภาพและการดูแลการใช้ยาฯโดยเภสัชกรชุมชน (FCPPROJECT)</p> 
     <p>ท่านสามารถเข้าใช้งานระบบได้ที่ <a href='https://www.fcpproject.com'>https://www.fcpproject.com</p> 
  <p>ข้อมูลผู้ใช้งานของท่าน</p>
        ชื่อผู้ใช้ (Username) : " & Username & " <br />
        รหัสผ่าน (Password) : " & Password & "  <br /> <br />

        <font color='#ff0000'> 
**อีเมล์นี้เป็นระบบอัตโนมัติ ห้ามตอบกลับใดๆทั้งสิ้น** </font><br />
        หากท่านต้องการติดต่อสอบถามข้อมูลเพิ่มเติมสามารถติดต่อได้ตามช่องทางต่อไปนี้ <br />
       
        <a href='https://compharmfund.or.th/site/page/contact_us'>มูลนิธิเภสัชกรรมชุมชน</a> <br />
        อีเมลล์ <a href='mailto:support@compharmfund.or.th'>support@compharmfund.or.th</a> <br />       

        <br /><br />
 
        ขอแสดงความนับถือ <br />
       
มูลนิธิเภสัชกรรมชุมชน<br />
40 ซอย สุขุมวิท 38 แขวงคลองเตย เขตคลองเตย กรุงเทพมหานคร 10110<br /> 
    </font>"


        Dim mailMessage As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage()
        mailMessage.From = New MailAddress("papcinfo@gmail.com", SenderDisplayName)
        mailMessage.Subject = MySubject
        mailMessage.Body = MyMessageBody
        mailMessage.IsBodyHtml = True
        mailMessage.[To].Add(New MailAddress(sTo))
        If CC <> "" Then
            mailMessage.CC.Add(New MailAddress(CC))
        End If

        Dim smtp As SmtpClient = New SmtpClient()
        smtp.Host = "smtp.gmail.com"
        smtp.EnableSsl = True
        Dim NetworkCred As NetworkCredential = New NetworkCredential()
        NetworkCred.UserName = mailMessage.From.Address
        NetworkCred.Password = "qktzlmsamzknnizq"
        smtp.UseDefaultCredentials = True
        smtp.Credentials = NetworkCred
        smtp.Port = 587
        smtp.Send(mailMessage)
    End Sub
End Class

