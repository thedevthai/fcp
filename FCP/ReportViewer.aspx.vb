﻿Imports System.Net
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
Imports System.IO
Imports System.Text

Public Class ReportViewer
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        UpdateProgress1.DisplayAfter = 0
        UpdateProgress1.Visible = True
        If Not IsPostBack Then
            LoadReport()
        End If
        'Response.AppendHeader("content-disposition", "attachment;filename=xxx.pdf")
        'Response.Charset = ""
        'Response.ContentType = "application/vnd.ms-pdf"
    End Sub
    Private Sub LoadReport()

        'System.Threading.Thread.Sleep(1000)
        'UpdateProgress1.Visible = True


        Dim credential As New ReportServerCredentials
        ReportViewer1.Reset()
        ReportViewer1.ServerReport.ReportServerCredentials = credential
        ReportViewer1.ServerReport.ReportServerUrl = credential.ReportServerUrl
        ReportViewer1.ShowPrintButton = True

        'Me.ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout)

        'System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("th-TH")
        Dim xParam As New List(Of ReportParameter)

        Select Case FagRPT

            'Case "CHKUP"
            '    If Reportskey = "TH" Then
            '        ReportViewer1.ServerReport.ReportPath = credential.ReportPath("CheckUpNoteTH")
            '    ElseIf Reportskey = "NE" Then
            '        ReportViewer1.ServerReport.ReportPath = credential.ReportPath("CheckUpReportTH")
            '    Else
            '        ReportViewer1.ServerReport.ReportPath = credential.ReportPath("CheckUpNoteEN")
            '    End If
            '    xParam.Add(New ReportParameter("P_OrganisationUID", "2"))
            '    xParam.Add(New ReportParameter("P_PatientUID", StrNull2Long(Session("PUID"))))
            '    xParam.Add(New ReportParameter("P_PatientVisitUID", StrNull2Long(Session("VNUID"))))
            Case "LocationGroup"
                'ReportViewer1.ServerReport.ReportPath = credential.ReportPath("LocationByGroup")
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("LocationDetail")
                xParam.Add(New ReportParameter("ProjectID", Request("pjid")))

            Case "PatientSummaryCount"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("PatientSummaryCount")
                xParam.Add(New ReportParameter("BYear", Request("y")))
            Case "SmokingPatientByForm"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("SmokingPatientByForm")
                xParam.Add(New ReportParameter("BYear", Request("y")))

            Case "NHSO"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("OrderNHSOLocationCount")
                xParam.Add(New ReportParameter("StartDate", Request("b")))
                xParam.Add(New ReportParameter("EndDate", Request("e")))
            Case "NHSO2"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("OrderNHSOServiceByPatient")
                xParam.Add(New ReportParameter("StartDate", Request("b")))
                xParam.Add(New ReportParameter("EndDate", Request("e")))
                xParam.Add(New ReportParameter("ProvinceID", Request("p")))
                'xParam.Add(New ReportParameter("RPTGRP", Request("grp")))

            Case "SmokingDetailA1", "SmokingDetailA4", "SmokingSummary", "SmokingFinance", "SmokingFinanceStopDay", "SmokingFinanceStopDate", "PatientQuitline"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportsName)
                Dim xBeginDate As Long = StrNull2Long(Request("b"))
                Dim xEndDate As Long = StrNull2Long(Request("e"))
                xParam.Add(New ReportParameter("StartDate", xBeginDate))
                xParam.Add(New ReportParameter("EndDate", xEndDate))
            Case "SmokingDetailA5"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportsName)
                Dim xBeginDate As Long = StrNull2Long(Request("b"))
                Dim xEndDate As Long = StrNull2Long(Request("e"))
                xParam.Add(New ReportParameter("StartDate", xBeginDate))
                xParam.Add(New ReportParameter("EndDate", xEndDate))
                xParam.Add(New ReportParameter("FollowSEQ", StrNull2Zero(Request("seq"))))
            Case "SummaryServiceCount"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("SummaryServiceCount")
                xParam.Add(New ReportParameter("StartDate", ConvertStrDate2DBString(Request("b"))))
                xParam.Add(New ReportParameter("EndDate", ConvertStrDate2DBString(Request("e"))))
                xParam.Add(New ReportParameter("ProjectID", Request("pj")))
            Case "VMI"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportsName)
                xParam.Add(New ReportParameter("StartDate", Request("b")))
                xParam.Add(New ReportParameter("EndDate", Request("e")))
                xParam.Add(New ReportParameter("LocationID", Request("l")))
            Case "VMI2"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportsName)
                xParam.Add(New ReportParameter("ProvinceID", Request("pv")))

            Case "MTM"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportsName)
                xParam.Add(New ReportParameter("StartDate", ConvertStrDate2DBString(Request("b"))))
                xParam.Add(New ReportParameter("EndDate", ConvertStrDate2DBString(Request("e"))))
                Reportskey = "XLS"
            Case "HV"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportsName)
                If Request("rpt") = "mtmcount" Then
                    xParam.Add(New ReportParameter("MTMTYPE", "MTM"))
                Else
                    xParam.Add(New ReportParameter("MTMTYPE", "HV"))
                End If

                xParam.Add(New ReportParameter("StartDate", ConvertStrDate2DBString(Request("b"))))
                xParam.Add(New ReportParameter("EndDate", ConvertStrDate2DBString(Request("e"))))
                Reportskey = "XLS"
            Case "DR"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportsName)
                xParam.Add(New ReportParameter("StartDate", ConvertStrDate2DBString(Request("b"))))
                xParam.Add(New ReportParameter("EndDate", ConvertStrDate2DBString(Request("e"))))
                xParam.Add(New ReportParameter("ProvinceID", Request("p")))
                Reportskey = "XLS"

            Case "MetabolicRisk"
                ReportViewer1.ServerReport.ReportPath = credential.ReportPath("MetabolicRisk")
                xParam.Add(New ReportParameter("StartDate", ConvertStrDate2DBString(Request("b"))))
                xParam.Add(New ReportParameter("EndDate", ConvertStrDate2DBString(Request("e"))))
                xParam.Add(New ReportParameter("ProvinceID", Request("p")))
                Reportskey = "XLS"
        End Select

        ReportViewer1.ServerReport.SetParameters(xParam)

        If Reportskey <> "XLS" Then
            ' Variables
            Dim warnings As Warning()
            Dim streamIds As String()
            Dim mimeType As String = String.Empty
            Dim encoding As String = String.Empty
            Dim extension As String = String.Empty

            ' Setup the report viewer object and get the array of bytes

            Dim bytes As Byte() = ReportViewer1.ServerReport.Render("PDF", Nothing, mimeType, encoding, extension, streamIds, Nothing)

            ' Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
            Response.Buffer = True
            Response.Clear()
            Response.ContentType = mimeType
            'Response.AddHeader("content-disposition", Convert.ToString("attachment; filename=C:\ttt.pdf"))
            Response.BinaryWrite(bytes)
            ' create the file
            'Response.Flush()
            ' send it to the client to download
        Else
            ' Variables
            Dim warnings As Warning()
            Dim streamIds As String()
            Dim mimeType As String = String.Empty
            Dim encoding As String = String.Empty
            Dim extension As String = String.Empty


            ' Setup the report viewer object and get the array of bytes

            Dim bytes As Byte() = ReportViewer1.ServerReport.Render("EXCEL", Nothing, mimeType, encoding, extension, streamIds, Nothing)

            ' Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
            Response.Buffer = True
            Response.Clear()
            Response.ContentType = mimeType
            Response.AddHeader("content-disposition", Convert.ToString("attachment; filename=CPA_" & FagRPT & "_" & ReportsName & "." & extension))
            Response.BinaryWrite(bytes)
            ' create the file
            Response.Flush()
            ' send it to the client to download
        End If

    End Sub


End Class