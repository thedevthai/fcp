﻿Public Class ICD10
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim ctlM As New ICDController
    Dim acc As New UserController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then
            grdData.PageIndex = 0
            ClearData()
            LoadData()
        End If
    End Sub

    Private Sub LoadData()
        dt = ctlM.ICD10_GetAll
        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"
                    ctlM.ICD10_Delete(e.CommandArgument)
                    acc.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_DEL, "ICD10", "Delete  ICD10:" & lblUID.Text & ">>" & txtName.Text, "")
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ลบข้อมูลเรียบร้อย');", True)
                    grdData.PageIndex = 0
                    LoadData()
            End Select


        End If
    End Sub


    Private Sub EditData(ByVal pID As String)

        dt = ctlM.ICD10_GetByID(pID)

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                Me.lblUID.Text = DBNull2Str(dt.Rows(0)("UID"))
                txtCode.Text = String.Concat(dt.Rows(0)("Code"))
                txtName.Text = DBNull2Str(dt.Rows(0)("Name"))
                txtDescription.Text = String.Concat(dt.Rows(0)("Description"))
                txtSort.Text = String.Concat(dt.Rows(0)("Sort"))
                chkActive.Checked = ConvertStatusFlag2Boolean(dt.Rows(0)("StatusFlag"))
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub ClearData()
        Me.lblUID.Text = ""
        txtName.Text = ""
        txtCode.Text = ""
        txtDescription.Text = ""
        txtSort.Text = "0"
        chkActive.Checked = True

    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(6).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub


    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If txtName.Text = "" Then

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอกข้อมูลให้ครบถ้วน');", True)
            Exit Sub
        End If
        Dim item As Integer

        If lblUID.Text = "" Then

            ctlM.ICD10_Add(txtCode.Text, txtName.Text, txtDescription.Text, StrNull2Zero(txtSort.Text), "", Boolean2StatusFlag(chkActive.Checked))

            acc.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "ICD10s", "Add new  ICD10:" & txtName.Text, "")

        Else
            ctlM.ICD10_Update(lblUID.Text, txtCode.Text, txtName.Text, txtDescription.Text, StrNull2Zero(txtSort.Text), "", Boolean2StatusFlag(chkActive.Checked))

            acc.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "ICD10s", "Update  ICD10:" & txtName.Text, "")

        End If
        grdData.PageIndex = 0
        LoadData()
        ClearData()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

    End Sub

    Protected Sub grdData_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadData()
    End Sub
End Class

