﻿Imports Microsoft.ApplicationBlocks.Data
Public Class ProblemController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Public Function ProblemGroup_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("ProblemGroup_Get"))
        Return ds.Tables(0)
    End Function

    Public Function ProblemItem_Get(pGroupUID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("ProblemItem_Get"), pGroupUID)
        Return ds.Tables(0)
    End Function

    Public Function BehaviorProblem_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("BehaviorProblem_Get"))
        Return ds.Tables(0)
    End Function

End Class
