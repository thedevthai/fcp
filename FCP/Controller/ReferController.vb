﻿Imports Microsoft.ApplicationBlocks.Data

Public Class ReferController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Public Function Refer_Add(ReferDate As String, ByVal ReferType As String, ByVal PatientID As Integer, Destination As String, ByVal ReferFrom As String, Remark As String, ReferBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Refer_Add"), ConvertStrDate2InformDateString(ReferDate) _
           , ReferType _
           , PatientID _
           , Destination _
           , ReferFrom _
           , Remark _
           , ReferBy)
    End Function

    Public Function Refer_CheckDup(ByVal PatientID As Integer, ReferType As String) As Boolean

        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Refer_CheckDup"), PatientID, ReferType)
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)(0) > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Public Function Refer_GetLocation(ByVal PatientID As Integer) As String

        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Refer_GetLocation"), PatientID)

        Return ds.Tables(0).Rows(0)(0).ToString()

    End Function
End Class
