﻿Imports Microsoft.ApplicationBlocks.Data
Public Class PersonController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Public Function GetPerson() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Person_Get")
        Return ds.Tables(0)
    End Function

    'Public Function GetPersonName(pCode As Integer) As String
    '    Dim strName As String
    '    SQL = "select  FirstName,LastName  from  Persons  Where PersonID= " & pCode
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
    '    strName = String.Concat(ds.Tables(0).Rows(0)(0)) & " " & String.Concat(ds.Tables(0).Rows(0)(1))
    '    Return strName
    'End Function

    Public Function GetPerson_ByID(id As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Person_GetByID", id)
        Return ds.Tables(0)
    End Function

    Public Function GetPerson_BySearch(id As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Person_GetBySearch", id)
        Return ds.Tables(0)

        'SQL = "select * from View_Persons where PersonID like '%" & id & "%' OR  FirstName like '%" & id & "%' OR  LocationID like '%" & id & "%' OR  LocationName like '%" & id & "%'"
        'ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        'Return ds.Tables(0)
    End Function

    'Public Function GetPerson_notUserBySearch(ptype As String, pkey As String) As DataTable
    '    SQL = "SELECT     *  FROM    Persons WHERE  (UserID is null) and PersonType='" & ptype & "' and  PersonID like '%" & pkey & "%' OR  FirstName like '%" & pkey & "%'"
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
    '    Return ds.Tables(0)
    'End Function

    Public Function GetPerson_ByLocation(id As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Person_GetByLocation", id)
        Return ds.Tables(0)
    End Function

    Public Function GetPerson_ByLocationSearch(Lid As String, pKey As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Person_GetByLocationSearch", Lid, pKey)
        Return ds.Tables(0)
    End Function

    'Public Function Person_GetLocationByUserID(id As Integer) As Integer
    '    SQL = "select LocationID from  Persons  where UserID=" & id
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
    '    Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    'End Function

    'Public Function GetLocation_ByPersonID(id As Integer) As Integer
    '    SQL = "select LocationID  from Persons where PersonID=" & id
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
    '    Return DBNull2Zero(ds.Tables(0).Rows(0)(0))

    'End Function


    Public Function Person_Add(ByVal PrefixID As Integer, ByVal FirstName As String, ByVal LastName As String, PositionName As String, LocationID As String, UpdateBy As String, LicenseNo As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Person_Add"), PrefixID, FirstName, LastName, PositionName, LocationID, UpdateBy, LicenseNo)
    End Function

    Public Function Person_AddUser(ByVal pUserName As String, ByVal pFName As String, ByVal pLName As String, pMail As String, ByVal pType As String, pLocation As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Person_AddUser"), pUserName, pFName, pLName, pMail, pType, pLocation)
    End Function

    Public Function Person_Update(ByVal PersonID As Integer, ByVal PrefixID As Integer, ByVal FirstName As String, ByVal LastName As String, PositionName As String, LocationID As String, UpdateBy As String, LicenseNo As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Person_Update"),
      PersonID _
      , PrefixID _
      , FirstName _
      , LastName _
      , PositionName _
      , LocationID _
      , UpdateBy, LicenseNo)
    End Function

    Public Function Person_UpdateUserIDandMail(ByVal PersonID As Integer, userid As Integer, mail As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Person_UpdateUserIDandMail", PersonID, userid, mail)

    End Function

    Public Function Person_Delete(ByVal pID As Integer) As Integer
        SQL = "delete from Persons where Personid =" & pID
        Return SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, SQL)
    End Function


End Class
