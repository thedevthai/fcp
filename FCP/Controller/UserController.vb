﻿Imports Microsoft.ApplicationBlocks.Data

Public Class UserController
    Inherits ApplicationBaseClass
    Public ds As New DataSet

    Public Function User_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("User_Get"))
        Return ds.Tables(0)
    End Function

    Public Function User_CheckLogin(ByVal pUsername As String, ByVal pPassword As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("User_Login"), pUsername, pPassword)
        Return ds.Tables(0)
    End Function
    Public Function User_GetByProjectID(ByVal pID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("User_GetByLocationID"), pID)
        Return ds.Tables(0)
    End Function

    Public Function User_GetProjectRole(ByVal pID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("User_GetProjectRole"), pID)
        Return ds.Tables(0)
    End Function
    Public Function User_GetProjectID(ByVal pID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("User_GetProjectID"), pID)
        Return ds.Tables(0)
    End Function
    Public Function User_GetByID(ByVal pID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("User_GetByID"), pID)
        Return ds.Tables(0)
    End Function

    Public Function User_GetByUsername(ByVal Username As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("User_GetByUsername"), Username)
        Return ds.Tables(0)
    End Function

    Public Function GetUsers_ByUsername(id As String) As DataTable
        SQL = "select * from Users where username ='" & id & "'"
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function

    Public Function User_GetEmail(pKey As String) As DataTable
        SQL = "select *  from  Users where username ='" & pKey & "' Or Email='" & pKey & "'"
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function

    Public Function GetUsersID_ByUsername(id As String) As Integer
        SQL = "select UserID  from Users where username ='" & id & "'"
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If

    End Function
    Public Function GetUsers_Online() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "UserLog_Online")
        Return ds.Tables(0)
    End Function

    Public Function UserLogFile_GetVisitCount() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("UserLogFile_GetVisitCount"))
        Return ds.Tables(0)
    End Function

    Public Function GetUsers_BySearch(KeySearch As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Users_GetBySearch", KeySearch)
        Return ds.Tables(0)
    End Function

    Public Function GetUsers_ByGroupSearch(proj As Integer, Grp As Integer, KeySearch As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Users_GetByGroupSearch", proj, Grp, KeySearch)
        Return ds.Tables(0)
    End Function

    Public Function LocationProject_GetByLocationID(LID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "LocationProject_GetByLocationID", LID)
        Return ds.Tables(0)
    End Function
    Public Function LocationProject_GetByUsername(username As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "LocationProject_GetByUsername", username)
        Return ds.Tables(0)
    End Function
    Public Function User_GetName(ByVal username As String) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("User_GetName"), username)
        Return ds.Tables(0).Rows(0)(0)
    End Function

    'Public Function User_GetActionByID(ByVal pID As String) As String
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, ("User_GetActionByID"), pID)
    '    Return ds.Tables(0).Rows(0)(0)
    'End Function


    Public Function User_LastLog_Update(ByVal pUsername As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("User_LastLog_Update"), pUsername)
    End Function
    Public Function User_Delete(ByVal pID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("User_Delete"), pID)
    End Function

    Public Function User_DeleteByUsername(ByVal pID As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("User_DeleteByUsername"), pID)
    End Function


    Public Function User_Add(ByVal Username As String, ByVal Password As String, ByVal FName As String, ByVal LName As String, ByVal IsSuperUser As Integer, ByVal Status As Integer, ByVal RoleID As Integer, ByVal LocationID As String, ByVal rptGrp As String, mngProj As Integer, ByVal Email As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("User_Add"), Username, Password, FName, LName, IsSuperUser, Status, RoleID, LocationID, rptGrp, mngProj, Email)
    End Function

    Public Function User_Update(ByVal Username As String, ByVal Password As String, ByVal FName As String, ByVal LName As String, ByVal IsSuperUser As Integer, ByVal Status As Integer, ByVal RoleID As Integer, ByVal LocationID As String, ByVal rptGrp As String, MngProj As Integer, ByVal Email As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("User_Update"), Username, Password, FName, LName, IsSuperUser, Status, RoleID, LocationID, rptGrp, MngProj, Email)
    End Function

    Public Function UserRole_Add(ByVal Username As String, ByVal RoleID As Integer, ByVal UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("UserRole_Add"), Username, RoleID, UpdBy)
    End Function

    Public Function User_UpdateProfileID(ByVal Username As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("User_UpdateProfileID"), Username)
    End Function



    Public Function UserRole_UpdateStatus(Username As String, RoleID As Integer, bActive As Integer, UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("UserRole_UpdateStatus"), Username, RoleID, bActive, UpdBy)
    End Function

    Public Function User_ChangePassword(ByVal Username As String, ByVal Password As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("User_ChangePassword"), Username, Password)
    End Function

    Public Function User_GenLogfile(ByVal Username As String, ByVal Act_Type As String, DB_Effective As String, Descrp As String, Remark As String) As Integer
        'Return 1
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("User_genLogfile"), Username, Act_Type, DB_Effective, Descrp, Remark)
    End Function

    Public Function User_AddLocationProject(ByVal ProjectID As Integer, ByVal LocationID As String, StatusFlag As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("User_AddLocationProject"), ProjectID, LocationID, StatusFlag)
    End Function

    Public Function User_UpdateMail(ByVal Username As String, email As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("User_UpdateEmail"), Username, email)
    End Function

    Public Function UserLogfile_UpdateByLicenseNo(ByVal LicenseNo As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("UserLogfile_UpdateByLicenseNo"), LicenseNo)
    End Function

End Class
Public Class UserRoleController
    Inherits ApplicationBaseClass
    Public ds As New DataSet

    Public Function UserRole_GetByUserID(ByVal pUserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("UserRole_GetByUserID"), pUserID)
        Return ds.Tables(0)
    End Function

    Public Function UserRole_GetActiveRoleByUID(ByVal pUserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("UserRole_GetActiveRoleByUID"), pUserID)
        Return ds.Tables(0)
    End Function


    Public Function UserAction_CheckByUser(ByVal pUser As String, ByVal pLocation As Integer) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("UserAction_CheckByUser"), pUser, pLocation)

        If ds.Tables(0).Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function UserAction_Checked(ByVal pUser As String, ByVal pLocation As Integer) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("UserAction_Checked"), pUser, pLocation)

        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)(0) = 1 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function




    Public Function UserAction_Add(ByVal Username As String, ByVal LocationID As Integer, ByVal RoleAction As Integer, ByVal UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("UserAction_Add"), Username, LocationID, RoleAction, UpdBy)
    End Function
    Public Function UserAction_Update(ByVal Username As String, ByVal LocationID As Integer, ByVal RoleAction As Integer, ByVal UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, ("UserAction_Update"), Username, LocationID, RoleAction, UpdBy)
    End Function


End Class
