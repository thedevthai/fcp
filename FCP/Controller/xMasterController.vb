﻿Imports Microsoft.ApplicationBlocks.Data
Public Class MasterController
    Inherits BaseClass
    Dim ds As New DataSet
    Public Function Province_GetAll() As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Province_GetAll"))
            Return ds.Tables(0)
        End Function

        Public Function Province_Get() As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Province_Get"))
            Return ds.Tables(0)
        End Function

        Public Function Province_GetByUID(ByVal pUID As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Province_GetByUID"), CObj(pUID))
            Return ds.Tables(0)
        End Function

        Public Function Province_GetBySearch(ByVal pKey As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Province_GetBySearch"), pKey)
            Return ds.Tables(0)
        End Function

        Public Function Province_Add(ByVal ProvinceCode As String, ByVal Name As String, ByVal Sort As Integer, ByVal Status As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Province_Add"), ProvinceCode, Name, CObj(Sort), Status)
        End Function

        Public Function Province_Update(ByVal ProvinceUID As Integer, ByVal ProvinceCode As String, ByVal Name As String, ByVal Sort As Integer, ByVal status As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Province_Update"), CObj(ProvinceUID), ProvinceCode, Name, CObj(Sort), status)
        End Function

        Public Function Province_Delete(ByVal ProvinceUID As Integer) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Province_Delete"), CObj(ProvinceUID))
        End Function

        Public Function Division_GetAll() As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Division_GetAll"))
            Return ds.Tables(0)
        End Function

        Public Function Division_Get() As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Division_Get"))
            Return ds.Tables(0)
        End Function

        Public Function Division_GetByUID(ByVal pUID As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Division_GetByUID"), CObj(pUID))
            Return ds.Tables(0)
        End Function

        Public Function Division_GetBySearch(ByVal pKey As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Division_GetBySearch"), pKey)
            Return ds.Tables(0)
        End Function

        Public Function Division_Add(ByVal DivisionCode As String, ByVal Name As String, ByVal Sort As Integer, ByVal Status As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Division_Add"), DivisionCode, Name, CObj(Sort), Status)
        End Function

        Public Function Division_Update(ByVal DivisionUID As Integer, ByVal DivisionCode As String, ByVal Name As String, ByVal Sort As Integer, ByVal status As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Division_Update"), CObj(DivisionUID), DivisionCode, Name, CObj(Sort), status)
        End Function

        Public Function Division_Delete(ByVal DivisionUID As Integer) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Division_Delete"), CObj(DivisionUID))
        End Function

        Public Function Department_GetByDivisionUID(ByVal DivisionUID As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Department_GetByDivisionUID"), CObj(DivisionUID))
            Return ds.Tables(0)
        End Function

        Public Function Department_GetAll() As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Department_GetAll"))
            Return ds.Tables(0)
        End Function

        Public Function Department_Get() As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Department_Get"))
            Return ds.Tables(0)
        End Function

        Public Function Department_GetByUID(ByVal pUID As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Department_GetByUID"), CObj(pUID))
            Return ds.Tables(0)
        End Function

        Public Function Department_GetBySearch(ByVal pKey As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Department_GetBySearch"), pKey)
            Return ds.Tables(0)
        End Function

        Public Function Department_Add(ByVal DepartmentCode As String, ByVal Name As String, ByVal Sort As Integer, ByVal Status As String, ByVal DeptUID As Integer) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Department_Add"), DepartmentCode, Name, CObj(Sort), Status, CObj(DeptUID))
        End Function

        Public Function Department_Update(ByVal DepartmentUID As Integer, ByVal DepartmentCode As String, ByVal Name As String, ByVal Sort As Integer, ByVal status As String, ByVal DeptUID As Integer) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Department_Update"), CObj(DepartmentUID), DepartmentCode, Name, CObj(Sort), status, CObj(DeptUID))
        End Function

        Public Function Department_Delete(ByVal DepartmentUID As Integer) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Department_Delete"), CObj(DepartmentUID))
        End Function

        Public Function Position_GetActive() As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Position_GetActive"))
            Return ds.Tables(0)
        End Function

        Public Function Position_GetAll() As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Position_GetAll"))
            Return ds.Tables(0)
        End Function

        Public Function Position_Get() As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Position_Get"))
            Return ds.Tables(0)
        End Function

        Public Function Position_GetByUID(ByVal pUID As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Position_GetByUID"), CObj(pUID))
            Return ds.Tables(0)
        End Function

        Public Function Position_GetBySearch(ByVal pKey As String) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Position_GetBySearch"), pKey)
            Return ds.Tables(0)
        End Function

        Public Function Position_Add(ByVal PositionCode As String, ByVal Name As String, ByVal Sort As Integer, ByVal Status As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Position_Add"), PositionCode, Name, CObj(Sort), Status)
        End Function

        Public Function Position_Update(ByVal PositionUID As Integer, ByVal PositionCode As String, ByVal Name As String, ByVal Sort As Integer, ByVal status As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Position_Update"), CObj(PositionUID), PositionCode, Name, CObj(Sort), status)
        End Function

        Public Function Position_Delete(ByVal PositionUID As Integer) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Position_Delete"), CObj(PositionUID))
        End Function

        Public Function DataConfig_GetByCode(ByVal pid As String) As String
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("DataConfig_GetByCode"), pid)
        Return DBNull2Str(ds.Tables(0).Rows(0)(0))
    End Function

        Public Function DataConfig_Add(ByVal pCode As String, ByVal pYear As String, ByVal pNo As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("DataConfig_Add"), pCode, pYear, pNo)
        End Function

        Public Function DataConfig_Update(ByVal pCode As String, ByVal pYear As String, ByVal pNo As String) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("DataConfig_Update"), pCode, pYear, pNo)
        End Function

        Public Function Running_Get() As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Running_Get"))
            Return ds.Tables(0)
        End Function

        Public Function Running_GetByCode(ByVal pid As String, ByVal pYear As Integer) As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Running_GetByCode"), pid, CObj(pYear))
            Return ds.Tables(0)
        End Function

        Public Function Running_Add(ByVal pCode As String, ByVal pYear As Integer, ByVal pNo As Integer) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Running_Add"), pCode, CObj(pYear), CObj(pNo))
        End Function

        Public Function Running_Update(ByVal pCode As String, ByVal pYear As Integer, ByVal pNo As Integer) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Running_Update"), pCode, CObj(pYear), CObj(pNo))
        End Function

        Public Function Running_Delete(ByVal pCode As String, ByVal pYear As Integer) As Integer
            Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Running_Delete"), pCode, CObj(pYear))
        End Function

        Public Function YearPermit_Get() As DataTable
            ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("YearPermit"))
            Return ds.Tables(0)
        End Function


    End Class
