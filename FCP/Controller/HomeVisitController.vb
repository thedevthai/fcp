﻿Imports Microsoft.ApplicationBlocks.Data
Public Class HomeVisitController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

#Region "Home Visit 2560"

    Public Function H01_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "H01_GetByUID", pUID)
        Return ds.Tables(0)
    End Function
    Public Function H01_GetLastInfo(BYear As Integer, PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "H01_GetLastInfo", BYear, PatientID)
        Return ds.Tables(0)
    End Function
    Public Function H01_GetBYear(UID As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "H01_GetBYear", UID)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function

    Public Function H01_GetByProvinceGroup(pGRPID As String, pType As String, pKey As String, pStatus As Integer, pProv As String, Optional pYear As Integer = 0) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "H01_GetByProvinceGroup", pGRPID, pType, pKey, pStatus, pProv, pYear)


        Return ds.Tables(0)
    End Function

    'Public Function H01_GetByProjectID(pProjID As Integer, pType As String, pKey As String, pStatus As Integer, pProv As String, Optional pYear As Integer = 0) As DataTable
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, "H01_GetByProjectID", pProjID, pType, pKey, pStatus, pProv, pYear)
    '    Return ds.Tables(0)
    'End Function

    Public Function H01_GetTypeIDByItemID(id As Integer, pid As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Smoking_GetTypeIDByItemID", id, pid)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Str(ds.Tables(0).Rows(0)(0))
        Else
            Return ""
        End If
    End Function

    Public Function H01_GetPatientIDByItemID(pID As Long) As Long
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("H01_GetPatientID"), pID)
        Try
            If ds.Tables(0).Rows.Count > 0 Then
                Return DBNull2Lng(ds.Tables(0).Rows(0)(0))
            Else
                Return 0
            End If
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Public Function H01_GetLastUID(ByVal LocationID As String _
           , ByVal BYear As Integer _
           , ByVal PatientID As Integer _
           , ByVal ServiceDate As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "H01_GetLastUID", LocationID, BYear, PatientID, ServiceDate)

        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function H01_GetLastSEQ(ByVal LocationID As String _
           , ByVal BYear As Integer _
           , ByVal PatientID As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "H01_GetLastSEQ", LocationID, BYear, PatientID)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function


    Public Function H01_ChkDupCustomer(PatientID As Integer, ServiceDate As Integer) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("H01_ChkDupCustomer"), PatientID, ServiceDate)
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)(0) > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Public Function H01_GetByStatus(pLID As String, pType As String, pKey As String, pStatus As Integer, pProv As String, Optional pYear As Integer = 0) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "H01_GetByStatus", pLID, pType, pKey, pStatus, pProv, pYear)
        Return ds.Tables(0)
    End Function

    Public Function H01_Delete(itemID As Long) As Integer
        SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("H01_Delete"), itemID)
    End Function


    Public Function H01_Add(
             ByVal LocationID As String _
           , ByVal BYear As Integer _
           , ByVal PatientID As Integer _
           , ByVal ServiceDate As Integer _
           , ByVal ServiceTime As Integer _
           , ByVal PersonID As Integer _
           , ByVal Smoke As Integer _
           , ByVal SmokeYear As Integer _
           , ByVal SmokeCigarette As Integer _
           , ByVal CigaretteType As Integer _
           , ByVal Alcohol As Integer _
           , ByVal AlcoholFQ As Integer _
           , ByVal Status As Integer _
           , ByVal CreateBy As String, ByVal SEQ As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "H01_Add", LocationID, BYear, PatientID, ServiceDate, ServiceTime, PersonID, Smoke, SmokeYear, SmokeCigarette, CigaretteType, Alcohol, AlcoholFQ, Status, CreateBy, SEQ)

    End Function
    Public Function H01_Update(ByVal UID As Integer,
             ByVal LocationID As String _
           , ByVal BYear As Integer _
           , ByVal PatientID As Integer _
           , ByVal ServiceDate As Integer _
           , ByVal ServiceTime As Integer _
           , ByVal PersonID As Integer _
           , ByVal Smoke As Integer _
           , ByVal SmokeYear As Integer _
           , ByVal SmokeCigarette As Integer _
           , ByVal CigaretteType As Integer _
           , ByVal Alcohol As Integer _
           , ByVal AlcoholFQ As Integer _
           , ByVal Status As Integer _
           , ByVal CreateBy As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "H01_Update", UID, LocationID, BYear, PatientID, ServiceDate, ServiceTime, PersonID, Smoke, SmokeYear, SmokeCigarette, CigaretteType, Alcohol, AlcoholFQ, Status, CreateBy)

    End Function

    Public Function H01_UpdatePE(ByVal UID As Integer,
             ByVal isPitting As String _
           , ByVal isWound As String _
           , ByVal isPeripheral As String _
           , ByVal Pitting As String _
           , ByVal Wound As String _
           , ByVal Peripheral As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "H01_UpdatePE", UID, isPitting, isWound, isPeripheral, Pitting, Wound, Peripheral)

    End Function

#End Region

#Region "LAB"
    Public Function LabResult1_Get(HUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "LabResult1_Get", HUID)
        Return ds.Tables(0)
    End Function
    Public Function LabResult2_Get(HUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "LabResult2_Get", HUID)
        Return ds.Tables(0)
    End Function

    Public Function LabResult_Save(
            ByVal RefUID As Integer _
          , ByVal LabUID As Integer _
          , ByVal ResultDate As Integer _
          , ByVal PatientID As Integer _
          , ByVal ResultValue As String _
          , ByVal CUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LabResult_Save", RefUID, LabUID, ResultDate, PatientID, ResultValue, CUser)

    End Function


#End Region



End Class
