﻿Imports Microsoft.ApplicationBlocks.Data
Public Class DeseaseController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Public Function Desease_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Desease_Get"))
        Return ds.Tables(0)
    End Function

    Public Function Desease_GetAll() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Desease_Get"))
        Return ds.Tables(0)
    End Function

    Public Function Desease_GetByID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Desease_GetByID", UID)
        Return ds.Tables(0)
    End Function
    Public Function Desease_GetBySearch(Search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Desease_GetSearch", Search)
        Return ds.Tables(0)
    End Function

    Public Function Desease_GetName(Code As String) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Desease_GetName", Code)
        If ds.Tables(0).Rows.Count > 0 Then
            Return String.Concat(ds.Tables(0).Rows(0)(0))
        Else
            Return ""
        End If
    End Function
    Public Function Desease_IsHas(Code As String) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Desease_GetByCode", Code)
        If ds.Tables(0).Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function Desease_Add(Code As String, Name As String, Desc As String, Sort As Integer, isICD As String, Status As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Desease_Add", Code, Name, Desc, Sort, isICD, Status)
    End Function
    Public Function Desease_Update(UID As Integer, Code As String, Name As String, Desc As String, Sort As Integer, isICD As String, Status As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Desease_Update", UID, Code, Name, Desc, Sort, isICD, Status)
    End Function

    Public Function Desease_Delete(UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Desease_Delete", UID)
    End Function

End Class


Public Class ICDController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Public Function ICD10_GetAll() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("ICD10_GetAll"))
        Return ds.Tables(0)
    End Function

    Public Function ICD10_GetByID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "ICD10_GetByID", UID)
        Return ds.Tables(0)
    End Function


    Public Function ICD10_GetBySearch(Search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "ICD10_GetSearch", Search)
        Return ds.Tables(0)
    End Function

    Public Function ICD10_GetName(Code As String) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, "ICD10_GetName", Code)
        If ds.Tables(0).Rows.Count > 0 Then
            Return String.Concat(ds.Tables(0).Rows(0)(0))
        Else
            Return ""
        End If
    End Function
    Public Function ICD10_IsHas(Code As String) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, "ICD10_GetByCode", Code)
        If ds.Tables(0).Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function ICD10_Add(Code As String, Name As String, Desc As String, Sort As Integer, isICD As String, Status As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "ICD10_Add", Code, Name, Desc, Sort, isICD, Status)
    End Function
    Public Function ICD10_Update(UID As Integer, Code As String, Name As String, Desc As String, Sort As Integer, isICD As String, Status As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "ICD10_Update", UID, Code, Name, Desc, Sort, isICD, Status)
    End Function
    Public Function ICD10_Delete(UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "ICD10_Delete", UID)
    End Function

End Class