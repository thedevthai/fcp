﻿Imports Microsoft.ApplicationBlocks.Data
Public Class CovidController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

#Region "COVID"
    Public Function COVID_Patient_Get(LocationID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "COVID_Patient_Get", LocationID)
        Return ds.Tables(0)
    End Function

    Public Function COVID_Head_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "COVID_Head_GetByUID", pUID)
        Return ds.Tables(0)
    End Function
    Public Function COVID_Head_Get(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "COVID_Head_Get", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function COVID_Head_GetLastUID(ByVal LocationID As String _
           , ByVal PatientID As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "COVID_Head_GetLastUID", LocationID, PatientID)

        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function COVID_Head_ChkDupCustomer(PatientID As Integer) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("COVID_Head_ChkDupCustomer"), PatientID)
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)(0) > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Public Function COVID_Head_Save(ByVal UID As Integer _
           , ByVal LocationID As String _
           , ByVal PatientID As Integer _
           , ByVal TestDate As Integer _
           , ByVal ConfirmDate As Integer _
           , ByVal IsHomeIsolation As String _
           , ByVal AdmitDate As Integer _
           , ByVal IsMedicine As String _
           , ByVal Medicine As String _
           , ByVal IsTempHospital As String _
           , ByVal TempHospitalName As String _
           , ByVal IsHospitel As String _
           , ByVal HospitalName As String _
           , ByVal FinalResult As String _
           , ByVal ResultRemark As String _
           , ByVal LabResult As String _
           , ByVal CloseStatus As String _
           , ByVal isSmoke As String _
           , ByVal SmokeQTY As String _
           , ByVal Disease0 As String _
           , ByVal Disease1 As String _
           , ByVal Disease2 As String _
           , ByVal Disease3 As String _
           , ByVal Disease4 As String _
           , ByVal Disease5 As String _
           , ByVal Disease6 As String _
           , ByVal Disease7 As String _
           , ByVal Disease8 As String _
           , ByVal Disease9 As String _
           , ByVal Disease10 As String _
           , ByVal Disease11 As String _
           , ByVal Disease12 As String _
           , ByVal Disease99 As String _
           , ByVal DiseaseOther As String _
           , ByVal SmokeFinal As String _
           , ByVal SmokeBegin As String _
           , ByVal SmokeEnd As String _
           , ByVal IsA1A5 As String _
           , ByVal ReasonA1A5 As String _
           , ByVal IsSmokingFollow As String _
           , ByVal IsMTMFollow As String _
           , ByVal IsOtherFollow As String _
           , ByVal FormOther As String _
           , ByVal MedicineOther As String
        ) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "COVID_Head_Save", UID, LocationID _
      , PatientID _
      , TestDate _
      , ConfirmDate _
      , IsHomeIsolation _
      , AdmitDate _
      , IsMedicine _
      , Medicine _
      , IsTempHospital _
      , TempHospitalName _
      , IsHospitel _
      , HospitalName _
      , FinalResult, ResultRemark, LabResult _
      , CloseStatus _
      , isSmoke _
      , SmokeQTY _
      , Disease0 _
      , Disease1 _
      , Disease2 _
      , Disease3 _
      , Disease4 _
      , Disease5 _
      , Disease6 _
      , Disease7 _
      , Disease8 _
      , Disease9 _
      , Disease10 _
      , Disease11 _
      , Disease12 _
      , Disease99 _
      , DiseaseOther _
      , SmokeFinal _
      , SmokeBegin _
      , SmokeEnd _
      , IsA1A5 _
      , ReasonA1A5 _
      , IsSmokingFollow _
      , IsMTMFollow _
      , IsOtherFollow _
      , FormOther, MedicineOther)

    End Function
#End Region

#Region "Long COVID"
    Public Function COVID2_Patient_Get(LocationID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "COVID2_Patient_Get", LocationID)
        Return ds.Tables(0)
    End Function
    Public Function COVID2_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "COVID2_GetByUID", pUID)
        Return ds.Tables(0)
    End Function
    Public Function COVID2_GetLastUID(ByVal LocationID As String _
           , ByVal PatientID As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "COVID2_GetLastUID", LocationID, PatientID)

        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function COVID2_ChkDupCustomer(PatientID As Integer) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("COVID2_ChkDupCustomer"), PatientID)
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)(0) > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Public Function COVID2_Delete(itemID As Long) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("COVID2_Delete"), itemID)
    End Function

    Public Function COVID2_Save(ByVal UID As Integer _
           , ByVal LocationID As String _
           , ByVal PatientID As Integer _
           , ByVal StartDate As String _
           , ByVal EndDate As String _
           , ByVal TestMethod As String _
           , ByVal ColorStatus As String _
           , ByVal Medication As String _
           , ByVal MedicationOther As String _
           , ByVal Smoking As String _
           , ByVal SmokingQuit As String _
           , ByVal AfterEffect As String _
           , ByVal Q1 As String _
           , ByVal Q2 As String _
           , ByVal Q3 As String _
           , ByVal Q4 As String _
           , ByVal Q5 As String _
           , ByVal Q6 As String _
           , ByVal Q7 As String _
           , ByVal Q8 As String _
           , ByVal Q9 As String _
           , ByVal Q10 As String _
           , ByVal Q11 As String _
           , ByVal Q12 As String _
           , ByVal Q13 As String _
           , ByVal Q14 As String _
           , ByVal Q15 As String _
           , ByVal Q16 As String _
           , ByVal M1 As String _
           , ByVal M2 As String _
           , ByVal M3 As String _
           , ByVal S1 As String _
           , ByVal S2 As String _
           , ByVal PhysicalRemark As String _
           , ByVal ProblemOther As String _
           , ByVal IsRecommend As String _
           , ByVal IsRefer As String _
           , ByVal Recommend As String _
           , ByVal ReferTo As String _
           , ByVal ReferOther As String _
           , ByVal FinalResult As String _
           , ByVal ResultOther As String _
           , ByVal FollowCount As Integer _
           , ByVal FollowDay1 As Integer _
           , ByVal FollowDay2 As Integer _
           , ByVal FollowDay3 As Integer _
           , ByVal FollowDay4 As Integer _
           , ByVal FollowDay5 As Integer _
           , ByVal CloseStatus As String _
           , ByVal CUser As Integer _
           , ByVal Disease0 As String _
           , ByVal Disease1 As String _
           , ByVal Disease2 As String _
           , ByVal Disease3 As String _
           , ByVal Disease4 As String _
           , ByVal Disease5 As String _
           , ByVal Disease6 As String _
           , ByVal Disease7 As String _
           , ByVal Disease8 As String _
           , ByVal Disease9 As String _
           , ByVal Disease10 As String _
           , ByVal Disease11 As String _
           , ByVal Disease12 As String _
           , ByVal Disease99 As String _
           , ByVal DiseaseOther As String _
           , ByVal IsMedicine As String _
           , ByVal Medicine As String _
           , ByVal MedicineOther As String, AfterEffRemark As String
        ) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "COVID2_Save", UID _
           , LocationID _
           , PatientID _
           , StartDate _
           , EndDate _
           , TestMethod _
           , ColorStatus _
           , Medication _
           , MedicationOther _
           , Smoking _
           , SmokingQuit _
           , AfterEffect _
           , Q1 _
           , Q2 _
           , Q3 _
           , Q4 _
           , Q5 _
           , Q6 _
           , Q7 _
           , Q8 _
           , Q9 _
           , Q10 _
           , Q11 _
           , Q12 _
           , Q13 _
           , Q14 _
           , Q15 _
           , Q16 _
           , M1 _
           , M2 _
           , M3 _
           , S1 _
           , S2 _
           , PhysicalRemark _
           , ProblemOther _
           , IsRecommend _
           , IsRefer _
           , Recommend _
           , ReferTo _
           , ReferOther _
           , FinalResult _
           , ResultOther _
           , FollowCount _
           , FollowDay1 _
           , FollowDay2 _
           , FollowDay3 _
           , FollowDay4 _
           , FollowDay5 _
           , CloseStatus _
           , CUser _
           , Disease0 _
           , Disease1 _
           , Disease2 _
           , Disease3 _
           , Disease4 _
           , Disease5 _
           , Disease6 _
           , Disease7 _
           , Disease8 _
           , Disease9 _
           , Disease10 _
           , Disease11 _
           , Disease12 _
           , Disease99 _
           , DiseaseOther _
           , IsMedicine, Medicine, MedicineOther, AfterEffRemark)

    End Function
#End Region

#Region "Self Isolation Care"
    Public Function COVID3_Patient_Get(LocationID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "COVID3_Patient_Get", LocationID)
        Return ds.Tables(0)
    End Function

    Public Function COVID3_Head_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "COVID3_Head_GetByUID", pUID)
        Return ds.Tables(0)
    End Function
    Public Function COVID3_Head_Get(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "COVID3_Head_Get", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function COVID3_Head_GetLastUID(ByVal LocationID As String _
           , ByVal PatientID As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "COVID3_Head_GetLastUID", LocationID, PatientID)

        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function COVID3_Head_ChkDupCustomer(PatientID As Integer) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("COVID3_Head_ChkDupCustomer"), PatientID)
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)(0) > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Public Function COVID3_Head_Save(ByVal UID As Integer _
           , ByVal LocationID As String _
           , ByVal PatientID As Integer _
           , ByVal TestDate As Integer _
           , ByVal ConfirmDate As Integer _
           , ByVal IsHomeIsolation As String _
           , ByVal AdmitDate As Integer _
           , ByVal IsCall As String _
           , ByVal CallDate As String _
           , ByVal IsMedicine As String _
           , ByVal Medicine As String _
           , ByVal FinalResult As String _
           , ByVal ResultRemark As String _
           , ByVal ATKDate As String _
           , ByVal ATKResult As String _
           , ByVal CloseStatus As String _
           , ByVal isSmoke As String _
           , ByVal SmokeQTY As String _
           , ByVal QuitDay As String _
           , ByVal Disease0 As String _
           , ByVal Disease1 As String _
           , ByVal Disease2 As String _
           , ByVal Disease3 As String _
           , ByVal Disease4 As String _
           , ByVal Disease5 As String _
           , ByVal Disease6 As String _
           , ByVal Disease7 As String _
           , ByVal Disease8 As String _
           , ByVal Disease9 As String _
           , ByVal Disease10 As String _
           , ByVal Disease11 As String _
           , ByVal Disease12 As String _
           , ByVal Disease99 As String _
           , ByVal DiseaseOther As String _
           , ByVal SmokeFinal As String _
           , ByVal SmokeBegin As String _
           , ByVal SmokeEnd As String _
           , ByVal IsA1A5 As String _
           , ByVal ReasonA1A5 As String _
           , ByVal IsSmokingFollow As String _
           , ByVal IsMTMFollow As String _
           , ByVal IsOtherFollow As String _
           , ByVal FormOther As String _
           , ByVal MedicineOther As String _
           , ByVal MedicineCough As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "COVID3_Head_Save", UID, LocationID _
      , PatientID _
      , TestDate _
      , ConfirmDate _
      , IsHomeIsolation _
      , AdmitDate, IsCall, CallDate _
      , IsMedicine _
      , Medicine _
      , FinalResult, ResultRemark, ATKDate, ATKResult _
      , CloseStatus _
      , isSmoke _
      , SmokeQTY, QuitDay _
      , Disease0 _
      , Disease1 _
      , Disease2 _
      , Disease3 _
      , Disease4 _
      , Disease5 _
      , Disease6 _
      , Disease7 _
      , Disease8 _
      , Disease9 _
      , Disease10 _
      , Disease11 _
      , Disease12 _
      , Disease99 _
      , DiseaseOther _
      , SmokeFinal _
      , SmokeBegin _
      , SmokeEnd _
      , IsA1A5 _
      , ReasonA1A5 _
      , IsSmokingFollow _
      , IsMTMFollow _
      , IsOtherFollow _
      , FormOther, MedicineOther, MedicineCough)

    End Function


    Public Function COVID3_Detail_Get(UID As Integer, patientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "COVID3_Detail_Get", UID, patientID)
        Return ds.Tables(0)
    End Function
    Public Function COVID3_Detail_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "COVID3_Detail_GetByUID", UID)
        Return ds.Tables(0)
    End Function
    Public Function COVID3_Detail_Save(
            ByVal UID As Integer _
          , ByVal HeadUID As Integer _
          , ByVal SEQ As Integer _
          , ByVal ServiceDate As Integer _
          , ByVal PersonID As Integer _
          , ByVal ServiceTime As Double _
          , ByVal Temparature As Double, ByVal O2Sat As String _
          , ByVal Cough As String _
          , ByVal Breathe As String _
          , ByVal Dia As String _
          , ByVal Smelless As String _
          , ByVal Tasteless As String _
          , ByVal Other As String _
          , ByVal Problem As String _
          , ByVal Recommend As String _
          , ByVal MUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "COVID3_Detail_Save", UID, HeadUID, SEQ, ServiceDate, PersonID, ServiceTime, Temparature, O2Sat, Cough, Breathe, Dia, Smelless, Tasteless, Other, Problem, Recommend, MUser)

    End Function
    Public Function COVID3_Detail_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "COVID3_Detail_Delete", UID)
    End Function

#End Region

#Region "Behavior Relate"
    Public Function COVID_Detail_Get(UID As Integer, patientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "COVID_Detail_Get", UID, patientID)
        Return ds.Tables(0)
    End Function
    Public Function COVID_Detail_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "COVID_Detail_GetByUID", UID)
        Return ds.Tables(0)
    End Function
    Public Function COVID_Detail_Save(
            ByVal UID As Integer _
          , ByVal HeadUID As Integer _
          , ByVal SEQ As Integer _
          , ByVal ServiceDate As Integer _
          , ByVal PersonID As Integer _
          , ByVal ServiceTime As Double _
          , ByVal Temparature As Double, ByVal O2Sat As String _
          , ByVal Cough As String _
          , ByVal Breathe As String _
          , ByVal Dia As String _
          , ByVal Smelless As String _
          , ByVal Tasteless As String _
          , ByVal Other As String _
          , ByVal Problem As String _
          , ByVal Recommend As String _
          , ByVal MUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "COVID_Detail_Save", UID, HeadUID, SEQ, ServiceDate, PersonID, ServiceTime, Temparature, O2Sat, Cough, Breathe, Dia, Smelless, Tasteless, Other, Problem, Recommend, MUser)

    End Function
    Public Function COVID_Detail_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "COVID_Detail_Delete", UID)
    End Function

#End Region
#Region "Hospital"
    Public Function COVID_Hospital_Get(COVIDUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "COVID_Hospital_Get", COVIDUID)
        Return ds.Tables(0)
    End Function
    Public Function COVID_Hospital_GetByPatient(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "COVID_Hospital_GetByPatient", PatientID)
        Return ds.Tables(0)
    End Function
    Public Function COVID_Hospital_Add(ByVal COVIDUID As Integer, TypeID As String, ByVal Name As String, PatientID As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "COVID_Hospital_Add", COVIDUID, TypeID, Name, PatientID)

    End Function
    Public Function COVID_Hospital_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "COVID_Hospital_Delete", UID)
    End Function

#End Region

End Class
