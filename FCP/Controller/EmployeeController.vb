﻿Imports Microsoft.ApplicationBlocks.Data

Public Class EmployeeController
    Inherits ApplicationBaseClass
    Public ds As DataSet = New DataSet

#Region "Personal"
    Public Function Employee_GetAll() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetAll"))
        Return ds.Tables(0)
    End Function

    Public Function Person_GetBySearch(ByVal pKey As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Person_GetBySearch"), pKey)
        Return ds.Tables(0)
    End Function

    Public Function Employee_GetBySearch(ByVal pKey As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Employee_GetBySearch"), pKey)
        Return ds.Tables(0)
    End Function


    Public Function Person_GetEmployeeSearch(ByVal pKey As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Person_GetEmployeeSearch"), pKey)
        Return ds.Tables(0)
    End Function

    Public Function Person_GetByStatus(ByVal pKey As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Person_GetByStatus"), pKey)
        Return ds.Tables(0)
    End Function

    Public Function Person_GetByUID(ByVal pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Person_GetByUID"), pUID)
        Return ds.Tables(0)
    End Function
    Public Function Person_GetByEmployeeCode(ByVal Empcode As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Person_GetByEmployeeCode"), Empcode)
        Return ds.Tables(0)
    End Function

    Public Function Person_GetNameTHByID(ByVal pUID As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Person_GetNameTHByID"), pUID)
        Return String.Concat(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function Person_Save(
           ByVal PersonUID As Integer,
           ByVal EmployeeCode As String,
           ByVal NameTH As String,
           ByVal SurnameTH As String,
           ByVal NameEN As String,
           ByVal SurnameEN As String,
           ByVal DateOfBirth As String,
           ByVal CardID As String,
           ByVal Gender As String,
           ByVal PersonTypeCode As String,
           ByVal CompanyUID As Integer,
           ByVal DivisionUID As Integer,
           ByVal SectionUID As Integer,
           ByVal PositionUID As Integer,
           ByVal StartWorkDate As String,
           ByVal EndWorkDate As String,
           ByVal PicturePath As String,
           ByVal EmployeeStatus As String,
           ByVal StatusFlag As String,
           ByVal isSameAddress As String,
           ByVal UpdBy As Integer, ByVal BU As String, ByVal BloodGroup As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Person_Save"),
             PersonUID _
           , EmployeeCode _
           , NameTH _
           , SurnameTH _
           , NameEN _
           , SurnameEN _
           , DateOfBirth _
           , CardID _
           , Gender _
           , PersonTypeCode _
           , CompanyUID _
           , DivisionUID _
           , SectionUID _
           , PositionUID _
           , StartWorkDate _
           , EndWorkDate _
           , PicturePath _
           , EmployeeStatus _
           , StatusFlag _
           , isSameAddress _
           , UpdBy, BU, BloodGroup)


    End Function

    Public Function Person_UpdateAllergy(ByVal PersonUID As Integer, ByVal MedicalHistory As String, ByVal Allergy As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Person_UpdateAllergy"), PersonUID, MedicalHistory, Allergy)
    End Function

    Public Function Person_Delete(ByVal pID As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Person_Delete"), pID)
    End Function
#End Region

#Region "Person Address"



    Function Person_SaveAddress(ByVal PersonCode As String, ByVal AddressType As Integer, ByVal AddressNo As String, ByVal VillageNo As String, ByVal Lane As String, ByVal Road As String, ByVal SubDistrict As String, ByVal District As String, ByVal ProvinceUID As Integer, ByVal ProvinceName As String, ByVal ZipCode As String, ByVal PhoneNumber As String, ByVal MobileNumber As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Person_SaveAddress"), PersonCode, AddressType, AddressNo, VillageNo, Lane, Road, SubDistrict, District, ProvinceUID, ProvinceName, ZipCode, PhoneNumber, MobileNumber)
    End Function
    Public Function Person_GetAddress(ByVal PersonUID As Integer, ByVal AddressType As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Person_GetAddress"), PersonUID, AddressType)
        Return ds.Tables(0)
    End Function
#End Region

#Region "Job History"
    Public Function PersonJobHistory_Save(
              ByVal JUID As Integer,
              ByVal PersonUID As Integer,
              ByVal CompanyName As String,
              ByVal BUType As String,
              ByVal Address As String,
              ByVal tel As String,
              ByVal StartWorkDate As String,
              ByVal EndWorkDate As String,
              ByVal Position As String,
              ByVal Department As String,
              ByVal WorkType As String,
              ByVal JobDesc As String,
              ByVal isNote As String,
              ByVal UpdBy As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("PersonJobHistory_Save"), JUID,
             PersonUID _
           , CompanyName _
           , BUType _
           , Address _
           , tel _
           , StartWorkDate _
           , EndWorkDate _
           , Position _
           , Department _
           , WorkType _
           , JobDesc _
           , isNote _
           , UpdBy)


    End Function

    Public Function PersonJobHistory_Get(ByVal pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("PersonJobHistory_Get"), pUID)
        Return ds.Tables(0)
    End Function
    Public Function PersonJobHistory_GetByUID(ByVal pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("PersonJobHistory_GetByUID"), pUID)
        Return ds.Tables(0)
    End Function
    Public Function PersonJobHistory_GetUID(ByVal pUID As Integer, JobCompany As String, JobPosition As String, JobWorkTime As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("PersonJobHistory_GetUID"), pUID, JobCompany, JobPosition, JobWorkTime)

        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function
    Public Function PersonJobHistory_Delete(ByVal pUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("PersonJobHistory_Delete"), pUID)

    End Function


#End Region

#Region "Risk Factor"

    Public Function PersonRiskFactor_Get(ByVal JobHistoryUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("PersonRisk_Get"), JobHistoryUID)
        Return ds.Tables(0)
    End Function
    Public Function PersonRisk_Save(ByVal JobHistoryUID As Integer,
            ByVal AttributeUID As Integer, ByVal PersonUID As Integer, ByVal DataValue As String, ByVal UpdBy As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("PersonRisk_Save"), JobHistoryUID, AttributeUID,
             PersonUID _
           , DataValue _
           , UpdBy)

    End Function
    Public Function PersonRisk_Delete(ByVal JobHistoryUID As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("PersonRisk_Delete"), JobHistoryUID)

    End Function
#End Region
#Region "PPE"

    Public Function PersonPPE_Get(ByVal JobHistoryUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("PersonPPE_Get"), JobHistoryUID)
        Return ds.Tables(0)
    End Function
    Public Function PersonPPE_Save(ByVal JobHistoryUID As Integer, ByVal AttributeUID As Integer, ByVal PersonUID As Integer, ByVal DataValue As String, ByVal UpdBy As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("PersonPPE_Save"), JobHistoryUID, AttributeUID,
             PersonUID _
           , DataValue _
           , UpdBy)
    End Function

    Public Function PersonPPE_Delete(ByVal JobHistoryUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("PersonPPE_Delete"), JobHistoryUID)
    End Function
#End Region

#Region "HealthHistory"
    Public Function PersonHealthHistory_GetByPersonUID(ByVal pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("PersonHealthHistory_GetByPersonUID"), pUID)
        Return ds.Tables(0)
    End Function
    Public Function PersonHealthHistory_Delete(ByVal PersonUID As Integer)
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("PersonHealthHistory_Delete"), PersonUID)
    End Function
    Public Function PersonHealthHistory_Save(ByVal param As String())
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("HealthHistory_Save"), param)
    End Function
    Public Function PersonHealthHistory_Save(
           ByVal PersonUID As Integer _
      , ByVal isCongenitalDisease As String _
      , ByVal DiseaseDetail As String _
      , ByVal isSurgery As String _
      , ByVal SurgeryDetail As String _
      , ByVal isImmunity As String _
      , ByVal ImmunityDetail As String _
      , ByVal isMedicine As String _
      , ByVal MedicineDetail As String _
      , ByVal isAllergy As String _
      , ByVal AllergyDetail As String _
      , ByVal isSmoking As String _
      , ByVal SmokeQTY As String _
      , ByVal SmokeQTYBefore As String _
      , ByVal SmokeTime As String _
      , ByVal SmokeUOM As String _
      , ByVal isAlcohol As String _
      , ByVal DrinkFrequency As String _
      , ByVal DrinkTime As String _
      , ByVal DrinkUOM As String _
      , ByVal isDrugs As String _
      , ByVal DrugsDetail As String _
      , ByVal Remark As String _
      , ByVal CUser As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("HealthHistory_Save"), PersonUID, isCongenitalDisease _
      , DiseaseDetail _
      , isSurgery _
      , SurgeryDetail _
      , isImmunity _
      , ImmunityDetail _
      , isMedicine _
      , MedicineDetail _
      , isAllergy _
      , AllergyDetail _
      , isSmoking _
      , SmokeQTY _
      , SmokeQTYBefore _
      , SmokeTime _
      , SmokeUOM _
      , isAlcohol _
      , DrinkFrequency _
      , DrinkTime _
      , DrinkUOM _
      , isDrugs _
      , DrugsDetail _
      , Remark _
      , CUser)
    End Function
    Public Function HealthFamily_Add(ByVal PUID As Integer, Relation As Integer, Desc As String, Cuser As String)
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("HealthFamily_Add"), PUID, Relation, Desc, Cuser)
    End Function
    Public Function HealthFamily_Update(UID As Integer, ByVal PUID As Integer, Relation As Integer, Desc As String, Cuser As String)
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("HealthFamily_Update"), UID, PUID, Relation, Desc, Cuser)
    End Function
    Public Function HealthFamily_Delete(ByVal PUID As Integer)
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("HealthFamily_Delete"), PUID)
    End Function

    Public Function HealthFamily_GetByPersonUID(ByVal pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("HealthFamily_GetByPersonUID"), pUID)
        Return ds.Tables(0)
    End Function
    Public Function HealthFamily_GetByUID(ByVal pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("HealthFamily_GetByUID"), pUID)
        Return ds.Tables(0)
    End Function

    Public Function Vaccine_Add(ByVal PUID As Integer, VWhen As String, VaccineUID As Integer, Remark As String, Cuser As String)
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("EmployeeVaccine_Add"), PUID, VWhen, VaccineUID, Remark, Cuser)
    End Function
    Public Function Vaccine_Update(ByVal PUID As Integer, Relation As Integer, Desc As String, Cuser As String)
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("EmployeeVaccine_Update"), PUID, Relation, Desc, Cuser)
    End Function
    Public Function Vaccine_Delete(ByVal PUID As Integer)
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("EmployeeVaccine_Delete"), PUID)
    End Function

    Public Function Vaccine_GetByPersonUID(ByVal pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EmployeeVaccine_GetByPersonUID"), pUID)
        Return ds.Tables(0)
    End Function
    Public Function Vaccine_GetByUID(ByVal pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("EmployeeVaccine_GetByUID"), pUID)
        Return ds.Tables(0)
    End Function
#End Region
End Class