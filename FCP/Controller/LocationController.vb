﻿Imports Microsoft.ApplicationBlocks.Data

Public Class LocationController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Public Function Location_GetAll() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Location_GetAll"))
        Return ds.Tables(0)
    End Function

    Public Function Location_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Location_Get"))
        Return ds.Tables(0)
    End Function
    Public Function Location_GetReport(pType As String, pProvince As String, ProjID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "RPT_LocationProject", pType, pProvince, ProjID)
        Return ds.Tables(0)
    End Function

    Public Function Location_GetByProvinceGroup(pProvGRP As String, Optional pKey As String = "") As DataTable
        SQL = "select * from  View_Locations Where 1<>2 "
        SQL &= " AND ProvinceGroupID='" & pProvGRP & "'"

        If pKey <> "" Then
            SQL &= " AND LocationName like '%" & pKey & "%'"
        End If

        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function

    Public Function Location_GetByID(id As String) As DataTable
        SQL = "select * from  View_Locations  where Locationid='" & id & "'"
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function
 

    'Public Function Location_GetBySearch(prmKey As String) As DataTable
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Location_GetBySearch"), prmKey)
    '    Return ds.Tables(0)
    'End Function
   
    Public Function Location_GetBySearchAll(provid As String, id As String) As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Location_GetByProvinceIDAll"), provid, id)
        Return ds.Tables(0)
    End Function

    Public Function Location_GetBySearch(provid As String, id As String) As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Location_GetByProvinceID"), provid, id)
        Return ds.Tables(0)
    End Function





    Public Function Location_GetNameByID(id As String) As String
        SQL = "select LocationName  from Locations  where Locationid='" & id & "'"
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0)("LocationName")
        Else
            Return ""
        End If

    End Function

    Public Function Location_Add(ByVal LocationID As String, ByVal LocationName1 As String, ByVal LocationName2 As String, ByVal LocationGroupID As String, TypeShop As String, TypeName As String, ByVal Address As String, ByVal ProvinceID As String, ByVal ProvinceName As String, ByVal ZipCode As String, ByVal Office_Tel As String, ByVal Office_Fax As String, ByVal Co_Name As String, ByVal Co_Mail As String, ByVal Co_Tel As String, ByVal AccNo As String, ByVal AccName As String, ByVal BankID As String, ByVal BankBrunch As String, ByVal BankType As String, ByVal UpdBy As String, ByVal isPublic As Integer, ByVal Office_Mail As String, CardID As String, RYear As String, LicenseNo As String, LocationCode As String, ByVal Lat As String, ByVal Lng As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Location_Add"), LocationID, LocationName1, LocationName2, LocationGroupID, TypeShop, TypeName, Address, ProvinceID, ProvinceName, ZipCode, Office_Tel, Office_Fax, Co_Name, Co_Mail, Co_Tel, AccNo, AccName, BankID, BankBrunch, BankType, UpdBy, isPublic, Office_Mail, CardID, RYear, LicenseNo, LocationCode, Lat, Lng)
    End Function

    Public Function Location_Update(ByVal LocationID As String, ByVal LocationName1 As String, ByVal LocationName2 As String, ByVal LocationGroupID As String, TypeShop As String, TypeName As String, ByVal Address As String, ByVal ProvinceID As String, ByVal ProvinceName As String, ByVal ZipCode As String, ByVal Office_Tel As String, ByVal Office_Fax As String, ByVal Co_Name As String, ByVal Co_Mail As String, ByVal Co_Tel As String, ByVal AccNo As String, ByVal AccName As String, ByVal BankID As String, ByVal BankBrunch As String, ByVal BankType As String, ByVal UpdBy As String, ByVal isPublic As Integer, ByVal Office_Mail As String, CardID As String, RYear As String, LicenseNo As String, LocationCode As String, ByVal Lat As String, ByVal Lng As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Location_Update", LocationID, LocationName1, LocationName2, LocationGroupID, TypeShop, TypeName, Address, ProvinceID, ProvinceName, ZipCode, Office_Tel, Office_Fax, Co_Name, Co_Mail, Co_Tel, AccNo, AccName, BankID, BankBrunch, BankType, UpdBy, isPublic, Office_Mail, CardID, RYear, LicenseNo, LocationCode, Lat, Lng)

    End Function
    Public Function Location_Add(ByVal LocationID As String, ByVal LocationName1 As String, ByVal LocationName2 As String, ByVal LocationGroupID As String, TypeShop As String, TypeName As String, ByVal Address As String, ByVal ProvinceID As String, ByVal ProvinceName As String, ByVal ZipCode As String, ByVal Office_Tel As String, ByVal Office_Fax As String, ByVal Co_Name As String, ByVal Co_Mail As String, ByVal Co_Tel As String, ByVal UpdBy As String, ByVal isPublic As Integer, ByVal Office_Mail As String, RYear As String, LicenseNo As String, LocationCode As String, ByVal Lat As String, ByVal Lng As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Location_Add2"), LocationID, LocationName1, LocationName2, LocationGroupID, TypeShop, TypeName, Address, ProvinceID, ProvinceName, ZipCode, Office_Tel, Office_Fax, Co_Name, Co_Mail, Co_Tel, UpdBy, isPublic, Office_Mail, RYear, LicenseNo, LocationCode, Lat, Lng)
    End Function

    Public Function Location_Update(ByVal LocationID As String, ByVal LocationName1 As String, ByVal LocationName2 As String, ByVal LocationGroupID As String, TypeShop As String, TypeName As String, ByVal Address As String, ByVal ProvinceID As String, ByVal ProvinceName As String, ByVal ZipCode As String, ByVal Office_Tel As String, ByVal Office_Fax As String, ByVal Co_Name As String, ByVal Co_Mail As String, ByVal Co_Tel As String, ByVal UpdBy As String, ByVal isPublic As Integer, ByVal Office_Mail As String, RYear As String, LicenseNo As String, LocationCode As String, ByVal Lat As String, ByVal Lng As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Location_Update2", LocationID, LocationName1, LocationName2, LocationGroupID, TypeShop, TypeName, Address, ProvinceID, ProvinceName, ZipCode, Office_Tel, Office_Fax, Co_Name, Co_Mail, Co_Tel, UpdBy, isPublic, Office_Mail, RYear, LicenseNo, LocationCode, Lat, Lng)

    End Function


    Public Function Location_UpdateByUser(ByVal LocationID As String, ByVal LocationName As String, TypeShop As String, TypeName As String, ByVal Address As String, ByVal ZipCode As String, ByVal Office_Tel As String, ByVal Office_Fax As String, ByVal Co_Name As String, ByVal Co_Mail As String, ByVal Co_Tel As String, ByVal AccNo As String, ByVal AccName As String, ByVal BankID As String, ByVal BankBrunch As String, ByVal BankType As String, ByVal UpdBy As String, ByVal Office_Mail As String, CardID As String, RYear As String, LicenseNo As String, LocationCode As String, ByVal Lat As String, ByVal Lng As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Location_UpdateByUser", LocationID, LocationName, TypeShop, TypeName, Address, ZipCode, Office_Tel, Office_Fax, Co_Name, Co_Mail, Co_Tel, AccNo, AccName, BankID, BankBrunch, BankType, UpdBy, Office_Mail, CardID, RYear, LicenseNo, LocationCode, Lat, Lng)

    End Function
    Public Function Location_UpdateByUser(ByVal LocationID As String, ByVal LocationName As String, TypeShop As String, TypeName As String, ByVal Address As String, ByVal ZipCode As String, ByVal Office_Tel As String, ByVal Office_Fax As String, ByVal Co_Name As String, ByVal Co_Mail As String, ByVal Co_Tel As String, ByVal UpdBy As String, ByVal Office_Mail As String, RYear As String, LicenseNo As String, LocationCode As String, ByVal Lat As String, ByVal Lng As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Location_UpdateByUser2", LocationID, LocationName, TypeShop, TypeName, Address, ZipCode, Office_Tel, Office_Fax, Co_Name, Co_Mail, Co_Tel, UpdBy, Office_Mail, RYear, LicenseNo, LocationCode, Lat, Lng)

    End Function

    Public Function Location_Delete(ByVal pID As String) As Integer
        SQL = "delete from Locations where Locationid ='" & pID & "'"
        Return SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, SQL)
    End Function

#Region "Register"
    Public Function Location_SearchByLicense(LicenseNo As String) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Location_SearchByLicense", LicenseNo)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function
    Public Function Register_SearchByLicense(LicenseNo As String) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Register_SearchByLicense", LicenseNo)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function

    Public Function Location_Register(ByVal LicenseNo As String, ByVal LocationName As String, ByVal NHSOCode As String, TypeID As String, ByVal GroupID As String, ByVal Address As String, ByVal ProvinceID As String, ByVal ProvinceName As String, ByVal ZipCode As String, ByVal Office_Tel As String, ByVal Office_Mail As String, LineID As String, ByVal Lat As String, ByVal Lng As String, Office_Hour As String, PYear As Integer, ByVal Co_Name As String, ByVal Co_Tel As String, ByVal Co_Mail As String, ByVal LicenseNoPharm As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Location_Register"), LicenseNo, LocationName, NHSOCode, TypeID, GroupID, Address, ProvinceID, ProvinceName, ZipCode, Office_Tel, Office_Mail, LineID, Lat, Lng, Office_Hour, PYear, Co_Name, Co_Tel, Co_Mail, LicenseNoPharm)
    End Function

    Public Function Register_Approve(ByVal RegisterID As Integer, ByVal LocationID As String, ByVal LocationName As String, ByVal SpecName As String, ByVal LocationGroupID As String, TypeShop As String, ByVal Address As String, ByVal ProvinceID As String, ByVal ProvinceName As String, ByVal ZipCode As String, ByVal Office_Tel As String, ByVal Office_Mail As String, ByVal LindID As String, ByVal Co_Name As String, ByVal Co_Mail As String, ByVal Co_Tel As String, RYear As String, LicenseNo As String, LocationCode As String, ByVal Lat As String, ByVal Lng As String, ByVal UpdBy As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Register_AddToLocation"), RegisterID, LocationID, LocationName, SpecName, LocationGroupID, TypeShop, Address, ProvinceID, ProvinceName, ZipCode, Office_Tel, Office_Mail, LindID, Co_Name, Co_Mail, Co_Tel, RYear, LicenseNo, LocationCode, Lat, Lng, UpdBy)
    End Function
    Public Function Register_UpdateStatus(ByVal RegisterUID As Integer, Status As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Register_UpdateStatus"), RegisterUID, Status)
    End Function
    Public Function Register_Reject(ByVal RegisterUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Register_Reject"), RegisterUID)
    End Function
    Public Function Register_Delete(ByVal RegisterUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Register_Delete"), RegisterUID)
    End Function
    Public Function LocationTypeDetail_Save(ByVal RegisterUID As Integer, ByVal TypeUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LocationTypeDetail_Save"), RegisterUID, TypeUID)
    End Function
    Public Function LocationTypeDetail_SaveByLicense(ByVal License As String, ByVal TypeUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LocationTypeDetail_SaveByLicense"), License, TypeUID)
    End Function

    Public Function Register_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Register_GetByUID"), UID)
        Return ds.Tables(0)
    End Function
#End Region
#Region "Group"

    Public Function LocationGroup_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LocationGroup_Get"))
        Return ds.Tables(0)
    End Function
    Public Function LocationGroup_GetForReport() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LocationGroup_GetForReport"))
        Return ds.Tables(0)
    End Function

    Public Function LocationGroup_GetBySearch(search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LocationGroup_GetBySearch"), search)
        Return ds.Tables(0)
    End Function
    Public Function LocationGroup_GetByUID(id As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LocationGroup_GetByUID"), id)
        Return ds.Tables(0)
    End Function
    Public Function LocationGroup_CheckDuplicate(Name As String) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LocationGroup_CheckDuplicate"), Name)
        If ds.Tables(0).Rows.Count > 0 Then
            If DBNull2Zero(ds.Tables(0).Rows(0)(0)) > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
    Public Function LocationGroup_Save(ByVal pUID As String, ByVal pCode As String, ByVal pName As String, pDesc As String, ByVal pStatus As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LocationGroup_Save", pUID, pCode, pName, pDesc, pStatus)
    End Function
    Public Function LocationGroup_Delete(ByVal pID As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LocationGroup_Delete", pID)
    End Function

#End Region

#Region "Chain"
    Public Function LocationChain_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LocationChain_Get"))
        Return ds.Tables(0)
    End Function
    Public Function LocationChain_GetForReport() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LocationChain_GetForReport"))
        Return ds.Tables(0)
    End Function
    Public Function LocationChain_GetBySearch(search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LocationChain_GetBySearch"), search)
        Return ds.Tables(0)
    End Function
    Public Function LocationChain_GetByUID(id As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LocationChain_GetByUID"), id)
        Return ds.Tables(0)
    End Function
    Public Function LocationChain_CheckDuplicate(Name As String) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LocationChain_CheckDuplicate"), Name)
        If ds.Tables(0).Rows.Count > 0 Then
            If DBNull2Zero(ds.Tables(0).Rows(0)(0)) > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
    Public Function LocationChain_Save(ByVal pUID As String, ByVal pCode As String, ByVal pName As String, pDesc As String, ByVal pStatus As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LocationChain_Save", pUID, pCode, pName, pDesc, pStatus)
    End Function
    Public Function LocationChain_Delete(ByVal pID As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LocationChain_Delete", pID)
    End Function

#End Region

#Region "Type"
    Public Function LocationType_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LocationType_Get"))
        Return ds.Tables(0)
    End Function
    Public Function LocationType_GetActive() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LocationType_GetActive"))
        Return ds.Tables(0)
    End Function
    Public Function LocationType_GetForReport() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LocationType_GetForReport"))
        Return ds.Tables(0)
    End Function

    Public Function LocationType_GetBySearch(search As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LocationType_GetBySearch"), search)
        Return ds.Tables(0)
    End Function
    Public Function LocationType_GetByUID(id As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LocationType_GetByUID"), id)
        Return ds.Tables(0)
    End Function
    Public Function LocationType_CheckDuplicate(Name As String) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LocationType_CheckDuplicate"), Name)
        If ds.Tables(0).Rows.Count > 0 Then
            If DBNull2Zero(ds.Tables(0).Rows(0)(0)) > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function
    Public Function LocationType_Save(ByVal pUID As String, ByVal pCode As String, ByVal pName As String, pDesc As String, ByVal pStatus As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LocationType_Save", pUID, pCode, pName, pDesc, pStatus)
    End Function
    Public Function LocationType_Delete(ByVal pID As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LocationType_Delete", pID)
    End Function

#End Region

End Class

Public Class LocationGroupController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Dim objLginfo As New LocationGroupInfo
    Public Function LocationGroup_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LocationGroup_Get"))
        Return ds.Tables(0)
    End Function
    Public Function LocationGroup_GetForReport() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("LocationGroup_GetForReport"))
        Return ds.Tables(0)
    End Function
    'Public Function LocationGroup_Get() As DataTable

    '    SQL = "select * from  " & objLginfo.strTableName
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
    '    Return ds.Tables(0)
    'End Function

    Public Function LocationGroup_ByID(id As String) As DataSet
        SQL = "select * from " & objLginfo.strTableName & " where code='" & id & "'"
        Return SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
    End Function

    Public Function LocationGroup_Add(ByVal pCode As String, ByVal pName As String, desc As String, ByVal pStatus As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("LocationGroup_Add"), pCode, pName, desc, pStatus)
    End Function

    Public Function LocationGroup_Update(ByVal pID_old As String, ByVal pID_new As String, ByVal pName As String, desc As String, ByVal pStatus As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LocationGroup_Update", pID_old, pID_new, pName, desc, pStatus)
    End Function

    Public Function LocationGroup_Delete(ByVal pID As String) As Integer
        SQL = "delete from " & objLginfo.strTableName & " where code ='" & pID & "'"
        Return SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, SQL)
    End Function


End Class