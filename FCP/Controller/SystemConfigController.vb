﻿Imports Microsoft.ApplicationBlocks.Data
Public Class SystemConfigController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Public Function SystemConfig_Get() As DataTable
        SQL = "select * from SystemConfig order by itemID "
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function

    Public Function SystemConfig_GetByCode(id As String) As String
        SQL = "select Value from SystemConfig where Code='" & id & "'"
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0)(0).ToString
        Else
            Return ""
        End If
    End Function

    Public Function SystemConfig_GetAllByCode(id As String) As DataTable
        SQL = "select * from SystemConfig where Code='" & id & "'"
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function


    Public Function DataConfig_Add(code As String, pValue As String, desc As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "DataConfig_Add", code, pValue, desc)
    End Function
    Public Function DataConfig_Update(code As String, pValue As String, desc As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, "DataConfig_Update", code, pValue, desc)
    End Function

End Class
 