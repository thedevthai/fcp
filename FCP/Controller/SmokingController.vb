﻿Imports Microsoft.ApplicationBlocks.Data
Public Class SmokingController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet


    Public Function PatientSmoking_GetByLocation(pLID As String, pKey As String, pProv As String, Optional pYear As Integer = 0) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "PatientSmoking_GetByLocation", pLID, pKey, pProv, pYear)
        Return ds.Tables(0)
    End Function
    Public Function PatientSmoking_Get(pKey As String, pProv As String, Optional pYear As Integer = 0) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "PatientSmoking_Get", pKey, pProv, pYear)
        Return ds.Tables(0)
    End Function

    Public Function Smoking_GetByStatus(pLID As String, pType As String, pKey As String, pStatus As Integer, pProv As String, Optional pYear As Integer = 0) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Smoking_GetByStatus", pLID, pType, pKey, pStatus, pProv, pYear)
        Return ds.Tables(0)
    End Function

    Public Function Smoking_GetByProvinceGroup(pGRPID As String, pType As String, pKey As String, pStatus As Integer, pProv As String, Optional pYear As Integer = 0) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Smoking_GetByProvinceGroup", pGRPID, pType, pKey, pStatus, pProv, pYear)


        Return ds.Tables(0)
    End Function

    Public Function Smoking_GetByProjectID(pProjID As Integer, pType As String, pKey As String, pStatus As Integer, pProv As String, Optional pYear As Integer = 0) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Smoking_GetByProjectID", pProjID, pType, pKey, pStatus, pProv, pYear)
        Return ds.Tables(0)
    End Function

    Public Function Smoking_GetTypeIDByItemID(id As Integer, pid As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Smoking_GetTypeIDByItemID", id, pid)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Str(ds.Tables(0).Rows(0)(0))
        Else
            Return ""
        End If
    End Function

    Public Function Smoking_GetPatientIDByItemID(pID As Long) As Long
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Smoking_GetPatientIDByItemID"), pID)
        Try
            If ds.Tables(0).Rows.Count > 0 Then
                Return DBNull2Lng(ds.Tables(0).Rows(0)(0))
            Else
                Return 0
            End If
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Public Function Smoking_Order_GetByID(ServiceOrderID As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Smoking_Order_GetByID"), ServiceOrderID)
        Return ds.Tables(0)
    End Function
    Public Function Smoking_Order_GetA05(RefID As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Smoking_Order_GetA05"), RefID)
        Return ds.Tables(0)
    End Function

    Public Function Smoking_Order_GetA05(PatientID As Long, LocationID As String, Byear As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Smoking_Order_GetA05ByPatientLocation"), PatientID, LocationID, Byear)
        Return ds.Tables(0)
    End Function

    Public Function Smoking_GetFinalStopDate(pType As String, PatientID As Integer, LocationID As String, SEQNO As Integer) As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Smoking_GetFinalStopDate"), pType, PatientID, LocationID, SEQNO)

        Return ds.Tables(0)

    End Function

    Public Function Smoking_ChkDupCustomer(pType As String, PID As Integer, pYear As Integer) As Boolean

        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Smoking_ChkDupCustomerByPatientID"), pType, PID, pYear)

        If DBNull2Zero(ds.Tables(0).Rows(0)(0)) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function Smoking_Order_ChkDup(pType As String, PID As Integer, pYear As Integer, seq As Integer) As Boolean

        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Smoking_Order_ChkDup"), pType, PID, pYear, seq)

        If DBNull2Zero(ds.Tables(0).Rows(0)(0)) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function Smoking_Order_GetID(BYear As Integer, LocationID As String, SType As String, patientID As Long, FollowSEQ As Integer) As Integer

        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Smoking_Order_GetID"), BYear, LocationID, SType, patientID, FollowSEQ)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If

    End Function

    'Public Function Smoking_OrderByYear(Year As Integer, sType As String, PID As Integer) As DataTable
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, "Smoking_Order_GetByYear", Year, sType, PID)
    '    Return ds.Tables(0)

    'End Function

    Public Function CPA_Smoking_OrderByYear(Year As Integer, sType As String, PID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedNameCPA("Smoking_Order_GetByYear"), Year, sType, PID)
        Return ds.Tables(0)

    End Function

    'Public Function GetOrder_ByID(id As Long) As DataTable

    '    ds = SqlHelper.ExecuteDataset(ConnectionString, "Smoking_Order_GetByID", id)
    '    Return ds.Tables(0)

    'End Function

    Public Function CPA_SmokingOrder_ByID(id As Long) As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedNameCPA("Smoking_Order_GetByID"), id)
        Return ds.Tables(0)

    End Function


    Public Function Smoking_Order_GetTypeIDByItemID(id As Integer) As String
        SQL = "select ServiceTypeID  from  Service_Order  where itemID=" & id
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Str(ds.Tables(0).Rows(0)(0))
        Else
            Return ""
        End If
    End Function

    Public Function Smoking_Order_GetSEQByItemID(id As Integer) As Integer
        SQL = "select SeqNo  from  Service_Order  where itemID=" & id
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 1
        End If
    End Function

    Public Function Smoking_Order_GetPatientIDByItemID(pID As Long) As Long

        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Smoking_Order_GetPatientIDByItemID"), pID)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Lng(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If

    End Function

    Public Function Smoking_Order_Delete(itemID As Long) As Integer

        SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Smoking_Order_Delete"), itemID)

    End Function
    Public Function Smoking_Order_DeleteByUID(UID As Long) As Integer

        SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("Smoking_Order_DeleteByUID"), UID)

    End Function

    Public Function Service_GetPatinetProfile(SID As String, PID As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Service_GetPatinetProfile"), SID, PID)
        Return ds.Tables(0)
    End Function

    Public Function Service_GetPatinetProfileNotYearNow(SID As String, PID As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("Service_GetPatinetProfileNotYearNow"), SID, PID)
        Return ds.Tables(0)
    End Function

    Public Function Medicine_Get4VMI() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "VMI_Medicine_Get")
        Return ds.Tables(0)
    End Function
    Public Function Medicine_Get() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Medicine_Get")
        Return ds.Tables(0)
    End Function


    Public Function Medicine_GetUOM(pID As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Medicine_GetUOM", pID)
        If ds.Tables(0).Rows.Count > 0 Then
            Return String.Concat(ds.Tables(0).Rows(0)(0))
        Else
            Return ""
        End If
    End Function
    Public Function MedOrder_DeleteStatusQ(SID As Integer, TypeID As String, LocationID As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("MedOrder_DeleteStatusQ"), SID, TypeID, LocationID)
    End Function

    Public Function MedOrder_DeleteAll(SID As Integer, TypeID As String, LocationID As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MedOrder_DeleteAll", SID, TypeID, LocationID)
    End Function
    Public Function MedOrder_Delete(SID As Integer, TypeID As String, MedTime As Integer, LocationID As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MedOrder_Delete", SID, TypeID, MedTime, LocationID)
    End Function

    Public Function MedOrder_DeleteByStatus(TypeID As String, LocationID As String, PatientID As Long, StatusFlag As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedNameCPA("MedOrder_DeleteByStatus"), TypeID, LocationID, PatientID, StatusFlag)
    End Function
    'Public Function MedOrder_DeleteByItem(SID As Integer, ServiceTypeID As String, itemID As Integer, MedTime As Integer, LocationID As String) As Integer
    '    Return SqlHelper.ExecuteNonQuery(ConnectionString, "MedOrder_DeleteByItem", SID, ServiceTypeID, itemID, MedTime, LocationID)

    'End Function
    Public Function MedOrder_DisableByItemA4(PatientID As Integer, ServiceTypeID As String, itemID As String, LocationID As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedNameCPA("MedOrder_DisableByItemA4"), PatientID, ServiceTypeID, itemID, LocationID)

    End Function
    Public Function MedOrder_DisableByItemA5(PatientID As Integer, ServiceTypeID As String, itemID As Integer, MedTime As Integer, LocationID As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedNameCPA("MedOrder_DisableByItemA5"), PatientID, ServiceTypeID, itemID, MedTime, LocationID)

    End Function
    Public Function MedOrder_GetMedItem(ServiceUID As Long, ServiceTypeID As String, LocationID As String, PatientID As Long, BYear As Integer, SEQ As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("MedOrder_GetMedItem"), ServiceUID, ServiceTypeID, LocationID, PatientID, BYear, SEQ)
        Return ds.Tables(0)
    End Function

    Public Function CPA_MedOrder_GetMedItem(ServiceTypeID As String, LocationID As String, PatientID As Long, BYear As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedNameCPA("MedOrder_GetMedItem"), ServiceTypeID, LocationID, PatientID, BYear)
        Return ds.Tables(0)
    End Function
    Public Function CPA_MedOrder_GetMedItemA5(ServiceTypeID As String, LocationID As String, PatientID As Long, BYear As Integer, SEQ As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedNameCPA("MedOrder_GetMedItemA5"), ServiceTypeID, LocationID, PatientID, BYear, SEQ)
        Return ds.Tables(0)
    End Function

    Public Function MedOrder_GetMedA4(ServiceUID As Long, ServiceTypeID As String, LocationID As String, PatientID As Long, BYear As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedNameCPA("MedOrder_GetMedA4"), ServiceTypeID, LocationID, PatientID, BYear)
        Return ds.Tables(0)
    End Function
    Public Function MedOrder_GetMedA5(ServiceUID As Long, ServiceTypeID As String, MedTime As Integer, LocationID As String, PatientID As Long, BYear As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MedOrder_GetMedA5", ServiceUID, ServiceTypeID, MedTime, LocationID, PatientID, BYear)
        Return ds.Tables(0)
    End Function

    Public Function CPA_MedOrder_GetMedA5(ServiceTypeID As String, MedTime As Integer, LocationID As String, PatientID As Long, BYear As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "CPA_MedOrder_GetMedA5", ServiceTypeID, MedTime, LocationID, PatientID, BYear)
        Return ds.Tables(0)
    End Function

    Public Function MedOrder_Add(MedTime As Integer, ServiceUID As Long, itemID As Integer, itemName As String, QTY As Integer, MUser As String, ServiceTypeID As String, LocationiID As String, StatusFlag As String, patientid As Long, byear As Integer, seq As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MedOrder_Save", MedTime, ServiceUID, itemID, itemName, QTY, MUser, ServiceTypeID, LocationiID, StatusFlag, patientid, byear, seq)
    End Function
#Region "Smoking 2558"

    Public Function A01_Add( _
             ByVal ServiceTypeID As String _
           , ByVal ServiceDate As Long _
           , ByVal ServiceTime As Double _
           , ByVal LocationID As String _
           , ByVal PatientID As Long _
           , ByVal PersonID As Integer _
           , ByVal PTYPE As String _
           , ByVal PWeight As Double _
           , ByVal PHeigh As Double _
           , ByVal Diseases As String _
           , ByVal DiseasesOther As String _
           , ByVal Alcohol As Integer _
           , ByVal SmokeFQ As Integer _
           , ByVal SmokeDay As Integer _
           , ByVal SmokeUOM As String _
           , ByVal SmokeQTY As Integer _
           , ByVal StopDay As Integer _
           , ByVal StopUOM As String _
           , ByVal CGTYPE As Integer _
           , ByVal SmokerType As Integer _
           , ByVal SmokerTypeOther As String _
           , ByVal IsIntimate As Integer _
           , ByVal IntimateFriend As String _
           , ByVal Q1 As String _
           , ByVal Q1TIME As String _
           , ByVal Q1DAY As Integer _
           , ByVal Q1UOM As String _
           , ByVal Q2 As String _
           , ByVal Q2MED As String _
           , ByVal Q2Other As String _
           , ByVal Q3 As String _
           , ByVal Q3Other As String _
           , ByVal Q4 As Integer _
           , ByVal Q5 As Integer _
           , ByVal Q6 As Integer _
           , ByVal Q7 As Integer _
           , ByVal Q8 As String _
           , ByVal Q9 As String _
           , ByVal Q10 As String _
           , ByVal Q10Other As String _
           , ByVal PEFR_Value As Double _
           , ByVal PEFR_Rate As Double _
           , ByVal ServicePlan As String _
           , ByVal ServicePlanProblem As String _
           , ByVal ServicePlanRemark As String _
           , ByVal ContactTime As Integer _
           , ByVal StatusFlag As Integer _
           , ByVal CreateBy As String _
           , ByVal UpdBy As String, BYear As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "A01_Add" _
          , ServiceTypeID _
           , ServiceDate _
           , ServiceTime _
           , LocationID _
           , PatientID _
           , PersonID _
           , PTYPE _
           , PWeight _
           , PHeigh _
           , Diseases _
           , DiseasesOther _
           , Alcohol _
           , SmokeFQ _
           , SmokeDay _
           , SmokeUOM _
           , SmokeQTY _
           , StopDay _
           , StopUOM _
           , CGTYPE _
           , SmokerType _
           , SmokerTypeOther _
           , IsIntimate _
           , IntimateFriend _
           , Q1 _
           , Q1TIME _
           , Q1DAY _
           , Q1UOM _
           , Q2 _
           , Q2MED _
           , Q2Other _
           , Q3 _
           , Q3Other _
           , Q4 _
           , Q5 _
           , Q6 _
           , Q7 _
           , Q8 _
           , Q9 _
           , Q10 _
           , Q10Other _
           , PEFR_Value _
           , PEFR_Rate _
           , ServicePlan _
           , ServicePlanProblem _
           , ServicePlanRemark _
           , ContactTime _
           , StatusFlag _
           , CreateBy _
           , UpdBy, BYear)

    End Function
    Public Function A01_Update(ByVal ServiceID As Long _
           , ByVal ServiceTypeID As String _
           , ByVal ServiceDate As Long _
           , ByVal ServiceTime As Double _
           , ByVal LocationID As String _
           , ByVal PatientID As Long _
           , ByVal PersonID As Integer _
           , ByVal PTYPE As String _
           , ByVal PWeight As Double _
           , ByVal PHeigh As Double _
           , ByVal Diseases As String _
           , ByVal DiseasesOther As String _
           , ByVal Alcohol As Integer _
           , ByVal SmokeFQ As Integer _
           , ByVal SmokeDay As Integer _
           , ByVal SmokeUOM As String _
           , ByVal SmokeQTY As Integer _
           , ByVal StopDay As Integer _
           , ByVal StopUOM As String _
           , ByVal CGTYPE As Integer _
           , ByVal SmokerType As Integer _
           , ByVal SmokerTypeOther As String _
           , ByVal IsIntimate As Integer _
           , ByVal IntimateFriend As String _
           , ByVal Q1 As String _
           , ByVal Q1TIME As String _
           , ByVal Q1DAY As Integer _
           , ByVal Q1UOM As String _
           , ByVal Q2 As String _
           , ByVal Q2MED As String _
           , ByVal Q2Other As String _
           , ByVal Q3 As String _
           , ByVal Q3Other As String _
           , ByVal Q4 As Integer _
           , ByVal Q5 As Integer _
           , ByVal Q6 As Integer _
           , ByVal Q7 As Integer _
           , ByVal Q8 As String _
           , ByVal Q9 As String _
           , ByVal Q10 As String _
           , ByVal Q10Other As String _
           , ByVal PEFR_Value As Double _
           , ByVal PEFR_Rate As Double _
           , ByVal ServicePlan As String _
           , ByVal ServicePlanProblem As String _
           , ByVal ServicePlanRemark As String _
           , ByVal ContactTime As Integer _
           , ByVal StatusFlag As Integer _
           , ByVal UpdBy As String, BYear As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "A01_Update" _
           , ServiceID _
           , ServiceTypeID _
           , ServiceDate _
           , ServiceTime _
           , LocationID _
           , PatientID _
           , PersonID _
           , PTYPE _
           , PWeight _
           , PHeigh _
           , Diseases _
           , DiseasesOther _
           , Alcohol _
           , SmokeFQ _
           , SmokeDay _
           , SmokeUOM _
           , SmokeQTY _
           , StopDay _
           , StopUOM _
           , CGTYPE _
           , SmokerType _
           , SmokerTypeOther _
           , IsIntimate _
           , IntimateFriend _
           , Q1 _
           , Q1TIME _
           , Q1DAY _
           , Q1UOM _
           , Q2 _
           , Q2MED _
           , Q2Other _
           , Q3 _
           , Q3Other _
           , Q4 _
           , Q5 _
           , Q6 _
           , Q7 _
           , Q8 _
           , Q9 _
           , Q10 _
           , Q10Other _
           , PEFR_Value _
           , PEFR_Rate _
           , ServicePlan _
           , ServicePlanProblem _
           , ServicePlanRemark _
           , ContactTime _
           , StatusFlag _
           , UpdBy, BYear)


    End Function
    Public Function A04_Add( _
             ByVal RefID As Long _
           , ByVal ServiceTypeID As String _
           , ByVal ServiceDate As Long _
           , ByVal ServiceTime As Double _
           , ByVal LocationID As String _
           , ByVal PatientID As Long _
           , ByVal PersonID As Integer _
           , ByVal PWeight As Double _
           , ByVal PHeigh As Double _
           , ByVal PEFR_Value As Double _
           , ByVal PEFR_Rate As Double _
           , ByVal StopPlan As Integer _
           , ByVal StopDate As String _
           , ByVal TargetDay As Integer _
           , ByVal TargetDate As String _
           , ByVal CGTargetRate As Integer _
           , ByVal IsFollow As Integer _
           , ByVal FollowDate As String _
           , ByVal FollowChannel As String _
           , ByVal FollowRemark As String _
           , ByVal FinalResult As Integer _
           , ByVal FinalStopDate As String _
           , ByVal StillSmoke As Integer _
           , ByVal ChangeRate As String _
           , ByVal NicotineEffect As String _
           , ByVal MedEffect As String _
           , ByVal ServicePlan As String _
           , ByVal ServicePlanProblem As String _
           , ByVal ServicePlanRemark As String _
           , ByVal ContactTime As Integer _
           , ByVal StatusFlag As Integer _
           , ByVal CreateBy As String _
           , ByVal UpdBy As String _
           , ByVal isA04 As Integer _
           , ByVal Recommend1 As String, ByVal Recommend2 As String, ByVal Recommend3 As String, ByVal Recommend4 As String, Byear As Integer, PTYPE As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "A04_Add" _
           , RefID _
           , ServiceTypeID _
           , ServiceDate _
           , ServiceTime _
           , LocationID _
           , PatientID _
           , PersonID _
           , PWeight _
           , PHeigh _
           , PEFR_Value _
           , PEFR_Rate _
           , StopPlan _
           , StopDate _
           , TargetDay _
           , TargetDate _
           , CGTargetRate _
           , IsFollow _
           , FollowDate _
           , FollowChannel _
           , FollowRemark _
           , FinalResult _
           , FinalStopDate _
           , StillSmoke _
           , ChangeRate _
           , NicotineEffect _
           , MedEffect _
           , ServicePlan _
           , ServicePlanProblem _
           , ServicePlanRemark _
           , ContactTime _
           , StatusFlag _
           , CreateBy _
           , UpdBy _
           , isA04 _
           , Recommend1, Recommend2, Recommend3, Recommend4, Byear, PTYPE)

    End Function
    Public Function A04_Update(ByVal ServiceUID As Long _
          , ByVal ServiceTypeID As String _
          , ByVal ServiceDate As Long _
          , ByVal ServiceTime As Double _
          , ByVal LocationID As String _
          , ByVal PatientID As Long _
          , ByVal PersonID As Integer _
          , ByVal PWeight As Double _
          , ByVal PHeigh As Double _
          , ByVal PEFR_Value As Double _
          , ByVal PEFR_Rate As Double _
          , ByVal StopPlan As Integer _
          , ByVal StopDate As String _
          , ByVal TargetDay As Integer _
          , ByVal TargetDate As String _
          , ByVal CGTargetRate As Integer _
          , ByVal IsFollow As Integer _
          , ByVal FollowDate As String _
          , ByVal FollowChannel As String _
          , ByVal FollowRemark As String _
          , ByVal FinalResult As Integer _
          , ByVal FinalStopDate As String _
          , ByVal StillSmoke As Integer _
          , ByVal ChangeRate As String _
          , ByVal NicotineEffect As String _
          , ByVal MedEffect As String _
          , ByVal ServicePlan As String _
          , ByVal ServicePlanProblem As String _
          , ByVal ServicePlanRemark As String _
          , ByVal ContactTime As Integer _
          , ByVal StatusFlag As Integer _
          , ByVal CreateBy As String _
          , ByVal UpdBy As String _
          , ByVal isA04 As Integer _
          , ByVal Recommend1 As String, ByVal Recommend2 As String, ByVal Recommend3 As String, ByVal Recommend4 As String, Byear As Integer, PTYPE As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "A04_Update" _
           , ServiceUID _
           , ServiceTypeID _
           , ServiceDate _
           , ServiceTime _
           , LocationID _
           , PatientID _
           , PersonID _
           , PWeight _
           , PHeigh _
           , PEFR_Value _
           , PEFR_Rate _
           , StopPlan _
           , StopDate _
           , TargetDay _
           , TargetDate _
           , CGTargetRate _
           , IsFollow _
           , FollowDate _
           , FollowChannel _
           , FollowRemark _
           , FinalResult _
           , FinalStopDate _
           , StillSmoke _
           , ChangeRate _
           , NicotineEffect _
           , MedEffect _
           , ServicePlan _
           , ServicePlanProblem _
           , ServicePlanRemark _
           , ContactTime _
           , StatusFlag _
           , CreateBy _
           , UpdBy _
           , isA04 _
           , Recommend1, Recommend2, Recommend3, Recommend4, Byear, PTYPE)

    End Function
    Public Function A05_Add( _
          ByVal RefID As Long _
        , ByVal FollowSEQ As Integer _
        , ByVal ServiceTypeID As String _
        , ByVal ServiceDate As Long _
        , ByVal ServiceTime As Double _
        , ByVal LocationID As String _
        , ByVal PatientID As Long _
        , ByVal PersonID As Integer _
        , ByVal FollowChannel As String _
        , ByVal FollowRemark As String _
        , ByVal FinalResult As Integer _
        , ByVal FinalStopDate As String _
        , ByVal StillSmoke As Integer _
        , ByVal ChangeRate As String _
        , ByVal NicotineEffect As String _
        , ByVal MedEffect As String _
        , ByVal ServicePlan As String _
        , ByVal ServicePlanProblem As String _
        , ByVal ServicePlanRemark As String _
        , ByVal ContactTime As Integer _
        , ByVal StatusFlag As Integer _
        , ByVal CreateBy As String _
        , ByVal UpdBy As String, ByVal Recommend1 As String, ByVal Recommend2 As String, ByVal Recommend3 As String, ByVal Recommend4 As String, Byear As Integer, PTYPE As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "A05_Add" _
           , RefID _
           , FollowSEQ _
           , ServiceTypeID _
           , ServiceDate _
           , ServiceTime _
           , LocationID _
           , PatientID _
           , PersonID _
           , FollowChannel _
           , FollowRemark _
           , FinalResult _
           , FinalStopDate _
           , StillSmoke _
           , ChangeRate _
           , NicotineEffect _
           , MedEffect _
           , ServicePlan _
           , ServicePlanProblem _
           , ServicePlanRemark _
           , ContactTime _
           , StatusFlag _
           , CreateBy _
           , UpdBy _
           , Recommend1, Recommend2, Recommend3, Recommend4, Byear, PTYPE)

    End Function
    Public Function A05_Update( _
         ByVal ServiceUID As Long _
       , ByVal FollowSEQ As Integer _
       , ByVal ServiceTypeID As String _
       , ByVal ServiceDate As Long _
       , ByVal ServiceTime As Double _
       , ByVal LocationID As String _
       , ByVal PatientID As Long _
       , ByVal PersonID As Integer _
       , ByVal FollowChannel As String _
       , ByVal FollowRemark As String _
       , ByVal FinalResult As Integer _
       , ByVal FinalStopDate As String _
       , ByVal StillSmoke As Integer _
       , ByVal ChangeRate As String _
       , ByVal NicotineEffect As String _
       , ByVal MedEffect As String _
       , ByVal ServicePlan As String _
       , ByVal ServicePlanProblem As String _
       , ByVal ServicePlanRemark As String _
       , ByVal ContactTime As Integer _
       , ByVal StatusFlag As Integer _
       , ByVal UpdBy As String _
       , ByVal Recommend1 As String, ByVal Recommend2 As String, ByVal Recommend3 As String, ByVal Recommend4 As String, Byear As Integer, PTYPE As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "A05_Update" _
           , FollowSEQ _
           , ServiceUID _
           , ServiceTypeID _
           , ServiceDate _
           , ServiceTime _
           , LocationID _
           , PatientID _
           , PersonID _
           , FollowChannel _
           , FollowRemark _
           , FinalResult _
           , FinalStopDate _
           , StillSmoke _
           , ChangeRate _
           , NicotineEffect _
           , MedEffect _
           , ServicePlan _
           , ServicePlanProblem _
           , ServicePlanRemark _
           , ContactTime _
           , StatusFlag _
           , UpdBy _
           , Recommend1, Recommend2, Recommend3, Recommend4, Byear, PTYPE)

    End Function
#End Region

    Public Function A05_GetCount(RefID As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, GetFullyQualifiedName("A05_GetCount"), RefID)

        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function


    Public Function A01_UpdateA04Count(ByVal itemID As Integer, ByVal EduCount As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "A01_UpdateA04Count", itemID, EduCount)
    End Function
    Public Function A01_UpdateA05Count(ByVal itemID As Integer, ByVal EduCount As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "A01_UpdateA05Count", itemID, EduCount)
    End Function

    Public Function A01_UpdateCloseStatus(ByVal itemID As Integer, ByVal Status As Integer, ByVal UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "A01_UpdateCloseStatus", itemID, Status, UpdBy)
    End Function
#Region "Smoking 2568"
    Public Function A1_Save(
             ByVal ServiceUID As Integer _
           , ByVal BYear As Integer _
           , ByVal ServiceTypeID As String _
           , ByVal ServiceDate As Long _
           , ByVal ServiceTime As Double _
           , ByVal LocationID As String _
           , ByVal PatientID As Long _
           , ByVal PersonID As Integer _
           , ByVal PTYPE As String _
           , ByVal PFrom As String _
           , ByVal FromTXT As String _
           , ByVal MTMService As String _
           , ByVal ServiceRemark As String _
           , ByVal ServiceRef As String _
           , ByVal TelepharmacyMethod As String _
           , ByVal RecordMethod As String _
           , ByVal RecordLocation As String _
           , ByVal TelepharmacyRemark As String _
           , ByVal Alcohol As Integer _
           , ByVal SmokeFQ As Integer _
           , ByVal SmokeDay As Integer _
           , ByVal SmokeUOM As String _
           , ByVal SmokeQTY As Integer _
           , ByVal StopDay As Integer _
           , ByVal StopUOM As String _
           , ByVal CGTYPE As Integer _
           , ByVal Nicotin As String _
           , ByVal Often As Integer _
           , ByVal SmokeTime As Integer _
           , ByVal IsIntimate As Integer _
           , ByVal IntimateFriend As String _
           , ByVal Q1 As String _
           , ByVal Q1TIME As String _
           , ByVal Q1DAY As Integer _
           , ByVal Q1UOM As String _
           , ByVal Q2 As String _
           , ByVal Q2MED As String _
           , ByVal Q2Other As String _
           , ByVal Q5 As Integer _
           , ByVal Q6 As Integer _
           , ByVal Q7 As Integer _
           , ByVal Q8 As Integer _
           , ByVal Q9 As Integer _
           , ByVal Q10 As Integer _
           , ByVal Q11 As Integer _
           , ByVal FTND As Integer _
           , ByVal Q12 As String _
           , ByVal Q13 As String _
           , ByVal Q14 As String _
           , ByVal Q14Other As String _
           , ByVal PWeight As Double _
           , ByVal PHeigh As Double _
           , ByVal BP As String _
           , ByVal PEFR_Value As Double _
           , ByVal PEFR_Rate As Double _
           , ByVal COvalue As Double _
           , ByVal ServicePlan1 As String _
           , ByVal QuitPlanDate As String _
           , ByVal NextDate As String _
           , ByVal StatusFlag As Integer _
           , ByVal CreateBy As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "A1_Save" _
           , ServiceUID, BYear _
           , ServiceTypeID _
           , ServiceDate _
           , ServiceTime _
           , LocationID _
           , PatientID _
           , PersonID _
           , PTYPE _
           , PFrom _
           , FromTXT _
           , MTMService _
           , ServiceRemark _
           , ServiceRef _
           , TelepharmacyMethod _
           , RecordMethod _
           , RecordLocation _
           , TelepharmacyRemark _
           , Alcohol _
           , SmokeFQ _
           , SmokeDay _
           , SmokeUOM _
           , SmokeQTY _
           , StopDay _
           , StopUOM _
           , CGTYPE _
           , Nicotin _
           , Often _
           , SmokeTime _
           , IsIntimate _
           , IntimateFriend _
           , Q1 _
           , Q1TIME _
           , Q1DAY _
           , Q1UOM _
           , Q2 _
           , Q2MED _
           , Q2Other _
           , Q5 _
           , Q6 _
           , Q7 _
           , Q8 _
           , Q9 _
           , Q10 _
           , Q11 _
           , FTND _
           , Q12 _
           , Q13 _
           , Q14 _
           , Q14Other _
           , PWeight _
           , PHeigh _
           , BP _
           , PEFR_Value _
           , PEFR_Rate _
           , COvalue _
           , ServicePlan1 _
           , QuitPlanDate _
           , NextDate _
           , StatusFlag _
           , CreateBy)

    End Function
#End Region
#Region "Smoking 2559"
    Public Function CPA_A01_Add(
             ByVal BYear As Integer _
           , ByVal ServiceTypeID As String _
           , ByVal ServiceDate As Long _
           , ByVal ServiceTime As Double _
           , ByVal LocationID As String _
           , ByVal PatientID As Long _
           , ByVal PersonID As Integer _
           , ByVal PTYPE As String _
           , ByVal PWeight As Double _
           , ByVal PHeigh As Double _
           , ByVal BP As String _
           , ByVal PCareer As String _
           , ByVal Diseases As String _
           , ByVal DiseasesOther As String _
           , ByVal DrugUsage As String _
           , ByVal Alcohol As Integer _
           , ByVal SmokeFQ As Integer _
           , ByVal SmokeDay As Integer _
           , ByVal SmokeUOM As String _
           , ByVal SmokeQTY As Integer _
           , ByVal StopDay As Integer _
           , ByVal StopUOM As String _
           , ByVal CGTYPE As Integer _
           , ByVal IsIntimate As Integer _
           , ByVal IntimateFriend As String _
           , ByVal Q1 As String _
           , ByVal Q1TIME As String _
           , ByVal Q1DAY As Integer _
           , ByVal Q1UOM As String _
           , ByVal Q2 As String _
           , ByVal Q2MED As String _
           , ByVal Q2Other As String _
           , ByVal Q3 As String _
           , ByVal Q3Other As String _
           , ByVal Q4 As Integer _
           , ByVal Q5 As Integer _
           , ByVal Q6 As Integer _
           , ByVal Q7 As Integer _
           , ByVal Q8 As Integer _
           , ByVal Q9 As Integer _
           , ByVal Q10 As Integer _
           , ByVal Q11 As Integer _
           , ByVal Q12 As String _
           , ByVal Q13 As String _
           , ByVal Q14 As String _
           , ByVal Q14Other As String _
           , ByVal PEFR_Value As Double _
           , ByVal PEFR_Rate As Double _
           , ByVal COvalue As Double _
           , ByVal ServicePlan1 As String _
           , ByVal DateServicePlan1 As String _
           , ByVal ServicePlan2 As Integer _
           , ByVal CGTargetRateBegin As Integer _
           , ByVal CGTargetRateEnd As Integer _
           , ByVal UntilDay As Integer _
           , ByVal DateServicePlan2 As String _
           , ByVal NextDate As String _
           , ByVal StatusFlag As Integer _
           , ByVal CreateBy As String _
           , ByVal PFrom As String _
           , ByVal FromTXT As String _
           , ByVal MTMService As String _
           , ByVal ServiceRemark As String _
           , ByVal ServiceRef As String _
           , ByVal TelepharmacyMethod As String _
           , ByVal RecordMethod As String _
           , ByVal RecordLocation As String _
           , ByVal TelepharmacyRemark As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedNameCPA("A01_Add") _
           , BYear _
           , ServiceTypeID _
           , ServiceDate _
           , ServiceTime _
           , LocationID _
           , PatientID _
           , PersonID _
           , PTYPE _
           , PWeight _
           , PHeigh _
           , BP _
           , PCareer _
           , Diseases _
           , DiseasesOther _
           , DrugUsage _
           , Alcohol _
           , SmokeFQ _
           , SmokeDay _
           , SmokeUOM _
           , SmokeQTY _
           , StopDay _
           , StopUOM _
           , CGTYPE _
           , IsIntimate _
           , IntimateFriend _
           , Q1 _
           , Q1TIME _
           , Q1DAY _
           , Q1UOM _
           , Q2 _
           , Q2MED _
           , Q2Other _
           , Q3 _
           , Q3Other _
           , Q4 _
           , Q5 _
           , Q6 _
           , Q7 _
           , Q8 _
           , Q9 _
           , Q10 _
           , Q11 _
           , Q12 _
           , Q13 _
           , Q14 _
           , Q14Other _
           , PEFR_Value _
           , PEFR_Rate _
           , COvalue _
           , ServicePlan1 _
           , DateServicePlan1 _
           , ServicePlan2 _
           , CGTargetRateBegin _
           , CGTargetRateEnd _
           , UntilDay _
           , DateServicePlan2 _
           , NextDate _
           , StatusFlag _
           , CreateBy _
           , PFrom _
           , FromTXT _
           , MTMService _
           , ServiceRemark _
           , ServiceRef _
           , TelepharmacyMethod _
           , RecordMethod _
           , RecordLocation _
           , TelepharmacyRemark)

    End Function
    Public Function CPA_A01_Update(ByVal ServiceID As Long _
           , ByVal BYear As Integer _
           , ByVal ServiceTypeID As String _
           , ByVal ServiceDate As Long _
           , ByVal ServiceTime As Double _
           , ByVal LocationID As String _
           , ByVal PatientID As Long _
           , ByVal PersonID As Integer _
           , ByVal PTYPE As String _
           , ByVal PWeight As Double _
           , ByVal PHeigh As Double _
           , ByVal BP As String _
           , ByVal PCareer As String _
           , ByVal Diseases As String _
           , ByVal DiseasesOther As String _
           , ByVal MedicineDesc As String _
           , ByVal Alcohol As Integer _
           , ByVal SmokeFQ As Integer _
           , ByVal SmokeDay As Integer _
           , ByVal SmokeUOM As String _
           , ByVal SmokeQTY As Integer _
           , ByVal StopDay As Integer _
           , ByVal StopUOM As String _
           , ByVal CGTYPE As Integer _
           , ByVal IsIntimate As Integer _
           , ByVal IntimateFriend As String _
           , ByVal Q1 As String _
           , ByVal Q1TIME As String _
           , ByVal Q1DAY As Integer _
           , ByVal Q1UOM As String _
           , ByVal Q2 As String _
           , ByVal Q2MED As String _
           , ByVal Q2Other As String _
           , ByVal Q3 As String _
           , ByVal Q3Other As String _
           , ByVal Q4 As Integer _
           , ByVal Q5 As Integer _
           , ByVal Q6 As Integer _
           , ByVal Q7 As Integer _
           , ByVal Q8 As Integer _
           , ByVal Q9 As Integer _
           , ByVal Q10 As Integer _
           , ByVal Q11 As Integer _
           , ByVal Q12 As String _
           , ByVal Q13 As String _
           , ByVal Q14 As String _
           , ByVal Q14Other As String _
           , ByVal PEFR_Value As Double _
           , ByVal PEFR_Rate As Double _
           , ByVal COvalue As Double _
           , ByVal ServicePlan1 As String _
           , ByVal DateServicePlan1 As String _
           , ByVal ServicePlan2 As Integer _
           , ByVal CGTargetRateBegin As Integer _
           , ByVal CGTargetRateEnd As Integer _
           , ByVal UntilDay As Integer _
           , ByVal DateServicePlan2 As String _
           , ByVal NextDate As String _
           , ByVal StatusFlag As Integer _
           , ByVal UpdBy As String _
           , ByVal PFrom As String _
           , ByVal FromTXT As String _
           , ByVal MTMService As String _
           , ByVal ServiceRemark As String _
           , ByVal ServiceRef As String _
           , ByVal TelepharmacyMethod As String _
           , ByVal RecordMethod As String _
           , ByVal RecordLocation As String _
           , ByVal TelepharmacyRemark As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedNameCPA("A01_Update") _
           , ServiceID _
           , BYear _
           , ServiceTypeID _
           , ServiceDate _
           , ServiceTime _
           , LocationID _
           , PatientID _
           , PersonID _
           , PTYPE _
           , PWeight _
           , PHeigh _
           , BP _
           , PCareer _
           , Diseases _
           , DiseasesOther _
           , MedicineDesc _
           , Alcohol _
           , SmokeFQ _
           , SmokeDay _
           , SmokeUOM _
           , SmokeQTY _
           , StopDay _
           , StopUOM _
           , CGTYPE _
           , IsIntimate _
           , IntimateFriend _
           , Q1 _
           , Q1TIME _
           , Q1DAY _
           , Q1UOM _
           , Q2 _
           , Q2MED _
           , Q2Other _
           , Q3 _
           , Q3Other _
           , Q4 _
           , Q5 _
           , Q6 _
           , Q7 _
           , Q8 _
           , Q9 _
           , Q10 _
           , Q11 _
           , Q12 _
           , Q13 _
           , Q14 _
           , Q14Other _
           , PEFR_Value _
           , PEFR_Rate _
           , COvalue _
           , ServicePlan1 _
           , DateServicePlan1 _
           , ServicePlan2 _
           , CGTargetRateBegin _
           , CGTargetRateEnd _
           , UntilDay _
           , DateServicePlan2 _
           , NextDate _
           , StatusFlag _
           , UpdBy _
           , PFrom _
           , FromTXT _
           , MTMService _
           , ServiceRemark _
           , ServiceRef _
           , TelepharmacyMethod _
           , RecordMethod _
           , RecordLocation _
           , TelepharmacyRemark)


    End Function
    Public Function CPA_A04_Add(
             ByVal RefID As Long _
           , ByVal BYear As Integer _
           , ByVal ServiceTypeID As String _
           , ByVal ServiceDate As Long _
           , ByVal ServiceTime As Double _
           , ByVal LocationID As String _
           , ByVal PatientID As Long _
           , ByVal PersonID As Integer _
           , ByVal PWeight As Double _
           , ByVal PHeigh As Double _
           , ByVal PEFR_Value As Double _
           , ByVal PEFR_Rate As Double _
           , ByVal CO_Value As Double _
           , ByVal FinalResult As Integer _
           , ByVal StillSmoke As Integer _
           , ByVal FinalStopDate As String _
           , ByVal ChangeRate As String _
           , ByVal NicotineEffect As String _
           , ByVal Recommend1 As String _
           , ByVal MedEffect As String _
           , ByVal Recommend2 As String _
           , ByVal Believe As String _
           , ByVal Recommend3 As String _
           , ByVal Recommend4 As String _
           , ByVal StopPlan1 As Integer _
           , ByVal StopDate1 As String _
           , ByVal StopPlan2 As Integer _
           , ByVal CGTargetRateFrom As Integer _
           , ByVal CGTargetRateTo As Integer _
           , ByVal TargetDay As Integer _
           , ByVal TargetDate As String _
           , ByVal NextFollowDate As String _
           , ByVal StatusFlag As Integer _
           , ByVal CreateBy As String _
           , ByVal PFrom As String _
           , ByVal FromTXT As String _
           , ByVal MTMService As String _
           , ByVal ServiceRemark As String _
           , ByVal ServiceRef As String _
           , ByVal TelepharmacyMethod As String _
           , ByVal RecordMethod As String _
           , ByVal RecordLocation As String _
           , ByVal TelepharmacyRemark As String) As Integer


        Return SqlHelper.ExecuteNonQuery(ConnectionString, "CPA_A04_Add" _
           , RefID _
           , BYear _
           , ServiceTypeID _
           , ServiceDate _
           , ServiceTime _
           , LocationID _
           , PatientID _
           , PersonID _
           , PWeight _
           , PHeigh _
           , PEFR_Value _
           , PEFR_Rate _
           , CO_Value _
           , FinalResult _
           , StillSmoke _
           , FinalStopDate _
           , ChangeRate _
           , NicotineEffect _
           , Recommend1 _
           , MedEffect _
           , Recommend2 _
           , Believe _
           , Recommend3 _
           , Recommend4 _
           , StopPlan1 _
           , StopDate1 _
           , StopPlan2 _
           , CGTargetRateFrom _
           , CGTargetRateTo _
           , TargetDay _
           , TargetDate _
           , NextFollowDate _
           , StatusFlag _
           , CreateBy _
           , PFrom _
           , FromTXT _
           , MTMService _
           , ServiceRemark _
           , ServiceRef _
           , TelepharmacyMethod _
           , RecordMethod _
           , RecordLocation _
           , TelepharmacyRemark)

    End Function
    Public Function CPA_A04_Update(ByVal ServiceUID As Long, BYear As Integer _
           , ByVal ServiceTypeID As String _
           , ByVal ServiceDate As Long _
           , ByVal ServiceTime As Double _
           , ByVal LocationID As String _
           , ByVal PatientID As Long _
           , ByVal PersonID As Integer _
           , ByVal PWeight As Double _
           , ByVal PHeigh As Double _
           , ByVal PEFR_Value As Double _
           , ByVal PEFR_Rate As Double _
           , ByVal CO_Value As Double _
           , ByVal FinalResult As Integer _
           , ByVal StillSmoke As Integer _
           , ByVal FinalStopDate As String _
           , ByVal ChangeRate As String _
           , ByVal NicotineEffect As String _
           , ByVal Recommend1 As String _
           , ByVal MedEffect As String _
           , ByVal Recommend2 As String _
           , ByVal Believe As String _
           , ByVal Recommend3 As String _
           , ByVal Recommend4 As String _
           , ByVal StopPlan1 As Integer _
           , ByVal StopDate1 As String _
           , ByVal StopPlan2 As Integer _
           , ByVal CGTargetRateFrom As Integer _
           , ByVal CGTargetRateTo As Integer _
           , ByVal TargetDay As Integer _
           , ByVal TargetDate As String _
           , ByVal NextFollowDate As String _
           , ByVal StatusFlag As Integer _
           , ByVal CreateBy As String _
           , ByVal PFrom As String _
           , ByVal FromTXT As String _
           , ByVal MTMService As String _
           , ByVal ServiceRemark As String _
           , ByVal ServiceRef As String _
           , ByVal TelepharmacyMethod As String _
           , ByVal RecordMethod As String _
           , ByVal RecordLocation As String _
           , ByVal TelepharmacyRemark As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "CPA_A04_Update" _
           , ServiceUID _
           , BYear _
           , ServiceTypeID _
           , ServiceDate _
           , ServiceTime _
           , LocationID _
           , PatientID _
           , PersonID _
           , PWeight _
           , PHeigh _
           , PEFR_Value _
           , PEFR_Rate _
           , CO_Value _
           , FinalResult _
           , StillSmoke _
           , FinalStopDate _
           , ChangeRate _
           , NicotineEffect _
           , Recommend1 _
           , MedEffect _
           , Recommend2 _
           , Believe _
           , Recommend3 _
           , Recommend4 _
           , StopPlan1 _
           , StopDate1 _
           , StopPlan2 _
           , CGTargetRateFrom _
           , CGTargetRateTo _
           , TargetDay _
           , TargetDate _
           , NextFollowDate _
           , StatusFlag _
           , CreateBy _
           , PFrom _
           , FromTXT _
           , MTMService _
           , ServiceRemark _
           , ServiceRef _
           , TelepharmacyMethod _
           , RecordMethod _
           , RecordLocation _
           , TelepharmacyRemark)

    End Function
    Public Function CPA_A05_Add(
            ByVal RefID As Long _
          , ByVal BYear As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal ServiceDate As Long _
          , ByVal ServiceTime As Double _
          , ByVal LocationID As String _
          , ByVal PatientID As Long _
          , ByVal PersonID As Integer _
          , ByVal PWeight As Double _
          , ByVal PHeigh As Double _
          , ByVal PEFR_Value As Double _
          , ByVal PEFR_Rate As Double _
          , ByVal CO_Value As Double _
          , ByVal FinalResult As Integer _
          , ByVal StillSmoke As Integer _
          , ByVal FinalStopDate As String _
          , ByVal ChangeRate As String _
          , ByVal NicotineEffect As String _
          , ByVal Recommend1 As String _
          , ByVal MedEffect As String _
          , ByVal Recommend2 As String _
          , ByVal Believe As String _
          , ByVal Recommend3 As String _
          , ByVal Recommend4 As String _
          , ByVal StopPlan1 As Integer _
          , ByVal StopDate1 As String _
          , ByVal StopPlan2 As Integer _
          , ByVal CGTargetRateFrom As Integer _
          , ByVal CGTargetRateTo As Integer _
          , ByVal TargetDay As Integer _
          , ByVal TargetDate As String _
          , ByVal NextFollowDate As String _
          , ByVal StatusFlag As Integer _
          , ByVal CreateBy As String, ByVal FollowSEQ As Integer, ByVal FollowChannel As String _
          , ByVal FollowRemark As String _
          , ByVal PFrom As String _
           , ByVal FromTXT As String _
           , ByVal MTMService As String _
           , ByVal ServiceRemark As String _
           , ByVal ServiceRef As String _
           , ByVal TelepharmacyMethod As String _
           , ByVal RecordMethod As String _
           , ByVal RecordLocation As String _
           , ByVal TelepharmacyRemark As String) As Integer


        Return SqlHelper.ExecuteNonQuery(ConnectionString, "CPA_A05_Add" _
           , RefID _
           , BYear _
           , ServiceTypeID _
           , ServiceDate _
           , ServiceTime _
           , LocationID _
           , PatientID _
           , PersonID _
           , PWeight _
           , PHeigh _
           , PEFR_Value _
           , PEFR_Rate _
           , CO_Value _
           , FinalResult _
           , StillSmoke _
           , FinalStopDate _
           , ChangeRate _
           , NicotineEffect _
           , Recommend1 _
           , MedEffect _
           , Recommend2 _
           , Believe _
           , Recommend3 _
           , Recommend4 _
           , StopPlan1 _
           , StopDate1 _
           , StopPlan2 _
           , CGTargetRateFrom _
           , CGTargetRateTo _
           , TargetDay _
           , TargetDate _
           , NextFollowDate _
           , StatusFlag _
           , CreateBy, FollowSEQ, FollowChannel, FollowRemark _
           , PFrom _
           , FromTXT _
           , MTMService _
           , ServiceRemark _
           , ServiceRef _
           , TelepharmacyMethod _
           , RecordMethod _
           , RecordLocation _
           , TelepharmacyRemark)

    End Function
    Public Function CPA_A05_Update(ByVal ServiceUID As Long, BYear As Integer _
           , ByVal ServiceTypeID As String _
           , ByVal ServiceDate As Long _
           , ByVal ServiceTime As Double _
           , ByVal LocationID As String _
           , ByVal PatientID As Long _
           , ByVal PersonID As Integer _
           , ByVal PWeight As Double _
           , ByVal PHeigh As Double _
           , ByVal PEFR_Value As Double _
           , ByVal PEFR_Rate As Double _
           , ByVal CO_Value As Double _
           , ByVal FinalResult As Integer _
           , ByVal StillSmoke As Integer _
           , ByVal FinalStopDate As String _
           , ByVal ChangeRate As String _
           , ByVal NicotineEffect As String _
           , ByVal Recommend1 As String _
           , ByVal MedEffect As String _
           , ByVal Recommend2 As String _
           , ByVal Believe As String _
           , ByVal Recommend3 As String _
           , ByVal Recommend4 As String _
           , ByVal StopPlan1 As Integer _
           , ByVal StopDate1 As String _
           , ByVal StopPlan2 As Integer _
           , ByVal CGTargetRateFrom As Integer _
           , ByVal CGTargetRateTo As Integer _
           , ByVal TargetDay As Integer _
           , ByVal TargetDate As String _
           , ByVal NextFollowDate As String _
           , ByVal StatusFlag As Integer _
           , ByVal CreateBy As String, ByVal FollowSEQ As Integer, ByVal FollowChannel As String _
          , ByVal FollowRemark As String _
          , ByVal PFrom As String _
           , ByVal FromTXT As String _
           , ByVal MTMService As String _
           , ByVal ServiceRemark As String _
           , ByVal ServiceRef As String _
           , ByVal TelepharmacyMethod As String _
           , ByVal RecordMethod As String _
           , ByVal RecordLocation As String _
           , ByVal TelepharmacyRemark As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "CPA_A05_Update" _
           , ServiceUID _
           , BYear _
           , ServiceTypeID _
           , ServiceDate _
           , ServiceTime _
           , LocationID _
           , PatientID _
           , PersonID _
           , PWeight _
           , PHeigh _
           , PEFR_Value _
           , PEFR_Rate _
           , CO_Value _
           , FinalResult _
           , StillSmoke _
           , FinalStopDate _
           , ChangeRate _
           , NicotineEffect _
           , Recommend1 _
           , MedEffect _
           , Recommend2 _
           , Believe _
           , Recommend3 _
           , Recommend4 _
           , StopPlan1 _
           , StopDate1 _
           , StopPlan2 _
           , CGTargetRateFrom _
           , CGTargetRateTo _
           , TargetDay _
           , TargetDate _
           , NextFollowDate _
           , StatusFlag _
           , CreateBy, FollowSEQ, FollowChannel, FollowRemark _
           , PFrom _
           , FromTXT _
           , MTMService _
           , ServiceRemark _
           , ServiceRef _
           , TelepharmacyMethod _
           , RecordMethod _
           , RecordLocation _
           , TelepharmacyRemark)

    End Function
    Public Function A5_Save(
             ByVal RefID As Long _
           , ByVal ServiceUID As Integer _
           , ByVal BYear As Integer _
           , ByVal ServiceTypeID As String _
           , ByVal ServiceDate As Long _
           , ByVal ServiceTime As Double _
           , ByVal LocationID As String _
           , ByVal PatientID As Long _
           , ByVal PersonID As Integer _
           , ByVal PFrom As String _
           , ByVal FromTXT As String _
           , ByVal MTMService As String _
           , ByVal ServiceRemark As String _
           , ByVal ServiceRef As String _
           , ByVal TelepharmacyMethod As String _
           , ByVal RecordMethod As String _
           , ByVal RecordLocation As String _
           , ByVal TelepharmacyRemark As String _
           , ByVal FollowChannel As String _
           , ByVal FollowSEQ As Integer _
           , ByVal FollowDate As String _
           , ByVal NicotineEffect As String _
           , ByVal NicotineRemark As String _
           , ByVal MedEffect As String _
           , ByVal MedRemark As String _
           , ByVal SmokeQTY As Integer _
           , ByVal Nicotin As String _
           , ByVal Often As Integer _
           , ByVal SmokeTime As Integer _
           , ByVal ChangeRate As String _
           , ByVal FinalStopDate As String _
           , ByVal QuitDay As Integer _
           , ByVal PWeight As Double _
           , ByVal PHeigh As Double _
           , ByVal BP As String _
           , ByVal PEFR_Value As Double _
           , ByVal PEFR_Rate As Double _
           , ByVal CO_Value As Double _
           , ByVal StopPlan As String _
           , ByVal StopPlanRemark As String _
           , ByVal NextFollowDate As String _
           , ByVal StatusFlag As Integer _
           , ByVal CreateBy As String) As Integer


        Return SqlHelper.ExecuteNonQuery(ConnectionString, "A5_Save" _
           , RefID _
           , ServiceUID _
           , BYear _
           , ServiceTypeID _
           , ServiceDate _
           , ServiceTime _
           , LocationID _
           , PatientID _
           , PersonID _
           , PFrom _
           , FromTXT _
           , MTMService _
           , ServiceRemark _
           , ServiceRef _
           , TelepharmacyMethod _
           , RecordMethod _
           , RecordLocation _
           , TelepharmacyRemark _
           , FollowChannel _
           , FollowSEQ _
           , FollowDate _
           , NicotineEffect _
           , NicotineRemark _
           , MedEffect _
           , MedRemark _
           , SmokeQTY _
           , Nicotin _
           , Often _
           , SmokeTime _
           , ChangeRate _
           , FinalStopDate _
           , QuitDay _
           , PWeight _
           , PHeigh _
           , BP _
           , PEFR_Value _
           , PEFR_Rate _
           , CO_Value _
           , StopPlan _
           , StopPlanRemark _
           , NextFollowDate _
           , StatusFlag _
           , CreateBy)

    End Function
    Public Function CPA_MedOrder_Add(SEQ As Integer, itemID As Integer, itemName As String, Frequency As String, QTY As Integer, MUser As String, ServiceTypeID As String, LocationID As String, StatusFlag As String, patientid As Long, byear As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "CPA_MedOrder_Save", SEQ, itemID, itemName, Frequency, QTY, MUser, ServiceTypeID, LocationID, StatusFlag, patientid, byear)
    End Function

   

    Public Function CPA_MedOrder_DeleteByStatus(TypeID As String, LocationID As String, PatientID As Integer, StatusFlag As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedNameCPA("MedOrder_DeleteByStatus"), TypeID, LocationID, PatientID, StatusFlag)
    End Function

    Public Function Smoking_Get4ReceivedByCloseDate(ByVal ProvID As String, ByVal LID As String, Bdate As Long, Edate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Smoking_Get4Received", ProvID, LID, Bdate, Edate)
        Return ds.Tables(0)
    End Function
    Public Function Smoking_UpdateStatusReceived(ByVal provID As String, ByVal LID As String, ByVal Bdate As Long, ByVal Edate As Long, ByVal Pdate As Long, ByVal pInv As String, ByVal UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Smoking_UpdateStatusReceived", provID, LID, Bdate, Edate, Pdate, pInv, UpdBy)
    End Function

    Public Function Smoking_Get4ReceivedByStopDayRange(ByVal ProvID As String, Bdate As Long, Edate As Long, RDay As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Smoking_StopDayRange", ProvID, Bdate, Edate, RDay)
        Return ds.Tables(0)
    End Function

    Public Function Smoking_PaymentDetail_Add(ByVal provID As String, ByVal Bdate As Long, ByVal Edate As Long, ByVal RDay As Integer, ByVal Pdate As Long, ByVal pInv As String, ByVal UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Smoking_PaymentDetail_Add", provID, Bdate, Edate, RDay, Pdate, pInv, UpdBy)
    End Function

#End Region
End Class
