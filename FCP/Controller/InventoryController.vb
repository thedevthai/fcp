﻿Imports Microsoft.ApplicationBlocks.Data
Public Class StockController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Public Function StockMain_GetSearch(pMedname As String, pLocation As String, pProv As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "VMI_Stock_Main_GetSearch", pMedname, pLocation, pProv)
        Return ds.Tables(0)
    End Function

    Public Function Stock_GetAll() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "VMI_Smoking_Stock_GetAll")
        Return ds.Tables(0)

    End Function
    Public Function Stock_GetSearch(pMedname As String, pLocation As String, pProv As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "VMI_Smoking_Stock_GetSearch", pMedname, pLocation, pProv)
        Return ds.Tables(0)
    End Function
    Public Function Stock_GetByLocation(pLocation As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "VMI_Smoking_Stock_GetByLocation", pLocation)
        Return ds.Tables(0)
    End Function

    Public Function Stock_GetBalance(pMedUID As String, pLocation As String) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "VMI_Stock_GetOnHand", pMedUID, pLocation)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function
    Public Function Stock_GetUOM(pMedUID As String, pLocation As String) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, "VMI_Smoking_Stock_GetUOM", pMedUID, pLocation)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Str(ds.Tables(0).Rows(0)(0))
        Else
            Return ""
        End If
    End Function

    Public Function Stock_Get4Replenishment(pMedname As String, pLocation As String, pProv As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "VMI_Stock_Get4Replenishment", pMedname, pLocation, pProv)
        Return ds.Tables(0)
    End Function

    Public Function Stock_UpdateROPByMedUID(LocationID As String, MedUID As Integer, ROP As Integer, MUser As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Stock_UpdateROPByMedUID", LocationID, MedUID, ROP, MUser)
    End Function
    Public Function Stock_Disable(PUID As Integer, MUser As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "VMI_Stock_Disable", PUID, MUser)
    End Function
    Public Function Stock_GetByUID(PUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("VMI_Stock_GetByUID"), PUID)
        Return ds.Tables(0)
    End Function

    Public Function Stock_GetByMedUID(MedUID As Integer, LocationID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("VMI_Stock_GetByMedUID"), MedUID, LocationID)
        Return ds.Tables(0)
    End Function

    Public Function Stock_Save(LocationID As String, MedUID As Integer, UOM As String, Stock As Integer, ROP As Integer, OnHand As Integer, CUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "VMI_Stock_Save" _
                                         , LocationID _
                                         , MedUID _
                                         , UOM _
                                         , Stock _
                                         , ROP _
                                         , OnHand _
                                         , CUser)
    End Function

    Public Function Stock_Inventory_ISS(itemID As Integer, QTY As Integer, MUser As String, ServiceTypeID As String, LocationID As String, StatusFlag As String, uom As String, balance As Integer, distrbType As String, ReferenceUID As Integer, Invdate As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "VMI_Inventory_ISS", itemID, QTY, MUser, ServiceTypeID, LocationID, StatusFlag, uom, balance, distrbType, ReferenceUID, Invdate)
    End Function
    Public Function Stock_Inventory_Delete(itemID As Integer, QTY As Integer, ServiceTypeID As String, LocationID As String, distrbType As String, ReferenceUID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "VMI_Stock_Inventory_Delete", itemID, QTY, ServiceTypeID, LocationID, distrbType, ReferenceUID)
    End Function
End Class


Public Class StoreController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet
    Public Function Store_GetAll() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "VMI_Store_GetAll")
        Return ds.Tables(0)

    End Function
    Public Function Store_GetByCode(pID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "VMI_Store_GetByCode", pID)
        Return ds.Tables(0)
    End Function
    Public Function Store_Save(pCode As String, pName As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "VMI_Store_Save", pCode, pName)
    End Function

    Public Function Store_Delete(pID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "VMI_Store_Delete", pID)
    End Function

    Public Function LocationAssociation_Save(pStore As String, pLocation As String, pUser As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "VMI_LocationAssociation_Save", pStore, pLocation, pUser)
    End Function
    Public Function LocationAssociation_GetSearch(pStore As String, pLocation As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "VMI_LocationAssociation_GetSearch", pStore, pLocation)
        Return ds.Tables(0)
    End Function

    Public Function LocationAssociation_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "VMI_LocationAssociation_GetByUID", pUID)
        Return ds.Tables(0)
    End Function
    Public Function LocationAssociation_Delete(pID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "VMI_LocationAssociation_Delete", pID)
    End Function

End Class
Public Class DistributionTypeController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet
    Public Function DistributionType_GetAll() As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "VMI_DistributionType_GetAll")
        Return ds.Tables(0)

    End Function
    Public Function DistributionType_GetByDomainUID(pID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "VMI_DistributionType_GetByDomainUID", pID)
        Return ds.Tables(0)
    End Function

End Class

Public Class InventoryController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet
    Public Function Store_Inventory_GetLine(pStore As String, pType As String, pMedUID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "VMI_Store_Inventory_GetLine", pStore, pType, pMedUID)
        Return ds.Tables(0)

    End Function
    Public Function Store_Inventory_Add(StoreCode As String, MedUID As Integer, INVDATE As String, DistributionType As String, QTY As Integer, CUser As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "VMI_Store_Inventory_Add", StoreCode, MedUID, INVDATE, DistributionType, QTY, CUser)
    End Function
    Public Function Store_Inventory_Delete(pUID As Integer, CUser As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "VMI_Store_Inventory_Delete", pUID, CUser)
    End Function
    Public Function Inventory_GetTransferByLocation(LocationID As String, StatusFlag As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "VMI_Inventory_GetTransferByLocation", LocationID, StatusFlag)
        Return ds.Tables(0)
    End Function
    Public Function Inventory_GetTransferByStatus(StatusFlag As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "VMI_Inventory_GetTransferByStatus", StatusFlag)
        Return ds.Tables(0)
    End Function

    Public Function Inventory_GetTransferByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "VMI_Inventory_GetTransferByUID", UID)
        Return ds.Tables(0)
    End Function

    Public Function Inventory_Transfer(STUID As Integer, LocationID As String, MedUID As Integer, QTY As Integer, uom As String, DistributionType As String, StatusFlag As String, MUser As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "VMI_Inventory_Transfer", STUID, LocationID, MedUID, QTY, uom, DistributionType, StatusFlag, MUser)
    End Function

    Public Function Inventory_Receive(pUID As Integer, invDate As String, CUser As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "VMI_Inventory_Receive", pUID, invDate, CUser)
    End Function

    Public Function Inventory_Add(itemID As Integer, QTY As Integer, MUser As String, ServiceTypeID As String, LocationID As String, StatusFlag As String, UOM As String, BalanceBefore As Integer, DistributionType As String, ReferenceUID As Integer, InvDate As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "VMI_Inventory_Add", itemID, QTY, MUser, ServiceTypeID, LocationID, StatusFlag, UOM, BalanceBefore, DistributionType, ReferenceUID, InvDate)
    End Function


End Class