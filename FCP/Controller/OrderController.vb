﻿Imports Microsoft.ApplicationBlocks.Data
Public Class OrderController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    Public Function LoadInvoiceNo(projID As Integer) As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, "Payment_GetInvioceNo", projID)
        Return ds.Tables(0)

    End Function

    Public Function LoadInvoiceNo_StopDay() As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, "Payment_GetInvioceNo_StopDay")
        Return ds.Tables(0)

    End Function


    Public Function GetCustomer_ByName(Fname As String, Lname As String) As DataTable
        Dim CName As String
        CName = Trim(Fname) & " " & Trim(Lname)
        SQL = "select * from  Service_Order  where CustName='" & CName & "' Order by ServiceTypeID"
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function


    'Public Function Order_ChkDupCustomer(pType As String, FName As String, LName As String) As Boolean
    '    Dim CName As String
    '    CName = Trim(FName) & " " & Trim(LName)

    '    'SQL = "select * from  Service_Order  where ServiceTypeID='" & pType & "' And  CustName='" & CName & "'"
    '    'ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, ("Order_ChkDupCustomer"), pType, CName)

    '    If DBNull2Zero(ds.Tables(0).Rows(0)(0)) > 0 Then
    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function

    Public Function Order_ChkDupCustomer(pType As String, PID As Integer, pYear As Integer) As Boolean

        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Order_ChkDupCustomerByPatientID"), pType, PID, pYear)

        If DBNull2Zero(ds.Tables(0).Rows(0)(0)) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function RPT_Order4Recieved(Bdate As Long, Edate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order4Recieved"), Bdate, Edate)
        Return ds.Tables(0)
    End Function
    Public Function LocationOrder_ByYear(Year As Integer, sType As String, LocationID As String, PID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "LocationOrder_ByYear", Year, sType, LocationID, PID)
        Return ds.Tables(0)

    End Function

    Public Function GetOrder_ByID(id As Integer) As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, "Order_GetByID", id)
        Return ds.Tables(0)

    End Function



    Public Function MetabolicRisk_ByOrderID(id As Integer) As DataTable
        SQL = "select * from  Metabolic_Risk  where ServiceOrderID=" & id
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function

    Public Function Order_GetTypeIDByItemID(id As Integer, pid As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Order_GetTypeIDByItemID", id, pid)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Str(ds.Tables(0).Rows(0)(0))
        Else
            Return ""
        End If
    End Function

    Public Function Smoking_Get4Edit(pLID As String, pType As String, pKey As String, pProv As String, Optional pYear As Integer = 0) As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, "Smoking_Get4Edit", pLID, pType, pKey, pProv, pYear)
        Return ds.Tables(0)
    End Function


    Public Function ServiceOrder_GetByStatus(pLID As String, pType As String, pKey As String, pStatus As Integer, pProv As String, Optional pYear As Integer = 0) As DataTable

        ds = SqlHelper.ExecuteDataset(ConnectionString, "ServiceOrder_GetByStatus", pLID, pType, pKey, pStatus, pProv, pYear)
        Return ds.Tables(0)
    End Function


    Public Function ServiceOrder_GetByProvinceGroup(pGRPID As String, pType As String, pKey As String, pStatus As Integer, pProv As String, Optional pYear As Integer = 0) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Order_GetByProvinceGroup", pGRPID, pType, pKey, pStatus, pProv, pYear)


        Return ds.Tables(0)
    End Function
    Public Function ServiceOrder_GetByProjectID(pProjID As Integer, pType As String, pKey As String, pStatus As Integer, pProv As String, Optional pYear As Integer = 0) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Order_GetByProjectID", pProjID, pType, pKey, pStatus, pProv, pYear)
        Return ds.Tables(0)
    End Function

    Public Function Order_Get4ReceivedByCloseDate(ByVal ProvID As String, ByVal LID As String, ByVal Bdate As String, ByVal Edate As String, pStatus As Integer) As DataTable

        SQL = "select * from  View_Service_Order   where  status =" & pStatus

        If ProvID <> "0" Then
            SQL &= "  And LocationProvince='" & ProvID & "'"
        End If

        If LID <> "0" Then
            SQL &= "  And ServiceTypeID ='" & LID & "'"
        End If

        If Bdate <> "" Then
            SQL &= "  And (CloseDate  between '" & Bdate & "' and '" & Edate & "')"
        End If

        ' CloseDate between '2013-12-01 00:00:00' and '2014-03-09 00:00:00' order by CloseDate

        SQL &= " order by LocationName ,ServiceDate,CustName  "

        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function

    Public Function Order_Get4ReceivedComplete(ByVal ProvID As String, ByVal LID As String, ByVal Pdate As Long, pStatus As Integer) As DataTable

        SQL = "select * from  View_Service_Order   where  status =" & pStatus

        If ProvID <> "0" Then
            SQL &= "  And LocationProvince='" & ProvID & "'"
        End If

        If LID <> "0" Then
            SQL &= "  And ServiceTypeID ='" & LID & "'"
        End If

        If Pdate <> 0 Then
            SQL &= "  And PayDate =" & Pdate
        End If
        SQL &= " order by CreateDate Desc ,CustName "

        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function


    Public Function F02_GetID(LocationID As String, SType As String, PatientID As Long, ServiceDate As Long, seq As Integer) As Integer

        ds = SqlHelper.ExecuteDataset(ConnectionString, "ServiceOrder_F02GetID", LocationID, SType, PatientID, ServiceDate, seq)

        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If

    End Function
    Public Function ServiceOrder_GetPatientIDByItemID(pID As Long, ProjID As Integer) As Long
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("ServiceOrder_GetPatientIDByItemID"), pID, ProjID)
        Try
            If ds.Tables(0).Rows.Count > 0 Then
                Return DBNull2Lng(ds.Tables(0).Rows(0)(0))
            Else
                Return 0
            End If
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Public Function ServiceOrder_GetID(LocationID As String, SType As String, patientID As Long, ServiceDate As String) As Integer

        ds = SqlHelper.ExecuteDataset(ConnectionString, ("ServiceOrder_GetID"), LocationID, SType, patientID, ServiceDate)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If

    End Function
    Public Function Service_GetPatinetProfile(SID As String, PID As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Service_GetPatinetProfile"), SID, PID)
        Return ds.Tables(0)
    End Function

    Public Function Service_GetPatinetProfileNotYearNow(SID As String, PID As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Service_GetPatinetProfileNotYearNow"), SID, PID)
        Return ds.Tables(0)
    End Function

    Public Function Service_GetBYear(TypeID As String, ServiceID As Long) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Service_GetBYear"), TypeID, ServiceID)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If

    End Function


    'Public Function GetRowsBlank() As DataTable
    '    SQL = "select '' RowBlank"
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
    '    Return ds.Tables(0)
    'End Function
    Public Function F04_GetVaccine(pID As Integer) As DataTable
        SQL = "select * from  Vaccine_item Where ServiceOrderID =" & pID & "  Order by itemID "
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function


    Public Function F04_DeleteVaccine(SID As Integer, SType As String) As Integer
        SQL = "Delete from Vaccine_item Where ServiceOrderID=" & SID & " And ServiceTypeID='" & SType & "'"
        Return SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, SQL)
        'Return SqlHelper.ExecuteNonQuery(ConnectionString, "F04_DeleteVaccine", SID, SType)
    End Function


    Public Function F04_AddVaccine(SID As Integer, SType As String, Vname As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F04_AddVaccine", SID, SType, Vname)
    End Function

    Public Function F01_Add(ByVal LocationID As String _
        , ByVal ServiceDate As Long _
        , ByVal ServiceTime As Integer _
        , ByVal PersonID As Integer _
        , ByVal ServiceTypeID As String _
        , ByVal PatientID As Long _
        , ByVal Status As Integer _
        , ByVal RefID As Integer _
        , ByVal UpdBy As String _
        , ByVal ServicePlan As String _
      , ByVal Hospital As String _
      , ByVal CreateDate As Long _
      , ByVal Gender As String _
          , ByVal Age As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F01_Add" _
          , LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , PatientID _
         , Status _
         , RefID _
         , UpdBy _
         , ServicePlan, Hospital, CreateDate, Gender, Age)

    End Function


    Public Function F01_Update(ByVal itemID As Integer, ByVal LocationID As String _
          , ByVal ServiceDate As Long _
          , ByVal ServiceTime As Integer _
          , ByVal PersonID As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal PatientID As Long _
          , ByVal Status As Integer _
           , ByVal RefID As Integer _
          , ByVal UpdBy As String, ByVal ServicePlan As String _
      , ByVal Hospital As String _
      , ByVal Gender As String _
          , ByVal Age As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F01_Update" _
          , itemID, LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , RefID _
         , UpdBy, ServicePlan, Hospital, PatientID, Gender, Age)


    End Function

    Public Function F01_SaveMetabolicRisk(ByVal ServiceOrderID As Integer _
      , ByVal ServiceTypeID As String _
      , ByVal isDiabetes As Integer _
      , ByVal isBloodPressure As Integer _
      , ByVal isAtheroma As Integer _
      , ByVal isCoronary As Integer _
      , ByVal isParalysis As Integer _
      , ByVal isNone As Integer _
      , ByVal Smoke As Integer _
      , ByVal SmokeYear As Integer _
      , ByVal SmokeCigarette As Integer _
      , ByVal CigaretteType As Integer _
      , ByVal Alcohol As Integer _
      , ByVal AlcoholFQ As Integer _
      , ByVal Exercise As Integer _
      , ByVal Food_Sweet As Integer _
      , ByVal Food_Salty As Integer _
      , ByVal Food_Fat As Integer _
      , ByVal Food_Other As Integer _
      , ByVal Sleep As Integer _
      , ByVal RiskGender As Integer _
      , ByVal RiskAge As Integer _
      , ByVal RiskDiabetes As Integer _
      , ByVal RiskBMI As Integer _
      , ByVal RiskShape As Integer _
      , ByVal RiskBloodAVG As Integer _
      , ByVal SumRiskFat As Integer _
      , ByVal SumRiskBloodPressure As Integer _
      , ByVal SumRiskDiabetes As Integer _
      , ByVal isRiskFat As Integer _
      , ByVal isRiskBloodPressure As Integer _
      , ByVal isRiskDiabetes As Integer _
      , ByVal ScreenAge As Integer _
      , ByVal ScreenShape As Integer _
      , ByVal ScreenWeight As Integer _
      , ByVal ScreenHeight As Integer _
      , ByVal BMI As Double _
      , ByVal BP1 As Double _
      , ByVal BP1_2 As Double _
      , ByVal BP2 As Double _
      , ByVal BP2_2 As Double _
      , ByVal BPAVG As Double _
      , ByVal BPAVG_2 As Double _
      , ByVal HR As Double _
      , ByVal FBS As Double _
      , ByVal Postprandial As Double _
      , ByVal TimeAfter As Double _
      , ByVal TimeNo As Integer _
      , ByVal ServicePlan As String _
      , ByVal Hospital As String _
      , ByVal ReferDocFile As String _
      , ByVal UpdBy As String, PatientID As Long) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F01_SaveMetabolicRisk" _
          , ServiceOrderID _
      , ServiceTypeID _
      , isDiabetes _
      , isBloodPressure _
      , isAtheroma _
      , isCoronary _
      , isParalysis _
      , isNone _
      , Smoke _
      , SmokeYear _
      , SmokeCigarette _
      , CigaretteType _
      , Alcohol _
      , AlcoholFQ _
      , Exercise _
      , Food_Sweet _
      , Food_Salty _
      , Food_Fat _
      , Food_Other _
      , Sleep _
      , RiskGender _
      , RiskAge _
      , RiskDiabetes _
      , RiskBMI _
      , RiskShape _
      , RiskBloodAVG _
      , SumRiskFat _
      , SumRiskBloodPressure _
      , SumRiskDiabetes _
      , isRiskFat _
      , isRiskBloodPressure _
      , isRiskDiabetes _
      , ScreenAge _
      , ScreenShape _
      , ScreenWeight _
      , ScreenHeight _
      , BMI _
      , BP1 _
      , BP1_2 _
      , BP2 _
      , BP2_2 _
      , BPAVG _
      , BPAVG_2 _
      , HR _
      , FBS _
      , Postprandial _
      , TimeAfter _
      , TimeNo _
      , ServicePlan _
      , Hospital _
      , ReferDocFile _
      , UpdBy, PatientID)

    End Function
    Public Function F03_SaveMetabolicRisk(ByVal ServiceOrderID As Integer _
     , ByVal RefOrderID As Integer, ByVal ServiceTypeID As String, ByVal BPAVG As Double, ByVal BPAVG2 As Double _
      , ByVal FBS As Double _
      , ByVal Postprandial As Double _
      , ByVal TimeAfter As Double _
      , ByVal EducateDate As String _
      , ByVal EducateTime As String _
      , ByVal ServiceChannel As String _
      , ByVal Diagnoses As String _
      , ByVal DiagnosesOther As String _
      , ByVal isMedicince As String _
      , ByVal MedicinceDesc As String _
      , ByVal DocFile1 As String _
      , ByVal DocFile2 As String _
      , ByVal DocFile3 As String _
      , ByVal DocFile4 As String _
      , ByVal DocFile5 As String _
      , ByVal TimeNo As Integer _
      , ByVal UpdBy As String _
      , ByVal Diseases As String, isRefer As Integer, RemarkRefer As String)

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F03_SaveMetabolicRisk" _
       , ServiceOrderID _
       , RefOrderID _
      , ServiceTypeID _
      , BPAVG, BPAVG2 _
      , FBS _
      , Postprandial _
      , TimeAfter _
      , EducateDate _
      , EducateTime _
      , ServiceChannel _
      , Diagnoses _
      , DiagnosesOther _
      , isMedicince _
      , MedicinceDesc _
      , DocFile1 _
      , DocFile2 _
      , DocFile3 _
       , DocFile4 _
      , DocFile5 _
      , TimeNo _
     , UpdBy, Diseases, isRefer, RemarkRefer)
    End Function

    Public Function F02_Add(ByVal LocationID As String _
          , ByVal ServiceDate As Long _
          , ByVal ServiceTime As Integer _
          , ByVal PersonID As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal RefID As Integer _
   , ByVal SeqNo As Integer _
   , ByVal EducateCount As Integer _
   , ByVal isProblem1 As Integer _
   , ByVal isProblem2 As Integer _
   , ByVal isProblem3 As Integer _
   , ByVal isProblem4 As Integer _
   , ByVal isProblem5 As Integer _
   , ByVal isProblem6 As Integer _
   , ByVal isProblemOther As Integer _
   , ByVal ProblemRemark As String _
   , ByVal isEducate1 As Integer _
   , ByVal isEducate2 As Integer _
   , ByVal isEducate3 As Integer _
   , ByVal isEducate4 As Integer _
   , ByVal isEducate5 As Integer _
   , ByVal isEducate6 As Integer _
   , ByVal isEducateOther As Integer _
   , ByVal EducateRemark As String _
   , ByVal Status As Integer _
   , ByVal UpdBy As String _
   , ByVal CreateDate As Long _
   , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer _
   , ByVal PatientID As Long _
   , ByVal Gender As String _
   , ByVal Ages As Integer _
   , ByVal isResponse As String _
   , ByVal CauseRsp As Integer _
   , ByVal OtherCause As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F02_Add" _
          , LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , RefID, SeqNo, EducateCount, isProblem1, isProblem2, isProblem3, isProblem4, isProblem5, isProblem6, isProblemOther, ProblemRemark, isEducate1, isEducate2, isEducate3, isEducate4, isEducate5, isEducate6, isEducateOther, EducateRemark, Status, UpdBy, CreateDate _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8 _
         , PatientID _
         , Gender _
         , Ages, isResponse, CauseRsp, OtherCause)

    End Function

    Public Function F02_Update(ByVal itemID As Integer, ByVal LocationID As String _
          , ByVal ServiceDate As Long _
          , ByVal ServiceTime As Integer _
          , ByVal PersonID As Integer _
          , ByVal ServiceTypeID As String _
         , ByVal RefID As Integer _
   , ByVal SeqNo As Integer _
   , ByVal EducateCount As Integer _
   , ByVal isProblem1 As Integer _
   , ByVal isProblem2 As Integer _
   , ByVal isProblem3 As Integer _
   , ByVal isProblem4 As Integer _
   , ByVal isProblem5 As Integer _
   , ByVal isProblem6 As Integer _
   , ByVal isProblemOther As Integer _
   , ByVal ProblemRemark As String _
   , ByVal isEducate1 As Integer _
   , ByVal isEducate2 As Integer _
   , ByVal isEducate3 As Integer _
   , ByVal isEducate4 As Integer _
   , ByVal isEducate5 As Integer _
   , ByVal isEducate6 As Integer _
   , ByVal isEducateOther As Integer _
   , ByVal EducateRemark As String _
   , ByVal Status As Integer _
   , ByVal UpdBy As String _
   , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long, ByVal isResponse As String _
   , ByVal CauseRsp As Integer _
   , ByVal OtherCause As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F02_Update" _
          , itemID, LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , RefID _
   , SeqNo _
   , EducateCount _
   , isProblem1 _
   , isProblem2 _
   , isProblem3 _
   , isProblem4 _
   , isProblem5 _
   , isProblem6 _
   , isProblemOther _
   , ProblemRemark _
   , isEducate1 _
   , isEducate2 _
   , isEducate3 _
   , isEducate4 _
   , isEducate5 _
   , isEducate6 _
   , isEducateOther _
   , EducateRemark _
         , Status _
         , UpdBy _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID, isResponse, CauseRsp, OtherCause)

    End Function

    Public Function F01_UpdateEducateCount(ByVal itemID As Integer, ByVal EduCount As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F01_UpdateEducateCount", itemID, EduCount)
    End Function
    Public Function F01_UpdateEducateCountByDelete(ByVal itemID As Integer, ByVal EduCount As Integer, ByVal UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F01_UpdateEducateCountByDelete", itemID, EduCount, UpdBy)
    End Function

    Public Function F01_UpdateCloseStatus(ByVal itemID As Integer, ByVal Status As Integer, ByVal UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F01_UpdateCloseStatus", itemID, Status, UpdBy)
    End Function

    Public Function F04_Add(ByVal LocationID As String _
       , ByVal ServiceDate As Long _
       , ByVal ServiceTime As Integer _
       , ByVal PersonID As Integer _
       , ByVal ServiceTypeID As String _
       , ByVal Status As Integer _
       , ByVal ChildName As String _
       , ByVal ChildBirthDate As String _
        , ByVal ChildAges As Integer _
       , ByVal Vaccine As String _
       , ByVal isProblem1 As Integer _
   , ByVal isProblem2 As Integer _
   , ByVal isProblem3 As Integer _
   , ByVal isProblem4 As Integer _
   , ByVal isProblemOther As Integer _
   , ByVal ProblemRemark As String _
   , ByVal isEducate1 As Integer _
   , ByVal isEducate2 As Integer _
   , ByVal isEducate3 As Integer _
   , ByVal isEducate4 As Integer _
   , ByVal isEducateOther As Integer _
   , ByVal EducateRemark As String _
       , ByVal UpdBy As String, ByVal CreateDate As Long, ChildSex As String _
   , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long _
   , ByVal Gender As String _
   , ByVal Ages As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F04_Add" _
          , LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , ChildName _
         , ChildBirthDate _
         , ChildAges _
         , Vaccine _
         , isProblem1 _
         , isProblem2 _
         , isProblem3 _
         , isProblem4 _
         , isProblemOther _
         , ProblemRemark _
         , isEducate1 _
         , isEducate2 _
         , isEducate3 _
         , isEducate4 _
         , isEducateOther _
         , EducateRemark _
         , UpdBy, CreateDate, ChildSex _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID _
         , Gender _
         , Ages)

    End Function


    Public Function F04_Update(ByVal itemID As Integer, ByVal LocationID As String _
          , ByVal ServiceDate As Long _
          , ByVal ServiceTime As Integer _
          , ByVal PersonID As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal Status As Integer _
          , ByVal ChildName As String _
       , ByVal ChildBirthDate As String _
        , ByVal ChildAges As Integer _
       , ByVal Vaccine As String _
        , ByVal isProblem1 As Integer _
   , ByVal isProblem2 As Integer _
   , ByVal isProblem3 As Integer _
   , ByVal isProblem4 As Integer _
   , ByVal isProblemOther As Integer _
   , ByVal ProblemRemark As String _
   , ByVal isEducate1 As Integer _
   , ByVal isEducate2 As Integer _
   , ByVal isEducate3 As Integer _
   , ByVal isEducate4 As Integer _
   , ByVal isEducateOther As Integer _
   , ByVal EducateRemark As String _
       , ByVal UpdBy As String, ChildSex As String _
   , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F04_Update" _
          , itemID, LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , ChildName _
       , ChildBirthDate _
        , ChildAges _
       , Vaccine _
        , isProblem1 _
         , isProblem2 _
         , isProblem3 _
         , isProblem4 _
         , isProblemOther _
         , ProblemRemark _
         , isEducate1 _
         , isEducate2 _
         , isEducate3 _
         , isEducate4 _
         , isEducateOther _
         , EducateRemark _
         , UpdBy, ChildSex _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID)


    End Function

    Public Function F05_Add(ByVal LocationID As String _
       , ByVal ServiceDate As Long _
       , ByVal ServiceTime As Integer _
       , ByVal PersonID As Integer _
       , ByVal ServiceTypeID As String _
       , ByVal Status As Integer _
    , ByVal isProblem1 As Integer _
   , ByVal isProblem2 As Integer _
   , ByVal isProblem3 As Integer _
   , ByVal isProblem4 As Integer _
     , ByVal isProblem5 As Integer _
   , ByVal isProblem6 As Integer _
   , ByVal isProblemOther As Integer _
   , ByVal ProblemRemark As String _
   , ByVal isEducate1 As Integer _
   , ByVal isEducate2 As Integer _
   , ByVal isEducate3 As Integer _
   , ByVal isEducate4 As Integer _
   , ByVal isEducate5 As Integer _
   , ByVal isEducate6 As Integer _
   , ByVal isEducate7 As Integer _
   , ByVal isEducate8 As Integer _
    , ByVal isEducate9 As Integer _
   , ByVal isEducateOther As Integer _
   , ByVal EducateRemark As String _
     , ByVal ProbMed1 As String _
         , ByVal ProbMed2 As String _
          , ByVal ProbMed3 As String _
           , ByVal EduMed1 As String _
         , ByVal EduMed2 As String _
          , ByVal EduMed3 As String _
        , ByVal ProbPro1 As String _
         , ByVal ProbPro2 As String _
          , ByVal ProbPro3 As String _
           , ByVal EduPro1 As String _
         , ByVal EduPro2 As String _
          , ByVal EduPro3 As String _
       , ByVal UpdBy As String, ByVal CreateDate As Long _
          , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long _
   , ByVal isEducate10 As Integer _
   , ByVal isEducate11 As Integer _
   , ByVal isEducate12 As Integer _
   , ByVal Gender As String _
   , ByVal Ages As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F05_Add" _
          , LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , isProblem1 _
   , isProblem2 _
   , isProblem3 _
   , isProblem4 _
     , isProblem5 _
   , isProblem6 _
   , isProblemOther _
   , ProblemRemark _
   , isEducate1 _
   , isEducate2 _
   , isEducate3 _
   , isEducate4 _
   , isEducate5 _
   , isEducate6 _
   , isEducate7 _
   , isEducate8 _
    , isEducate9 _
   , isEducateOther _
   , EducateRemark _
     , ProbMed1 _
         , ProbMed2 _
          , ProbMed3 _
           , EduMed1 _
         , EduMed2 _
          , EduMed3 _
        , ProbPro1 _
         , ProbPro2 _
          , ProbPro3 _
           , EduPro1 _
         , EduPro2 _
          , EduPro3 _
         , UpdBy, CreateDate _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID _
         , isEducate10 _
   , isEducate11 _
   , isEducate12 _
         , Gender _
         , Ages)

    End Function


    Public Function F05_Update(ByVal itemID As Integer, ByVal LocationID As String _
          , ByVal ServiceDate As Long _
          , ByVal ServiceTime As Integer _
          , ByVal PersonID As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal Status As Integer _
          , ByVal isProblem1 As Integer _
   , ByVal isProblem2 As Integer _
   , ByVal isProblem3 As Integer _
   , ByVal isProblem4 As Integer _
     , ByVal isProblem5 As Integer _
   , ByVal isProblem6 As Integer _
   , ByVal isProblemOther As Integer _
   , ByVal ProblemRemark As String _
   , ByVal isEducate1 As Integer _
   , ByVal isEducate2 As Integer _
   , ByVal isEducate3 As Integer _
   , ByVal isEducate4 As Integer _
   , ByVal isEducate5 As Integer _
   , ByVal isEducate6 As Integer _
   , ByVal isEducate7 As Integer _
   , ByVal isEducate8 As Integer _
    , ByVal isEducate9 As Integer _
   , ByVal isEducateOther As Integer _
   , ByVal EducateRemark As String _
     , ByVal ProbMed1 As String _
         , ByVal ProbMed2 As String _
          , ByVal ProbMed3 As String _
           , ByVal EduMed1 As String _
         , ByVal EduMed2 As String _
          , ByVal EduMed3 As String _
        , ByVal ProbPro1 As String _
         , ByVal ProbPro2 As String _
          , ByVal ProbPro3 As String _
           , ByVal EduPro1 As String _
         , ByVal EduPro2 As String _
          , ByVal EduPro3 As String _
          , ByVal UpdBy As String _
             , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long _
   , ByVal isEducate10 As Integer _
   , ByVal isEducate11 As Integer _
   , ByVal isEducate12 As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F05_Update" _
          , itemID, LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
          , isProblem1 _
   , isProblem2 _
   , isProblem3 _
   , isProblem4 _
     , isProblem5 _
   , isProblem6 _
   , isProblemOther _
   , ProblemRemark _
   , isEducate1 _
   , isEducate2 _
   , isEducate3 _
   , isEducate4 _
   , isEducate5 _
   , isEducate6 _
   , isEducate7 _
   , isEducate8 _
    , isEducate9 _
   , isEducateOther _
   , EducateRemark _
     , ProbMed1 _
         , ProbMed2 _
          , ProbMed3 _
           , EduMed1 _
         , EduMed2 _
          , EduMed3 _
        , ProbPro1 _
         , ProbPro2 _
          , ProbPro3 _
           , EduPro1 _
         , EduPro2 _
          , EduPro3 _
         , UpdBy _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID, isEducate10 _
   , isEducate11 _
   , isEducate12)


    End Function

    Public Function ServiceOrder_Main_Add(ByVal LocationID As String _
         , ByVal ServiceDate As Long _
         , ByVal ServiceTime As Integer _
         , ByVal PersonID As Integer _
         , ByVal ServiceTypeID As String _
         , ByVal Status As Integer _
         , ByVal isEducate As Integer _
          , ByVal RefID As Integer _
         , ByVal UpdBy As String, ByVal CreateDate As Long _
             , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long _
   , ByVal Gender As String _
   , ByVal Ages As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "ServiceOrder_Main_Add" _
          , LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , isEducate _
         , RefID _
         , UpdBy _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID _
         , Gender _
         , Ages)

    End Function


    Public Function ServiceOrder_Main_Update(ByVal itemID As Integer, ByVal LocationID As String _
          , ByVal ServiceDate As Long _
          , ByVal ServiceTime As Integer _
          , ByVal PersonID As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal Status As Integer _
          , ByVal isEducate As Integer _
           , ByVal RefID As Integer _
          , ByVal UpdBy As String _
             , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "ServiceOrder_Main_Update" _
          , itemID, LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , isEducate _
          , RefID _
         , UpdBy _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID)


    End Function
    Public Function F06_Add(ByVal LocationID As String _
       , ByVal ServiceDate As Long _
       , ByVal ServiceTime As Integer _
       , ByVal PersonID As Integer _
       , ByVal ServiceTypeID As String _
       , ByVal Status As Integer _
        , ByVal isProblem1 As Integer _
        , ByVal isProblem2 As Integer _
        , ByVal isProblem3 As Integer _
        , ByVal isProblem4 As Integer _
        , ByVal isProblem5 As Integer _
        , ByVal isProblem6 As Integer _
        , ByVal isProblem7 As Integer _
        , ByVal isProblemOther As Integer _
        , ByVal ProblemRemark As String _
   , ByVal isEducate1 As Integer _
   , ByVal isEducate2 As Integer _
   , ByVal isEducate3 As Integer _
   , ByVal isEducate4 As Integer _
   , ByVal isEducate5 As Integer _
   , ByVal isEducateOther As Integer _
   , ByVal EducateRemark As String _
       , ByVal UpdBy As String, ByVal CreateDate As Long _
          , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long _
   , ByVal Gender As String _
   , ByVal Ages As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F06_Add" _
          , LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , isProblem1 _
        , isProblem2 _
        , isProblem3 _
        , isProblem4 _
        , isProblem5 _
        , isProblem6 _
        , isProblem7 _
        , isProblemOther _
        , ProblemRemark _
   , isEducate1 _
   , isEducate2 _
   , isEducate3 _
   , isEducate4 _
   , isEducate5 _
   , isEducateOther _
   , EducateRemark _
         , UpdBy, CreateDate _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID _
         , Gender _
         , Ages)

    End Function

    Public Function F06_Update(ByVal itemID As Integer, ByVal LocationID As String _
          , ByVal ServiceDate As Long _
          , ByVal ServiceTime As Integer _
          , ByVal PersonID As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal Status As Integer _
          , ByVal isProblem1 As Integer _
        , ByVal isProblem2 As Integer _
        , ByVal isProblem3 As Integer _
        , ByVal isProblem4 As Integer _
        , ByVal isProblem5 As Integer _
        , ByVal isProblem6 As Integer _
        , ByVal isProblem7 As Integer _
        , ByVal isProblemOther As Integer _
        , ByVal ProblemRemark As String _
   , ByVal isEducate1 As Integer _
   , ByVal isEducate2 As Integer _
   , ByVal isEducate3 As Integer _
   , ByVal isEducate4 As Integer _
   , ByVal isEducate5 As Integer _
   , ByVal isEducateOther As Integer _
   , ByVal EducateRemark As String _
          , ByVal UpdBy As String _
             , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F06_Update" _
          , itemID, LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , isProblem1 _
        , isProblem2 _
        , isProblem3 _
        , isProblem4 _
        , isProblem5 _
        , isProblem6 _
        , isProblem7 _
        , isProblemOther _
        , ProblemRemark _
   , isEducate1 _
   , isEducate2 _
   , isEducate3 _
   , isEducate4 _
   , isEducate5 _
   , isEducateOther _
   , EducateRemark _
                  , UpdBy _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID)


    End Function

    Public Function F07_Add(ByVal LocationID As String _
      , ByVal ServiceDate As Long _
      , ByVal ServiceTime As Integer _
      , ByVal PersonID As Integer _
      , ByVal ServiceTypeID As String _
      , ByVal Status As Integer _
       , ByVal isProblem1 As Integer _
       , ByVal isProblem2 As Integer _
       , ByVal isProblem3 As Integer _
       , ByVal isProblem4 As Integer _
       , ByVal isProblem5 As Integer _
       , ByVal isProblem6 As Integer _
        , ByVal isProblem7 As Integer _
       , ByVal isProblemOther As Integer _
       , ByVal ProblemRemark As String _
  , ByVal isEducate1 As Integer _
  , ByVal isEducate2 As Integer _
  , ByVal isEducate3 As Integer _
  , ByVal isEducate4 As Integer _
  , ByVal isEducate5 As Integer _
  , ByVal isEducate6 As Integer _
   , ByVal isEducate7 As Integer _
   , ByVal isEducate8 As Integer _
   , ByVal isEducate9 As Integer _
   , ByVal isEducate10 As Integer _
  , ByVal isEducateOther As Integer _
  , ByVal EducateRemark As String _
        , ByVal Objectives As String _
      , ByVal UpdBy As String, ByVal CreateDate As Long _
         , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long _
   , ByVal Gender As String _
   , ByVal Ages As Integer _
   , ByVal PatientFrom As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F07_Add" _
          , LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
          , isProblem1 _
        , isProblem2 _
        , isProblem3 _
        , isProblem4 _
        , isProblem5 _
        , isProblem6 _
         , isProblem7 _
        , isProblemOther _
        , ProblemRemark _
   , isEducate1 _
   , isEducate2 _
   , isEducate3 _
   , isEducate4 _
   , isEducate5 _
   , isEducate6 _
   , isEducate7 _
   , isEducate8 _
   , isEducate9 _
   , isEducate10 _
   , isEducateOther _
   , EducateRemark _
     , Objectives _
         , UpdBy, CreateDate _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID _
         , Gender _
         , Ages, PatientFrom)

    End Function

    Public Function F07_Update(ByVal itemID As Integer, ByVal LocationID As String _
          , ByVal ServiceDate As Long _
          , ByVal ServiceTime As Integer _
          , ByVal PersonID As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal Status As Integer _
          , ByVal isProblem1 As Integer _
        , ByVal isProblem2 As Integer _
        , ByVal isProblem3 As Integer _
        , ByVal isProblem4 As Integer _
        , ByVal isProblem5 As Integer _
        , ByVal isProblem6 As Integer _
         , ByVal isProblem7 As Integer _
        , ByVal isProblemOther As Integer _
        , ByVal ProblemRemark As String _
   , ByVal isEducate1 As Integer _
   , ByVal isEducate2 As Integer _
   , ByVal isEducate3 As Integer _
   , ByVal isEducate4 As Integer _
   , ByVal isEducate5 As Integer _
   , ByVal isEducate6 As Integer _
   , ByVal isEducate7 As Integer _
   , ByVal isEducate8 As Integer _
   , ByVal isEducate9 As Integer _
   , ByVal isEducate10 As Integer _
   , ByVal isEducateOther As Integer _
   , ByVal EducateRemark As String _
     , ByVal Objectives As String _
          , ByVal UpdBy As String _
             , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long _
   , ByVal PatientFrom As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F07_Update" _
          , itemID, LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , isProblem1 _
        , isProblem2 _
        , isProblem3 _
        , isProblem4 _
        , isProblem5 _
        , isProblem6 _
         , isProblem7 _
        , isProblemOther _
        , ProblemRemark _
   , isEducate1 _
   , isEducate2 _
   , isEducate3 _
   , isEducate4 _
   , isEducate5 _
   , isEducate6 _
   , isEducate7 _
   , isEducate8 _
   , isEducate9 _
   , isEducate10 _
   , isEducateOther _
   , EducateRemark _
     , Objectives _
                  , UpdBy _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID, PatientFrom)


    End Function

    Public Function F08_Add(ByVal LocationID As String _
      , ByVal ServiceDate As Long _
      , ByVal ServiceTime As Integer _
      , ByVal PersonID As Integer _
      , ByVal ServiceTypeID As String _
      , ByVal Status As Integer _
       , ByVal isProblem1 As Integer _
       , ByVal isProblem2 As Integer _
       , ByVal isProblem3 As Integer _
       , ByVal isProblem4 As Integer _
       , ByVal isProblem5 As Integer _
       , ByVal isProblem6 As Integer _
        , ByVal isProblem7 As Integer _
       , ByVal isProblemOther As Integer _
       , ByVal ProblemRemark As String _
  , ByVal isEducate1 As Integer _
  , ByVal isEducate2 As Integer _
  , ByVal isEducate3 As Integer _
  , ByVal isEducate4 As Integer _
  , ByVal isEducate5 As Integer _
  , ByVal isEducate6 As Integer _
  , ByVal isEducateOther As Integer _
  , ByVal EducateRemark As String _
        , ByVal Objectives As String _
      , ByVal UpdBy As String, ByVal CreateDate As Long _
         , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long _
   , ByVal Gender As String _
   , ByVal Ages As Integer _
   , ByVal PatientFrom As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F08_Add" _
          , LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
          , isProblem1 _
        , isProblem2 _
        , isProblem3 _
        , isProblem4 _
        , isProblem5 _
        , isProblem6 _
         , isProblem7 _
        , isProblemOther _
        , ProblemRemark _
   , isEducate1 _
   , isEducate2 _
   , isEducate3 _
   , isEducate4 _
   , isEducate5 _
   , isEducate6 _
   , isEducateOther _
   , EducateRemark _
     , Objectives _
         , UpdBy, CreateDate _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID _
         , Gender _
         , Ages, PatientFrom)

    End Function

    Public Function F08_Update(ByVal itemID As Integer, ByVal LocationID As String _
          , ByVal ServiceDate As Long _
          , ByVal ServiceTime As Integer _
          , ByVal PersonID As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal Status As Integer _
          , ByVal isProblem1 As Integer _
        , ByVal isProblem2 As Integer _
        , ByVal isProblem3 As Integer _
        , ByVal isProblem4 As Integer _
        , ByVal isProblem5 As Integer _
        , ByVal isProblem6 As Integer _
         , ByVal isProblem7 As Integer _
        , ByVal isProblemOther As Integer _
        , ByVal ProblemRemark As String _
   , ByVal isEducate1 As Integer _
   , ByVal isEducate2 As Integer _
   , ByVal isEducate3 As Integer _
   , ByVal isEducate4 As Integer _
   , ByVal isEducate5 As Integer _
   , ByVal isEducate6 As Integer _
   , ByVal isEducateOther As Integer _
   , ByVal EducateRemark As String _
     , ByVal Objectives As String _
          , ByVal UpdBy As String _
             , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long _
   , ByVal PatientFrom As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F08_Update" _
          , itemID, LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , isProblem1 _
        , isProblem2 _
        , isProblem3 _
        , isProblem4 _
        , isProblem5 _
        , isProblem6 _
         , isProblem7 _
        , isProblemOther _
        , ProblemRemark _
   , isEducate1 _
   , isEducate2 _
   , isEducate3 _
   , isEducate4 _
   , isEducate5 _
   , isEducate6 _
   , isEducateOther _
   , EducateRemark _
     , Objectives _
                  , UpdBy _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID, PatientFrom)


    End Function

    Public Function F09_Add(ByVal LocationID As String _
      , ByVal ServiceDate As Long _
      , ByVal ServiceTime As Integer _
      , ByVal PersonID As Integer _
      , ByVal ServiceTypeID As String _
      , ByVal Status As Integer _
       , ByVal isProblem1 As Integer _
       , ByVal isProblem2 As Integer _
       , ByVal isProblem3 As Integer _
       , ByVal isProblem4 As Integer _
       , ByVal isProblemOther As Integer _
       , ByVal ProblemRemark As String _
  , ByVal isEducate1 As Integer _
  , ByVal isEducate2 As Integer _
  , ByVal isEducate3 As Integer _
  , ByVal isEducateOther As Integer _
  , ByVal EducateRemark As String _
      , ByVal UpdBy As String, ByVal CreateDate As Long _
         , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long _
   , ByVal Gender As String _
   , ByVal Ages As Integer _
   , ByVal PatientFrom As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F09_Add" _
          , LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
          , isProblem1 _
        , isProblem2 _
        , isProblem3 _
        , isProblem4 _
        , isProblemOther _
        , ProblemRemark _
   , isEducate1 _
   , isEducate2 _
   , isEducate3 _
   , isEducateOther _
   , EducateRemark _
         , UpdBy, CreateDate _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID _
         , Gender _
         , Ages, PatientFrom)

    End Function

    Public Function F09_Update(ByVal itemID As Integer, ByVal LocationID As String _
          , ByVal ServiceDate As Long _
          , ByVal ServiceTime As Integer _
          , ByVal PersonID As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal Status As Integer _
          , ByVal isProblem1 As Integer _
        , ByVal isProblem2 As Integer _
        , ByVal isProblem3 As Integer _
        , ByVal isProblem4 As Integer _
        , ByVal isProblemOther As Integer _
        , ByVal ProblemRemark As String _
   , ByVal isEducate1 As Integer _
   , ByVal isEducate2 As Integer _
   , ByVal isEducate3 As Integer _
   , ByVal isEducateOther As Integer _
   , ByVal EducateRemark As String _
          , ByVal UpdBy As String _
             , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long _
   , ByVal PatientFrom As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F09_Update" _
          , itemID, LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , isProblem1 _
        , isProblem2 _
        , isProblem3 _
        , isProblem4 _
        , isProblemOther _
        , ProblemRemark _
   , isEducate1 _
   , isEducate2 _
   , isEducate3 _
   , isEducateOther _
   , EducateRemark _
                  , UpdBy _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID, PatientFrom)


    End Function

    Public Function F10_Add(ByVal LocationID As String _
       , ByVal ServiceDate As Long _
       , ByVal ServiceTime As Integer _
       , ByVal PersonID As Integer _
       , ByVal ServiceTypeID As String _
       , ByVal Status As Integer _
    , ByVal isProblem1 As Integer _
   , ByVal isProblem2 As Integer _
   , ByVal isProblem3 As Integer _
   , ByVal isProblem4 As Integer _
     , ByVal isProblem5 As Integer _
   , ByVal isProblem6 As Integer _
, ByVal isProblem7 As Integer _
   , ByVal isProblemOther As Integer _
   , ByVal ProblemRemark As String _
   , ByVal isEducate1 As Integer _
   , ByVal isEducate2 As Integer _
   , ByVal isEducate3 As Integer _
   , ByVal isEducate4 As Integer _
   , ByVal isEducate5 As Integer _
   , ByVal isEducate6 As Integer _
   , ByVal isEducate7 As Integer _
   , ByVal isEducate8 As Integer _
    , ByVal isEducate9 As Integer _
   , ByVal isEducateOther As Integer _
   , ByVal EducateRemark As String _
     , ByVal isFollow As Integer _
         , ByVal HospitalName As String _
          , ByVal Remark As String _
           , ByVal vct_Follow1 As String _
         , ByVal vct_Follow2 As String _
          , ByVal vct_FollowDate As String _
       , ByVal UpdBy As String, ByVal CreateDate As Long _
          , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long _
   , ByVal Gender As String _
   , ByVal Ages As Integer _
   , ByVal PatientFrom As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F10_Add" _
          , LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , isProblem1 _
   , isProblem2 _
   , isProblem3 _
   , isProblem4 _
     , isProblem5 _
   , isProblem6 _
, isProblem7 _
   , isProblemOther _
   , ProblemRemark _
   , isEducate1 _
   , isEducate2 _
   , isEducate3 _
   , isEducate4 _
   , isEducate5 _
   , isEducate6 _
   , isEducate7 _
   , isEducate8 _
    , isEducate9 _
   , isEducateOther _
   , EducateRemark _
     , isFollow _
         , HospitalName _
          , Remark _
           , vct_Follow1 _
         , vct_Follow2 _
          , vct_FollowDate _
         , UpdBy, CreateDate _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID _
         , Gender _
         , Ages, PatientFrom)

    End Function


    Public Function F10_Update(ByVal itemID As Integer, ByVal LocationID As String _
          , ByVal ServiceDate As Long _
          , ByVal ServiceTime As Integer _
          , ByVal PersonID As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal Status As Integer _
          , ByVal isProblem1 As Integer _
   , ByVal isProblem2 As Integer _
   , ByVal isProblem3 As Integer _
   , ByVal isProblem4 As Integer _
     , ByVal isProblem5 As Integer _
   , ByVal isProblem6 As Integer _
   , ByVal isProblem7 As Integer _
   , ByVal isProblemOther As Integer _
   , ByVal ProblemRemark As String _
   , ByVal isEducate1 As Integer _
   , ByVal isEducate2 As Integer _
   , ByVal isEducate3 As Integer _
   , ByVal isEducate4 As Integer _
   , ByVal isEducate5 As Integer _
   , ByVal isEducate6 As Integer _
   , ByVal isEducate7 As Integer _
   , ByVal isEducate8 As Integer _
    , ByVal isEducate9 As Integer _
   , ByVal isEducateOther As Integer _
   , ByVal EducateRemark As String _
      , ByVal isFollow As Integer _
         , ByVal HospitalName As String _
          , ByVal Remark As String _
           , ByVal vct_Follow1 As String _
         , ByVal vct_Follow2 As String _
          , ByVal vct_FollowDate As String _
          , ByVal UpdBy As String _
             , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long _
   , ByVal PatientFrom As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F10_Update" _
          , itemID, LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
          , isProblem1 _
   , isProblem2 _
   , isProblem3 _
   , isProblem4 _
     , isProblem5 _
   , isProblem6 _
 , isProblem7 _
   , isProblemOther _
   , ProblemRemark _
   , isEducate1 _
   , isEducate2 _
   , isEducate3 _
   , isEducate4 _
   , isEducate5 _
   , isEducate6 _
   , isEducate7 _
   , isEducate8 _
    , isEducate9 _
   , isEducateOther _
   , EducateRemark _
     , isFollow _
         , HospitalName _
          , Remark _
           , vct_Follow1 _
         , vct_Follow2 _
          , vct_FollowDate _
         , UpdBy _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID, PatientFrom)


    End Function


    Public Function F11_Add(ByVal LocationID As String _
       , ByVal ServiceDate As Long _
       , ByVal ServiceTime As Integer _
       , ByVal PersonID As Integer _
       , ByVal ServiceTypeID As String _
       , ByVal Status As Integer _
        , ByVal isProblem1 As Integer _
   , ByVal isProblem2 As Integer _
   , ByVal isProblem3 As Integer _
   , ByVal isProblem4 As Integer _
     , ByVal isProblem5 As Integer _
   , ByVal isProblem6 As Integer _
   , ByVal isProblem7 As Integer _
   , ByVal isProblem8 As Integer _
    , ByVal isProblem9 As Integer _
      , ByVal isProblem10 As Integer _
   , ByVal isProblemOther As Integer _
   , ByVal ProblemRemark As String _
   , ByVal isEducate1 As Integer _
   , ByVal isEducate2 As Integer _
   , ByVal isEducate3 As Integer _
   , ByVal isEducate4 As Integer _
   , ByVal isEducate5 As Integer _
   , ByVal isEducateOther As Integer _
   , ByVal EducateRemark As String _
       , ByVal isFollow As Integer _
       , ByVal UpdBy As String _
       , ByVal subProblem As String, ByVal CreateDate As Long _
          , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long, PapYeardone As String _
   , ByVal Gender As String _
   , ByVal Ages As Integer _
   , ByVal PatientFrom As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F11_Add" _
          , LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , isProblem1 _
         , isProblem2 _
   , isProblem3 _
   , isProblem4 _
     , isProblem5 _
   , isProblem6 _
 , isProblem7 _
 , isProblem8 _
 , isProblem9 _
 , isProblem10 _
   , isProblemOther _
   , ProblemRemark _
   , isEducate1 _
   , isEducate2 _
   , isEducate3 _
   , isEducate4 _
   , isEducate5 _
   , isEducateOther _
   , EducateRemark _
       , isFollow _
         , UpdBy _
         , subProblem, CreateDate _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID, PapYeardone _
         , Gender _
         , Ages, PatientFrom)

    End Function

    Public Function F11_Update(ByVal itemID As Integer, ByVal LocationID As String _
          , ByVal ServiceDate As Long _
          , ByVal ServiceTime As Integer _
          , ByVal PersonID As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal Status As Integer _
          , ByVal isProblem1 As Integer _
   , ByVal isProblem2 As Integer _
   , ByVal isProblem3 As Integer _
   , ByVal isProblem4 As Integer _
     , ByVal isProblem5 As Integer _
   , ByVal isProblem6 As Integer _
   , ByVal isProblem7 As Integer _
   , ByVal isProblem8 As Integer _
    , ByVal isProblem9 As Integer _
      , ByVal isProblem10 As Integer _
   , ByVal isProblemOther As Integer _
   , ByVal ProblemRemark As String _
   , ByVal isEducate1 As Integer _
   , ByVal isEducate2 As Integer _
   , ByVal isEducate3 As Integer _
   , ByVal isEducate4 As Integer _
   , ByVal isEducate5 As Integer _
   , ByVal isEducateOther As Integer _
   , ByVal EducateRemark As String _
       , ByVal isFollow As Integer _
       , ByVal UpdBy As String _
        , ByVal subProblem As String _
           , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long, PapYearDone As String _
   , ByVal PatientFrom As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F11_Update" _
          , itemID, LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , isProblem1 _
         , isProblem2 _
   , isProblem3 _
   , isProblem4 _
     , isProblem5 _
   , isProblem6 _
 , isProblem7 _
 , isProblem8 _
 , isProblem9 _
 , isProblem10 _
   , isProblemOther _
   , ProblemRemark _
   , isEducate1 _
   , isEducate2 _
   , isEducate3 _
   , isEducate4 _
   , isEducate5 _
   , isEducateOther _
   , EducateRemark _
       , isFollow _
         , UpdBy _
         , subProblem _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID, PapYearDone, PatientFrom)
    End Function

    Public Function F11_UpdateFollowStatus(ByVal itemID As Integer, ByVal isFollow As Integer, ByVal UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F11_UpdateFollowStatus", itemID, isFollow, UpdBy)
    End Function

    Public Function F11_GetRefID(ByVal itemID As Integer) As Integer
        SQL = "select RefID  from  Service_Order  Where itemID =" & itemID
        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If

    End Function

    Public Function F11_Follow_Add(ByVal LocationID As String _
     , ByVal ServiceDate As Long _
     , ByVal ServiceTime As Integer _
     , ByVal PersonID As Integer _
     , ByVal ServiceTypeID As String _
     , ByVal Status As Integer _
     , ByVal isPapSmear As String _
     , ByVal isFollow As Integer _
      , ByVal HospitalName As String _
     , ByVal DateCheck As String _
      , ByVal isNormal As Integer _
     , ByVal Remark As String _
     , ByVal MedicinceDesc As String _
      , ByVal DocFile As String _
     , ByVal UpdBy As String, RefID As Integer, ByVal CreateDate As Long, ByVal Follow_Channel As String, ByVal HType As Integer, _
     ByVal PatientID As Long, isRole As Integer _
      , ByVal Gender As String _
          , ByVal Ages As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F11_Follow_Add" _
          , LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , isPapSmear _
       , isFollow _
        , HospitalName _
       , DateCheck _
        , isNormal _
       , Remark _
        , MedicinceDesc _
      , DocFile _
         , UpdBy _
         , RefID, CreateDate, Follow_Channel, HType _
        , PatientID, isRole _
         , Gender _
         , Ages)

    End Function

    Public Function F11_Follow_Update(ByVal itemID As Integer, ByVal LocationID As String _
        , ByVal ServiceDate As Long _
        , ByVal ServiceTime As Integer _
        , ByVal PersonID As Integer _
        , ByVal ServiceTypeID As String _
        , ByVal Status As Integer _
        , ByVal isPapSmear As String _
     , ByVal isFollow As Integer _
      , ByVal HospitalName As String _
     , ByVal DateCheck As String _
      , ByVal isNormal As Integer _
     , ByVal Remark As String _
     , ByVal MedicinceDesc As String _
      , ByVal DocFile As String _
     , ByVal UpdBy As String, ByVal RefID As Integer, ByVal Follow_Channel As String, ByVal HType As Integer _
   , ByVal PatientID As Long, isRole As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F11_Follow_Update" _
          , itemID, LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , isPapSmear _
       , isFollow _
        , HospitalName _
       , DateCheck _
        , isNormal _
       , Remark _
        , MedicinceDesc _
      , DocFile _
         , UpdBy, RefID, Follow_Channel, HType _
         , PatientID, isRole)


    End Function

    Public Function F12_Add(ByVal LocationID As String _
       , ByVal ServiceDate As Long _
       , ByVal ServiceTime As Integer _
       , ByVal PersonID As Integer _
       , ByVal ServiceTypeID As String _
       , ByVal Status As Integer _
        , ByVal isEducate1 As Integer _
        , ByVal isEducate2 As Integer _
       , ByVal HospitalName As String _
       , ByVal UpdBy As String, ByVal CreateDate As Long _
          , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long _
   , ByVal Gender As String _
   , ByVal Ages As Integer _
   , ByVal PatientFrom As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F12_Add" _
          , LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , isEducate1 _
         , isEducate2 _
         , HospitalName _
         , UpdBy, CreateDate _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID _
         , Gender _
         , Ages, PatientFrom)

    End Function


    Public Function F12_Update(ByVal itemID As Integer, ByVal LocationID As String _
          , ByVal ServiceDate As Long _
          , ByVal ServiceTime As Integer _
          , ByVal PersonID As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal Status As Integer _
            , ByVal isEducate1 As Integer _
            , ByVal isEducate2 As Integer _
          , ByVal HospitalName As String _
       , ByVal UpdBy As String _
          , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long _
   , ByVal PatientFrom As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F12_Update" _
          , itemID, LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
          , isEducate1 _
         , isEducate2 _
         , HospitalName _
         , UpdBy _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID, PatientFrom)

    End Function




    Public Function F16_Add( _
         ByVal LocationID As String _
       , ByVal ServiceDate As Long _
       , ByVal ServiceTime As Integer _
       , ByVal PersonID As Integer _
       , ByVal ServiceTypeID As String _
       , ByVal PatientID As Long _
       , ByVal Ages As Integer _
       , ByVal Allergy As String _
       , ByVal AllergyOther As String _
       , ByVal MedicineDesc As String _
       , ByVal isFollow As Integer _
       , ByVal NextFollowDate As String _
       , ByVal FollowDateSave As String _
       , ByVal isAbNormal As Integer _
       , ByVal Remark As String _
       , ByVal isNHSO1 As Integer _
       , ByVal isNHSO2 As Integer _
       , ByVal isNHSO3 As Integer _
       , ByVal isNHSO4 As Integer _
       , ByVal isNHSO5 As Integer _
       , ByVal isNHSO6 As Integer _
       , ByVal isNHSO7 As Integer _
       , ByVal isNHSO8 As Integer _
       , ByVal Status As Integer _
       , ByVal CreateBy As String _
       , ByVal CreateDate As Long) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F16_Add", _
         LocationID _
       , ServiceDate _
       , ServiceTime _
       , PersonID _
       , ServiceTypeID _
       , PatientID _
       , Ages _
       , Allergy _
       , AllergyOther _
       , MedicineDesc _
       , isFollow _
       , NextFollowDate _
       , FollowDateSave _
       , isAbNormal _
       , Remark _
       , isNHSO1 _
       , isNHSO2 _
       , isNHSO3 _
       , isNHSO4 _
       , isNHSO5 _
       , isNHSO6 _
       , isNHSO7 _
       , isNHSO8 _
       , Status _
       , CreateBy _
       , CreateDate)

    End Function

    Public Function F16_Update(ByVal itemID As Integer _
       , ByVal LocationID As String _
       , ByVal ServiceDate As Long _
       , ByVal ServiceTime As Integer _
       , ByVal PersonID As Integer _
       , ByVal ServiceTypeID As String _
       , ByVal PatientID As Long _
       , ByVal Ages As Integer _
       , ByVal Allergy As String _
       , ByVal AllergyOther As String _
       , ByVal MedicineDesc As String _
       , ByVal isFollow As Integer _
       , ByVal NextFollowDate As String _
       , ByVal FollowDateSave As String _
       , ByVal isAbNormal As Integer _
       , ByVal Remark As String _
       , ByVal isNHSO1 As Integer _
       , ByVal isNHSO2 As Integer _
       , ByVal isNHSO3 As Integer _
       , ByVal isNHSO4 As Integer _
       , ByVal isNHSO5 As Integer _
       , ByVal isNHSO6 As Integer _
       , ByVal isNHSO7 As Integer _
       , ByVal isNHSO8 As Integer _
       , ByVal Status As Integer _
       , ByVal UpdBy As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F16_Update", itemID _
       , LocationID _
       , ServiceDate _
       , ServiceTime _
       , PersonID _
       , ServiceTypeID _
       , PatientID _
       , Ages _
       , Allergy _
       , AllergyOther _
       , MedicineDesc _
       , isFollow _
       , NextFollowDate _
       , FollowDateSave _
       , isAbNormal _
       , Remark _
       , isNHSO1 _
       , isNHSO2 _
       , isNHSO3 _
       , isNHSO4 _
       , isNHSO5 _
       , isNHSO6 _
       , isNHSO7 _
       , isNHSO8 _
       , Status _
       , UpdBy)

    End Function

    Public Function F13_Add(ByVal LocationID As String _
    , ByVal ServiceDate As Long _
    , ByVal ServiceTime As Integer _
    , ByVal PersonID As Integer _
    , ByVal ServiceTypeID As String _
    , ByVal Status As Integer _
    , ByVal Allergy As String _
    , ByVal Disease As String _
    , ByVal MedicineUsage As String _
    , ByVal UsageDay As Integer _
    , ByVal isConsult As Integer _
    , ByVal Med1 As String _
    , ByVal Med2 As String _
    , ByVal Med3 As String _
    , ByVal Med4 As String _
    , ByVal Med5 As String _
    , ByVal isFollow As Integer _
    , ByVal FollowChannel As String _
    , ByVal ChannelTime As String _
    , ByVal FollowDate As String _
    , ByVal DiagenoseResult As String _
    , ByVal isCause As String _
    , ByVal CauseRemark As String _
    , ByVal HospitalType As String _
    , ByVal HospitalName As String _
    , ByVal Remark As String _
    , ByVal AbNormalRemark As String _
    , ByVal ProbPro1 As String _
    , ByVal ProbPro2 As String _
    , ByVal UpdBy As String _
    , ByVal CreateDate As Long _
       , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long _
   , ByVal Gender As String _
   , ByVal Ages As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F13_Add" _
         , LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , Allergy _
         , Disease _
         , MedicineUsage _
         , UsageDay _
         , isConsult _
         , Med1 _
         , Med2 _
         , Med3 _
         , Med4 _
         , Med5 _
         , isFollow _
    , FollowChannel _
    , ChannelTime _
    , FollowDate _
    , DiagenoseResult _
    , isCause _
    , CauseRemark _
    , HospitalType _
    , HospitalName _
    , Remark _
         , AbNormalRemark _
         , ProbPro1 _
         , ProbPro2 _
         , UpdBy _
         , CreateDate _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID _
         , Gender _
         , Ages)

    End Function

    Public Function F13_Update(ByVal itemID As Integer, ByVal LocationID As String _
    , ByVal ServiceDate As Long _
    , ByVal ServiceTime As Integer _
    , ByVal PersonID As Integer _
    , ByVal ServiceTypeID As String _
    , ByVal Status As Integer _
    , ByVal Allergy As String _
    , ByVal Disease As String _
    , ByVal MedicineUsage As String _
    , ByVal UsageDay As Integer _
    , ByVal isConsult As Integer _
    , ByVal Med1 As String _
    , ByVal Med2 As String _
    , ByVal Med3 As String _
    , ByVal Med4 As String _
    , ByVal Med5 As String _
    , ByVal isFollow As Integer _
    , ByVal FollowChannel As String _
    , ByVal ChannelTime As String _
    , ByVal FollowDate As String _
    , ByVal DiagenoseResult As String _
    , ByVal isCause As String _
    , ByVal CauseRemark As String _
    , ByVal HospitalType As String _
    , ByVal HospitalName As String _
    , ByVal Remark As String _
    , ByVal AbNormalRemark As String _
    , ByVal ProbPro1 As String _
    , ByVal ProbPro2 As String _
    , ByVal UpdBy As String _
    , ByVal CreateDate As Long _
       , ByVal isNHSO1 As Integer _
   , ByVal isNHSO2 As Integer _
   , ByVal isNHSO3 As Integer _
   , ByVal isNHSO4 As Integer _
   , ByVal isNHSO5 As Integer _
   , ByVal isNHSO6 As Integer _
   , ByVal isNHSO7 As Integer _
   , ByVal isNHSO8 As Integer, ByVal PatientID As Long) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F13_Update" _
          , itemID _
          , LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , Status _
         , Allergy _
         , Disease _
         , MedicineUsage _
         , UsageDay _
         , isConsult _
         , Med1 _
         , Med2 _
         , Med3 _
         , Med4 _
         , Med5 _
         , isFollow _
    , FollowChannel _
    , ChannelTime _
    , FollowDate _
    , DiagenoseResult _
    , isCause _
    , CauseRemark _
    , HospitalType _
    , HospitalName _
    , Remark _
         , AbNormalRemark _
         , ProbPro1 _
         , ProbPro2 _
         , UpdBy _
         , CreateDate _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8, PatientID)

    End Function

    Public Function F17_Add(ByVal LocationID As String _
       , ByVal ServiceDate As Long _
       , ByVal ServiceTime As Integer _
       , ByVal PersonID As Integer _
       , ByVal ServiceTypeID As String _
       , ByVal PatientID As Long _
       , ByVal isProblem1 As Integer _
       , ByVal isProblem2 As Integer _
       , ByVal isProblem3 As Integer _
       , ByVal isProblem4 As Integer _
       , ByVal isProblem5 As Integer _
       , ByVal isProblem6 As Integer _
       , ByVal isProblem7 As Integer _
       , ByVal isProblem8 As Integer _
       , ByVal AbnormalRemark As String _
       , ByVal isEducate1 As Integer _
       , ByVal isEducate2 As Integer _
       , ByVal isEducate3 As Integer _
       , ByVal isEducate4 As Integer _
       , ByVal isEducate5 As Integer _
       , ByVal isEducateOther As Integer _
       , ByVal EducateRemark As String _
       , ByVal isNHSO1 As Integer _
       , ByVal isNHSO2 As Integer _
       , ByVal isNHSO3 As Integer _
       , ByVal isNHSO4 As Integer _
       , ByVal isNHSO5 As Integer _
       , ByVal isNHSO6 As Integer _
       , ByVal isNHSO7 As Integer _
       , ByVal isNHSO8 As Integer _
       , ByVal Status As Integer _
       , ByVal UpdBy As String _
       , ByVal CreateDate As Long) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F17_Add" _
          , LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , PatientID _
         , isProblem1 _
       , isProblem2 _
       , isProblem3 _
       , isProblem4 _
       , isProblem5 _
       , isProblem6 _
       , isProblem7 _
       , isProblem8 _
       , AbnormalRemark _
       , isEducate1 _
       , isEducate2 _
       , isEducate3 _
       , isEducate4 _
       , isEducate5 _
       , isEducateOther _
       , EducateRemark _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8 _
         , Status _
         , UpdBy _
         , CreateDate)

    End Function
    Public Function F17_Update(pID As Integer, ByVal LocationID As String _
     , ByVal ServiceDate As Long _
     , ByVal ServiceTime As Integer _
     , ByVal PersonID As Integer _
     , ByVal ServiceTypeID As String _
     , ByVal PatientID As Long _
     , ByVal isProblem1 As Integer _
     , ByVal isProblem2 As Integer _
     , ByVal isProblem3 As Integer _
     , ByVal isProblem4 As Integer _
     , ByVal isProblem5 As Integer _
     , ByVal isProblem6 As Integer _
     , ByVal isProblem7 As Integer _
     , ByVal isProblem8 As Integer _
     , ByVal AbnormalRemark As String _
     , ByVal isEducate1 As Integer _
     , ByVal isEducate2 As Integer _
     , ByVal isEducate3 As Integer _
     , ByVal isEducate4 As Integer _
     , ByVal isEducate5 As Integer _
     , ByVal isEducateOther As Integer _
     , ByVal EducateRemark As String _
     , ByVal isNHSO1 As Integer _
     , ByVal isNHSO2 As Integer _
     , ByVal isNHSO3 As Integer _
     , ByVal isNHSO4 As Integer _
     , ByVal isNHSO5 As Integer _
     , ByVal isNHSO6 As Integer _
     , ByVal isNHSO7 As Integer _
     , ByVal isNHSO8 As Integer _
     , ByVal Status As Integer _
     , ByVal UpdBy As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F17_Update" _
          , pID, LocationID _
         , ServiceDate _
         , ServiceTime _
         , PersonID _
         , ServiceTypeID _
         , PatientID _
         , isProblem1 _
       , isProblem2 _
       , isProblem3 _
       , isProblem4 _
       , isProblem5 _
       , isProblem6 _
       , isProblem7 _
       , isProblem8 _
       , AbnormalRemark _
       , isEducate1 _
       , isEducate2 _
       , isEducate3 _
       , isEducate4 _
       , isEducate5 _
       , isEducateOther _
       , EducateRemark _
         , isNHSO1 _
         , isNHSO2 _
         , isNHSO3 _
         , isNHSO4 _
         , isNHSO5 _
         , isNHSO6 _
         , isNHSO7 _
         , isNHSO8 _
         , Status _
         , UpdBy)

    End Function
    Public Function F18_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "F18_GetByUID", UID)
        Return ds.Tables(0)
    End Function

    Public Function F18_Save(ByVal ServiceUID As Long _
            , ByVal LocationID As String _
            , ByVal PatientID As Long _
            , ByVal ServiceDate As Long _
            , ByVal ServiceTime As Integer _
            , ByVal PersonID As Integer _
            , ByVal P1 As String _
            , ByVal P2 As String _
            , ByVal Q1 As Integer _
            , ByVal Q2 As Integer _
            , ByVal Q3 As Integer _
            , ByVal Q4 As Integer _
            , ByVal Q5 As Integer _
            , ByVal Q6 As Integer _
            , ByVal Q7 As Integer _
            , ByVal Q8 As Integer _
            , ByVal Q9 As Integer _
            , ByVal Total9Q As Integer _
            , ByVal ST1 As Integer _
            , ByVal ST2 As Integer _
            , ByVal ST3 As Integer _
            , ByVal ST4 As Integer _
            , ByVal ST5 As Integer _
            , ByVal TotalST5 As Integer _
            , ByVal isEducate1 As String _
            , ByVal isEducate2 As String _
            , ByVal isAgree As String _
            , ByVal CloseStatus As Integer _
            , ByVal CUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F18_Save" _
            , ServiceUID _
            , LocationID _
            , PatientID _
            , ServiceDate _
            , ServiceTime _
            , PersonID _
            , P1 _
            , P2 _
            , Q1 _
            , Q2 _
            , Q3 _
            , Q4 _
            , Q5 _
            , Q6 _
            , Q7 _
            , Q8 _
            , Q9 _
            , Total9Q _
            , ST1 _
            , ST2 _
            , ST3 _
            , ST4 _
            , ST5 _
            , TotalST5 _
            , isEducate1 _
            , isEducate2 _
            , isAgree _
            , CloseStatus _
            , CUser)

    End Function
    Public Function F19_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "F19_GetByUID", UID)
        Return ds.Tables(0)
    End Function

    Public Function F19_Save(ByVal ServiceUID As Long _
            , ByVal LocationID As String _
            , ByVal PatientID As Long _
            , ByVal ServiceDate As Long _
            , ByVal ServiceTime As Integer _
            , ByVal PersonID As Integer _
            , ByVal Q1 As Integer _
            , ByVal Q2A As Integer _
            , ByVal Q2B As Integer _
            , ByVal Q2C As Integer _
            , ByVal Q3 As Integer _
            , ByVal Q4 As Integer _
            , ByVal Q5 As Integer _
            , ByVal Q6 As Integer _
            , ByVal Q7 As Integer _
            , ByVal Q8 As Integer _
            , ByVal Q9 As Integer _
            , ByVal Q10 As Integer _
            , ByVal Total9Q As Integer _
            , ByVal isAgree As String _
            , ByVal CloseStatus As Integer _
            , ByVal CUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "F19_Save" _
            , ServiceUID _
            , LocationID _
            , PatientID _
            , ServiceDate _
            , ServiceTime _
            , PersonID _
            , Q1 _
            , Q2A _
            , Q2B _
            , Q2C _
            , Q3 _
            , Q4 _
            , Q5 _
            , Q6 _
            , Q7 _
            , Q8 _
            , Q9 _
            , Q10 _
            , Total9Q _
            , isAgree _
            , CloseStatus _
            , CUser)

    End Function


    Public Function RelatedValues_Save(ByVal ServiceOrderID As Integer, ByVal SeviceTypeID As String _
        , ByVal ServiceDate As Long _
        , ByVal PEFR As Double _
        , ByVal PEFR_STD As Double _
        , ByVal PEFR_RATE As Double _
        , ByVal isFollow As String _
        , ByVal isRisk As String _
        , ByVal Follow_Channel As String _
        , ByVal isAbNormal As Integer _
        , ByVal AbnormalRemark As String _
        , ByVal MedicineRemark As String _
        , ByVal HospitalName As String _
        , ByVal UpdBy As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "RelatedValues_Save" _
          , ServiceOrderID _
      , SeviceTypeID _
      , ServiceDate _
      , PEFR _
      , PEFR_STD _
      , PEFR_RATE _
      , isFollow _
      , isRisk _
      , Follow_Channel _
      , isAbNormal _
      , AbnormalRemark _
      , MedicineRemark _
      , HospitalName _
      , UpdBy)

    End Function


    Public Function Order_UpdateSmall(ByVal StdCode_Old As String, ByVal StdCode_New As String, ByVal FirstName As String, ByVal LastName As String, MajorID As Integer, MinorID As Integer, SubMinorID As Integer, AdvisorID As Integer, gpax As Double, lvClass As Integer, UpdateBy As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Order_UpdateSmall", StdCode_Old, StdCode_New, FirstName, LastName, MajorID, MinorID, SubMinorID, AdvisorID, gpax, lvClass, UpdateBy)

    End Function


    Public Function Order_Delete(ByVal pID As Integer, ServiceTypeID As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Order_Delete", pID, ServiceTypeID)
    End Function

    Public Function Order_F01DeleteByRefID(ByVal pID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Order_F01DeleteByRefID", pID)
    End Function
    Public Function Order_F02DeleteByRefID(ByVal pID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Order_F02DeleteByRefID", pID)
    End Function


    Public Function OrderLevel_UpdateOneLevel() As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "OrderLevel_UpdateOneLevel")
    End Function

    Public Function OrderLevel_UpdateByCode(ByVal pID As String, pLevel As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "OrderLevel_UpdateByCode", pID, pLevel)
    End Function


    Public Function Order_GetByCondition(ByVal pLID As String, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_FinanceByCondition"), pLID, bDate, eDate)
        Return ds.Tables(0)
    End Function

    Public Function Order_GetSummaryByLocation(ByVal pLID As String, ByVal bDate As Long, ByVal eDate As Long, Proj As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetSummaryByLocation"), pLID, bDate, eDate, Proj)
        Return ds.Tables(0)
    End Function
    Public Function Order_GetByCustomer(ByVal PatientID As Long, ByVal pLID As String, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Order_GetByCustomer"), PatientID, pLID, bDate, eDate)
        Return ds.Tables(0)
    End Function

    Public Function Order_GetLocationByCondition(ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Order_LocationByCondition"), bDate, eDate)
        Return ds.Tables(0)
    End Function
    Public Function Order_GetLocationByDate(ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Order_GetLocationByDate"), bDate, eDate)
        Return ds.Tables(0)
    End Function
    Public Function Order_GetLocationByProvinceAndDate(ByVal provID As String, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Order_GetLocationByProvinceAndDate"), provID, bDate, eDate)
        Return ds.Tables(0)
    End Function

    Public Function Order_GetLocationByProvinceGroupAndDate(ByVal provID As String, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("Order_GetLocationByProvinceGroupAndDate"), provID, bDate, eDate)
        Return ds.Tables(0)
    End Function


    Public Function Order_UpdateStatusReceived(ByVal provID As String, ByVal LID As String, ByVal Bdate As String, ByVal Edate As String, ByVal Pdate As Long, ByVal pInv As String, ByVal UpdBy As String) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "Order_UpdateStatusReceived", provID, LID, Bdate, Edate, Pdate, pInv, UpdBy)
    End Function

    Public Function RPT_Order_GetCustomerF01(ByVal pLID As String, ByVal pType As String, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetCustomerF01"), pLID, pType, bDate, eDate)
        Return ds.Tables(0)
    End Function

    Public Function RPT_Order_GetCustomerByActivity(ByVal pLID As String, ByVal pType As String, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetCustomerByActivity"), pLID, pType, bDate, eDate)
        Return ds.Tables(0)
    End Function

    Public Function RPT_Smoking_GetCustomerByActivity(ByVal pLID As String, ByVal pType As String, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Smoking_GetCustomerByActivity"), pLID, pType, bDate, eDate)
        Return ds.Tables(0)
    End Function
    Public Function RPT_Order_GetCustomerF01ByProvince(ByVal provID As String, ByVal pLID As String, ByVal pType As String, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetCustomerF01ByProvince"), provID, pLID, pType, bDate, eDate)
        Return ds.Tables(0)
    End Function

    Public Function RPT_Order_GetCustomerF01ByProvinceGroup(ByVal provID As String, ByVal pLID As String, ByVal pType As String, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetCustomerF01ByProvinceGroup"), provID, pLID, pType, bDate, eDate)
        Return ds.Tables(0)
    End Function


    Public Function RPT_Order_GetCustomerByActivityAndProvince(ByVal provID As String, ByVal pLID As String, ByVal pType As String, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetCustomerByActivityAndProvince"), provID, pLID, pType, bDate, eDate)
        Return ds.Tables(0)
    End Function

    Public Function RPT_Order_GetCustomerByActivityAndProvinceGroup(ByVal provID As String, ByVal pLID As String, ByVal pType As String, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetCustomerByActivityAndProvinceGroup"), provID, pLID, pType, bDate, eDate)
        Return ds.Tables(0)
    End Function
    '-----------------------------------------------

    Public Function RPT_Order_GetF01Province(ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF01Province"), bDate, eDate)
        Return ds.Tables(0)
    End Function
    Public Function RPT_Order_GetF01Count(ByVal provID As String, ByVal bDate As Long, ByVal eDate As Long) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF01Count"), provID, bDate, eDate)
        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function RPT_Order_GetF01DiabeteRiskCount(ByVal provID As String, RiskLevel As Integer, ByVal bDate As Long, ByVal eDate As Long) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF01RiskCount"), provID, RiskLevel, bDate, eDate)
        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function
    'Public Function RPT_Order_GetF03DiagnoseCount(ByVal provID As String, ByVal bDate As Long, ByVal eDate As Long) As Integer
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF03DiagnoseCount"), provID, bDate, eDate)
    '    Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    'End Function
    Public Function RPT_Order_GetF03AbnormalCount(ByVal provID As String, ByVal bDate As Long, ByVal eDate As Long) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF03AbnormalCount"), provID, bDate, eDate)
        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function
    Public Function RPT_Order_GetF03DiseaseAbnormalCount(ByVal provID As String, Dcount As Integer, ByVal bDate As Long, ByVal eDate As Long) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF03DiseaseAbnormalCount"), provID, Dcount, bDate, eDate)
        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function RPT_Order_GetF03ReplyCount(ByVal provID As String, ByVal bDate As Long, ByVal eDate As Long) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF03ReplyCount"), provID, bDate, eDate)
        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function RPT_Order_GetF01CustomerByProvince(ByVal provID As String, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF01CustomerByProvince"), provID, bDate, eDate)
        Return ds.Tables(0)
    End Function
    Public Function RPT_Order_GetF01CustomerByProvinceGroup(ByVal provID As String, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF01CustomerByProvinceGroup"), provID, bDate, eDate)
        Return ds.Tables(0)
    End Function


    Public Function RPT_GetCustomerByProvince(ByVal provID As String, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_GetCustomerByProvince"), provID, bDate, eDate)
        Return ds.Tables(0)
    End Function
    Public Function RPT_GetCustomerByProvinceGroup(ByVal provID As String, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_GetCustomerByProvinceGroup"), provID, bDate, eDate)
        Return ds.Tables(0)
    End Function


    Public Function RPT_Order_GetF03ByRefID(ByVal refID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF03ByRefID"), refID)
        Return ds.Tables(0)
    End Function

    Public Function RPT_Order_GetF01FatCustomerByProvince(ByVal provID As String, AG1 As Integer, AG2 As Integer, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF01FatCustomerByProvince"), provID, AG1, AG2, bDate, eDate)
        Return ds.Tables(0)
    End Function
    Public Function RPT_Order_GetF01FatCustomerByProvinceGroup(ByVal provID As String, AG1 As Integer, AG2 As Integer, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF01FatCustomerByProvinceGroup"), provID, AG1, AG2, bDate, eDate)
        Return ds.Tables(0)
    End Function


    Public Function RPT_Order_GetF01FatRiskCount(ByVal provID As String, RiskLevel As Integer, ByVal bDate As Long, ByVal eDate As Long) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF01FatRiskCount"), provID, RiskLevel, bDate, eDate)
        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function RPT_Order_GetF01FatRiskGenderCount(ByVal provID As String, Gender As String, ByVal bDate As Long, ByVal eDate As Long) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF01FatRiskGenderCount"), provID, Gender, bDate, eDate)
        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function RPT_Order_GetF01FatRiskAgeCount(ByVal provID As String, BAge As Integer, EAge As Integer, ByVal bDate As Long, ByVal eDate As Long) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF01FatRiskAgeCount"), provID, BAge, EAge, bDate, eDate)
        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function RPT_GetF01ByCustomer(ByVal pName As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_GetF01ByCustomer"), pName)
        Return ds.Tables(0)
    End Function
    Public Function RPT_Order_GetF01BloodRiskCount(ByVal provID As String, RiskLevel As Integer, ByVal bDate As Long, ByVal eDate As Long) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF01BloodRiskCount"), provID, RiskLevel, bDate, eDate)
        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function RPT_Order_GetF01BloodRiskGenderCount(ByVal provID As String, Gender As String, ByVal bDate As Long, ByVal eDate As Long) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF01BloodRiskGenderCount"), provID, Gender, bDate, eDate)
        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function RPT_Order_GetF01BloodRiskAgeCount(ByVal provID As String, BAge As Integer, EAge As Integer, ByVal bDate As Long, ByVal eDate As Long) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF01BloodRiskAgeCount"), provID, BAge, EAge, bDate, eDate)
        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function


    Public Function RPT_Order_GetF01BloodRiskBPCount(ByVal provID As String, BAge As Integer, EAge As Integer, ByVal bDate As Long, ByVal eDate As Long) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF01BloodRiskBPCount"), provID, BAge, EAge, bDate, eDate)
        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function RPT_Order_GetF03BloodRiskDiagnoseCount(ByVal provID As String, YesOrNo As String, ByVal bDate As Long, ByVal eDate As Long) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF03BloodRiskDiagnoseCount"), provID, YesOrNo, bDate, eDate)
        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function RPT_Order_GetF01ReferCount(ByVal provID As String, ByVal bDate As Long, ByVal eDate As Long, Diseases As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF01ReferCount"), provID, bDate, eDate, Diseases)
        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function


    Public Function RPT_Order_GetF01BloodCustomerByProvince(ByVal provID As String, ByVal BP1 As Integer, ByVal BP2 As Integer, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF01BloodCustomerByProvince"), provID, BP1, BP2, bDate, eDate)
        Return ds.Tables(0)
    End Function
    Public Function RPT_Order_GetF01BloodCustomerByProvinceGroup(ByVal provID As String, ByVal BP1 As Integer, ByVal BP2 As Integer, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF01BloodCustomerByProvinceGroup"), provID, BP1, BP2, bDate, eDate)
        Return ds.Tables(0)
    End Function


    Public Function Order_F03GetDocument(pLID As String, pKey As String, pProv As String) As DataTable

        SQL = "select * from   View_Order_Metabolic_HasDocument    where ServiceTypeID='F03'"

        If pLID <> "" Then
            SQL &= " And LocationID='" & pLID & "'"
        End If

        If pProv <> "0" And pProv <> "ALL" Then
            SQL &= "  And LocationProvince ='" & pProv & "'"
        End If

        SQL &= "  And ( LocationName like '%" & pKey & "%'  OR CustName like '%" & pKey & "%' OR CardID like '%" & pKey & "%' ) order by itemID Desc "

        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function

    Public Function Order_F11GetDocument(pLID As String, pKey As String, pProv As String) As DataTable

        SQL = "select * from   View_Service_Order    where  MedicinceDesc<>'' And ServiceTypeID ='F11F' "

        If pLID <> "" Then
            SQL &= " And LocationID='" & pLID & "'"
        End If

        If pProv <> "0" And pProv <> "ALL" Then
            SQL &= "  And LocationProvince ='" & pProv & "'"
        End If

        SQL &= "  And ( LocationName like '%" & pKey & "%'  OR CustName like '%" & pKey & "%' OR CardID like '%" & pKey & "%' ) order by itemID Desc "

        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function

    Public Function Order_GetF03DoucumentByProvinceGroup(pGRPID As String, pKey As String, pProv As String) As DataTable

        SQL = "select * from  View_Order_Metabolic_HasDocument    where ServiceTypeID='F03'"
        SQL &= " And ProvinceGroupID='" & pGRPID & "'"

        If pGRPID <> pProv Then
            If pProv <> "0" Then
                SQL &= "  And LocationProvince ='" & pProv & "'"
            End If
        End If
        SQL &= "  And ( LocationName like '%" & pKey & "%'  OR CustName like '%" & pKey & "%' OR CardID like '%" & pKey & "%' ) order by itemID Desc "

        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function


    Public Function Order_GetF11DoucumentByProvinceGroup(pGRPID As String, pKey As String, pProv As String) As DataTable

        SQL = "select * from  View_Service_Order   where  MedicinceDesc<>'' And ServiceTypeID ='F11F' "
        SQL &= " And ProvinceGroupID='" & pGRPID & "'"

        If pGRPID <> pProv Then
            If pProv <> "0" Then
                SQL &= "  And LocationProvince ='" & pProv & "'"
            End If
        End If
        SQL &= "  And ( LocationName like '%" & pKey & "%'  OR CustName like '%" & pKey & "%' OR CardID like '%" & pKey & "%' ) order by itemID Desc "

        ds = SqlHelper.ExecuteDataset(ConnectionString, CommandType.Text, SQL)
        Return ds.Tables(0)
    End Function


    Public Function RPT_Order_GetServiceCountByProvince(ByVal provID As String, ByVal bDate As Long, ByVal eDate As Long) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetServiceCountByProvince"), provID, bDate, eDate)
        Return DBNull2Zero(ds.Tables(0).Rows.Count)
    End Function

    Public Function RPT_Order_GetCustomerByProvince(ByVal provID As String, ByVal bDate As Long, ByVal eDate As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetCustomerByProvince"), provID, bDate, eDate)
        Return ds.Tables(0)
    End Function

    Public Function RPT_Order_GetServiceOfCustomer(ByVal PatientID As Long) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetServiceOfCustomer"), PatientID)
        Return ds.Tables(0)
    End Function


    Public Function RPT_Order_GetF01CountByShopGroup(ByVal provID As String, ShopGroup As String, ByVal bDate As Long, ByVal eDate As Long) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF01CountByShopGroup"), provID, ShopGroup, bDate, eDate)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function

    Public Function RPT_Order_GetF03YesCountByShopGroup(ByVal provID As String, ShopGroup As String, ByVal bDate As Long, ByVal eDate As Long) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF03YesCountByShopGroup"), provID, ShopGroup, bDate, eDate)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function


    Public Function RPT_Order_GetF03DiagnoseCountByDisease(ByVal provID As String, DiseaseFlag As Integer, ByVal bDate As Long, ByVal eDate As Long) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("RPT_Order_GetF03DiagnoseCountByDisease"), provID, DiseaseFlag, bDate, eDate)
        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function


    Public Function ServiceOrder_SaveRelatedValues(ByVal ServiceOrderID As Integer _
           , ByVal ServiceTypeID As String _
           , ByVal Descriptions As String _
           , ByVal ServiceDate As Long _
           , ByVal Weight As Double _
           , ByVal Heigh As Double _
           , ByVal PEFR As Double _
           , ByVal PEFR_STD As Double _
           , ByVal PEFR_RATE As Double _
           , ByVal UpdBy As String _
           , ByVal Topic1 As String _
           , ByVal Topic2 As String _
           , ByVal Topic3 As String _
           , ByVal Topic4 As String _
           , ByVal Topic5 As String _
           , ByVal TotalScore1 As Integer _
           , ByVal Topic6 As String _
           , ByVal Topic7 As String _
           , ByVal Topic8 As String _
           , ByVal Topic9 As String _
           , ByVal Topic10 As String _
           , ByVal TotalScore2 As Integer _
           , ByVal Topic11 As String _
           , ByVal Topic12 As String _
           , ByVal Topic13 As String _
           , ByVal TotalScore3 As Integer _
           , ByVal Status As Integer _
           , ByVal PERF_SUM As String _
           , ByVal COPD_SUM As String _
           , ByVal ASTHMA_SUM As String) As Integer
        ', ByVal isFollow    As String _
        ', ByVal isRisk    As String _
        ', ByVal Follow_Channel    As String _
        ', ByVal isAbnormal    As String _
        ', ByVal AbnormalRemark    As String _
        ', ByVal MedicineRemark    As String _
        ', ByVal HospitalName    As String _

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "ServiceOrder_SaveRelatedValues" _
          , ServiceOrderID _
           , ServiceTypeID _
           , Descriptions _
           , ServiceDate _
           , Weight _
           , Heigh _
           , PEFR _
           , PEFR_STD _
           , PEFR_RATE _
           , UpdBy _
           , Topic1 _
           , Topic2 _
           , Topic3 _
           , Topic4 _
           , Topic5 _
           , TotalScore1 _
           , Topic6 _
           , Topic7 _
           , Topic8 _
           , Topic9 _
           , Topic10 _
           , TotalScore2 _
           , Topic11 _
           , Topic12 _
           , Topic13 _
           , TotalScore3 _
           , Status, PERF_SUM, COPD_SUM, ASTHMA_SUM)

    End Function

#Region "Behavior Relate"
    Public Function BehaviorRelate_Get(PatientID As Integer, ServiceTypeID As String) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "BehaviorRelate_Get", PatientID, ServiceTypeID)
        Return ds.Tables(0)
    End Function
    Public Function BehaviorRelate_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "BehaviorRelate_GetByUID", UID)
        Return ds.Tables(0)
    End Function
    Public Function BehaviorRelate_Save(
            ByVal ServiceUID As Integer _
          , ByVal UID As Integer _
          , ByVal BehaviorUID As Integer _
          , ByVal Other As String _
          , ByVal Intervention As String _
          , ByVal FinalResult As String _
          , ByVal FinalResultOther As String _
          , ByVal ResultBegin As String _
          , ByVal ResultEnd As String _
          , ByVal FatFollow As String _
          , ByVal Remark As String _
          , ByVal isFollow As String _
          , ByVal CreateBy As Integer, ByVal ServiceTypeID As String, ByVal PatientID As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "BehaviorRelate_Save", ServiceUID, UID, BehaviorUID, Other, Intervention, FinalResult, FinalResultOther, ResultBegin, ResultEnd, FatFollow, Remark, isFollow, CreateBy, ServiceTypeID, PatientID)

    End Function
    Public Function BehaviorRelate_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "BehaviorRelate_Delete", UID)
    End Function

#End Region
End Class
