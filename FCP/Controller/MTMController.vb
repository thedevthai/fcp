﻿Imports Microsoft.ApplicationBlocks.Data
Public Class MTMController
    Inherits ApplicationBaseClass
    Dim ds As New DataSet

    '#Region "MTM 2560"

    '    Public Function MTM_Master_GetByUID(pUID As Integer) As DataTable
    '        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetByUID", pUID)
    '        Return ds.Tables(0)
    '    End Function
    '    Public Function MTM_Master_GetLastInfo(PatientID As Integer) As DataTable
    '        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetLastInfo", PatientID)
    '        Return ds.Tables(0)
    '    End Function
    '    Public Function MTM_Master_GetHerbLastInfo(PatientID As Integer) As DataTable
    '        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetHerbLastInfo", PatientID)
    '        Return ds.Tables(0)
    '    End Function

    '    Public Function MTM_Master_GetBYear(UID As Integer) As Integer
    '        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetBYear", UID)
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    '        Else
    '            Return 0
    '        End If
    '    End Function

    '    Public Function MTM_Master_GetByProvinceGroup(pGRPID As String, pType As String, pKey As String, pStatus As Integer, pProv As String, Optional pYear As Integer = 0) As DataTable
    '        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetByProvinceGroup", pGRPID, pType, pKey, pStatus, pProv, pYear)


    '        Return ds.Tables(0)
    '    End Function

    '    'Public Function MTM_Master_GetByProjectID(pProjID As Integer, pType As String, pKey As String, pStatus As Integer, pProv As String, Optional pYear As Integer = 0) As DataTable
    '    '    ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetByProjectID", pProjID, pType, pKey, pStatus, pProv, pYear)
    '    '    Return ds.Tables(0)
    '    'End Function

    '    Public Function MTM_Master_GetTypeIDByItemID(id As Integer, pid As Integer) As String
    '        ds = SqlHelper.ExecuteDataset(ConnectionString, "Smoking_GetTypeIDByItemID", id, pid)
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            Return DBNull2Str(ds.Tables(0).Rows(0)(0))
    '        Else
    '            Return ""
    '        End If
    '    End Function

    '    Public Function MTM_Master_GetPatientIDByItemID(pID As Long) As Long
    '        ds = SqlHelper.ExecuteDataset(ConnectionString, ("MTM_Master_GetPatientID"), pID)
    '        Try
    '            If ds.Tables(0).Rows.Count > 0 Then
    '                Return DBNull2Lng(ds.Tables(0).Rows(0)(0))
    '            Else
    '                Return 0
    '            End If
    '        Catch ex As Exception
    '            Return 0
    '        End Try
    '    End Function

    '    Public Function MTM_Master_GetLastUID(ByVal LocationID As String _
    '           , ByVal PatientID As Integer _
    '           , ByVal ServiceDate As Integer) As Integer
    '        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetLastUID", LocationID, PatientID, ServiceDate)

    '        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    '    End Function

    '    Public Function MTM_Master_GetLastSEQ(ByVal LocationID As String _
    '           , ByVal PatientID As Integer, MTMTYPE As String) As Integer
    '        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetLastSEQByMTMTYPE", LocationID, PatientID, MTMTYPE)
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    '        Else
    '            Return 0
    '        End If

    '    End Function

    '    Public Function MTM_Master_ChkDupCustomer(PatientID As Integer, ServiceDate As Integer) As Boolean
    '        ds = SqlHelper.ExecuteDataset(ConnectionString, ("MTM_Master_ChkDupCustomer"), PatientID, ServiceDate)
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            If ds.Tables(0).Rows(0)(0) > 0 Then
    '                Return True
    '            Else
    '                Return False
    '            End If
    '        Else
    '            Return False
    '        End If
    '    End Function

    '    Public Function MTM_Master_GetByStatus(pLID As String, pType As String, pKey As String, pStatus As Integer, pProv As String, Optional pYear As Integer = 0) As DataTable
    '        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetByStatus", pLID, pType, pKey, pStatus, pProv, pYear)
    '        Return ds.Tables(0)
    '    End Function

    '    Public Function MTM_Master_Delete(itemID As Long) As Integer
    '        SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("MTM_Master_Delete"), itemID)
    '    End Function


    '    Public Function MTM_Master_Add(
    '             ByVal LocationID As String _
    '           , ByVal PatientID As Integer _
    '           , ByVal ServiceDate As Integer _
    '           , ByVal ServiceTime As Integer _
    '           , ByVal PersonID As Integer _
    '           , ByVal Smoke As Integer _
    '           , ByVal SmokeYear As Integer _
    '           , ByVal SmokeCigarette As Integer _
    '           , ByVal CigaretteType As Integer _
    '           , ByVal Alcohol As Integer _
    '           , ByVal AlcoholFQ As Integer _
    '           , ByVal MedicationUsed1 As String _
    '           , ByVal MedicationUsed2 As String _
    '           , ByVal MedicationUsed3 As String _
    '           , ByVal MedicationUsed4 As String _
    '           , ByVal MedicationUsed5 As String _
    '           , ByVal MedicationUsed6 As String _
    '           , ByVal MedicationUsed7 As String _
    '           , ByVal HospitalType As Integer _
    '           , ByVal HospitalName As String _
    '           , ByVal Status As Integer _
    '           , ByVal CreateBy As String, ByVal SEQ As Integer, MTMTYPE As String, PFrom As String, FromTXT As String) As Integer

    '        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_Master_Add", LocationID, PatientID, ServiceDate, ServiceTime, PersonID, Smoke, SmokeYear, SmokeCigarette, CigaretteType, Alcohol, AlcoholFQ, MedicationUsed1, MedicationUsed2, MedicationUsed3, MedicationUsed4, MedicationUsed5, MedicationUsed6, MedicationUsed7, HospitalType, HospitalName, Status, CreateBy, SEQ, MTMTYPE, PFrom, FromTXT)

    '    End Function
    '    Public Function MTM_Master_Update(ByVal UID As Integer,
    '             ByVal LocationID As String _
    '           , ByVal PatientID As Integer _
    '           , ByVal ServiceDate As Integer _
    '           , ByVal ServiceTime As Integer _
    '           , ByVal PersonID As Integer _
    '           , ByVal Smoke As Integer _
    '           , ByVal SmokeYear As Integer _
    '           , ByVal SmokeCigarette As Integer _
    '           , ByVal CigaretteType As Integer _
    '           , ByVal Alcohol As Integer _
    '           , ByVal AlcoholFQ As Integer _
    '           , ByVal MedicationUsed1 As String _
    '           , ByVal MedicationUsed2 As String _
    '           , ByVal MedicationUsed3 As String _
    '           , ByVal MedicationUsed4 As String _
    '           , ByVal MedicationUsed5 As String _
    '           , ByVal MedicationUsed6 As String _
    '           , ByVal MedicationUsed7 As String _
    '           , ByVal HospitalType As Integer _
    '           , ByVal HospitalName As String _
    '           , ByVal Status As Integer _
    '           , ByVal CreateBy As String, MTMTYPE As String, PFrom As String, FromTXT As String) As Integer

    '        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_Master_Update", UID, LocationID, PatientID, ServiceDate, ServiceTime, PersonID, Smoke, SmokeYear, SmokeCigarette, CigaretteType, Alcohol, AlcoholFQ, MedicationUsed1, MedicationUsed2, MedicationUsed3, MedicationUsed4, MedicationUsed5, MedicationUsed6, MedicationUsed7, HospitalType, HospitalName, Status, CreateBy, MTMTYPE, PFrom, FromTXT)

    '    End Function

    '    Public Function MTM_UpdateHerb(ByVal UID As Integer _
    '           , ByVal MedicationUsed1 As String _
    '           , ByVal MedicationUsed2 As String _
    '           , ByVal MedicationUsed3 As String _
    '           , ByVal MedicationUsed4 As String _
    '           , ByVal MedicationUsed5 As String _
    '           , ByVal MedicationUsed6 As String _
    '           , ByVal MedicationUsed7 As String) As Integer

    '        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_Master_UpdateHerb", UID, MedicationUsed1, MedicationUsed2, MedicationUsed3, MedicationUsed4, MedicationUsed5, MedicationUsed6, MedicationUsed7)

    '    End Function



    '    Public Function MTM_UpdatePE(ByVal UID As Integer,
    '             ByVal isPitting As String _
    '           , ByVal isWound As String _
    '           , ByVal isPeripheral As String _
    '           , ByVal Pitting As String _
    '           , ByVal Wound As String _
    '           , ByVal Peripheral As String) As Integer

    '        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_UpdatePE", UID, isPitting, isWound, isPeripheral, Pitting, Wound, Peripheral)

    '    End Function



    '#End Region

    '#Region "Drug"
    '    Public Function MTM_DrugProblem_Get(MTMUID As Integer, PatientID As Integer) As DataTable
    '        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_DrugProblem_Get", MTMUID, PatientID)
    '        Return ds.Tables(0)
    '    End Function

    '    Public Function MTM_DrugProblem_Save(
    '            ByVal MTMUID As Integer _
    '          , ByVal UID As Integer _
    '          , ByVal ServiceTypeID As String _
    '          , ByVal MedUID As Integer _
    '          , ByVal ProblemGroupUID As String _
    '          , ByVal ProblemUID As String _
    '          , ByVal ProblemOther As String _
    '          , ByVal Intervention As String _
    '          , ByVal ResultID As String _
    '          , ByVal ResultOther As String _
    '          , ByVal PatientID As Integer _
    '          , ByVal CUser As String) As Integer

    '        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DrugProblem_Save", MTMUID, UID, ServiceTypeID, MedUID, ProblemGroupUID, ProblemUID, ProblemOther, Intervention, ResultID, ResultOther, PatientID, CUser)

    '    End Function
    '    Public Function MTM_DrugProblem_Delete(ByVal MTMUID As Integer) As Integer

    '        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DrugProblem_Delete", MTMUID)

    '    End Function
    '    Public Function MTM_DrugProblem_GetByUID(pUID As Integer) As DataTable
    '        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_DrugProblem_GetByUID", pUID)
    '        Return ds.Tables(0)
    '    End Function
    '#End Region



#Region "MTM 2563"

    Public Function MTM_GetVisit(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_GetVisit", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function MTM_Desease_Get(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Desease_Get", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function MTM_DrugProblem_Get(PatientID As Integer, UserID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_DrugProblem_Get", PatientID, UserID)
        Return ds.Tables(0)
    End Function
    Public Function MTM_Master_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetByUID", pUID)
        Return ds.Tables(0)
    End Function
    Public Function MTM_Master_GetLastInfo(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetLastInfo", PatientID)
        Return ds.Tables(0)
    End Function
    Public Function MTM_Master_GetHerbLastInfo(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetHerbLastInfo", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function MTM_Master_GetBYear(UID As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetBYear", UID)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If
    End Function

    Public Function MTM_Master_GetByProvinceGroup(pGRPID As String, pType As String, pKey As String, pStatus As Integer, pProv As String, Optional pYear As Integer = 0) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetByProvinceGroup", pGRPID, pType, pKey, pStatus, pProv, pYear)


        Return ds.Tables(0)
    End Function

    'Public Function MTM_Master_GetByProjectID(pProjID As Integer, pType As String, pKey As String, pStatus As Integer, pProv As String, Optional pYear As Integer = 0) As DataTable
    '    ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetByProjectID", pProjID, pType, pKey, pStatus, pProv, pYear)
    '    Return ds.Tables(0)
    'End Function

    Public Function MTM_Master_GetTypeIDByItemID(id As Integer, pid As Integer) As String
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Smoking_GetTypeIDByItemID", id, pid)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Str(ds.Tables(0).Rows(0)(0))
        Else
            Return ""
        End If
    End Function

    Public Function MTM_Master_GetPatientIDByItemID(pID As Long) As Long
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("MTM_Master_GetPatientID"), pID)
        Try
            If ds.Tables(0).Rows.Count > 0 Then
                Return DBNull2Lng(ds.Tables(0).Rows(0)(0))
            Else
                Return 0
            End If
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Public Function MTM_Master_GetLastUID(ByVal LocationID As String _
           , ByVal PatientID As Integer _
           , ByVal ServiceDate As Integer) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetLastUID", LocationID, PatientID, ServiceDate)

        Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
    End Function

    Public Function MTM_Master_GetLastSEQ(ByVal LocationID As String _
           , ByVal PatientID As Integer, MTMTYPE As String) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetLastSEQByMTMTYPE", LocationID, PatientID, MTMTYPE)
        If ds.Tables(0).Rows.Count > 0 Then
            Return DBNull2Zero(ds.Tables(0).Rows(0)(0))
        Else
            Return 0
        End If

    End Function

    Public Function MTM_Master_ChkDupCustomer(PatientID As Integer, ServiceDate As Integer) As Boolean
        ds = SqlHelper.ExecuteDataset(ConnectionString, ("MTM_Master_ChkDupCustomer"), PatientID, ServiceDate)
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)(0) > 0 Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Public Function MTM_Master_GetByStatus(pLID As String, pType As String, pKey As String, pStatus As Integer, pProv As String, Optional pYear As Integer = 0) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Master_GetByStatus", pLID, pType, pKey, pStatus, pProv, pYear)
        Return ds.Tables(0)
    End Function

    Public Function MTM_Master_Delete(itemID As Long) As Integer
        SqlHelper.ExecuteNonQuery(ConnectionString, GetFullyQualifiedName("MTM_Master_Delete"), itemID)
    End Function


    Public Function MTM_Master_Add(
             ByVal LocationID As String _
           , ByVal PatientID As Integer _
           , ByVal ServiceDate As Integer _
           , ByVal ServiceTime As Integer _
           , ByVal PersonID As Integer _
           , ByVal Smoke As Integer _
           , ByVal SmokeYear As Integer _
           , ByVal SmokeCigarette As Integer _
           , ByVal CigaretteType As Integer _
           , ByVal Alcohol As Integer _
           , ByVal AlcoholFQ As Integer _
           , ByVal MedicationUsed1 As String _
           , ByVal MedicationUsed2 As String _
           , ByVal MedicationUsed3 As String _
           , ByVal MedicationUsed4 As String _
           , ByVal MedicationUsed5 As String _
           , ByVal MedicationUsed6 As String _
           , ByVal MedicationUsed7 As String _
           , ByVal HospitalType As Integer _
           , ByVal HospitalName As String _
           , ByVal Status As Integer _
           , ByVal CreateBy As String _
           , ByVal SEQ As Integer _
           , MTMTYPE As String _
           , PFrom As String _
           , FromTXT As String _
           , MTMService As String _
           , ServiceRemark As String _
           , ServiceRef As String _
           , TelepharmacyMethod As String _
           , RecordMethod As String _
           , RecordLocation As String, TelepharmacyRemark As String
        ) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_Master_Add", LocationID, PatientID, ServiceDate, ServiceTime, PersonID, Smoke, SmokeYear, SmokeCigarette, CigaretteType, Alcohol, AlcoholFQ, MedicationUsed1, MedicationUsed2, MedicationUsed3, MedicationUsed4, MedicationUsed5, MedicationUsed6, MedicationUsed7, HospitalType, HospitalName, Status, CreateBy, SEQ, MTMTYPE, PFrom, FromTXT, MTMService, ServiceRemark, ServiceRef, TelepharmacyMethod, RecordMethod, RecordLocation, TelepharmacyRemark)

    End Function
    Public Function MTM_Master_Update(ByVal UID As Integer,
             ByVal LocationID As String _
           , ByVal PatientID As Integer _
           , ByVal ServiceDate As Integer _
           , ByVal ServiceTime As Integer _
           , ByVal PersonID As Integer _
           , ByVal Smoke As Integer _
           , ByVal SmokeYear As Integer _
           , ByVal SmokeCigarette As Integer _
           , ByVal CigaretteType As Integer _
           , ByVal Alcohol As Integer _
           , ByVal AlcoholFQ As Integer _
           , ByVal MedicationUsed1 As String _
           , ByVal MedicationUsed2 As String _
           , ByVal MedicationUsed3 As String _
           , ByVal MedicationUsed4 As String _
           , ByVal MedicationUsed5 As String _
           , ByVal MedicationUsed6 As String _
           , ByVal MedicationUsed7 As String _
           , ByVal HospitalType As Integer _
           , ByVal HospitalName As String _
           , ByVal Status As Integer _
           , ByVal CreateBy As String _
           , MTMTYPE As String _
           , PFrom As String _
           , FromTXT As String _
           , MTMService As String _
           , ServiceRemark As String _
           , ServiceRef As String _
           , TelepharmacyMethod As String _
           , RecordMethod As String _
           , RecordLocation As String, TelepharmacyRemark As String
           ) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_Master_Update", UID, LocationID, PatientID, ServiceDate, ServiceTime, PersonID, Smoke, SmokeYear, SmokeCigarette, CigaretteType, Alcohol, AlcoholFQ, MedicationUsed1, MedicationUsed2, MedicationUsed3, MedicationUsed4, MedicationUsed5, MedicationUsed6, MedicationUsed7, HospitalType, HospitalName, Status, CreateBy, MTMTYPE, PFrom, FromTXT, MTMService, ServiceRemark, ServiceRef, TelepharmacyMethod, RecordMethod, RecordLocation, TelepharmacyRemark)

    End Function

    Public Function MTM_UpdateHerb(ByVal UID As Integer _
           , ByVal MedicationUsed1 As String _
           , ByVal MedicationUsed2 As String _
           , ByVal MedicationUsed3 As String _
           , ByVal MedicationUsed4 As String _
           , ByVal MedicationUsed5 As String _
           , ByVal MedicationUsed6 As String _
           , ByVal MedicationUsed7 As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_Master_UpdateHerb", UID, MedicationUsed1, MedicationUsed2, MedicationUsed3, MedicationUsed4, MedicationUsed5, MedicationUsed6, MedicationUsed7)

    End Function



    Public Function MTM_UpdatePE(ByVal UID As Integer,
             ByVal isPitting As String _
           , ByVal isWound As String _
           , ByVal isPeripheral As String _
           , ByVal Pitting As String _
           , ByVal Wound As String _
           , ByVal Peripheral As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_UpdatePE", UID, isPitting, isWound, isPeripheral, Pitting, Wound, Peripheral)

    End Function



#End Region


#Region "Drug"

    Public Function MTM_DrugProblem_Save(
            ByVal MTMUID As Integer _
          , ByVal UID As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal MedUID As Integer _
          , ByVal ProblemGroupUID As String _
          , ByVal ProblemUID As String _
          , ByVal ProblemOther As String _
          , ByVal Intervention As String _
          , ByVal ResultID As String _
          , ByVal ResultOther As String _
          , ByVal PatientID As Integer _
          , ByVal CUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DrugProblem_Save", MTMUID, UID, ServiceTypeID, MedUID, ProblemGroupUID, ProblemUID, ProblemOther, Intervention, ResultID, ResultOther, PatientID, CUser)

    End Function
    Public Function MTM_DrugProblem_Delete(ByVal MTMUID As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DrugProblem_Delete", MTMUID)

    End Function
    Public Function MTM_DrugProblem_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_DrugProblem_GetByUID", pUID)
        Return ds.Tables(0)
    End Function
#End Region
#Region "Desease Relate"
    Public Function MTM_DeseaseRelate_Get(MTMUID As Integer, PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_DeseaseRelate_Get", MTMUID, PatientID)
        Return ds.Tables(0)
    End Function
    Public Function MTM_DeseaseRelate_Get2(MTMUID As Integer, PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_DeseaseRelate_Get2", MTMUID, PatientID)
        Return ds.Tables(0)
    End Function


    Public Function MTM_DeseaseRelate_Add(
            ByVal MTMUID As Integer, ServiceTypeID As String _
          , ByVal ICDCode As String _
          , ByVal DeseaseOther As String _
          , ByVal PatientID As Integer _
          , ByVal CreateBy As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DeseaseRelate_Add", MTMUID, ServiceTypeID, ICDCode, DeseaseOther, PatientID, CreateBy)

    End Function

    Public Function MTM_DeseaseRelate_Add2(
            ByVal MTMUID As Integer, ServiceTypeID As String _
          , ByVal DeseaseName As String _
          , ByVal DeseaseOther As String _
          , ByVal PatientID As Integer _
          , ByVal CreateBy As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DeseaseRelate_Add2", MTMUID, ServiceTypeID, DeseaseName, DeseaseOther, PatientID, CreateBy)

    End Function


    Public Function MTM_DeseaseRelate_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DeseaseRelate_Delete", UID)
    End Function

#End Region
#Region "Behavior Relate"
    Public Function MTM_BehaviorProblem_Get(UID As Integer, patientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_BehaviorProblem_Get", UID, patientID)
        Return ds.Tables(0)
    End Function
    Public Function MTM_BehaviorProblem_GetByUID(UID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_BehaviorProblem_GetByUID", UID)
        Return ds.Tables(0)
    End Function
    Public Function MTM_BehaviorProblem_Save(
            ByVal MTMUID As Integer _
          , ByVal UID As Integer _
          , ByVal BehaviorUID As Integer _
          , ByVal Other As String _
          , ByVal Intervention As String _
          , ByVal FinalResult As String _
          , ByVal FinalResultOther As String _
          , ByVal ResultBegin As String _
          , ByVal ResultEnd As String _
          , ByVal FatFollow As String _
          , ByVal Remark As String _
          , ByVal isFollow As String _
          , ByVal PatientID As Integer _
          , ByVal CreateBy As Integer, TasteFollow As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_BehaviorProblem_Save", MTMUID, UID, BehaviorUID, Other, Intervention, FinalResult, FinalResultOther, ResultBegin, ResultEnd, FatFollow, Remark, isFollow, PatientID, CreateBy, TasteFollow)

    End Function
    Public Function MTM_BehaviorProblem_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_BehaviorProblem_Delete", UID)
    End Function

#End Region
#Region "Hospital"
    Public Function MTM_Hospital_Get(MTMUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Hospital_Get", MTMUID)
        Return ds.Tables(0)
    End Function
    Public Function MTM_Hospital_GetByPatient(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_Hospital_GetByPatient", PatientID)
        Return ds.Tables(0)
    End Function
    Public Function MTM_Hospital_Add(ByVal MTMUID As Integer, TypeID As String, ByVal Name As String, PatientID As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_Hospital_Add", MTMUID, TypeID, Name, PatientID)

    End Function
    Public Function MTM_Hospital_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_Hospital_Delete", UID)
    End Function

#End Region
#Region "Drug Use"
    Public Function MTM_DrugUse_Get(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_DrugUse_Get", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function MTM_DrugUse_Save(ServiceUID As Integer, SEQ As Integer, itemID As Integer, itemName As String, Frequency As String, QTY As Integer, MUser As String, ServiceTypeID As String, LocationID As String, StatusFlag As String, patientid As Long) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DrugUse_Save", ServiceUID, SEQ, itemID, itemName, Frequency, QTY, MUser, ServiceTypeID, LocationID, StatusFlag, patientid)
    End Function



    Public Function MTM_DrugUse_Delete(ByVal UID As Integer) As Integer
        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DrugUse_Delete", UID)
    End Function
#End Region

#Region "LAB"
    Public Function LabResult1_Get(HUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "LabResult1_Get", HUID)
        Return ds.Tables(0)
    End Function
    Public Function LabResult2_Get(HUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "LabResult2_Get", HUID)
        Return ds.Tables(0)
    End Function

    Public Function LabResult_Save(
            ByVal RefUID As Integer _
          , ByVal LabUID As Integer _
          , ByVal ResultDate As Integer _
          , ByVal PatientID As Integer _
          , ByVal ResultValue As String _
          , ByVal CUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "LabResult_Save", RefUID, LabUID, ResultDate, PatientID, ResultValue, CUser)

    End Function


#End Region

#Region "Drug Remain"
    Public Function MTM_DrugRemain_Get(PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_DrugRemain_Get", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function MTM_DrugRemain_Save(
            ByVal MTMUID As Integer _
          , ByVal UID As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal DrugUID As Integer _
          , ByVal QTY As Double _
          , ByVal UOM As String _
          , ByVal Hospital As String _
          , ByVal ReasonUID As Integer _
          , ByVal ReasonRemark As String _
          , ByVal PatientID As Integer _
          , ByVal CUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DrugRemain_Save", MTMUID, UID, ServiceTypeID, DrugUID, QTY, UOM, Hospital, ReasonUID, ReasonRemark, PatientID, CUser)

    End Function
    Public Function MTM_DrugRemain_Delete(ByVal MTMUID As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DrugRemain_Delete", MTMUID)

    End Function
    Public Function MTM_DrugRemain_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "MTM_DrugRemain_GetByUID", pUID)
        Return ds.Tables(0)
    End Function

#End Region

#Region "Drug Refill"
    Public Function DrugRefill_Get(ByVal PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugRefill_GetByPatient", PatientID)
        Return ds.Tables(0)
    End Function
    Public Function DrugRefill_Get(ByVal MTMUID As Integer, ByVal PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugRefill_GetByMTM", MTMUID, PatientID)
        Return ds.Tables(0)
    End Function


    Public Function DrugRefill_GetByDate(RefillDate As String, PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugRefill_GetByDate", ConvertStrDate2InformDateString(RefillDate), PatientID)
        Return ds.Tables(0)
    End Function


    Public Function MTM_DrugRefill_Save(
            ByVal MTMUID As Integer _
          , ByVal UID As Integer _
          , ByVal ServiceTypeID As String _
          , ByVal DrugUID As Integer _
          , ByVal QTY As Double _
          , ByVal UOM As String _
          , ByVal UsedRemark As String _
          , ByVal PatientID As Integer _
          , ByVal CUser As String, ByVal RefillDate As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_DrugRefill_Save", MTMUID, UID, ServiceTypeID, DrugUID, QTY, UOM, UsedRemark, PatientID, CUser, RefillDate)

    End Function
    Public Function DrugRefillHeader_GetUID(ByVal RefillDate As String, ByVal PatientID As Integer, ByVal LocationID As String) As Integer
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugRefillHeader_GetUID", RefillDate, PatientID, LocationID)
        Return DBNull2Lng(ds.Tables(0).Rows(0)(0))
    End Function


    Public Function DrugRefillHeader_Add(
            ByVal RefillDate As String, ByVal PatientID As Integer, ByVal LocationID As String _
          , ByVal Remark As String _
          , ByVal MTMUID As Integer _
          , ByVal CUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DrugRefillHeader_Add", RefillDate, PatientID, LocationID, Remark, MTMUID, CUser)

    End Function

    Public Function DrugRefillItem_Save(
            ByVal UID As Integer _
          , ByVal RefillHeaderUID As Long _
          , ByVal MedUID As Integer _
          , ByVal QTY As Double _
          , ByVal UOM As String _
          , ByVal UsedRemark As String _
          , ByVal CUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DrugRefillItem_Save", UID, RefillHeaderUID, MedUID, QTY, UOM, UsedRemark, CUser)

    End Function

    Public Function DrugRefill_Delete(ByVal UID As Integer) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "DrugRefill_Delete", UID)

    End Function
    Public Function DrugRefill_GetByUID(pUID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugRefill_GetByUID", pUID)
        Return ds.Tables(0)
    End Function
    Public Function DrugRefill_GetByRefillNo(RNO As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "DrugRefill_GetByRefillNo", RNO)
        Return ds.Tables(0)
    End Function


#End Region

#Region "Refer"
    Public Function Refer_Get(MTMUID As Integer, PatientID As Integer) As DataTable
        ds = SqlHelper.ExecuteDataset(ConnectionString, "Refer_Get", PatientID)
        Return ds.Tables(0)
    End Function

    Public Function Refer_Save(
          ByVal MTMUID As Integer _
        , ByVal UID As Integer _
        , ByVal HospitalUID As Integer _
        , ByVal Reason As String _
        , ByVal PatientID As Integer _
        , ByVal CUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_Refer_Save", MTMUID, UID, HospitalUID, Reason, PatientID, CUser)

    End Function

    Public Function Refer_Delete(
       ByVal MTMUID As Integer _
     , ByVal PatientID As Integer _
     , ByVal MUser As String) As Integer

        Return SqlHelper.ExecuteNonQuery(ConnectionString, "MTM_Refer_Delete", MTMUID, PatientID, MUser)

    End Function

#End Region


End Class
