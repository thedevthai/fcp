﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class A1

    '''<summary>
    '''ucHistoryYear1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ucHistoryYear1 As Global.ucHistoryYear

    '''<summary>
    '''lblID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblID As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblLocationID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLocationID As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblLocationName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLocationName As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblLocationCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLocationCode As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblLocationProvince control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblLocationProvince As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtYear control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtYear As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtServiceDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtServiceDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTime As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlPerson control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPerson As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''RegularExpressionValidator1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator1 As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''optPatientType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optPatientType As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''optPatientFrom control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optPatientFrom As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtFromRemark control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFromRemark As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''optService control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optService As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtServiceRemark control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtServiceRemark As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''optTelepharm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optTelepharm As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtTelepharmacyRemark control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTelepharmacyRemark As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''optTelepharmacyMethod control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optTelepharmacyMethod As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtRecordMethod control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRecordMethod As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtRecordLocation control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtRecordLocation As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''optAlcohol control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optAlcohol As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtSmokeDay control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSmokeDay As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''optSmokeUOM control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optSmokeUOM As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''optSmokeFQ control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optSmokeFQ As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''lblCGFQ control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCGFQ As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtSmokeQTY control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSmokeQTY As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblCGday control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCGday As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''optStopUOM control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optStopUOM As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''lblCgType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCgType As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''optCigarette control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optCigarette As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtNicotinRate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtNicotinRate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtOften control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtOften As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtMinutePerTime control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMinutePerTime As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''optIntimate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optIntimate As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtCloseupPerson control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCloseupPerson As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''optQ1A control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ1A As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtQ1B control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtQ1B As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtQ1C control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtQ1C As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''optQ1D control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ1D As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''chkQ2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkQ2 As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''chkQ2Med control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkQ2Med As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''txtQ2Other control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtQ2Other As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''optQ5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ5 As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''optQ6 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ6 As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''optQ7 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ7 As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''optQ8 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ8 As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''optQ9 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ9 As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''optQ10 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ10 As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''txtFTND control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFTND As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''optQ12 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ12 As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''optQ13 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optQ13 As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''chkQ14 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkQ14 As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''txtQ14 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtQ14 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtWeight control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtWeight As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtHeigh control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtHeigh As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtBP control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBP As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtPEFR_Value control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPEFR_Value As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtPEFR_Rate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtPEFR_Rate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtCO_Value control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCO_Value As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''optStopPlane1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents optStopPlane1 As Global.System.Web.UI.WebControls.RadioButtonList

    '''<summary>
    '''ddlMed control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlMed As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtMedName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMedName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtFrequency control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFrequency As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtMedQTY control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtMedQTY As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''lblBalanceLabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBalanceLabel As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblBalance control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblBalance As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''cmdAddMed control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdAddMed As Global.DevExpress.Web.ASPxButton

    '''<summary>
    '''grdMed control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdMed As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''QuitPlanDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents QuitPlanDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''RegularExpressionValidator2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator2 As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''xDateNextService control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents xDateNextService As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''RegularExpressionValidator4 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents RegularExpressionValidator4 As Global.System.Web.UI.WebControls.RegularExpressionValidator

    '''<summary>
    '''chkStatus control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkStatus As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''cmdSave control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdSave As Global.System.Web.UI.WebControls.Button
End Class
