﻿Imports System.Data
Public Class PatientList
    Inherits System.Web.UI.Page
    Dim ctlL As New PatientController
    Public dtQ As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx")
        End If
        If Not IsPostBack Then
            dtQ = ctlL.PatientQuitline_Get()
        End If
    End Sub
End Class