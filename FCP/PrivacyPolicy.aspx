<%@ Page Title="News" Language="vb"  AutoEventWireup="false"  CodeBehind="PrivacyPolicy.aspx.vb" Inherits=".PrivacyPolicy" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-874" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>�к��ҹ�����šԨ������ԡ�����ҧ������آ�Ҿ��С�ô��š������������Ѫ�ê���� ��ŹԸ����Ѫ���������������͢������Ѫ���Ҿ���ԡ������</title>
  
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">  

  <link rel="stylesheet" type="text/css" href="css/rajchasistyles.css"> 
  <link rel="stylesheet" type="text/css" href="css/uidialog.css">
  <link rel="stylesheet" type="text/css" href="css/pagestyles.css"> 
  <link rel="stylesheet" type="text/css" href="css/button.css"> 

  <link rel="stylesheet" href="components/bootstrap/assets/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="assets/css/AdminLTE.min.css">
  <link rel="stylesheet" href="assets/css/skins/_all-skins.min.css">  

         <link rel="stylesheet" href="css/style-privacy.css">
     
<!-- JS -->
<script type="text/javascript" src="css/jquery-1.6.3.min.js"></script>

</head>

<body class="hold-transition skin-blue sidebar-mini">
<form id="form2" name="form1"  runat="server">

    <asp:ScriptManager ID="ScriptManager2" runat="server"></asp:ScriptManager>
<div class="wrapper">

  <header class="main-header">    
    <a href="#" class="logo"><b>FCP</b>PROJECT </a>
    <nav class="navbar navbar-static-top">    
  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation
      </a>   
        <div class="slk-header" >
            <span  class="header-full">�к��ҹ�����šԨ������ԡ�����ҧ������آ�Ҿ��С�ô��š������������Ѫ�ê���� 
            <span  class="header-mini">��ŹԸ����Ѫ���������������͢������Ѫ���Ҿ���ԡ������
        </div>
    </nav>
  </header>

     
   
    <!-- Main content -->
<section class="contentCPA">
     
          <div class="box box-primary">
            <div class="box-header"> 

              <h1 class="box-title"><b>Privacy Policy</b></h1>
                
            </div>
            <div class="box-body"> 
                                    	<div class="txt-prg">
                                    
                                        �ô��ҹ��º�¤ء��驺Ѻ����������´ �����駹�º�¤�������ǹ���������͹䢡������ԡ�÷������躹���䫵�ͧ���
                                    
                                     <br />   <strong>1 �ء���������</strong>
                                    <div class="txt-indent">
                                        �ء���������ͤ�����Ҵ��� ��˹�ҷ��Ѵ�纤���͡�ѡɳ����͵���кط������ӡѹ �������������������ѧ���������� ���Ѿ����Ͷ�� �����ػ�ó����� ����Դ��ҹ�Թ������ ���ҷ��س��ҹ���䫵������ͻ��Ͷ�� �����ѵ�ػ��ʧ�������������ҹ���䫵��ջ���Է���Ҿ ������Ǻ�������������ǡѺ�ѡɳС����ҹ�͹�Ź�ͧ����� ����Ѻ��º�©�Ѻ��� ����;ٴ�֧�ء��� ����Ҩ��������֧෤�������蹷�����¡ѹ���� ���ͧ��µ�͡������
                                    </div>
                                    <br />  <strong>  2. ����觻����������Ǵ����ء�������ҹ</strong><br />
                                    <div class="txt-indent">
                                        �ء������¡������������ҧ � �ѧ���<br />

                                            <div class="txt-indent -two">
                                                � �ء������ʪѹ �ء�����������зӧҹ㹢�з���Դ��������������ͻ���पѹ���� ��Шж١ź���ѵ��ѵ� �ѹ�շ��Դ��������������ͻ���पѹ���<br />
                                         
                                       � �ء������ �ء�������������������Ҩ��ӡ�õ�駤�Ңͧ�س����ͤس��Ѻ����ҹ���䫵����ͺ�ԡ�âͧ����ա���� ��ШФ�����㹤�����������ѧ�ҡ��ѧ�ҡ���س���ԡ��������ҹ���䫵�ͧ���<br />
                                       �  �ء���ͧ�ؤ�ŷ��˹�� �ء�����������١���ҧ��������䫵���س��Ҫ� ���URL �ͧ���䫵�л�ҡ�����ᶺ�������<br />
                                  
                                        � �ء���ͧ�ؤ�ŷ����� �ء�����������١���ҧ��������䫵���� � �������Ңͧ��ͤ������������Һҧ��ǹ�� �ɳ������ٻ�Ҿ����䫵���س��Ҫ�<br />
                                       
                                    </div>
                                        �͡�ҡ��� �ѧ�ա���觤ء�������Ǵ���� �ѧ���<br />
                                    <div class="txt-indent -two">
                                                � �ء�����������ҧ��� ���¶֧�ء�������繵�ͧ�� �������кص�ǵ� ����������س��ҹ�������ҧ� �����䫵���з���������ԡ����<br />
                                      
                                       � �ء���������ҹ ��������ͨ��ӵ�����͡��ҧ� �ͧ�س (�� ���ͼ���� ����) ��觨Ъ��»�Ѻ��ا���ʺ��ó�����ҹ���䫵���к�ԡ�âͧ������ç�Ѻ������ͧ��âͧ���кؤ��<br />
                                    
                                       �  �ء������͡�������������ͻ���Է���Ҿ ��˹�ҷ���Ǻ�����������Ҥس��ҹ���䫵���к�ԡ�âͧ����������������ҧ�� (�� ˹��ྨ���ٺ��·���ش) ��е�Ǩ�ͺ��Ҥس���Ѻ�������͹��ͼԴ��Ҵ�������
                                        <br />
                                     �  �ء����ɳ� (������������) ����红���������ǡѺ�ٻẺ��÷�ͧ���䫵�ͧ�س �����ʴ��ɳ���������ҷ������Ǣ�ͧ��еç�������ʹ㨢ͧ�س �͡�ҡ��� �ѧ�ӡѴ�ӹǹ����㹡���ʴ������������ɳҺҧ���ҧ �����駪����Ѵ����Է���Ҿ�ͧ�ɳ�������໭��õ�Ҵ �»����������͢����ɳҺؤ�ŷ���������ء�������ҹ�� ���ͨ������䫵���س��ҹ ��Ш��������Ŵѧ��������Ѻ�ؤ������ �� ����ѷ�ɳ�
                                       <br />
                                    </div>
                                    
                                        �͡�ҡ��� ����ѧ��෤��������� �����红����Ţͧ�س �� �����ŷ���������ػ�ó����������������� ����к��ػ�ó���ЫͿ���������� �����ѵ�ػ��ʧ�������¤�֧�ѹ ����
                                     <div class="txt-indent -two">
                                                � �硾ԡ�� �硾ԡ�� (���ͷ�����¡��Һդ͹���;ԡ��) �繡���������� ���Դ�������� (�������¡����) ���ྨ ����� �ͻ �����ɳ� �·�˹�ҷ��֧�����źҧ���ҧ����ǡѺ�ػ�ó�������������ͧ�س ����֧�ѡɳС����ͺ�ͧ�س�Ѻ�ػ�ó��������������� �� ����������ǡѺ�������ػ�ó� �к���Ժѵԡ�� �����������蹢ͧ��������� ���䫵����������� ����㹡���������� ���䫵�����ҧ�ԧ ������� IP �����š���Դ��������ͤ�ԡ�ɳ� ��Т��������� ������¤�֧ ����֧����ͤ�����Ҵ��� (�ء���) ����к��ػ�ó�������ӡѹ �ؤ�ŷ���������ԡ��������㹡�õ�駤�������ҹ�ء������������ҡ���� ��觵�������繼����Թ����������Ңͧ������� 
                                                ��Т�͹حҵ�ҡ��Ңͧ���������红���������ǡѺ�����Ҫ��������<br />
                                        
                                       �  Local Storage/Session Storage Web storage ���Ըա���红�����㹤�������������͹�Ѻ�ء��� �����ŷ���纴��� Local Storage ���繡���红��������ҧ���è����Ҽ�����ź��� ��ǹ Session Storage ���红�������§˹���ʪѹ ��Ш���������ͤس�Դ˹��ྨ���������� ���ᵡ��ҧ�ҡ�ء��� ��� �к�������觢�������ѧ���������ء���駷���ա����ͧ�� �������ö�红��������ҡ���� (�٧�ش 5MB) ����ѧ�Ǵ������л�ʹ��¡��Ҵ���
                                       <br />
                                      �  �ش�Ѳ�ҫͿ������ �ش�Ѳ�ҫͿ������ (���� SDK) ��˹�ҷ������͹�ԡ�šѺ�ء��� ��зӧҹ����㹺�Ժ��ͧ�ͻ��Ͷ�� ����������ö��ԡ�šѺ�ء����� �� SDK �еԴ���������ͻ ��͹حҵ��������о��������红����š����ͺ�ͧ�س�Ѻ�ͻ��Т���������ǡѺ�ػ�ó�������͢��·����ҹ
                                      <br />
                                    �  �Ū�ء��� ��෤����շ���˹�ҷ����º��§�ء��� �������ö�红�������ػ�ó�ͧ�س����� �»����к����红���������������ҧ�ҡ�ء������� �����˵ع�� �֧���ԸըѴ������ź�����ŷ���������͹�ѹ
                                       <br />
                                    </div>
                                    </div>
                                      <br />  <strong>    3. �˵ؼ�����Ըա����ء���</strong>    <br />
                                    <div class="txt-indent">
                                        �����ء��������ѵ�ػ��ʧ����仹��
                                    <div class="txt-indent -two">
                                                �  �������س�����䫵���к�ԡ�������ҧ������<br />
                                      �  ��Ǩ�ͺ�Է�������кص�ǵ��ͧ�س㹰ҹм�������䫵���к�ԡ�� �����ͺ��ԡ�÷�����<br />
                                  
                                       �  �ѹ�֡��õ�駤�����͵��˹觷���駷��س�鹴����䫵��������ԡ�âͧ���<br />
                                       � ������������ҹ���䫵���к�ԡ�� ���ͻ�Ѻ����¹���䫵���к�ԡ�����ç�Ѻ������ͧ��âͧ�س<br />
                                       
                                       �  �����Ҥسʹ����� ���͹��ʹ������ҷ��ç�������ʹ�<br />
                                    </div>
                                    </div>
                                         <div class="txt-indent">   ��Ҩ������ء����红�������ǹ�ؤ�� �� ���� ���ҧ�á��� �����ŷ������㹤ء����Ҩ�ա��������§�Ѻ��������ǹ�ؤ�ŷ��س������Ѻ��Ҽ�ҹ�ҧ��ͧ�ҧ������ԡ������ (�� ��ҹ����ʴ������Դ��繺����䫵�)
                                    
                                        ������ Google Analytics ��� Google �繼������ԡ������Ѻ���䫵�ͧ��� �������������������л�Ѻ��ا�����ҹ��Ե�ѳ�����ҧ������ͧ �� Google Analytics ����੾�Фء������Ǵ�������������ҹ�� �ء���ͧ Google Analytics ���红���������ǡѺ�ѡɳС����ҹ���䫵� �¨����ẺʶԵ������������������Ҿ��Ҽ������������ҹ���䫵�ͧ������ҧ��
                                    
                                        �ô��Һ�������䫵��� �� Google Analytics ����������� �gat._anonymizeIp();� �������Ǻ��������ŷ������ IP �Ẻ����кص�ǵ� (���¶֧�����ʡ����ͻ��Դ������� IP ���) ��Ҩ�ź�������кص�ǵ��ͧ������� IP � Google Analytics ��Ш��觢����ŷ������кص�ǵ���������������������ͧ Google ����Ѱ����ԡ� Google �Ҩ�觢����Ź�����Ѻ�ؤ�ŷ����� 㹡óշ���բ�ͺѧ�Ѻ�ҧ������ ���ͺؤ�ŷ�������ͧ�����żŢ����Ź��㹹���ͧ Google Google �����������§������� IP �ͧ�س�Ѻ���������㴷�� Google ������ �٢���������ǡѺ��͡�˹�������͹䢡����ҹ��й�º�¤�������ǹ��Ǣͧ Google ����&nbsp;<a href="https://support.google.com/analytics/answer/6004245" style="box-sizing: border-box; -webkit-font-smoothing: antialiased; color: rgb(0, 123, 255); text-decoration: none; background-color: transparent;"><span style="box-sizing: border-box; -webkit-font-smoothing: antialiased; font-size: 11pt; font-family: Arial; color: rgb(17, 85, 204); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: underline; text-decoration-skip-ink: none; vertical-align: baseline; white-space: pre-wrap;">https://support.google.com/analytics/answer/6004245</a>,&nbsp;<a href="https://www.google.com/analytics/terms/gb.html" style="box-sizing: border-box; -webkit-font-smoothing: antialiased; color: rgb(0, 123, 255); text-decoration: none; background-color: transparent;"><span style="box-sizing: border-box; -webkit-font-smoothing: antialiased; font-size: 11pt; font-family: Arial; color: rgb(17, 85, 204); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: underline; text-decoration-skip-ink: none; vertical-align: baseline; white-space: pre-wrap;">https://www.google.com/analytics/terms/gb.html</a>&nbsp;����&nbsp;<a href="https://www.google.de/intl/en_uk/policies/" style="box-sizing: border-box; -webkit-font-smoothing: antialiased; color: rgb(0, 123, 255); text-decoration: none; background-color: transparent;"><span style="box-sizing: border-box; -webkit-font-smoothing: antialiased; font-size: 11pt; font-family: Arial; color: rgb(17, 85, 204); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: underline; text-decoration-skip-ink: none; vertical-align: baseline; white-space: pre-wrap;">https://www.google.de/intl/en_uk/policies/</a>&nbsp;(�Դ�ԧ����¹͡�������)
                                    
          <!--                              �٢���������ǡѺ�ء��������������䫵���ҡ���ҧ��ҹ��ҧ </div><br />

                                     <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <p>
                                                            ���ͤء���
                                                    </td>
                                                    <td>
                                                        <p>
                                                            �ѹ�������/�ѹ������ص������������
                                                    </td>
                                                    <td>
                                                        <p>
                                                            ��͸Ժ��
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>
                                                            _ga
                                                    </td>
                                                    <td>
                                                        <p>
                                                            2 ��
                                                    </td>
                                                    <td>
                                                        <p>
                                                            �������¡��ǵ������
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>
                                                            _gid
                                                    </td>
                                                    <td>
                                                        <p>
                                                            24 �������
                                                    </td>
                                                    <td>
                                                        <p>
                                                            �������¡��ǵ������
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>
                                                            _gat
                                                    </td>
                                                    <td>
                                                        <p>
                                                            1 �ҷ�
                                                    </td>
                                                    <td>
                                                        <p>
                                                            �����ͤǺ����ѵ�ҡ�â� ����� Google Analytics ��ҹ Google Tag Manager �ء�������ժ������ _dc_gtm_&lt;property-id&gt;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>
                                                            AMP_TOKEN
                                                    </td>
                                                    <td>
                                                        <p>
                                                            30 �Թҷն֧ 1 ��
                                                    </td>
                                                    <td>
                                                        <p>
                                                            ����繷����֧��������������繵�ҡ��ԡ����������繵�ͧ AMP ��ǹ������� ����������кآ����š��������͡�� (opt-out) �Ӣͷ����Թ������� ���͢�ͼԴ��Ҵ������¡��������������繵�ҡ��ԡ����������繵�ͧ AMP
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>
                                                            _gac_&lt;property-id&gt;
                                                    </td>
                                                    <td>
                                                        <p>
                                                            90 �ѹ
                                                    </td>
                                                    <td>
                                                        <p>
                                                            �红���������ǡѺ��໭����Ѻ����� ��Ҥس�ԧ��ѭ�� Google Analytics �Ѻ�ѭ�� Google Ads �� Conversion �����䫵� Google Ads ����ҹ��Ҥء����� �����س�����͡�����ء���
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>
                                                           
                                                    </td>
                                                    <td>
                                                        <p>
                                                            30 �ѹ�Ѻ������Թ���
                                                    </td>
                                                    <td>
                                                        <p>
                                                            �红���������ǡѺ�������Ѻ��º�¤ء���ͧ����� ��Ңͧ�ء���: ��
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                 -->
                              
                                    <br /> <br />   <strong>  4. �����������������ǡѺ�ء���</strong> <br />
                                      <div class="txt-indent">
                                        ���������ͧ�س����ö�ѹ�֡�ء������ҧ��ǹ���ͷ��������� �¤س���Է���� ����¹�ŧ ��ШѴ��á�õ�駤�Ҥء��� ��駹�� �������÷ӧҹ����ҹ�������ػ�ó�����͹�������� �س����ö��駤����������������ػ�ó���ء�������Ҩ��������Ѻ�ء�������� ����͹������ա�����ҧ�ء��� �������͡���������ء��� 㹡óշ�����͡������͡�ش���� ��������ҹ������੾�кؤ�źҧ���ҧ����� ����Ҩ��������ҧ� �ͧ���䫵�������������Է���Ҿ �������������Ẻᵡ��ҧ�ѹ� �й���ô��Ǩ�ͺ���٪�������� ������͡ ���ͤ�ҡ�˹��ͧ�����������س��ҹ ���ʹ��Ըա������¹��ҡ�˹��ͧ�ء��� �٢����������������ǡѺ��õ�駤�Ҥء���ͧ����������褹������ѹ��ҡ�ԧ���ҹ��ҧ <br />
                                    
                                        -&nbsp;<a href="https://support.microsoft.com/en-us/products/windows" style="box-sizing: border-box; -webkit-font-smoothing: antialiased; color: rgb(0, 123, 255); text-decoration: none; background-color: transparent;"><span style="box-sizing: border-box; -webkit-font-smoothing: antialiased; font-size: 11pt; font-family: Arial; color: rgb(17, 85, 204); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: underline; text-decoration-skip-ink: none; vertical-align: baseline; white-space: pre-wrap;">Internet Explorer</a> <br />
                                    
                                        -&nbsp;<a href="https://support.google.com/chrome/answer/95647" style="box-sizing: border-box; -webkit-font-smoothing: antialiased; color: rgb(0, 123, 255); text-decoration: none; background-color: transparent;"><span style="box-sizing: border-box; -webkit-font-smoothing: antialiased; font-size: 11pt; font-family: Arial; color: rgb(17, 85, 204); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: underline; text-decoration-skip-ink: none; vertical-align: baseline; white-space: pre-wrap;">Chrome</a> <br />
                                    
                                        -&nbsp;<a href="https://support.apple.com/kb/PH21411" style="box-sizing: border-box; -webkit-font-smoothing: antialiased; color: rgb(0, 123, 255); text-decoration: none; background-color: transparent;"><span style="box-sizing: border-box; -webkit-font-smoothing: antialiased; font-size: 11pt; font-family: Arial; color: rgb(17, 85, 204); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: underline; text-decoration-skip-ink: none; vertical-align: baseline; white-space: pre-wrap;">Safari</a> <br />
                                    
                                        -&nbsp;<a href="https://support.mozilla.org/en-US/kb/enable-and-disable-cookies-website-preferences" style="box-sizing: border-box; -webkit-font-smoothing: antialiased; color: rgb(0, 123, 255); text-decoration: none; background-color: transparent;"><span style="box-sizing: border-box; -webkit-font-smoothing: antialiased; font-size: 11pt; font-family: Arial; color: rgb(17, 85, 204); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: underline; text-decoration-skip-ink: none; vertical-align: baseline; white-space: pre-wrap;">FireFox</a> <br />
                                    
                                        -&nbsp;<a href="https://support.apple.com/en-gb/HT201265" style="box-sizing: border-box; -webkit-font-smoothing: antialiased; color: rgb(0, 123, 255); text-decoration: none; background-color: transparent;"><span style="box-sizing: border-box; -webkit-font-smoothing: antialiased; font-size: 11pt; font-family: Arial; color: rgb(17, 85, 204); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: underline; text-decoration-skip-ink: none; vertical-align: baseline; white-space: pre-wrap;">Safari IOS</a> <br /> <br />
                                       </div>   
                                   <br />   <strong>   5. �Ըջ���ʸ���ź�ء���</strong> <br />
                                      <div class="txt-indent">
                                        ���Ըջ���ʸ���/ź�ء�������������ҡ���䫵���仹��&nbsp;<a href="https://www.allaboutcookies.org/" style="box-sizing: border-box; -webkit-font-smoothing: antialiased; color: rgb(0, 123, 255); text-decoration: none; background-color: transparent;"><span style="box-sizing: border-box; -webkit-font-smoothing: antialiased; font-size: 11pt; font-family: Arial; color: rgb(17, 85, 204); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: underline; text-decoration-skip-ink: none; vertical-align: baseline; white-space: pre-wrap;">https://www.allaboutcookies.org/</a>,&nbsp;<a href="https://www.youronlinechoices.eu/" style="box-sizing: border-box; -webkit-font-smoothing: antialiased; color: rgb(0, 123, 255); text-decoration: none; background-color: transparent;"><span style="box-sizing: border-box; -webkit-font-smoothing: antialiased; font-size: 11pt; font-family: Arial; color: rgb(17, 85, 204); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: underline; text-decoration-skip-ink: none; vertical-align: baseline; white-space: pre-wrap;">www.youronlinechoices.eu</a>&nbsp;(����Ѻ������ 
                                        EEA),&nbsp;<a href="https://youradchoices.ca/choices" style="box-sizing: border-box; -webkit-font-smoothing: antialiased; color: rgb(0, 123, 255); text-decoration: none; background-color: transparent;"><span style="box-sizing: border-box; -webkit-font-smoothing: antialiased; font-size: 11pt; font-family: Arial; color: rgb(17, 85, 204); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: underline; text-decoration-skip-ink: none; vertical-align: baseline; white-space: pre-wrap;">https://youradchoices.ca/choices</a>&nbsp;(����Ѻ������᤹Ҵ�),&nbsp;<a href="https://www.aboutads.info/choices/" style="box-sizing: border-box; -webkit-font-smoothing: antialiased; color: rgb(0, 123, 255); text-decoration: none; background-color: transparent;"><span style="box-sizing: border-box; -webkit-font-smoothing: antialiased; font-size: 11pt; font-family: Arial; color: rgb(17, 85, 204); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: underline; text-decoration-skip-ink: none; vertical-align: baseline; white-space: pre-wrap;">https://www.aboutads.info/choices/</a>&nbsp;(����Ѻ���������Ѱ����ԡ�) 
                                        �͡�ҡ��� �س�ѧ��˹������� Google ���Ǻ������������� (�� �ء�����з������ IP) ���´�ǹ���Ŵ��еԴ��駻����Թ���������ҡ���䫵���&nbsp;<a href="https://tools.google.com/dlpage/gaoptout/" style="box-sizing: border-box; -webkit-font-smoothing: antialiased; color: rgb(0, 123, 255); text-decoration: none; background-color: transparent;"><span style="box-sizing: border-box; -webkit-font-smoothing: antialiased; font-size: 11pt; font-family: Arial; color: rgb(17, 85, 204); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: underline; text-decoration-skip-ink: none; vertical-align: baseline; white-space: pre-wrap;">https://tools.google.com/dlpage/gaoptout/</a>&nbsp;(�Դ�ԧ����¹͡�������)
                                    
                                        ���ҷ��س��Ҫ����䫵��������ԡ�âͧ��� ���һԴ�����ҹ�ء��� ���ͧ�ҡ�Ҩ�觼ŵ�ͻ��ʺ��ó�����ҹ�� �� �к��Ш��Ӥ�ҡ�˹� �� ������л���� ���س����������� ��Ш�������Ҥس��������Ѻ���͹���Т�͡�˹���������� �������͡����Ѻ�����ҹ����
                                    
                                        �͡�ҡ��� �����ŷ��ء������͡�������������Ǻ�������Ҩ�١ź��� ��Ҥء���١ź ����Ҩ���ѧ������ŷ��������͹�����ա������¹�ŧ��ҡ�˹� ����Ҩ����Ӥء�����Դ��ҹ��������红��������� �����ҡ�����ҹ�ͧ�س
                                       </div>   
                                   <br />   <br />  <strong>   6. ���Թ���</strong> <br />
                                      <div class="txt-indent">
                                        �����ҹ���䫵��������������¹�ŧ��õ�駤���������������Ҥس�Թ�����������ء������෤��������� ������¤�֧ �����͡�˹�����к����㹹�º�©�Ѻ���
                                       </div>   
                                   <br />   <br />  <strong>   7. �������㹡�û�Ѻ��ا��º�¤ء���</strong> <br />
                                      <div class="txt-indent">
                                        ����Ҩ��䢻�Ѻ��ا��º�©�Ѻ���������� ��������������ʹ���ͧ�Ѻ�������¹�ŧ������������Ǵ����ͧ�ء����������ҹ ���������ѵ�ػ��ʧ���ҹ෤�Ԥ ������ ���͢�ͺѧ�Ѻ �ѧ��� �ô��µ�Ǩ�ͺ�������˹�ҹ�º�� ��������Һ�֧�Ըա����ҹ�ء������෤����շ������Ǣ�ͧ �ѹ����觻�ҡ������ҹ���ͧ��º�¤���ѹ����ѻവ����������ش
                                       </div>   
                                    <br />  <br /> <strong>    8. �Դ������</strong> <br />
                                      <div class="txt-indent">
                                        �ҡ�դӶ�������������ǡѺ�Ըա����ء��� �ô�Դ�����ҷ��     </div>   
                            </div>     
         
            </div>
            <!-- /.chat -->
               <div class="box-footer clearfix no-border">
            </div>
          </div>
         </div>
      
</section>
    <!-- /.content -->
   

     
  <!-- /.content-wrapper -->
  <footer class="main-footerCPA">
       <div class="pull-right hidden-xs">
          <b>Version</b> <asp:Label ID="lblVersion" runat="server" Text=""></asp:Label>
    </div>
       <div class="pull-left hidden-xs">
       </div>

       <strong>&copy; �Ԣ�Է����� ��ŹԸ����Ѫ���������</strong>
    
  </footer>
  
</div>
<!-- ./wrapper --> 



</form>
    
<script src="components/jquery/assets/jquery.min.js"></script>
<script src="components/jquery-ui/jquery-ui.min.js"></script>
<script src="components/bootstrap/assets/js/bootstrap.min.js"></script>
<script src="assets/js/adminlte.min.js"></script>


</body>
</html>
