﻿Imports System.IO
Public Class SiteNoAjax
    Inherits System.Web.UI.MasterPage
    Dim dt As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then

        End If
        hlnkUserName.Text = Request.Cookies("NameOfUser").Value
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            hlnkUserName.NavigateUrl = "LocationsEdit.aspx?id=" & Request.Cookies("LocationID").Value
        Else
            hlnkUserName.NavigateUrl = "#"
        End If

    End Sub

End Class