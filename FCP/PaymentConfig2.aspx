﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="PaymentConfig2.aspx.vb" Inherits=".PaymentConfig2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  
    </asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1> Payment Config
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
  
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title"> Payment Config</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">

    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td align="center" valign="top">&nbsp;</td>
      </tr>
        <tr>
            <td align="center" valign="top">
             
            
<table cellSpacing="1" cellPadding="1" border="0">
                                                <tr>
                                                    <td width="150" align="left" class="texttopic">
                                                        ID : 
                                                        <asp:Label ID="lblID" runat="server"></asp:Label>
                                                        </td>
                                                    <td align="left" >
                                                      <asp:TextBox ID="txtCode" runat="server" Width="50px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="150" align="left" class="texttopic">
                                                        ฟอร์ม :</td>
                                                    <td align="left" >
                                                        <asp:DropDownList ID="ddlServiceType" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="texttopic">Rate ค่าตอบแทน :</td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtRateAmount" runat="server" MaxLength="4" Width="50px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="texttopic">Effective To</td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtEffectiveTo" runat="server"></asp:TextBox> <cc1:calendarextender  Format="dd/MM/yyyy" 
            ID="CalendarExtender1" runat="server"  PopupButtonID="txtEffectiveTo"  
            TargetControlID="txtEffectiveTo">        </cc1:calendarextender>   
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="texttopic">Status</td>
                                                    <td align="left">
                                                        <asp:CheckBox ID="chkActive" runat="server" Checked="True" Text="Active" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center" class="texttopic">
        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="70px" />
        &nbsp;<asp:Button ID="cmdClear" runat="server" Text="ยกเลิก" CssClass="buttonCancle" Width="70px" />
                                                    </td>
                                                </tr>
  </table>        
  
 
  
      </td>
      </tr>
       <tr>
          <td align="center" valign="top">&nbsp;</td>
      </tr>
        <tr>
          <td align="left" valign="top"><table border="0" cellspacing="1" cellpadding="0">
            <tr>
              <td width="50" >ค้นหา</td>
              <td >
                  <asp:TextBox ID="txtSearch" runat="server" Width="200px"></asp:TextBox>                </td>
              <td >
                  &nbsp;
                  <asp:Button ID="cmdFind" runat="server" CssClass="buttonFind" Text="ค้นหา" />
                </td>
            </tr>
            
          </table></td>
      </tr>
        <tr>
          <td align="center" valign="top">
              <asp:GridView ID="grdData" CssClass="table table-hover" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" DataKeyNames="PaymentID" PageSize="20">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="No.">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />                      </asp:BoundField>
            <asp:BoundField DataField="ServiceTypeID" HeaderText="ฟอร์ม">
              <headerstyle HorizontalAlign="Left" />          
              <itemstyle HorizontalAlign="Left" />                      </asp:BoundField>
                <asp:BoundField DataField="Amount" HeaderText="ค่าตอบแทน" />
                <asp:BoundField DataField="EffectiveTo" HeaderText="Effective To" />
                <asp:BoundField DataField="ServiceName" HeaderText="Description" >
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Active">
                     <itemtemplate>
                <asp:Image ID="imgStatus" runat="server" ImageUrl="images/icon-ok.png" 
                                    Visible='<%# DataBinder.Eval(Container.DataItem, "IsPublic")%>' />                        </itemtemplate>
                </asp:TemplateField>
            <asp:TemplateField HeaderText="Edit">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PaymentID")%>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgDel" runat="server" 
                                    ImageUrl="images/icon-delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PaymentID")%>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView></td>
      </tr>
       
    </table>
     </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    </section>
</asp:Content>
