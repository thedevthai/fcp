﻿Public Class SiteSimple
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not Request("PatientID") Is Nothing Then
            Session("patientid") = Request("PatientID")
        End If
        If Session("patientid") Is Nothing Then
            Response.Redirect("PatientSearch.aspx")
        End If


        If Not IsPostBack Then

        End If
        hlnkUserName.Text = Request.Cookies("NameOfUser").Value
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            hlnkUserName.NavigateUrl = "LocationsEdit.aspx?id=" & Request.Cookies("LocationID").Value
        Else
            hlnkUserName.NavigateUrl = "#"
        End If

    End Sub

End Class