﻿<%@ Page Title="Patient" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="PatientCovid3.aspx.vb" Inherits=".PatientCovid3" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">    
        function openModals(sender, title, message) {
            $("#spnTitle").text(title);
            $("#spnMsg").text(message);
            $('#btnConfirm').attr('onclick', "$('#mdEditPerson').modal('hide');setTimeout(function(){" + $(sender).prop('href') + "}, 50);");
            $('.editemp').click(function () {
             var fname = $(this).attr('data-fname');
             $('#fname').val(fname);
             $('#mdEditPerson').modal('show');

            });
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">  
     <section class="content-header">
      <h1><%: Title %>        
        <small>รายชื่อผู้ป่วย Self Isolation Care by Community Pharmacist</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">รายชื่อผู้ป่วย Self Isolation Care</li>
      </ol>
    </section>

    <section class="content">   
  <% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then%> 
        <div class="row no-print">
        <div class="col-xs-12">
          <a href="PatientSearch?ActionType=ptl"  class="btn btn-success pull-right"><i class="fa fa-plus-circle"></i> เพิ่มผู้ป่วยใหม่</a>
        </div>
      </div> <% End If %>
        <br />
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">รายชื่อผู้ป่วย Self Isolation Care</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body"> 
              <table id="tbdata" class="table table-bordered table-striped">
                <thead>
                <tr>
                 <th class="sorting_asc_disabled"></th>    
                    <th>ID</th>
                  <th>ชื่อผู้ป่วย</th>
                  <th>เพศ</th>
                  <th>อายุ</th>  
                    <th>วันที่เริ่ม</th>
                      <th>Days</th>
                     <th>สถานะ</th>
                     <% If Convert.ToInt32(Request.Cookies("RoleID").Value) <> isShopAccess Then%>  <th>ร้านยา</th> <% End If %>
                    
                </tr>
                </thead>
                <tbody>
            <% For Each row As DataRow In dtC3.Rows %>
                <tr>
                 <td width="50px">
                       <% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then%> 
                     <a class="editemp" href="Covid3?ActionType=cov3&fid=<% =String.Concat(row("UID")) %>" ><img src="images/icon-edit.png"/></a>
                       <% Else %>
                       <a class="editemp" href="Covid3?acttype=view&fid=<% =String.Concat(row("UID")) %>" ><img src="images/view.png"/></a>
                     <% End If %>
                    </td>

                  <td class="text-center"><% =String.Concat(row("PatientID")) %></td>
                  <td><% =String.Concat(row("PatientName")) %>    </td>
                  <td class="text-center"><% =String.Concat(row("Gender")) %></td>
                  <td class="text-center"><% =String.Concat(row("Ages")) %></td>
                  <td class="text-center"><% =String.Concat(row("AdmitDate")) %></td>
                  <td class="text-center"><% =String.Concat(row("SEQ")) %></td>
                  <td class="text-center"><% =IIf(String.Concat(row("CloseStatus")) = "Y", "<img src='images/icon-ok.png'>", "") %> </td>
                 <% If Convert.ToInt32(Request.Cookies("RoleID").Value) <> isShopAccess Then%>    <td><% =String.Concat(row("LocationName")) %></td> <% End If %>
                </tr>
            <%  Next %>
                </tbody>               
              </table>                                    
            </div>
            <!-- /.box-body -->
          </div>
  
    </section>
</asp:Content>
