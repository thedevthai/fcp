﻿
Public Class vmiStockRefill
    Inherits System.Web.UI.Page

    Dim dt As New DataTable

    Dim acc As New UserController
    Dim ctlbase As New ApplicationBaseClass
    'Dim ctlOrder As New SmokingController
    Dim ctlSk As New StockController
    Dim ctlInv As New InventoryController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("Username") Is Nothing Then
        '    Response.Redirect("Default.aspx?logout=YES")
        'End If
        If Not IsPostBack Then
            lblAlert.Visible = False
            grdData.PageIndex = 0
            LoadProvince()
            LoadStock4Replenishment()
        End If

    End Sub

    Private Sub LoadProvince()
        dt = ctlbase.LoadProvince
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .DataSource = dt
                .DataTextField = "ProvinceName"
                .DataValueField = "ProvinceID"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadStock4Replenishment()
        dt = ctlSk.Stock_Get4Replenishment(txtMedName.Text.Trim(), txtLocationName.Text.Trim(), ddlProvince.SelectedValue)
        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To .Rows.Count - 1
                    .Rows(i).Cells(0).Text = i + 1
                Next
            End With
            cmdReplenishment.Visible = True
            lblAlert.Visible = False
        Else
            grdData.DataSource = dt
            grdData.DataBind()
            cmdReplenishment.Visible = False
            lblAlert.Visible = True
        End If
        dt = Nothing
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadStock4Replenishment()
    End Sub
    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    Inventory_Transfer(e.CommandArgument())

            End Select

        End If
    End Sub

    Private Sub Inventory_Transfer(pUID As Integer)
        dt = ctlSk.Stock_GetByUID(pUID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Dim iQTY As Integer = DBNull2Zero(.Item("Stock")) - DBNull2Zero(.Item("OnHand"))
                ctlInv.Inventory_Transfer(pUID, .Item("LocationID"), .Item("MedUID"), iQTY, .Item("UOM"), "ISSCPA", "P", Session("Username"))

                acc.User_GenLogfile(Session("username"), ACTTYPE_ADD, "VMI_Transfer", "เติมยาให้ร้านยา: " & .Item("LocationID") & ">>" & .Item("MedUID") & ">>QTY:" & iQTY, "")
            End With

        End If
        dt = Nothing
        LoadStock4Replenishment()
    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then


            'If StrNull2Zero(e.Row.Cells(5).Text) <= StrNull2Zero(e.Row.Cells(6).Text) Then
            '    e.Row.ForeColor = Drawing.Color.Red
            'End If

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#9bd6fb';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub
    Protected Sub cmdReplenishment_Click(sender As Object, e As EventArgs) Handles cmdReplenishment.Click

        For i = 0 To grdData.Rows.Count - 1
            Inventory_Transfer(grdData.DataKeys(i).Value)
        Next
        LoadStock4Replenishment()
        DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อยแล้ว")

    End Sub
    Protected Sub cmdSearch_Click(sender As Object, e As EventArgs) Handles cmdSearch.Click
        LoadStock4Replenishment()
    End Sub
End Class

