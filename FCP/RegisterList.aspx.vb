﻿
Public Class RegisterList
    Inherits System.Web.UI.Page
    Dim ctlR As New RegisterController
    Public dtReg As New DataTable
    Public enc As New CryptographyEngine
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Not IsPostBack Then
            txtStartDate.Text = DateAdd(DateInterval.Year, -1, Today.Date).ToString("dd/MM/yyyy")
            txtEndDate.Text = Today.Date.ToString("dd/MM/yyyy")
            LoadLocationGroupToDDL()
            LoadRegisterList()
        End If
    End Sub
    Private Sub LoadLocationGroupToDDL()
        Dim dt As New DataTable
        Dim ctlLG As New LocationGroupController
        dt = ctlLG.LocationGroup_GetForReport
        If dt.Rows.Count > 0 Then
            With ddlGroup
                .Enabled = True
                .DataSource = dt
                .DataTextField = "Name"
                .DataValueField = "Code"
                .DataBind()
                .SelectedValue = "0"
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadRegisterList()
        If txtStartDate.Text = "" Then
            txtStartDate.Text = DateAdd(DateInterval.Year, -1, Today.Date).ToString("dd/MM/yyyy")
        End If
        If txtEndDate.Text = "" Then
            txtEndDate.Text = Today.Date.ToString("dd/MM/yyyy")
        End If

        Dim Bdate, Edate As String
        Bdate = ConvertStrDate2DBDate(txtStartDate.Text)
        Edate = ConvertStrDate2DBDate(txtEndDate.Text)

        dtReg = ctlR.Register_GetBySearch(Bdate, Edate, ddlGroup.SelectedValue, ddlStatus.SelectedValue)

    End Sub


    Protected Sub cmdView_Click(sender As Object, e As EventArgs) Handles cmdView.Click
        LoadRegisterList()
    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged
        LoadRegisterList()
    End Sub

    Protected Sub ddlStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlStatus.SelectedIndexChanged
        LoadRegisterList()
    End Sub

End Class