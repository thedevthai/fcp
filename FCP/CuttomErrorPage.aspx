﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="CuttomErrorPage.aspx.vb" Inherits=".CuttomErrorPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/uidialog.css">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
     <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td align="center"><h3>ขณะนี้ ผู้ดูแลระบบกำลังปรับปรุงระบบอยู่<br />
          <asp:Label ID="lblMsg" runat="server" Text="ขอปิดระบบการบันทึกข้อมูล ชั่วคราว"></asp:Label></h3>
        </td>
    </tr>
   <tr>
      <td align="center">ท่านสามารถเข้ามาบันทึกข้อมูลได้อีกครั้ง ภายใน 5 นาที หลังจากนี้ </td>
    </tr>
    <tr>
      <td align="center">        ขออภัยในความไม่สะดวก</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
  </table>
</asp:Content>
