﻿Public Class DrugSearch
    Inherits System.Web.UI.Page
    Public Shared pageName As String
    Public Shared ItemCode As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then
            pageName = Request("p")
            LoadDrug()
        End If

    End Sub
    Private Sub LoadDrug()
        Dim dtM As New DataTable
        Dim ctlD As New DrugController
        If txtDrugSearch.Text <> "" Then
            dtM = ctlD.Drug_GetBySearch(txtDrugSearch.Text)
            If dtM.Rows.Count > 0 Then
                'grdDrugTMT.DataSource = dtM
                'grdDrugTMT.DataBind()
                lstData.DataSource = dtM
                lstData.DataTextField = "Name"
                lstData.DataValueField = "Name"
                lstData.DataBind()
            End If
        End If

        dtM = Nothing
    End Sub

    Protected Sub cmdDrugSearch_Click(sender As Object, e As EventArgs) Handles cmdDrugSearch.Click
        LoadDrug()
    End Sub

    Private Sub cmdOK_Click(sender As Object, e As EventArgs) Handles cmdOK.Click
        ItemCode = lstData.SelectedValue
    End Sub
End Class