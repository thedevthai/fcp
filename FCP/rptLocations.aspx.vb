﻿Public Class rptLocations
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlType As New LocationGroupController
    Dim ctlLoc As New LocationController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        LoadDataToGrid(Request("grp"), Request("p"), Request("pj"))

        If Request("ex") = 1 Then
            Response.AppendHeader("content-disposition", "attachment;filename=" & ReportsName & ".xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
        End If

    End Sub

    Private Sub LoadDataToGrid(ByVal PType As String, ByVal pProvID As String, ByVal ProjID As Integer)

        dt = ctlLoc.Location_GetReport(PType, pProvID, ProjID)

        If dt.Rows.Count > 0 Then
            lblCount.Text = dt.Rows.Count

            If PType = "0" Then
                lblTypeName.Text = "ทั้งหมด"
            Else
                lblTypeName.Text = String.Concat(dt.Rows(0)("LocationGroupName"))
            End If

            If pProvID = "0" Then
                lblProvinceName.Text = "ทั้งหมด"
            Else
                lblProvinceName.Text = String.Concat(dt.Rows(0)("ProvinceName"))
            End If

            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To dt.Rows.Count - 1
                    .Rows(i).Cells(0).Text = i + 1
                Next
                If Request("ex") = 1 Then
                    .Columns(5).Visible = True
                Else
                    .Columns(5).Visible = False
                End If

            End With


        Else
            lblCount.Text = 0
            grdData.Visible = False
            grdData.DataSource = Nothing

        End If
        dt = Nothing
    End Sub

End Class