﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucShopMenu.ascx.vb" Inherits=".ucShopMenu" %><style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
<table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td>
<ul class="menu1" >

<% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess  Then%>
            <li>
                <% If Request.QueryString("ItemType") = "shop" Then%>
                    <a class="current" href="LocationsEdit.aspx?ActionType=cus&ItemType=shop"><span>ข้อมูลร้านยา</span></a>
                <% Else%>
                    <a href="LocationsEdit.aspx?ActionType=cus&ItemType=shop"><span>ข้อมูลร้านยา</span></a>
                <% End If%>
            </li>
            
<% End If%>
               	        
<% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isAdminAccess Or Convert.ToInt32(Request.Cookies("RoleID").Value) = isSuperAdminAccess Then%>
            
      <% If Request.QueryString("ItemType") = "grp" Then%>
        <li><a class="current" href="LocationGroup.aspx?ActionType=shop&ItemType=grp"> ประเภทร้านยา </a></li>
      <% Else %>
        <li><a href="LocationGroup.aspx?ActionType=shop&ItemType=grp"> ประเภทร้านยา</a></li>
      <% End If %>
                 
      <% If Request("ItemType") = "AddShop" Then%>
        <li><a class="current" href="Locations.aspx?ActionType=shop&ItemType=AddShop">ร้านยา</a></li>
      <% Else %>
        <li><a href="Locations.aspx?ActionType=shop&ItemType=AddShop">ร้านยา</a></li>
      <% End If %> 

<% End If %>      
 
<% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isAdminAccess Or Convert.ToInt32(Request.Cookies("RoleID").Value) = isSuperAdminAccess Or (Convert.ToInt32(Request.Cookies("RoleID").Value) = isProjectManager And Request.Cookies("PRJMNG").Value = PROJECT_SMOKING) Then%>                  
      <% If Request("ItemType") = "AddPM" Then%>
        <li><a class="current" href="Pharmacist.aspx?ActionType=shop&ItemType=AddPM">เภสัชกร</a></li>
      <% Else %>
        <li><a href="Pharmacist.aspx?ActionType=shop&ItemType=AddPM">เภสัชกร</a></li>
      <% End If %> 

<% End If%>
</ul></td>
  </tr>
</table>