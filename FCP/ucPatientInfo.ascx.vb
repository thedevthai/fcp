﻿Public Class ucPatientInfo
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Request("PatientID") Is Nothing Then
            Session("patientid") = Request("PatientID")
        End If
        If Not IsPostBack Then
            If Session("patientid") <> 0 Then
                ShowPatientInfo()
            End If
        End If

    End Sub
    Private Sub ShowPatientInfo()
        Dim ctlP As New PatientController
        Dim dtP As New DataTable
        dtP = ctlP.Patient_GetByID(Session("patientid"))
        If dtP.Rows.Count > 0 Then
            With dtP.Rows(0)
                Session("patientid") = String.Concat(.Item("PatientID"))
                lblPatientID.Text = String.Concat(.Item("PatientID"))
                lblName.Text = String.Concat(.Item("PatientName"))
                lblGender.Text = String.Concat(.Item("Gender"))
                'lblAges.Text = String.Concat(.Item("Ages"))
                lblAddress.Text = String.Concat(.Item("Addresss"))
                lblCardID.Text = FormatCardID(String.Concat(.Item("CardID")))
                lblClaim.Text = String.Concat(.Item("Claim"))
                lblTel.Text = String.Concat(.Item("Tel"))
                If String.Concat(.Item("BirthDate")) <> "" Then
                    Try
                        Dim iAge As Integer
                        iAge = DateDiff(DateInterval.Year, CDate(DisplayStr2ShortDateTH(String.Concat(.Item("BirthDate")))), ctlP.GET_DATE_SERVER.Date)
                        If iAge < 0 Then
                            iAge = iAge + 543
                        ElseIf iAge > 543 Then
                            iAge = iAge - 543
                        End If

                        lblAges.Text = iAge.ToString()


                    Catch ex As Exception
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาตรวจสอบวันเกิดผู้ใช้บริการ');", True)
                    End Try
                Else
                    lblAges.Text = String.Concat(.Item("Ages"))
                End If
                Session("age") = StrNull2Zero(lblAges.Text)
                If String.Concat(.Item("Gender")) = "ชาย" Then
                    Session("sex") = "M"
                Else
                    Session("sex") = "F"
                End If



            End With



        End If
        dtP = Nothing
    End Sub
End Class