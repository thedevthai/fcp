﻿Public Class rptCustomerByActivityAndProvince
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlType As New ServiceTypeController
    Dim ctlOrder As New OrderController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            
            If Request("p") <> Request("grp") Then
                lblProvinceName.Text = "จังหวัด " & ctlOrder.Province_GetNameByID(Request("p"))
            Else
                lblProvinceName.Text = ctlOrder.ProvinceGroupName_GetByID(Request("grp"))
            End If


            Select Case Request("t")
                Case FORM_TYPE_ID_F01
                    grdData.Columns(11).Visible = True
                    pnF01.Visible = True
                Case FORM_TYPE_ID_F04
                    grdData.Columns(10).HeaderText = "การฉีดวัคซีน"
                    grdData.Columns(11).Visible = False
                    pnF04.Visible = True
                Case Else
                    grdData.Columns(10).HeaderText = "หมายเหตุ"
                    grdData.Columns(11).Visible = False
                    pnF01.Visible = False
                    pnF04.Visible = False
            End Select

            If Request("t") = "0" Then
                lblServiceTypeName.Text = "กิจกรรมทั้งหมด"
            Else
                grdData.Columns(2).Visible = False
                lblServiceTypeName.Text = ctlType.ServiceType_GetNameByID(Request("t"))
            End If

            If (Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess) And (Request("lid") <> "0") Then
                grdData.Columns(1).Visible = False
            End If

            If Request("lid") <> "0" Then
                grdData.Columns(1).Visible = False
                lblShop.Visible = True
                lblLocationName.Visible = True
            Else
                lblShop.Visible = False
                lblLocationName.Visible = False
            End If

        End If

        lblStartDate.Text = Request("b")
        lblEndDate.Text = Request("e")

        LoadDataToGrid(Request("lid"), Request("t"), Request("b"), Request("e"))

        If Request("ex") = 1 Then
            Response.AppendHeader("content-disposition", "attachment;filename=" & ReportsName & ".xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
        End If

    End Sub
    'Private Sub LoadLocationData()
    '    dt = ctlLct.Location_GetByID(Request.Cookies("LocationID").Value)
    '    If dt.Rows.Count > 0 Then
    '        With dt.Rows(0)
    '            lblLocationName.Text = .Item("LocationName") & " (" & .Item("LocationID") & ")"
    '        End With
    '    End If
    '    dt = Nothing
    'End Sub

    Private Sub LoadDataToGrid(ByVal LocationID As String, ByVal PType As String, ByVal Bdate As String, ByVal EDate As String)

        Dim StartDate As String = "0"
        Dim EndDate As String = "0"
        Dim sumCount, sumRisk, sumEdu1, sumEdu2 As Integer
        sumCount = 0
        sumRisk = 0
        sumEdu1 = 0
        sumEdu2 = 0

        If Bdate = "" And EDate = "" Then
            StartDate = 0
            EndDate = 0
        Else
            StartDate = ConvertStrDate2DBString(Bdate)
            EndDate = ConvertStrDate2DBString(EDate)
        End If

        Select Case PType
            Case FORM_TYPE_ID_F01

                If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then

                    If Request("p") <> Request("grp") Then
                        dt = ctlOrder.RPT_Order_GetCustomerF01ByProvince(Request("p"), LocationID, PType, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                    Else
                        dt = ctlOrder.RPT_Order_GetCustomerF01ByProvinceGroup(Request("grp"), LocationID, PType, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                    End If
                Else
                    dt = ctlOrder.RPT_Order_GetCustomerF01ByProvince(Request("p"), LocationID, PType, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                End If

            Case Else

                If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
                    If Request("p") <> Request("grp") Then
                        dt = ctlOrder.RPT_Order_GetCustomerByActivityAndProvince(Request("p"), LocationID, PType, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                    Else
                        dt = ctlOrder.RPT_Order_GetCustomerByActivityAndProvinceGroup(Request("grp"), LocationID, PType, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                    End If

                Else
                    dt = ctlOrder.RPT_Order_GetCustomerByActivityAndProvince(Request("p"), LocationID, PType, StrNull2Zero(StartDate), StrNull2Zero(EndDate))
                End If

        End Select


        If dt.Rows.Count > 0 Then
            lblCount.Text = dt.Rows.Count
            sumCount = dt.Rows.Count

            lblLocationName.Text = dt.Rows(0)("LocationName") & " (" & dt.Rows(0)("LocationID") & ") "

            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To dt.Rows.Count - 1
                    .Rows(i).Cells(0).Text = i + 1
                    .Rows(i).Cells(5).Text = DisplayGenderName(String.Concat(dt.Rows(i)("Gender")))

                    Dim ad As String
                    ad = String.Concat(dt.Rows(i)("AddressNo"))
                    If String.Concat(dt.Rows(i)("Road")) <> "" Then
                        ad = ad + " ถนน" + String.Concat(dt.Rows(i)("Road"))
                    End If
                    If String.Concat(dt.Rows(i)("District")) <> "" Then
                        ad = ad + " แขวง" + String.Concat(dt.Rows(i)("District"))
                    End If
                    If String.Concat(dt.Rows(i)("City")) <> "" Then
                        ad = ad + " เขต" + String.Concat(dt.Rows(i)("City"))
                    End If

                    .Rows(i).Cells(8).Text = ad


                    Select Case PType
                        Case FORM_TYPE_ID_F01
                            .Rows(i).Cells(10).Text = DBNull2Str(dt.Rows(i)("EducateCount"))
                            If DBNull2Str(dt.Rows(i)("EducateCount")) = 1 Then
                                sumEdu1 = sumEdu1 + 1
                            ElseIf DBNull2Str(dt.Rows(i)("EducateCount")) = 2 Then
                                sumEdu1 = sumEdu1 + 1
                                sumEdu2 = sumEdu2 + 1
                            End If


                            If DBNull2Zero(dt.Rows(i)("isRiskFat")) = 0 And DBNull2Zero(dt.Rows(i)("isRiskBloodPressure")) = 0 And DBNull2Zero(dt.Rows(i)("isRiskDiabetes")) = 0 Then
                                .Rows(i).Cells(11).Text = "ไม่เสี่ยง"

                            Else
                                .Rows(i).Cells(11).Text = "เสี่ยง"
                                sumRisk = sumRisk + 1
                            End If


                        Case FORM_TYPE_ID_F04

                            If DBNull2Str(dt.Rows(i)("VaccineComplete")) = "Y" Then
                                .Rows(i).Cells(10).Text = "ครบ"
                                sumEdu1 = sumEdu1 + 1
                            ElseIf DBNull2Str(dt.Rows(i)("VaccineComplete")) = "N" Then
                                .Rows(i).Cells(10).Text = "ไม่ครบ"
                                sumEdu2 = sumEdu2 + 1
                            End If
                        Case Else
                            .Rows(i).Cells(10).Text = ""
                    End Select

                Next
            End With



        Else
            lblCount.Text = 0
            grdData.Visible = False
            grdData.DataSource = Nothing

        End If

        pnF01.Visible = False
        pnF04.Visible = False

        Select Case PType
            Case FORM_TYPE_ID_F01
                lblRisk.Text = sumRisk.ToString("#,##0")
                lblRiskNo.Text = (sumCount - sumRisk).ToString("#,##0")
                lblEdu1.Text = sumEdu1.ToString("#,##0")
                lblEdu2.Text = sumEdu2.ToString("#,##0")

                grdData.Columns(11).Visible = True
                pnF01.Visible = True
            Case FORM_TYPE_ID_F04
                lblFull.Text = sumEdu1.ToString("#,##0")
                lblFullNot.Text = sumEdu2.ToString("#,##0")
                grdData.Columns(10).HeaderText = "การฉีดวัคซีน"
                grdData.Columns(11).Visible = False
                pnF04.Visible = True
            Case Else
                grdData.Columns(10).HeaderText = "หมายเหตุ"
                grdData.Columns(11).Visible = False

                pnF01.Visible = False
                pnF04.Visible = False
        End Select

        dt = Nothing
    End Sub

End Class