﻿Imports System.Data
Public Class PatientCovid2
    Inherits System.Web.UI.Page
    Dim ctlCov As New CovidController
    Public dtC As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx")
        End If

        If Not IsPostBack Then
            LoadPatient()
        End If
    End Sub
    Private Sub LoadPatient()
        dtC = ctlCov.COVID2_Patient_Get(Request.Cookies("LocationID").Value)
    End Sub

End Class