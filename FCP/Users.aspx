﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="Users.aspx.vb" Inherits=".Users" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/rajchasistyles.css">
    <link href="css/pagestyles.css" rel="stylesheet" type="text/css" />
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>จัดการข้อมูล User Account
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
  
 <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลกิจกรรม</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">



 
                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
        
          
          <tr>
            <td bgcolor="#FFFFFF">
            
            
            
            <TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" bgColor="#ffffff" border="0">
<TBODY>
					<TR>
						<TD>
						  <TABLE id="Table2" height="100%" cellSpacing="2" cellPadding="0" width="100%" border="0">
				  <TR>
									<TD  valign="top">
                                    
                                    <table width="100%" border="0" align="left" cellpadding="0" cellspacing="2">
                                      <tr>
                                        <td align="center">&nbsp;</td>
                                        <td width="26%" align="center">&nbsp;</td>
                                        <td align="center">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td  valign="top" class="block_step1" >
                                            <table width="100%" border="0" class="table"  cellpadding="0" cellspacing="2">
                                        
                                        <tr>
                                            <td colspan="3" class="Topic_header">Step 1. เลือกกลุ่มผู้ใช้</td>
                                          </tr>
                                          
                                          <tr>
                                          
                                            <td colspan="3" >
                                              <table border="0" align="left" cellpadding="0" cellspacing="0">
                                                <tr>
                                                  <td width="65" align="left">กลุ่มผู้ใช้                                              </td>
                                                  <td><asp:DropDownList ID="ddlUserGroup" 
                                                    runat="server"  AutoPostBack="True" CssClass="form-control select2">
                                                    <asp:ListItem Selected="True" Value="0">--- เลือกกลุ่มผู้ใช้ ---</asp:ListItem>
                                                    <asp:ListItem Value="1">ร้านยา</asp:ListItem>
                                                    <asp:ListItem Value="2">เจ้าหน้าที่ CPA</asp:ListItem>
                                                  </asp:DropDownList></td>
                                                </tr>
                                              </table></td>
                                          </tr>
                                      <tr>
                                            <td  width="65" >
                                                <asp:Label ID="lblLocation" runat="server" Text="ร้านยา"></asp:Label>                                            </td>
                                            <td  width="400" >     <asp:DropDownList ID="ddlLocation" runat="server"  
                                                            AutoPostBack="True" CssClass="form-control select2" Width="400px">                                                        </asp:DropDownList>
                                                                                             </td>
                                            <td><asp:Image ID="imgArrowLocation" runat="server" ImageUrl="images/arrow-orange-icons.png" />
                                            <asp:TextBox ID="txtFindLocation" runat="server" Width="100px"></asp:TextBox>
                                              <asp:LinkButton ID="lnkFindLocation" runat="server" CssClass="buttonFind">ค้นหา</asp:LinkButton>                                              </td>
                                          </tr>
                                      <tr>
                                        <td colspan="3" >
                                            <asp:Label ID="lblvalidate1" runat="server" CssClass="validateAlert" 
                                                Visible="False" Width="99%"></asp:Label></td>
                                        </tr> 
                                        </table>                                        </td>
                                        
                                        <td  valign="top" class="block_step2" ><table width="100%" border="0" cellpadding="1" cellspacing="1">
                                          <tr>
                                            <td colspan="2" class="Topic_header">Step 2. บันทึกข้อมูลผู้ใช้</td>
                                            <td class="Topic_header">&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td width="80" align="left" >UserID : </td>
                                            <td align="left" class="Normal"><asp:Label ID="lblID" runat="server"></asp:Label>                                            </td>
                                            <td align="left" class="Normal">&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td align="left" >Username :</td>
                                            <td align="left" class="Normal" valign="middle"><asp:TextBox ID="txtUsername" runat="server" 
                                                        CssClass="form-control" ></asp:TextBox>                                          </td>
                                            <td align="left" class="Normal" valign="middle" width="30">&nbsp;<asp:Image 
                                                      ID="imgAlert" runat="server" Height="12px" ImageUrl="images/alert_icon.png" 
                                                      Width="12px" />  </td>
                                          </tr>
                                          <tr>
                                            <td align="left" >Password :</td>
                                            <td align="left" class="Normal"><span >
                                              <asp:TextBox ID="txtPassword" runat="server" 
                                                         CssClass="form-control"></asp:TextBox>
                                            </span></td>
                                            <td align="left" class="Normal">&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td align="left" valign="top" >ชื่อ :</td>
                                            <td align="left" valign="top" class="Normal"><asp:TextBox ID="txtFirstName" runat="server" 
                                                       CssClass="form-control"></asp:TextBox></td>
                                            <td align="left" valign="top" class="Normal">&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td align="left" valign="top" >นามสกุล :</td>
                                            <td align="left" valign="top" class="Normal"><asp:TextBox ID="txtLastName" runat="server" CssClass="form-control"></asp:TextBox>                                            </td>
                                            <td align="left" valign="top" class="Normal">&nbsp;</td>
                                          </tr>
                                             <tr>
                                            <td align="left" valign="top" >อีเมล์ :</td>
                                            <td align="left" valign="top" class="Normal"><asp:TextBox ID="txtMail" runat="server"  CssClass="form-control"></asp:TextBox>                                            </td>
                                            <td align="left" valign="top" class="Normal">&nbsp;</td>
                                          </tr>
                                          <tr>
                                            <td colspan="3" align="left" ><asp:Label ID="lblvalidate2" runat="server" CssClass="validateAlert" 
                                                Visible="False" Width="99%"></asp:Label></td>
                                         
                                          </tr>
                                        </table></td>
                                        <td  valign="top" class="block_step3" ><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2">
                                          <tr>
                                            <td align="left" class="Topic_header">Step 3. กำหนดสิทธิ์ให้ผู้ใช้</td>
                                          </tr>
                                             <tr>
                                            <td align="left" class="texttopic" >โครงการ :</td>
                                          </tr>
                                             <tr>
                                            <td align="left">
                                                <asp:CheckBoxList ID="chkProject" runat="server">
                                                    <asp:ListItem Value="1">F : โครงการให้คำแนะนำ ปรึกษาฯ</asp:ListItem>
                                                    <asp:ListItem Value="2">A : โครงการบุหรี่</asp:ListItem>
                                                    <asp:ListItem Value="3">H : โครงการเยี่ยมบ้าน และ MTM</asp:ListItem>

                                                    <asp:ListItem Value="4">Home Isolation (Covid-19)</asp:ListItem>

                                                </asp:CheckBoxList>
                                                 </td>
                                          </tr>
                                          <tr>
                                            <td align="left" class="texttopic" >สิทธิ์การใช้งาน :</td>
                                          </tr>
                                          <tr>
                                            <td align="left">
                                                <asp:RadioButtonList ID="optRoles" runat="server" AutoPostBack="True">                                                </asp:RadioButtonList>                                              </td>
                                          </tr>
                                           <tr>
                                            <td><span class="texttopic">
                                                <asp:Label ID="lblMNGRole" runat="server" Text="ผู้จัดการโครงการ :"></asp:Label> 
                                                <asp:Label ID="lblRPTRole" runat="server" Text="สิทธิ์ดูรายงาน :"></asp:Label> &nbsp;</span><asp:DropDownList 
                                                    ID="ddlRPTRole" runat="server">
                                                </asp:DropDownList>
                                                <asp:DropDownList 
                                                    ID="ddlProjectRole" runat="server">
                                                    <asp:ListItem Value="1">F : โครงการให้คำแนะนำ ปรึกษา ฯ</asp:ListItem>
                                                    <asp:ListItem Value="2">A : โครงการบุหรี่</asp:ListItem>
                                                </asp:DropDownList>
                                               </td>
                                          </tr>
                                          <tr>
                                            <td><span class="texttopic">Status : </span>
                                              <asp:CheckBox ID="chkStatus" runat="server" Checked="True" Text="อนุญาตให้ใช้ได้" /></td>
                                          </tr>
                                          <tr>
                                            <td><asp:Label ID="lblvalidate3" runat="server" CssClass="validateAlert" 
                                                    Visible="False" Width="99%"></asp:Label>                                            </td>
                                          </tr>
                                        </table></td>
                                      </tr>
                                      <tr>
                                        <td colspan="3" align="center" valign="top">
                                            <br />
        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="btn btn-primary" Width="100px" />
        &nbsp;<asp:Button ID="cmdClear" runat="server" Text="ยกเลิก" CssClass="btn btn-default" Width="100px" />
                                              &nbsp;<asp:Button ID="cmdSendMail" runat="server" Text="ส่งอีเมล์แจ้งข้อมูลผู้ใช้งาน" CssClass="btn btn-success" />
                                        
                                          </td>
                                      </tr>
                    </table>					</TD>
</TR>
		<tr>
          <td> &nbsp;</td>
              </tr>
       <tr>
          <td  align="left" valign="top" class="skin_Search"><table border="0" cellspacing="1" cellpadding="0">
            <tr>
              <td width="50" >ค้นหา</td>
               <td width="50" > <asp:DropDownList ID="ddlGroupFind" 
                                                    runat="server"  AutoPostBack="True" 
                       CssClass="form-control select2" Width="200px">
                                                    <asp:ListItem Selected="True" Value="0">--- ทั้งหมด ---</asp:ListItem>
                                                    <asp:ListItem Value="1">ร้านยา</asp:ListItem>
                                                    <asp:ListItem Value="2">เจ้าหน้าที่ CPA</asp:ListItem>
                                                  </asp:DropDownList> </td>

              <td>
                  <asp:TextBox ID="txtSearch" runat="server" Width="150px"></asp:TextBox>
                </td>
              <td>
                  <asp:Button ID="cmdFind" runat="server" CssClass="buttonFind" Text="ค้นหา" />
                </td>
            </tr>
            <tr>
                  <td colspan="3" class="text10_nblue"><strong>&nbsp;*</strong>คำค้นหาสามารถค้นหาได้จาก username , ชื่อ</td>
              </tr>
          </table></td>
      </tr>
       <tr>
          <td align="left" valign="top"  >
              <asp:Label ID="lblStudentCount" runat="server"></asp:Label>           </td>
    </tr>
				  <TR>
				     <TD  valign="top" class="text12b_nblue">&nbsp;<asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" CssClass="txtcontent" 
                             Font-Bold="False">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                            <asp:BoundField HeaderText="No.">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />                            </asp:BoundField>
                        <asp:BoundField DataField="Username" HeaderText="Username" />
                            <asp:BoundField HeaderText="ชื่อ" />
                            <asp:BoundField DataField="EMail" HeaderText="อีเมล์" />
                            <asp:BoundField DataField="RoleName" HeaderText="สิทธิ์การใช้งาน" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />                            </asp:BoundField>
                            <asp:BoundField HeaderText="ดูรายงาน" />
                        <asp:BoundField DataField="LastLogin" HeaderText="Last Login" >
                            <HeaderStyle HorizontalAlign="Left" />                            </asp:BoundField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Image ID="imgStatus" runat="server" ImageUrl="images/icon-ok.png" 
                                    Visible='<%# DataBinder.Eval(Container.DataItem,"IsPublic") %>' />                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem,"UserID") %>' />                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgDel" runat="server" 
                                    ImageUrl="images/icon-delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem,"UserID") %>' />                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                              <asp:TemplateField HeaderText="">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgMail" runat="server" ImageUrl="images/paperplane.png" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UserID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView>  </TD>
				     </TR>
				  <TR>
				     <TD  valign="top">&nbsp;</TD>
				     </TR>
								</TABLE>					  </TD>
		  </TR>
				</TBODY>
			</TABLE>            </td>
            </tr>
         
        </table>   
                 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>  
    </section>    
</asp:Content>
