﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="COVID3.aspx.vb" Inherits=".COVID3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">           
</asp:Content> 

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <section class="content-header">
      <h1>แบบฟอร์มการดูแลผู้ป่วย COVID 19  โดยเภสัชกรชุมชน
        <small>Self Isolation Care by Community Pharmacist</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Home Isolation</li>
      </ol>
    </section>

<section class="content"> 
     <div class="pull-right">
    <table  border="0" cellspacing="2" cellpadding="0" align="right">
        <tr>        
          <td class="NameEN">Item ID : </td>
          <td class="NameEN">              <asp:Label ID="lblCOVIDUID"                  runat="server"></asp:Label>            </td>
        </tr>
      </table> 
         </div>
<br />
 <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">ข้อมูลร้านยาที่ให้บริการ</h3>
          <div class="box-tools pull-right">           

            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
 <table width="100%" border="0" align="left" cellpadding="0" cellspacing="2" class="OptionPanels">
      <tr>
        <td width="50">รหัสร้าน</td>
        <td width="120" class="LocationName">
            <asp:Label ID="lblLocationID" runat="server"></asp:Label>          </td>
        <td width="50">ชื่อร้าน</td>
        <td width="300" class="LocationName">
            <asp:Label ID="lblLocationName" runat="server"></asp:Label>          </td>
           <td width="100">รหัสหน่วยบริการ</td>
           <td  class="LocationName">
               <asp:Label ID="lblLocationCode" runat="server"></asp:Label>
          </td>
        <td width="50">จังหวัด</td>
        <td class="LocationName">
            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>          </td>
      </tr>
    </table>
                  
      </div>
      
</div> 

       <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">ข้อมูลเบื้องต้น</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">   
             <div class="row">
           <div class="col-md-3">
          <div class="form-group">
            <label>วันที่ตรวจ Ag Test Kit ได้ผลบวก </label>
              <asp:TextBox ID="txtSwabDate1" runat="server" placeholder="" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
                    <label>วันที่ตรวจยืนยัน RT-PCR</label>
              <asp:TextBox ID="txtSwabDate2" runat="server" placeholder="" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
          </div>
        </div>    
                     <div class="col-md-3">
          <div class="form-group">
            <label>วันที่ร้านยาเริ่มดูแล</label>
              <asp:TextBox ID="txtStartDate" runat="server" placeholder="" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
          </div>
        </div>
      </div>
   <div class="row">
         <div class="col-md-6">
          <div class="form-group">
            <label>ได้มีการแจ้งเข้าระบบ HI 1330 ต่อ 14 หรือช่องทางอื่นหรือไม่</label>
                <asp:RadioButtonList ID="optCall" runat="server" RepeatDirection="Horizontal" >
                   <asp:ListItem Value="N">ไม่ได้แจ้ง</asp:ListItem>
                    <asp:ListItem Value="Y">แจ้ง</asp:ListItem>                  
              </asp:RadioButtonList>
          </div>
        </div>
         <div class="col-md-3">
          <div class="form-group">
            <label>ระบุวันที่</label>
                <asp:TextBox ID="txtCallDate" runat="server" placeholder="" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
          </div>
        </div>

   </div>
  <div class="row">
       <div class="col-md-3">
          <div class="form-group">
               <label>ท่านสูบบุหรี่หรือไม่</label>
                  <asp:RadioButtonList ID="optSmoke" runat="server" AutoPostBack="true" RepeatDirection="Vertical">
                    <asp:ListItem Value="1">สูบ (ระบุจำนวนมวน)</asp:ListItem>
                    <asp:ListItem Value="2">ไม่สูบ</asp:ListItem>
                    <asp:ListItem Value="3">เคยสูบแต่เลิกแล้ว (ระบุ)</asp:ListItem>
                  </asp:RadioButtonList>
          </div>
        </div>
       <div class="col-md-6">
          <div class="form-group"> <br />
               <label>ระบุจำนวนมวน</label>  
              <asp:TextBox ID="txtSmokeQTY" runat="server" placeholder="มวน/วัน" CssClass="text-center" Width="100"></asp:TextBox>
              <br /> <br />
 <label>ระบุเลิกแล้วนานเท่าใด</label>   
              <asp:TextBox ID="txtY" runat="server" placeholder="ปี" CssClass="text-center" Width="50"></asp:TextBox>         
               <asp:TextBox ID="txtM" runat="server" placeholder="เดือน" CssClass="text-center" Width="50"></asp:TextBox>
                    <asp:TextBox ID="txtD" runat="server" placeholder="วัน" CssClass="text-center" Width="50"></asp:TextBox>
          </div>
        </div>
      <div class="col-md-12">
          <div class="form-group">
            <label>โรคประจำตัว</label>   
                <table>
                <tr>
                    <td align="left"><asp:CheckBox ID="chkDisease1" runat="server" Text="เบาหวาน"/>                    </td> 
                    <td align="left"><asp:CheckBox ID="chkDisease2" runat="server" Text="ความดันโลหิตสูง" />                    </td>
                    <td align="left"><asp:CheckBox ID="chkDisease3" runat="server" Text="ไขมันในเลือดสูง" />                    </td>
                </tr>
                <tr>
                    <td align="left"><asp:CheckBox ID="chkDisease4" runat="server" Text="กล้ามเนื้อหัวใจขาดเลือด" />                    </td> 
                    <td align="left"><asp:CheckBox ID="chkDisease5" runat="server" Text="หลอดเลือดสมองอุดตัน" /></td>
                    <td align="left"><asp:CheckBox ID="chkDisease6" runat="server" Text="ซึมเศร้า" /></td>
                </tr>
                <tr>
                    <td align="left"><asp:CheckBox ID="chkDisease7" runat="server" Text="ปอดอุดกั้นเรื้องรัง" />                    </td> 
                    <td align="left"><asp:CheckBox ID="chkDisease8" runat="server" Text="หืด" /></td>
                    <td align="left"><asp:CheckBox ID="chkDisease9" runat="server" Text="ภูมิแพ้" /></td>
                </tr>
                <tr>
                  <td align="left"><asp:CheckBox ID="chkDisease10" runat="server" Text="ตั้งครรภ์/ให้นมบุตร" /></td>
                  <td align="left"><asp:CheckBox ID="chkDisease11" runat="server" Text="ไต" /></td>
                  <td align="left"><asp:CheckBox ID="chkDisease12" runat="server" Text="ตับ" /></td>                 
                </tr>
                <tr>
                  <td align="left"><asp:CheckBox ID="chkDisease0" runat="server" Text="ไม่มีโรคประจำตัว" /></td>                  
                  <td align="left"><asp:CheckBox ID="chkDisease99" runat="server" Text="ไม่ทราบ" /></td>
                  <td colspan="2" align="left" valign="middle"><asp:CheckBox ID="chkDiseaseOther" runat="server" Text="อื่นๆ"  /> 
                    &nbsp;<asp:TextBox ID="txtDiseaseOther" runat="server"  Width="160px"></asp:TextBox></td>
                </tr>
            </table>
             
          </div>
        </div>
            </div>
              <h5 class="text-blue text-bold">การรักษา</h5>
             <div class="row">       
        <div class="col-md-6">
          <div class="form-group">
                    <label>ยา (เลือกได้มากกว่า 1)</label>
               <asp:CheckBoxList ID="chkMed" runat="server" RepeatDirection="Vertical" RepeatColumns="3">
                    <asp:ListItem Value="M1">ฟ้าทะลายโจร</asp:ListItem>
                    <asp:ListItem Value="M2">Favipiravir</asp:ListItem> 
                    <asp:ListItem Value="M3">ยาสมุนไพร</asp:ListItem>      
                   <asp:ListItem Value="M4">Nat Long </asp:ListItem>    
                   <asp:ListItem Value="M5">Ivermectin</asp:ListItem> 
                   <asp:ListItem Value="M6">ยาพาราลดไข้</asp:ListItem>
                   <asp:ListItem Value="M7">ยาแก้ไอ</asp:ListItem>
                   <asp:ListItem Value="M8">ชุดยาบรรเทาอาการเบื่องต้นของโครงการฯ </asp:ListItem> 
                   <asp:ListItem Value="M99">อื่นๆ ระบุ</asp:ListItem>
              </asp:CheckBoxList>

          </div>
        </div>    
                 <div class="col-md-2">
          <div class="form-group">
            <label>ระบุยาแก้ไอ</label>
              <asp:TextBox ID="txtMedicineCough" runat="server" placeholder="ระบุยาอื่นๆ" CssClass="form-control"></asp:TextBox>
          </div> 
         </div>
          <div class="col-md-2">
          <div class="form-group">
            <label>ระบุยาอื่นๆ</label>
              <asp:TextBox ID="txtMedOther" runat="server" placeholder="ระบุยาอื่นๆ" CssClass="form-control"></asp:TextBox>
          </div>
        </div>   
      </div>
           </div>
</div>

  <asp:Panel ID="pnCOVID" runat="server">
    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">การติดตามอาการ</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">  

              <div class="row">
           <div class="col-md-1">
          <div class="form-group">
            <label>ครั้งที่</label>
              <asp:TextBox ID="txtSEQ" runat="server" placeholder="" CssClass="form-control text-center"></asp:TextBox>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
                    <label>วันที่</label>
              <asp:TextBox ID="txtServiceDate" runat="server" placeholder="" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
          </div>
        </div>   
                    <div class="col-md-4">
          <div class="form-group">
            <label>เภสัชกร</label>
              <asp:DropDownList ID="ddlPerson" runat="server" CssClass="form-control select2"></asp:DropDownList>    
          </div>
        </div>

                    <div class="col-md-2">
          <div class="form-group">
            <label>เวลาให้บริการ(นาที)</label>
              <asp:TextBox ID="txtTime" runat="server" CssClass="form-control text-center" ></asp:TextBox> 
          </div>
        </div>



      </div>
                  <div class="row">
           <div class="col-md-1">
          <div class="form-group">
            <label>ไข้ ( ํc)</label>
              <asp:TextBox ID="txtTemp" runat="server" placeholder="" CssClass="form-control text-center"></asp:TextBox>
          </div>
        </div>
                         <div class="col-md-1">
          <div class="form-group">
            <label>O2Sat</label>
              <asp:TextBox ID="txtO2sat" runat="server" placeholder="" CssClass="form-control text-center"></asp:TextBox>
          </div>
        </div>

        <div class="col-md-2">
          <div class="form-group">
                    <label>ไอ</label>
               <asp:RadioButtonList ID="optCough" runat="server" RepeatDirection="Horizontal" >
                  <asp:ListItem Value="Y">ใช่</asp:ListItem>
                    <asp:ListItem Value="N">ไม่ใช่</asp:ListItem> 
              </asp:RadioButtonList>
          </div>
        </div>      
                        <div class="col-md-2">
          <div class="form-group">
                    <label>หายใจไม่สะดวก</label>
               <asp:RadioButtonList ID="optBreath" runat="server" RepeatDirection="Horizontal" >
                  <asp:ListItem Value="Y">ใช่</asp:ListItem>
                    <asp:ListItem Value="N">ไม่ใช่</asp:ListItem> 
              </asp:RadioButtonList>
          </div>
        </div>     
  <div class="col-md-2">
          <div class="form-group">
                    <label>ท้องเสีย</label>
               <asp:RadioButtonList ID="optdiarrhea" runat="server" RepeatDirection="Horizontal" >
                  <asp:ListItem Value="Y">ใช่</asp:ListItem>
                    <asp:ListItem Value="N">ไม่ใช่</asp:ListItem> 
              </asp:RadioButtonList>
          </div>
        </div>     
 
  <div class="col-md-2">
          <div class="form-group">
                    <label>จมูกไม่ได้กลิ่น</label>
               <asp:RadioButtonList ID="optSmell" runat="server" RepeatDirection="Horizontal" >
                  <asp:ListItem Value="Y">ใช่</asp:ListItem>
                    <asp:ListItem Value="N">ไม่ใช่</asp:ListItem> 
              </asp:RadioButtonList>
          </div>
        </div>     
  <div class="col-md-2">
          <div class="form-group">
                    <label>ลิ้นไม่รับรส</label>
               <asp:RadioButtonList ID="optTast" runat="server" RepeatDirection="Horizontal" >
                  <asp:ListItem Value="Y">ใช่</asp:ListItem>
                    <asp:ListItem Value="N">ไม่ใช่</asp:ListItem> 
              </asp:RadioButtonList>
          </div>
        </div>     
                  
                    
      </div>
                  <div class="row">  
                      <div class="col-md-6">
          <div class="form-group">
                    <label>อื่นๆ</label>
                <asp:TextBox ID="txtOtherSymp" runat="server" CssClass="form-control"  ></asp:TextBox>
          </div>
        </div>     

      </div>

            <div class="row">
                  <div class="col-md-6">
          <div class="form-group">
                    <label>พบปัญหา/อาการที่สำคัญ</label>
                <asp:TextBox ID="txtProblem" runat="server" CssClass="form-control" Height="50px" TextMode="MultiLine"  ></asp:TextBox>
          </div>
        </div>     
                  <div class="col-md-6">
          <div class="form-group">
                    <label>คำแนะนำ/แก้ไข</label>
                <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control" Height="50px" TextMode="MultiLine"  ></asp:TextBox>
          </div>
        </div>     
            </div>

  <table align="center" width="100%">                  
                <tr>
                    <td>ID:<asp:Label ID="lblUID" runat="server"></asp:Label>
                    </td>
                    <td align="center" >
                        <asp:Button ID="cmdSaveVisit" runat="server" CssClass="buttonSave" Text="บันทึกการติดตามอาการ" />
                        &nbsp;<asp:Button ID="cmdCancel" runat="server" CssClass="buttonCancle" Text="ยกเลิก" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                       <asp:Panel ID="pnAlert" runat="server">
                 <div class="alert alert-danger alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">&times;</a>
                
                <h4><i class="icon fa fa-ban"></i> Warning!</h4>
                    <p><asp:Label ID="lblAlert" runat="server"></asp:Label></p>
              </div>
                   </asp:Panel>
                          <asp:Panel ID="pnSuccess" runat="server" Visible="false">  
            <div class="alert alert-success fade in alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">&times;</a>
  <strong>Success!</strong> บันทึกข้อมูลเรียบร้อย.
</div>
    
                     </asp:Panel>   
</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>                                   
                                <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%" CssClass="table table-hover">
                                    <RowStyle BackColor="White" VerticalAlign="Top" />
                                    <columns>
                                        <asp:TemplateField HeaderText="Edit">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEdit" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID")%>' CssClass="gridbutton" ImageUrl="images/icon-edit.png" />
                                            </ItemTemplate>
                                            <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Del">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgDel" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' CssClass="gridbutton" ImageUrl="images/delete.png" />
                                            </ItemTemplate>
                                            <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="SEQNO" HeaderText="ครั้งที่">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <itemstyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ServiceDtt" HeaderText="วันที่">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Temperature" HeaderText="ไข้" >
                                          <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                          <asp:BoundField DataField="O2Sat" HeaderText="O2Sat" >

                                        <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>

                                        <asp:TemplateField HeaderText="ไอ">
                                            <ItemTemplate>
                                                <asp:Image ID="imgCough" runat="server" ImageUrl="images/icon-ok.png" Visible='<%# ConvertYesNo2Boolean(DataBinder.Eval(Container.DataItem, "Cough")) %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="หายใจไม่สะดวก">
                                            <ItemTemplate>
                                                <asp:Image ID="Image1" runat="server" ImageUrl="images/icon-ok.png" Visible='<%# ConvertYesNo2Boolean(DataBinder.Eval(Container.DataItem, "Breathe")) %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ท้องเสีย">
                                            <ItemTemplate>
                                                <asp:Image ID="Image2" runat="server" ImageUrl="images/icon-ok.png" Visible='<%# ConvertYesNo2Boolean(DataBinder.Eval(Container.DataItem, "Diarrhea")) %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ไม่ได้กลิ่น">
                                            <ItemTemplate>
                                                <asp:Image ID="Image3" runat="server" ImageUrl="images/icon-ok.png" Visible='<%# ConvertYesNo2Boolean(DataBinder.Eval(Container.DataItem, "Smelless")) %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ลิ้นไม่รับรส">
                                            <ItemTemplate>
                                                <asp:Image ID="Image4" runat="server" ImageUrl="images/icon-ok.png" Visible='<%# ConvertYesNo2Boolean(DataBinder.Eval(Container.DataItem, "Tasteless")) %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="OtherFactor" HeaderText="อื่นๆ" />
                                        <asp:BoundField DataField="Problem" HeaderText="ปัญหาที่พบ" />
                                        <asp:BoundField DataField="Recommend" HeaderText="คำแนะนำ" />
                                    </columns>
                                    <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <AlternatingRowStyle BackColor="#F7F7F7" />
                                </asp:GridView>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="cmdSaveVisit" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
       </div>
        <div class="box-footer">
          
        </div>
  
      </div>             
</asp:Panel>
     <asp:Panel ID="pnResult" runat="server">
    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">สรุปผลการติดตาม</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">  
             <div class="row">
           <div class="col-md-5">
          <div class="form-group">
            <label>สรุปผลการติดตาม</label>
               <asp:RadioButtonList ID="optFinalResult" runat="server" RepeatDirection="Horizontal" >
                  <asp:ListItem Value="Y">หายปกติ</asp:ListItem>
                    <asp:ListItem Value="R">Refer เข้า รพ.</asp:ListItem> 
                       <asp:ListItem Value="D">เสียชีวิต</asp:ListItem>
                       <asp:ListItem Value="U">ตามไม่ได้</asp:ListItem>
                       <asp:ListItem Value="O">อื่นๆ</asp:ListItem>
              </asp:RadioButtonList>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
                    <label>ระบุเหตุผล</label>
               <asp:TextBox ID="txtRemarkResult" runat="server" placeholder="" CssClass="form-control"></asp:TextBox>
          </div>
        </div>  
                  <div class="col-md-2">
          <div class="form-group">
                    <label>วันที่ตรวจ ATK ซ้ำ</label>
              <asp:TextBox ID="txtATKDate" runat="server" placeholder="" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
          </div>
        </div> 

        <div class="col-md-2">
          <div class="form-group">
                    <label>ผลตรวจ ATK ซ้ำ</label>
             <asp:RadioButtonList ID="optATK" runat="server" RepeatDirection="Horizontal" >
                  <asp:ListItem Value="Y">บวก</asp:ListItem>
                    <asp:ListItem Value="N">ลบ</asp:ListItem>  
              </asp:RadioButtonList>
          </div>
        </div>  
</div>
 <h5 class="text-blue">กรณี ผป.ที่สูบบุหรี่  เมื่อครบเวลาการติดตาม อาการ COVID</h5>
    <div class="row">
       
             <div class="col-md-6">
          <div class="form-group">
                    <label>การสูบบุหรี่</label>

               <table>
                                    <tr>
                                        <td>
                                            <asp:RadioButtonList ID="optSmokeResult" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="สูบลดลง">สูบลดลง</asp:ListItem>
                                                <asp:ListItem Value="สูบเท่าเดิม">สูบเท่าเดิม</asp:ListItem>
                                                <asp:ListItem Value="สูบเพิ่มขึ้น">สูบเพิ่มขึ้น</asp:ListItem>
                                                <asp:ListItem Value="เลิกสูบแล้ว">เลิกสูบ</asp:ListItem>
                                                <asp:ListItem Value="อื่นๆ">อื่นๆ</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>ระบุ</td>
                                        <td>
                                            <asp:TextBox ID="txtSmokeOther" runat="server" CssClass="form-control"  placeholder="ระบุ" Width="150px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <table>
                                    <tr>
                                        <td>จาก</td>
                                        <td>
                                            <asp:TextBox ID="txtBegin" runat="server" cssclass="text-center"></asp:TextBox>
                                        </td>
                                        <td>เป็น</td>
                                        <td>
                                            <asp:TextBox ID="txtEnd" runat="server" cssclass="text-center"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>

             
          </div>
        </div>         
             <div class="col-md-6">
          <div class="form-group">
                    <label>กรณที่ ผป. ยังสูบบุหรี่อยู่มีการให้บริการเลิกบุหรี่ ( A1=A5 ) ต่อหรือไม่    </label>
  <asp:RadioButtonList ID="optIsSmokingService" runat="server" RepeatDirection="Horizontal" >
                  <asp:ListItem Value="Y">มี</asp:ListItem>
                    <asp:ListItem Value="N">ไม่มี</asp:ListItem>  
              </asp:RadioButtonList>
               <asp:TextBox ID="txtServiceReason" runat="server" placeholder="เพราะ (ระบุ)" CssClass="form-control"></asp:TextBox>
                </div>
        </div>  
          </div> 
         <div class="row">
                  <div class="col-md-6">
          <div class="form-group">
                    <label>มีการติดตามให้บริการอื่นๆอีกหรือไม่ </label>
   <asp:CheckBox ID="chkA" runat="server" Text="การเลิกบุหรี่" />
                   <asp:CheckBox ID="chkMTM" runat="server" Text="การติดตามการใช้ยา  / MTM" />  
              <asp:CheckBox ID="chkF" runat="server" Text="การคัดกรองอื่นๆ " />
               <asp:TextBox ID="txtFormOther" runat="server" placeholder="ระบุ" CssClass="form-control"></asp:TextBox>    

          </div>
        </div>  

                 <div class="col-md-6">
          <div class="form-group">
                    <label>สถานะ</label>
   <asp:CheckBox ID="chkStatus" runat="server" Text="จบการทำงาน" />
          </div>
        </div>  

                </div>
           </div>
        </div>
     
        </asp:Panel>

      <div align="center" >    
         <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="100px" />
    </div>    <br />


</section>
    
</asp:Content>