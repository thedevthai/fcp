﻿Imports System.Data
Imports System.Data.SqlClient
Public Class FormList
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlType As New ServiceTypeController
    Dim objUser As New UserController
    Dim ctlOrder As New OrderController
    Dim ctlSmk As New SmokingController
    Dim svTypeID As String 

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then

            lblNo.Visible = False
            lblNo2.Visible = False
            lblNoComplete.Visible = False
            ddlYear.SelectedValue = DisplayYear(Today.Date)

            LoadProvinceToDDL()

            LoadActivityTypeToDDL()

            grdData.PageIndex = 0
            grdDataStep2.PageIndex = 0
            grdComplete.PageIndex = 0
            LoadActivityListToGrid()
            LoadActivityListStep2ToGrid()
            LoadActivityListCompleteToGrid()

            SumCount()

            If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
                grdData.Columns(2).Visible = False
                grdDataStep2.Columns(2).Visible = False
                grdComplete.Columns(2).Visible = False

                lblProv.Visible = False
                ddlProvinceID.Visible = False

            Else
                grdData.Columns(2).Visible = True
                grdDataStep2.Columns(2).Visible = True
                grdComplete.Columns(2).Visible = True

                lblProv.Visible = True
                ddlProvinceID.Visible = True

            End If
        End If
    End Sub
    Protected Function DateText(ByVal input As Date) As String
        Dim dStr As String
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/0544", "", input.ToString("dd/MM/yyyy"))
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/0001", "", input.ToString("dd/MM/yyyy"))
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/0544", "", input.ToString("dd/MM/yyyy"))
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/2443", "", input.ToString("dd/MM/yyyy"))
        Return dStr
    End Function
    Private Sub LoadProvinceToDDL()

        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            dt = ctlOrder.Province_GetInGroup(Request.Cookies("RPTGRP").Value)
        Else
            dt = ctlOrder.Province_GetInLocation
        End If


        ddlProvinceID.Items.Clear()
        If dt.Rows.Count > 0 Then
            With ddlProvinceID
                .Enabled = True
                .Items.Add("---ทั้งหมด---")
                If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
                    .Items(0).Value = Request.Cookies("RPTGRP").Value
                Else
                    .Items(0).Value = "0"
                End If

                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(String.Concat(dt.Rows(i)("ProvinceName")))
                    .Items(i + 1).Value =String.Concat( dt.Rows(i)("ProvinceID"))
                Next
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadActivityListToGrid()

        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            dt = ctlOrder.ServiceOrder_GetByStatus(Request.Cookies("LocationID").Value, ddlActivity.SelectedValue, Trim(txtSearch.Text), 1, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
        ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            dt = ctlOrder.ServiceOrder_GetByProvinceGroup(Request.Cookies("RPTGRP").Value, ddlActivity.SelectedValue, Trim(txtSearch.Text), 1, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
        ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isProjectManager Then
            dt = ctlOrder.ServiceOrder_GetByProjectID(Request.Cookies("PRJMNG").Value, ddlActivity.SelectedValue, Trim(txtSearch.Text), 1, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
        Else
            dt = ctlOrder.ServiceOrder_GetByStatus("", ddlActivity.SelectedValue, Trim(txtSearch.Text), 1, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
        End If

        If dt.Rows.Count > 0 Then
            lblCount1.Text = dt.Rows.Count
            lblNo.Visible = False
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                Try

                    Dim nrow As Integer = dt.Rows.Count
                    Dim n05Count As Integer = 0

                    If .PageCount > 1 Then
                        If .PageIndex > 0 Then
                            If (.PageSize) < nrow - (.PageSize * .PageIndex) Then
                                For i = 0 To .PageSize - 1
                                    ' .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                    .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows((.PageSize * .PageIndex) + i)("ServiceDate"))
                                    .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows((.PageSize * .PageIndex) + i)("Gender"))

                                    ' Dim lnkF As LinkButton = .Rows(i).Cells(8).FindControl("lnkFollow")
                                    Dim lnkE1 As LinkButton = .Rows(i).Cells(8).FindControl("lnkEdu1")
                                    Dim lnkE2 As LinkButton = .Rows(i).Cells(8).FindControl("lnkEdu2")

                                    lnkE1.Visible = False
                                    lnkE2.Visible = False
                                    '  lnkF.Visible = False



                                    Select Case DBNull2Str(dt.Rows((.PageSize * .PageIndex) + i)("ServiceTypeID"))
                                        Case FORM_TYPE_ID_F01
                                            lnkE1.Visible = True
                                            lnkE2.Visible = True
                                            If DBNull2Zero(dt.Rows((.PageSize * .PageIndex) + i)("EducateCount")) = 1 Then
                                                lnkE1.Visible = False
                                            ElseIf DBNull2Zero(dt.Rows((.PageSize * .PageIndex) + i)("EducateCount")) >= 2 Then
                                                lnkE1.Visible = False
                                                lnkE2.Visible = False
                                            Else
                                                lnkE2.Visible = False
                                            End If
                                        Case FORM_TYPE_ID_F02
                                            lnkE1.Visible = False
                                            lnkE2.Visible = True
                                            If DBNull2Zero(dt.Rows((.PageSize * .PageIndex) + i)("EducateCount")) >= 2 Then
                                                lnkE2.Visible = False
                                            Else
                                                lnkE2.Visible = False
                                            End If

                                            'Case FORM_TYPE_ID_F11
                                            '    If DBNull2Zero(dt.Rows((.PageSize * .PageIndex) + i)("isFollow")) > 0 Then
                                            '        lnkF.Visible = False
                                            '    Else
                                            '        lnkF.Visible = True
                                            '    End If

                                    End Select


                                Next
                            Else
                                For i = 0 To nrow - (.PageSize * .PageIndex) - 1
                                    '.Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                    .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows((.PageSize * .PageIndex) + i)("ServiceDate"))
                                    .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows((.PageSize * .PageIndex) + i)("Gender"))

                                    '  Dim lnkF As LinkButton = .Rows(i).Cells(8).FindControl("lnkFollow")
                                    Dim lnkE1 As LinkButton = .Rows(i).Cells(8).FindControl("lnkEdu1")
                                    Dim lnkE2 As LinkButton = .Rows(i).Cells(8).FindControl("lnkEdu2")


                                    lnkE1.Visible = False
                                    lnkE2.Visible = False

                                    '   lnkF.Visible = False
                                    Select Case DBNull2Str(dt.Rows((.PageSize * .PageIndex) + i)("ServiceTypeID"))
                                        Case FORM_TYPE_ID_F01
                                            lnkE1.Visible = True
                                            lnkE2.Visible = True
                                            If DBNull2Zero(dt.Rows((.PageSize * .PageIndex) + i)("EducateCount")) = 1 Then
                                                lnkE1.Visible = False
                                            ElseIf DBNull2Zero(dt.Rows((.PageSize * .PageIndex) + i)("EducateCount")) >= 2 Then
                                                lnkE1.Visible = False
                                                lnkE2.Visible = False
                                            Else
                                                lnkE2.Visible = False
                                            End If
                                        Case FORM_TYPE_ID_F02
                                            lnkE1.Visible = False
                                            lnkE2.Visible = True
                                            If DBNull2Zero(dt.Rows((.PageSize * .PageIndex) + i)("EducateCount")) >= 2 Then
                                                lnkE2.Visible = False
                                            Else
                                                lnkE2.Visible = False
                                            End If
                                            'Case FORM_TYPE_ID_F11
                                            '    If DBNull2Zero(dt.Rows((.PageSize * .PageIndex) + i)("isFollow")) > 0 Then
                                            '        lnkF.Visible = False
                                            '    Else
                                            '        lnkF.Visible = True
                                            '    End If

                                    End Select



                                Next
                            End If
                        Else
                            For i = 0 To .PageSize - 1
                                ' .Rows(i).Cells(0).Text = i + 1
                                .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows(i)("ServiceDate"))
                                .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows(i)("Gender"))

                                '  Dim lnkF As LinkButton = .Rows(i).Cells(8).FindControl("lnkFollow")
                                Dim lnkE1 As LinkButton = .Rows(i).Cells(8).FindControl("lnkEdu1")
                                Dim lnkE2 As LinkButton = .Rows(i).Cells(8).FindControl("lnkEdu2")


                                lnkE1.Visible = False
                                lnkE2.Visible = False
                                ' lnkF.Visible = False
                                Select Case DBNull2Str(dt.Rows(i)("ServiceTypeID"))
                                    Case FORM_TYPE_ID_F01
                                        lnkE1.Visible = True
                                        lnkE2.Visible = True
                                        If DBNull2Zero(dt.Rows(i)("EducateCount")) = 1 Then
                                            lnkE1.Visible = False
                                        ElseIf DBNull2Zero(dt.Rows(i)("EducateCount")) >= 2 Then
                                            lnkE1.Visible = False
                                            lnkE2.Visible = False
                                        Else
                                            lnkE2.Visible = False
                                        End If
                                    Case FORM_TYPE_ID_F02
                                        lnkE1.Visible = False
                                        lnkE2.Visible = True
                                        If DBNull2Zero(dt.Rows(i)("EducateCount")) >= 2 Then
                                            lnkE2.Visible = False
                                        Else
                                            lnkE2.Visible = False
                                        End If

                                        'Case FORM_TYPE_ID_F11
                                        '    If DBNull2Zero(dt.Rows(i)("isFollow")) > 0 Then
                                        '        lnkF.Visible = False
                                        '    Else
                                        '        lnkF.Visible = True                                  
                                End Select

                            Next
                        End If
                    Else
                        For i = 0 To nrow - 1
                            ' .Rows(i).Cells(0).Text = i + 1
                            .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows(i)("ServiceDate"))
                            .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows(i)("Gender"))

                            ' Dim lnkF As LinkButton = .Rows(i).Cells(8).FindControl("lnkFollow")
                            Dim lnkE1 As LinkButton = .Rows(i).Cells(8).FindControl("lnkEdu1")
                            Dim lnkE2 As LinkButton = .Rows(i).Cells(8).FindControl("lnkEdu2")


                            lnkE1.Visible = False
                            lnkE2.Visible = False
                            'lnkF.Visible = False
                            Select Case DBNull2Str(dt.Rows(i)("ServiceTypeID"))
                                Case FORM_TYPE_ID_F01
                                    lnkE1.Visible = True
                                    lnkE2.Visible = True
                                    If DBNull2Zero(dt.Rows(i)("EducateCount")) = 1 Then
                                        lnkE1.Visible = False
                                    ElseIf DBNull2Zero(dt.Rows(i)("EducateCount")) >= 2 Then
                                        lnkE1.Visible = False
                                        lnkE2.Visible = False
                                    Else
                                        lnkE2.Visible = False
                                    End If
                                Case FORM_TYPE_ID_F02
                                    lnkE1.Visible = False
                                    lnkE2.Visible = True
                                    If DBNull2Zero(dt.Rows(i)("EducateCount")) >= 2 Then
                                        lnkE2.Visible = False
                                    Else
                                        lnkE2.Visible = False
                                    End If

                                    'Case FORM_TYPE_ID_F11
                                    '    If DBNull2Zero(dt.Rows(i)("isFollow")) > 0 Then
                                    '        lnkF.Visible = False
                                    '    Else
                                    '        lnkF.Visible = True
                                    '    End If                               
                            End Select

                        Next
                    End If
                Catch ex As Exception
                    'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','" & ex.Message & "');", True)
                    DisplayMessage(Me.Page, ex.Message)
                End Try

            End With
        Else
            lblCount1.Text = 0
            lblNo.Visible = True
            grdData.Visible = False
            grdData.DataSource = Nothing
        End If

        dt = Nothing
    End Sub

    Private Sub LoadActivityListStep2ToGrid()

        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            dt = ctlOrder.ServiceOrder_GetByStatus(Request.Cookies("LocationID").Value, ddlActivity.SelectedValue, Trim(txtSearch.Text), 2, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
        ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            dt = ctlOrder.ServiceOrder_GetByProvinceGroup(Request.Cookies("RPTGRP").Value, ddlActivity.SelectedValue, Trim(txtSearch.Text), 2, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
        ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isProjectManager Then
            dt = ctlOrder.ServiceOrder_GetByProjectID(Request.Cookies("PRJMNG").Value, ddlActivity.SelectedValue, Trim(txtSearch.Text), 2, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
        Else
            dt = ctlOrder.ServiceOrder_GetByStatus("", ddlActivity.SelectedValue, Trim(txtSearch.Text), 2, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
        End If


        If dt.Rows.Count > 0 Then
            lblCount2.Text = dt.Rows.Count
            lblNo2.Visible = False
            With grdDataStep2
                .Visible = True
                .DataSource = dt
                .DataBind()

                Try
                    Dim nrow As Integer = dt.Rows.Count
                    Dim n05Count As Integer = 0
                    If .PageCount > 1 Then
                        If .PageIndex > 0 Then
                            If (.PageSize) < nrow - (.PageSize * .PageIndex) Then
                                For i = 0 To .PageSize - 1
                                    ' .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                    '.Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows((.PageSize * .PageIndex) + i)("CloseDate"))
                                    .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows((.PageSize * .PageIndex) + i)("Gender"))

                                    Dim lnkF As LinkButton = .Rows(i).Cells(8).FindControl("lnkFollow2")
                                    Dim lnkE1 As LinkButton = .Rows(i).Cells(8).FindControl("lnkEdu12")
                                    Dim lnkE2 As LinkButton = .Rows(i).Cells(8).FindControl("lnkEdu22")


                                    lnkE1.Visible = False
                                    lnkE2.Visible = False
                                    lnkF.Visible = False
                                    Select Case DBNull2Str(dt.Rows((.PageSize * .PageIndex) + i)("ServiceTypeID"))
                                        Case FORM_TYPE_ID_F01
                                            lnkE1.Visible = True
                                            lnkE2.Visible = True
                                            If DBNull2Zero(dt.Rows((.PageSize * .PageIndex) + i)("EducateCount")) = 1 Then
                                                lnkE1.Visible = False
                                            ElseIf DBNull2Zero(dt.Rows((.PageSize * .PageIndex) + i)("EducateCount")) >= 2 Then
                                                lnkE1.Visible = False
                                                lnkE2.Visible = False
                                            Else
                                                lnkE2.Visible = False
                                            End If
                                        Case FORM_TYPE_ID_F02
                                            lnkE1.Visible = False
                                            lnkE2.Visible = True
                                            If DBNull2Zero(dt.Rows((.PageSize * .PageIndex) + i)("EducateCount")) >= 2 Then
                                                lnkE2.Visible = False
                                            Else
                                                lnkE2.Visible = False
                                            End If

                                        Case FORM_TYPE_ID_F11
                                            If DBNull2Zero(dt.Rows((.PageSize * .PageIndex) + i)("isFollow")) > 0 Then
                                                lnkF.Visible = False
                                            Else
                                                lnkF.Visible = True
                                            End If

                                    End Select


                                Next
                            Else
                                For i = 0 To nrow - (.PageSize * .PageIndex) - 1
                                    '.Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                    ' .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows((.PageSize * .PageIndex) + i)("CloseDate"))
                                    .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows((.PageSize * .PageIndex) + i)("Gender"))

                                    Dim lnkF As LinkButton = .Rows(i).Cells(8).FindControl("lnkFollow2")
                                    Dim lnkE1 As LinkButton = .Rows(i).Cells(8).FindControl("lnkEdu12")
                                    Dim lnkE2 As LinkButton = .Rows(i).Cells(8).FindControl("lnkEdu22")


                                    lnkE1.Visible = False
                                    lnkE2.Visible = False
                                    lnkF.Visible = False
                                    Select Case DBNull2Str(dt.Rows((.PageSize * .PageIndex) + i)("ServiceTypeID"))
                                        Case FORM_TYPE_ID_F01
                                            lnkE1.Visible = True
                                            lnkE2.Visible = True
                                            If DBNull2Zero(dt.Rows((.PageSize * .PageIndex) + i)("EducateCount")) = 1 Then
                                                lnkE1.Visible = False
                                            ElseIf DBNull2Zero(dt.Rows((.PageSize * .PageIndex) + i)("EducateCount")) >= 2 Then
                                                lnkE1.Visible = False
                                                lnkE2.Visible = False
                                            Else
                                                lnkE2.Visible = False
                                            End If
                                        Case FORM_TYPE_ID_F02
                                            lnkE1.Visible = False
                                            lnkE2.Visible = True
                                            If DBNull2Zero(dt.Rows((.PageSize * .PageIndex) + i)("EducateCount")) >= 2 Then
                                                lnkE2.Visible = False
                                            Else
                                                lnkE2.Visible = False
                                            End If
                                        Case FORM_TYPE_ID_F11
                                            If DBNull2Zero(dt.Rows((.PageSize * .PageIndex) + i)("isFollow")) > 0 Then
                                                lnkF.Visible = False
                                            Else
                                                lnkF.Visible = True
                                            End If

                                    End Select

                                Next
                            End If
                        Else
                            For i = 0 To .PageSize - 1
                                ' .Rows(i).Cells(0).Text = i + 1
                                ' .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows(i)("ServiceDate"))
                                .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows(i)("Gender"))

                                Dim lnkF As LinkButton = .Rows(i).Cells(8).FindControl("lnkFollow2")
                                Dim lnkE1 As LinkButton = .Rows(i).Cells(8).FindControl("lnkEdu12")
                                Dim lnkE2 As LinkButton = .Rows(i).Cells(8).FindControl("lnkEdu22")


                                lnkE1.Visible = False
                                lnkE2.Visible = False
                                lnkF.Visible = False
                                Select Case DBNull2Str(dt.Rows(i)("ServiceTypeID"))
                                    Case FORM_TYPE_ID_F01
                                        lnkE1.Visible = True
                                        lnkE2.Visible = True
                                        If DBNull2Zero(dt.Rows(i)("EducateCount")) = 1 Then
                                            lnkE1.Visible = False
                                        ElseIf DBNull2Zero(dt.Rows(i)("EducateCount")) >= 2 Then
                                            lnkE1.Visible = False
                                            lnkE2.Visible = False
                                        Else
                                            lnkE2.Visible = False
                                        End If
                                    Case FORM_TYPE_ID_F02
                                        lnkE1.Visible = False
                                        lnkE2.Visible = True
                                        If DBNull2Zero(dt.Rows(i)("EducateCount")) >= 2 Then
                                            lnkE2.Visible = False
                                        Else
                                            lnkE2.Visible = False
                                        End If

                                    Case FORM_TYPE_ID_F11
                                        If DBNull2Zero(dt.Rows(i)("isFollow")) > 0 Then
                                            lnkF.Visible = False
                                        Else
                                            lnkF.Visible = True
                                        End If

                                End Select

                            Next
                        End If
                    Else
                        For i = 0 To nrow - 1
                            ' .Rows(i).Cells(0).Text = i + 1
                            ' .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows(i)("ServiceDate"))
                            .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows(i)("Gender"))

                            Dim lnkF As LinkButton = .Rows(i).Cells(8).FindControl("lnkFollow2")
                            Dim lnkE1 As LinkButton = .Rows(i).Cells(8).FindControl("lnkEdu12")
                            Dim lnkE2 As LinkButton = .Rows(i).Cells(8).FindControl("lnkEdu22")


                            lnkE1.Visible = False
                            lnkE2.Visible = False
                            lnkF.Visible = False
                            Select Case DBNull2Str(dt.Rows(i)("ServiceTypeID"))
                                Case FORM_TYPE_ID_F01
                                    lnkE1.Visible = True
                                    lnkE2.Visible = True
                                    If DBNull2Zero(dt.Rows(i)("EducateCount")) = 1 Then
                                        lnkE1.Visible = False
                                    ElseIf DBNull2Zero(dt.Rows(i)("EducateCount")) >= 2 Then
                                        lnkE1.Visible = False
                                        lnkE2.Visible = False
                                    Else
                                        lnkE2.Visible = False
                                    End If
                                Case FORM_TYPE_ID_F02
                                    lnkE1.Visible = False
                                    lnkE2.Visible = True
                                    If DBNull2Zero(dt.Rows(i)("EducateCount")) >= 2 Then
                                        lnkE2.Visible = False
                                    Else
                                        lnkE2.Visible = False
                                    End If
                                Case FORM_TYPE_ID_F11
                                    If DBNull2Zero(dt.Rows(i)("isFollow")) > 0 Then
                                        lnkF.Visible = False
                                    Else
                                        lnkF.Visible = True
                                    End If

                            End Select

                        Next
                    End If
                Catch ex As Exception
                    'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','" & ex.Message & "');", True)
                    DisplayMessage(Me.Page, ex.Message)
                End Try

               
            End With
        Else
            lblCount2.Text = 0
            lblNo2.Visible = True
            grdDataStep2.Visible = False
            grdDataStep2.DataSource = Nothing
        End If

        dt = Nothing
    End Sub

    Private Sub LoadActivityListCompleteToGrid()

        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            dt = ctlOrder.ServiceOrder_GetByStatus(Request.Cookies("LocationID").Value, ddlActivity.SelectedValue, Trim(txtSearch.Text), 3, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
        ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            dt = ctlOrder.ServiceOrder_GetByProvinceGroup(Request.Cookies("RPTGRP").Value, ddlActivity.SelectedValue, Trim(txtSearch.Text), 3, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
        ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isProjectManager Then
            dt = ctlOrder.ServiceOrder_GetByProjectID(Request.Cookies("PRJMNG").Value, ddlActivity.SelectedValue, Trim(txtSearch.Text), 3, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
        Else
            dt = ctlOrder.ServiceOrder_GetByStatus("", ddlActivity.SelectedValue, Trim(txtSearch.Text), 3, ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))
        End If


        If dt.Rows.Count > 0 Then
            lblCount3.Text = dt.Rows.Count
            lblNoComplete.Visible = False
            With grdComplete
                .Visible = True
                .DataSource = dt
                .DataBind()

                Try

                    Dim nrow As Integer = dt.Rows.Count
                    If .PageCount > 1 Then
                        If .PageIndex > 0 Then
                            If (.PageSize) < nrow - (.PageSize * .PageIndex) Then
                                For i = 0 To .PageSize - 1
                                    ' .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                    .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows((.PageSize * .PageIndex) + i)("PayDate"))
                                    .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows((.PageSize * .PageIndex) + i)("Gender"))
  
                                Next
                            Else
                                For i = 0 To nrow - (.PageSize * .PageIndex) - 1
                                    '.Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                    .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows((.PageSize * .PageIndex) + i)("PayDate"))
                                    .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows((.PageSize * .PageIndex) + i)("Gender")) 
                                Next
                            End If
                        Else
                            For i = 0 To .PageSize - 1
                                ' .Rows(i).Cells(0).Text = i + 1
                                .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows(i)("PayDate"))
                                .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows(i)("Gender")) 
                            Next
                        End If
                    Else
                        For i = 0 To nrow - 1
                            ' .Rows(i).Cells(0).Text = i + 1
                            .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows(i)("PayDate"))
                            .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows(i)("Gender")) 
                        Next
                    End If 
                Catch ex As Exception

                End Try  
            End With
        Else
            lblCount3.Text = 0
            lblNoComplete.Visible = True
            grdComplete.Visible = False
            grdComplete.DataSource = Nothing
        End If

        dt = Nothing
    End Sub

    Private Sub LoadActivityTypeToDDL()

        'If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
        '    dt = ctlType.ServiceType_GetByLocationID(Request.Cookies("LocationID").Value)
        'ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isProjectManager Then
        '    dt = ctlType.ServiceType_GetByProjectID(PROJECT_F)
        'Else
        dt = ctlType.ServiceType_GetByProjectID(PROJECT_F)
        'End If

        If dt.Rows.Count > 0 Then
            ddlActivity.Items.Clear()
            ddlActivity.Items.Add("---ทั้งหมด---")
            ddlActivity.Items(0).Value = "0"
            For i = 0 To dt.Rows.Count - 1
                With ddlActivity
                    .Items.Add("" & dt.Rows(i)("ServiceTypeID") & " : " & dt.Rows(i)("ServiceName"))
                    .Items(i + 1).Value = dt.Rows(i)("ServiceTypeID")
                End With
            Next
        End If
        dt = Nothing
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadActivityListToGrid()
    End Sub
  
    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand

        Dim sArg(2), sStr() As String
        Dim iSID As Long
        Dim iPID As Integer
        Dim iSTID As String
        sArg(0) = ""
        sArg(1) = ""
        sArg(2) = ""

        sStr = Split(DBNull2Str(e.CommandArgument), "|")
        For i = 0 To sStr.Length - 1
            sArg(i) = sStr(i)
        Next
        iSID = StrNull2Long(sArg(0))
        iPID = StrNull2Zero(sArg(1))
        iSTID = sArg(2)
        Session("patientid") = ctlOrder.ServiceOrder_GetPatientIDByItemID(iSID, iPID)

        If TypeOf e.CommandSource Is WebControls.LinkButton Then
            Dim ButtonPressed As WebControls.LinkButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "lnkEdu1"
                    Response.Redirect("F02.aspx?t=new&seq=1&fid=" & iSID)
                Case "lnkEdu2"
                    Response.Redirect("F03.aspx?t=new&seq=2&fid=" & iSID)
                Case "lnkFollow"
                    Response.Redirect("F11_Follow.aspx?t=new&fid=" & iSID)
                Case "lnkA04"
                    Response.Redirect("A4.aspx?t=new&fid=" & iSID)
                Case "lnkA05"
                    Response.Redirect("A5.aspx?t=new&fid=" & iSID)
            End Select
        End If

        Select Case iSTID
            Case "F18", "F19"
                svTypeID = iSTID
            Case Else
                svTypeID = ctlOrder.Order_GetTypeIDByItemID(iSID, iPID)
        End Select

        'svSeqNo = ctlOrder.Order_GetSEQByItemID(isid)


        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"

                    Select Case svTypeID
                        Case FORM_TYPE_ID_F11F
                            Response.Redirect("F11_Follow.aspx?t=edit&fid=" & iSID)
                        Case FORM_TYPE_ID_F02
                            Response.Redirect("F02.aspx?t=edit&fid=" & iSID)
                        Case FORM_TYPE_ID_F03
                            Response.Redirect("F03.aspx?t=edit&fid=" & iSID)
                        Case FORM_TYPE_ID_A4F
                            Response.Redirect("A4.aspx?t=edit&fid=" & iSID)
                        Case FORM_TYPE_ID_A05
                            Response.Redirect("A5.aspx?t=edit&fid=" & iSID)
                        Case Else
                            Response.Redirect("" & svTypeID & ".aspx?t=edit&fid=" & iSID)
                    End Select

                Case "imgDel"
                    Dim RID As Integer

                    Select Case svTypeID
                        Case FORM_TYPE_ID_F01
                            ctlOrder.Order_f01DeleteByRefID(iSID)
                        Case FORM_TYPE_ID_F02
                            ctlOrder.F01_UpdateEducateCountByDelete(iSID, 0, Request.Cookies("username").Value)
                            ctlOrder.Order_f02DeleteByRefID(iSID)
                        Case FORM_TYPE_ID_F03
                            ctlOrder.F01_UpdateEducateCountByDelete(iSID, 1, Request.Cookies("username").Value)
                        Case FORM_TYPE_ID_F11F
                            RID = ctlOrder.F11_GetRefID(iSID)
                            ctlOrder.F11_UpdateFollowStatus(RID, 0, Request.Cookies("username").Value)

                    End Select

                    If Left(svTypeID, 1) = "A" Then
                        ctlSmk.Smoking_Order_Delete(iSID)
                        objUser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_DEL, "Smoking", "ลบรายการกิจกรรม :" & svTypeID, iSID)
                    Else
                        ctlOrder.Order_Delete(iSID, svTypeID)
                        objUser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_DEL, "ServiceOrder", "ลบรายการกิจกรรม :" & svTypeID, iSID)
                    End If

                    DisplayMessage(Me.Page, "ลบข้อมูลเรียบร้อย")

                    'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ลบข้อมูลเรียบร้อย');", True)
                    grdData.PageIndex = 0
                    LoadActivityListToGrid()
                    'Else
                    '  ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    'End If
            End Select
        End If

    End Sub

    Private Sub grdData_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            'e.Row.Cells(0).Text = e.Row.RowIndex + 1
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(9).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub


    Protected Sub ddlActivity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlActivity.SelectedIndexChanged
        grdData.PageIndex = 0
        LoadActivityListToGrid()

        grdDataStep2.PageIndex = 0
        LoadActivityListStep2ToGrid()

        grdComplete.PageIndex = 0
        LoadActivityListCompleteToGrid()

        SumCount()
    End Sub


    Private Sub grdComplete_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdComplete.PageIndexChanging
        grdComplete.PageIndex = e.NewPageIndex
        LoadActivityListCompleteToGrid()
    End Sub
    Private Sub grdComplete_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdComplete.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Private Sub grdDataStep2_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdDataStep2.PageIndexChanging
        grdDataStep2.PageIndex = e.NewPageIndex
        LoadActivityListStep2ToGrid()
    End Sub
    Private Sub grdDataStep2_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdDataStep2.RowCommand


        Dim sArg(2), sStr() As String
        Dim iSID As Long
        Dim iPID As Integer
        Dim iSTID As String
        sArg(0) = ""
        sArg(1) = ""
        sArg(2) = ""
        sStr = Split(DBNull2Str(e.CommandArgument), "|")
        For i = 0 To sStr.Length - 1
            sArg(i) = sStr(i)
        Next
        iSID = StrNull2Long(sArg(0))
        iPID = StrNull2Zero(sArg(1))
        iSTID = sArg(2)
        Session("patientid") = ctlOrder.ServiceOrder_GetPatientIDByItemID(iSID, iPID)

        If TypeOf e.CommandSource Is WebControls.LinkButton Then
            Dim ButtonPressed As WebControls.LinkButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "lnkEdu12"
                    Response.Redirect("F02.aspx?t=new&fid=" & iSID)
                Case "lnkEdu22"
                    Response.Redirect("F03.aspx?t=new&fid=" & iSID)
                Case "lnkFollow2"
                    Response.Redirect("F11_Follow.aspx?t=new&fid=" & iSID)
                Case "lnkA042"
                    Response.Redirect("A4.aspx?t=new&fid=" & iSID)
                Case "lnkA052"
                    Response.Redirect("A5.aspx?t=new&fid=" & iSID)
            End Select
        End If

        Select Case iSTID
            Case "F18", "F19"
                svTypeID = iSTID
            Case Else
                svTypeID = ctlOrder.Order_GetTypeIDByItemID(iSID, iPID)
        End Select

        ' svSeqNo = ctlOrder.Order_GetSEQByItemID(isid)

        If svTypeID = "" Then
            svTypeID = iSTID
        End If

        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit2"

                    'If svTypeID = FORM_TYPE_ID_F11F Then
                    '    Response.Redirect("F11_Follow.aspx?t=edit&fid=" & isid)
                    'ElseIf svTypeID = FORM_TYPE_ID_F02 Then
                    '    Response.Redirect("F02.aspx?t=edit&fid=" & isid)
                    'ElseIf svTypeID = FORM_TYPE_ID_F03 Then
                    '    Response.Redirect("F03.aspx?t=edit&fid=" & isid)
                    'Else
                    '    Response.Redirect("" & svTypeID & ".aspx?fid=" & isid)
                    'End If

                    Select Case svTypeID
                        Case FORM_TYPE_ID_F11F
                            Response.Redirect("F11_Follow.aspx?t=edit&fid=" & iSID)
                        Case FORM_TYPE_ID_F02
                            Response.Redirect("F02.aspx?t=edit&fid=" & iSID)
                        Case FORM_TYPE_ID_F03
                            Response.Redirect("F03.aspx?t=edit&fid=" & iSID)
                        Case FORM_TYPE_ID_A4F
                            Response.Redirect("A4.aspx?t=edit&fid=" & iSID)
                        Case FORM_TYPE_ID_A05
                            Response.Redirect("A5.aspx?t=edit&fid=" & iSID)
                        Case Else
                            Response.Redirect("" & svTypeID & ".aspx?t=edit&fid=" & iSID)
                    End Select



                Case "imgDel2"
                    Dim RID As Integer
                    Select Case svTypeID
                        Case FORM_TYPE_ID_F01
                            ctlOrder.Order_F01DeleteByRefID(iSID)
                        Case FORM_TYPE_ID_F02
                            ctlOrder.F01_UpdateEducateCountByDelete(iSID, 0, Request.Cookies("username").Value)
                            ctlOrder.Order_F02DeleteByRefID(iSID)
                        Case FORM_TYPE_ID_F03
                            ctlOrder.F01_UpdateEducateCountByDelete(iSID, 1, Request.Cookies("username").Value)
                        Case FORM_TYPE_ID_F11F
                            RID = ctlOrder.F11_GetRefID(iSID)
                            ctlOrder.F11_UpdateFollowStatus(RID, 0, Request.Cookies("username").Value)
                    End Select


                    If Left(svTypeID, 1) = "A" Then
                        ctlSmk.Smoking_Order_Delete(iSID)
                        objUser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_DEL, "Smoking", "ลบรายการกิจกรรม :" & svTypeID, iSID)
                    Else
                        ctlOrder.Order_Delete(iSID, svTypeID)
                        objUser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_DEL, "ServiceOrder", "ลบรายการกิจกรรม :" & svTypeID, iSID)
                    End If

                    'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ลบข้อมูลเรียบร้อย');", True)
                    DisplayMessage(Me.Page, "ลบข้อมูลเรียบร้อย")
                    grdDataStep2.PageIndex = 0
                    LoadActivityListStep2ToGrid()

            End Select
        End If
    End Sub

    Private Sub grdDataStep2_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdDataStep2.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            'e.Row.Cells(0).Text = e.Row.RowIndex + 1
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(9).FindControl("imgDel2")
            imgD.Attributes.Add("onClick", scriptString)

        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Protected Sub ddlProvinceID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProvinceID.SelectedIndexChanged
        grdData.PageIndex = 0
        LoadActivityListToGrid()

        grdDataStep2.PageIndex = 0
        LoadActivityListStep2ToGrid()

        grdComplete.PageIndex = 0
        LoadActivityListCompleteToGrid()

        SumCount()
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        grdData.PageIndex = 0
        LoadActivityListToGrid()

        grdDataStep2.PageIndex = 0
        LoadActivityListStep2ToGrid()

        grdComplete.PageIndex = 0
        LoadActivityListCompleteToGrid()

        SumCount()
    End Sub

    Private Sub SumCount()
        lblCount.Text = StrNull2Zero(lblCount1.Text) + StrNull2Zero(lblCount2.Text) + StrNull2Zero(lblCount3.Text)
    End Sub

    Protected Sub cmdSearch_Click(sender As Object, e As EventArgs) Handles cmdSearch.Click
        grdData.PageIndex = 0
        LoadActivityListToGrid()

        grdDataStep2.PageIndex = 0
        LoadActivityListStep2ToGrid()

        grdComplete.PageIndex = 0
        LoadActivityListCompleteToGrid()

        SumCount()
    End Sub
End Class

