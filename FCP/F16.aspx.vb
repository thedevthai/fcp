﻿Imports System.Data
Imports System.Data.SqlClient

Public Class F16
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlLct As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlOrder As New OrderController
    Dim ctlKnw As New KnowledgeController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then

            If Request("acttype") = "view" Then
                cmdSave.Visible = False
                cmdClear.Visible = False
            Else
                cmdSave.Visible = True
                cmdClear.Visible = True
            End If

            txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)

            LoadFormData()

            LoadPharmacist(Request.Cookies("LocationID").Value)

        End If

        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        'txtCardID.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")


    End Sub
    Private Sub LoadPharmacist(ByVal LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With
            End If
        End If
        dtP = Nothing
    End Sub

    Private Sub LoadLocationData(LocationID As String)
        dt = ctlLct.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblLocationID.Text = .Item("LocationID")
                lblLocationName.Text = .Item("LocationName")
                lblLocationProvince.Text = .Item("ProvinceName")
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadFormData()
        Dim pYear As Integer
        If Not Request("fid") Is Nothing Then
            dt = ctlOrder.GetOrder_ByID(StrNull2Zero(Request("fid")))
        Else
            If Not Request("yid") Is Nothing Then
                pYear = CInt(Request("yid"))
            Else
                pYear = Year(ctlLct.GET_DATE_SERVER)
                If pYear < 2500 Then
                    pYear = pYear + 543
                End If

            End If
            dt = ctlOrder.LocationOrder_ByYear(pYear, FORM_TYPE_ID_F16, Request.Cookies("LocationID").Value, Session("patientid"))
        End If
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Session("patientid") = DBNull2Lng(.Item("PatientID"))
                lblID.Text = .Item("itemID")
                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                txtTime.Text = String.Concat(.Item("ServiceTime"))
                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))

                chkNHSO1.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO1")))
                chkNHSO2.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO2")))
                chkNHSO3.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO3")))
                chkNHSO4.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO4")))
                chkNHSO5.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO5")))
                chkNHSO6.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO6")))
                chkNHSO7.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO7")))
                chkNHSO8.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO8")))

                chkClose.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("Status")))

                optFollow.SelectedValue = DBNull2Zero(.Item("isFollow"))
                txtNextTime.Text = DBNull2Str(.Item("NextDate"))
                txtCheckDate.Text = DBNull2Str(.Item("FollowDateSave"))
                optChkResult.SelectedValue = DBNull2Zero(.Item("IsAbNormal"))
                txtRemark.Text = DBNull2Str(.Item("AbNormalRemark"))
                txtProMed.Text = DBNull2Str(.Item("MedicinceDesc"))
                If DBNull2Zero(.Item("Status")) >= 3 Then
                    cmdSave.Visible = False
                    cmdClear.Visible = False
                Else
                    If Request.Cookies("LocationID").Value <> String.Concat(.Item("LocationID")) Then
                        LoadLocationData(String.Concat(.Item("LocationID")))
                        If Request("t") = "edit" Then
                            cmdSave.Visible = True
                            cmdClear.Visible = True
                        Else
                            cmdSave.Visible = False
                            cmdClear.Visible = False
                        End If
                    Else
                        LoadLocationData(Request.Cookies("LocationID").Value)
                    End If
                End If


            End With
            Dim dtK As New DataTable

            dtK = ctlKnw.Knowledge_GetByCustomer(FORM_TYPE_ID_F16, StrNull2Zero(lblID.Text))

            If dtK.Rows.Count > 0 Then
                For i = 0 To dtK.Rows.Count - 1
                    Select Case dtK.Rows(i)("KnowledgeCode")
                        Case "P1601"
                            chkProblem1.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "P1602"
                            chkProblem2.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "P1603"
                            chkProblem3.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "P1604"
                            chkProblem4.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "P1605"
                            chkProblem5.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "P1606"
                            chkProblem6.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "P1607"
                            chkProblem7.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "P1608"
                            chkProblem8.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "P1609"
                            chkProblem9.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "P1610"
                            chkProblem10.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "P1611"
                            chkProblem11.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "P1612"
                            chkProblem12.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "P1613"
                            chkProblem13.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "P1614"
                            chkProblem14.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "P1615"
                            chkProblem15.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "P16RM"
                            txtProblemRemark.Text = DBNull2Str(dtK.Rows(i)("KnowledgeDescription"))
                        Case "E1601"
                            chkEdu1.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "E1602"
                            chkEdu2.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "E1603"
                            chkEdu3.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "E1604"
                            chkEdu4.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "E1605"
                            chkEdu5.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                        Case "E16OT"
                            chkEduOther.Checked = Decimal2Boolean(DBNull2Zero(dtK.Rows(i)("KnowledgeValue")))
                            txtEduOther.Text = DBNull2Str(dtK.Rows(i)("KnowledgeDescription"))
                        Case "E16OR"
                            txtEduRemark.Text = DBNull2Str(dtK.Rows(i)("KnowledgeDescription"))
                    End Select
                Next
            End If
            dtK = Nothing
        Else
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing
    End Sub
    Protected Sub cmdClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdClear.Click

        lblID.Text = ""
        txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)
        txtTime.Text = ""
        ddlPerson.SelectedIndex = 0
        chkClose.Checked = False


        optFollow.SelectedValue = 0
        txtNextTime.Text = ""
        txtCheckDate.Text = ""
        optChkResult.SelectedIndex = 0
        txtRemark.Text = ""

        chkProblem1.Checked = False
        chkProblem2.Checked = False
        chkProblem3.Checked = False
        chkProblem4.Checked = False
        chkProblem5.Checked = False
        chkProblem6.Checked = False
        chkProblem7.Checked = False
        chkProblem8.Checked = False
        chkProblem9.Checked = False
        chkProblem10.Checked = False
        chkProblem11.Checked = False
        chkProblem12.Checked = False
        chkProblem13.Checked = False
        chkProblem14.Checked = False
        chkProblem15.Checked = False
        txtProblemRemark.Text = False

        chkNHSO1.Checked = False
        chkNHSO2.Checked = False
        chkNHSO3.Checked = False
        chkNHSO4.Checked = False
        chkNHSO5.Checked = False
        chkNHSO6.Checked = False
        chkNHSO7.Checked = False
        chkNHSO8.Checked = False
        chkEdu1.Checked = False
        chkEdu2.Checked = False
        chkEdu3.Checked = False
        chkEdu4.Checked = False
        chkEdu5.Checked = False

        chkEduOther.Checked = False
        txtEduOther.Text = ""

    End Sub


    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSave.Click

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If


        If StrNull2Zero(txtTime.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนระยะเวลาในการให้บริการก่อน');", True)
            Exit Sub
        End If

        Dim objuser As New UserController
        Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))
        'Dim BirthDate As String = ConvertStrDate2DBString(txtBirthDate.Text)

        'Dim CustName As String
        ' CustName = Trim(txtFName.Text) & " " & Trim(txtLName.Text)

        Dim sDisease As String
        
        sDisease = ""

        If chkDisease1.Checked Then
            sDisease = "D01"
        End If
        If chkDisease2.Checked Then
            sDisease &= "|D02"
        End If
        If chkDisease3.Checked Then
            sDisease &= "|D03"
        End If
        If chkDisease4.Checked Then
            sDisease &= "|D04"
        End If
        If chkDisease8.Checked Then
            sDisease &= "|D08"
        End If
        If chkDisease9.Checked Then
            sDisease &= "|D09"
        End If
        If chkDisease10.Checked Then
            sDisease &= "|D10"
        End If



        If lblID.Text = "" Then 'Add new

            If ctlOrder.Order_ChkDupCustomer(FORM_TYPE_ID_F16, Session("patientid"), StrNull2Zero(Left(Str(ServiceDate), 4))) = True Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ผู้รับบริการรายนี้เคยได้รับบริการในกิจกรรมนี้แล้ว');", True)
                Exit Sub
            End If

            ctlOrder.F16_Add(lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F16, Session("patientid"), 0, sDisease, txtDiseaseOther.Text, txtProMed.Text, Boolean2Decimal(optFollow.SelectedValue), txtNextTime.Text, txtCheckDate.Text, StrNull2Zero(optChkResult.SelectedValue), txtRemark.Text, Boolean2Decimal(chkNHSO1.Checked), Boolean2Decimal(chkNHSO2.Checked), Boolean2Decimal(chkNHSO3.Checked), Boolean2Decimal(chkNHSO4.Checked), Boolean2Decimal(chkNHSO5.Checked), Boolean2Decimal(chkNHSO6.Checked), Boolean2Decimal(chkNHSO7.Checked), Boolean2Decimal(chkNHSO8.Checked), Convert2Status(chkClose.Checked), Request.Cookies("username").Value, CLng(ConvertDate2DBString(ctlLct.GET_DATE_SERVER)))

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Service_Order", "บันทึกเพิ่มกิจกรรมการค้นหาปัญหาจากการใช้ยา ( DRP )(F16):" & Session("patientname"), "F16")
        Else
            ctlOrder.F16_Update(StrNull2Zero(lblID.Text), lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F16, Session("patientid"), 0, sDisease, txtDiseaseOther.Text, txtProMed.Text, Boolean2Decimal(optFollow.SelectedValue), txtNextTime.Text, txtCheckDate.Text, StrNull2Zero(optChkResult.SelectedValue), txtRemark.Text, Boolean2Decimal(chkNHSO1.Checked), Boolean2Decimal(chkNHSO2.Checked), Boolean2Decimal(chkNHSO3.Checked), Boolean2Decimal(chkNHSO4.Checked), Boolean2Decimal(chkNHSO5.Checked), Boolean2Decimal(chkNHSO6.Checked), Boolean2Decimal(chkNHSO7.Checked), Boolean2Decimal(chkNHSO8.Checked), Convert2Status(chkClose.Checked), Request.Cookies("username").Value)

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Service_Order", "บันทึกแก้ไขกิจกรรมการค้นหาปัญหาจากการใช้ยา ( DRP )(F16) :" & Session("patientname"), "F16")
        End If

        Dim ServiceID As Integer = ctlOrder.ServiceOrder_GetID(lblLocationID.Text, FORM_TYPE_ID_F16, Session("patientid"), ServiceDate)


        AddKnowledgeItem(chkProblem1.Checked, ServiceID, FORM_TYPE_ID_F16, "P1601", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkProblem2.Checked, ServiceID, FORM_TYPE_ID_F16, "P1602", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkProblem3.Checked, ServiceID, FORM_TYPE_ID_F16, "P1603", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkProblem4.Checked, ServiceID, FORM_TYPE_ID_F16, "P1604", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkProblem5.Checked, ServiceID, FORM_TYPE_ID_F16, "P1605", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkProblem6.Checked, ServiceID, FORM_TYPE_ID_F16, "P1606", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkProblem7.Checked, ServiceID, FORM_TYPE_ID_F16, "P1607", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkProblem8.Checked, ServiceID, FORM_TYPE_ID_F16, "P1608", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkProblem9.Checked, ServiceID, FORM_TYPE_ID_F16, "P1609", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkProblem10.Checked, ServiceID, FORM_TYPE_ID_F16, "P1610", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkProblem11.Checked, ServiceID, FORM_TYPE_ID_F16, "P1611", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkProblem12.Checked, ServiceID, FORM_TYPE_ID_F16, "P1612", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkProblem13.Checked, ServiceID, FORM_TYPE_ID_F16, "P1613", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkProblem14.Checked, ServiceID, FORM_TYPE_ID_F16, "P1614", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkProblem15.Checked, ServiceID, FORM_TYPE_ID_F16, "P1615", "", 1, Request.Cookies("username").Value)
        If Trim(txtProblemRemark.Text) <> "" Then
            AddKnowledgeItem(1, ServiceID, FORM_TYPE_ID_F16, "P16RM", txtProblemRemark.Text, 2, Request.Cookies("username").Value)
        End If

        AddKnowledgeItem(chkEdu1.Checked, ServiceID, FORM_TYPE_ID_F16, "E1601", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkEdu2.Checked, ServiceID, FORM_TYPE_ID_F16, "E1602", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkEdu3.Checked, ServiceID, FORM_TYPE_ID_F16, "E1603", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkEdu4.Checked, ServiceID, FORM_TYPE_ID_F16, "E1604", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkEdu5.Checked, ServiceID, FORM_TYPE_ID_F16, "E1605", "", 1, Request.Cookies("username").Value)
        AddKnowledgeItem(chkEduOther.Checked, ServiceID, FORM_TYPE_ID_F16, "E16OT", txtEduOther.Text, 1, Request.Cookies("username").Value)

        If Trim(txtEduRemark.Text) <> "" Then
            AddKnowledgeItem(1, ServiceID, FORM_TYPE_ID_F16, "E16OR", txtEduRemark.Text, 2, Request.Cookies("username").Value)
        End If
        Response.Redirect("ResultPage.aspx")

        ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย")
    End Sub

    Private Sub AddKnowledgeItem(ByVal isSelect As Boolean, ByVal ServiceOrderID As Integer _
           , ByVal ServiceTypeID As String, ByVal KnowledgeCode As String, ByVal KnowledgeDescription As String, ByVal KNTYPEID As Integer, ByVal UpdBy As String)

        If isSelect Then
            ctlKnw.Knowledge_Save(ServiceOrderID, ServiceTypeID, KnowledgeCode, 1, KnowledgeDescription, KNTYPEID, UpdBy)
        End If

    End Sub
  
    Protected Sub chkDisease10_CheckedChanged(sender As Object, e As EventArgs) Handles chkDisease10.CheckedChanged
        If chkDisease10.Checked Then
            txtDiseaseOther.Visible = True
        Else
            txtDiseaseOther.Visible = False
        End If
    End Sub
End Class