﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="Locations.aspx.vb" Inherits=".Locations" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>ข้อมูลร้านยา
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
   
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลร้านยา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
                <table class="table table-responsive tbhead">
                    <tr>
<td width="200" align="left">รหัสร้านยา <asp:HiddenField ID="hdID" runat="server" /><asp:HiddenField ID="hdNewID" runat="server" /></td>
<td width="200" align="left" >
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                          <ContentTemplate>
                                                              <asp:TextBox ID="txtLocationID" runat="server" CssClass="form-control text-center" ReadOnly="True" BackColor="#D0E8FF">Auto ID</asp:TextBox>
                                                          </ContentTemplate>
                                                          <Triggers>
                                                              <asp:AsyncPostBackTrigger ControlID="ddlType" 
                                                                  EventName="SelectedIndexChanged" />
                                                              <asp:AsyncPostBackTrigger ControlID="ddlProvince" 
                                                                  EventName="SelectedIndexChanged" />
                                                          </Triggers>
                                                      </asp:UpdatePanel>        
    </td>   
                                              
                                                  <td align="right" >เลขที่ ขย.5</td>
                                                  <td align="left" ><asp:TextBox ID="txtLicenseNo" runat="server" CssClass="form-control text-center" ></asp:TextBox></td>
                                                    <td align="right" >รหัสหน่วยบริการ สปสช.</td>
                                                     <td align="left">
                                                         <asp:TextBox ID="txtNHSOCode" runat="server" CssClass="form-control text-center" ></asp:TextBox>
                                                    </td>
                                                </tr>
                    </table>


<table class="table table-responsive">


                                                <tr>
                                                    <td align="left"  width="200">กลุ่มร้านยา :</td>
                                                    <td align="left" >
                                                        <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="True" CssClass="form-control select2" >                                                        </asp:DropDownList>                                                    </td>
                                                  <td align="left" >จังหวัด : </td>
                                                    <td align="left" ><asp:DropDownList CssClass="form-control select2" ID="ddlProvince" runat="server" AutoPostBack="True"> </asp:DropDownList></td>
                                                </tr>

                                                <tr>
                                                  <td align="left" >ชื่อร้านยา/เภสัชกรอาสา/ชื่อ รพ.</td>
                                                  <td align="left" ><asp:TextBox ID="txtLocationName" runat="server" CssClass="form-control" ></asp:TextBox></td>
                                                    <td align="left" >ชื่อร้านยาสำหรับ API</td>
                                                  <td align="left" ><asp:TextBox ID="txtLocationName2" runat="server" CssClass="form-control" ></asp:TextBox></td>
                                                </tr>
                                              <tr>
                                                  <td align="left" >ประเภทร้านยา:</td>
                                                     <td align="left">  
                                                         <asp:DropDownList ID="ddlTypeShop" runat="server" CssClass="form-control select2">
                                                            <asp:ListItem Selected="True">ร้านยาคุณภาพ</asp:ListItem>
                                                            <asp:ListItem Value="ร้านยา GPP">ร้านยา GPP</asp:ListItem>
                                                            <asp:ListItem Value="ร้านยาเภสัชกร"></asp:ListItem>
                                                          <asp:ListItem Value="โรงพยาบาล"></asp:ListItem>
                                                             <asp:ListItem Value="รพสต."></asp:ListItem>
                                                          <asp:ListItem Value="เภสัชกร"></asp:ListItem>
                                                          <asp:ListItem Value="อื่นๆ"></asp:ListItem>
                                                                     </asp:DropDownList>                                                         
                                                    </td>
<td>ระบุ</td>
                                                                 <td>                                                    
                                                        <asp:TextBox ID="txtTypeName" runat="server" CssClass="form-control"></asp:TextBox>                                                    </td>

                                                  </tr>
                                                <tr>
                                                    <td align="left" valign="top" >ที่อยู่ :</td>
                                                    <td align="left" colspan="3" ><asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" CssClass="form-control" Height="50px"></asp:TextBox></td>
                                                       
                                                </tr>
                                                <tr> 
                                                     <td align="left" valign="bottom" >รหัสไปรษณีย์:</td> 
                                                    <td align="left" > <asp:TextBox ID="txtZipCode" runat="server" MaxLength="5"></asp:TextBox></td>
                                                    
                                                    <td align="left" valign="top" >ละติจูด,ลองติจูด</td>
                                                  
                                                   
                                                    <td align="left" valign="bottom" >
                                                        <small class="text-blue">ละติจูด/ลองติจูด ในรูปแบบองศาทศนิยมเท่านั้น และคั่นชุดข้อมูลด้วยคอมม่า (,)</small>
                                                        <asp:TextBox ID="txtLat" runat="server" CssClass="form-control"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td align="left" >เบอร์โทรเพื่อการประชาสัมพันธ์ร้าน :</td>
                                                    <td align="left" >
                                                        <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                                          <tr>
                                                            <td width="350"><asp:TextBox ID="txtTel" runat="server"  CssClass="form-control"></asp:TextBox></td>
                                                            <td width="80"  align="right">Line ID :</td>
                                                            <td><asp:TextBox ID="txtLineID" runat="server"  CssClass="form-control"></asp:TextBox></td>
                                                          </tr>
                                                        </table>
                                                    </td>
                                                    <td align="left" >E-mail :</td>
                                              <td align="left" ><asp:TextBox ID="txtMail" runat="server" 
                                                       CssClass="form-control"></asp:TextBox></td>
          </tr>
                                                <tr>
                                                  <td align="left" >ชื่อเจ้าของ(คู่สัญญา)</td>
                                                  <td align="left" >
                                                      <table width="100%" border="0" cellspacing="2" cellpadding="0">
                                                        <tr>
                                                          <td width="350"><asp:TextBox ID="txtCoName" runat="server"  CssClass="form-control"></asp:TextBox></td>
                                                          <td width="80"  align="right">E-mail :</td>
                                                          <td><asp:TextBox ID="txtCoMail" runat="server"  CssClass="form-control"></asp:TextBox></td>
                                                    </tr>
                                                  </table>

                                                  </td>
                                                       <td align="left" >โทรศัพท์ :</td>
                                                       <td align="left" ><asp:TextBox ID="txtCoTel" runat="server"  CssClass="form-control"></asp:TextBox></td>
          </tr>
                                            <!--    <tr>
                                                  <td align="left" >เลขที่บัญชี :</td>
                                                  <td align="left" >
                                                     <asp:TextBox ID="txtAccNo" runat="server"  CssClass="form-control"></asp:TextBox>                                                    </td>
                                                  <td align="left" >ธนาคาร :</td>
                                                <td align="left" >
                                                   <asp:DropDownList ID="ddlBank" runat="server" CssClass="form-control select2" >                                                    </asp:DropDownList>                                                    </td>
          </tr>
                                                <tr>
                                                  <td align="left" >ชื่อบัญชี :</td>
                                                  <td align="left" >
                                                     <asp:TextBox ID="txtAccName" runat="server"  CssClass="form-control"></asp:TextBox>                                                    </td>
                                                  <td align="left" >สาขา :</td>
                                                <td align="left" >
                                                   <asp:TextBox ID="txtBrunch" runat="server"  CssClass="form-control"></asp:TextBox>                                                    </td>
                                                </tr>
                                                <tr>
                                                  <td align="left" >ประเภทบัญชี :</td>
                                                  <td colspan="3" align="left" class="mailbox-messages">
                                                     <asp:RadioButtonList ID="optBankType" runat="server" 
                                                          RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True" Value="SAV">ออมทรัพย์/สะสมทรัพย์/เผื่อเรียก</asp:ListItem>
                                                    <asp:ListItem Value="DDA">กระแสรายวัน/เดินสะพัด</asp:ListItem>
                                                    <asp:ListItem Value="BOC">ฝากประจำ</asp:ListItem>
                                                  </asp:RadioButtonList></td>
                                                </tr>
    -->
                                                <tr>
                                                  <td align="left" >ปี พ.ศ.ที่สมัครเข้าโครงการ</td>
                                                  <td align="left">
                                                      <asp:TextBox ID="txtYear" runat="server" MaxLength="4"></asp:TextBox>
                                                    </td>
                                                  <td align="left" >Status :</td>
                                                  <td align="left" class="mailbox-messages"><asp:CheckBox ID="chkStatus" runat="server" Checked="True" Text="Public" /></td>
                                                </tr>
                                                <tr>
                                                  <td align="left" >&nbsp;</td>
                                                  <td align="left">       
                                                      &nbsp;</td>
                                                  <td align="left" >&nbsp;</td>
                                                  <td align="left" >&nbsp;</td>
                                                </tr>
                                                                                               
                                             
      <tr>
                                                  <td colspan="4" align="left"  class="MenuSt">โครงการ</td>
                                                </tr>
      <tr>
                                                  <td colspan="4" align="left">
                                                <asp:CheckBoxList ID="chkProject" runat="server"  class="mailbox-messages">
                                                    <asp:ListItem Value="1">F : โครงการให้คำแนะนำ ปรึกษาฯ</asp:ListItem>
                                                    <asp:ListItem Value="2">A : โครงการบุหรี่</asp:ListItem>
                                                    <asp:ListItem Value="3">M : โครงการเยี่ยมบ้าน และ MTM</asp:ListItem>
                                                    <asp:ListItem Value="4">H : Home Isolation (Covid-19)</asp:ListItem>
                                                </asp:CheckBoxList>
                                                  </td>
                                                </tr>
  </table>   

     </div>
            <div class="box-footer clearfix">
           
                <asp:Label ID="lblValidate" runat="server" 
                  Visible="False" CssClass="validateAlert" Width="99%"></asp:Label>
           
            </div>
          </div>

     <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">เภสัชกรที่ปฏิบัติหน้าที่</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">

<asp:GridView ID="grdPharmacist" CssClass="table table-hover"  
                             runat="server" CellPadding="2" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False"  
                             Font-Bold="False">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                            <asp:BoundField DataField="nRow" HeaderText="No." >
                            <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:BoundField>
                        <asp:BoundField DataField="PrefixName" HeaderText="คำนำหน้า" >                            
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="100px" />                            </asp:BoundField>
                            <asp:BoundField DataField="FirstName" HeaderText="ชื่อ" >
                            <HeaderStyle HorizontalAlign="Left" />                            
                            <ItemStyle Width="200px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LastName" HeaderText="นามสกุล" >
                            <HeaderStyle HorizontalAlign="Left" />                            
                            <ItemStyle Width="200px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PositionName" HeaderText="ตำแหน่ง">
                            <HeaderStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView>
            </div>
    </div>

    <div class="text-center">
          <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="btn btn-primary" Width="100px" />
        &nbsp;<asp:Button ID="cmdClear" runat="server" Text="ยกเลิก" CssClass="btn btn-default" Width="100px" />
                                              &nbsp;<asp:Button ID="cmdSendMail" runat="server" Text="ส่งอีเมล์แจ้งข้อมูลผู้ใช้งาน" CssClass="btn btn-success" />
      
    </div>
          <br />
      <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">รายชื่อร้านยา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
 <table border="0" cellspacing="1" cellpadding="0">
            <tr>
              <td width="50" >ค้นหา</td>
              <td >
                  <asp:TextBox ID="txtSearch" runat="server" Width="200px"></asp:TextBox>                </td>
              <td >
                  <asp:Button ID="cmdFind" runat="server" CssClass="buttonFind" Text="ค้นหา"  Width="100" />
                </td>
            </tr>
            
          </table>
            <asp:GridView ID="grdData" CssClass="table table-hover"
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="No.">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />                      </asp:BoundField>
            <asp:BoundField DataField="LocationID" HeaderText="รหัส">
              <headerstyle HorizontalAlign="Center" />          
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />                      </asp:BoundField>
            <asp:BoundField DataField="LocationName" HeaderText="ร้านยา">
              <headerstyle HorizontalAlign="Left" />          
              <itemstyle HorizontalAlign="Left" />                      </asp:BoundField>
                <asp:BoundField DataField="ProvinceName" HeaderText="จังหวัด">
                <ItemStyle HorizontalAlign="Left" />                </asp:BoundField>
                <asp:BoundField DataField="LocationGroupName" HeaderText="ประเภทร้านยา" />
            <asp:TemplateField HeaderText="Active">
              <itemtemplate>
                <asp:Image ID="imgStatus" runat="server" ImageUrl="images/icon-ok.png" Visible='<%# DataBinder.Eval(Container.DataItem, "IsPublic") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            <asp:TemplateField HeaderText="">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "LocationID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            <asp:TemplateField HeaderText="">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgDel" runat="server" 
                                    ImageUrl="images/icon-delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "LocationID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
                  <asp:TemplateField HeaderText="">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgMail" runat="server" ImageUrl="images/paperplane.png" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "LocationID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView> 

            </div>
    </div>


</section>

</asp:Content>
