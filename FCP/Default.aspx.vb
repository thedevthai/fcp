﻿Public Class Default2
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim acc As New UserController
    Dim enc As New CryptographyEngine
    Dim ctlNews As New NewsController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim ctlS As New SystemConfigController

        If ctlS.SystemConfig_GetByCode("ALIVE") = "N" Then
            Response.Redirect("404.aspx")
        End If


        Try

            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "reconn key", KeepAlive())

            Dim jsString As String = "if ((event.which && event.which == 13) || (event.keyCode && event.keyCode == 13)) {document.forms[0].all['" + cmdLogin.ClientID + "'].click();return false;} else return true; "
            txtPassword.Attributes.Add("onkeydown", jsString)

            Session.Timeout = 60

            If Request("logout") = "y" Then
                Session.Contents.RemoveAll()
                Session.Abandon()
                Session.RemoveAll()
                Response.Cookies.Clear()

                Dim delCookie1 As HttpCookie
                delCookie1 = New HttpCookie("FCPCPA")
                delCookie1.Expires = DateTime.Now.AddDays(-1D)
                Response.Cookies.Add(delCookie1)
            Else
                'RenewSessionFromCookie()
            End If


            If Not IsPostBack Then


                lblVersion.Text = ctlS.SystemConfig_GetByCode("Version")

                If Not Request("logout") Is Nothing Then
                    If Request("logout") = "YES" Or Request("logout").ToLower() = "y" Then
                        acc.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_LOGOUT, "Users", Request.Cookies("username").Value, "")
                        Session.Contents.RemoveAll()
                        Session.Abandon()
                        Request.Cookies("username").Value = Nothing
                    End If
                End If

                LoadNewsToGrid()
                LoadUserOnline()

                txtUserName.Focus()
            End If

            'btnOK.OnClientClick = String.Format("fnClickOK('{0}','{1}')", btnOK.UniqueID, "")
        Catch eX As Exception

        End Try
    End Sub
    Private Sub LoadNewsToGrid()

        dt = ctlNews.News_GetFirstPage

        With grdData
            .Visible = True
            .DataSource = dt
            .DataBind()

            For i = 0 To .Rows.Count - 1
                Dim img1 As Image = .Rows(i).Cells(0).FindControl("imgNews")

                Dim hlnkN As HyperLink = .Rows(i).Cells(1).FindControl("hlnkNews")
                hlnkN.Text = dt.Rows(i)("Title")

                Select Case dt.Rows(i)("NewsType")
                    Case "URL"
                        img1.ImageUrl = "images/www.png"
                        hlnkN.NavigateUrl = String.Concat(dt.Rows(i)("LinkPath"))
                    Case "UPL"
                        img1.ImageUrl = "images/pdf.png"
                        hlnkN.NavigateUrl = tmpUpload & "/" & String.Concat(dt.Rows(i)("LinkPath"))
                    Case "CON"
                        img1.ImageUrl = "images/comms.png"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                    Case Else
                        img1.ImageUrl = "images/comms.png"
                        hlnkN.NavigateUrl = "News.aspx?id=" & .DataKeys(i).Value
                End Select

                Dim img2 As Image = .Rows(i).Cells(0).FindControl("imgNew")
                img2.Visible = False
                If DateDiff(DateInterval.Day, CDate(dt.Rows(i)("NewsDate")), Today.Date) <= 5 Then
                    img2.Visible = True
                End If

                .Rows(i).Cells(2).Text = "<span class='Newsdate'>Update : " & DisplayDateTH(dt.Rows(i)("NewsDate")) & "</span>"

                'Select Case i Mod 4
                '    Case 0
                '        .Rows(i).Cells(2).Text = "<span class='label label-success'>" & DisplayDateTH(dt.Rows(i)("NewsDate")) & "</span>"
                '    Case 1
                '        .Rows(i).Cells(2).Text = "<span class='label label-info'>" & DisplayDateTH(dt.Rows(i)("NewsDate")) & "</span>"
                '    Case 2
                '        .Rows(i).Cells(2).Text = "<span class='label label-danger'>" & DisplayDateTH(dt.Rows(i)("NewsDate")) & "</span>"
                '    Case 3
                '        .Rows(i).Cells(2).Text = "<span class='label label-warning'>" & DisplayDateTH(dt.Rows(i)("NewsDate")) & "</span>"
                '    Case Else
                '        .Rows(i).Cells(2).Text = "<span class='label label-success'>" & DisplayDateTH(dt.Rows(i)("NewsDate")) & "</span>"
                'End Select




            Next

        End With

    End Sub


    Private Sub CheckUser()
        'If txtUserName.Text <> "admin" And txtUserName.Text <> "itee" Then
        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','ท่านไม่สามารถใช้งานได้ เนื่องจากอยู่ระหว่างการปิดปรับปรุงระบบ');", True)
        '    Exit Sub
        'End If

        dt = acc.User_CheckLogin(txtUserName.Text, enc.EncryptString(txtPassword.Text, True))

        If dt.Rows.Count > 0 Then

            If dt.Rows(0).Item("isPublic") = 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','ID ของท่านไม่สามารถใช้งานได้ชั่วคราว');", True)
                Exit Sub
            End If



            'Request.Cookies("UserID").Value = dt.Rows(0).Item("UserID")
            'Request.Cookies("username").Value = dt.Rows(0).Item("USERNAME")
            'Request.Cookies("Password").Value = enc.DecryptString(dt.Rows(0).Item("PASSWORD"), True)
            ''Session("UserRole") = dt.Rows(0).Item("USERROLE")
            'Request.Cookies("LastLog").Value = String.Concat(dt.Rows(0).Item("LastLogin"))
            'Request.Cookies("NameOfUser").Value = String.Concat(dt.Rows(0).Item("FirstName")) & " " & String.Concat(dt.Rows(0).Item("LastName"))
            'Request.Cookies("LocationID").Value = String.Concat(dt.Rows(0).Item("LocationID"))
            'Convert.ToInt32(Request.Cookies("RoleID").Value) = String.Concat(dt.Rows(0).Item("RoleID"))
            'Request.Cookies("RPTGRP").Value = String.Concat(dt.Rows(0).Item("ReportGroup"))
            'Request.Cookies("PRJMNG").Value = DBNull2Zero(dt.Rows(0).Item("ProjectManager"))

            'Dim dtP As New DataTable
            'dtP = acc.User_GetProjectRole(Request.Cookies("UserID").Value)
            'If dtP.Rows.Count > 0 Then
            '    Request.Cookies("ProjectF").Value = Decimal2Boolean(dtP.Rows(0)("ProjectF"))
            '    Request.Cookies("ProjectA").Value = Decimal2Boolean(dtP.Rows(0)("ProjectA"))
            'End If


            '--------------------------------------------------------------------------------------------

            Dim ckUserID As New HttpCookie("UserID")
            Dim ckUsername As New HttpCookie("Username")
            Dim ckLoginUser As New HttpCookie("LoginUser")
            Dim ckPassword As New HttpCookie("Password")
            Dim ckLastLogin As New HttpCookie("LastLogin")
            Dim ckNameOfUser As New HttpCookie("NameOfUser")
            Dim ckLocationID As New HttpCookie("LocationID")
            Dim ckROLE_ID As New HttpCookie("RoleID")
            Dim ckRPTGRP As New HttpCookie("RPTGRP")
            Dim ckPRJMNG As New HttpCookie("PRJMNG")
            Dim ckProjectF As New HttpCookie("ProjectF")
            Dim ckProjectA As New HttpCookie("ProjectA")

            'Dim ckPAYEAR As New HttpCookie("PAYEAR")


            ckUserID.Value = dt.Rows(0).Item("UserID")
            ckUsername.Value = String.Concat(dt.Rows(0).Item("Username")).ToString()
            ckLoginUser.Value = String.Concat(dt.Rows(0).Item("Username")).ToString()
            ckPassword.Value = String.Concat(dt.Rows(0).Item("Password")).ToString()
            ckLastLogin.Value = String.Concat(dt.Rows(0).Item("LastLogin")).ToString()
            ckNameOfUser.Value = String.Concat(dt.Rows(0).Item("FirstName")) & " " & String.Concat(dt.Rows(0).Item("LastName"))
            ckLocationID.Value = String.Concat(dt.Rows(0)("LocationID"))
            ckROLE_ID.Value = String.Concat(dt.Rows(0)("RoleID"))
            ckRPTGRP.Value = DBNull2Zero(dt.Rows(0)("ReportGroup"))
            ckPRJMNG.Value = DBNull2Zero(dt.Rows(0)("ProjectManager"))

            Dim dtP As New DataTable
            dtP = acc.User_GetProjectRole(dt.Rows(0).Item("UserID"))
            If dtP.Rows.Count > 0 Then
                ckProjectF.Value = DBNull2Zero(dtP.Rows(0)("ProjectF"))
                ckProjectA.Value = DBNull2Zero(dtP.Rows(0)("ProjectA"))
            End If


            Dim iFCPCookie As HttpCookie = New HttpCookie("FCPCPA")
            iFCPCookie("UserID") = dt.Rows(0).Item("UserID")

            Response.Cookies.Add(ckUserID)
            Response.Cookies.Add(ckUsername)
            Response.Cookies.Add(ckPassword)
            Response.Cookies.Add(ckLoginUser)
            Response.Cookies.Add(ckNameOfUser)
            Response.Cookies.Add(ckLastLogin)
            Response.Cookies.Add(ckLocationID)
            Response.Cookies.Add(ckROLE_ID)
            Response.Cookies.Add(ckRPTGRP)
            Response.Cookies.Add(ckPRJMNG)
            Response.Cookies.Add(ckProjectF)
            Response.Cookies.Add(ckProjectA)


            'ckUserID.Expires = acc.GET_DATE_SERVER().AddMinutes(60)

            iFCPCookie.Expires = acc.GET_DATE_SERVER().AddMinutes(120)
            Response.Cookies.Add(iFCPCookie)


            genLastLog()
            acc.User_GenLogfile(txtUserName.Text, ACTTYPE_LOG, "Users", Request.Cookies("username").Value, "")

            Response.Redirect("Home.aspx?actionType=h")

        Else
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','Username  หรือ Password ไม่ถูกต้อง');", True)
        End If
    End Sub

    Private Sub genLastLog() ' เก็บวันเวลาที่ User เข้าใช้ระบบครั้งล่าสุด
        acc.User_LastLog_Update(txtUserName.Text)
    End Sub

    Protected Sub cmdLogin_Click(sender As Object, e As EventArgs) Handles cmdLogin.Click

        'txtUserName.Text = "admin"
        'txtPassword.Text = "112233"

        If txtUserName.Text = "" Or txtPassword.Text = "" Then

            'lblAlert.Text = "กรุณาป้อน Username  หรือ Password ให้ครบถ้วนก่อน"
            'ModalPopupExtender1.Show()

            '  ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อน Username  หรือ Password ให้ครบถ้วนก่อน ")
            txtUserName.Focus()
            Exit Sub
        End If

        Session.Contents.RemoveAll()
        CheckUser()

    End Sub

    Private Sub grdData_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
        '    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        'End If
    End Sub

    Private Sub LoadUserOnline()

        lblUserOnlineCount.Text = Application("OnlineNow")

        dt = acc.GetUsers_Online
        lblOnline.Text = "ผู้ใช้งานล่าสุด : "
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1

                Select Case i Mod 4
                    Case 0
                        lblOnline.Text &= " <span class='label label-success'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("FirstName") & "</span>"
                    Case 1
                        lblOnline.Text &= " <span class='label label-info'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("FirstName") & "</span>"
                    Case 2
                        lblOnline.Text &= " <span class='label label-danger'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("FirstName") & "</span>"
                    Case 3
                        lblOnline.Text &= " <span class='label label-warning'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("FirstName") & "</span>"
                    Case Else
                        lblOnline.Text &= " <span class='label label-success'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("FirstName") & "</span>"
                End Select
                'If i Mod 2 = 0 Then
                '    lblOnline.Text &= " <span class='label label-success'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("Name") & "</span>"
                '    'lblOnline.Text &= "<font color=#1878ca>" & "[" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("Name") & "]" & "</font>"
                'Else
                '    lblOnline.Text &= " <span class='label label-warning'>" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("Name") & "</span>"
                'End If

            Next

        Else
            lblOnline.Text = "ไม่มี User Online ในขณะนี้"
        End If

    End Sub


End Class
