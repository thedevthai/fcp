﻿Public Class FormMenu
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ctlU As New UserController
        Dim dt As New DataTable
        pnProjectF.Visible = False
        pnProjectA.Visible = False
        pnCovid.Visible = False
        dt = ctlU.User_GetProjectRole(Request.Cookies("UserID").Value)
        If dt.Rows.Count > 0 Then
            pnProjectF.Visible = Decimal2Boolean(dt.Rows(0)("ProjectF"))
            pnProjectA.Visible = Decimal2Boolean(dt.Rows(0)("ProjectA"))
            pnHomeVisit.Visible = Decimal2Boolean(dt.Rows(0)("ProjectH"))
            'pnCovid.Visible = Decimal2Boolean(dt.Rows(0)("ProjectC"))
        End If
        dt = Nothing
    End Sub

End Class