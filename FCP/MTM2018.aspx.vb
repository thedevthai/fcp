﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Subgurim.Controles
Public Class MTM2018
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlL As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlMTM As New MTMController
    'Dim ctlH As New HomeVisitController
    'Dim ServiceDate As Long
    Dim sAlert As String
    Dim isValid As Boolean


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Session("patientid") Is Nothing Then
            Response.Redirect("PatientSearch.aspx")
        End If

        If Not IsPostBack Then
            isAdd = True
            txtCGNo.Visible = False
            lblCGday.Visible = False
            txtCGYear.Visible = False
            lblCGYear.Visible = False
            lblCgType.Visible = False
            optCigarette.Visible = False
            lblAlcohol.Visible = False
            txtAlcoholFQ.Visible = False

            pnFat.Visible = False
            txtBegin.Enabled = True
            txtEnd.Enabled = True

            pnAlertM.Visible = False
            pnAlertB.Visible = False
            pnDrugRemainAlert.Visible = False

            If Request("acttype") = "view" Then
                cmdSave.Visible = False
            Else
                cmdSave.Visible = True
            End If


            txtServiceDate.Text = DisplayShortDateTH(ctlL.GET_DATE_SERVER)
            'txtBYear.Text = Right(txtServiceDate.Text, 4)

            'If StrNull2Zero(txtBYear.Text) < 2500 Then
            '    txtBYear.Text = StrNull2Zero(txtBYear.Text) + 543
            'Else
            '    txtBYear.Text = StrNull2Zero(txtBYear.Text)
            'End If

            LoadProblemGroup()
            LoadDeseaseToDDL()
            LoadMedicine()
            LoadLocationData(Session("LocationID"))
            LoadPharmacist(Session("LocationID"))
            LoadProblemRelate()

            If Request("t") = "new" Or (Request("t") Is Nothing) Then
                lblNotic.Visible = True
                pnMTM.Visible = False
                optMTMType.SelectedIndex = 0
                BindPatientFromList()
                lblSEQ.Text = (ctlMTM.MTM_Master_GetLastSEQ(Session("LocationID"), Session("patientid"), optMTMType.SelectedValue) + 1).ToString
            Else
                If Not Request("fid") Is Nothing Then
                    LoadMTMHeader()
                End If
            End If

            LoadLabResultToGrid()
            LoadDrugUse()
            LoadHospital()
        End If

        'If (FileUploaderAJAX1.IsPosting) Then
        '    ServiceDate = CLng(ConvertDate2DBString(Today.Date))
        '    'F01_sFile = Path.GetExtension(FileUploaderAJAX1.PostedFile.FileName)
        '    F01_DocFile = "Refer" & "_" & lblLocationID.Text & "_" & ServiceDate & "_" & ConvertTimeToString(Now())
        '    UploadFile(F01_DocFile)
        'End If


        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        ''txtCardID.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")

    End Sub

    Private Sub BindFinalResultToRadioList()
        If chkNotFollow.Checked Then
            'optFat.Enabled = False
            optResultB.Enabled = False
            txtResultOtherB.Enabled = False
            txtBegin.Enabled = False
            txtEnd.Enabled = False
        Else
            optFat.Enabled = True
            optResultB.Enabled = True
            txtResultOtherB.Enabled = True
            txtBegin.Enabled = True
            txtEnd.Enabled = True
            optResultB.Items.Clear()
            pnFat.Visible = False
            pnEating.Visible = False
            Select Case ddlBehavior.SelectedValue
                Case "1" 'การสูบบุหรี่
                    optResultB.Items.Add("สูบลดลง")
                    optResultB.Items.Add("สูบเท่าเดิม")
                    optResultB.Items.Add("สูบเพิ่มขึ้น")
                    optResultB.Items.Add("เลิกสูบแล้ว")
                    optResultB.Items.Add("อื่นๆ")
                Case "2" 'การดื่มเหล้า
                    optResultB.Items.Add("ดื่มลดลง")
                    optResultB.Items.Add("ดื่มเพิ่มขึ้น")
                    optResultB.Items.Add("คงเดิม")
                    optResultB.Items.Add("อื่นๆ")
                Case "3" 'ความอ้วน
                    pnFat.Visible = True
                    optResultB.Items.Add("ลดลง")
                    optResultB.Items.Add("เพิ่มขึ้น")
                    optResultB.Items.Add("คงเดิม")
                    optResultB.Items.Add("อื่นๆ")
                Case "4" 'การออกกำลังกาย
                    optResultB.Items.Add("ไม่ได้ออกกำลังกายเพิ่ม(เหมือนเดิม)")
                    optResultB.Items.Add("ออกกำลังกายเพิ่มขึ้น")
                    optResultB.Items.Add("ออกกำลังกายลดลง")
                    optResultB.Items.Add("จำนวนครั้งเท่าเดิม แต่เพิ่มเวลาในแต่ละครั้ง")
                    optResultB.Items.Add("อื่นๆ")
                Case "5" 'การรับประทานอาหาร

                    pnEating.Visible = True
                    optResultB.Items.Add("ดีขึ้น")
                    optResultB.Items.Add("แย่ลง")
                    optResultB.Items.Add("เท่าเดิม")
                    optResultB.Items.Add("อื่นๆ")

                    txtBegin.Enabled = False
                    txtEnd.Enabled = False
                Case "6" 'ความเครียด
                    optResultB.Items.Add("ลดลง")
                    optResultB.Items.Add("เพิ่มขึ้น")
                    optResultB.Items.Add("คงเดิม")
                    optResultB.Items.Add("อื่นๆ")
                Case Else
                    optResultB.Items.Add("ลดลง")
                    optResultB.Items.Add("เพิ่มขึ้น")
                    optResultB.Items.Add("คงเดิม")
                    optResultB.Items.Add("อื่นๆ")
            End Select
        End If
        LoadDataToComboBox()
    End Sub

    Private Sub LoadDeseaseToDDL()
        cboDesease.Items.Clear()
        Dim dtD As New DataTable
        Dim ctlD As New DeseaseController

        dtD = ctlD.Desease_Get

        If dtD.Rows.Count > 0 Then
            cboDesease.Items.Clear()

            With cboDesease
                .DataSource = dtD
                .TextField = "Name"
                .ValueField = "UID"
                .DataBind()
            End With


        End If

    End Sub

    Private Sub LoadLocation()

        dt = ctlL.Location_GetByID(Session("LocationID"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("ProvinceName"))
            End With
        End If
    End Sub
    Private Sub LoadPharmacist(LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With

                'With ddlPharmacist_D
                '    .Visible = True
                '    For i = 0 To dtP.Rows.Count - 1
                '        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                '        .Items(i).Value = dtP.Rows(i)("PersonID")
                '    Next
                '    .SelectedIndex = 0
                'End With

                'With ddlPharmacistB
                '    .Visible = True
                '    For i = 0 To dtP.Rows.Count - 1
                '        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                '        .Items(i).Value = dtP.Rows(i)("PersonID")
                '    Next
                '    .SelectedIndex = 0
                'End With

            End If
        End If
        dtP = Nothing
    End Sub
    Private Sub LoadProblemRelate()
        Dim dtPG As New DataTable
        Dim ctlP As New ProblemController
        ddlBehavior.Items.Clear()
        dtPG = ctlP.BehaviorProblem_Get
        If dtPG.Rows.Count > 0 Then
            ddlBehavior.DataSource = dtPG
            ddlBehavior.DataTextField = "Descriptions"
            ddlBehavior.DataValueField = "UID"
            ddlBehavior.DataBind()
        End If
        dtPG = Nothing
    End Sub

    Private Sub LoadMedicine()
        Dim dtM As New DataTable
        Dim ctlD As New DrugController
        cboDrug.Items.Clear()
        cboDrugUse.Items.Clear()
        dtM = ctlD.Drug_Get
        If dtM.Rows.Count > 0 Then
            cboDrug.DataSource = dtM
            cboDrug.TextField = "Name"
            cboDrug.ValueField = "UID"
            cboDrug.DataBind()

            cboDrugUse.DataSource = dtM
            cboDrugUse.TextField = "Name"
            cboDrugUse.ValueField = "UID"
            cboDrugUse.DataBind()

            cboDrugRemain.DataSource = dtM
            cboDrugRemain.TextField = "Name"
            cboDrugRemain.ValueField = "UID"
            cboDrugRemain.DataBind()

        End If
        dtM = Nothing
    End Sub

    Private Sub LoadProblemGroup()
        Dim dtPG As New DataTable
        Dim ctlP As New ProblemController
        cboProblemGroup.Items.Clear()
        dtPG = ctlP.ProblemGroup_Get
        If dtPG.Rows.Count > 0 Then
            cboProblemGroup.DataSource = dtPG
            cboProblemGroup.DataTextField = "Descriptions"
            cboProblemGroup.DataValueField = "Code"
            cboProblemGroup.DataBind()
            cboProblemGroup.SelectedIndex = 0
            LoadProblem()
        End If

        dtPG = Nothing
    End Sub
    Private Sub LoadProblem()
        Dim dtP As New DataTable
        Dim ctlP As New ProblemController
        cboProblemItem.Items.Clear()
        cboProblemItem.Value = ""
        dtP = ctlP.ProblemItem_Get(cboProblemGroup.SelectedValue)
        If dtP.Rows.Count > 0 Then
            cboProblemItem.DataSource = dtP
            cboProblemItem.TextField = "Descriptions"
            cboProblemItem.ValueField = "Code"
            cboProblemItem.DataBind()
        End If
        dtP = Nothing
    End Sub

    Private Sub LoadLocationData(LocationID As String)
        dt = ctlL.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblLocationID.Text = .Item("LocationID")
                lblLocationName.Text = .Item("LocationName")
                lblLocationProvince.Text = .Item("ProvinceName")
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadMTMHerb()
        Dim dtH As New DataTable

        dtH = ctlMTM.MTM_Master_GetHerbLastInfo(StrNull2Zero(Session("patientid")))
        If dtH.Rows.Count > 0 Then
            With dtH.Rows(0)
                txtDrug1.Text = String.Concat(.Item("MedicationUsed1"))
                txtDrug2.Text = String.Concat(.Item("MedicationUsed2"))
                txtDrug3.Text = String.Concat(.Item("MedicationUsed3"))
                txtDrug4.Text = String.Concat(.Item("MedicationUsed4"))
            End With
        End If

    End Sub



    Private Sub LoadMTMHeader()
        'Dim BYear As Integer = 0

        'BYear = ctlMTM.MTM_Master_GetBYear(StrNull2Zero(Request("fid")))

        If Request("t") = "New" Then
            dt = ctlMTM.MTM_Master_GetLastInfo(Session("patientid"))
        Else
            dt = ctlMTM.MTM_Master_GetByUID(StrNull2Zero(Request("fid")))
        End If


        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                lblNotic.Visible = False
                pnMTM.Visible = True
                'txtBYear.Text = .Item("BYear")
                lblMTMUID.Text = .Item("UID")
                lblSEQ.Text = DBNull2Str(.Item("SEQ"))

                optMTMType.SelectedValue = DBNull2Str(.Item("MTMTYPE"))
                BindPatientFromList()

                optPatientFrom.SelectedValue = DBNull2Str(.Item("PFROM"))
                txtFrom.Text = DBNull2Str(.Item("FROMTXT"))

                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                'lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                'lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                txtTime.Text = String.Concat(.Item("ServiceTime"))

                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))


                optSmoke.SelectedValue = String.Concat(.Item("Smoke"))
                txtCGYear.Text = String.Concat(.Item("SmokeYear"))
                txtCGNo.Text = String.Concat(.Item("SmokeCigarette"))
                optCigarette.SelectedValue = String.Concat(.Item("CigaretteType"))
                optAlcohol.SelectedValue = String.Concat(.Item("Alcohol"))
                txtAlcoholFQ.Text = String.Concat(.Item("AlcoholFQ"))

                LoadMTMHerb()

                'txtDrug1.Text = String.Concat(.Item("MedicationUsed1"))
                'txtDrug2.Text = String.Concat(.Item("MedicationUsed2"))
                'txtDrug3.Text = String.Concat(.Item("MedicationUsed3"))
                'txtDrug4.Text = String.Concat(.Item("MedicationUsed4"))
                'txtDrug5.Text = String.Concat(.Item("MedicationUsed5"))
                'txtMed6.Text = String.Concat(.Item("MedicationUsed6"))
                'txtMed7.Text = String.Concat(.Item("MedicationUsed7"))



                chkPitting.Checked = ConvertYesNo2Boolean(String.Concat(.Item("isPitting")))
                chkWound.Checked = ConvertYesNo2Boolean(String.Concat(.Item("isWound")))
                chkPeripheral.Checked = ConvertYesNo2Boolean(String.Concat(.Item("isPeripheral")))

                txtPitting.Text = String.Concat(.Item("Pitting"))
                txtWound.Text = String.Concat(.Item("Wound"))
                txtPeripheral.Text = String.Concat(.Item("Peripheral"))


                'optMedFrom.SelectedValue = String.Concat(.Item("HospitalType"))
                'txtHospital.Text = String.Concat(.Item("HospitalName"))
                chkStatus.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("Status")))

                If Request("t") = "New" Then
                    lblMTMUID.Text = ""
                    txtServiceDate.Text = DisplayShortDateTH(ctlL.GET_DATE_SERVER)
                    txtTime.Text = ""
                    ddlPerson.SelectedIndex = 0
                    pnMTM.Visible = False
                Else
                    pnMTM.Visible = True
                    LoadDrugProblemToGrid()
                    LoadDrugRemainToGrid()
                    LoadDeseaseRelateToGrid()
                    LoadBehaviorProblemToGrid()
                End If

            End With
        Else
            LoadLocationData(Session("LocationID"))
        End If
        dt = Nothing

    End Sub

    Protected Sub optSmoke_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optSmoke.SelectedIndexChanged
        Select Case optSmoke.SelectedValue
            Case "2"
                txtCGYear.Visible = True
                lblCGYear.Visible = True
            Case "1"
                lblCGday.Visible = True
                txtCGNo.Visible = True
                lblCGYear.Visible = True
                txtCGYear.Visible = True
                optAlcohol.Visible = True
            Case Else
                txtCGNo.Visible = False
                lblCGday.Visible = False
                txtCGYear.Visible = False
                lblCGYear.Visible = False
                lblCgType.Visible = False
                optCigarette.Visible = False
        End Select
        LoadDataToComboBox()
    End Sub

    Protected Sub optAlcohol_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optAlcohol.SelectedIndexChanged
        Select Case optAlcohol.SelectedValue
            Case "3"
                lblAlcohol.Visible = True
                txtAlcoholFQ.Visible = True
            Case Else
                lblAlcohol.Visible = False
                txtAlcoholFQ.Visible = False
        End Select
        LoadDataToComboBox()
    End Sub


    Private Function ValidateData() As Boolean
        sAlert = ""
        isValid = True

        If cboDrug.Text = "" Then
            sAlert &= "- กรุณาเลือกรายการยาก่อน <br/>"
            isValid = False
        End If

        If DBNull2Str(cboProblemGroup.SelectedValue) = "" Then
            sAlert &= "- กรุณาเลือกปัญหาก่อน <br/>"
            isValid = False
        End If

        If cboProblemItem.Text = "" Then
            sAlert &= "- กรุณาเลือกปัญหาย่อยก่อน <br/>"
            isValid = False
        End If
        If txtInterventionDrug.Text = "" Then
            sAlert &= "- Intervention <br/>"
            isValid = False
        End If
        Return isValid
    End Function
    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If txtServiceDate.Text = "" Then
            DisplayMessage(Me.Page, "กรุณาป้อนวันที่ให้บริการก่อน")
            Exit Sub
        End If
        If StrNull2Zero(txtTime.Text) = 0 Then
            DisplayMessage(Me.Page, "กรุณาป้อนระยะเวลาในการให้บริการก่อน")
            Exit Sub
        End If

        If optMTMType.SelectedIndex = -1 Then
            DisplayMessage(Me.Page, "กรุณาเลือกประเภทของการให้บริการก่อน ")
            Exit Sub
        End If

        Dim objuser As New UserController
        Dim ServiceDate As Long
        ServiceDate = CLng(ConvertStrDate2DBString(txtServiceDate.Text))

        If lblMTMUID.Text = "" Then 'Add new

            If ctlMTM.MTM_Master_ChkDupCustomer(Session("patientid"), ServiceDate) = True Then
                DisplayMessage(Me.Page, "ท่านได้บันทึกการให้บริการของผู้รับบริการนี้ ในวันที่ท่านระบุแล้ว")
                LoadDataToComboBox()
                Exit Sub
            End If

            ctlMTM.MTM_Master_Add(lblLocationID.Text, Session("patientid"), ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), StrNull2Zero(optSmoke.SelectedValue), StrNull2Zero(txtCGYear.Text), StrNull2Zero(txtCGNo.Text), StrNull2Zero(optCigarette.SelectedValue), StrNull2Zero(optAlcohol.SelectedValue), StrNull2Zero(txtAlcoholFQ.Text), txtDrug1.Text, txtDrug2.Text, txtDrug3.Text, txtDrug4.Text, "", "", "", 0, "", Convert2Status(chkStatus.Checked), Session("username"), StrNull2Zero(lblSEQ.Text), optMTMType.SelectedValue, optPatientFrom.SelectedValue, txtFrom.Text)

            objuser.User_GenLogfile(Session("Username"), ACTTYPE_ADD, "MTM", "เพิ่มแบบฟอร์มMTM  การดูแลเรื่องการใช้ยา :  <<PatientID:" & Session("patientid") & ">>", "MTM")

            lblMTMUID.Text = ctlMTM.MTM_Master_GetLastUID(lblLocationID.Text, Session("patientid"), ServiceDate).ToString()

            LoadMedicine()
            LoadProblemGroup()
            LoadDeseaseToDDL()

            pnMTM.Visible = True

            Response.Redirect("MTM.aspx?t=edit&fid=" & lblMTMUID.Text)
        Else
            ctlMTM.MTM_Master_Update(StrNull2Zero(lblMTMUID.Text), lblLocationID.Text, Session("patientid"), ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), StrNull2Zero(optSmoke.SelectedValue), StrNull2Zero(txtCGYear.Text), StrNull2Zero(txtCGNo.Text), StrNull2Zero(optCigarette.SelectedValue), StrNull2Zero(optAlcohol.SelectedValue), StrNull2Zero(txtAlcoholFQ.Text), txtDrug1.Text, txtDrug2.Text, txtDrug3.Text, txtDrug4.Text, "", "", "", 0, "", Convert2Status(chkStatus.Checked), Session("username"), optMTMType.SelectedValue, optPatientFrom.SelectedValue, txtFrom.Text)

            objuser.User_GenLogfile(Session("Username"), ACTTYPE_UPD, "Service_MTM", "บันทึกเพิ่มแบบฟอร์มMTM  การดูแลเรื่องการใช้ยา :<<PatientID:" & Session("patientid") & ">>", "MTM")
        End If
        'Response.Redirect("ResultPage.aspx")   

        DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อย")
        LoadDataToComboBox()


    End Sub
#Region "Edit Data"
    Private Sub EditDrugProblem(UID As Integer)
        LoadMedicine()
        LoadProblemGroup()
        LoadDeseaseToDDL()

        dt = ctlMTM.MTM_DrugProblem_GetByUID(UID)
        If dt.Rows.Count > 0 Then
            lblUID_Drug.Text = dt.Rows(0)("UID")
            cboDrug.Value = DBNull2Str(dt.Rows(0)("DrugUID"))
            cboProblemGroup.SelectedValue = dt.Rows(0)("ProblemGroupUID")
            LoadProblem()
            cboProblemItem.Value = dt.Rows(0)("ProblemUID")
            txtInterventionDrug.Text = DBNull2Str(dt.Rows(0)("Interventions"))
            optResult_D.SelectedValue = DBNull2Str(dt.Rows(0)("FinalResult"))
            txtResultOther_D.Text = DBNull2Str(dt.Rows(0)("FinalResultOther"))
            txtProblemOtherDrug.Text = DBNull2Str(dt.Rows(0)("ProblemOther"))
            pnAlertM.Visible = False
            cmdDrugProblemAdd.Text = "บันทึกการแก้ไข"
        End If
    End Sub
    Private Sub EditBehaviorProblem(UID As Integer)
        dt = ctlMTM.MTM_BehaviorProblem_GetByUID(UID)
        If dt.Rows.Count > 0 Then
            lblUID_B.Text = dt.Rows(0)("UID")
            ddlBehavior.SelectedValue = DBNull2Str(dt.Rows(0)("ProblemUID"))
            BindFinalResultToRadioList()

            txtProblemBehaviorOther.Text = DBNull2Str(dt.Rows(0)("ProblemOther"))

            txtInterventionB.Text = DBNull2Str(dt.Rows(0)("Interventions"))



            txtBegin.Text = DBNull2Str(dt.Rows(0)("ResultBegin"))
            txtEnd.Text = DBNull2Str(dt.Rows(0)("ResultEnd"))

            Select Case ddlBehavior.SelectedValue
                'Case "1" 'การสูบบุหรี่

                'Case "2" 'การดื่มเหล้า

                Case "3" 'ความอ้วน
                    pnFat.Visible = True
                    optFat.SelectedValue = DBNull2Str(dt.Rows(0)("FatFollow"))
                Case "4" 'การออกกำลังกาย 
                    txtResultOtherB.Text = DBNull2Str(dt.Rows(0)("Remark"))
                    txtBegin.Enabled = False
                    txtEnd.Enabled = False
                Case "5" 'การรับประทานอาหาร
                    pnEating.Visible = True
                    optTast.SelectedValue = DBNull2Str(dt.Rows(0)("TasteFollow"))
                    'Case "6" 'ความเครียด

            End Select

            If DBNull2Str(dt.Rows(0)("isFollow")) = "N" Then
                chkNotFollow.Checked = True
            Else
                optResultB.SelectedValue = DBNull2Str(dt.Rows(0)("FinalResult"))
                txtResultOtherB.Text = DBNull2Str(dt.Rows(0)("FinalResultOther"))
                chkNotFollow.Checked = False
            End If
            cmdBehaviorAdd.Text = "บันทึกการแก้ไข"

        End If
    End Sub
#End Region

    Protected Sub cboProblemGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboProblemGroup.SelectedIndexChanged
        LoadDataToComboBox()
    End Sub

#Region "Load Data to Grid"
    Private Sub LoadDeseaseRelateToGrid()
        dt = ctlMTM.MTM_DeseaseRelate_Get(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(Session("patientid")))
        grdDesease.DataSource = dt
        grdDesease.DataBind()
    End Sub
    Private Sub LoadDrugProblemToGrid()
        dt = ctlMTM.MTM_DrugProblem_Get(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(Session("patientid")))
        grdDrugProblem.DataSource = dt
        grdDrugProblem.DataBind()
    End Sub
    Private Sub LoadBehaviorProblemToGrid()
        dt = ctlMTM.MTM_BehaviorProblem_Get(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(Session("patientid")))
        grdBehavior.DataSource = dt
        grdBehavior.DataBind()
    End Sub
    Private Sub LoadLabResultToGrid()
        dt = ctlMTM.LabResult1_Get(StrNull2Zero(lblMTMUID.Text))
        grdLab.DataSource = dt
        grdLab.DataBind()

        dt = ctlMTM.LabResult2_Get(StrNull2Zero(lblMTMUID.Text))
        grdLab2.DataSource = dt
        grdLab2.DataBind()
    End Sub
#End Region
#Region "Grid Event"
    Protected Sub grdDrugProblem_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDrugProblem.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(7).FindControl("imgDel_D")
            imgD.Attributes.Add("onClick", scriptString)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub
    Private Sub grdDrugProblem_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDrugProblem.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit_D"
                    EditDrugProblem(e.CommandArgument())
                Case "imgDel_D"
                    ctlMTM.MTM_DrugProblem_Delete(StrNull2Zero(e.CommandArgument()))
                    LoadDrugProblemToGrid()
                    LoadDataToComboBox()
            End Select
        End If
    End Sub
    Protected Sub grdDesease_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDesease.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(2).FindControl("imgDel_DS")
            imgD.Attributes.Add("onClick", scriptString)

            e.Row.Cells(0).Text = e.Row.RowIndex + 1
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub
    Protected Sub grdBehavior_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdBehavior.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(6).FindControl("imgDel_B")
            imgD.Attributes.Add("onClick", scriptString)
            'e.Row.Cells(0).Text = e.Row.RowIndex + 1
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub
    Private Sub grdDesease_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDesease.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel_DS"
                    ctlMTM.MTM_DeseaseRelate_Delete(e.CommandArgument())
                    LoadDeseaseRelateToGrid()
                    LoadDataToComboBox()
            End Select
        End If
    End Sub
    Private Sub grdBehavior_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdBehavior.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit_B"
                    EditBehaviorProblem(e.CommandArgument())
                Case "imgDel_B"
                    ctlMTM.MTM_BehaviorProblem_Delete(e.CommandArgument())
                    ClearTextBehavior()
                    LoadBehaviorProblemToGrid()
                    LoadDataToComboBox()
            End Select
        End If
    End Sub
#End Region
    Protected Sub cmdAddDesease_Click(sender As Object, e As EventArgs) Handles cmdAddDesease.Click
        If cboDesease.Value = "" Then
            'DisplayMessage(Me.Page, "เลือกโรคที่ต้องการเพิ่มก่อน")
            LoadDataToComboBox()
            Exit Sub
        End If

        ctlMTM.MTM_DeseaseRelate_Add(StrNull2Zero(lblMTMUID.Text), FORM_TYPE_ID_MTM, cboDesease.Value, txtDeseaseOther.Text, Session("patientid"), Session("userid"))
        LoadDeseaseRelateToGrid()
        LoadDataToComboBox()
    End Sub


    Protected Sub cmdAddBehavior_Click(sender As Object, e As EventArgs) Handles cmdBehaviorAdd.Click
        If txtInterventionB.Text = "" Then
            lblAlertB.Text = "ท่านยังไม่ได้ป้อน Interventions"
            lblAlertB.Visible = True
            pnAlertB.Visible = True

            pnAlertM.Visible = False

            LoadDataToComboBox()
            Exit Sub
        End If

        Dim isFollow, ResultB, ResultOtherB, ResultBegin, ResultEnd As String
        If chkNotFollow.Checked Then ' not follow
            isFollow = "N"
            ResultB = ""
            ResultOtherB = ""
            ResultBegin = ""
            ResultEnd = ""
        Else
            isFollow = "Y"
            ResultB = optResultB.SelectedValue
            ResultOtherB = txtResultOtherB.Text
            ResultBegin = txtBegin.Text
            ResultEnd = txtEnd.Text
        End If

        ctlMTM.MTM_BehaviorProblem_Save(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(lblUID_B.Text), ddlBehavior.SelectedValue, txtProblemBehaviorOther.Text, txtInterventionB.Text, ResultB, ResultOtherB, ResultBegin, ResultEnd, optFat.SelectedValue, ResultOtherB, isFollow, Session("patientid"), Session("userid"), optTast.SelectedValue)

        ClearTextBehavior()
        LoadBehaviorProblemToGrid()
        LoadDataToComboBox()

        pnAlertB.Visible = False
    End Sub
    Private Sub ClearTextBehavior()
        lblUID_B.Text = "0"
        ddlBehavior.SelectedIndex = 0
        BindFinalResultToRadioList()

        txtProblemBehaviorOther.Text = ""
        txtInterventionB.Text = ""
        txtResultOtherB.Text = ""
        txtBegin.Text = ""
        txtEnd.Text = ""
        chkNotFollow.Checked = False
        cmdBehaviorAdd.Text = "เพิ่มปัญหาพฤติกรรม"
    End Sub
    Protected Sub cmdAddDrugProblem_Click(sender As Object, e As EventArgs) Handles cmdDrugProblemAdd.Click
        If Not ValidateData() Then
            lblAlertM.Text = "<b>กรุณากรอกข้อมูลให้ครบถ้วน</b> <br/>" & sAlert
            lblAlertM.Visible = True
            pnAlertM.Visible = True
            pnAlertB.Visible = False
            LoadDataToComboBox()
            Exit Sub
        Else
            pnAlertM.Visible = False
        End If

        Dim ProblemGroup, ProblemItem As String
        ProblemGroup = cboProblemGroup.SelectedValue
        ProblemItem = cboProblemItem.Value

        ctlMTM.MTM_DrugProblem_Save(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(lblUID_Drug.Text), FORM_TYPE_ID_MTM, StrNull2Zero(cboDrug.Value), ProblemGroup, ProblemItem, txtProblemOtherDrug.Text, txtInterventionDrug.Text, optResult_D.SelectedValue, txtResultOther_D.Text, Session("patientid"), Session("username"))

        'ddlPharmacist_D.SelectedIndex = 0
        ClearTextDrugProblem()
        LoadDrugProblemToGrid()
        'DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อย")

        pnAlertM.Visible = False
        pnAlertB.Visible = False
        LoadDataToComboBox()
        pnAlertM.Visible = False
    End Sub

    Private Sub ClearTextDrugProblem()
        lblUID_Drug.Text = "0"
        cboDrug.Value = ""
        cboProblemGroup.SelectedIndex = 0
        cboProblemItem.Value = ""
        cboDesease.Value = ""
        txtInterventionDrug.Text = ""
        cmdDrugProblemAdd.Text = "เพิ่มปัญหาจากการใช้ยา"
    End Sub

    Protected Sub cmdAddLab_Click(sender As Object, e As EventArgs) Handles cmdAddLab.Click
        If txtServiceDate.Text = "" Then
            DisplayMessage(Me.Page, "กรุณาระบุวันที่ให้บริการก่อน")
            Exit Sub
        End If
        Dim ServiceDate As Long

        ServiceDate = CLng(ConvertStrDate2DBString(txtServiceDate.Text))
        For i = 0 To grdLab.Rows.Count - 1
            Dim txtV As TextBox = grdLab.Rows(i).Cells(1).FindControl("txtResultValue")
            ctlMTM.LabResult_Save(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(grdLab.DataKeys(i).Value), ServiceDate, Session("patientid"), txtV.Text, Session("userid"))
        Next
        For i = 0 To grdLab2.Rows.Count - 1
            Dim txtV As TextBox = grdLab2.Rows(i).Cells(1).FindControl("txtResultValue0")
            ctlMTM.LabResult_Save(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(grdLab2.DataKeys(i).Value), ServiceDate, Session("patientid"), txtV.Text, Session("userid"))
        Next
        DisplayMessage(Me.Page, "บันทึกผล Lab เรียบร้อย")
        LoadDataToComboBox()
    End Sub
    Protected Sub cmdPE_Save_Click(sender As Object, e As EventArgs) Handles cmdPE_Save.Click
        ctlMTM.MTM_UpdatePE(StrNull2Zero(lblMTMUID.Text), ConvertStatus2YN(chkPitting.Checked), ConvertStatus2YN(chkWound.Checked), ConvertStatus2YN(chkPeripheral.Checked), txtPitting.Text, txtWound.Text, txtPeripheral.Text)
        DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อย")
        LoadDataToComboBox()
    End Sub


    Private Sub LoadDataToComboBox()
        LoadDeseaseToDDL()
        LoadProblem()
        LoadMedicine()
    End Sub

    Protected Sub ddlBehavior_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBehavior.SelectedIndexChanged
        BindFinalResultToRadioList()

    End Sub

    Protected Sub chkFollow_CheckedChanged(sender As Object, e As EventArgs) Handles chkNotFollow.CheckedChanged
        BindFinalResultToRadioList()
    End Sub

    Protected Sub cmdSave2_Click(sender As Object, e As EventArgs) Handles cmdSave2.Click
        If optMTMType.SelectedIndex = -1 Then
            DisplayMessage(Me.Page, "กรุณาเลือกประเภทของการให้บริการก่อน ")
            Exit Sub
        End If
        If txtServiceDate.Text = "" Then
            DisplayMessage(Me.Page, "กรุณาป้อนวันที่ให้บริการก่อน")
            Exit Sub
        End If
        If StrNull2Zero(txtTime.Text) = 0 Then
            DisplayMessage(Me.Page, "กรุณาป้อนระยะเวลาในการให้บริการก่อน")
            Exit Sub
        End If
        Dim ServiceDate As Long
        ServiceDate = CLng(ConvertStrDate2DBString(txtServiceDate.Text))


        ctlMTM.MTM_Master_Update(StrNull2Zero(lblMTMUID.Text), lblLocationID.Text, Session("patientid"), ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), StrNull2Zero(optSmoke.SelectedValue), StrNull2Zero(txtCGYear.Text), StrNull2Zero(txtCGNo.Text), StrNull2Zero(optCigarette.SelectedValue), StrNull2Zero(optAlcohol.SelectedValue), StrNull2Zero(txtAlcoholFQ.Text), txtDrug1.Text, txtDrug2.Text, txtDrug3.Text, txtDrug4.Text, "", "", "", 0, "", Convert2Status(chkStatus.Checked), Session("username"), optMTMType.SelectedValue, optPatientFrom.SelectedValue, txtFrom.Text)

        Response.Redirect("FormList_HomeVisit.aspx")
        LoadDataToComboBox()
    End Sub

    Protected Sub cmdAddHospital_Click(sender As Object, e As EventArgs) Handles cmdAddHospital.Click
        If txtHospital.Text.Trim = "" Then
            DisplayMessage(Me.Page, "กรุณาระบุชื่อสถานพยาบาลก่อน")
            LoadDataToComboBox()
            Exit Sub
        End If

        ctlMTM.MTM_Hospital_Add(lblMTMUID.Text, optMedFrom.SelectedValue, txtHospital.Text, Session("patientid"))
        LoadHospital()
        txtHospital.Text = ""
        LoadDataToComboBox()
    End Sub
    Private Sub LoadHospital()
        dt = ctlMTM.MTM_Hospital_GetByPatient(StrNull2Zero(Session("patientid")))
        grdHospital.DataSource = dt
        grdHospital.DataBind()
    End Sub

    Protected Sub cmdAddDrug_Click(sender As Object, e As EventArgs) Handles cmdAddDrug.Click
        If cboDrugUse.Value = "" Then
            DisplayMessage(Me.Page, "กรุณาเลือกยาที่ใช้ก่อน")
            LoadDataToComboBox()
            Exit Sub
        End If

        ctlMTM.MTM_DrugUse_Save(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(lblSEQ.Text), StrNull2Zero(cboDrugUse.Value), "", txtFQ.Text, 0, Session("Username"), FORM_TYPE_ID_MTM, lblLocationID.Text, "A", StrNull2Long(Session("patientid")))

        txtFQ.Text = ""
        cboDrugUse.SelectedIndex = 0
        LoadDrugUse()
        LoadDataToComboBox()
    End Sub
    Private Sub LoadDrugUse()
        dt = ctlMTM.MTM_DrugUse_Get(StrNull2Zero(Session("patientid")))
        grdDrugUse.DataSource = dt
        grdDrugUse.DataBind()
    End Sub

    Private Sub grdDrugUse_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDrugUse.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Private Sub grdHospital_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdHospital.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Private Sub grdDrugUse_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDrugUse.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel_DU"
                    ctlMTM.MTM_DrugUse_Delete(e.CommandArgument())
                    LoadDrugUse()
                    LoadDataToComboBox()
            End Select
        End If
    End Sub

    Private Sub grdHospital_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdHospital.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel_H"
                    ctlMTM.MTM_Hospital_Delete(e.CommandArgument())
                    LoadHospital()
                    LoadDataToComboBox()
            End Select
        End If
    End Sub

    Protected Sub optMTMType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optMTMType.SelectedIndexChanged

        lblSEQ.Text = (ctlMTM.MTM_Master_GetLastSEQ(Session("LocationID"), Session("patientid"), optMTMType.SelectedValue) + 1).ToString
        BindPatientFromList()
        LoadDataToComboBox()
    End Sub

    Private Sub BindPatientFromList()
        optPatientFrom.Items.Clear()

        Select Case optMTMType.SelectedValue
            Case "MTM" 'MTM
                optPatientFrom.Items.Add("Walk in")
                optPatientFrom.Items(0).Value = "1"
                optPatientFrom.Items.Add("จากหน่วยบริการ")
                optPatientFrom.Items(1).Value = "2"
                optPatientFrom.Items.Add("อื่นๆ")
                optPatientFrom.Items(2).Value = "9"
            Case "HV" 'Visit
                optPatientFrom.Items.Add("จากหน่วยบริการ")
                optPatientFrom.Items(0).Value = "2"
                optPatientFrom.Items.Add("ร้านเยี่ยมเอง")
                optPatientFrom.Items(1).Value = "3"
                optPatientFrom.Items.Add("อื่นๆ")
                optPatientFrom.Items(2).Value = "9"

        End Select
    End Sub

    Protected Sub cmdSaveDrug_Click(sender As Object, e As EventArgs) Handles cmdSaveDrug.Click
        If optMTMType.SelectedIndex = -1 Then
            DisplayMessage(Me.Page, "กรุณาเลือกประเภทของการให้บริการก่อน ")
            Exit Sub
        End If

        ctlMTM.MTM_UpdateHerb(StrNull2Zero(lblMTMUID.Text), txtDrug1.Text, txtDrug2.Text, txtDrug3.Text, txtDrug4.Text, "", "", "")

        Response.Redirect("FormList_HomeVisit.aspx")
        LoadDataToComboBox()
    End Sub

    Protected Sub cmdBehaviorCancel_Click(sender As Object, e As EventArgs) Handles cmdBehaviorCancel.Click
        ClearTextBehavior()
        LoadDataToComboBox()
    End Sub

    Protected Sub cmdDrugProblemCancel_Click(sender As Object, e As EventArgs) Handles cmdDrugProblemCancel.Click
        ClearTextDrugProblem()
        LoadDataToComboBox()
    End Sub
    Private Function ValidateDataMedEx() As Boolean
        sAlert = ""
        isValid = True

        If cboDrugRemain.Text = "" Then
            sAlert &= "- กรุณาเลือกรายการยาก่อน <br/>"
            isValid = False
        End If

        If StrNull2Zero(txtQTYRemain.Text) = 0 Then
            sAlert &= "- กรุณาระบุจำนวนก่อน <br/>"
            isValid = False
        End If

        If txtUOMRemain.Text = "" Then
            sAlert &= "- กรุณาระบุหน่วยนับก่อน <br/>"
            isValid = False
        End If

        Return isValid
    End Function
    Protected Sub cmdSaveMedEx_Click(sender As Object, e As EventArgs) Handles cmdDrugRemain.Click
        If Not ValidateDataMedEx() Then
            lblRemainAlert.Text = "<b>กรุณากรอกข้อมูลให้ครบถ้วน</b> <br/>" & sAlert
            lblRemainAlert.Visible = True
            pnDrugRemainAlert.Visible = True
            LoadDataToComboBox()
            Exit Sub
        Else
            pnDrugRemainAlert.Visible = False
        End If

        ctlMTM.MTM_DrugRemain_Save(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(lblUID_Drug.Text), FORM_TYPE_ID_MTM, StrNull2Zero(cboDrugRemain.Value), txtQTYRemain.Text, txtUOMRemain.Text, Session("patientid"), Session("username"))

        ClearTextDrugRemain()
        LoadDrugRemainToGrid()
        'DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อย")

        pnDrugRemainAlert.Visible = False
        LoadDataToComboBox()
    End Sub
    Private Sub ClearTextDrugRemain()
        lblDrugRemainID.Text = "0"
        cboDrugRemain.Value = ""
        txtQTYRemain.Text = ""
        txtUOMRemain.Text = ""
    End Sub
    Private Sub LoadDrugRemainToGrid()
        dt = ctlMTM.MTM_DrugRemain_Get(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(Session("patientid")))
        grdDrugRemain.DataSource = dt
        grdDrugRemain.DataBind()
        pnDrugRemainAlert.Visible = False
    End Sub


    Protected Sub grdDrugRemain_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDrugRemain.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(6).FindControl("imgDel_Ex")
            imgD.Attributes.Add("onClick", scriptString)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub
    Protected Sub grdDrugRemain_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDrugRemain.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit_Ex"
                    EditDrugRemain(e.CommandArgument())
                Case "imgDel_Ex"
                    ctlMTM.MTM_DrugRemain_Delete(StrNull2Zero(e.CommandArgument()))
                    LoadDrugRemainToGrid()
                    LoadDataToComboBox()
            End Select
        End If
    End Sub
    Private Sub EditDrugRemain(UID As Integer)
        LoadMedicine()
        LoadDeseaseToDDL()

        dt = ctlMTM.MTM_DrugRemain_GetByUID(UID)
        If dt.Rows.Count > 0 Then
            lblUID_Drug.Text = dt.Rows(0)("UID")
            cboDrugRemain.Value = DBNull2Str(dt.Rows(0)("DrugUID"))
            txtQTYRemain.Text = DBNull2Str(dt.Rows(0)("QTY"))
            txtUOMRemain.Text = DBNull2Str(dt.Rows(0)("UOM"))
            pnDrugRemainAlert.Visible = False
            cmdDrugRemain.Text = "บันทึกการแก้ไข"
        End If
    End Sub
End Class