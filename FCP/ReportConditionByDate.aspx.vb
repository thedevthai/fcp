﻿Public Class ReportConditionByDate
    Inherits System.Web.UI.Page
    Dim dt As New DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim m, y As Integer
            y = Year(Date.Now)
            m = Month(Date.Now)
            If y < 2300 Then
                y = y + 543
            End If

            txtStartDate.Text = "01/" & m & "/" & y 'Date.Now.AddDays(-30).ToString("dd/MM/yyyy", DateFormat_TH)
            txtEndDate.Text = Date.DaysInMonth(y, m) & "/" & m & "/" & y  'Date.Now.ToString("dd/MM/yyyy", DateFormat_TH)
            Select Case Request("ItemType")
                Case "use"
                    lblReportHeader.Text = "รายงานการจ่ายยา"
                Case "vmi2"
                    lblReportHeader.Text = "Stok Report "
                Case Else
                   lblReportHeader.Text = "รายงานจำนวนกิจกรรมแยกตามเขต สปสช."
            End Select


        End If

    End Sub
    Protected Sub cmdPrint_Click(sender As Object, e As EventArgs) Handles cmdPrint.Click

        Select Case Request("ItemType")
            Case "vmi1"
                FagRPT = "VMI"
                Reportskey = "XLS"
                ReportsName = "StockInventoryUsage"
           
            Case Else
                FagRPT = "NHSO"
                Reportskey = "XLS"
                ReportsName = "OrderNHSOLocationCount"
        End Select


        Dim dStart As String = ConvertStrDate2DBString(txtStartDate.Text)
        Dim dEnd As String = ConvertStrDate2DBString(txtEndDate.Text)
        Dim pLocationID As String

        If Request.Cookies("LocationID").Value <> "CPA" Then
            pLocationID = Request.Cookies("LocationID").Value
        Else
            pLocationID = ""
        End If

        Response.Redirect("ReportViewer.aspx?b=" & dStart & "&e=" & dEnd & "&y=" & dStart & "_" & dEnd & "&l=" & pLocationID)

    End Sub

    Protected Sub cmdExcel_Click(sender As Object, e As EventArgs) Handles cmdExcel.Click

       
        Select Case Request("ItemType")
            Case "use"
                FagRPT = "VMI"
                Reportskey = "XLS"
                ReportsName = "StockInventoryUsage"
            Case "vmi2"
                FagRPT = "VMI"
                Reportskey = "XLS"
                ReportsName = "VMISmokingStock"
            Case Else
                FagRPT = "NHSO"
                Reportskey = "XLS"
                ReportsName = "OrderNHSOLocationCount"
        End Select


        Dim dStart As String = ConvertStrDate2DBString(txtStartDate.Text)
        Dim dEnd As String = ConvertStrDate2DBString(txtEndDate.Text)
        Dim pLocationID As String

        If Request.Cookies("LocationID").Value <> "CPA" Then
            pLocationID = Request.Cookies("LocationID").Value
        Else
            pLocationID = ""
        End If

        Response.Redirect("ReportViewer.aspx?b=" & dStart & "&e=" & dEnd & "&y=" & dStart & "_" & dEnd & "&l=" & pLocationID)


    End Sub

End Class