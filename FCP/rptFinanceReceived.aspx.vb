﻿Public Class rptFinanceReceived
    Inherits System.Web.UI.Page
    Dim dt As New DataTable 

    Dim ctlOrder As New OrderController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblStartDate.Text = Request("b")
        lblEndDate.Text = Request("e")

        LoadDataToGrid(Request("p"))

        If Request("ex") = 1 Then
            Response.AppendHeader("content-disposition", "attachment;filename=" & ReportsName & ".xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
        End If

    End Sub
    Private Sub LoadDataToGrid(ByVal Pdate As String)

        Dim PayDate As String = "0"

        PayDate = ConvertStrDate2DBString(Pdate)

        dt = ctlOrder.Order_Get4ReceivedComplete(Request("pv"), Request("t"), StrNull2Zero(PayDate), 3)

        If dt.Rows.Count > 0 Then
            lblCount.Text = dt.Rows.Count

            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                For i = 0 To grdData.Rows.Count - 1
                    .Rows(i).Cells(0).Text = i + 1
                    .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(String.Concat(dt.Rows(i)("PayDate")))
                    .Rows(i).Cells(7).Text = DisplayGenderName(dt.Rows(i)("Gender"))
                Next
            End With
        Else
            lblCount.Text = 0
            grdData.Visible = False
            grdData.DataSource = Nothing

        End If

        dt = Nothing
    End Sub

End Class