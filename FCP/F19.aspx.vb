﻿
Public Class F19
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlLct As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlOrder As New OrderController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then
            If Request("acttype") = "view" Then
                cmdSave.Visible = False
            Else
                cmdSave.Visible = True
            End If
            txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)

            If Request("t") <> "new" Then
                If Request("fid") <> "0" Then
                    LoadFormData()
                End If
            End If

            LoadPharmacist(Request.Cookies("LocationID").Value)
        End If

        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")


    End Sub
    Private Sub LoadPharmacist(LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With
            End If
        End If
        dtP = Nothing
    End Sub
    Private Sub LoadLocationData(LocationID As String)
        dt = ctlLct.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblLocationID.Text = .Item("LocationID")
                lblLocationName.Text = .Item("LocationName")
                lblLocationProvince.Text = .Item("ProvinceName")
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub CalScore_Q()
        Dim iScore As Integer = 0
        iScore = StrNull2Zero(optQ1.SelectedValue) +
                 StrNull2Zero(optQ2A.SelectedValue) +
                 StrNull2Zero(optQ2B.SelectedValue) +
                 StrNull2Zero(optQ2C.SelectedValue) +
                 StrNull2Zero(optQ3.SelectedValue) +
                 StrNull2Zero(optQ4.SelectedValue) +
                 StrNull2Zero(optQ5.SelectedValue) +
                 StrNull2Zero(optQ6.SelectedValue) +
                 StrNull2Zero(optQ7.SelectedValue) +
                 StrNull2Zero(optQ8.SelectedValue) +
                 StrNull2Zero(optQ9.SelectedValue) +
                 StrNull2Zero(optQ10.SelectedValue)

        lblQTotal.Text = iScore.ToString()
        lblQResultTxt.Text = ""
        lblQResultTxt.Visible = True
        If iScore <= 7 Then
            lblQResultTxt.Text = "(ผู้ดื่มแบบเสี่ยงต่ำ)"
        ElseIf iScore > 7 And iScore <= 15 Then
            lblQResultTxt.Text = "(ผู้ดื่มแบบเสี่ยง)"
        ElseIf iScore > 15 And iScore <= 19 Then
            lblQResultTxt.Text = "(ผู้ดื่มแบบอันตราย)"
        ElseIf iScore >= 20 Then
            lblQResultTxt.Text = "(ผู้ดื่มแบบติด)"
        End If

    End Sub


    Private Sub LoadFormData()
        'Dim pYear As Integer
        'If Not Request("fid") Is Nothing Then
        '    dt = ctlOrder.GetOrder_ByID(StrNull2Zero(Request("fid")))
        'Else
        '    If Not Request("yid") Is Nothing Then
        '        pYear = CInt(Request("yid"))
        '    Else
        '        pYear = Year(ctlLct.GET_DATE_SERVER)
        '        If pYear < 2500 Then
        '            pYear = pYear + 543
        '        End If

        '    End If
        'dt = ctlOrder.LocationOrder_ByYear(pYear, FORM_TYPE_ID_F19, Request.Cookies("LocationID").Value, Session("patientid"))
        'End If

        dt = ctlOrder.F19_GetByUID(Request("fid"))

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Session("patientid") = DBNull2Lng(.Item("PatientID"))
                lblID.Text = .Item("UID")
                'Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                txtTime.Text = String.Concat(.Item("ServiceTime"))
                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))

                chkClose.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("CloseStatus")))

                optQ1.SelectedValue = DBNull2Str(.Item("Q1"))
                optQ2A.SelectedValue = DBNull2Str(.Item("Q2A"))
                optQ2B.SelectedValue = DBNull2Str(.Item("Q2B"))
                optQ2C.SelectedValue = DBNull2Str(.Item("Q2C"))
                optQ3.SelectedValue = DBNull2Str(.Item("Q3"))
                optQ4.SelectedValue = DBNull2Str(.Item("Q4"))
                optQ5.SelectedValue = DBNull2Str(.Item("Q5"))
                optQ6.SelectedValue = DBNull2Str(.Item("Q6"))
                optQ7.SelectedValue = DBNull2Str(.Item("Q7"))
                optQ8.SelectedValue = DBNull2Str(.Item("Q8"))
                optQ9.SelectedValue = DBNull2Str(.Item("Q9"))
                optQ10.SelectedValue = DBNull2Str(.Item("Q10"))
                lblQTotal.Text = DBNull2Str(.Item("TotalQ"))

                CalScore_Q()

                'chkEducate1.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate1")))
                'chkEducate2.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate2")))

                chkAgree.Checked = ConvertYesNo2Boolean(.Item("isAgree"))


                If DBNull2Zero(.Item("CloseStatus")) >= 3 Then
                    cmdSave.Visible = False
                Else
                    If Request.Cookies("LocationID").Value <> String.Concat(.Item("LocationID")) Then
                        LoadLocationData(String.Concat(.Item("LocationID")))
                        If Request("t") = "edit" Then
                            cmdSave.Visible = True
                        Else
                            cmdSave.Visible = False
                        End If
                    Else
                        LoadLocationData(Request.Cookies("LocationID").Value)
                    End If
                End If


            End With
        Else
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing

    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If


        If StrNull2Zero(txtTime.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนระยะเวลาในการให้บริการก่อน');", True)
            Exit Sub
        End If


        If optQ1.SelectedValue = Nothing Or optQ2A.SelectedValue = Nothing Or optQ2B.SelectedValue = Nothing Or optQ2C.SelectedValue = Nothing Or optQ3.SelectedValue = Nothing Or optQ4.SelectedValue = Nothing Or optQ5.SelectedValue = Nothing Or optQ6.SelectedValue = Nothing Or optQ7.SelectedValue = Nothing Or optQ8.SelectedValue = Nothing Or optQ9.SelectedValue = Nothing Or optQ10.SelectedValue = Nothing Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาประเมิน Q ให้ครบทุกข้อ');", True)
            Exit Sub
        End If




        'If (chkEducate1.Checked = False) And (chkEducate2.Checked = False) Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาการสรุปผลของเภสัชกรก่อน');", True)
        '    Exit Sub
        'End If

        If chkAgree.Checked = False Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกให้คำยินยอมก่อน');", True)
            Exit Sub
        End If


        Dim objuser As New UserController
        Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))

        If lblID.Text = "" Then 'Add new
            If ctlOrder.Order_ChkDupCustomer(FORM_TYPE_ID_F19, Session("patientid"), StrNull2Zero(Left(Str(ServiceDate), 4))) = True Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ผู้รับบริการรายนี้เคยได้รับบริการในกิจกรรมนี้แล้ว');", True)
                Exit Sub
            End If
            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Service_Order", "แบบคัดกรอง/แบบประเมิน โรคซึมเศร้าและความเครียด (F19):" & Session("patientname"), "F19")
        Else
            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Service_Order", "แบบคัดกรอง/แบบประเมิน โรคซึมเศร้าและความเครียด (F19):" & Session("patientname"), "F19")
        End If
        ctlOrder.F19_Save(StrNull2Long(lblID.Text), lblLocationID.Text, Session("patientid"), ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), optQ1.SelectedValue, optQ2A.SelectedValue, optQ2B.SelectedValue, optQ2C.SelectedValue, optQ3.SelectedValue, optQ4.SelectedValue, optQ5.SelectedValue, optQ6.SelectedValue, optQ7.SelectedValue, optQ8.SelectedValue, optQ9.SelectedValue, optQ10.SelectedValue, lblQTotal.Text, ConvertStatus2YN(chkAgree.Checked), Convert2Status(chkClose.Checked), Request.Cookies("username").Value)

        Response.Redirect("ResultPage.aspx?p=F19")

    End Sub

    Protected Sub optQ1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ1.SelectedIndexChanged
        CalScore_Q()
    End Sub

    Protected Sub optQ2A_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ2A.SelectedIndexChanged
        CalScore_Q()
    End Sub
    Protected Sub optQ2B_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ2B.SelectedIndexChanged
        CalScore_Q()
    End Sub
    Protected Sub optQ2C_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ2C.SelectedIndexChanged
        CalScore_Q()
    End Sub

    Protected Sub optQ3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ3.SelectedIndexChanged
        CalScore_Q()
    End Sub

    Protected Sub optQ4_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ4.SelectedIndexChanged
        CalScore_Q()
    End Sub

    Protected Sub optQ5_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ5.SelectedIndexChanged
        CalScore_Q()
    End Sub

    Protected Sub optQ6_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ6.SelectedIndexChanged
        CalScore_Q()
    End Sub

    Protected Sub optQ7_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ7.SelectedIndexChanged
        CalScore_Q()
    End Sub

    Protected Sub optQ8_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ8.SelectedIndexChanged
        CalScore_Q()
    End Sub

    Protected Sub optQ9_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ9.SelectedIndexChanged
        CalScore_Q()
    End Sub
    Protected Sub optQ10_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ10.SelectedIndexChanged
        CalScore_Q()
    End Sub

End Class