﻿Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms

Partial Public Class rptReceivedView
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlO As New OrderController
    Dim RPTKEY As String
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then

            RPTKEY = ""
            If Request("ItemType") = "smk" Then
                LoadInvoiceNo(PROJECT_SMOKING)
            ElseIf Request("ItemType") = "smk2" Then
                RPTKEY = "STOPDAY"
                LoadInvoiceNo(0)
            Else
                LoadInvoiceNo(PROJECT_F)
            End If 
        End If
    End Sub
    'Private Sub InitRptPreview()
    '    With ReportViewer1
    '        .ShowExportControls = True
    '        .ShowPageNavigationControls = True
    '        .ShowPrintButton = True
    '        .ShowRefreshButton = True
    '    End With

    'End Sub
    Private Sub LoadInvoiceNo(ProjID As Integer)

        If RPTKEY = "STOPDAY" Then
            dt = ctlO.LoadInvoiceNo_StopDay()
        Else
            dt = ctlO.LoadInvoiceNo(ProjID)
        End If


        ddlInvoiceNo.Items.Clear()
        If dt.Rows.Count > 0 Then
            With ddlInvoiceNo
                .Enabled = True
                .Items.Add("---ทั้งหมด---")
                .Items(0).Value = "0"
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(DBNull2Str(dt.Rows(i)("InvoiceNo")))
                    .Items(i + 1).Value = DBNull2Str(dt.Rows(i)("InvoiceNo"))
                Next
                .SelectedIndex = 0

            End With
        End If
        dt = Nothing

    End Sub

    Private Sub LoadReport()

        System.Threading.Thread.Sleep(1000)
        UpdateProgress1.Visible = True

        Dim credential As New ReportServerCredentials
        ReportViewer1.Reset()
        ReportViewer1.ServerReport.ReportServerCredentials = credential
        ReportViewer1.ServerReport.ReportServerUrl = credential.ReportServerUrl
        ReportViewer1.ShowPrintButton = True
        ReportViewer1.Height = 500
        'Me.ReportViewer1.SetDisplayMode(DisplayMode.PrintLayout)

        'System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("th-TH")
        Dim xParam As New List(Of ReportParameter)
        Dim ReportName As String = ""

        If Request("ItemType") = "smk" Then
            ReportName = "SmokingOrderReceived"
        ElseIf Request("ItemType") = "smk2" Then
            ReportName = "SmokingOrderReceivedStopDay"
        Else
            ReportName = "OrderReceived"
        End If

        ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportName)


        xParam.Add(New ReportParameter("InvoiceNo", ddlInvoiceNo.SelectedValue))

        ReportViewer1.ServerReport.SetParameters(xParam)
        ReportViewer1.ServerReport.Refresh()
        'If FagRPT <> "CHKLIST" Then
        '    ' Variables
        '    Dim warnings As Warning()
        '    Dim streamIds As String()
        '    Dim mimeType As String = String.Empty
        '    Dim encoding As String = String.Empty
        '    Dim extension As String = String.Empty


        '    ' Setup the report viewer object and get the array of bytes

        '    Dim bytes As Byte() = ReportViewer1.ServerReport.Render("PDF", Nothing, mimeType, encoding, extension, streamIds, Nothing)

        '    ' Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
        '    Response.Buffer = True
        '    Response.Clear()
        '    Response.ContentType = mimeType
        '    'Response.AddHeader("content-disposition", Convert.ToString("attachment; filename=C:\ttt.pdf"))
        '    Response.BinaryWrite(bytes)
        '    ' create the file
        '    Response.Flush()
        '    ' send it to the client to download
        'Else
        '    ' Variables
        '    Dim warnings As Warning()
        '    Dim streamIds As String()
        '    Dim mimeType As String = String.Empty
        '    Dim encoding As String = String.Empty
        '    Dim extension As String = String.Empty


        '    ' Setup the report viewer object and get the array of bytes

        '    Dim bytes As Byte() = ReportViewer1.ServerReport.Render("EXCEL", Nothing, mimeType, encoding, extension, streamIds, Nothing)

        '    ' Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
        '    Response.Buffer = True
        '    Response.Clear()
        '    Response.ContentType = mimeType
        '    Response.AddHeader("content-disposition", Convert.ToString("attachment; filename=CheckUpList_" & ConvertStrDate2DBString(Request("b")) & "_" & ConvertStrDate2DBString(Request("e")) & "." & extension))
        '    Response.BinaryWrite(bytes)
        '    ' create the file
        '    Response.Flush()
        '    ' send it to the client to download
        'End If



        'Dim ctlOrder As New OrderController
        'Dim InvoiceNo As String

        'InvoiceNo = ddlInvoiceNo.SelectedValue

        'ReportViewer1.LocalReport.DataSources.Clear()

        ''InitRptPreview()

        'Dim adapter As New OrderReceivedDataSetTableAdapters.ServiceOrder_GetTableAdapter
        'Dim table As New OrderReceivedDataSet.ServiceOrder_GetDataTable

        ''EndDate = CDate(EndDate).AddDays(1)
        ''adapter.FillData(table, ConvertStrDate2DateQueryString(StartDate), ConvertStrDate2DateQueryString(EndDate), ProvinceID)

        'adapter.FillData(table, InvoiceNo)

        'ReportViewer1.LocalReport.DataSources.Add(New ReportDataSource("OrderReceivedDataSet", CType(table, DataTable)))
        'ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/" & "Reports/" & "rptOrderReceived.rdlc")

        ''Dim paraStartDate As String = Convert.ToDateTime(StartDate).ToShortDateString()
        'Dim paraINV As String = ddlInvoiceNo.SelectedValue 'Convert.ToDateTime(EndDate).ToShortDateString()
        'Dim param As ReportParameter() = New ReportParameter(0) {}
        'param(0) = New ReportParameter("INVNO", paraINV, False)
        'param(1) = New ReportParameter("ToDate", paraEndDate, False)
        'Me.ReportViewer1.LocalReport.SetParameters(param)
        'ReportViewer1.LocalReport.Refresh()

        ' ReportViewer1.DataBind()
        ' ReportViewer1.LocalReport.Refresh()
        UpdateProgress1.Visible = False
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        LoadReport()
    End Sub
End Class