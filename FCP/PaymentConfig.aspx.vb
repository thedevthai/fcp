﻿
Public Class PaymentConfig
    Inherits System.Web.UI.Page

    Dim dt As New DataTable

    Dim acc As New UserController
    Dim ctlPay As New PaymentController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If IsNothing(Request.Cookies("FCPCPA")) Then
        '    Response.Redirect("Default.aspx?logout=YES")
        'End If
        If Not IsPostBack Then
            grdData.PageIndex = 0
            txtCode.Text = ""
            LoadProjectToDDL()
            LoadProvince()
            LoadPaymentMethod()
            LoadPaymentConfig()

        End If

    End Sub
    Private Sub LoadPaymentMethod()
        dt = ctlPay.Payment_GetAllMethod
        If dt.Rows.Count > 0 Then
            With ddlPayment
                .DataSource = dt
                .DataTextField = "PaymentName"
                .DataValueField = "PaymentID"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadProjectToDDL()
        Dim ctlPj As New ProjectController
        dt = ctlPj.Project_Get
        If dt.Rows.Count > 0 Then
            With ddlProject
                .DataSource = dt
                .DataTextField = "Description"
                .DataValueField = "ProjectID"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadProvince()
        dt = ctlPay.LoadProvince
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .DataSource = dt
                .DataTextField = "ProvinceName"
                .DataValueField = "ProvinceID"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadPaymentConfig()
        If Trim(txtSearch.Text) <> "" Then
            dt = ctlPay.Payment_GetConfigBySearch(Trim(txtSearch.Text))
        Else
            dt = ctlPay.Payment_GetConfig
        End If

        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                Try

                    Dim nrow As Integer = dt.Rows.Count
                    If .PageCount > 1 Then
                        If .PageIndex > 0 Then
                            If (.PageSize) < nrow - (.PageSize * .PageIndex) Then
                                For i = 0 To .PageSize - 1
                                    .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                Next
                            Else
                                For i = 0 To nrow - (.PageSize * .PageIndex) - 1
                                    .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                Next
                            End If
                        Else
                            For i = 0 To .PageSize - 1
                                .Rows(i).Cells(0).Text = i + 1
                            Next
                        End If
                    Else
                        For i = 0 To nrow - 1
                            .Rows(i).Cells(0).Text = i + 1
                        Next
                    End If
                Catch ex As Exception

                End Try

            End With
        End If
        dt = Nothing
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadPaymentConfig()
    End Sub




    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())

                    'lblCode.Text = grdData.Rows(
                Case "imgDel"
                    ctlPay.Payment_Delete(e.CommandArgument)
                    acc.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_DEL, "Provinces", "Delete PaymentConfig:" & txtCode.Text & ">>" & ddlProvince.SelectedItem.Text, "")
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ลบข้อมูลเรียบร้อย');", True)
                    LoadPaymentConfig()
            End Select


        End If
    End Sub


    Private Sub EditData(ByVal pID As String)

        dt = ctlPay.Payment_GetConfigByID(pID)

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                Me.txtCode.Text = DBNull2Str(dt.Rows(0)("itemID"))
                txtCode.ReadOnly = True
                ddlProvince.SelectedValue = DBNull2Str(dt.Rows(0)("ProvinceID"))
                ddlPayment.SelectedValue = DBNull2Str(dt.Rows(0)("PaymentID"))
                txtEffectiveTo.Text = DisplayStr2ShortDateTH(DBNull2Str(dt.Rows(0)("EffectiveTo")))
                chkActive.Checked = ConvertStatusFlag2Boolean(DBNull2Str(dt.Rows(0)("StatusFlag")))
                ddlProject.SelectedValue = DBNull2Str(dt.Rows(0)("ProjectID"))

            End With
        End If
        dt = Nothing
    End Sub

    Private Sub ClearData() 
        ddlPayment.SelectedIndex = 0
        txtCode.Text = ""
        ddlProvince.SelectedIndex = 0
        txtEffectiveTo.Text = ""

    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(7).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub


    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        'If ddl.Text = "" Then

        '    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณากรอกข้อมูลให้ครบถ้วน');", True)
        '    Exit Sub
        'End If

        If txtCode.Text = "" Then

            ctlPay.Payment_Add(ddlProvince.SelectedValue, ddlPayment.SelectedValue, StrNull2Long(ConvertStrDate2DBString(txtEffectiveTo.Text)), Boolean2StatusFlag(chkActive.Checked), ddlProject.SelectedValue)

            acc.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "PaymentConfig", "Add new Payment Config:" & ddlProvince.SelectedItem.Text, "")

        Else
            ctlPay.Payment_Update(txtCode.Text, ddlProvince.SelectedValue, ddlPayment.SelectedValue, StrNull2Long(ConvertStrDate2DBString(txtEffectiveTo.Text)), Boolean2StatusFlag(chkActive.Checked), ddlProject.SelectedValue)

            acc.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "PaymentConfig", "Update Payment Config:" & ddlProvince.SelectedItem.Text, "")

        End If

        LoadPaymentConfig()
        ClearData()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadPaymentConfig()
    End Sub

End Class

