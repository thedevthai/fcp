﻿Public Class PatientSearch
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim objUser As New UserController
    Dim ctlP As New PatientController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If IsNothing(Request.Cookies("FCPCPA")) Then
        '    Response.Redirect("Default.aspx?logout=YES")
        'End If

        If Not IsPostBack Then
            pnNew.Visible = False
            grdData.PageIndex = 0
            LoadPatientListToGrid()

        End If
    End Sub
    Private Sub LoadPatientListToGrid()

        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            dt = ctlP.Patient_GetByLocation(Request.Cookies("LocationID").Value, Trim(txtSearch.Text), 1)
        ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            dt = ctlP.Patient_GetBySearch(Request.Cookies("RPTGRP").Value, Trim(txtSearch.Text))
        Else
            dt = ctlP.Patient_GetBySearch("", Trim(txtSearch.Text))
        End If

        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                Try
                    For i = 0 To .PageSize - 1

                        Dim btnD As ImageButton = .Rows(i).Cells(6).FindControl("imgDel")

                        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isAdminAccess Then
                            btnD.Visible = True
                        Else
                            btnD.Visible = False
                        End If

                    Next

                Catch ex As Exception

                End Try
                pnNew.Visible = False
            End With
        Else
            grdData.Visible = False
            grdData.DataSource = Nothing
            pnNew.Visible = True
        End If

        dt = Nothing
    End Sub

    Protected Function DateText(ByVal input As String) As String
        Dim dStr As String
        dStr = Replace(DisplayStr2ShortDateTH(input), "/", "-")
        Return dStr
    End Function
    Protected Sub cmdSearch_Click(sender As Object, e As EventArgs) Handles cmdSearch.Click
        grdData.PageIndex = 1
        LoadPatientListToGrid()
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadPatientListToGrid()
    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdData.RowCommand
        Session("patientid") = e.CommandArgument
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgNew"
                    Response.Redirect("FormMenu.aspx?ActionType=frm&PatientID=" & e.CommandArgument)
                Case "imgPt"
                    Response.Redirect("PatientProfile.aspx?PatientID=" & e.CommandArgument)
                Case "imgEdit"
                    Response.Redirect("Patient.aspx?ActionType=pt&PatientID=" & e.CommandArgument)
                Case "imgDel"
                    If ctlP.Patient_Delete(e.CommandArgument) Then

                        objUser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_DEL, "Patient", "ลบ Patient:" & e.CommandArgument, "")

                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ลบข้อมูลเรียบร้อย');", True)
                        LoadPatientListToGrid()
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    End If


            End Select


        End If

        'If TypeOf e.CommandSource Is WebControls.HyperLink Then
        '    Dim ButtonPressed As WebControls.HyperLink = e.CommandSource
        '    Select Case ButtonPressed.ID
        '        Case "lnkPatientID", "lnkCardID", "lnkName"
        '            Session("patientid") = e.CommandArgument

        '            'Case "lnkEdu2"
        '            '    Response.Redirect("F03.aspx?t=new&seq=2&fid=" & DBNull2Zero(e.CommandArgument))
        '            'Case "lnkFollow"
        '            '    Response.Redirect("F11_Follow.aspx?t=new&fid=" & DBNull2Zero(e.CommandArgument))
        '    End Select
        'End If
    End Sub
      
    Protected Sub cmdAddNew_Click(sender As Object, e As EventArgs) Handles cmdAddNew.Click
        Response.Redirect("PatientRegister.aspx")
    End Sub

End Class