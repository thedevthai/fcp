﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucMenuVertical.ascx.vb" Inherits=".ucMenuVertical" %>
<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    <% If Request("actionType") = "h"  %>
    <li class="active"><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i><span>หน้าแรก</span></a></li>
    <% Else %>
    <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i><span>หน้าแรก</span></a></li>
    <% End If %>    
    
    <% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isSuperAdminAccess Then%>
         <% If Request.QueryString("ActionType") = "reg" Then%>
        <li class="active"><a href="RegisterList.aspx?ActionType=reg"><i class="fa fa-registered"></i><span>รายการลงทะเบียน</span></a></li>
        <% Else%>
        <li><a href="RegisterList.aspx?ActionType=reg"><i class="fa fa-registered"></i><span>รายการลงทะเบียน</span></a></li>
        <% End If%>
    <% End If %>

    <% If Request.QueryString("ActionType") = "pt" Then%>
    <li class="active"><a href="Patient.aspx?ActionType=pt&ItemType=pt"><i class="fa fa-user"></i><span>Patient</span></a></li>
    <% Else%>
    <li><a href="Patient.aspx?ActionType=pt&ItemType=pt"><i class="fa fa-user"></i><span>Patient</span></a></li>
    <% End If%>

    <% If Request.QueryString("ActionType") = "ptl" Then%>
    <li class="active"><a class="active" href="PatientSearch.aspx?ActionType=ptl&ItemType=list"><i class="fa fa-users"></i><span>Patient List</span></a></li>
    <% Else%>
    <li><a href="PatientSearch.aspx?ActionType=ptl&ItemType=list"><i class="fa fa-users"></i><span>Patient List</span></a></li>
    <% End If%>

    <% If Request.QueryString("ActionType") = "pt2" Then%>
    <li class="active"><a class="active" href="PatientList.aspx?ActionType=pt2"><i class="fa fa-fire"></i><span>ผู้ที่อยากเลิกบุหรี่</span></a></li>
    <% Else%>
    <li><a href="PatientList.aspx?ActionType=pt2"><i class="fa fa-fire"></i><span>ผู้ที่อยากเลิกบุหรี่</span></a></li>
    <% End If%>


    <% If Request.QueryString("ActionType") = "act" Then  %>
    <li class="active treeview">
        <% Else %>
    <li class="treeview">
        <% End If %>
        <a href="#">
            <i class="fa fa-dashboard"></i><span>รายการกิจกรรม</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <% If Request("ItemType") = "f"  %>
            <li class="active"><a href="FormList.aspx?ActionType=act&ItemType=f"><i class="fa fa-facebook"></i>โครงการให้คำปรึกษา</a></li>
            <% Else %>
            <li><a href="FormList.aspx?ActionType=act&ItemType=f"><i class="fa fa-facebook"></i>โครงการให้คำปรึกษา</a></li>
            <% End If %>


            <% If Request("ItemType") = "smk"  %>
            <li class="active"><a href="FormList_Smoking.aspx?ActionType=act&ItemType=smk"><i class="fa fa-font"></i>โครงการบุหรี่</a></li>
            <% Else %>
            <li><a href="FormList_Smoking.aspx?ActionType=act&ItemType=smk"><i class="fa fa-font"></i>โครงการบุหรี่</a></li>
            <% End If %>

            <% If Request("ItemType") = "mtm"  %>
            <li class="active"><a href="FormList_HomeVisit.aspx?ActionType=act&ItemType=mtm"><i class="fa fa-header"></i>โครงการเยี่ยมบ้านและMTM</a></li>
            <% Else %>
            <li><a href="FormList_HomeVisit.aspx?ActionType=act&ItemType=mtm"><i class="fa fa-header"></i>โครงการเยี่ยมบ้านและMTM</a></li>
            <% End If %>
        </ul>
    </li>


    <% If Request.QueryString("ActionType") = "ref" Then  %>
    <li class="active treeview">
        <% Else %>
    <li class="treeview">
        <% End If %>
        <a href="#">
            <i class="fa fa-registered"></i><span>Refer</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <% If Request("ItemType") = "s"  %>
            <li class="active"><a href="PatientServiceList.aspx?ActionType=ref&ItemType=s"><i class="fa fa-share"></i>Refer ไปหน่วยอื่น </a></li>
            <% Else %>
            <li><a href="PatientServiceList.aspx?ActionType=ref&ItemType=s"><i class="fa fa-share"></i>Refer ไปหน่วยอื่น </a></li>
            <% End If %>


            <% If Request("ItemType") = "r"  %>
            <li class="active"><a href="404.aspx?ActionType=ref&ItemType=r"><i class="fa fa-reply-all"></i>รายการรับ Refer</a></li>
            <% Else %>
            <li><a href="404.aspx?ActionType=ref&ItemType=r"><i class="fa fa-reply-all"></i>รายการรับ Refer</a></li>
            <% End If %>
        </ul>
    </li>

    <% If Request.QueryString("ActionType") = "cov" Then%>
    <li class="active"><a href="PatientCovid.aspx?ActionType=cov"><i class="fa fa-copyright"></i><span>Home Isolation COVID</span></a></li>
    <% Else%>
    <li><a href="PatientCovid.aspx?ActionType=cov"><i class="fa fa-copyright"></i><span>Home Isolation COVID</span></a></li>
    <% End If%>
    <% If Request.QueryString("ActionType") = "cov2" Then%>
    <li class="active"><a href="PatientCovid2.aspx?ActionType=cov2"><i class="fa fa-copyright"></i><span>Long COVID</span><span class="pull-right-container">
             
    </span></a></li>
    <% Else%>
    <li><a href="PatientCovid2.aspx?ActionType=cov2"><i class="fa fa-copyright"></i><span>Long COVID</span><span class="pull-right-container">
            
    </span></a></li>
    <% End If%>
    <% If Request.QueryString("ActionType") = "cov3" Then%>
    <li class="active"><a href="PatientCovid3.aspx?ActionType=cov3"><i class="fa fa-copyright"></i><span>Self Isolation Care</span><span class="pull-right-container">
        <small class="label pull-right bg-green">new</small>
    </span></a></li>
    <% Else%>
    <li><a href="PatientCovid3.aspx?ActionType=cov3"><i class="fa fa-copyright"></i><span>Self Isolation Care</span><span class="pull-right-container">
        <small class="label pull-right bg-green">new</small>
    </span></a></li>
    <% End If%>


    <% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then%>

    <% If Request.QueryString("ActionType") = "frm" Then%>
    <li class="active"><a href="FormMenu.aspx?ActionType=frm&ItemType=frm"><i class="fa fa-check"></i><span>บันทึกกิจกรรม</span></a></li>
    <% Else%>
    <li><a href="FormMenu.aspx?ActionType=frm&ItemType=frm"><i class="fa fa-check"></i><span>บันทึกกิจกรรม</span></a></li>
    <% End If%>

    <% End If%>

    <% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isSuperAdminAccess Then%>

    <% If Request.QueryString("ActionType") = "doc" Then%>
    <li class="active"><a href="FormDoc.aspx?ActionType=doc&ItemType=list"><i class="fa fa-file-text"></i><span>รายการเอกสารตอบกลับ</span></a> </li>
    <% Else%>
    <li><a href="FormDoc.aspx?ActionType=doc&ItemType=list"><i class="fa fa-file-text"></i><span>รายการเอกสารตอบกลับ</span></a> </li>
    <% End If%>

    <% If Request.QueryString("ActionType") = "inv" Then  %>
    <li class="active treeview">
        <% Else %>
    <li class="treeview">
        <% End If %>
        <a href="#">
            <i class="fa fa-dollar"></i><span>บันทึกชำระเงิน</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <% If Request("ItemType") = "f" Then%>
            <li class="active"><a href="FormReceived.aspx?ActionType=inv&ItemType=f"><i class="fa fa-dollar"></i>โครงการให้คำปรึกษา </a></li>
            <% Else %>
            <li><a href="FormReceived.aspx?ActionType=inv&ItemType=f"><i class="fa fa-dollar"></i>โครงการให้คำปรึกษา</a></li>
            <% End If %>

            <% If Request("ItemType") = "smk" Then%>
            <li class="active"><a href="FormReceived_Smoking.aspx?ActionType=inv&ItemType=smk"><i class="fa fa-cny"></i>โครงการบุหรี่</a></li>
            <% Else %>
            <li><a href="FormReceived_Smoking.aspx?ActionType=inv&ItemType=smk"><i class="fa fa-cny"></i>โครงการบุหรี่</a></li>
            <% End If %>

            <% If Request("ItemType") = "smk2" Then%>
            <li class="active"><a href="FormReceived_SmokingStop.aspx?ActionType=inv&ItemType=smk2"><i class="fa fa-money"></i>โครงการบุหรี่จ่ายหยุดสูบ</a></li>
            <% Else %>
            <li><a href="FormReceived_SmokingStop.aspx?ActionType=inv&ItemType=smk2"><i class="fa fa-money"></i>โครงการบุหรี่จ่ายหยุดสูบ</a></li>
            <% End If %>
        </ul>
    </li>

    <% End If%>

    <% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then%>

    <% If Request("ItemType") = "shop" Then%>
    <li class="active"><a href="LocationsEdit.aspx?ActionType=cus&ItemType=shop"><i class="fa fa-hospital-o"></i><span>ข้อมูลร้านยา</span></a></li>
    <% Else%>
    <li><a href="LocationsEdit.aspx?ActionType=cus&ItemType=shop"><i class="fa fa-hospital-o"></i><span>ข้อมูลร้านยา</span></a></li>
    <% End If%>

    <% End If%>

    <% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isAdminAccess Or Convert.ToInt32(Request.Cookies("RoleID").Value) = isSuperAdminAccess Or (Convert.ToInt32(Request.Cookies("RoleID").Value) = isProjectManager And Request.Cookies("PRJMNG").Value = PROJECT_SMOKING) Then%>

    <% If Request.QueryString("ActionType") = "shop" Then  %>
    <li class="active treeview">
        <% Else %>
    <li class="treeview">
        <% End If %>
        <a href="#">
            <i class="fa fa-hospital-o"></i><span>ข้อมูลร้านยา</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isAdminAccess Or Convert.ToInt32(Request.Cookies("RoleID").Value) = isSuperAdminAccess Then%>

            <% If Request("ItemType") = "grp" Then%>
            <li class="active"><a href="LocationGroup.aspx?ActionType=shop&ItemType=grp"><i class="fa fa-home"></i>ประเภทร้านยา </a></li>
            <% Else %>
            <li><a href="LocationGroup.aspx?ActionType=shop&ItemType=grp"><i class="fa fa-home"></i>ประเภทร้านยา</a></li>
            <% End If %>

            <% If Request("ItemType") = "AddShop" Then%>
            <li class="active"><a href="Locations.aspx?ActionType=shop&ItemType=AddShop"><i class="fa fa-hospital-o"></i>ร้านยา</a></li>
            <% Else %>
            <li><a href="Locations.aspx?ActionType=shop&ItemType=AddShop"><i class="fa fa-hospital-o"></i>ร้านยา</a></li>
            <% End If %>

            <% End If %>

            <% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isAdminAccess Or Convert.ToInt32(Request.Cookies("RoleID").Value) = isSuperAdminAccess Or (Convert.ToInt32(Request.Cookies("RoleID").Value) = isProjectManager And Request.Cookies("PRJMNG").Value = PROJECT_SMOKING) Then%>
            <% If Request("ItemType") = "AddPM" Then%>
            <li class="active"><a href="Pharmacist.aspx?ActionType=shop&ItemType=AddPM"><i class="fa  fa-user-md"></i>เภสัชกร</a></li>
            <% Else %>
            <li><a href="Pharmacist.aspx?ActionType=shop&ItemType=AddPM"><i class="fa fa-user-md"></i>เภสัชกร</a></li>
            <% End If %>

            <% End If%>
        </ul>
    </li>
    
    <% End If%>

    <% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isAdminAccess Or Convert.ToInt32(Request.Cookies("RoleID").Value) = isSuperAdminAccess Then%>
    <% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isSuperAdminAccess Then%>
       
    <% If Request("ActionType") = "user" Then  %>
    <li class="active treeview">
        <% Else %>
    <li class="treeview">
        <% End If %>
        <a href="#">
            <i class="fa fa-users"></i><span>ข้อมูลผู้ใช้งาน(User)</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">

            <% If Request("ItemType") = "add" Then%>
            <li class="active"><a href="Users.aspx?ActionType=user&ItemType=add"><i class="fa fa-users"></i>เพิ่มผู้ใช้งาน </a></li>
            <% Else %>
            <li><a href="Users.aspx?ActionType=user&ItemType=add"><i class="fa fa-users"></i>เพิ่มผู้ใช้งาน</a></li>
            <% End If %>

            <% If Request("ItemType") = "history" Then%>
            <li class="active"><a href="LogfilesByUser.aspx?ActionType=user&ItemType=history"><i class="fa fa-navicon"></i>ตรวจสอบประวัติการใช้งาน</a></li>
            <% Else %>
            <li><a href="LogfilesByUser.aspx?ActionType=user&ItemType=history"><i class="fa fa-navicon"></i>ตรวจสอบประวัติการใช้งาน</a></li>
            <% End If %>
        </ul>
    </li>

    <% End If%>


    <% If Request("ActionType") = "news" Then  %>
    <li class="active treeview">
        <% Else %>
    <li class="treeview">
        <% End If %>
        <a href="#">
            <i class="fa fa-bullhorn"></i><span>ข่าวประชาสัมพันธ์</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <% If Request("ItemType") = "news" Then%>
            <li class="active"><a href="News_List.aspx?ActionType=news&ItemType=news&patientid=0">รายการข่าวประชาสัมพันธ์ </a></li>
            <% Else %>
            <li><a href="News_List.aspx?ActionType=news&ItemType=news&patientid=0">รายการข่าวประชาสัมพันธ์</a></li>
            <% End If %>

            <% If Request("ItemType") = "addnews" Then%>
            <li class="active"><a href="News_Manage.aspx?ActionType=news&ItemType=addnews&patientid=0">เพิ่มข่าวประชาสัมพันธ์ </a></li>
            <% Else %>
            <li><a href="News_Manage.aspx?ActionType=news&ItemType=addnews&patientid=0">เพิ่มข่าวประชาสัมพันธ์</a></li>
            <% End If %>
        </ul>
    </li>


    <% If Request("ActionType") = "setting" Then  %>
    <li class="active treeview">
        <% Else %>
    <li class="treeview">
        <% End If %>
        <a href="#">
            <i class="fa fa-gears"></i><span>ตั้งค่าระบบ</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">

            <% If Request("ItemType") = "stype" Then%>
            <li class="active"><a href="ServiceType.aspx?ActionType=setting&ItemType=stype">ประเภทกิจกรรม</a></li>
            <% Else %>
            <li><a href="ServiceType.aspx?ActionType=setting&ItemType=stype">ประเภทกิจกรรม</a></li>
            <% End If %>

            <% If Request("ItemType") = "gprov" Then%>
            <li class="active"><a href="ProvincesGroup.aspx?ActionType=setting&ItemType=gprov">กลุ่มจังหวัด/ภาค</a></li>
            <% Else %>
            <li><a href="ProvincesGroup.aspx?ActionType=setting&ItemType=gprov">กลุ่มจังหวัด/ภาค</a></li>
            <% End If %>

            <% If Request("ItemType") = "prov" Then%>
            <li class="active"><a href="Provinces.aspx?ActionType=setting&ItemType=prov">จังหวัด</a></li>
            <% Else %>
            <li><a href="Provinces.aspx?ActionType=setting&ItemType=prov">จังหวัด</a></li>
            <% End If %>
            <% If Request("ItemType") = "bank" Then%>
            <li class="active"><a href="Banks.aspx?ActionType=setting&ItemType=bank">ธนาคาร</a></li>
            <% Else %>
            <li><a href="Banks.aspx?ActionType=setting&ItemType=bank">ธนาคาร</a></li>
            <% End If %>
            <% If Request("ItemType") = "hos" Then%>
            <li class="active"><a href="Hospital.aspx?ActionType=setting&ItemType=hos">โรงพยาบาล</a></li>
            <% Else %>
            <li><a href="Hospital.aspx?ActionType=setting&ItemType=hos">โรงพยาบาล</a></li>
            <% End If %>

            <% If Request("ItemType") = "pcfg" Then%>
            <li class="active"><a href="PaymentConfig2.aspx?ActionType=setting&ItemType=pcfg">กำหนดค่าตอบแทน</a></li>
            <% Else %>
            <li><a href="PaymentConfig2.aspx?ActionType=setting&ItemType=pcfg">กำหนดค่าตอบแทน</a></li>
            <% End If %>

            <% If Request("ItemType") = "med" Then%>
            <li class="active"><a href="Drug.aspx?ActionType=setting&ItemType=med">Drug Master (ยา)</a></li>
            <% Else %>
            <li><a href="Drug.aspx?ActionType=setting&ItemType=med">Drug Master (ยา)</a></li>
            <% End If %>

            <% If Request("ItemType") = "des" Then%>
            <li class="active"><a href="Desease.aspx?ActionType=setting&ItemType=des">โรค</a></li>
            <% Else %>
            <li><a href="Desease.aspx?ActionType=setting&ItemType=des">โรค</a></li>
            <% End If %>

            <% If Request("ItemType") = "icd" Then%>
            <li class="active"><a href="ICD10.aspx?ActionType=setting&ItemType=icd">ICD10</a></li>
            <% Else %>
            <li><a href="ICD10.aspx?ActionType=setting&ItemType=icd">ICD10</a></li>
            <% End If %>
        </ul>
    </li>
    <% End If%>

    <% If Convert.ToInt32(Request.Cookies("RoleID").Value) <> isShopAccess Then%>

    <% If Request.QueryString("ActionType") = "rpt" Then%>
    <li class="active"><a href="ReportMenu.aspx?ActionType=rpt&ItemType=fnc"><i class="fa fa-pie-chart"></i><span>รายงาน</span></a> </li>
    <% Else%>
    <li><a href="ReportMenu.aspx?ActionType=rpt&ItemType=fnc"><i class="fa fa-pie-chart"></i><span>รายงาน</span></a> </li>
    <% End If%>

    <% End If%>


    <% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then%>
    <% If Request("ActionType") = "rpt" Then  %>
    <li class="active treeview">
        <% Else %>
    <li class="treeview">
        <% End If %>
        <a href="#">
            <i class="fa fa-pie-chart"></i><span>รายงาน</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">

            <% If Request("ItemType") = "fnc" Then%>
            <li class="active"><a href="ReportCondition.aspx?ActionType=rpt&ItemType=fnc"><i class="fa fa-bar-chart"></i>รายงานสรุปจำนวนกิจกรรม</a></li>
            <% Else%>
            <li><a href="ReportCondition.aspx?ActionType=rpt&ItemType=fnc"><i class="fa fa-bar-chart"></i>รายงานสรุปจำนวนกิจกรรม</a></li>
            <% End If%>

            <% If Request("ItemType") = "cus" Then%>
            <li class="active"><a href="ReportCondition.aspx?ActionType=rpt&ItemType=cus"><i class="fa fa-pie-chart"></i>ผู้เข้ารับบริการแยกตามกิจกรรม</a></li>
            <% Else%>
            <li><a href="ReportCondition.aspx?ActionType=rpt&ItemType=cus"><i class="fa fa-pie-chart"></i>ผู้เข้ารับบริการแยกตามกิจกรรม</a></li>
            <% End If%>
        </ul>
    </li>
    <% End If%>

    <%--<% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isAdminAccess Or Convert.ToInt32(Request.Cookies("RoleID").Value) = isSuperAdminAccess Then%>
            <li>
                <% If Request.QueryString("ActionType") = "vmi" Then%>
                    <a class="active" href="vmiMenu.aspx?ActionType=vmi&ItemType=vmi"> <i class="fa fa-vimeo"></i><span>VMI</span></a>
                <% Else%>
                    <a href="vmiMenu.aspx?ActionType=vmi&ItemType=vmi"> <i class="fa fa-vimeo"></i><span>VMI</span></a>
                <% End If%>
            </li>
     <% End If%>  
            
      <% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then%>

       <% If Request("ActionType") = "vmi" Then  %>
        <li class="active treeview">
     <% Else %>
         <li class="treeview">
     <% End If %>        
          <a href="#">
            <i class="fa fa-vimeo"></i> <span>VMI</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

               <li>
                <% If Request("ItemType") = "stk" Then%>
                    <a class="active" href="vmiStock.aspx?ActionType=vmi&ItemType=stk"><span>Stock</span></a>
                <% Else%>
                    <a href="vmiStock.aspx?ActionType=vmi&ItemType=stk"><span>Stock</span></a>
                <% End If%>
            </li>              
               	           
  
             
              <% If Request("ItemType") = "rec" Then%>
                <li class="active"><a href="vmiStockReceive.aspx?ActionType=vmi&ItemType=rec">Receive</a></li>
              <% Else %>
                <li><a href="vmiStockReceive.aspx?ActionType=vmi&ItemType=rec">Receive</a></li>
              <% End If %>


              <% If Request("ItemType") = "use" Then%>
                <li class="active"><a href="ReportConditionByDate.aspx?ActionType=vmi&ItemType=use">รายงานการจ่ายยา</a></li>
              <% Else %>
                <li><a href="ReportConditionByDate.aspx?ActionType=vmi&ItemType=use">รายงานการจ่ายยา</a></li>
              <% End If %>

              </ul>
             </li>

     <% End If%>    --%>

    <% If Request.QueryString("ActionType") = "pwd" Then%>
    <li class="active"><a href="ChangePassword?ActionType=pwd"><i class="fa fa-key"></i><span>เปลี่ยนรหัสผ่าน</span></a></li>
    <% Else%>
    <li><a href="ChangePassword?ActionType=pwd"><i class="fa fa-key"></i><span>เปลี่ยนรหัสผ่าน</span></a></li>
    <% End If%>



    <li><a href="Default.aspx?logout=y"><i class="fa fa-lock"></i><span>ออกจากระบบ</span></a></li>


</ul>
