﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="Pharmacist.aspx.vb" Inherits=".Pharmacist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>จัดการข้อมูลเภสัชกร
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">     
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">จัดการข้อมูลเภสัชกร</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">




 <table width="100%" border="0" cellspacing="2" cellpadding="0">
  <tr>
     <td>&nbsp;</td>
   </tr>
   <tr>
     <td width="100%"><table border="0" align="center" cellpadding="0" cellspacing="2">
      <tr>
         <td width="140">ID</td>
         <td width="220"> <asp:Label ID="lblID" runat="server"></asp:Label>                        
            </td>
         <td width="100">คำนำหน้า</td>
         <td><asp:DropDownList ID="ddlPrefix" runat="server"  CssClass="form-control select2"> </asp:DropDownList></td>
      </tr>
       <tr>
         <td>ชื่อ</td>
         <td>
             <asp:TextBox ID="txtFirstName" runat="server" Width="200px"></asp:TextBox>           </td>
         <td>นามสกุล</td>
         <td><asp:TextBox ID="txtLastName" runat="server" Width="200px"></asp:TextBox></td>
       </tr>
      
       <tr>
         <td>เลขใบประกอบวิชาชีพ</td>
         <td>
             <asp:TextBox ID="txtLicenseNo" CssClass="text-center" runat="server" Width="200px"></asp:TextBox>   
           </td>
         <td>ตำแหน่ง</td>
         <td>
             <asp:TextBox ID="txtPositionName" runat="server" Width="200px"></asp:TextBox>           </td>
       </tr>
      
       <tr>
         <td>สังกัดร้านยา</td>
         <td colspan="3">
             <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control select2" AutoPostBack="True">             </asp:DropDownList>           </td>
       </tr>
      
       <tr>
         <td colspan="4" align="center"><span class="texttopic">
           <asp:Button ID="cmdSave" runat="server" text="บันทึก" cssclass="buttonSave" Width="80px" ></asp:Button>
         </span></td>
         </tr>
       
     </table></td>
    </tr>
   <tr>
     <td align="center">
         <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
             <ProgressTemplate>
<img alt="" src="images/progress_bar.gif" height="25" />             </ProgressTemplate>
         </asp:UpdateProgress>      </td>
    </tr>
     <tr>
          <td  align="left" valign="top" class="skin_Search"><table border="0" cellspacing="1" cellpadding="0">
            <tr>
              <td width="50" >ค้นหา</td>
              <td >
                  <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>                </td>
              <td >
                  <asp:Button ID="cmdFind" runat="server" text="ค้นหา" CssClass="buttonFind" />                </td>
            </tr>
            <tr>
                  <td colspan="3" class="text10_nblue"><strong>&nbsp;*</strong>คำค้นหาสามารถค้นหาได้จาก 
                      ชื่อ, นามสกุล หรือ ร้านยา</td>
              </tr>
          </table></td>
      </tr>
       <tr>
          <td align="left" valign="top"  >
              <asp:Label ID="lblStudentCount" runat="server"></asp:Label>           </td>
    </tr>

   <tr>
     <td>
         <asp:GridView ID="grdData" CssClass="table table-hover" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" AllowPaging="True" 
                             Font-Bold="False" Width="100%">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                        <asp:BoundField DataField="PrefixName" HeaderText="คำนำหน้า" >                            
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="90px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FirstName" HeaderText="ชื่อ" >
                            <HeaderStyle HorizontalAlign="Left" />                            </asp:BoundField>
                            <asp:BoundField DataField="LastName" HeaderText="นามสกุล" >
                            <HeaderStyle HorizontalAlign="Left" />                            </asp:BoundField>
                            <asp:BoundField DataField="LicenseNo" HeaderText="เลขใบประกอบวิชาชีพ">
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="ตำแหน่ง" DataField="PositionName">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />                            </asp:BoundField>
                            <asp:BoundField DataField="LocationName" HeaderText="สังกัด" />
                            <asp:TemplateField>
                                <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgEdit" runat="server" 
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem,"PersonID") %>' 
                                        ImageUrl="images/icon-edit.png" />
                                    &nbsp;
                                     <asp:ImageButton ID="imgDel"  cssclass="gridbutton"  runat="server" 
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem,"PersonID") %>' 
                                        ImageUrl="images/icon-delete.png" />                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" 
                                                            />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView>  </td>
    </tr>
 </table>
                
 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    </section>
</asp:Content>
