﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="F15.aspx.vb" Inherits=".F15" %>
<%@ Register src="ucHistoryYear.ascx" tagname="ucHistoryYear" tagprefix="uc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/pagestyles.css">
<link rel="stylesheet" type="text/css" href="css/rajchasistyles.css">
<link rel="stylesheet" type="text/css" href="css/uidialog.css">
<link rel="stylesheet" type="text/css" href="css/dc_table.css">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>F15
        <small>แบบคัดกรองโรคหืดและโรคปอดอุดกั้นเรื้อรัง</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">     
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลกิจกรรม</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">


<asp:HiddenField ID="HiddenField1" runat="server" />
 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="10"> <uc1:ucHistoryYear ID="ucHistoryYear1" runat="server" /></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="OptionPanels">
      <tr>
        <td width="100" class="texttopic">รหัสร้าน</td>
        <td width="120" class="LocationName">
            <asp:Label ID="lblLocationID" runat="server"></asp:Label>          </td>
        <td width="50" class="texttopic">ชื่อร้าน</td>
        <td width="300" class="LocationName">
            <asp:Label ID="lblLocationName" runat="server"></asp:Label>          </td>
        <td width="50" class="texttopic">จังหวัด</td>
        <td class="LocationName">
            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
   <tr>
    <td align="center"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">
      <tr>
        <td width="100">วันที่ให้บริการ</td>
        <td>
            <asp:TextBox ID="txtServiceDate" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
(วว/ดด/ปปปป พ.ศ.) 
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="txtServiceDate" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="\d{2}/\d{2}/\d{4}"></asp:RegularExpressionValidator></td>
        <td width="100">รวมระยะเวลา<img src="images/star.png" width="10" height="10" /></td>
        <td>
            <asp:TextBox ID="txtTime" runat="server" Width="50px"></asp:TextBox>
&nbsp;นาที</td>
        <td>เภสัชกรผู้ให้บริการ</td>
        <td>
            <asp:DropDownList ID="ddlPerson" runat="server">            </asp:DropDownList>          </td>
      </tr>
    </table></td>
  </tr>
 
   <tr>
    <td>
      <table width="100%" border="0" cellspacing="2" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
          <td width="100" align="right"><span class="NameEN">Ref.ID :</span></td>
          <td width="50"><span class="NameEN">
              <asp:Label ID="lblID" 
                  runat="server"></asp:Label></span>            </td>
        </tr>
      </table></td>
  </tr>
     <tr>
    <td align="center" class="MenuSt">(1) แบบประเมินสมรรถภาพปอด (Lung Function Questionnaire)</td>
  </tr>
   <tr>
    <td align="center"><table width="100%" align="left" class="dc_table_s3">
     
      <tfoot>
      </tfoot>
      <tbody>
        <tr>
          <th width="550" align="left" valign="middle" scope="row">1.	อายุ &#8805;50 ปี</th>
          <th align="left"> 
              <asp:RadioButtonList ID="optLung_1Age" runat="server" 
                 RepeatDirection="Horizontal" AutoPostBack="True">
            <asp:ListItem Value="A1">ใช่ [1]</asp:ListItem>
            <asp:ListItem Value="B0" Selected="True">ไม่ใช่ [0]</asp:ListItem>
          </asp:RadioButtonList> </th>
        </tr>
        <tr>
          <th align="left" valign="middle" scope="row"> 2.  เคยมีเสียงดัง (เสียงหวีด) ระหว่างหายใจหรือไม่ </th>
          <th align="left"> 
              <asp:RadioButtonList ID="optLung_2Sound" runat="server" 
                RepeatDirection="Horizontal" CssClass="nono" BackColor="#F7F9FC" 
                AutoPostBack="True">
            <asp:ListItem Value="A1">มี [1]</asp:ListItem>
            <asp:ListItem Value="B0" Selected="True">ไม่มี [0]</asp:ListItem>
          </asp:RadioButtonList>
          </th>
        </tr>
        <tr >
          <th align="left" valign="middle" scope="row">3.  เคยต้องหอบหรือหายใจถี่ ๆ ระหว่างออกกำลังกายหรือเดินขึ้นทางลาดชันโดยไม่หยุดพักหรือไม่ </th>
          <th align="left"> 
              <asp:RadioButtonList ID="optLung_3" runat="server" 
                RepeatDirection="Horizontal" AutoPostBack="True">
            <asp:ListItem Value="A1">มี [1]</asp:ListItem>
            <asp:ListItem Value="B0" Selected="True">ไม่มี [0]</asp:ListItem>
          </asp:RadioButtonList>
          </th>
        </tr>
        <tr>
          <th align="left" valign="middle" scope="row">4.  ไอมีเสมหะบ่อย ๆ หรือไม่ </th>
          <th align="left"> 
              <asp:RadioButtonList ID="optLung_4" runat="server" 
                RepeatDirection="Horizontal" AutoPostBack="True">
           <asp:ListItem Value="A1">มี [1]</asp:ListItem>
            <asp:ListItem Value="B0" Selected="True">ไม่มี [0]</asp:ListItem>
          </asp:RadioButtonList>
          </th>
        </tr>
        <tr>
          <th align="left" scope="row" valign="middle">5.  มีประวัติสูบบุหรี่มา &#8805;20 ปี </th>
          <th align="left"> 
              <asp:RadioButtonList ID="optLung_5" runat="server" 
                  AutoPostBack="True" RepeatDirection="Horizontal">
            <asp:ListItem Value="A1">ใช่ [1]</asp:ListItem>
            <asp:ListItem Value="B0" Selected="True">ไม่ใช่ [0]</asp:ListItem>
          </asp:RadioButtonList>
          </th>
        </tr>
        <tr class="odd">
          <th colspan="2" align="center" scope="row">คะแนนรวม 
              <asp:TextBox ID="txtLung_Score" runat="server" Width="100px"></asp:TextBox>
&nbsp;คะแนน  (A total score &#8805; 3 suggests airflow obstruction)</th>
          </tr>
      </tbody>
    </table></td>
  </tr>
  
   <tr>
    <td align="center" class="MenuSt">(2) ผลการตรวจสมรรถภาพปอดด้วย Peak Flow Meter</td>
  </tr>
   <tr>
    <td><table border="0" align="center" cellpadding="0" cellspacing="3">
      <tr>
        <td class="texttopic">น้ำหนัก</td>
        <td align="left"><asp:TextBox ID="txtWeight" runat="server" Width="50px"></asp:TextBox></td>
        <td class="texttopic">ส่วนสูง</td>
        <td align="left"><asp:TextBox ID="txtHeigh" runat="server" Width="50px" AutoPostBack="True"></asp:TextBox></td>
      </tr>
      <tr>
        <td class="texttopic">PEFR ที่วัดได้ค่าที่ดีที่สุด</td>
        <td align="left"><asp:TextBox ID="txtPEFR_Value" runat="server" Width="87px" 
                AutoPostBack="True"></asp:TextBox></td>
        <td class="texttopic">PEFR มาตรฐาน</td>
        <td align="left"><asp:TextBox ID="txtPEFR_Standard" runat="server" Width="87px" 
                AutoPostBack="True"></asp:TextBox></td>
      </tr>
      <tr>
        <td colspan="4" class="texttopic">% PEFR predicted value = (PEFR ที่วัดได้ค่าที่ดีที่สุด/ PEFR มาตรฐานจากตาราง) x 100 =<asp:TextBox ID="txtPEFR_Rate" runat="server" Width="87px"></asp:TextBox></td>
        </tr>
     
      <tr>
        <td  class="texttopic">สรุป</td>
         <td colspan="3" align="left">
             <asp:RadioButtonList ID="optPEFR_Summary" runat="server">
                 <asp:ListItem Value="G">มากกว่า  70 %     แนะนำการปรับเปลี่ยนพฤติกรรม</asp:ListItem>
                 <asp:ListItem Selected="True" Value="L">น้อยกว่า 70 %     ประเมินต่อ ( 3)  (4)</asp:ListItem>
             </asp:RadioButtonList>
          </td>
        </tr>
          <tr>
        <td colspan="4" class="texttopic">&nbsp;</td>
        </tr>
     
    </table></td> 
  </tr>
 
  <tr>
    <td>&nbsp;</td>
  </tr>
   <tr>
    <td align="center" class="MenuSt">(3) แบบประเมินความเสี่ยงต่อโรคปอดอุดกั้นเรื้อรัง(COPD Population Screener Questionnaire)</td>
  </tr>
  
  <tr>
    <td align="left"><table width="100%" border="0" cellpadding="0" cellspacing="2" class="dc_table_s3">
      
      <tbody>
        <tr>
          <th width="420" align="left" valign="top">
          1. อายุ</th>
          <th valign="top" align="left">
            <asp:RadioButtonList ID="optCOPD_1Age" runat="server" 
                RepeatDirection="Horizontal" AutoPostBack="True">
                <asp:ListItem Value="A0" Selected="True">&#8804; 49 ปี [0]</asp:ListItem>
                <asp:ListItem Value="B1">50– 59 ปี [1]</asp:ListItem>
<asp:ListItem Value="C2" Selected="True">&#8805; 60ปี [2]</asp:ListItem>
            </asp:RadioButtonList>          </th>
        </tr>
        <tr>
          <th valign="top"  align="left">2. ช่วง 4 สัปดาห์ที่ผ่านมา คุณเคยรู้สึกหอบ หรือ ต้องหายใจถี่ ๆ บ่อยหรือไม</th>
          <th valign="top" align="left">
              <asp:RadioButtonList ID="optCOPD_2" runat="server" 
                  RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Selected="True" Value="A0">ไม่เคยเลยถึงน้อยมาก [0]</asp:ListItem>
                  <asp:ListItem Value="B1">บางครั้ง [1]</asp:ListItem>
                  <asp:ListItem Value="C2">บ่อยครั้ง [2]</asp:ListItem>
                  <asp:ListItem Value="D2">ตลอดเวลา [2]</asp:ListItem>
              </asp:RadioButtonList>
            </th>
        </tr>
        <tr>
          <th valign="top"  align="left">3. คุณเคยไอ มีเสมหะ</th>
          <th valign="top" align="left">
              <asp:RadioButtonList ID="optCOPD_3" runat="server" AutoPostBack="True">
                  <asp:ListItem Selected="True" Value="A0">ไม่เคยเลย [0]</asp:ListItem>
                  <asp:ListItem Value="B0">บางเวลาที่อากาศเย็น [0]</asp:ListItem>
                  <asp:ListItem Value="C1">บางวันใน 1 เดือน [1]</asp:ListItem>
                  <asp:ListItem Value="D1">หลายวัน ใน 1สัปดาห์ [1]</asp:ListItem>
                  <asp:ListItem Value="E2">ทุกวัน [2]</asp:ListItem>
              </asp:RadioButtonList>
            </th>
        </tr>
        <tr>
          <th align="left" valign="top" >4. ในช่วง 1 ปีที่ผ่านมาคุณมีความสามารถในการทำกิจกรรมต่าง ๆ ลดลง 
              <br />
              เพราะมีปัญหาเรื่องระบบหายใจ</th>
          <th valign="top" align="left">
              <asp:RadioButtonList ID="optCOPD_4" runat="server" 
                  RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Selected="True" Value="A0">ไม่เห็นด้วย/ไม่แน่ใจ [0]</asp:ListItem>
                  <asp:ListItem Value="B1">เห็นด้วย [1]</asp:ListItem>
                  <asp:ListItem Value="C2">เห็นด้วยมากที่สุด [2]</asp:ListItem>
              </asp:RadioButtonList>
            </th>
        </tr>
        <tr>
          <th valign="top"  align="left"> 5. ตลอดชีวิต เคยสูบบุหรี่มากกว่า 100 มวน </th>
          <th valign="top" align="left"> 
              <asp:RadioButtonList ID="optCOPD_5" runat="server" 
                  AutoPostBack="True" RepeatDirection="Horizontal">
            <asp:ListItem Value="A1">ใช่ [1]</asp:ListItem>
            <asp:ListItem Value="B0" Selected="True">ไม่ใช่ [0]</asp:ListItem>
          </asp:RadioButtonList>
            </th>
        </tr>
        <tr  class="odd">
          <th colspan="2" align="center" valign="top" >คะแนน
              <asp:TextBox ID="txtCOPD_Score" runat="server" Width="100px"></asp:TextBox>
&nbsp;คะแนน       (A total score &#8805; 5 suggests COPD)</th>
          </tr>
        <tr  class="odd">
          <th align="right" valign="top" >สรุป</th>
           <th align="left" valign="top" >
               <asp:RadioButtonList ID="optCOPD_Sum" runat="server">
                   <asp:ListItem Value="H">เสี่ยง  COPD / คะแนนมากกว่า หรือเท่ากับ 5  ทำข้อ (4)</asp:ListItem>
                   <asp:ListItem Value="L">เสี่ยงต่ำ / คะแนนน้อยกว่า 5</asp:ListItem>
                   <asp:ListItem Selected="True" Value="S">ส่งต่อแพทย์</asp:ListItem>
               </asp:RadioButtonList>
            </th>
          </tr>
      </tbody>
    </table></td>
  </tr>
 
  <tr>    <td align="center" class="MenuSt">(4) แบบประเมินความเสี่ยงต่อโรคหืด (Asthma Predictive Index)</td>
  </tr>
   <tr>
     <td align="left">
     <table width="100%" border="0" cellpadding="0" cellspacing="2"  class="dc_table_s3">
        <thead>
      <tr>
        <th colspan="2" align="center">
        <table border="0" align="center" cellpadding="2" cellspacing="0">
          <tr>
            <td>ในช่วง 1 
              ปีที่ผ่านมาคุณมีอาการไอ/ แน่นหน้าอก/ หายใจมีเสียงหวีด &#8805;3 ครั้ง/ปี</td>
            <td><asp:RadioButtonList ID="optAsthma_Year" runat="server" 
                 RepeatDirection="Horizontal" AutoPostBack="True">
              <asp:ListItem Value="A1">ใช่ [1]</asp:ListItem>
              <asp:ListItem Value="B0" Selected="True">ไม่ใช่ [0]</asp:ListItem>
            </asp:RadioButtonList></td>
          </tr>
        </table></th>
        </tr> 
        <tr>
          <th colspan="4" align="center" valign="top">(ถ้า “ใช่” ให้ทำข้อ 1 และ 2 ต่อ) </th>
          </tr>
    </thead>
      <tbody>
        <tr>
          <th width="550" align="left" valign="top">1.  มีประวัติครอบครัว พ่อ/แม่ เป็นโรคหืด หรือ เคยถูกแพทย์วินิจฉัยเป็น  atopic dermatitis</th>
          <th colspan="3" align="left" valign="top"> 
              <asp:RadioButtonList ID="optAsthma_1" runat="server" 
                 RepeatDirection="Horizontal" AutoPostBack="True">
            <asp:ListItem Value="A1">ใช่ [1]</asp:ListItem>
            <asp:ListItem Value="B0" Selected="True">ไม่ใช่ [0]</asp:ListItem>
          </asp:RadioButtonList> </th>
        </tr>
        <tr>
          <th valign="top"  align="left">2. เคยหายใจมีเสียงหวีดที่ไม่ได้เกิดจากอากาศเย็น และ เคยถูกแพทย์วินิจฉัยเป็น allergic rhinitis</th>
          <th colspan="3" valign="top" align="left"> 
              <asp:RadioButtonList ID="optAsthma_2" runat="server" 
                 RepeatDirection="Horizontal" AutoPostBack="True">
            <asp:ListItem Value="A1">ใช่ [1]</asp:ListItem>
            <asp:ListItem Value="B0" Selected="True">ไม่ใช่ [0]</asp:ListItem>
          </asp:RadioButtonList> </th>
        </tr>
       
        <tr  class="odd">
          <th colspan="4" align="center" valign="top">คะแนน
            <asp:TextBox ID="txtAsthma_Score" runat="server" Width="100px"></asp:TextBox>
            &nbsp;คะแนน       (A total score &#8805; 2 suggests asthma)&nbsp;</th>
          </tr>
           <tr  class="odd">
          <th align="right" valign="top" >สรุป</th>
           <th align="left" valign="top" >
               <asp:RadioButtonList ID="optAsthma_Sum" runat="server">
                   <asp:ListItem Value="H">เสี่ยง  Asthma / คะแนนมากกว่า หรือเท่ากับ 2</asp:ListItem>
                   <asp:ListItem Value="L">เสี่ยงต่ำ / คะแนนน้อยกว่า 2</asp:ListItem>
                   <asp:ListItem Selected="True" Value="S">ส่งต่อแพทย์</asp:ListItem>
               </asp:RadioButtonList>
            </th>
          </tr>
      </tbody>
    </table></td>
   </tr>
  
  
      <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="dc_table_s3">
     <thead>
      <tr>
        <th scope="col" width="50%" valign="top" class="texttopic">แจ้งสิทธิประโยช์  สปสช.</th>
      </tr>
       </thead>
      <tr>
        <td align="left">
              <table border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td><asp:CheckBox ID="chkNHSO1" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวจคัดกรองความเสี่ยงDM/HT" /></td>
                  <td><asp:CheckBox ID="chkNHSO2" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวจคัดกรองความเสี่ยงมะเร็งปากมดลูก/มะเร็งเต้านม" /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO3" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับวัคซีนเสริมสร้างภูมิคุ้มกันโรค" /></td>
                  <td><asp:CheckBox ID="chkNHSO4" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับคำแนะนำปรึกษาการวางแผนครอบครัว / คุมกำเนิด" /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO5" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการดูแล/หญิงมีครรภ์ " /></td>
                  <td><asp:CheckBox ID="chkNHSO6" runat="server" Font-Bold="False" Text="สิทธิที่จะได้รับการตรวดเอดส์ ฟรี ปีละ 2 ครั้ง " /></td>
                </tr>
                <tr>
                  <td><asp:CheckBox ID="chkNHSO7" runat="server" Font-Bold="False" Text="การแจ้งย้ายสิทธิ " /></td>
                  <td><asp:CheckBox ID="chkNHSO8" runat="server" Font-Bold="False" Text="สายด่วน 1330" /></td>
                </tr>
              </table></td>
      </tr>
    </table></td>
  </tr>  
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="Section_Header">
      <tr>
        <td width="100">สถานะ</td>
        <td>
            <asp:CheckBox ID="chkStatus" runat="server" Text="จบการทำงาน" />          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center">
        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="70px" />
        &nbsp;<asp:Button ID="cmdClear" runat="server" Text="ยกเลิก" CssClass="buttonCancle" Width="70px" />
        </td>
  </tr>
</table>
    

 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    </section>
</asp:Content>