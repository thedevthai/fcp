﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="ReferReg.aspx.vb" Inherits=".ReferReg" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">   

    </asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>ส่งต่อข้อมูลไปยังฐานข้อมูลกลาง (TCDB)
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
 <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">บันทึกรายละเอียด</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">




  
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td align="center" valign="top">&nbsp;</td>
      </tr>
        <tr>
            <td align="center"  >
             
            
<table cellSpacing="1" cellPadding="1" border="0">
                                                <tr>
                                                    <td align="left">
                                                        วันที่ Refer</td>
                                                    <td align="left" >
                                                        <asp:TextBox ID="txtDate" runat="server" Width="200px" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        ส่งต่อไปยัง</td>
                                                    <td align="left"><asp:TextBox ID="txtDestination" runat="server" Width="300px" TabIndex="1" >TCDB</asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        Remark </td>
                                                    <td align="left"><asp:TextBox ID="txtRemark" runat="server" Width="300px" TabIndex="2" ></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td align="left">&nbsp;</td>
                                                    <td align="left">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td align="left">&nbsp;                                                    </td>
                                                    <td align="left">
                                                        <asp:Button ID="cmdSave" runat="server" CssClass="buttonSave" Text="บันทึก" Width="100px" />
&nbsp;<asp:Button ID="cmdClear" runat="server" CssClass="buttonSave" Text="ยกเลิก" Width="100px" />
                                                    </td>
                                                </tr>
                                                </table>        
  
 
  
      </td>
      </tr>
        
       
    </table>
     </div>
            <div class="box-footer clearfix">
           
            </div>
          </div> 
    </section>
</asp:Content>
