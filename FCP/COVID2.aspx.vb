﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Subgurim.Controles
'Imports Rajchasi
Public Class COVID2
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlL As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlCovid As New CovidController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx")
        End If

        'If Session("patientid") Is Nothing Then
        '    Response.Redirect("PatientSearch.aspx")
        'End If

        If Not IsPostBack Then
            If Request("acttype") = "view" Then
                cmdSave.Visible = False
            Else
                cmdSave.Visible = True
            End If


            cmdDelete.Visible = False

            txtStartDate.Text = DisplayShortDateTH(ctlCovid.GET_DATE_SERVER.AddDays(-14))
            txtEndDate.Text = DisplayShortDateTH(ctlCovid.GET_DATE_SERVER)

            LoadLocationData(Request.Cookies("LocationID").Value)
            'LoadPharmacist(Request.Cookies("LocationID").Value)

            If Not Request("fid") Is Nothing Then
                LoadData()
            End If
        End If

        'txtFollowCount.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtFollowDay1.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtFollowDay2.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtFollowDay3.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtFollowDay4.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtFollowDay5.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        'txtFollowTime.Attributes.Add("OnKeyPress", "return AllowOnlyDouble();")
        'txtSmokeQTY.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        'txtBegin.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        'txtEnd.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")


        Dim scriptString As String = "javascript:return confirm(""คุณแน่ใจที่จะลบรายการรับบริการนี้ใช่หรือไม่?"");"
        cmdDelete.Attributes.Add("onClick", scriptString)

    End Sub

    'Private Sub LoadPharmacist(LID As String)
    '    Dim dtP As New DataTable
    '    dtP = ctlPs.GetPerson_ByLocation(LID)
    '    If dtP.Rows.Count > 0 Then
    '        ddlPerson.Items.Clear()
    '        If dtP.Rows.Count > 0 Then
    '            With ddlPerson
    '                .Visible = True
    '                For i = 0 To dtP.Rows.Count - 1
    '                    .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
    '                    .Items(i).Value = dtP.Rows(i)("PersonID")
    '                Next
    '                .SelectedIndex = 0
    '            End With
    '        End If
    '    End If
    '    dtP = Nothing
    'End Sub

    Private Sub LoadLocationData(LocationID As String)
        dt = ctlL.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationCode.Text = DBNull2Str(.Item("LocationCode"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("ProvinceName"))
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadData()

        dt = ctlCovid.COVID2_GetByUID(StrNull2Zero(Request("fid")))


        If dt.Rows.Count > 0 Then
            With dt.Rows(0)

                cmdDelete.Visible = True
                'txtBYear.Text = .Item("BYear")
                lblCOVIDUID.Text = .Item("UID")
                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                Session("patientid") = DBNull2Lng(.Item("PatientID"))

                'lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                'lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                If String.Concat(.Item("StartDate")) <> "" Then
                    txtStartDate.Text = DisplayDateStr2ShortDateTH(String.Concat(.Item("StartDate")))
                Else
                    txtStartDate.Text = ""
                End If

                If String.Concat(.Item("EndDate")) <> "" Then
                    txtEndDate.Text = DisplayDateStr2ShortDateTH(String.Concat(.Item("EndDate")))
                Else
                    txtEndDate.Text = ""
                End If

                optTest.SelectedValue = String.Concat(.Item("TestMethod"))
                optColor.SelectedValue = String.Concat(.Item("ColorStatus"))
                optMedical.SelectedValue = String.Concat(.Item("Medication"))
                txtMedicalOther.Text = String.Concat(.Item("MedicationOther"))

                chkDisease0.Checked = False
                chkDisease1.Checked = False
                chkDisease2.Checked = False
                chkDisease3.Checked = False
                chkDisease4.Checked = False
                chkDisease5.Checked = False
                chkDisease6.Checked = False
                chkDisease7.Checked = False
                chkDisease8.Checked = False
                chkDisease9.Checked = False
                chkDisease10.Checked = False
                chkDisease11.Checked = False
                chkDisease12.Checked = False
                chkDisease99.Checked = False
                chkDiseaseOther.Checked = False
                txtDiseaseOther.Text = DBNull2Str(.Item("DiseaseOther"))

                chkDisease0.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease0")))
                chkDisease1.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease1")))
                chkDisease2.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease2")))
                chkDisease3.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease3")))
                chkDisease4.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease4")))
                chkDisease5.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease5")))
                chkDisease6.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease6")))
                chkDisease7.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease7")))
                chkDisease8.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease8")))
                chkDisease9.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease9")))
                chkDisease10.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease10")))
                chkDisease11.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease11")))
                chkDisease12.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease12")))
                chkDisease99.Checked = ConvertYesNo2Boolean(String.Concat(.Item("Disease99")))

                If txtDiseaseOther.Text <> "" Then
                    chkDiseaseOther.Checked = True
                Else
                    chkDiseaseOther.Checked = False
                End If

                If DBNull2Str(.Item("IsMedicine")) = "Y" Then
                    Dim sMed As String()
                    sMed = Split(DBNull2Str(.Item("Medicine")), "|")

                    For i = 0 To sMed.Length - 1
                        Select Case sMed(i)
                            Case "M1"
                                chkMed.Items(0).Selected = True
                            Case "M2"
                                chkMed.Items(1).Selected = True
                            Case "M3"
                                chkMed.Items(2).Selected = True
                            Case "M4"
                                chkMed.Items(3).Selected = True
                            Case "M5"
                                chkMed.Items(4).Selected = True
                            Case "M6"
                                chkMed.Items(5).Selected = True
                            Case "M99"
                                chkMed.Items(6).Selected = True
                        End Select
                    Next
                End If

                txtMedicineOther.Text = String.Concat(.Item("MedicineOther"))

                optSmoke.SelectedValue = String.Concat(.Item("Smoking"))
                optSmokingQuit.SelectedValue = String.Concat(.Item("SmokingQuit"))
                optAfterEffect.Text = String.Concat(.Item("AfterEffect"))
                txtAfterEffextRemark.Text = String.Concat(.Item("AfterEffectRemark"))

                optQ1.SelectedValue = String.Concat(.Item("Q1"))
                optQ2.SelectedValue = String.Concat(.Item("Q2"))
                optQ3.SelectedValue = String.Concat(.Item("Q3"))
                optQ4.SelectedValue = String.Concat(.Item("Q4"))
                optQ5.SelectedValue = String.Concat(.Item("Q5"))
                optQ6.SelectedValue = String.Concat(.Item("Q6"))
                optQ7.SelectedValue = String.Concat(.Item("Q7"))
                optQ8.SelectedValue = String.Concat(.Item("Q8"))
                optQ9.SelectedValue = String.Concat(.Item("Q9"))
                optQ10.SelectedValue = String.Concat(.Item("Q10"))
                optQ11.SelectedValue = String.Concat(.Item("Q11"))
                optQ12.SelectedValue = String.Concat(.Item("Q12"))
                optQ13.SelectedValue = String.Concat(.Item("Q13"))
                optQ14.SelectedValue = String.Concat(.Item("Q14"))
                optQ15.SelectedValue = String.Concat(.Item("Q15"))
                optQ16.SelectedValue = String.Concat(.Item("Q16"))
                optM1.SelectedValue = String.Concat(.Item("M1"))
                optM2.SelectedValue = String.Concat(.Item("M2"))
                optM3.SelectedValue = String.Concat(.Item("M3"))
                optS1.SelectedValue = String.Concat(.Item("S1"))
                optS2.SelectedValue = String.Concat(.Item("S2"))

                txtPhysicalRemark.Text = DBNull2Str(.Item("PhysicalRemark"))
                txtProblemRemark.Text = DBNull2Str(.Item("ProblemOther"))

                chkRecommend.Checked = ConvertYesNo2Boolean(String.Concat(.Item("IsRecommend")))
                chkRefer.Checked = ConvertYesNo2Boolean(String.Concat(.Item("IsRefer")))

                txtRecommend.Text = String.Concat(.Item("Recommend"))
                optRefer.SelectedValue = String.Concat(.Item("ReferTo"))
                txtReferOther.Text = String.Concat(.Item("ReferOther"))
                optResult.SelectedValue = String.Concat(.Item("FinalResult"))
                txtResultOther.Text = String.Concat(.Item("ResultOther"))

                'txtFollowCount.Text = String.Concat(.Item("FollowCount"))
                txtFollowDay1.Text = String.Concat(.Item("FollowDay1"))
                txtFollowDay2.Text = String.Concat(.Item("FollowDay2"))
                txtFollowDay3.Text = String.Concat(.Item("FollowDay3"))
                txtFollowDay4.Text = String.Concat(.Item("FollowDay4"))
                txtFollowDay5.Text = String.Concat(.Item("FollowDay5"))
                chkStatus.Checked = ConvertYesNo2Boolean(DBNull2Str(.Item("CloseStatus")))

            End With
        Else
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing

    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If Request.Cookies("UserID").Value Is Nothing Then
            Response.Redirect("Default.aspx")
        End If

        If txtStartDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนวันที่ตรวจพบเชื้อ');", True)
            Exit Sub
        End If
        If txtEndDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนวันที่หาย');", True)
            Exit Sub
        End If
        If optTest.SelectedValue = Nothing Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกวิธีตรวจพบ');", True)
            Exit Sub
        End If
        If optColor.SelectedValue = Nothing Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุสีสถานะตอนเป็น Covid');", True)
            Exit Sub
        End If
        If optMedical.SelectedValue = Nothing Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุการรักษา');", True)
            Exit Sub
        End If
        If optSmoke.SelectedValue = Nothing Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุการสูบบุหรี่');", True)
            Exit Sub
        End If
        If optSmoke.SelectedValue = "2" Then
            If optSmokingQuit.SelectedValue = Nothing Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุความต้องการเลิกสูบบุหรี่ก่อน');", True)
                Exit Sub
            End If
        End If
        If optAfterEffect.SelectedValue = Nothing Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุว่าท่านมีอาการที่เกิดขึ้นหลังการติดเชื้อโควิด-19 หรือไม่ก่อน');", True)
            Exit Sub
        End If

        If lblLocationID.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาตรวจสอบรหัสร้านยาของท่านก่อน แล้วลองใหม่อีกครั้ง');", True)
            Exit Sub
        End If
        If optAfterEffect.SelectedValue = "Y" Then
            If optQ1.SelectedValue = Nothing Or optQ2.SelectedValue = Nothing Or optQ3.SelectedValue = Nothing Or optQ4.SelectedValue = Nothing Or optQ5.SelectedValue = Nothing Or optQ6.SelectedValue = Nothing Or optQ7.SelectedValue = Nothing Or optQ8.SelectedValue = Nothing Or optQ9.SelectedValue = Nothing Or optQ10.SelectedValue = Nothing Or optQ11.SelectedValue = Nothing Or optQ12.SelectedValue = Nothing Or optQ13.SelectedValue = Nothing Or optQ14.SelectedValue = Nothing Or optQ15.SelectedValue = Nothing Or optQ16.SelectedValue = Nothing Or optM1.SelectedValue = Nothing Or optM2.SelectedValue = Nothing Or optM3.SelectedValue = Nothing Or optS1.SelectedValue = Nothing Or optS2.SelectedValue = Nothing Then

                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุอาการที่เกิดขึ้นหลังการติดเชื้อโควิด-19 ให้ครบทุกข้อ');", True)
                Exit Sub
            End If
        End If

        If chkRecommend.Checked = True Then
            If txtRecommend.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','หากท่านเลือกการให้คำแนะนำ กรุณาระบุสิ่งที่แนะนำก่อน');", True)
                Exit Sub
            End If
        End If
        If chkRefer.Checked = True Then
            If optRefer.SelectedValue = Nothing Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','หากท่านเลือกการส่งต่อ กรุณาแหล่งที่ส่งต่อก่อน');", True)
                Exit Sub
            End If
        End If

        If optRefer.SelectedValue = "9" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุการส่งต่ออื่นๆ');", True)
            Exit Sub
        End If
        If optResult.SelectedValue = Nothing Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกการติดตามผล');", True)
            Exit Sub
        End If

        'If optResult.SelectedValue <> "0" Then
        '    If StrNull2Zero(txtFollowCount.Text) = 0 Then
        '        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุระยะเวลาติดตามของเภสัชกร');", True)
        '        Exit Sub
        '    End If
        '    If StrNull2Zero(txtFollowDay.Text) = 0 Then
        '        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุจำนวนสัปดาห์ในการติดตามของเภสัชกร');", True)
        '        Exit Sub
        '    End If
        'End If

        Dim objuser As New UserController

        If lblCOVIDUID.Text = "" Then 'Add new
            If ctlCovid.COVID2_ChkDupCustomer(Session("patientid")) = True Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านได้บันทึกการให้บริการของผู้รับบริการนี้ ในวันที่ท่านระบุแล้ว');", True)
                Exit Sub
            End If
        End If
        Dim FollowCount As Integer = 0
        If txtFollowDay1.Text <> "" Then
            FollowCount = FollowCount + 1
        End If
        If txtFollowDay2.Text <> "" Then
            FollowCount = FollowCount + 1
        End If
        If txtFollowDay3.Text <> "" Then
            FollowCount = FollowCount + 1
        End If
        If txtFollowDay4.Text <> "" Then
            FollowCount = FollowCount + 1
        End If
        If txtFollowDay5.Text <> "" Then
            FollowCount = FollowCount + 1
        End If

        Dim isMed, Medicine As String
        isMed = "N"
        Medicine = ""
        ' 1 ฟ้าทะลายโจร 2  Favipiravir   3 ยาสมุนไพร   4 Nat Long    5 Ivermectin 6 ยาลดไข้ แก้ไอ  99 อื่นๆ 

        If chkMed.Items(0).Selected Then
            Medicine = "M1M|"
        End If
        If chkMed.Items(1).Selected Then
            Medicine = Medicine & "M2M|"
        End If
        If chkMed.Items(2).Selected Then
            Medicine = Medicine & "M3M|"
        End If
        If chkMed.Items(3).Selected Then
            Medicine = Medicine & "M4M|"
        End If
        If chkMed.Items(4).Selected Then
            Medicine = Medicine & "M5M|"
        End If
        If chkMed.Items(5).Selected Then
            Medicine = Medicine & "M6M|"
        End If
        If chkMed.Items(6).Selected Then
            Medicine = Medicine & "M99"
        End If

        Medicine = Replace(Medicine, "M|M", "|M")
        Medicine = Replace(Medicine, "M|", "")

        If Medicine <> "" Then
            isMed = "Y"
        End If

        If txtMedicineOther.Text <> "" Then
            isMed = "Y"
        End If

        ctlCovid.COVID2_Save(StrNull2Zero(lblCOVIDUID.Text), lblLocationID.Text, Session("patientid"), ConvertStrDate2DateQueryString(txtStartDate.Text), ConvertStrDate2DateQueryString(txtEndDate.Text), optTest.SelectedValue, optColor.SelectedValue, optMedical.SelectedValue, txtMedicalOther.Text, optSmoke.SelectedValue, optSmokingQuit.SelectedValue, optAfterEffect.SelectedValue, optQ1.SelectedValue, optQ2.SelectedValue, optQ3.SelectedValue, optQ4.SelectedValue, optQ5.SelectedValue, optQ6.SelectedValue, optQ7.SelectedValue, optQ8.SelectedValue, optQ9.SelectedValue, optQ10.SelectedValue, optQ11.SelectedValue, optQ12.SelectedValue, optQ13.SelectedValue, optQ14.SelectedValue, optQ15.SelectedValue, optQ16.SelectedValue, optM1.SelectedValue, optM2.SelectedValue, optM3.SelectedValue, optS1.SelectedValue, optS2.SelectedValue, txtPhysicalRemark.Text, txtProblemRemark.Text, ConvertStatus2YN(chkRecommend.Checked), ConvertStatus2YN(chkRefer.Checked), txtRecommend.Text, optRefer.SelectedValue, txtReferOther.Text, optResult.SelectedValue, txtResultOther.Text, FollowCount, StrNull2Zero(txtFollowDay1.Text), StrNull2Zero(txtFollowDay2.Text), StrNull2Zero(txtFollowDay3.Text), StrNull2Zero(txtFollowDay4.Text), StrNull2Zero(txtFollowDay5.Text), ConvertStatus2YN(chkStatus.Checked), Request.Cookies("UserID").Value, ConvertStatus2YN(chkDisease0.Checked), ConvertStatus2YN(chkDisease1.Checked), ConvertStatus2YN(chkDisease2.Checked), ConvertStatus2YN(chkDisease3.Checked), ConvertStatus2YN(chkDisease4.Checked), ConvertStatus2YN(chkDisease5.Checked), ConvertStatus2YN(chkDisease6.Checked), ConvertStatus2YN(chkDisease7.Checked), ConvertStatus2YN(chkDisease8.Checked), ConvertStatus2YN(chkDisease9.Checked), ConvertStatus2YN(chkDisease10.Checked), ConvertStatus2YN(chkDisease11.Checked), ConvertStatus2YN(chkDisease12.Checked), ConvertStatus2YN(chkDisease99.Checked), txtDiseaseOther.Text, isMed, Medicine, txtMedicineOther.Text, txtAfterEffextRemark.Text)

        objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Covid2", "บันทึกเพิ่มแบบฟอร์มการติดตามผู้ป่วย Long COVID :<<PatientID:" & Session("patientid") & ">>", "COVIDLong")


        If lblCOVIDUID.Text = "" Then
            lblCOVIDUID.Text = ctlCovid.COVID2_GetLastUID(lblLocationID.Text, Session("patientid")).ToString()
        End If


        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)

    End Sub

    Protected Sub cmdDelete_Click(sender As Object, e As EventArgs) Handles cmdDelete.Click

        ctlCovid.COVID2_Delete(StrNull2Zero(lblCOVIDUID.Text))
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ลบข้อมูลเรียบร้อย');", True)

        lblCOVIDUID.Text = ""
        cmdDelete.Visible = False
        Response.Redirect("PatientCovid2.aspx")
    End Sub
End Class