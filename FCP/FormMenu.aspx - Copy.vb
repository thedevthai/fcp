﻿Public Class FormMenu
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ctlU As New UserController
        Dim dt As New DataTable
        pnProjectF.Visible = False
        pnProjectA.Visible = False
        dt = ctlU.User_GetProjectRole(Session("UserID"))
        If dt.Rows.Count > 0 Then
            pnProjectF.Visible = Decimal2Boolean(dt.Rows(0)("ProjectF"))
            pnProjectA.Visible = Decimal2Boolean(dt.Rows(0)("ProjectA"))
            pnHomeVisit.Visible = Decimal2Boolean(dt.Rows(0)("ProjectH"))
        End If
        dt = Nothing
    End Sub

End Class