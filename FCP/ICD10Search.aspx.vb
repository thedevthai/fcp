﻿Public Class ICD10Search
    Inherits System.Web.UI.Page
    Dim ctlD As New DeseaseController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then
            LoadData()
        End If

    End Sub
    Private Sub LoadData()
        Dim dtM As New DataTable

        If txtDrugSearch.Text <> "" Then
            dtM = ctlD.Desease_GetBySearch(txtDrugSearch.Text)
            If dtM.Rows.Count > 0 Then
                'grdDrugTMT.DataSource = dtM
                'grdDrugTMT.DataBind()
                lstData.DataSource = dtM
                lstData.DataTextField = "Name"
                lstData.DataValueField = "Name"
                lstData.DataBind()
            End If
        End If

        dtM = Nothing
    End Sub

    Protected Sub cmdDrugSearch_Click(sender As Object, e As EventArgs) Handles cmdDrugSearch.Click
        LoadData()
    End Sub
End Class