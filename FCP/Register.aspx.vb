﻿
Public Class Register
    Inherits Page
    Dim dt As New DataTable
    Dim ctlL As New LocationController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Select Case Request("t")
                Case "1"
                    lblType.Text = "สำหรับร้านยา"
                    lblTypeDesc.Text = "เลขที่ใบอนุญาตขายยา (ตัวอย่าง กท.28/2565)"
                Case "2"
                    lblType.Text = "สำหรับเภสัชกร"
                    lblTypeDesc.Text = "เลขที่ใบประกอบวิชาชีพ (ตัวอย่าง ภ.xxxx)"
                Case Else
                    Response.Redirect("Default")
            End Select
        End If
    End Sub

    Protected Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        If Request("t") = "1" Then
            If Trim(txtLicenseNo.Text) = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุเลขที่ใบอนุญาตขายยา (ขย.5) ก่อน');", True)
                Exit Sub
            End If

            If Len(Trim(txtLicenseNo.Text)) < 8 Or Len(Trim(txtLicenseNo.Text)) > 12 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุเลขที่ใบอนุญาตขายยา (ขย.5) ให้ถูกต้อง');", True)
                Exit Sub
            End If

            If ctlL.Location_SearchByLicense(txtLicenseNo.Text) > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','เลขที่ใบอนุญาตนี้มี User ในระบบแล้ว กรุณา Login ด้วยรหัสผู้ใช้งานของท่าน');", True)
            Else
                Dim ctlFDA As New FDAServiceController
                Dim NewCode As String
                NewCode = ctlFDA.ConvertLicenseToNewCode(txtLicenseNo.Text)
                Try
                    dt = ctlFDA.GET_DRUG_LCN_INFORMATION(NewCode)
                Catch ex As Exception

                End Try

                If Not dt Is Nothing Then
                    If dt.Rows.Count > 0 Then 'มีใน อย.
                        Session("LicenseRegister") = txtLicenseNo.Text
                        Response.Redirect("Policy.aspx?p=reg&t=1")

                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','ไม่พบเลขที่ใบอนุญาตนี้ในฐานข้อมูลของ อย. กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                        Exit Sub
                    End If
                Else
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','ไม่พบเลขที่ใบอนุญาตนี้ในฐานข้อมูลของ อย. กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    Exit Sub
                End If

            End If

        ElseIf Request("t") = "2" Then
            If Trim(txtLicenseNo.Text) = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุเลขที่ใบประกอบวิชาชีพ ก่อน');", True)
                Exit Sub
            End If

            If Len(Trim(txtLicenseNo.Text)) < 4 Or Len(Trim(txtLicenseNo.Text)) > 8 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุเลขที่ใบประกอบวิชาชีพ ให้ถูกต้อง');", True)
                Exit Sub
            End If

            If ctlL.Location_SearchByLicense(txtLicenseNo.Text) > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','เลขที่ประกอบวิชาชีพนี้มี User ในระบบแล้ว');", True)
                Exit Sub
            Else
                Session("LicenseRegister") = txtLicenseNo.Text
                Response.Redirect("Policy.aspx?p=reg&t=2")
            End If

        End If
    End Sub


End Class