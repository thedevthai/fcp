﻿Public Class Organization
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlOrg As New OrganizationController
    Dim ctlM As New MasterController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            LoadProvince()
            LoadOrganization()
        End If
    End Sub

    Private Sub LoadOrganization()
        grdData.DataSource = ctlOrg.Organization_Get()
        grdData.DataBind()
    End Sub
    Private Sub LoadProvince()
        ddlProvince.DataSource = ctlM.Province_Get()
        ddlProvince.DataTextField = "ProvinceName"
        ddlProvince.DataValueField = "ProvinceID"
        ddlProvince.DataBind()
    End Sub

    Private Sub ClearData()
        hdUID.Value = ""
        txtBU.Text = ""
        txtNameTH.Text = ""
        txtNameEN.Text = ""
        txtAddressEng.Text = ""
        txtAddressNo.Text = ""
        txtAddressTha.Text = ""
        txtDistrict.Text = ""
        txtEmail.Text = ""
        txtFax.Text = ""
        txtLane.Text = ""
        txtRoad.Text = ""
        txtSubDistrict.Text = ""
        txtTel.Text = ""
        txtWebsite.Text = ""
        txtZipcode.Text = ""
        ddlCountry.Text = ""
        ddlProvince.SelectedIndex = 0
    End Sub

    Private Sub EditData(ByVal pID As Integer)
        ClearData()
        dt = ctlOrg.Organization_GetByUID(pID)

        If dt.Rows.Count > 0 Then
            Dim dr As DataRow = dt.Rows(0)
            txtBU.Text = String.Concat(dr("OrganizationCode"))
            txtNameTH.Text = String.Concat(dr("NameTH"))
            txtNameEN.Text = String.Concat(dr("NameEN"))
            'txtAddressEng.Text = String.Concat(dr("AddressEng"))
            txtAddressNo.Text = String.Concat(dr("AddressNumber"))
            txtAddressTha.Text = String.Concat(dr("AddressTH"))
            txtDistrict.Text = String.Concat(dr("District"))
            txtEmail.Text = String.Concat(dr("Email"))
            txtFax.Text = String.Concat(dr("Fax"))
            txtLane.Text = String.Concat(dr("Lane"))
            txtRoad.Text = String.Concat(dr("Road"))
            txtSubDistrict.Text = String.Concat(dr("SubDistrict"))
            txtTel.Text = String.Concat(dr("Telephone"))
            txtWebsite.Text = String.Concat(dr("Website"))
            txtZipcode.Text = String.Concat(dr("Zipcode"))
            ddlCountry.SelectedValue = String.Concat(dr("Country"))
            'ddlProvince.SelectedValue = String.Concat(dr("ProvinceID"))
            dr = Nothing
        End If

        dt = Nothing
    End Sub

    Protected Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If CInt(e.Row.RowType) = CInt(ListItemType.AlternatingItem) Or CInt(e.Row.RowType) = CInt(ListItemType.Item) Then
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = CType(e.Row.Cells(6).FindControl("imgDel"), Image)
            imgD.Attributes.Add("onClick", scriptString)
        End If
    End Sub

    Protected Sub grdData_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)
        If TypeOf e.CommandSource Is ImageButton Then
            Dim ButtonPressed As ImageButton = CType(e.CommandSource, ImageButton)

            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(Convert.ToInt32(e.CommandArgument))
                    Exit Select
                Case "imgDel"
                    ctlOrg.Organization_Delete(Convert.ToInt32(e.CommandArgument))
                    LoadOrganization()
                    Exit Select
            End Select
        End If
    End Sub
End Class