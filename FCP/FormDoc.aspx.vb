﻿
Public Class FormDoc
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlType As New ServiceTypeController
    Dim objUser As New UserController
    Dim ctlOrder As New OrderController

    Dim svTypeID As String
    '  Dim svSeqNo As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then

           
            lblNoComplete.Visible = False
            LoadProvinceToDDL()
            grdComplete.PageIndex = 0
            LoadActivityListCompleteToGrid()

            If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then

                grdComplete.Columns(2).Visible = False

                lblProv.Visible = False
                ddlProvinceID.Visible = False

            Else

                grdComplete.Columns(2).Visible = True

                lblProv.Visible = True
                ddlProvinceID.Visible = True

            End If
        End If
    End Sub
    Protected Function DateText(ByVal input As Date) As String
        Dim dStr As String
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/0544", "", input.ToString("dd/MM/yyyy"))
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/0001", "", input.ToString("dd/MM/yyyy"))
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/0544", "", input.ToString("dd/MM/yyyy"))
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/2443", "", input.ToString("dd/MM/yyyy"))
        Return dStr
    End Function
    Private Sub LoadProvinceToDDL()

        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            dt = ctlOrder.Province_GetInGroup(Request.Cookies("RPTGRP").Value)
        Else
            dt = ctlOrder.Province_GetInLocation
        End If

        ddlProvinceID.Items.Clear()
        If dt.Rows.Count > 0 Then
            With ddlProvinceID
                .Enabled = True
                .Items.Add("---ทั้งหมด---")
                If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
                    .Items(0).Value = Request.Cookies("RPTGRP").Value
                Else
                    .Items(0).Value = "0"
                End If

                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)("ProvinceName"))
                    .Items(i + 1).Value = dt.Rows(i)("ProvinceID")
                Next
                .SelectedIndex = 0

            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadActivityListCompleteToGrid()

        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            If ddlActivity.SelectedValue = FORM_TYPE_ID_F03 Then
                dt = ctlOrder.Order_F03GetDocument(Request.Cookies("LocationID").Value, Trim(txtSearch.Text), ddlProvinceID.SelectedValue)
            ElseIf ddlActivity.SelectedValue = FORM_TYPE_ID_F11F Then
                dt = ctlOrder.Order_F11GetDocument(Request.Cookies("LocationID").Value, Trim(txtSearch.Text), ddlProvinceID.SelectedValue)
            End If

        ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then

            If ddlActivity.SelectedValue = FORM_TYPE_ID_F03 Then
                dt = ctlOrder.Order_GetF03DoucumentByProvinceGroup(Request.Cookies("RPTGRP").Value, Trim(txtSearch.Text), ddlProvinceID.SelectedValue)
            ElseIf ddlActivity.SelectedValue = FORM_TYPE_ID_F11F Then
                dt = ctlOrder.Order_GetF11DoucumentByProvinceGroup(Request.Cookies("RPTGRP").Value, Trim(txtSearch.Text), ddlProvinceID.SelectedValue)
            End If

        Else
            If ddlActivity.SelectedValue = FORM_TYPE_ID_F03 Then
                dt = ctlOrder.Order_F03GetDocument("", Trim(txtSearch.Text), ddlProvinceID.SelectedValue)
            ElseIf ddlActivity.SelectedValue = FORM_TYPE_ID_F11F Then
                dt = ctlOrder.Order_F11GetDocument("", Trim(txtSearch.Text), ddlProvinceID.SelectedValue)
            End If

        End If


        If dt.Rows.Count > 0 Then
            lblCount.Text = dt.Rows.Count
            lblNoComplete.Visible = False
            With grdComplete
                .Visible = True
                .DataSource = dt
                .DataBind()

                Try

                    Dim nrow As Integer = dt.Rows.Count
                    If .PageCount > 1 Then
                        If .PageIndex > 0 Then
                            If (.PageSize) < nrow - (.PageSize * .PageIndex) Then
                                For i = 0 To .PageSize - 1
                                    ' .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                    .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows((.PageSize * .PageIndex) + i)("ServiceDate"))
                                    .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows((.PageSize * .PageIndex) + i)("Gender"))

                                Next
                            Else
                                For i = 0 To nrow - (.PageSize * .PageIndex) - 1
                                    '.Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                    .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows((.PageSize * .PageIndex) + i)("ServiceDate"))
                                    .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows((.PageSize * .PageIndex) + i)("Gender"))
                                Next
                            End If
                        Else
                            For i = 0 To .PageSize - 1
                                ' .Rows(i).Cells(0).Text = i + 1
                                .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows(i)("ServiceDate"))
                                .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows(i)("Gender"))
                            Next
                        End If
                    Else
                        For i = 0 To nrow - 1
                            ' .Rows(i).Cells(0).Text = i + 1
                            .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows(i)("ServiceDate"))
                            .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows(i)("Gender"))
                        Next
                    End If
                Catch ex As Exception

                End Try
            End With
        Else

            lblNoComplete.Visible = True
            grdComplete.Visible = False
            grdComplete.DataSource = Nothing
        End If

        dt = Nothing
    End Sub

    Protected Sub ddlActivity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlActivity.SelectedIndexChanged
        grdComplete.PageIndex = 0
        LoadActivityListCompleteToGrid()
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdComplete.PageIndex = 0
        LoadActivityListCompleteToGrid()
    End Sub
    Private Sub grdComplete_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdComplete.PageIndexChanging
        grdComplete.PageIndex = e.NewPageIndex
        LoadActivityListCompleteToGrid()
    End Sub
    Private Sub grdComplete_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdComplete.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub
    Protected Sub ddlProvinceID_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProvinceID.SelectedIndexChanged
        grdComplete.PageIndex = 0
        LoadActivityListCompleteToGrid()
    End Sub
End Class

