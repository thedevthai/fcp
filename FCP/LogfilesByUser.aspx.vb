﻿Imports Microsoft.ApplicationBlocks.Data

Public Class LogfilesByUser
    Inherits System.Web.UI.Page
    Dim ctlUser As New UserController
    Dim dt As New DataTable
    Dim ds As New DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then
            LoadUserOnline()

            grdData.PageIndex = 0
            With ddlUserID
                .Enabled = True
                .Items.Add("ทั้งหมด")
                .Items(0).Value = 0
                .SelectedIndex = 0
            End With

        End If
    End Sub

    Private Sub LoadUserOnline()
        dt = ctlUser.GetUsers_Online
        lblOnline.Text = "<font color=#006400>User ที่กำลังใช้งาน >> </font>"
        If dt.Rows.Count > 0 Then
            For i = 0 To dt.Rows.Count - 1
                If i Mod 2 = 0 Then
                    lblOnline.Text &= "<font color=#1878ca>" & "[" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("FirstName") & " " & dt.Rows(i)("LastName") & "]" & "</font>"
                Else
                    lblOnline.Text &= "<font color=#990099>" & "[" & dt.Rows(i)("Username") & " : " & dt.Rows(i)("FirstName") & " " & dt.Rows(i)("LastName") & "]" & "</font>"
                End If

            Next

        Else
            lblOnline.Text = "ไม่มี User Online ในขณะนี้"
        End If

    End Sub
    Private Sub LoadUserToDDL()
        If txtFind.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนคำค้นหาก่อน');", True)
            Exit Sub
        Else
            dt = ctlUser.GetUsers_BySearch(txtFind.Text)
        End If
        ddlUserID.Items.Clear()
        If dt.Rows.Count > 0 Then
            With ddlUserID
                .Enabled = True
                .Items.Add("---ทั้งหมด---")
                .Items(0).Value = 0
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add("" & dt.Rows(i)("FirstName") & " " & dt.Rows(i)("LastName"))
                    .Items(i + 1).Value = dt.Rows(i)("UserID")
                Next
                .SelectedIndex = 0
            End With
        End If
    End Sub


    Private Sub LoadLogfile()
        SQL = ""
        SQL = "SELECT   *   FROM    View_Logfile  Where (Descrp Like '%" & txtSearch.Text & "%' OR Remark  Like '%" & txtSearch.Text & "%') "

        If txtBeginDate.Text <> "" And txtEndDate.Text <> "" Then
            Dim str() As String
            Dim dY As Integer
            Dim Bdate As String
            Dim Edate As String

          

            str = Split(Me.txtBeginDate.Text, "/")
            dY = Now.Date.Year - CInt(str(2))
            If dY < 0 Then
                dY = CInt(str(2)) - 543
            End If
            Bdate = str(1) & "/" & str(0) & "/" & dY

            str = Split(Me.txtEndDate.Text, "/")
            dY = Now.Date.Year - CInt(str(2))
            If dY < 0 Then
                dY = CInt(str(2)) - 543
            End If
            Edate = str(1) & "/" & CInt(str(0)) + 1 & "/" & dY

            If ddlUserID.SelectedValue = 0 Then
                SQL &= "  AND  Work_Date  between   '" & Bdate & "' and '" & Edate & "'"

            Else
                SQL &= "  AND  Userid=" & ddlUserID.SelectedValue & " and  Work_Date  between   '" & Bdate & "' and '" & Edate & "'"

            End If
        Else
            If ddlUserID.SelectedValue <> 0 Then
                SQL &= "  AND  Userid=" & ddlUserID.SelectedValue
            Else
                SQL &= "  AND logid>=1 "
            End If

            'Controls.Add(New LiteralControl("<script>alert('กรุณาตรวจสอบวันที่');</script>"))
            'Exit Sub
        End If

        SQL &= " order by logid  desc"


        ds = SqlHelper.ExecuteDataset(ApplicationBaseClass.ConnectionString, CommandType.Text, SQL)
        dt = ds.Tables(0)


        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                'For i = 0 To dt.Rows.Count - 1
                '    .Rows(i).Cells(0).Text = i + 1
                'Next
            End With
        Else
            grdData.Visible = False
        End If
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadLogfile()
    End Sub

    Protected Sub cmdSearch_Click(sender As Object, e As EventArgs) Handles cmdSearch.Click
        LoadUserToDDL()
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadLogfile()
    End Sub
End Class