﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="ucHistoryYear.ascx.vb" Inherits=".ucHistoryYear" %>
<table border="0" align="center" cellpadding="0" cellspacing="2">
      <tr>
        <td>ประจำปี :</td>
        <td><asp:GridView ID="grdHistory" 
                             runat="server" 
                      AutoGenerateColumns="False" Width="100%" 
                  PageSize="15"  ShowHeader="False" BorderStyle="None" GridLines="None">
            <columns>
                <asp:BoundField DataField="HYear" /> 
            </columns>
            <pagerstyle 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <headerstyle CssClass="th" 
                      VerticalAlign="Middle" />          
          </asp:GridView>
</td>
      </tr>
    </table>