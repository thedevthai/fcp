﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="Desease.aspx.vb" Inherits=".Desease" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">   

    </asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>จัดการข้อมูลโรค
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
 <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลโรค</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
  
    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
       
        <tr>
            <td align="center" valign="top">
             
            
<table cellSpacing="1" cellPadding="1" border="0">
                                                <tr>
                                                    <td align="left">
                                                        ID : 
                                                        </td>
                                                    <td align="left" >
                                                        <asp:Label ID="lblUID" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        Code</td>
                                                    <td align="left" >
                                                        <asp:TextBox ID="txtCode" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        ชื่อโรค</td>
                                                    <td align="left"><asp:TextBox ID="txtName" runat="server" Width="300px"></asp:TextBox></td>
                                                </tr>
    <tr>
                                                    <td align="left">
                                                        คำอธิบาย</td>
                                                    <td align="left"><asp:TextBox ID="txtDescription" runat="server" Width="300px"></asp:TextBox></td>
                                                </tr>
    <tr>
                                                    <td align="left">
                                                        Sort</td>
                                                    <td align="left"><asp:TextBox ID="txtSort" runat="server" Width="300px"></asp:TextBox></td>
                                                </tr>
     <tr>
                                                    <td align="left">Status :</td>
                                                    <td align="left">
                                                        <asp:CheckBox ID="chkICD10" runat="server" Checked="False" Text="ICD10" />
                                                    &nbsp;<asp:CheckBox ID="chkActive" runat="server" Checked="True" Text="Active" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">&nbsp;                                                    </td>
                                                    <td align="left">
                                                        <asp:Button ID="cmdSave" runat="server" CssClass="buttonSave" Text="บันทึก" Width="100px" />
&nbsp;<asp:Button ID="cmdClear" runat="server" CssClass="buttonSave" Text="ยกเลิก" Width="100px" />
                                                    </td>
                                                </tr>
                                                </table>        
  
 
  
      </td>
      </tr>
       <tr>
          <td align="center" valign="top">&nbsp;</td>
      </tr>
        <tr>
          <td align="center" valign="top">
              <asp:GridView ID="grdData" CssClass="table table-hover" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" PageSize="15">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="No." DataField="nRow">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />                      </asp:BoundField>
            <asp:BoundField DataField="Code" HeaderText="Code">
              <headerstyle HorizontalAlign="Center" />          
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />                      </asp:BoundField>
            <asp:BoundField DataField="Name" HeaderText="ชื่อโรค">
              <headerstyle HorizontalAlign="Left" />          
              <itemstyle HorizontalAlign="Left" />                      </asp:BoundField>
                <asp:BoundField DataField="Description" HeaderText="คำอธิบาย">
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="ICD10">
                    <ItemTemplate>
                        <asp:Image ID="imgICD" runat="server" ImageUrl="images/icon-ok.png" Visible='<%# DataBinder.Eval(Container.DataItem, "StatusICD") %>'/>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status">
                    <ItemTemplate>
                        <asp:Image ID="imgActive" runat="server" ImageUrl="images/icon-ok.png"  Visible='<%# DataBinder.Eval(Container.DataItem, "StatusActive") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            <asp:TemplateField HeaderText="Edit">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgDel" runat="server" 
                                    ImageUrl="images/delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView></td>
      </tr>
       
    </table>
     </div>
            <div class="box-footer clearfix">
           
            </div>
          </div> 
    </section>
</asp:Content>
