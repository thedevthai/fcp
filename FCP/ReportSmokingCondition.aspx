﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="ReportSmokingCondition.aspx.vb" Inherits=".ReportSmokingCondition" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
      
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1> <asp:Label ID="lblReportName" runat="server" Text="รายงานสรุปจำนวนผู้รับบริการ"></asp:Label>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
  
 <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">เงื่อนไข</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
           
          </div>
        </div>
        <div class="box-body">

    <table width="90%" border="0" align="left" cellpadding="0" cellspacing="2">       
        <tr>
            <td align="center" valign="top">
             
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td  valign="top"> <table border="0" align="center" cellpadding="0" cellspacing="3">
          
      <tr>
    <td>ตั้งแต่</td>
    <td>
        <asp:TextBox ID="txtStartDate" runat="server">        </asp:TextBox>
          </td>
    <td>ถึง</td>
    <td>
        <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
          </td>
  </tr>
    
      <tr>
    <td>
        <asp:Label ID="lblSEQ" runat="server" Text="ระดับ"></asp:Label>
          </td>
    <td align="left">
        <asp:DropDownList ID="ddlFollowSEQ" runat="server" Width="100px" CssClass="OptionControl">
            <asp:ListItem Selected="True" Value="1">5-1</asp:ListItem>
            <asp:ListItem Value="2">5-2</asp:ListItem>
            <asp:ListItem Value="3">5-3</asp:ListItem>
            <asp:ListItem Value="4">5-4</asp:ListItem>
            <asp:ListItem Value="5">5-5</asp:ListItem>
        </asp:DropDownList>
          </td>
    <td>&nbsp;</td>
    <td>
        &nbsp;</td>
  </tr>
    
</table></td>
    </tr>
    <tr>
      <td align="center">
                                                      <asp:Button ID="cmdView" runat="server" CssClass="buttonFind" Text="ดูรายงาน" />
        </td>
    </tr>
  </table>
                           
            </td>
      </tr>  
        <tr>
          <td align="center" valign="top">

              <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
                  <ProgressTemplate>
                      <asp:Image ID="Image2" runat="server" ImageUrl="images/loading2.gif" />
                      <br />
                      <asp:LinkButton ID="lnkProcess" runat="server">กำลังโหลดข้อมูล...กรุณารอสักครู่</asp:LinkButton>
                  </ProgressTemplate>
              </asp:UpdateProgress>
            </td>
      </tr>       
    </table>
       </div>
</div>

</section>
</asp:Content>
