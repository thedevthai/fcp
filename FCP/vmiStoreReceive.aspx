﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="vmiStoreReceive.aspx.vb" Inherits=".vmiStoreReceive" %>
<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/cpastyles.css">
    <link href="css/pagestyles.css" rel="stylesheet" type="text/css" />
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>บันทึกสินค้าคงคลัง
        <small>Inventory Store</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
   <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">Inventory Store</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">




     
                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
        
          
          <tr>
            <td bgcolor="#FFFFFF">
            
            
            
            <TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" bgColor="#ffffff" border="0">
<TBODY>
					<TR>
						<TD>
						  <TABLE id="Table2" height="100%" cellSpacing="2" cellPadding="0" width="100%" border="0">
				  <TR>
									<TD  valign="top">
                                    
                                    <table border="0" cellspacing="2" cellpadding="2">
                                      <tr>
                                        <td width="100">Date</td>
                                        <td>
                                            <dx:ASPxDateEdit ID="dtpDate" runat="server" Theme="PlasticBlue">
                                            </dx:ASPxDateEdit>
                                          </td>
                                        <td width="100" align="right">&nbsp;</td>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td width="100">Store</td>
                                        <td><asp:DropDownList ID="ddlStore" 
                                                    runat="server" CssClass="OptionControl"> </asp:DropDownList></td>
                                        <td width="100" align="right">Item Name</td>
                                        <td><asp:DropDownList ID="ddlMedicine" runat="server" CssClass="OptionControl"> </asp:DropDownList></td>
                                      </tr>
                                      <tr>
                                        <td><asp:Label ID="lblLocation" runat="server" Text="Distribution Type"></asp:Label></td>
                                        <td>
                                            <asp:DropDownList ID="ddlType" runat="server" CssClass="OptionControl"> </asp:DropDownList>
                                          </td>
                                        <td align="right">QTY</td>
                                        <td><asp:TextBox ID="txtQTY" runat="server" Width="60px"></asp:TextBox></td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                        <td align="left">
                  <asp:Button ID="cmdFind" runat="server" CssClass="buttonFind" Text="ค้นหา" Width="90px" />
                                          &nbsp;<asp:Button ID="cmdSave" runat="server" CssClass="buttonRedial" Text="Save" Width="90px" />
                                          </td>
                                        <td align="left">
                                            &nbsp;</td>
                                        <td align="left">&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td colspan="4"><asp:Label ID="lblvalidate1" runat="server" CssClass="validateAlert" 
                                                Visible="False" Width="99%"></asp:Label></td>
                                      </tr>
                                    </table></TD>
</TR>
				 
       <tr>
          <td  align="left" valign="top" class="skin_Search">&nbsp;</td>
      </tr>
				  <TR>
				     <TD  valign="top" class="text12b_nblue"><asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" CssClass="txtcontent" 
                             Font-Bold="False">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                            <asp:BoundField HeaderText="No.">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />                            </asp:BoundField>
<asp:BoundField HeaderText="Date" DataField="INVDATE"></asp:BoundField>
                            <asp:BoundField HeaderText="Store Code" DataField="StoreCode" />
                            <asp:BoundField DataField="StoreName" HeaderText="Store Name" />
                        <asp:BoundField DataField="DistributionName" HeaderText="Distb Type" />
                            <asp:BoundField DataField="itemName" HeaderText="Item Name" />
                            <asp:BoundField DataField="QTY" DataFormatString="{0:#,###}" HeaderText="QTY" />
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgDel" runat="server" 
                                    ImageUrl="images/icon-delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID")%>' />                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Left" />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView>  </TD>
				     </TR>
				  <TR>
				     <TD  valign="top">&nbsp;</TD>
				     </TR>
								</TABLE>					  </TD>
		  </TR>
				</TBODY>
			</TABLE>            </td>
            </tr>
         
        </table>  
                 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    
    </section>     
</asp:Content>
