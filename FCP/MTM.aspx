﻿<%@ Page Title="" Language="vb" AutoEventWireup="false"  MasterPageFile="~/SitePatient.Master" CodeBehind="MTM.aspx.vb" Inherits=".MTM" %>
<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/pagestyles.css">
<link rel="stylesheet" type="text/css" href="css/rajchasistyles.css">
<link rel="stylesheet" type="text/css" href="css/uidialog.css">
<link rel="stylesheet" type="text/css" href="css/dc_table.css">   
        
</asp:Content> 

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">        
  

     <section class="content-header">
      <h1>แบบฟอร์มการทบทวนการใช้ยาและแก้ไขปัญหาการใช้ยา
        <small>(MTM)</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">MTM</li>
      </ol>
    </section>

<section class="content">    

   <!--  <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        //$(document).ready(function () {

        //    $('.btnCssDrugRefill').click(function () {
        //        window.open('DrugSearch.aspx?p=drugrefill','Drug','height=400,width=600');
        //        return false;
        //    });

        //     $('.btnCssDrugRemain').click(function () {
        //        window.open('DrugSearch.aspx?p=drugremain','Drug','height=400,width=600');
        //        return false;
        //    });
          

        //     $('.btnCssICD').click(function () {
        //        window.open('ICD10Search.aspx?p=icd10','ICD10','height=400,width=600');
        //        return false;
        //    });

        //});

        function getListItems(items,pname) {
            if (pname == 'icd10') {
                $('.icd10').val(items);
            }
            if (pname == 'drugrefill') {
                $('.drugrefill').val(items);
            }
            if (pname == 'drugremain') {
                $('.drugremain').val(items);
            }
            if (pname == 'drug') {
                $('.drug').val(items);
            }
            if (pname == 'druguse') {
                $('.druguse').val(items);
            }
            //$('.csstextbox').val(items);
        } 

    </script>-->




<asp:HiddenField ID="HiddenField1" runat="server" />
<div align="center">
    <h2>Visit No. <asp:Label ID="lblSEQ" runat="server" Text=""></asp:Label></h2>
</div>

     <div class="pull-right">
    <table  border="0" cellspacing="2" cellpadding="0" align="right">
        <tr>        
          <td class="NameEN">Item ID : </td>
          <td class="NameEN">              <asp:Label ID="lblMTMUID"                  runat="server"></asp:Label>            </td>
        </tr>
      </table> 
         </div>
<br />
 <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">ข้อมูลร้านยาที่ให้บริการ</h3>
          <div class="box-tools pull-right">
              

            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
 <table width="100%" border="0" align="left" cellpadding="0" cellspacing="2" class="OptionPanels">
      <tr>
        <td width="50">รหัสร้าน</td>
        <td width="120" class="LocationName">
            <asp:Label ID="lblLocationID" runat="server"></asp:Label>          </td>
        <td width="50">ชื่อร้าน</td>
        <td width="300" class="LocationName">
            <asp:Label ID="lblLocationName" runat="server"></asp:Label>          </td>
           <td width="100">รหัสหน่วยบริการ</td>
           <td  class="LocationName">
               <asp:Label ID="lblLocationCode" runat="server"></asp:Label>
          </td>
        <td width="50">จังหวัด</td>
        <td class="LocationName">
            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>          </td>
      </tr>
    </table>

        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">
      <tr>
        <td width="100">วันที่ให้บริการ</td>
        <td>
            <asp:TextBox ID="txtServiceDate" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
(วว/ดด/ปปปป พ.ศ.) 
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="txtServiceDate" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="\d{2}/\d{2}/\d{4}"></asp:RegularExpressionValidator></td>
        <td width="100">รวมระยะเวลา<img src="images/star.png" width="10" height="10" /></td>
        <td>
            <asp:TextBox ID="txtTime" runat="server" Width="40px"></asp:TextBox>
&nbsp;นาที</td>
        <td>เภสัชกรผู้ให้บริการ</td>
        <td>
            <asp:DropDownList ID="ddlPerson" runat="server" CssClass="form-control select2"></asp:DropDownList>          </td>
      </tr>
    </table>
  <!-- /.box-body -->       
      </div>
      
</div>

 <table   border="0" cellspacing="2" cellpadding="0" align="center">
      <tr>
        <td class="LocationName"> ประเภทการให้บริการ &nbsp; </td>
        <td>
             
            <asp:RadioButtonList ID="optMTMType" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                <asp:ListItem Value="MTM">MTM ที่ร้านยา</asp:ListItem>
                <asp:ListItem Value="HV">MTM จากการเยี่ยมบ้าน (Home Visit)</asp:ListItem>
            </asp:RadioButtonList>
             
          </td>
      </tr>


    </table>

      <br />
  
<div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">การให้บริการ</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">         
 
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">
    
      <tr>
         <td>
            แหล่งที่มา</td>
        <td align="left" colspan="5">

            <table>
                <tr>
                    <td>     <asp:RadioButtonList ID="optPatientFrom" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Value="W">Walk in</asp:ListItem>
                <asp:ListItem Value="H">Home visit</asp:ListItem>
                <asp:ListItem Value="R">หน่วยบริการส่งมา</asp:ListItem>
                <asp:ListItem Value="C">โครงการลดความแออัด</asp:ListItem> 
                <asp:ListItem Value="9">อื่นๆ</asp:ListItem> 
            </asp:RadioButtonList>   </td>
                       <td>ระบุ</td>
                       <td>
                           <asp:TextBox ID="txtFromRemark" runat="server" Width="300px"></asp:TextBox></td>
                </tr>
            </table>
               

          </td>
      </tr>
      <tr>
         <td valign="top">การให้บริการ</td>
        <td align="left" colspan="5">
            <table>
                <tr>
                    <td>
            <asp:RadioButtonList ID="optService" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Value="1">Face to face</asp:ListItem>
                <asp:ListItem Value="2">Telepharmacy</asp:ListItem>
                <asp:ListItem Value="9">อื่นๆ</asp:ListItem>
            </asp:RadioButtonList>
                    </td>
                    <td>
             <asp:TextBox ID="txtServiceRemark" runat="server" Width="300px" placeholder="กรณีอื่นๆ ระบุ"></asp:TextBox>
                    </td>
                </tr>
            </table>
          </td>
      </tr>
      <tr>
         <td valign="top">กรณี Telepharmacy</td>
        <td align="left" colspan="5">
             <table>
                <tr>
                    <td>  <asp:RadioButtonList ID="optTelepharm" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Value="1">ผป.ของร้านเอง</asp:ListItem>
                <asp:ListItem Value="2">ผป.โครงการลดแอัด (ระบุ รพ.แม่ข่าย)</asp:ListItem>
                <asp:ListItem Value="3">ผป.รับยาทาง ปณ.จาก รพ. (ระบุ รพ.)</asp:ListItem>
                <asp:ListItem Value="9">ผป.ในโครงการอื่นๆ (ระบุ)</asp:ListItem>
            </asp:RadioButtonList></td>
                       <td width="60" class="text-right">ระบุ</td>
                       <td>
                           <asp:TextBox ID="txtTelepharmacyRemark" runat="server" Width="300px"></asp:TextBox></td>
                </tr>
            </table>

          
          </td>
      </tr>
      <tr>
         <td valign="top">วิธีการสื่อสาร Telepharmacy</td>
        <td align="left" colspan="5">

             <table>
                <tr>
                    <td>  <asp:RadioButtonList ID="optTelepharmacyMethod" runat="server">
                  <asp:ListItem Value="1" >โทรศัพท์ หรือ  line call  <u><b>ไม่มีการบันทึกทั้งภาพและเสียง</b></u>        </asp:ListItem>
                <asp:ListItem  Value="2">โทรศัพท์  หรือ  Line call <u><b>บันทึกแต่เสียง</b></u></asp:ListItem> 
                  <asp:ListItem Value="3">มี soft ware หรือ program หรือ  Line Call  หรือ อื่นๆ  และ<u><b>มีการบันทึก ภาพและเสียง</b></u></asp:ListItem>
            </asp:RadioButtonList></td>
                       <td> <table class="nav-justified">
                  <tr>
                      <td>ระบุ วิธีการบันทึก</td>
                      <td><asp:TextBox ID="txtRecordMethod" runat="server" Width="300px"></asp:TextBox></td>
                  </tr>
                  <tr>
                      <td>ระบุ  สถานที่เก็บข้อมูล</td>
                      <td> <asp:TextBox ID="txtRecordLocation" runat="server" Width="300px"></asp:TextBox></td>
                  </tr>
              </table></td>

                </tr>
            </table>
            
          
           </td>
          <td  colspan="2" valign="top">
             
          </td>
      </tr>
    </table>      

</div>   
</div>  
  <asp:Panel ID="pnMTM" runat="server">
<div class="row">
    <section class="col-lg-5 connectedSortable"> 
        
        <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">โรคที่เป็น</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">   
          <table align="left"  width="100%">
            <tr>
                <td width="150">เพิ่มโรคที่เป็น 

                </td>
                <td colspan="2">
                    <asp:TextBox ID="txtDeseaseName" runat="server" CssClass="OptionControl" Width="100%"></asp:TextBox>
                </td> 
            </tr>
              <tr>
                  <td>เป็นโรคมาแล้วกี่ปี</td>
                  <td>
                      <asp:TextBox ID="txtDeseaseOther" runat="server" CssClass="OptionControl" Width="100%" placeholder="ระบุจำนวนปีที่เป็นโรคมาแล้ว"></asp:TextBox>
                  </td>
                  <td align="center" width="80">
                      <asp:Button ID="cmdDesease" runat="server" CssClass="buttonSave" Text="เพิ่มโรค" Width="70px" />
                  </td>
              </tr>
              <tr>
                  <td colspan="3">
                      <asp:UpdatePanel ID="UpdatePanelDesease" runat="server">
                          <ContentTemplate>
                              <asp:GridView ID="grdDesease" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%" CssClass="table table-hover">
                                  <RowStyle BackColor="White" VerticalAlign="Top" />
                                  <columns>
                                      <asp:BoundField HeaderText="No">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="DeseaseName" HeaderText="โรคที่เป็น">
                                      <headerstyle HorizontalAlign="Left" />
                                      <itemstyle HorizontalAlign="Left" />
                                      </asp:BoundField>
                                      <asp:TemplateField HeaderText="ลบ">
                                          <ItemTemplate>
                                              <asp:ImageButton ID="imgDel_DS" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' CssClass="gridbutton" ImageUrl="images/delete.png" />
                                          </ItemTemplate>
                                          <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                      </asp:TemplateField>
                                  </columns>
                                  <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                  <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                                  <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                  <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                                  <EditRowStyle BackColor="#2461BF" />
                                  <AlternatingRowStyle BackColor="#F7F7F7" />
                              </asp:GridView>
                          </ContentTemplate>
                          <Triggers>
                              <asp:AsyncPostBackTrigger ControlID="cmdDesease" EventName="Click" />
                          </Triggers>
                      </asp:UpdatePanel>
                  </td>
              </tr>
        </table>
    
      </div>
</div>


        <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">ยาที่ใช้ ณ ปัจจุบัน</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">   
            <small>(1.เลือกชื่อยาที่ใช้ 2.ระบุขนาด วิธีการรับประทาน และระยะเวลาที่ได้รับยา รวมถึงข้อบ่งใช้ ถ้าทราบ)</small>
          <table align="left"  width="100%">
            <tr>
                <td width="100">เพิ่มยาที่ใช้</td>
                <td colspan="2"><asp:DropDownList ID="ddlDrugUse" runat="server" cssclass="form-control select2">
                                </asp:DropDownList>                 
                </td>
            </tr>
              <tr>
                  <td>ระบุวิธีกิน</td>
                  <td>
                      <asp:TextBox ID="txtFQ" runat="server" CssClass="OptionControl" Width="100%"></asp:TextBox>
                  </td>
                  <td align="center" width="80">
                      <asp:Button ID="cmdAddDrug" runat="server" CssClass="buttonSave" Text="เพิ่มยา" Width="70px" />
                  </td>
              </tr>
              <tr>
                  <td colspan="3">
                      <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                          <ContentTemplate>
                              <asp:GridView ID="grdDrugUse" CssClass="table table-hover" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%">
                                  <RowStyle BackColor="White" VerticalAlign="Top" />
                                  <columns>
                                      <asp:BoundField DataField="nRow" HeaderText="No">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="Name" HeaderText="ยาที่ใช้">
                                      <headerstyle HorizontalAlign="Left" />
                                      <itemstyle HorizontalAlign="Left" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="FrequencyDescription" HeaderText="รายละเอียด" />
                                      <asp:TemplateField HeaderText="ลบ">
                                          <ItemTemplate>
                                              <asp:ImageButton ID="imgDel_DU" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "OrderUID") %>' CssClass="gridbutton" ImageUrl="images/delete.png" />
                                          </ItemTemplate>
                                          <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                      </asp:TemplateField>
                                  </columns>
                                  <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                  <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                                  <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                  <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                                  <EditRowStyle BackColor="#2461BF" />
                                  <AlternatingRowStyle BackColor="#F7F7F7" />
                              </asp:GridView>
                          </ContentTemplate>
                          <Triggers>
                              <asp:AsyncPostBackTrigger ControlID="cmdAddDrug" EventName="Click" />
                          </Triggers>
                      </asp:UpdatePanel>
                  </td>
              </tr>
              <tr>
                  <td colspan="3" class="LocationName">กรณี ผป ใช้ กลุ่มที่ไม่ใช่ยารักษา ได้แก่</td>
              </tr>
              <tr>
                  <td colspan="3">
                      <table width="100%">
                          <tr>
                              <td width="100">อาหารเสริม</td>
                              <td>
                                  <asp:TextBox ID="txtDrug1" runat="server" CssClass="OptionControl" Width="100%"></asp:TextBox>
                              </td>
                          </tr>
                          <tr>
                              <td>วิตามิน</td>
                              <td>
                                  <asp:TextBox ID="txtDrug2" runat="server" CssClass="OptionControl" Width="100%"></asp:TextBox>
                              </td>
                          </tr>
                          <tr>
                              <td>สมุนไพร</td>
                              <td>
                                  <asp:TextBox ID="txtDrug3" runat="server" CssClass="OptionControl" Width="100%"></asp:TextBox>
                              </td>
                          </tr>
                          <tr>
                              <td>อื่นๆ</td>
                              <td>
                                  <asp:TextBox ID="txtDrug4" runat="server" CssClass="OptionControl" Width="100%"></asp:TextBox>
                              </td>
                          </tr>
                          <tr>
                              <td>&nbsp;</td>
                              <td>
                                  <asp:Button ID="cmdSaveDrug" runat="server" CssClass="buttonSave" Text="บันทึก" Width="82px" />
                              </td>
                          </tr>
                      </table>
                  </td>
              </tr>
        </table>
    
      </div>
</div>
        <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">แหล่งที่ได้รับยา</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">   
          <table align="left" width="100%">
            <tr>
                <td width="100">ประเภท</td>
                <td colspan="2">
                    <asp:RadioButtonList ID="optMedFrom" runat="server" RepeatDirection="Horizontal" RepeatColumns="3">
                <asp:ListItem Value="1" >โรงพยาบาลของรัฐ</asp:ListItem>
                <asp:ListItem Value="2">โรงพยาบาลเอกชน</asp:ListItem>
                <asp:ListItem Value="3">คลินิก</asp:ListItem>
                <asp:ListItem Value="4">ร้านยา</asp:ListItem>
                <asp:ListItem Value="9">อื่นๆ</asp:ListItem>
            </asp:RadioButtonList>

                </td>
            </tr>
              
              <tr>
                  <td>สถานพยาบาล</td>
                  <td>
                      <asp:TextBox ID="txtHospital" runat="server" CssClass="OptionControl" Width="100%"></asp:TextBox>
                  </td>
                  <td width="75" align="center">
                      <asp:Button ID="cmdAddHospital" runat="server" CssClass="buttonSave" Text="เพิ่ม" Width="70px" />
                  </td>
              </tr>
              <tr>
                  <td colspan="3">
                      <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                          <ContentTemplate>
                              <asp:GridView ID="grdHospital" runat="server" CssClass="table table-hover" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%">
                                  <RowStyle BackColor="White" VerticalAlign="Top" />
                                  <columns>
                                      <asp:BoundField DataField="nRow" HeaderText="No">
                                      <HeaderStyle HorizontalAlign="Center" />
                                      <itemstyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="HospitalType" HeaderText="ประเภท">
                                      <headerstyle HorizontalAlign="Left" />
                                      <itemstyle HorizontalAlign="Left" />
                                      </asp:BoundField>
                                      <asp:BoundField DataField="HospitalName" HeaderText="สถานพยาบาล" />
                                      <asp:TemplateField HeaderText="ลบ">
                                          <ItemTemplate>
                                              <asp:ImageButton ID="imgDel_H" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' CssClass="gridbutton" ImageUrl="images/delete.png" />
                                          </ItemTemplate>
                                          <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                                      </asp:TemplateField>
                                  </columns>
                                  <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                  <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                                  <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                  <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                                  <EditRowStyle BackColor="#2461BF" />
                                  <AlternatingRowStyle BackColor="#F7F7F7" />
                              </asp:GridView>
                          </ContentTemplate>
                          <Triggers>
                              <asp:AsyncPostBackTrigger ControlID="cmdAddHospital" EventName="Click" />
                          </Triggers>
                      </asp:UpdatePanel>
                  </td>
              </tr>
        </table>
    
      </div>
</div>


    </section>
    <section class="col-lg-7 connectedSortable"> 
       <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Laboratory</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">  
            <table width="100%">            
                <tr>
                    <td valign="top">
                        
                        
                        <asp:GridView ID="grdLab" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" DataKeyNames="LabUID" Width="100%" CssClass="table table-hover">
                            <RowStyle BackColor="White" VerticalAlign="Top" CssClass="small" />
                            <columns>
                                <asp:BoundField DataField="Name" HeaderText="Name" >
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="ผล">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtResultValue" runat="server" CssClass="LabResult" Text='<%# DataBinder.Eval(Container.DataItem, "ResultValue") %>' Width="70px"  ></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="UOM" HeaderText="หน่วย" />
                                <asp:BoundField DataField="NormalRange" HeaderText="ค่าปกติ" />
                            </columns>
                            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <headerstyle   Font-Bold="True" VerticalAlign="Middle" />
                            <EditRowStyle BackColor="#2461BF" />
                            <AlternatingRowStyle BackColor="#F7F7F7" />
                        </asp:GridView>
                     </td>
                    <td  valign="top">
                      
                         <asp:GridView ID="grdLab2" runat="server" AutoGenerateColumns="False" CellPadding="0" DataKeyNames="LabUID" ForeColor="#333333" GridLines="None" Width="100%" CssClass="table table-hover">
                            <RowStyle BackColor="White" VerticalAlign="Top" CssClass="small" />
                            <columns>
                                <asp:BoundField DataField="Name" HeaderText="Name">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="ผล">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtResultValue0" runat="server" CssClass="LabResult" Text='<%# DataBinder.Eval(Container.DataItem, "ResultValue") %>' Width="70px"></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="UOM" HeaderText="UOM" />
                                <asp:BoundField DataField="NormalRange" HeaderText="ค่าปกติ" />
                            </columns>
                            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <headerstyle   Font-Bold="True" VerticalAlign="Middle" />
                            <EditRowStyle BackColor="#2461BF" />
                            <AlternatingRowStyle BackColor="#F7F7F7" />
                        </asp:GridView>
                      
                        


                    </td>
                </tr>
                <tr>
                  
                    <td align="center" colspan="2">
                        <asp:Button ID="cmdAddLab" runat="server" CssClass="buttonSave" Text="บันทึก" Width="100px" />
                    </td>
                </tr>

            </table>
       </div>
        <div class="box-footer">
          
            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="buttonFind"   NavigateUrl="ResultCumulative.aspx" Target="_blank" Height="30px">Cumulative View</asp:HyperLink>
          
        </div>
        <!-- /.box-footer-->
      </div>
       <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">PE</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">  
            <table width="100%">            
                <tr>
                    <td valign="top" width="100">การตรวจเท้า</td>
                    <td  valign="top">
                        <table width="100%">
                            <tr>
                                <td width="180">
                                    <asp:CheckBox ID="chkPitting" runat="server" Text="Pitting edema" />
                                </td>
                                <td width="40">ระดับ</td>
                                <td>
                                    <asp:TextBox ID="txtPitting" runat="server" Width="90%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkWound" runat="server" Text="Wound  " />
                                </td>
                                <td>ระบุ</td>
                                <td>
                                    <asp:TextBox ID="txtWound" runat="server" Width="90%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkPeripheral" runat="server" Text="Peripheral Neuropathy " />
                                </td>
                                <td>ระบุ</td>
                                <td>
                                    <asp:TextBox ID="txtPeripheral" runat="server" Width="90%"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                   <tr>
                    <td valign="top">อาการอื่นๆ</td>
                    <td  valign="top">
                        <asp:TextBox ID="txtPEOther" runat="server" Width="90%"></asp:TextBox>
                       </td>
                </tr>
                <tr>
                  
                    <td align="center" colspan="2">
                        <asp:Button ID="cmdPE_Save" runat="server" CssClass="buttonSave" Text="บันทึก" Width="100px" />
                    </td>
                </tr>

            </table>
       </div>
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
       
    </section>
</div>

    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">ปัญหาเรื่องการปรับเปลี่ยนพฤติกรรม</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">  
  <table width="100%">
                <tr>
                    <td width="150">เรื่อง</td>
                    <td>
                        <asp:DropDownList ID="ddlBehavior" runat="server" CssClass="form-control select2" AutoPostBack="True" Width="90%">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td width="150">กรณี อื่นๆ ระบุ</td>
                    <td>
                        <asp:TextBox ID="txtProblemBehaviorOther" runat="server" CssClass="OptionControl2" Width="90%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Interventions</td>
                    <td>
                        <asp:TextBox ID="txtInterventionB" runat="server" CssClass="OptionControl2" Width="90%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td> <asp:CheckBox ID="chkNotFollow" runat="server" AutoPostBack="True" CssClass="text-blue" Text="ไม่ใช่การติดตามผล" />

                    </td>
                </tr>
                <tr>
                    <td valign="top">กรณีติดตามผล 
                        <br /> ผลการติดตาม
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanelB" runat="server">
                            <ContentTemplate>
                                <asp:Panel ID="pnFat" runat="server">
                                    <table>
                                        <tr>
                                            <td>สิ่งที่พบ</td>
                                            <td>
                                                <asp:RadioButtonList ID="optFat" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem>น้ำหนัก</asp:ListItem>
                                                    <asp:ListItem>รอบเอว</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnEating" runat="server">
                                    <table>
                                        <tr>
                                            <td>รส</td>
                                            <td>
                                                <asp:RadioButtonList ID="optTast" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem>รสจัด</asp:ListItem>
                                                    <asp:ListItem>หวาน</asp:ListItem>
                                                    <asp:ListItem>เค็ม</asp:ListItem>
                                                    <asp:ListItem>มัน</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:RadioButtonList ID="optResultB" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Value="สูบลดลง">สูบลดลง</asp:ListItem>
                                                <asp:ListItem Value="สูบเท่าเดิม">สูบเท่าเดิม</asp:ListItem>
                                                <asp:ListItem Value="สูบเพิ่มขึ้น">สูบเพิ่มขึ้น</asp:ListItem>
                                                <asp:ListItem Value="เลิกสูบแล้ว">เลิกสูบแล้ว</asp:ListItem>
                                                <asp:ListItem Value="อื่นๆ">อื่นๆ</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>ระบุ</td>
                                        <td>
                                            <asp:TextBox ID="txtResultOtherB" runat="server" CssClass="OptionControl2" Width="150px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <table>
                                    <tr>
                                        <td>จาก</td>
                                        <td>
                                            <asp:TextBox ID="txtBegin" runat="server"></asp:TextBox>
                                        </td>
                                        <td>เป็น</td>
                                        <td>
                                            <asp:TextBox ID="txtEnd" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlBehavior" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
              
                <tr>
                    <td colspan="2">
                   
                    </td>
                </tr>
                <tr>
                    <td>ID:<asp:Label ID="lblUID_B" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="cmdBehaviorAdd" runat="server" CssClass="buttonSave" Text="เพิ่มปัญหาพฤติกรรม" />
                        &nbsp;<asp:Button ID="cmdBehaviorCancel" runat="server" CssClass="buttonCancle" Text="ยกเลิก" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                       <asp:Panel ID="pnAlertB" runat="server">
                  <div class="alert alert-danger alert-dismissible">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">&times;</a>
                
                <h4><i class="icon fa fa-ban"></i> Warning!</h4>
                    <p><asp:Label ID="lblAlertB" runat="server"></asp:Label></p>
              </div>
                   </asp:Panel>

                           <asp:Panel ID="pnB_Success" runat="server" Visible="false">  
            <div class="alert alert-success fade in alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">&times;</a>
  <strong>Success!</strong> บันทึกข้อมูลเรียบร้อย.
</div>
    
                     </asp:Panel>   

</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                   
                                <asp:GridView ID="grdBehavior" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%" CssClass="table table-hover">
                                    <RowStyle BackColor="White" VerticalAlign="Top" />
                                    <columns>
                                        <asp:BoundField DataField="nRow" HeaderText="No">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <itemstyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ServiceDate" HeaderText="วันที่">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ProblemDesc" HeaderText="ปัญหาที่พบ" />
                                        <asp:BoundField DataField="Interventions" HeaderText="Interventions">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FinalResult" HeaderText="ผลการติดตาม" />
                                        <asp:TemplateField HeaderText="Edit">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEdit_B" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID")%>' CssClass="gridbutton" ImageUrl="images/icon-edit.png"  Visible='<%# DataBinder.Eval(Container.DataItem, "isDeletable") %>' />
                                            </ItemTemplate>
                                            <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Del">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgDel_B" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' CssClass="gridbutton" ImageUrl="images/delete.png" Visible='<%# DataBinder.Eval(Container.DataItem, "isDeletable") %>' />
                                            </ItemTemplate>
                                            <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                    </columns>
                                    <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                                    <EditRowStyle BackColor="#2461BF" />
                                    <AlternatingRowStyle BackColor="#F7F7F7" />
                                </asp:GridView>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="cmdBehaviorAdd" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
       </div>
        <div class="box-footer">
          
        </div>
  
      </div>
 
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Drug Related Problem</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">  
     
        <table width="100%">
            <tr>
                <td width="150">Ref. ID</td>
                <td>
                    <asp:Label ID="lblUID_Drug" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>ปัญหาที่พบ</td>
                <td>
                    <asp:DropDownList ID="cboProblemGroup" runat="server" AutoPostBack="True" CssClass="form-control select2" Width="99%">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td> อาการ/รายละเอียดปัญหา</td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>               
                                    <asp:DropDownList ID="cboProblemItem" runat="server" CssClass="form-control select2" Width="99%">
                                    </asp:DropDownList>                              
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="cboProblemGroup"  EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
            </tr>
           
            <tr>
                <td>กรณี อื่นๆ ระบุ</td>
                <td>
                    <asp:TextBox ID="txtProblemOtherDrug" runat="server" CssClass="form-control" Width="99%"></asp:TextBox>
                </td>
            </tr>
             <tr>
                 <td>Drug related</td>
                 <td>                    
                     <asp:DropDownList ID="ddlDrug" runat="server" CssClass="form-control select2" title="Please select">
                                </asp:DropDownList>                    
                 </td>
            </tr>
            <tr>
                <td>Interventions</td>
                <td>
                    <asp:TextBox ID="txtInterventionDrug" runat="server" CssClass="form-control" Width="99%"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td valign="top">สรุปการแก้ไขปัญหา</td>
                <td>
                    <asp:RadioButtonList ID="optResult_D" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" AutoPostBack="True">
                        <asp:ListItem Value="1" >แก้ได้เลย (Resolved)</asp:ListItem>
                        <asp:ListItem Value="2">ส่งต่อ (Refer)</asp:ListItem>  
                        <asp:ListItem Value="3">ดีขึ้นแต่ยังเป็นปัญหา ติดตามต่อ</asp:ListItem>
                        <asp:ListItem Value="4">มีปัญหาเพิ่มขึ้น  / ปัญหาหนักขึ้น  ติดตามต่อ</asp:ListItem>
                        <asp:ListItem Value="5">ไม่ดีขึ้น ติดตามต่อ</asp:ListItem> 
                        <asp:ListItem Value="9">อื่นๆ</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:TextBox ID="txtResultOther_D" runat="server" CssClass="OptionControl2" Width="99%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td valign="top">&nbsp;</td>
                <td>
                    <asp:Button ID="cmdDrugProblemAdd" runat="server" CssClass="buttonSave" Text="เพิ่มปัญหาการใช้ยา" />
                    &nbsp;<asp:Button ID="cmdDrugProblemCancel" runat="server" CssClass="buttonCancle" Text="ยกเลิก" />
                </td>
            </tr>
    </table>
 
                     <asp:Panel ID="pnDRP_Warning" runat="server" Visible="false">                      

                         <div class="alert alert-danger fade in alert-dismissible">
                             <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">&times;</a>
  <strong>Warning!</strong> <asp:Label ID="lblDRP_Warning" runat="server"></asp:Label>
</div>

                     </asp:Panel>
             <asp:Panel ID="pnDRP_Success" runat="server" Visible="false">  
            <div class="alert alert-success fade in alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">&times;</a>
  <strong>Success!</strong> บันทึกข้อมูลเรียบร้อย.
</div>
    
                     </asp:Panel>   
            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdDrugProblem" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%" CssClass="table table-hover">
                        <RowStyle BackColor="White" VerticalAlign="Top" />
                        <columns>
                            <asp:BoundField DataField="nRow" HeaderText="No">
                            <HeaderStyle HorizontalAlign="Center" />
                            <itemstyle HorizontalAlign="Center" VerticalAlign="Top" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ServiceDate" HeaderText="วันที่">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ProblemDesc" HeaderText="ปัญหาที่พบ" />
                            <asp:BoundField DataField="DrugName" HeaderText="Drug related problems">
                            <headerstyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Interventions" HeaderText="Interventions ">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ResultTXT" HeaderText="สรุป" />
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEdit_D" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID")%>' CssClass="gridbutton" ImageUrl="images/icon-edit.png"  Visible='<%# DataBinder.Eval(Container.DataItem, "isDeletable") %>' />
                                </ItemTemplate>
                                <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Del">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDel_D" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' Visible='<%# DataBinder.Eval(Container.DataItem, "isDeletable") %>'  CssClass="gridbutton" ImageUrl="images/delete.png" />
                                </ItemTemplate>
                                <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="#F7F7F7" />
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="cmdDrugProblemAdd" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
       
             <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->  
</div> 

      
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">บันทึกยาเหลือใช้</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">  
     
        <table width="100%">
            <tr>
                <td width="150">Ref. ID</td>
                <td>
                    <asp:Label ID="lblDrugRemainID" runat="server"></asp:Label>
                </td>
            </tr>
             <tr>
                 <td>ยา</td>
                 <td> <asp:DropDownList ID="ddlDrugRemain" runat="server" cssclass="form-control select2">
                                </asp:DropDownList>
                         

                 </td>
            </tr>
            <tr>
                <td>จำนวน</td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtQTYRemain" runat="server" CssClass="OptionControl2" Width="100px"></asp:TextBox>
                            </td>
                            <td>หน่วย</td>
                            <td>
                                <asp:TextBox ID="txtUOMRemain" runat="server" CssClass="OptionControl2" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>ยาจาก (รพ./ร้านยา)</td>
                <td>
                    <asp:TextBox ID="txtHospitalRemain" runat="server" CssClass="OptionControl2" Width="100%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>สาเหตุที่เหลือ</td>
                <td>  
                    <table class="nav-justified">
                        <tr>
                            <td width="260">
                                <asp:DropDownList ID="ddlRemainReason" runat="server" CssClass="form-control select2" Width="250px">
                                </asp:DropDownList>
                            </td>
                            <td width="30">ระบุ</td>
                            <td>
                                <asp:TextBox ID="txtRemarkRemain" runat="server" Width="100%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                   
                </td>
            </tr>
            <tr>
                <td valign="top">&nbsp;</td>
                <td>
                    <asp:Button ID="cmdDrugRemain" runat="server" CssClass="buttonSave" Text="เพิ่มยาเหลือใช้" />
                    &nbsp;</td>
            </tr>
    </table>
 
                     <asp:Panel ID="pnDrugRemainAlert" runat="server">
                         <div class="alert alert-danger alert-dismissible">
                             <h4><i class="icon fa fa-ban"></i>Warning!</h4>
                             <p>
                                 <asp:Label ID="lblRemainAlert" runat="server"></asp:Label>
                             </p>
                         </div>
                     </asp:Panel>
            <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdDrugRemain" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%" CssClass="table table-hover">
                        <RowStyle BackColor="White" VerticalAlign="Top" />
                        <columns>
                            <asp:BoundField DataField="nRow" HeaderText="No">
                            <HeaderStyle HorizontalAlign="Center" />
                            <itemstyle HorizontalAlign="Center" VerticalAlign="Top" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ServiceDate" HeaderText="วันที่">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DrugName" HeaderText="ยา" >
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="QTY" HeaderText="จำนวน">
                            </asp:BoundField>
                            <asp:BoundField DataField="UOM" HeaderText="หน่วย">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="RemainFrom" HeaderText="เหลือจาก" />
                            <asp:BoundField DataField="ReasonDesc" HeaderText="เหตุผลที่เหลือ" />
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEditRemain" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID")%>' CssClass="gridbutton" ImageUrl="images/icon-edit.png"  Visible='<%# DataBinder.Eval(Container.DataItem, "isDeletable") %>' />
                                </ItemTemplate>
                                <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Del">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDelRemain" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' Visible='<%# DataBinder.Eval(Container.DataItem, "isDeletable") %>'  CssClass="gridbutton" ImageUrl="images/delete.png" />
                                </ItemTemplate>
                                <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="#F7F7F7" />
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="cmdDrugProblemAdd" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
       
             <!-- /.box-body -->
       
      </div>
      <!-- /.box -->  
</div> 

      
      <asp:Panel ID="pnDrugRefill" runat="server">

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">บันทึกจ่ายยา</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">  
      
        <table width="100%">
            <tr>
                <td width="150">Ref. ID</td>
                <td>
                    <asp:Label ID="lblDrugRefillID" runat="server"></asp:Label>
                    <asp:HiddenField ID="hdRefillHeaderUID" runat="server" />
                </td>
            </tr>
             <tr>
                 <td>ยา</td>
                 <td><asp:DropDownList ID="ddlDrugRefill" runat="server" cssclass="form-control select2">
                                </asp:DropDownList>
                       
                 </td>
            </tr>
            <tr>
                <td>จำนวน</td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtQTYRefill" runat="server" CssClass="OptionControl2" Width="100px"></asp:TextBox>
                            </td>
                            <td>หน่วย</td>
                            <td>
                                <asp:TextBox ID="txtUOMRefill" runat="server" CssClass="OptionControl2" Width="100px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>วิธีใช้/รับประทาน</td>
                <td>
                    <asp:TextBox ID="txtRefillDesc" runat="server" CssClass="OptionControl2" Width="100%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td valign="top">&nbsp;</td>
                <td>
                    <asp:Button ID="cmdDrugRefill" runat="server" CssClass="buttonSave" Text="เพิ่มการจ่ายยา" />
                    &nbsp;</td>
            </tr>
    </table>
 
                     <asp:Panel ID="pnDrugRefillAlert" runat="server">
                         <div class="alert alert-danger alert-dismissible">
                             <h4><i class="icon fa fa-ban"></i>Warning!</h4>
                             <p>
                                 <asp:Label ID="lblDrugRefillAlert" runat="server"></asp:Label>
                             </p>
                         </div>
                     </asp:Panel>
         

            <asp:UpdatePanel ID="UpdatePanel44" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="grdDrugRefill" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%" CssClass="table table-hover">
                        <RowStyle BackColor="White" VerticalAlign="Top" />
                        <columns>
                            <asp:BoundField DataField="nRow" HeaderText="No">
                            <HeaderStyle HorizontalAlign="Center" />
                            <itemstyle HorizontalAlign="Center" VerticalAlign="Top" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ServiceDate" HeaderText="วันที่">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DrugName" HeaderText="ยา" >
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="QTY" HeaderText="จำนวน">
                            </asp:BoundField>
                            <asp:BoundField DataField="UOM" HeaderText="หน่วย">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Edit">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgEditRefill" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID")%>' CssClass="gridbutton" ImageUrl="images/icon-edit.png" />
                                </ItemTemplate>
                                <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Del">
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgDelRefill" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>'  CssClass="gridbutton" ImageUrl="images/delete.png" />
                                </ItemTemplate>
                                <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="#F7F7F7" />
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="cmdDrugProblemAdd" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
       
             <!-- /.box-body -->
       
      </div>
      <!-- /.box -->  
</div> 
</asp:Panel>
  <asp:Panel ID="pnRefer" runat="server">
   <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Refer</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">  
     
        <table width="100%">
             <tr>
                 <td width="150">Refer to</td>
                 <td>
                     <asp:DropDownList ID="ddlReferHospital" runat="server" cssclass="form-control select2">
                     </asp:DropDownList>

                 </td>
            </tr>
            <tr>
                <td>เหตุผลที่ Refer</td>
                <td> <asp:TextBox ID="txtReferReason" runat="server" CssClass="OptionControl2" Width="100%"></asp:TextBox>
              
                </td>
            </tr>
            <tr>
                <td valign="top">&nbsp;</td>
                <td>
                    <asp:Button ID="cmdRefer" runat="server" CssClass="buttonSave" Text="บันทึกการส่งต่อ" />
                    &nbsp;<asp:Button ID="cmdCancelRefer" runat="server" CssClass="buttonCancle" Text="ยกเลิกการส่งต่อ" />
                </td>
            </tr>
    </table>
 
                     <asp:Panel ID="pnReferAlert" runat="server">
                         <div class="alert alert-danger alert-dismissible">
                             <h4><i class="icon fa fa-ban"></i>Warning!</h4>
                             <p>
                                 <asp:Label ID="lblReferAlert" runat="server"></asp:Label>
                             </p>
                         </div>
                     </asp:Panel>
           
       
             <!-- /.box-body -->
       
      </div>
      <!-- /.box -->  
</div> 
      </asp:Panel>
</asp:Panel>

      <table   border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td width="100">สถานะ</td>
        <td>
            <asp:CheckBox ID="chkStatus" runat="server" Text="จบการทำงาน" />
          </td>
      </tr>
    </table>


      <div align="center" >    
         <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="100px" />
    </div>    <br />


</section>   
 
</asp:Content>