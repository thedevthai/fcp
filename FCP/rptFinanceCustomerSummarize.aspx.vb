﻿Public Class rptFinanceCustomerSummarize
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim dtL As New DataTable
    Dim grp As New OrderController
    Dim ctlLct As New LocationController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lblStartDate.Text = Request("b")
        lblEndDate.Text = Request("e")
        LoadProjectName()
        LoadLocationData()
        LoadDataToGrid(Request("b"), Request("e"))

        If Request("ex") = 1 Then
            Response.AppendHeader("content-disposition", "attachment;filename=" & ReportsName & ".xls")
            Response.Charset = ""
            Response.ContentType = "application/vnd.ms-excel"
        End If

    End Sub
    Private Sub LoadProjectName()
        Dim ctlProj As New ProjectController
        lblProjectName.Text = ctlProj.Project_GetName(Request("pj"))
    End Sub

    Private Sub LoadLocationData()
        dt = ctlLct.Location_GetByID(Request.Cookies("LocationID").Value)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblLocationName.Text = .Item("LocationName") & " (" & .Item("LocationID") & ")"
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadDataToGrid(ByVal Bdate As String, ByVal EDate As String)
        Dim StartDate As String = "0"
        Dim EndDate As String = "0"
        Dim Sumcount As Integer = 0
        Dim SumAmount As Integer = 0
        If Bdate = "" And EDate = "" Then
            StartDate = 0
            EndDate = 0
        Else
            StartDate = ConvertStrDate2DBString(Bdate)
            EndDate = ConvertStrDate2DBString(EDate)
        End If

        dtL = grp.Order_GetSummaryByLocation(Request("lid"), StartDate, EndDate, StrNull2Zero(Request("pj")))

        If dtL.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dtL
                .DataBind()

                'Dim PayAmount As Double
                'Dim ctlP As New PaymentController

                For i = 0 To dtL.Rows.Count - 1
                    .Rows(i).Cells(0).Text = i + 1
                    'PayAmount = 0
                    'Select Case Left(grdData.Rows(i).Cells(1).Text, 1)
                    '    Case "F"
                    '        PayAmount = ctlP.Payment_GetAmountByLocation(DBNull2Str(Request.Cookies("LocationID").Value), StrNull2Long(StartDate), 1)
                    '    Case "A"
                    '        PayAmount = ctlP.Payment_GetAmountByLocation(DBNull2Str(Request.Cookies("LocationID").Value), StrNull2Long(StartDate), 2)
                    '    Case Else
                    '        PayAmount = 0
                    'End Select

                    '.Rows(i).Cells(4).Text = (DBNull2Zero(dtL.Rows(i)("nCount")) * PayAmount).ToString("#,###")
                    Sumcount = Sumcount + DBNull2Zero(dtL.Rows(i)("nCount"))
                    SumAmount = SumAmount + DBNull2Zero(dtL.Rows(i)("Amount"))

                Next

                lblSumCount.Text = Sumcount
                lblSumPrice.Text = SumAmount.ToString("#,###")
            End With
        Else
            grdData.DataSource = Nothing
            grdData.Visible = False
        End If

        dtL = Nothing
    End Sub

End Class