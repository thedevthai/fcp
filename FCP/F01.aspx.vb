﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Subgurim.Controles 
Public Class F01
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlLct As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlOrder As New OrderController
    Dim ServiceDate As Long
    Public F01_DocFile, F01_sFile As String
    Private _dateDiff As Object

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Session("patientid") Is Nothing Then
            Response.Redirect("PatientSearch.aspx")
        End If

        If Not IsPostBack Then
            isAdd = True
            txtCGNo.Visible = False
            lblCGday.Visible = False
            txtCGYear.Visible = False
            lblCGYear.Visible = False
            lblCgType.Visible = False
            optCigarette.Visible = False
            lblAlcohol.Visible = False
            txtAlcoholFQ.Visible = False

            If Request("acttype") = "view" Then
                cmdSave.Visible = False
            Else
                cmdSave.Visible = True
            End If


            LoadPharmacist(Request.Cookies("LocationID").Value)
            txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)
            CalculateAge()
            CalculateGender()
            LoadFormData()

            CalScore()

        End If

        If (FileUploaderAJAX1.IsPosting) Then
            ServiceDate = CLng(ConvertDate2DBString(Today.Date))
            'F01_sFile = Path.GetExtension(FileUploaderAJAX1.PostedFile.FileName)
            F01_DocFile = "Refer" & "_" & lblLocationID.Text & "_" & ServiceDate & "_" & ConvertTimeToString(Now())
            UploadFile(F01_DocFile)
        End If


        'txtScreenBP1.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")
        'txtScreenBP2.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")
        'txtScreenBP3.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")

        'If HiddenField1.Value <> "" Then
        '    CalBPAVG()
        'End If
        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        ''txtCardID.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")

       
    End Sub
    Private Sub LoadPharmacist(LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With
            End If
        End If
        dtP = Nothing
    End Sub

    Private Sub LoadLocationData(LocationID As String)
        dt = ctlLct.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblLocationID.Text = .Item("LocationID")
                lblLocationName.Text = .Item("LocationName")
                lblLocationProvince.Text = .Item("ProvinceName")
            End With
        End If
        dt = Nothing
    End Sub

    'Private Sub LoadProvinceToDDL()
    '    dt = ctlLct.LoadProvince
    '    If dt.Rows.Count > 0 Then
    '        With ddlProvince
    '            .Enabled = True
    '            .DataSource = dt
    '            .DataTextField = "ProvinceName"
    '            .DataValueField = "ProvinceID"
    '            .DataBind()
    '            .SelectedIndex = 1
    '        End With
    '    End If
    '    dt = Nothing
    'End Sub

    'Private Sub BindDayToDDL()
    '    Dim sD As String = ""
    '    For i = 1 To 31
    '        If i < 10 Then
    '            sD = "0" & i
    '        Else
    '            sD = i
    '        End If

    '        With ddlDay
    '            .Items.Add(i)
    '            .Items(i - 1).Value = sD
    '            .SelectedIndex = 0
    '        End With
    '    Next
    'End Sub
    'Private Sub BindMonthToDDL()
    '    Dim sD As String = ""

    '    For i = 1 To 12
    '        If i < 10 Then
    '            sD = "0" & i
    '        Else
    '            sD = i
    '        End If

    '        With ddlMonth
    '            .Items.Add(DisplayNumber2Month(i))
    '            .Items(i - 1).Value = sD
    '            .SelectedIndex = 0
    '        End With
    '    Next
    'End Sub

    'Private Sub BindYearToDDL()
    '    Dim ctlb As New ApplicationBaseClass
    '    Dim sDate As Date
    '    Dim y As Integer

    '    sDate = ctlb.GET_DATE_SERVER()

    '    If sDate.Year < 2300 Then
    '        y = (sDate.Year + 543)
    '    Else
    '        y = sDate.Year
    '    End If
    '    Dim i As Integer = 0
    '    Dim n As Integer = y - 17

    '    For i = 0 To 9
    '        With ddlYear
    '            .Items.Add(n)
    '            .Items(i).Value = n
    '            .SelectedIndex = 0
    '        End With
    '        n = n - 1
    '    Next
    'End Sub

    Private Sub LoadFormData()
        Dim pYear As Integer

        If Not Request("fid") Is Nothing Then
            dt = ctlOrder.GetOrder_ByID(StrNull2Zero(Request("fid")))
        Else
            If Not Request("yid") Is Nothing Then
                pYear = CInt(Request("yid"))
            Else
                pYear = Year(ctlLct.GET_DATE_SERVER)
                If pYear < 2500 Then
                    pYear = pYear + 543
                End If

            End If
            dt = ctlOrder.LocationOrder_ByYear(pYear, FORM_TYPE_ID_F01, Request.Cookies("LocationID").Value, Session("patientid"))
        End If

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                lblID.Text = .Item("itemID")
                Session("patientid") = DBNull2Lng(.Item("PatientID"))
                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                txtTime.Text = String.Concat(.Item("ServiceTime"))

                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))

                CalculateAge()

                'Dim sName(1) As String
                'sName(0) = ""
                'sName(1) = ""
                'sName = Split(DBNull2Str(.Item("CustName")), " ")
                'txtFName.Text = sName(0)
                'If sName.Length > 1 Then
                '    txtLName.Text = sName(1)
                'End If


                'optGender.SelectedValue = DBNull2Str(.Item("Gender"))
                'txtAges.Text = DBNull2Str(.Item("Ages"))
                'txtCardID.Text = DBNull2Str(.Item("CardID"))
                'txtBirthDate.Text = DisplayStr2ShortDateTH(DBNull2Str(.Item("BirthDate")))
                'txtTelephone.Text = DBNull2Str(.Item("Telephone"))
                'txtMobile.Text = DBNull2Str(.Item("Mobile"))
                'optAddressType.SelectedValue = DBNull2Str(.Item("AddressType"))
                'txtAddress.Text = DBNull2Str(.Item("AddressNo"))
                'txtRoad.Text = DBNull2Str(.Item("Road"))
                'txtDistrict.Text = DBNull2Str(.Item("District"))
                'txtCity.Text = DBNull2Str(.Item("City"))
                'ddlProvince.SelectedValue = DBNull2Str(.Item("ProvinceID"))
                'optClaim.SelectedValue = DBNull2Str(.Item("MainClaim"))
                chkStatus.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("Status")))

                If DBNull2Zero(.Item("Status")) >= 3 Then  'Year(ctlLct.GET_DATE_SERVER.Date) <> DBNull2Zero(.Item("BYear")) 
                    cmdSave.Visible = False 
                Else
                    If Request.Cookies("LocationID").Value <> String.Concat(.Item("LocationID")) Then
                        LoadLocationData(String.Concat(.Item("LocationID")))
                        If Request("t") = "edit" Then
                            cmdSave.Visible = True 
                        Else
                            cmdSave.Visible = False 
                        End If
                    Else
                        LoadLocationData(Request.Cookies("LocationID").Value)
                    End If
                End If


                LoadMetabolicRisk(StrNull2Zero(lblID.Text))
            End With
        Else
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing

    End Sub
    Private Sub LoadMetabolicRisk(pID As Integer)
        Dim dtM As New DataTable

        dtM = ctlOrder.MetabolicRisk_ByOrderID(pID)
        If dtM.Rows.Count > 0 Then
            With dtM.Rows(0)
                chkFamDisease1.Checked = Decimal2Boolean(.Item("isDiabetes"))
                chkFamDisease2.Checked = Decimal2Boolean(.Item("isBloodPressure"))
                chkFamDisease3.Checked = Decimal2Boolean(.Item("isAtheroma"))
                chkFamDisease4.Checked = Decimal2Boolean(.Item("isCoronary"))
                chkFamDisease5.Checked = Decimal2Boolean(.Item("isParalysis"))
                chkFamDisease6.Checked = Decimal2Boolean(.Item("isNone"))

                optSmoke.SelectedValue = String.Concat(.Item("Smoke"))
                txtCGYear.Text = String.Concat(.Item("SmokeYear"))
                txtCGNo.Text = String.Concat(.Item("SmokeCigarette"))
                optCigarette.SelectedValue = String.Concat(.Item("CigaretteType"))
                optAlcohol.SelectedValue = String.Concat(.Item("Alcohol"))
                txtAlcoholFQ.Text = String.Concat(.Item("AlcoholFQ"))
                optExercise.SelectedValue = String.Concat(.Item("Exercise"))

                chkFood_Sweet.Checked = Decimal2Boolean(String.Concat(.Item("isFood_Sweet")))
                chkFood_Salt.Checked = Decimal2Boolean(String.Concat(.Item("isFood_Salty")))
                chkFood_Fat.Checked = Decimal2Boolean(String.Concat(.Item("isFood_Fat")))
                chkFood_Other.Checked = Decimal2Boolean(String.Concat(.Item("isFood_Other")))

                optSleep.SelectedValue = String.Concat(.Item("Sleep"))
                optRiskGender.SelectedValue = String.Concat(.Item("RiskGender"))
                optRiskAge.SelectedValue = String.Concat(.Item("RiskAge"))
                optRiskFam.SelectedValue = String.Concat(.Item("RiskDiabetes"))
                optRiskBMI.SelectedValue = String.Concat(.Item("RiskBMI"))
                optRiskShape.SelectedValue = String.Concat(.Item("RiskShape"))
                optRiskBloodAVG.SelectedValue = String.Concat(.Item("RiskBloodAVG"))
                txtFat.Text = String.Concat(.Item("SumRiskFat"))
                txtBlood.Text = String.Concat(.Item("SumRiskBloodPressure"))
                txtDiabetes.Text = String.Concat(.Item("SumRiskDiabetes"))
                optRiskFat.SelectedValue = String.Concat(.Item("isRiskFat"))
                optRiskBloodRisk.SelectedValue = String.Concat(.Item("isRiskBloodPressure"))
                optRiskBlood.SelectedValue = String.Concat(.Item("isRiskDiabetes"))
                txtScreenAge.Text = String.Concat(.Item("ScreenAge"))
                txtScreenShape.Text = String.Concat(.Item("ScreenShape"))
                txtScreenWeight.Text = String.Concat(.Item("ScreenWeight"))
                txtScreenHeigh.Text = String.Concat(.Item("ScreenHeight"))
                txtScreenBMI.Text = String.Concat(.Item("BMI"))
                txtScreenBP1.Text = String.Concat(.Item("BP1"))
                txtScreenBP2.Text = String.Concat(.Item("BP2"))
                txtScreenBP1_2.Text = String.Concat(.Item("BP1_2"))
                txtScreenBP2_2.Text = String.Concat(.Item("BP2_2"))
                txtScreenBPAVG.Text = String.Concat(.Item("BPAVG"))
                txtScreenBPAVG2.Text = String.Concat(.Item("BPAVG_2"))
                txtScreenHR.Text = String.Concat(.Item("HR"))
                txtFBS.Text = String.Concat(.Item("FBS"))
                txtRandom.Text = String.Concat(.Item("Postprandial"))
                txtHourAfter.Text = String.Concat(.Item("TimeAfter"))


                Dim pstr() As String
                pstr = Split(String.Concat(.Item("ServicePlan")), "|")

                For i = 0 To pstr.Length - 1
                    Select Case pstr(i)
                        Case "1"
                            chkPlan.Items(0).Selected = True
                        Case "2"
                            chkPlan.Items(1).Selected = True
                        Case "3"
                            chkPlan.Items(2).Selected = True
                    End Select
                Next

                txtPlanHospital.Text = String.Concat(.Item("HospitalName"))

                If String.Concat(.Item("ReferDocFile")) <> "" Then
                    hlnkReferFile.Text = String.Concat(.Item("ReferDocFile"))
                    hlnkReferFile.NavigateUrl = "~/" & tmpUpload & "/" & String.Concat(.Item("ReferDocFile"))
                Else
                    hlnkReferFile.Text = "ไม่มีไฟล์แนบ"
                End If

            End With
        End If
        dtM = Nothing
    End Sub
    Private Sub UploadFile(sName As String)

        Dim pf As HttpPostedFileAJAX = FileUploaderAJAX1.PostedFile
        Dim xfile As String

        If pf.ContentType.Equals("image/jpeg") Then
            xfile = ".jpeg"
        ElseIf pf.ContentType.Equals("image/jpg") Then
            xfile = ".jpg"
        ElseIf pf.ContentType.Equals("image/png") Then
            xfile = ".png"
        ElseIf pf.ContentType.Equals("image/gif") Then
            xfile = ".gif"
        Else
            xfile = ".jpg"
        End If

        'กรณีต้องการต้องสอบชนิดและขนาดไฟล์
        If ((pf.ContentType.Equals("image/jpeg") Or pf.ContentType.Equals("image/jpg") Or pf.ContentType.Equals("image/png") Or pf.ContentType.Equals("image/gif")) And pf.ContentLength <= 500 * 1024) Then

            Dim objfile As FileInfo = New FileInfo(Server.MapPath("~/" & tmpUpload & "/" & Session("ProfileID") & xfile))

            If objfile.Exists Then
                objfile.Delete()
            End If

            FileUploaderAJAX1.SaveAs("~/" & tmpUpload, sName & xfile)

            Session("picname") = sName & xfile

            ' picStudent.ImageUrl = "~/" & tmpUpload & "/" & Session("ProfileID") & ".jpg"

        Else

            'lblAlert.Text = "ไม่สามารถอัปโหลดรูปท่านได้ เนื่องจากขนาดรูปท่านใหญ่เกิน 500K กรุณาลองใหม่ภายหลัง"
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถอัปโหลดรูปท่านได้ เนื่องจากขนาดรูปท่านใหญ่เกิน 500K กรุณาลองใหม่ภายหลัง');", True)
        End If

    End Sub

    'Sub UploadFile(ByVal Fileupload As Object, sName As String)
    '    Dim FileFullName As String = Fileupload.PostedFile.FileName
    '    Dim FileNameInfo As String = Path.GetFileName(FileFullName)
    '    Dim filename As String = Path.GetFileName(Fileupload.PostedFile.FileName)
    '    Dim objfile As FileInfo = New FileInfo(Server.MapPath(tmpUpload))
    '    If FileNameInfo <> "" Then
    '        'Fileupload.PostedFile.SaveAs(objfile.DirectoryName & "\" & filename)
    '        'sFile = Path.GetExtension(FileFullName)
    '        Fileupload.PostedFile.SaveAs(objfile.DirectoryName & "\" & tmpUpload & "\" & sName)
    '    End If
    '    objfile = Nothing
    'End Sub
    Protected Sub optRiskShape_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRiskShape.SelectedIndexChanged
        txtFat.Text = optRiskShape.SelectedValue
        optRiskFat.SelectedValue = optRiskShape.SelectedValue
        CalScore()
    End Sub
    Private Sub CalScore()
        'ข้อ 5
        txtFat.Text = optRiskShape.SelectedValue
        optRiskFat.SelectedValue = txtFat.Text
        'ข้อ 6
        txtBlood.Text = optRiskBloodAVG.SelectedValue
        optRiskBloodRisk.SelectedValue = txtBlood.Text
        'ข้อ 1-6

        txtDiabetes.Text = StrNull2Zero(optRiskGender.SelectedValue) + StrNull2Zero(optRiskAge.SelectedValue) + StrNull2Zero(optRiskFam.SelectedValue) + StrNull2Zero(optRiskBMI.SelectedValue) + StrNull2Zero(optRiskShape.SelectedValue) + StrNull2Zero(optRiskBloodAVG.SelectedValue)

        If CInt(txtDiabetes.Text) <= 2 Then
            optRiskBlood.SelectedValue = 2
        ElseIf CInt(txtDiabetes.Text) >= 3 And CInt(txtDiabetes.Text) <= 5 Then
            optRiskBlood.SelectedValue = 5
        ElseIf CInt(txtDiabetes.Text) >= 6 And CInt(txtDiabetes.Text) <= 8 Then
            optRiskBlood.SelectedValue = 8
        ElseIf CInt(txtDiabetes.Text) >= 9 Then
            optRiskBlood.SelectedValue = 9
        End If
    End Sub
    Private Sub CalBPAVG()
        Dim n As Integer = 0
        Dim sumBP As Double = 0
        Dim sumBP2 As Double = 0

        If Str2Double(txtScreenBP1.Text) > 0 Then
            n = n + 1
            sumBP = sumBP + Str2Double(txtScreenBP1.Text)
        End If
        If Str2Double(txtScreenBP2.Text) > 0 Then
            n = n + 1
            sumBP = sumBP + Str2Double(txtScreenBP2.Text)
        End If

        If Str2Double(txtScreenBP1_2.Text) > 0 Then
            sumBP2 = sumBP2 + Str2Double(txtScreenBP1_2.Text)
        End If
        If Str2Double(txtScreenBP2_2.Text) > 0 Then
            sumBP2 = sumBP2 + Str2Double(txtScreenBP2_2.Text)
        End If

        txtScreenBPAVG.Text = (sumBP / n).ToString("#,###")
        txtScreenBPAVG2.Text = (sumBP2 / n).ToString("#,###")

        If ((sumBP / n) >= 140) Or ((sumBP / n) >= 90) Then
            optRiskBloodAVG.SelectedValue = 2
        Else
            optRiskBloodAVG.SelectedValue = 0
        End If

        CalScore()
    End Sub
    Private Sub CalBMI()
        Dim sumBMI As Double = 0
        Dim mHeight As Double = Str2Double(txtScreenHeigh.Text) / 100
        mHeight = mHeight * mHeight
        sumBMI = Str2Double(txtScreenWeight.Text) / mHeight
        txtScreenBMI.Text = sumBMI.ToString("#,###.##")

        Dim iBMI As Double = Str2Double(txtScreenBMI.Text)

        If iBMI < 23 Then
            optRiskBMI.SelectedValue = 0
        ElseIf iBMI >= 23 And iBMI <= 27.5 Then
            optRiskBMI.SelectedValue = 3
        Else
            optRiskBMI.SelectedValue = 5
        End If
        CalScore()
    End Sub

    Protected Sub optRiskBloodAVG_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRiskBloodAVG.SelectedIndexChanged
        txtBlood.Text = optRiskBloodAVG.SelectedValue
        optRiskBloodRisk.SelectedValue = optRiskBloodAVG.SelectedValue
        CalScore()
    End Sub

    Protected Sub optRiskGender_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRiskGender.SelectedIndexChanged
        CalScore()
    End Sub

    Protected Sub optRiskAge_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRiskAge.SelectedIndexChanged
        CalScore()
    End Sub

    Protected Sub optRiskFam_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRiskFam.SelectedIndexChanged
        CalScore()
    End Sub

    Protected Sub optRiskBMI_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRiskBMI.SelectedIndexChanged
        CalScore()
    End Sub

    Protected Sub txtScreenBP1_TextChanged(sender As Object, e As EventArgs) Handles txtScreenBP1.TextChanged
        CalBPAVG()
        txtScreenBP1_2.Focus()

    End Sub

    Protected Sub txtScreenBP2_TextChanged(sender As Object, e As EventArgs) Handles txtScreenBP2.TextChanged
        CalBPAVG()
        txtScreenBP2_2.Focus()
    End Sub

    Protected Sub txtScreenBP1_2_TextChanged(sender As Object, e As EventArgs) Handles txtScreenBP1_2.TextChanged
        CalBPAVG()
        txtScreenBP2.Focus()

    End Sub

    Protected Sub txtScreenBP2_2_TextChanged(sender As Object, e As EventArgs) Handles txtScreenBP2_2.TextChanged
        CalBPAVG()
        txtScreenHR.Focus()
    End Sub


    Protected Sub optSmoke_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optSmoke.SelectedIndexChanged
        Select Case optSmoke.SelectedValue
            Case "2"
                txtCGYear.Visible = True
                lblCGYear.Visible = True
            Case "1"
                lblCGday.Visible = True
                txtCGNo.Visible = True
                lblCGYear.Visible = True
                txtCGYear.Visible = True
                optAlcohol.Visible = True
            Case Else
                txtCGNo.Visible = False
                lblCGday.Visible = False
                txtCGYear.Visible = False
                lblCGYear.Visible = False
                lblCgType.Visible = False
                optCigarette.Visible = False
        End Select
    End Sub

    Protected Sub optAlcohol_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optAlcohol.SelectedIndexChanged
        Select Case optAlcohol.SelectedValue
            Case "3"
                lblAlcohol.Visible = True
                txtAlcoholFQ.Visible = True
            Case Else
                lblAlcohol.Visible = False
                txtAlcoholFQ.Visible = False
        End Select
    End Sub

    Protected Sub txtScreenWeight_TextChanged(sender As Object, e As EventArgs) Handles txtScreenWeight.TextChanged
        CalBMI()
        txtScreenHeigh.Focus()
    End Sub

    Protected Sub txtScreenHeigh_TextChanged(sender As Object, e As EventArgs) Handles txtScreenHeigh.TextChanged
        CalBMI()
        txtScreenBP1.Focus()
    End Sub

    Private Sub CalculateAge()
        Try

            Dim ctlP As New PatientController

            Dim sAge As Integer

            sAge = DateDiff(DateInterval.Year, CDate(DisplayStr2ShortDateTH(ctlP.Patient_GetBirthDate(Session("patientid")))), CDate(txtServiceDate.Text))

            If sAge < 0 Then
                sAge = sAge + 543
            ElseIf sAge > 543 Then
                sAge = sAge - 543
            End If

            txtScreenAge.Text = sAge.ToString()



        Catch ex As Exception

        End Try
        Dim iAge As Integer = StrNull2Zero(txtScreenAge.Text)

        If iAge <= 44 Then
            optRiskAge.SelectedValue = 0
        ElseIf iAge > 44 And iAge < 50 Then
            optRiskAge.SelectedValue = 1
        Else
            optRiskAge.SelectedValue = 2
        End If

        CalScore()
    End Sub
    Private Sub CalculateGender()
        Try
            Dim dtG As New DataTable

            Dim ctlP As New PatientController
            dtG = ctlP.Patient_GetByID(Session("patientid"))
            If dtG.Rows.Count > 0 Then
                Dim iSex As String = DBNull2Str(dtG.Rows(0)("Gender"))

                If iSex = "ชาย" Then
                    optRiskGender.SelectedValue = "2"
                Else
                    optRiskGender.SelectedValue = "0"
                End If
            End If

        Catch ex As Exception

        End Try

    End Sub


    'Protected Sub txtBirthDate_TextChanged(sender As Object, e As EventArgs) Handles txtBirthDate.TextChanged

    '    Try

    '        txtAges.Text = DateDiff(DateInterval.Year, CDate(txtBirthDate.Text), ctlLct.GET_DATE_SERVER.Date)
    '        txtScreenAge.Text = txtAges.Text
    '        txtCardID.Focus()
    '    Catch ex As Exception

    '    End Try
    '    Dim iAge As Integer = StrNull2Zero(txtAges.Text)

    '    If iAge <= 44 Then
    '        optRiskAge.SelectedValue = 0
    '    ElseIf iAge > 44 And iAge < 50 Then
    '        optRiskAge.SelectedValue = 1
    '    Else
    '        optRiskAge.SelectedValue = 2
    '    End If

    '    CalScore()
    'End Sub

    'Protected Sub optGender_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optGender.SelectedIndexChanged
    '    Select Case optGender.SelectedValue
    '        Case "M"
    '            optRiskGender.SelectedValue = 2
    '        Case Else
    '            optRiskGender.SelectedValue = 0
    '    End Select
    '    CalScore()
    'End Sub

    Protected Sub chkFamDisease1_CheckedChanged(sender As Object, e As EventArgs) Handles chkFamDisease1.CheckedChanged
        If chkFamDisease1.Checked = False Then
            optRiskFam.SelectedValue = 0
        Else
            optRiskFam.SelectedValue = 4
        End If
        CalScore()
    End Sub

    Protected Sub txtScreenBMI_TextChanged(sender As Object, e As EventArgs) Handles txtScreenBMI.TextChanged
        Dim iBMI As Double = Str2Double(txtScreenBMI.Text)

        If iBMI < 23 Then
            optRiskBMI.SelectedValue = 0
        ElseIf iBMI >= 23 And iBMI <= 27.5 Then
            optRiskBMI.SelectedValue = 3
        Else
            optRiskBMI.SelectedValue = 5
        End If
        CalScore()
    End Sub

    Protected Sub txtScreenShape_TextChanged(sender As Object, e As EventArgs) Handles txtScreenShape.TextChanged
        Dim iBMI As Double = StrNull2Zero(txtScreenShape.Text)

        If optRiskGender.SelectedIndex = 0 Then
            If iBMI < 90 Then
                optRiskShape.SelectedValue = 0
            Else
                optRiskShape.SelectedValue = 1
            End If
        Else
            If iBMI < 80 Then
                optRiskShape.SelectedValue = 0
            Else
                optRiskShape.SelectedValue = 1
            End If
        End If
        CalScore()

        txtScreenWeight.Focus()


    End Sub

    Private Sub CalAges()
        Dim iAge As Integer = StrNull2Zero(txtScreenAge.Text)

        If iAge <= 44 Then
            optRiskAge.SelectedValue = 0
        ElseIf iAge > 44 And iAge < 50 Then
            optRiskAge.SelectedValue = 1
        Else
            optRiskAge.SelectedValue = 2
        End If


        CalScore()
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Trim(lblLocationID.Text) = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาตรวจสอบรหัสร้านยา แล้วลองใหม่อีกครั้ง');", True)
            Exit Sub
        End If


        'If Trim(txtFName.Text) = "" Or Trim(txtLName.Text) = "" Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนชื่อ-นามสกุลผู้รับบริการก่อน');", True)
        '    Exit Sub
        'End If

        'If Trim(txtMobile.Text) = "" Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนเบอร์มือถือผู้รับบริการก่อน หากไม่มีให้ใส่เบอร์บ้านในช่องเบอร์มือถือได้")
        '    Exit Sub
        'End If
        If StrNull2Zero(txtTime.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนระยะเวลาในการให้บริการก่อน');", True)
            Exit Sub
        End If
        'If StrNull2Zero(txtAges.Text) = 0 Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนอายุผู้รับบริการก่อน');", True)
        '    Exit Sub
        'End If
        Dim objuser As New UserController
        Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))

        ''Dim BirthDate As String = ConvertStrDate2DBString(txtBirthDate.Text)

        Dim strP As String
        ' If chkPlan.Items(0).Selected Then
        strP = "1"
        '  End If
        If chkPlan.Items(1).Selected Then
            strP &= "|2"
        End If
        If chkPlan.Items(2).Selected Then
            strP &= "|3"
        End If
        Dim GD As String
        If optRiskGender.SelectedIndex = 0 Then
            GD = "M"
        Else
            GD = "F"
        End If
        'CustName = Trim(txtFName.Text) & " " & Trim(txtLName.Text)
        If lblID.Text = "" Then 'Add new

            If ctlOrder.Order_ChkDupCustomer(FORM_TYPE_ID_F01, Session("patientid"), StrNull2Zero(Left(Str(ServiceDate), 4))) = True Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ผู้รับบริการรายนี้เคยได้รับบริการในกิจกรรมนี้แล้ว');", True)
                Exit Sub
            End If

            ctlOrder.F01_Add(lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F01, Session("patientid"), Convert2Status(chkStatus.Checked), 0, Request.Cookies("username").Value, strP, txtPlanHospital.Text, CLng(ConvertDate2DBString(ctlLct.GET_DATE_SERVER)), GD, StrNull2Zero(txtScreenAge.Text))

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Service_Order", "บันทึกเพิ่มกิจกรรมการคัดกรองความเสี่ยงในกลุ่มภาวะโรคเมตาบอลิก(F01) :<<PatientID:" & Session("patientid") & ">>", "F01")

        Else
            ctlOrder.F01_Update(StrNull2Zero(lblID.Text), lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F01, Session("patientid"), Convert2Status(chkStatus.Checked), 0, Request.Cookies("username").Value, strP, txtPlanHospital.Text, GD, StrNull2Zero(txtScreenAge.Text))

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Service_Order", "บันทึกเพิ่มกิจกรรมการคัดกรองความเสี่ยงในกลุ่มภาวะโรคเมตาบอลิก(F01) :<<PatientID:" & Session("patientid") & ">>", "F01")
        End If

        Dim ServiceID As Integer = ctlOrder.ServiceOrder_GetID(lblLocationID.Text, FORM_TYPE_ID_F01, Session("patientid"), ServiceDate)


        'sFile = Path.GetExtension(FileUploaderAJAX1.PostedFile.FileName)
        'DocFile1 = "Refer" & "_" & ServiceDate & "_" & ConvertTimeToString(Now()) & "_" & ServiceID & sFile


        ctlOrder.F01_SaveMetabolicRisk(ServiceID, FORM_TYPE_ID_F01, Boolean2Decimal(chkFamDisease1.Checked), Boolean2Decimal(chkFamDisease2.Checked), Boolean2Decimal(chkFamDisease3.Checked), Boolean2Decimal(chkFamDisease4.Checked), Boolean2Decimal(chkFamDisease5.Checked), Boolean2Decimal(chkFamDisease6.Checked), StrNull2Zero(optSmoke.SelectedValue), StrNull2Zero(txtCGYear.Text), StrNull2Zero(txtCGNo.Text), StrNull2Zero(optCigarette.SelectedValue), StrNull2Zero(optAlcohol.SelectedValue), StrNull2Zero(txtAlcoholFQ.Text), StrNull2Zero(optExercise.SelectedValue), Boolean2Decimal(chkFood_Sweet.Checked), Boolean2Decimal(chkFood_Salt.Checked), Boolean2Decimal(chkFood_Fat.Checked), Boolean2Decimal(chkFood_Other.Checked), StrNull2Zero(optSleep.SelectedValue), StrNull2Zero(optRiskGender.SelectedValue), StrNull2Zero(optRiskAge.SelectedValue), StrNull2Zero(optRiskFam.SelectedValue), StrNull2Zero(optRiskBMI.SelectedValue), StrNull2Zero(optRiskShape.SelectedValue), StrNull2Zero(optRiskBloodAVG.SelectedValue), StrNull2Zero(txtFat.Text), StrNull2Zero(txtBlood.Text), StrNull2Zero(txtDiabetes.Text), StrNull2Zero(optRiskFat.SelectedValue), StrNull2Zero(optRiskBloodRisk.SelectedValue), StrNull2Zero(optRiskBlood.SelectedValue), StrNull2Zero(txtScreenAge.Text), StrNull2Zero(txtScreenShape.Text), StrNull2Zero(txtScreenWeight.Text), StrNull2Zero(txtScreenHeigh.Text), Str2Double(txtScreenBMI.Text), Str2Double(txtScreenBP1.Text), Str2Double(txtScreenBP1_2.Text), Str2Double(txtScreenBP2.Text), Str2Double(txtScreenBP2_2.Text), Str2Double(txtScreenBPAVG.Text), Str2Double(txtScreenBPAVG2.Text), Str2Double(txtScreenHR.Text), Str2Double(txtFBS.Text), Str2Double(txtRandom.Text), Str2Double(txtHourAfter.Text), 1, strP, txtPlanHospital.Text, Session("picname"), Request.Cookies("username").Value, Session("patientid"))

        If Request("t") = "edit" Or isAdd = False Then
            Response.Redirect("ResultPage.aspx")
        Else
            Response.Redirect("F02.aspx?seq=1&fid=" & ServiceID)
        End If

        ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย")

    End Sub

    Private Sub txtScreenAge_TextChanged(sender As Object, e As EventArgs) Handles txtScreenAge.TextChanged
        CalAges()
        txtScreenShape.Focus()
    End Sub

    Protected Sub txtServiceDate_TextChanged(sender As Object, e As EventArgs) Handles txtServiceDate.TextChanged
        CalculateAge()
    End Sub
End Class