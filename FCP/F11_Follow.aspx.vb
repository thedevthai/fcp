﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO



Public Class F11_Follow
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlLct As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlOrder As New OrderController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then

            If Request("acttype") = "view" Then
                cmdSave.Visible = False
                cmdClear.Visible = False
            Else
                cmdSave.Visible = True
                cmdClear.Visible = True
            End If

            txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)

            LoadFormData() 
        LoadPharmacist(Request.Cookies("LocationID").Value)

        End If

        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        'txtCardID.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")


    End Sub
  Private Sub LoadPharmacist(LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With
            End If
        End If
        dtP = Nothing
    End Sub

    Private Sub LoadLocationData(LocationID As String)
        Dim dtL As New DataTable
        dtL = ctlLct.Location_GetByID(LocationID)
        If dtL.Rows.Count > 0 Then
            With dtL.Rows(0)
                lblLocationID.Text = .Item("LocationID")
                lblLocationName.Text = .Item("LocationName")
                lblLocationProvince.Text = .Item("ProvinceName")
            End With
        End If
        dtL = Nothing
    End Sub

    Sub UploadFile(ByVal Fileupload As Object, sName As String)
        Dim FileFullName As String = Fileupload.PostedFile.FileName
        Dim FileNameInfo As String = Path.GetFileName(FileFullName)
        Dim filename As String = Path.GetFileName(Fileupload.PostedFile.FileName)
        Dim objfile As FileInfo = New FileInfo(Server.MapPath(tmpUpload))
        If FileNameInfo <> "" Then
            'Fileupload.PostedFile.SaveAs(objfile.DirectoryName & "\" & filename)
            'sFile = Path.GetExtension(FileFullName)
            Fileupload.PostedFile.SaveAs(objfile.DirectoryName & "\" & tmpUpload & "\" & sName)
        End If
        objfile = Nothing
    End Sub

    'Private Sub BindDayToDDL()
    '    Dim sD As String = ""
    '    For i = 1 To 31
    '        If i < 10 Then
    '            sD = "0" & i
    '        Else
    '            sD = i
    '        End If

    '        With ddlDay
    '            .Items.Add(i)
    '            .Items(i - 1).Value = sD
    '            .SelectedIndex = 0
    '        End With
    '    Next
    'End Sub
    'Private Sub BindMonthToDDL()
    '    Dim sD As String = ""

    '    For i = 1 To 12
    '        If i < 10 Then
    '            sD = "0" & i
    '        Else
    '            sD = i
    '        End If

    '        With ddlMonth
    '            .Items.Add(DisplayNumber2Month(i))
    '            .Items(i - 1).Value = sD
    '            .SelectedIndex = 0
    '        End With
    '    Next
    'End Sub

    'Private Sub BindYearToDDL()
    '    Dim ctlb As New ApplicationBaseClass
    '    Dim sDate As Date
    '    Dim y As Integer

    '    sDate = ctlb.GET_DATE_SERVER()

    '    If sDate.Year < 2300 Then
    '        y = (sDate.Year + 543)
    '    Else
    '        y = sDate.Year
    '    End If
    '    Dim i As Integer = 0
    '    Dim n As Integer = y - 17

    '    For i = 0 To 9
    '        With ddlYear
    '            .Items.Add(n)
    '            .Items(i).Value = n
    '            .SelectedIndex = 0
    '        End With
    '        n = n - 1
    '    Next
    'End Sub

    Private Sub LoadFormData()

        Dim pYear As Integer
        If Not Request("fid") Is Nothing Then
            dt = ctlOrder.GetOrder_ByID(StrNull2Zero(Request("fid")))
        Else
            If Not Request("yid") Is Nothing Then
                pYear = CInt(Request("yid"))
            Else
                pYear = Year(ctlLct.GET_DATE_SERVER)
                If pYear < 2500 Then
                    pYear = pYear + 543
                End If

            End If
            dt = ctlOrder.LocationOrder_ByYear(pYear, FORM_TYPE_ID_F11F, Request.Cookies("LocationID").Value, Session("patientid"))
        End If
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)


                If Request("t") = "new" Then
                    lblID.Text = ""
                    lblRefID.Text = String.Concat(.Item("itemID"))
                ElseIf Request("t") = "edit" Then
                    lblID.Text = String.Concat(.Item("itemID"))
                    lblRefID.Text = String.Concat(.Item("RefID"))
                ElseIf Request("t") Is Nothing Then
                    lblID.Text = ""
                    lblRefID.Text = String.Concat(.Item("itemID"))
                End If
                Session("patientid") = DBNull2Lng(.Item("PatientID"))
                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                txtTime.Text = String.Concat(.Item("ServiceTime"))
                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))
                chkClose.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("Status")))


                optChannel.SelectedValue = DBNull2Str(.Item("Follow_Channel"))
                optHospitalType.SelectedValue = DBNull2Zero(.Item("Hospital_Type"))
                txtHospitalName.Text = DBNull2Str(.Item("HospitalName"))
                txtCheckDate.Text = DBNull2Str(.Item("DateCheck"))
                txtRemark.Text = DBNull2Str(.Item("Remark"))

                optChkResult.SelectedValue = DBNull2Str(.Item("isNormal"))

                If DBNull2Zero(.Item("Status")) >= 3 Then
                    cmdSave.Visible = False
                    cmdClear.Visible = False
                Else
                    If Request.Cookies("LocationID").Value <> String.Concat(.Item("LocationID")) Then
                        LoadLocationData(String.Concat(.Item("LocationID")))
                        If Request("t") = "edit" Then
                            cmdSave.Visible = True
                            cmdClear.Visible = True
                        Else
                            cmdSave.Visible = False
                            cmdClear.Visible = False
                        End If
                    Else
                        LoadLocationData(Request.Cookies("LocationID").Value)
                    End If
                End If


                'Dim sStr(4) As String
                'sStr(0) = ""
                'sStr(1) = ""
                'sStr(2) = ""
                'sStr(3) = ""
                'sStr(4) = ""

                'If DBNull2Str(.Item("MedicinceDesc")) <> "" Then
                '    sStr = Split(DBNull2Str(.Item("MedicinceDesc")), "|")
                '    For k = 0 To sStr.Length - 1
                '        Select Case k
                '            Case 0
                '                txtMed1.Text = sStr(0)
                '            Case 1
                '                txtMed2.Text = sStr(1)
                '            Case 2
                '                txtMed3.Text = sStr(2)
                '            Case 3
                '                txtMed4.Text = sStr(3)
                '            Case 4
                '                txtMed5.Text = sStr(4)
                '        End Select
                '    Next


                'End If
                'ReDim sStr(4)
                'If DBNull2Str(.Item("DocFile")) <> "" Then
                '    sStr(0) = ""
                '    sStr(1) = ""
                '    sStr(2) = ""
                '    sStr(3) = ""
                '    sStr(4) = ""
                '    sStr = Split(DBNull2Str(.Item("DocFile")), "|")

                '    If sStr(0) <> "" Then
                '        If txtMed1.Text <> "" Then
                '            HyperLink1.NavigateUrl = "~/" & tmpUpload & "/" & sStr(0)
                '            HyperLink1.Text = sStr(0)
                '        End If

                '    End If

                '    If sStr(1) <> "" Then
                '        If txtMed2.Text <> "" Then
                '            HyperLink2.NavigateUrl = "~/" & tmpUpload & "/" & sStr(1)
                '            HyperLink2.Text = sStr(1)
                '        End If

                '    End If

                '    If sStr(2) <> "" Then
                '        If txtMed3.Text <> "" Then
                '            HyperLink3.NavigateUrl = "~/" & tmpUpload & "/" & sStr(2)
                '            HyperLink3.Text = sStr(2)
                '        End If

                '    End If

                '    If sStr(3) <> "" Then
                '        If txtMed4.Text <> "" Then
                '            HyperLink4.NavigateUrl = "~/" & tmpUpload & "/" & sStr(3)
                '            HyperLink4.Text = sStr(3)
                '        End If

                '    End If

                '    If sStr(4) <> "" Then
                '        If txtMed5.Text <> "" Then
                '            HyperLink5.NavigateUrl = "~/" & tmpUpload & "/" & sStr(4)
                '            HyperLink5.Text = sStr(4)
                '        End If

                '    End If


                'End If


                Dim sStr(4) As String
                sStr(0) = ""
                sStr(1) = ""
                sStr(2) = ""
                sStr(3) = ""
                sStr(4) = ""

                If DBNull2Str(.Item("MedicinceDesc")) <> "" Then
                    sStr = Split(DBNull2Str(.Item("MedicinceDesc")), "|")

                    For k = 0 To sStr.Length - 1
                        Select Case k
                            Case 0
                                txtMed1.Text = sStr(0)
                            Case 1
                                txtMed2.Text = sStr(1)
                            Case 2
                                txtMed3.Text = sStr(2)
                            Case 3
                                txtMed4.Text = sStr(3)
                            Case 4
                                txtMed5.Text = sStr(4)
                        End Select
                    Next

                End If
                ReDim sStr(4)
                If DBNull2Str(.Item("DocFile")) <> "" Then
                    sStr(0) = ""
                    sStr(1) = ""
                    sStr(2) = ""
                    sStr(3) = ""
                    sStr(4) = ""
                    sStr = Split(DBNull2Str(.Item("DocFile")), "|")

                    If chkFileExist(Server.MapPath("~/" & tmpUpload & "/" & sStr(0))) Then
                        HyperLink1.NavigateUrl = "~/" & tmpUpload & "/" & sStr(0)
                        HyperLink1.Text = sStr(0)
                    Else
                        HyperLink1.Visible = False
                        HyperLink1.Text = ""
                    End If

                    If chkFileExist(Server.MapPath("~/" & tmpUpload & "/" & sStr(1))) Then
                        HyperLink2.NavigateUrl = "~/" & tmpUpload & "/" & sStr(1)
                        HyperLink2.Text = sStr(1)
                    Else
                        HyperLink2.Visible = False
                        HyperLink2.Text = ""
                    End If

                    If chkFileExist(Server.MapPath("~/" & tmpUpload & "/" & sStr(2))) Then
                        HyperLink3.NavigateUrl = "~/" & tmpUpload & "/" & sStr(2)
                        HyperLink3.Text = sStr(2)
                    Else
                        HyperLink3.Visible = False
                        HyperLink3.Text = ""
                    End If

                    If chkFileExist(Server.MapPath("~/" & tmpUpload & "/" & sStr(3))) Then
                        HyperLink4.NavigateUrl = "~/" & tmpUpload & "/" & sStr(3)
                        HyperLink4.Text = sStr(3)
                    Else
                        HyperLink4.Visible = False
                        HyperLink4.Text = ""
                    End If

                    If chkFileExist(Server.MapPath("~/" & tmpUpload & "/" & sStr(4))) Then
                        HyperLink5.NavigateUrl = "~/" & tmpUpload & "/" & sStr(4)
                        HyperLink5.Text = sStr(4)
                    Else
                        HyperLink5.Visible = False
                        HyperLink5.Text = ""
                    End If

                End If


            End With
        Else
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing

    End Sub
   
    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        txtHospitalName.Text = ""
        txtCheckDate.Text = "'"
        txtRemark.Text = ""
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        'If Trim(txtName.Text) = "" Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนชื่อ-นามสกุลผู้รับบริการก่อน');", True)
        '    Exit Sub
        'End If
        'If Trim(txtMobile.Text) = "" Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนเบอร์มือถือผู้รับบริการก่อน หากไม่มีให้ใส่เบอร์บ้านในช่องเบอร์มือถือได้")
        '    Exit Sub
        'End If
        If StrNull2Zero(txtTime.Text) = 0 Then
            DisplayMessage(Me.Page, "กรุณาป้อนระยะเวลาในการให้บริการก่อน")
            'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนระยะเวลาในการให้บริการก่อน');", True)
            Exit Sub
        End If

        Dim objuser As New UserController
        Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))
        'Dim BirthDate As String = ConvertStrDate2DBString(txtBirthDate.Text)
        'Dim isEdu, isRev As Integer
        'isEdu = 0
        'isRev = 0
        'If chkWork.Items(0).Selected = True Then
        '    isEdu = 1
        'End If
        'If chkWork.Items(1).Selected = True Then
        '    isRev = 1
        'End If

        Dim ServiceID As Integer = lblRefID.Text

        Dim DocFile1, DocFile2, DocFile3, DocFile4, DocFile5, sFile, MedDesc As String

        DocFile1 = FORM_TYPE_ID_F11F & "_" & ServiceDate & "_" & ConvertTimeToString(Now()) & "_" & ServiceID
        DocFile2 = DocFile1
        DocFile3 = DocFile1
        DocFile4 = DocFile1
        DocFile5 = DocFile1

        If FileUpload1.HasFile Then
            DocFile1 = DocFile1 & "_1" & Path.GetExtension(FileUpload1.PostedFile.FileName)
        Else
            DocFile1 = HyperLink1.Text
        End If

        If FileUpload2.HasFile Then
            DocFile2 = DocFile2 & "_2" & Path.GetExtension(FileUpload2.PostedFile.FileName)
        Else
            DocFile2 = HyperLink2.Text
        End If

        If FileUpload3.HasFile Then
            DocFile3 = DocFile3 & "_3" & Path.GetExtension(FileUpload3.PostedFile.FileName)
        Else
            DocFile3 = HyperLink3.Text
        End If

        If FileUpload4.HasFile Then
            DocFile4 = DocFile4 & "_4" & Path.GetExtension(FileUpload4.PostedFile.FileName)
        Else
            DocFile4 = HyperLink4.Text
        End If

        If FileUpload5.HasFile Then
            DocFile5 = DocFile5 & "_5" & Path.GetExtension(FileUpload5.PostedFile.FileName)
        Else
            DocFile5 = HyperLink5.Text
        End If

        MedDesc = ""
        If Trim(txtMed1.Text) <> "" Then
            MedDesc = txtMed1.Text & "|"
        Else
            DocFile1 = ""
        End If

        If Trim(txtMed2.Text) <> "" Then
            MedDesc &= txtMed2.Text & "|"
        Else
            DocFile2 = ""
        End If

        If Trim(txtMed3.Text) <> "" Then
            MedDesc &= txtMed3.Text & "|"
        Else
            DocFile3 = ""
        End If

        If Trim(txtMed4.Text) <> "" Then
            MedDesc &= txtMed4.Text & "|"
        Else
            DocFile4 = ""
        End If

        If Trim(txtMed5.Text) <> "" Then
            MedDesc &= txtMed5.Text
        Else
            DocFile5 = ""
        End If

        'If FileUpload1.HasFile Then
        '    sFile = Path.GetExtension(FileUpload1.PostedFile.FileName)
        'Else
        '    sFile = ".jpg"
        'End If

        'DocFile1 = DocFile1 & "_1" & sFile

        'If FileUpload2.HasFile Then
        '    sFile = Path.GetExtension(FileUpload2.PostedFile.FileName)
        'Else
        '    sFile = ".jpg"
        'End If

        'DocFile2 = DocFile2 & "_2" & sFile

        'If FileUpload3.HasFile Then
        '    sFile = Path.GetExtension(FileUpload3.PostedFile.FileName)
        'Else
        '    sFile = ".jpg"
        'End If

        'DocFile3 = DocFile3 & "_3" & sFile

        'If FileUpload4.HasFile Then
        '    sFile = Path.GetExtension(FileUpload4.PostedFile.FileName)
        'Else
        '    sFile = ".jpg"
        'End If

        'DocFile4 = DocFile4 & "_4" & sFile

        'If FileUpload5.HasFile Then
        '    sFile = Path.GetExtension(FileUpload5.PostedFile.FileName)
        'Else
        '    sFile = ".jpg"
        'End If

        'DocFile5 = DocFile5 & "_5" & sFile

        'MedDesc = ""
        'sFile = ""
        'If Trim(txtMed1.Text) <> "" Then
        '    MedDesc = txtMed1.Text & "|"
        '    sFile &= DocFile1 & "|"
        'Else
        '    DocFile1 = ""
        'End If
        'If Trim(txtMed2.Text) <> "" Then
        '    MedDesc &= txtMed2.Text & "|"
        '    sFile &= DocFile2 & "|"
        'Else
        '    DocFile2 = ""
        'End If
        'If Trim(txtMed3.Text) <> "" Then
        '    MedDesc &= txtMed3.Text & "|"
        '    sFile &= DocFile3 & "|"
        'Else
        '    DocFile3 = ""
        'End If
        'If Trim(txtMed4.Text) <> "" Then
        '    MedDesc &= txtMed4.Text & "|"
        '    sFile &= DocFile4 & "|"
        'Else
        '    DocFile4 = ""
        'End If

        'If Trim(txtMed5.Text) <> "" Then
        '    MedDesc &= txtMed5.Text
        '    sFile &= DocFile5
        'Else
        '    DocFile5 = ""
        'End If

        'sFile = DocFile1 & "|" & DocFile2 & "|" & DocFile3 & "|" & DocFile4 & "|" & DocFile5

        If lblID.Text = "" Then

            ctlOrder.F11_Follow_Add(lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F11F, Convert2Status(chkClose.Checked), 1, 1, txtHospitalName.Text, txtCheckDate.Text, StrNull2Zero(optChkResult.SelectedValue), txtRemark.Text, MedDesc, sFile, Request.Cookies("username").Value, StrNull2Zero(lblRefID.Text), CLng(ConvertDate2DBString(ctlLct.GET_DATE_SERVER)), optChannel.SelectedValue, optHospitalType.SelectedValue, Session("patientid"), optIsClaim.SelectedValue, Session("sex"), Session("age"))

            ctlOrder.F11_UpdateFollowStatus(StrNull2Zero(lblRefID.Text), 1, Request.Cookies("username").Value)

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Service_Order", "บันทึกเพิ่มผลการติดตามการตรวจคัดกรองมะเร็งปากมดลูกโดยวิธี Pap Smear (F11-F):" & Session("patientname"), "F11-F")
        Else
            ctlOrder.F11_Follow_Update(StrNull2Zero(lblID.Text), lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F11F, Convert2Status(chkClose.Checked), 1, 1, txtHospitalName.Text, txtCheckDate.Text, StrNull2Zero(optChkResult.SelectedValue), txtRemark.Text, MedDesc, sFile, Request.Cookies("username").Value, StrNull2Zero(lblRefID.Text), optChannel.SelectedValue, optHospitalType.SelectedValue, Session("patientid"), optIsClaim.SelectedValue)

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Service_Order", "บันทึกแก้ไขผลการติดตามการตรวจคัดกรองมะเร็งปากมดลูกโดยวิธี Pap Smear (F11-F) :" & Session("patientname"), "F11-F")
        End If

        UploadFile(FileUpload1, DocFile1)
        UploadFile(FileUpload2, DocFile2)
        UploadFile(FileUpload3, DocFile3)
        UploadFile(FileUpload4, DocFile4)
        UploadFile(FileUpload5, DocFile5)


        ctlOrder.F01_UpdateCloseStatus(StrNull2Zero(lblRefID.Text), Convert2Status(chkClose.Checked), Request.Cookies("username").Value)
        Response.Redirect("ResultPage.aspx?p=F11")

        '  ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย")
    End Sub

End Class