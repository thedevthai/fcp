﻿
<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="vmiMenu.aspx.vb" Inherits=".vmiMenu" %>
<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
     <link rel="stylesheet" type="text/css" href="css/Tabs.css"> 
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>Inventory Module
        <small>VMI</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
    <table width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
   
  <tr>
    <td align="left">
              <dx:ASPxPageControl ID="tabPageSearch" runat="server" ActiveTabIndex="2"  EnableTheming="True" Theme="Material" Width="100%" BackColor="White" EnableTabScrolling="True">
                  <TabPages>
                      <dx:TabPage Name="pStore" Text="Main Store">
                          <ContentCollection>
                              <dx:ContentControl runat="server">

                               <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                <tr>
                                  <td width="50%"><asp:HyperLink ID="HyperLink1" runat="server" CssClass="button_rpt_Menu_Blue"   NavigateUrl="vmiLocation.aspx?ActionType=vmi&ItemType=sto" Width="300px"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;คลังยา / Store</asp:HyperLink></td>
                                  <td>
                                    <asp:HyperLink ID="HyperLink2" runat="server" CssClass="button_rpt_Menu_Blue" NavigateUrl="vmiStockMain.aspx?ActionType=vmi&ItemType=stm" Width="350px"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;สินค้าคงคลัง / Main Stock</asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                  <td>
                                      <asp:HyperLink ID="HyperLink3" runat="server" CssClass="button_rpt_Menu_Blue" NavigateUrl="vmiStoreReceive.aspx?ActionType=vmi&ItemType=rcv" Width="300px"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;บันทึกสินค้าคงคลัง / Inventory Store</asp:HyperLink>
                                    </td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                              </table>

                              </dx:ContentControl>
                             
                          </ContentCollection>
                      </dx:TabPage>
                      <dx:TabPage Name="pStock" Text="Stock">
                          <ContentCollection>
                              <dx:ContentControl runat="server">
                                  
                               <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                <tr>
                                  <td width="50%"><asp:HyperLink ID="HyperLink4" runat="server" CssClass="button_rpt_Menu_Blue"   NavigateUrl="vmiStockAdmin.aspx?ActionType=vmi&ItemType=stk" Width="300px"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;จัดการสต๊อกร้านยา / Location Stock</asp:HyperLink></td>
                                  <td>
                                    <asp:HyperLink ID="HyperLink5" runat="server" CssClass="button_rpt_Menu_Blue" NavigateUrl="vmiLocationAssociation.aspx?ActionType=vmi&ItemType=sas" Width="350px"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;กำหนดคลังจ่ายยาแต่ละร้าน / Location Association</asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                  <td>
                                      &nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                              </table>

                              </dx:ContentControl>
                          </ContentCollection>
                      </dx:TabPage>
                      <dx:TabPage Text="Inventory">
                          <ContentCollection>
                              <dx:ContentControl runat="server">
                                  
                               <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                <tr>
                                  <td width="50%"><asp:HyperLink ID="HyperLink7" runat="server" CssClass="button_rpt_Menu_Blue"   NavigateUrl="vmiStockRefill.aspx?ActionType=vmi&ItemType=ref" Width="300px"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;เติมยาให้ร้าน / Replenishment (Auto)</asp:HyperLink></td>
                                  <td>
                                      <asp:HyperLink ID="HyperLink9" runat="server" CssClass="button_rpt_Menu_Blue" NavigateUrl="vmiStockReceiveStatus.aspx?ActionType=vmi&amp;ItemType=rcv" Width="300px"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;สถานะการเติมยา / Receive Status</asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                  <td>
                                      <asp:HyperLink ID="HyperLink12" runat="server" CssClass="button_rpt_Menu_Blue" NavigateUrl="vmiStockRefillManual.aspx?ActionType=vmi&amp;ItemType=ref" Width="300px"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;เติมยาให้ร้าน / Replenishment (Manually)</asp:HyperLink>
                                    </td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                              </table>

                              </dx:ContentControl>
                          </ContentCollection>
                      </dx:TabPage>
                      <dx:TabPage Text="Reports">
                          <ContentCollection>
                              <dx:ContentControl runat="server">
                                  
                               <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                <tr>
                                  <td width="50%"><asp:HyperLink ID="HyperLink10" runat="server" CssClass="button_rpt_Menu_Blue"   NavigateUrl="ReportVMIConditionByProvince.aspx?ActionType=vmi&ItemType=vmi2" Width="300px"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;รายงานสินค้าคงเหลือ / Stock Balance</asp:HyperLink></td>
                                  <td>
                                    <asp:HyperLink ID="HyperLink11" runat="server" CssClass="button_rpt_Menu_Blue" NavigateUrl="ReportConditionByDate.aspx?ActionType=vmi&ItemType=use" Width="350px"><img src="images/star20.png" width="20" height="20"  align="absmiddle"  />&nbsp;รายงานการจ่ายยา / Inventory Report</asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                  <td>
                                      &nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                              </table>

                              </dx:ContentControl>
                          </ContentCollection>
                      </dx:TabPage>
                      <dx:TabPage Text="Setting">
                          <ContentCollection>
                              <dx:ContentControl runat="server">
                                  
                               <table width="100%" border="0" cellspacing="2" cellpadding="2">
                                <tr>
                                  <td width="50%">&nbsp;</td>
                                  <td>
                                      &nbsp;</td>
                                </tr>
                                <tr>
                                  <td>
                                      &nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                              </table>

                              </dx:ContentControl>
                          </ContentCollection>
                      </dx:TabPage>
                  </TabPages>
                  <Paddings Padding="0px" />
                  <Border BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" />
              </dx:ASPxPageControl>
            </td>
  </tr>
  <tr>
    <td align="left">&nbsp;</td>
  </tr>
  <tr>
    <td align="left">&nbsp;</td>
  </tr>
</table>
    </section>
</asp:Content>
