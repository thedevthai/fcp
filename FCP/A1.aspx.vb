﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization

Public Class A1
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlLct As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlOrder As New SmokingController
    Dim ctlSK As New StockController
    Dim ServiceDate As Long
    Private _dateDiff As Object

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Response.Redirect("CuttomErrorPage.aspx")

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Session("patientid") Is Nothing Then
            If Request("pid") Is Nothing Then
                Response.Redirect("PatientSearch.aspx")
            Else
                Session("patientid") = Request("pid")
            End If
        End If


        If Not IsPostBack Then
            'txtSmokeQTY.Visible = False
            'lblCGday.Visible = False
            'txtSmokeDay.Visible = False
            'lblCgType.Visible = False
            'optCigarette.Visible = False
            'lblProblem.Visible = False
            'optNextCause.Visible = False
            'txtCause.Visible = False
            optStopUOM.Visible = False

            CallQ11()

            If Request("acttype") = "view" Then
                cmdSave.Visible = False
            Else
                cmdSave.Visible = True
            End If


            LoadMedicine()


            LoadPharmacist(Request.Cookies("LocationID").Value)
            txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)
            txtYear.Text = Right(txtServiceDate.Text, 4)

            If StrNull2Zero(txtYear.Text) < 2500 Then
                txtYear.Text = StrNull2Zero(txtYear.Text) + 543
            Else
                txtYear.Text = StrNull2Zero(txtYear.Text)
            End If

            LoadFormData()
            'LoadBalanceStock()
            lblBalance.Visible = False
            lblBalanceLabel.Visible = False
        End If


        'dtpServiceDate.DisplayFormatString = CultureInfo.CreateSpecificCulture("th-TH").DateTimeFormat.ShortDatePattern
        'xDateNextService.DisplayFormatString = CultureInfo.CreateSpecificCulture("th-TH").DateTimeFormat.ShortDatePattern
        'xDateStopPlan1.DisplayFormatString = CultureInfo.CreateSpecificCulture("th-TH").DateTimeFormat.ShortDatePattern
        'xDateStopPlan2.DisplayFormatString = CultureInfo.CreateSpecificCulture("th-TH").DateTimeFormat.ShortDatePattern

        'dtpServiceDate.EditFormatString = CultureInfo.CreateSpecificCulture("th-TH").DateTimeFormat.ShortDatePattern
        'xDateNextService.EditFormatString = CultureInfo.CreateSpecificCulture("th-TH").DateTimeFormat.ShortDatePattern
        'xDateStopPlan1.EditFormatString = CultureInfo.CreateSpecificCulture("th-TH").DateTimeFormat.ShortDatePattern
        'xDateStopPlan2.EditFormatString = CultureInfo.CreateSpecificCulture("th-TH").DateTimeFormat.ShortDatePattern


        'txtScreenBP1.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")
        'txtScreenBP2.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")
        'txtScreenBP3.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")

        'If HiddenField1.Value <> "" Then
        '    CalBPAVG()
        'End If
        'txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        ''txtCardID.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")


    End Sub

    Private Sub LoadMedicine()
        Dim dtM As New DataTable
        dtM = ctlOrder.Medicine_Get
        If dtM.Rows.Count > 0 Then
            ddlMed.DataSource = dtM
            ddlMed.DataTextField = "itemName"
            ddlMed.DataValueField = "itemID"
            ddlMed.DataBind()
        End If
        dtM = Nothing
    End Sub

    Private Sub LoadPharmacist(LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With
            End If
        End If
        dtP = Nothing
    End Sub
    Private Sub LoadLocationData(LocationID As String)
        dt = ctlLct.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblLocationID.Text = .Item("LocationID")
                lblLocationName.Text = .Item("LocationName")
                lblLocationProvince.Text = .Item("ProvinceName")
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadFormData()
        Dim pYear As Integer

        If Not Request("fid") Is Nothing Then
            dt = ctlOrder.CPA_SmokingOrder_ByID(StrNull2Zero(Request("fid")))
        Else
            If Not Request("yid") Is Nothing Then
                pYear = CInt(Request("yid"))
            Else
                pYear = Year(ctlLct.GET_DATE_SERVER)
                If pYear < 2500 Then
                    pYear = pYear + 543
                End If

            End If
            dt = ctlOrder.CPA_Smoking_OrderByYear(pYear, FORM_TYPE_ID_A1, Session("patientid"))
        End If

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                lblID.Text = .Item("ServiceUID")
                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                Session("patientid") = DBNull2Lng(.Item("PatientID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                txtTime.Text = String.Concat(.Item("ServiceTime"))
                txtYear.Text = DBNull2Str(.Item("BYear"))

                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))


                If DBNull2Str(.Item("PTYPE")) <> "" Then
                    optPatientType.SelectedValue = DBNull2Str(.Item("PTYPE"))
                End If

                '-------------------
                If DBNull2Str(.Item("PFROM")) <> "" Then
                    optPatientFrom.SelectedValue = DBNull2Str(.Item("PFROM"))
                    txtFromRemark.Text = DBNull2Str(.Item("PFROM_DESC"))
                End If

                optService.SelectedValue = DBNull2Str(.Item("MTMService"))
                txtServiceRemark.Text = DBNull2Str(.Item("ServiceRemark"))

                If DBNull2Str(.Item("MTMService")) = "2" Then
                    optTelepharm.SelectedValue = DBNull2Str(.Item("ServiceRef"))
                Else
                    optTelepharm.ClearSelection()
                End If

                optTelepharmacyMethod.SelectedValue = DBNull2Str(.Item("TelepharmacyMethod"))
                txtRecordMethod.Text = DBNull2Str(.Item("RecordMethod"))
                txtRecordLocation.Text = DBNull2Str(.Item("RecordLocation"))

                txtTelepharmacyRemark.Text = DBNull2Str(.Item("TelepharmacyRemark"))

                '---------------------------------
                txtSmokeQTY.Text = String.Concat(.Item("SmokeQTY"))
                txtNicotinRate.Text = String.Concat(.Item("Nicotine"))
                txtOften.Text = String.Concat(.Item("Often"))
                txtMinutePerTime.Text = String.Concat(.Item("SmokeTime"))


                'txtCareer.Text = DBNull2Str(.Item("PatientCareer"))

                'Dim sDisease() As String = Split(DBNull2Str(.Item("Diseases")), "|")
                'chkDisease1.Checked = False
                'chkDisease2.Checked = False
                'chkDisease3.Checked = False
                'chkDisease4.Checked = False
                'chkDisease5.Checked = False
                'chkDisease6.Checked = False
                'chkDisease7.Checked = False
                'chkDisease8.Checked = False
                'chkDisease9.Checked = False
                'chkDiseaseOther.Checked = False
                'txtDiseaseOther.Text = DBNull2Str(.Item("DiseasesOther"))
                'For i = 0 To sDisease.Length - 1
                '    Select Case sDisease(i)
                '        Case "D01"
                '            chkDisease1.Checked = True
                '        Case "D02"
                '            chkDisease2.Checked = True
                '        Case "D03"
                '            chkDisease3.Checked = True
                '        Case "D04"
                '            chkDisease4.Checked = True
                '        Case "D05"
                '            chkDisease5.Checked = True
                '        Case "D06"
                '            chkDisease6.Checked = True
                '        Case "D07"
                '            chkDisease7.Checked = True
                '        Case "D08"
                '            chkDisease8.Checked = True
                '        Case "D09"
                '            chkDisease9.Checked = True
                '        Case "D10"
                '            chkDisease10.Checked = True
                '        Case "D11"
                '            chkDisease11.Checked = True
                '        Case "D12"
                '            chkDisease12.Checked = True
                '        Case "D99"
                '            chkDiseaseOther.Checked = True
                '    End Select
                'Next

                'txtDrugsUsed.Text = String.Concat(.Item("DrugUsage"))
                optAlcohol.SelectedValue = String.Concat(.Item("Alcohol"))
                txtSmokeDay.Text = String.Concat(.Item("SmokeDay"))
                optSmokeUOM.SelectedValue = String.Concat(.Item("SmokeUOM"))
                optSmokeFQ.SelectedValue = String.Concat(.Item("SmokeFQ"))


                If optSmokeFQ.SelectedValue <> "3" Then
                    txtSmokeQTY.Text = String.Concat(.Item("SmokeQTY"))
                    lblCGday.Visible = True
                    optStopUOM.Visible = False
                Else
                    txtSmokeQTY.Text = String.Concat(.Item("StopDay"))
                    optStopUOM.Visible = True
                    lblCGday.Visible = False
                    optStopUOM.SelectedValue = String.Concat(.Item("StopUOM"))
                End If

                optCigarette.SelectedValue = String.Concat(.Item("CGTYPE"))

                optIntimate.SelectedValue = String.Concat(.Item("IsIntimate"))
                txtCloseupPerson.Text = String.Concat(.Item("IntimateFriend"))

                optQ1A.SelectedValue = String.Concat(.Item("Q1"))
                txtQ1B.Text = String.Concat(.Item("Q1TIME"))
                txtQ1C.Text = String.Concat(.Item("Q1DAY"))
                optQ1D.SelectedValue = String.Concat(.Item("Q1UOM"))


                chkQ2.ClearSelection()
                Dim sQ2() As String = Split(DBNull2Str(.Item("Q2")), "|")
                For i = 0 To sQ2.Length - 1
                    Select Case sQ2(i)
                        Case "Q21"
                            chkQ2.Items(0).Selected = True
                        Case "Q22"
                            chkQ2.Items(1).Selected = True
                        Case "Q23"
                            chkQ2.Items(2).Selected = True
                        Case "Q24"
                            chkQ2.Items(3).Selected = True
                        Case "Q2OT"
                            chkQ2.Items(4).Selected = True
                    End Select
                Next
                chkQ2Med.ClearSelection()
                Dim sQ2Med() As String = Split(DBNull2Str(.Item("Q2MED")), "|")
                For i = 0 To sQ2Med.Length - 1
                    Select Case sQ2Med(i)
                        Case "M1"
                            chkQ2Med.Items(0).Selected = True
                        Case "M2"
                            chkQ2Med.Items(1).Selected = True
                        Case "M3"
                            chkQ2Med.Items(2).Selected = True
                        Case "M4"
                            chkQ2Med.Items(3).Selected = True
                        Case "M5"
                            chkQ2Med.Items(4).Selected = True
                        Case "M6"
                            chkQ2Med.Items(5).Selected = True
                        Case "M7"
                            chkQ2Med.Items(6).Selected = True

                    End Select
                Next
                txtQ2Other.Text = DBNull2Str(.Item("Q2Other"))

                'chkQ3.ClearSelection()
                'Dim sQ3() As String = Split(DBNull2Str(.Item("Q3")), "|")
                'For i = 0 To sQ3.Length - 1
                '    Select Case sQ3(i)
                '        Case "1"
                '            chkQ3.Items(0).Selected = True
                '        Case "2"
                '            chkQ3.Items(1).Selected = True
                '        Case "3"
                '            chkQ3.Items(2).Selected = True
                '        Case "4"
                '            chkQ3.Items(3).Selected = True
                '        Case "5"
                '            chkQ3.Items(4).Selected = True
                '        Case "0"
                '            chkQ3.Items(5).Selected = True
                '    End Select
                'Next
                'txtQ3.Text = DBNull2Str(.Item("Q3Other"))

                'optQ4.SelectedValue = DBNull2Str(.Item("Q4"))
                optQ5.SelectedValue = DBNull2Str(.Item("Q5"))
                optQ6.SelectedValue = DBNull2Str(.Item("Q6"))
                optQ7.SelectedValue = DBNull2Str(.Item("Q7"))
                optQ8.SelectedValue = DBNull2Str(.Item("Q8"))
                optQ9.SelectedValue = DBNull2Str(.Item("Q9"))
                optQ10.SelectedValue = DBNull2Str(.Item("Q10"))
                'optQ11.SelectedValue = DBNull2Str(.Item("Q11"))
                txtFTND.Text = DBNull2Str(.Item("FTND"))
                optQ12.SelectedValue = DBNull2Str(.Item("Q12"))
                optQ13.SelectedValue = DBNull2Str(.Item("Q13"))

                chkQ14.ClearSelection()
                Dim sQ14() As String = Split(DBNull2Str(.Item("Q14")), "|")
                For i = 0 To sQ14.Length - 1
                    Select Case sQ14(i)
                        Case "1"
                            chkQ14.Items(0).Selected = True
                        Case "2"
                            chkQ14.Items(1).Selected = True
                        Case "3"
                            chkQ14.Items(2).Selected = True
                        Case "4"
                            chkQ14.Items(3).Selected = True
                        Case "5"
                            chkQ14.Items(4).Selected = True
                    End Select
                Next
                txtQ14.Text = DBNull2Str(.Item("Q14Other"))

                txtWeight.Text = DBNull2Str(.Item("PWeight"))
                txtHeigh.Text = DBNull2Str(.Item("PHeigh"))
                txtBP.Text = DBNull2Str(.Item("BP"))
                txtPEFR_Value.Text = DBNull2Str(.Item("PEFR_Value"))
                txtPEFR_Rate.Text = DBNull2Str(.Item("PEFR_Rate"))
                txtCO_Value.Text = DBNull2Str(.Item("CO_Value"))

                'Dim pstr() As String
                'pstr = Split(String.Concat(.Item("ServicePlan")), "|")

                'For i = 0 To pstr.Length - 1
                '    Select Case pstr(i)
                '        Case "1"
                '            chkDisease1.Items(0).Selected = True
                '        Case "2"
                '            chkPlan.Items(1).Selected = True
                '        Case "3"
                '            chkPlan.Items(2).Selected = True
                '    End Select
                'Next

                'optNextStep.SelectedValue = DBNull2Str(.Item("ServicePlan"))
                'optNextCause.SelectedValue = DBNull2Str(.Item("ServicePlanProblem"))
                'txtCause.Text = String.Concat(.Item("ServicePlanRemark"))
                'ddlTimeService.SelectedValue = DBNull2Str(.Item("ContactTime"))


                'xDateStopPlan1.Text = DBNull2Str(.Item("ServicePlanDate"))

                'optStopPlane2.SelectedValue = DBNull2Str(.Item("Guidelines"))
                'txtCGTargetRateBegin.Text = DBNull2Str(.Item("GuidelinesCGFrom"))
                'txtCGTargetRateEnd.Text = DBNull2Str(.Item("GuidelinesCGTo"))
                'txtTargetDay.Text = DBNull2Str(.Item("GuidelinesDay"))
                'xDateStopPlan2.Text = DBNull2Str(.Item("GuidelinesDate"))
                'xDateNextService.Text = DBNull2Str(.Item("ServiceNextDate"))

                'If DBNull2Str(.Item("ServicePlanDate")) <> "" Then
                '    xDateStopPlan1.Text = DisplayShortDateTH(.Item("ServicePlanDate"))
                'Else
                '    xDateStopPlan1.Text = ""
                'End If
                'If DBNull2Str(.Item("GuidelinesDate")) <> "" Then
                '    xDateStopPlan2.Text = DisplayShortDateTH(.Item("GuidelinesDate"))
                'Else
                '    xDateStopPlan2.Text = ""
                'End If

                optStopPlane1.SelectedValue = DBNull2Str(.Item("ServicePlan"))
                If DBNull2Str(.Item("QuitPlanDate")) <> "" Then
                    QuitPlanDate.Text = DisplayShortDateTH(.Item("QuitPlanDate"))
                Else
                    QuitPlanDate.Text = ""
                End If

                If DBNull2Str(.Item("ServiceNextDate")) <> "" Then
                    xDateNextService.Text = DisplayShortDateTH(.Item("ServiceNextDate"))
                Else
                    xDateNextService.Text = ""
                End If

                chkStatus.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("StatusFlag")))
                LoadMedOrderItem(lblID.Text)


                If DBNull2Zero(.Item("StatusFlag")) >= 3 Then  'Year(ctlLct.GET_DATE_SERVER.Date) <> DBNull2Zero(.Item("BYear")) 
                    cmdSave.Visible = False
                Else
                    If Request.Cookies("LocationID").Value <> String.Concat(.Item("LocationID")) Then
                        LoadLocationData(String.Concat(.Item("LocationID")))
                        If Request("t") = "edit" Then
                            cmdSave.Visible = True
                        Else
                            cmdSave.Visible = False
                        End If
                    Else
                        LoadLocationData(Request.Cookies("LocationID").Value)
                    End If
                End If

            End With
        Else
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing

    End Sub

    'Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
    '    isAdd = True
    '    lblID.Text = ""
    '    txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)
    '    'txtTime.Text = ""
    '    ddlPerson.SelectedIndex = 0
    '    txtWeight.Text = ""
    '    txtHeigh.Text = ""
    '    txtCause.Text = ""
    '    txtSmokeQTY.Text = ""
    '    txtSmokeDay.Text = ""
    '    txtCloseupPerson.Text = ""
    '    txtDiseaseOther.Text = ""
    '    txtPEFR_Rate.Text = ""
    '    txtPEFR_Value.Text = ""
    '    txtQ1B.Text = ""
    '    txtQ1C.Text = ""
    '    txtQ2Other.Text = ""
    '    txtQ3.Text = ""
    '    txtQ10.Text = ""
    'End Sub

    Protected Sub txtHeigh_TextChanged(sender As Object, e As EventArgs) Handles txtHeigh.TextChanged
        CalPEFR_Rate()
    End Sub
    Private Sub CalPEFR_Rate()
        Dim PEF, PEFR_Standard As Double

        Dim A As Integer = StrNull2Zero(Session("age"))
        Dim H As Double = StrNull2Zero(txtHeigh.Text)
        If H > 0 Then
            If StrNull2Zero(Session("age")) >= 15 Then
                If Session("sex") = "M" Then
                    PEF = (((((-16.859 + (0.307 * A)) + (0.141 * H)) - (0.0018 * (A * A))) - (0.001 * A * H)) * 60)
                Else
                    PEF = ((((((-31.355 + (0.162 * A)) + (0.391 * H)) - (0.00084 * (A * A))) - (0.00099 * (H * H))) - (0.00072 * A * H)) * 60)
                End If
            Else
                PEF = (StrNull2Zero(txtHeigh.Text) * 5) - 400
            End If

            PEFR_Standard = Math.Round(PEF)
            If StrNull2Zero(txtPEFR_Value.Text) <> 0 Then
                txtPEFR_Rate.Text = Math.Round((StrNull2Zero(txtPEFR_Value.Text) / PEFR_Standard) * 100)
            Else
                txtPEFR_Rate.Text = "0"
            End If

            'If Not IsNumeric(txtPEFR_Rate.Text) Then
            '    txtPEFR_Rate.Text = "0"
            'End If
        End If
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        'If Trim(txtFName.Text) = "" Or Trim(txtLName.Text) = "" Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาป้อนชื่อ-นามสกุลผู้รับบริการก่อน');", True)
        '    Exit Sub
        'End If
        'If Trim(txtMobile.Text) = "" Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาป้อนเบอร์มือถือผู้รับบริการก่อน หากไม่มีให้ใส่เบอร์บ้านในช่องเบอร์มือถือได้")
        '    Exit Sub
        'End If
        If Trim(txtServiceDate.Text) = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุวันที่ให้บริการ');", True)
            Exit Sub
        End If

        If Trim(txtTime.Text) = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุระยะเวลาที่ให้บริการ');", True)
            Exit Sub
        End If

        If Trim(txtTime.Text) <> "" Then
            If IsNumeric(txtTime.Text) = False Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาตรวจสอบระยะเวลาในการให้บริการ ควรป้อนเฉพาะตัวเลขเท่านั้น หากไม่มีข้อมูลให้ใส่ 0');", True)
                Exit Sub
            End If
        End If

        If optPatientType.SelectedIndex = -1 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกผู้รับบริการ รายใหม่/รายเก่า ก่อน');", True)
            Exit Sub
        End If
        If optPatientFrom.SelectedIndex = -1 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทผู้รับบริการก่อน');", True)
            Exit Sub
        End If

        If optPatientFrom.SelectedIndex = -1 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทแหล่งที่มาก่อน');", True)
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทแหล่งที่มาก่อน ")
            Exit Sub
        End If

        If optPatientFrom.SelectedValue = "9" And txtFromRemark.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุประเภทแหล่งที่มาก่อน');", True)
            Exit Sub
        End If


        If optService.SelectedIndex = -1 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกการให้บริการก่อน');", True)
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกการให้บริการมาก่อน ")
            Exit Sub
        End If

        If optService.SelectedIndex = 1 Then
            If optTelepharm.SelectedIndex = -1 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทการให้บริการ Telepharmacy ก่อน ');", True)
                Exit Sub
            Else
                If optTelepharm.SelectedValue <> "1" Then
                    If txtTelepharmacyRemark.Text.Trim() = "" Then
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุชื่อ รพ. หรือ ชื่อโครงการอื่นๆ กรณี Telepharmacy ก่อน');", True)
                        Exit Sub
                    End If
                End If
            End If
        End If


        Dim objuser As New UserController
        Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))

        ''Dim BirthDate As String = ConvertStrDate2DBString(txtBirthDate.Text)
        Dim FTND, sQ11 As Integer
        Dim strQ2, sQ2Med, sDisease, sQ3, sQ12, sQ13, sQ14 As String
        strQ2 = ""
        sQ2Med = ""
        sQ3 = ""
        sQ12 = ""
        sQ13 = ""
        sQ14 = ""
        sDisease = ""

        'If chkDisease1.Checked Then
        '    sDisease = "D01"
        'End If
        'If chkDisease2.Checked Then
        '    sDisease &= "|D02"
        'End If
        'If chkDisease3.Checked Then
        '    sDisease &= "|D03"
        'End If
        'If chkDisease4.Checked Then
        '    sDisease &= "|D04"
        'End If
        'If chkDisease5.Checked Then
        '    sDisease &= "|D05"
        'End If
        'If chkDisease6.Checked Then
        '    sDisease &= "|D06"
        'End If
        'If chkDisease7.Checked Then
        '    sDisease &= "|D07"
        'End If
        'If chkDisease8.Checked Then
        '    sDisease &= "|D08"
        'End If
        'If chkDisease9.Checked Then
        '    sDisease &= "|D09"
        'End If
        'If chkDisease10.Checked Then
        '    sDisease &= "|D10"
        'End If
        'If chkDisease11.Checked Then
        '    sDisease &= "|D11"
        'End If

        'If chkDisease12.Checked Then
        '    sDisease &= "|D12"
        'End If

        'If chkDiseaseOther.Checked Then
        '    sDisease &= "|D99"
        'End If

        If chkQ2.Items(0).Selected Then
            strQ2 = "Q21"
        End If
        If chkQ2.Items(1).Selected Then
            strQ2 &= "|Q22"
        End If
        If chkQ2.Items(2).Selected Then
            strQ2 &= "|Q23"
        End If

        If chkQ2Med.Items(0).Selected Then
            sQ2Med = "M1"
        End If
        If chkQ2Med.Items(1).Selected Then
            sQ2Med &= "|M2"
        End If
        If chkQ2Med.Items(2).Selected Then
            sQ2Med &= "|M3"
        End If
        If chkQ2Med.Items(3).Selected Then
            sQ2Med &= "|M4"
        End If
        If chkQ2Med.Items(4).Selected Then
            sQ2Med &= "|M5"
        End If
        If chkQ2Med.Items(5).Selected Then
            sQ2Med &= "|M6"
        End If
        If chkQ2Med.Items(6).Selected Then
            sQ2Med &= "|M7"
        End If

        'If chkQ3.Items(0).Selected Then
        '    sQ3 = "1"
        'End If
        'If chkQ3.Items(1).Selected Then
        '    sQ3 &= "|2"
        'End If
        'If chkQ3.Items(2).Selected Then
        '    sQ3 &= "|3"
        'End If
        'If chkQ3.Items(3).Selected Then
        '    sQ3 &= "|4"
        'End If
        'If chkQ3.Items(4).Selected Then
        '    sQ3 &= "|5"
        'End If
        'If chkQ3.Items(5).Selected Then
        '    sQ3 &= "|0"
        'End If

        'If chkQ12.Items(0).Selected Then
        '    sQ12 = "1"
        'End If
        'If chkQ12.Items(1).Selected Then
        '    sQ12 &= "|2"
        'End If
        'If chkQ12.Items(2).Selected Then
        '    sQ12 &= "|3"
        'End If

        'If chkQ13.Items(0).Selected Then
        '    sQ13 = "1"
        'End If
        'If chkQ13.Items(1).Selected Then
        '    sQ13 &= "|2"
        'End If
        'If chkQ13.Items(2).Selected Then
        '    sQ13 &= "|3"
        'End If
        FTND = StrNull2Zero(txtFTND.Text)
        If FTND < 4 Then
            sQ11 = 0
        ElseIf FTND >= 4 And FTND <= 6 Then
            sQ11 = 1
        Else
            sQ11 = 2
        End If


        If chkQ14.Items(0).Selected Then
            sQ14 = "1"
        End If
        If chkQ14.Items(1).Selected Then
            sQ14 &= "|2"
        End If
        If chkQ14.Items(2).Selected Then
            sQ14 &= "|3"
        End If
        If chkQ14.Items(3).Selected Then
            sQ14 &= "|4"
        End If
        If chkQ14.Items(4).Selected Then
            sQ14 &= "|5"
        End If


        Dim SmokeFQ, SmokeDay, SmokeQTY, StopDay As Integer
        Dim SmokeUOM, StopUOM As String

        SmokeFQ = StrNull2Zero(optSmokeFQ.SelectedValue)
        SmokeUOM = optSmokeUOM.SelectedValue
        StopUOM = optStopUOM.SelectedValue
        SmokeDay = StrNull2Zero(txtSmokeDay.Text)
        Select Case SmokeFQ
            Case 1, 2
                SmokeQTY = StrNull2Zero(txtSmokeQTY.Text)
            Case 3
                StopDay = StrNull2Zero(txtSmokeQTY.Text)
        End Select

        'Dim dStopPlan1, dStopPlan2 As String
        Dim dQuitDate, dNextService As String
        'dStopPlan1 = ConvertStrDate2InformDateString(xDateStopPlan1.Text)
        'dStopPlan2 = ConvertStrDate2InformDateString(xDateStopPlan2.Text)
        dQuitDate = ConvertStrDate2InformDateString(QuitPlanDate.Text)
        dNextService = ConvertStrDate2InformDateString(xDateNextService.Text)


        '------------------------------------------------------
        If lblID.Text = "" Then 'Add new
            If ctlOrder.Smoking_ChkDupCustomer(FORM_TYPE_ID_A1, Session("patientid"), StrNull2Zero(txtYear.Text)) = True Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','ผู้รับบริการรายนี้เคยได้รับบริการในกิจกรรมนี้แล้ว');", True)
                Exit Sub
            End If

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Smoking", "บันทึกเพิ่มกิจกรรมปรับเปลี่ยนพฤติกรรมผู้เสพยาสูบโดยเภสัชกรชุมชน (A1-A4):<<PatientID:" & Session("patientid") & ">>", "A1-A4")
        Else
            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Smoking", "บันทึกเพิ่มกิจกรรมปรับเปลี่ยนพฤติกรรมผู้เสพยาสูบโดยเภสัชกรชุมชน (A1-A4) :<<PatientID:" & Session("patientid") & ">>", "A1-A4")
        End If

        ctlOrder.A1_Save(StrNull2Zero(lblID.Text), StrNull2Zero(txtYear.Text), FORM_TYPE_ID_A1, ServiceDate, Str2Double(txtTime.Text), lblLocationID.Text, StrNull2Zero(Session("patientid")), StrNull2Zero(ddlPerson.SelectedValue), optPatientType.SelectedValue, optPatientFrom.SelectedValue, txtFromRemark.Text, optService.SelectedValue, txtServiceRemark.Text, optTelepharm.SelectedValue, optTelepharmacyMethod.SelectedValue, txtRecordMethod.Text, txtRecordLocation.Text, txtTelepharmacyRemark.Text, StrNull2Zero(optAlcohol.SelectedValue), SmokeFQ, SmokeDay, SmokeUOM, SmokeQTY, StopDay, StopUOM, StrNull2Zero(optCigarette.SelectedValue), txtNicotinRate.Text, StrNull2Zero(txtOften.Text), StrNull2Zero(txtMinutePerTime.Text), StrNull2Zero(optIntimate.SelectedValue), txtCloseupPerson.Text,
              optQ1A.SelectedValue, txtQ1B.Text, StrNull2Zero(txtQ1C.Text), optQ1D.SelectedValue,
              strQ2, sQ2Med, txtQ2Other.Text,
              StrNull2Zero(optQ5.SelectedValue),
              StrNull2Zero(optQ6.SelectedValue),
              StrNull2Zero(optQ7.SelectedValue),
              StrNull2Zero(optQ8.SelectedValue),
              StrNull2Zero(optQ9.SelectedValue),
              StrNull2Zero(optQ10.SelectedValue),
              sQ11, StrNull2Zero(txtFTND.Text),
              sQ12, sQ13, sQ14, txtQ14.Text, Str2Double(txtWeight.Text), Str2Double(txtHeigh.Text), txtBP.Text,
              Str2Double(txtPEFR_Value.Text), Str2Double(txtPEFR_Rate.Text), Str2Double(txtCO_Value.Text), optStopPlane1.SelectedValue, dQuitDate,
             dNextService, Convert2Status(chkStatus.Checked), Request.Cookies("username").Value)

        Dim ServiceID As Integer = ctlOrder.Smoking_Order_GetID(StrNull2Zero(txtYear.Text), lblLocationID.Text, FORM_TYPE_ID_A1, Session("patientid"), 0)

        ctlOrder.CPA_MedOrder_DeleteByStatus(FORM_TYPE_ID_A1, lblLocationID.Text, StrNull2Zero(Session("patientid")), "D")

        If grdMed.Rows.Count > 0 Then

            Dim UOM As String = ""
            Dim BALANCE As Integer = 0

            For i = 0 To grdMed.Rows.Count - 1
                UOM = ctlOrder.Medicine_GetUOM(StrNull2Zero(grdMed.DataKeys(i).Value))

                BALANCE = ctlSK.Stock_GetBalance(StrNull2Zero(grdMed.DataKeys(i).Value), lblLocationID.Text)

                ctlOrder.CPA_MedOrder_Add(0, StrNull2Zero(grdMed.DataKeys(i).Value), grdMed.Rows(i).Cells(1).Text, grdMed.Rows(i).Cells(3).Text, StrNull2Zero(grdMed.Rows(i).Cells(2).Text), Request.Cookies("username").Value, FORM_TYPE_ID_A1, lblLocationID.Text, "A", StrNull2Long(Session("patientid")), StrNull2Zero(txtYear.Text))

                ctlSK.Stock_Inventory_ISS(StrNull2Zero(grdMed.DataKeys(i).Value), StrNull2Zero(grdMed.Rows(i).Cells(2).Text), Request.Cookies("username").Value, FORM_TYPE_ID_A1, lblLocationID.Text, "A", UOM, BALANCE, "ISSPTN", ServiceID, ParseDateToSQL(txtServiceDate.Text, "/"))


            Next
        End If
        Response.Redirect("ResultPage.aspx")
    End Sub

    Protected Sub optSmokeFQ_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optSmokeFQ.SelectedIndexChanged
        optStopUOM.Visible = False
        txtSmokeQTY.Visible = False
        lblCGday.Visible = False
        Select Case optSmokeFQ.SelectedValue
            Case 1, 2
                txtSmokeQTY.Visible = True
                lblCGday.Visible = True
            Case 3
                optStopUOM.Visible = True
                txtSmokeQTY.Visible = True
        End Select
    End Sub

    Private Sub txtPEFR_Value_TextChanged(sender As Object, e As EventArgs) Handles txtPEFR_Value.TextChanged
        CalPEFR_Rate()
    End Sub

    'Protected Sub optNextStep_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optNextStep.SelectedIndexChanged
    '    If optNextStep.SelectedValue = "2" Then
    '        lblProblem.Visible = True
    '        optNextCause.Visible = True
    '        txtCause.Visible = True
    '    Else
    '        lblProblem.Visible = False
    '        optNextCause.Visible = False
    '        txtCause.Visible = False
    '    End If
    'End Sub

    Private Sub CallQ11()
        Try
            txtFTND.Text = StrNull2Zero(optQ5.SelectedValue) + StrNull2Zero(optQ6.SelectedValue) + StrNull2Zero(optQ7.SelectedValue) + StrNull2Zero(optQ8.SelectedValue) + StrNull2Zero(optQ9.SelectedValue) + StrNull2Zero(optQ10.SelectedValue)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub optQ5_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ5.SelectedIndexChanged
        CallQ11()
    End Sub

    Protected Sub optQ6_SelectedIndexChanged1(sender As Object, e As EventArgs) Handles optQ6.SelectedIndexChanged
        CallQ11()
    End Sub
    Protected Sub optQ7_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ7.SelectedIndexChanged
        CallQ11()
    End Sub
    Protected Sub optQ8_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ8.SelectedIndexChanged
        CallQ11()
    End Sub

    Protected Sub optQ9_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ9.SelectedIndexChanged
        CallQ11()
    End Sub

    Protected Sub optQ10_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ10.SelectedIndexChanged
        CallQ11()
    End Sub

    Private Sub grdMed_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdMed.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel"

                    'dtPOS = Session("dtposA4")
                    'dtPOS.Rows(e.CommandArgument).Delete()
                    'Session("dtposA4") = Nothing
                    'Session("dtposA4") = dtPOS

                    'grdData.DataSource = Nothing
                    'grdData.DataSource = dtPOS
                    'grdData.DataBind()

                    ctlOrder.MedOrder_DisableByItemA4(StrNull2Long(Session("patientid")), FORM_TYPE_ID_A1, StrNull2Zero(grdMed.DataKeys(e.CommandArgument).Value), lblLocationID.Text)

                    ctlSK.Stock_Inventory_Delete(StrNull2Zero(grdMed.DataKeys(e.CommandArgument).Value), StrNull2Zero(grdMed.Rows(e.CommandArgument).Cells(2).Text), FORM_TYPE_ID_A1, lblLocationID.Text, "ISSPTN", lblID.Text)

                    grdMed.DataSource = Nothing
                    LoadMedOrderItem(StrNull2Long(lblID.Text))

                    ddlMed.Focus()

            End Select


        End If
    End Sub
    Private Sub LoadMedOrderItem(ServiceUID As Long)
        Dim dtM2 As New DataTable

        'ctlOrder.MedOrder_DeleteByStatus(FORM_TYPE_ID_A1, lblLocationID.Text, StrNull2Long(Session("patientid")), "Q")

        dtM2 = ctlOrder.CPA_MedOrder_GetMedItem(FORM_TYPE_ID_A1, lblLocationID.Text, Session("patientid"), txtYear.Text)
        If dtM2.Rows.Count > 0 Then
            With grdMed
                .Visible = True
                .DataSource = dtM2
                .DataBind()
                'For i = 0 To .Rows.Count - 1
                '    .Rows(i).Cells(0).Text = i + 1
                'Next
            End With
            'Session("dtposA4") = dtM
        Else
            grdMed.Visible = False
            grdMed.DataSource = dtM2
            grdMed.DataBind()

        End If
        dtM2 = Nothing
    End Sub

    Protected Sub cmdAddMed_Click(sender As Object, e As EventArgs) Handles cmdAddMed.Click

        If ddlMed.SelectedValue = "12" Then
            If txtMedName.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','ท่านยังไม่ได้ป้อนชื่อยา');", True)
                Exit Sub
            End If
            'Else
            '    If StrNull2Zero(txtMedQTY.Text) > StrNull2Zero(lblBalance.Text) Then
            '         ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','ยาในสต๊อกมีไม่พอ")
            '        Exit Sub
            '    End If
        End If

        If StrNull2Zero(txtMedQTY.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','ท่านยังไม่ได้ป้อนจำนวนที่จ่าย');", True)
            Exit Sub
        End If

        Dim MedName As String = ""

        If ddlMed.SelectedValue = "12" Then
            MedName = txtMedName.Text
        Else
            MedName = ddlMed.SelectedItem.Text
        End If

        ctlOrder.CPA_MedOrder_Add(0, ddlMed.SelectedValue, MedName, txtFrequency.Text, StrNull2Zero(txtMedQTY.Text), Request.Cookies("username").Value, FORM_TYPE_ID_A1, lblLocationID.Text, "Q", StrNull2Long(Session("patientid")), StrNull2Zero(txtYear.Text))

        LoadMedOrderItem(StrNull2Long(lblID.Text))

        txtMedName.Text = ""
        txtMedQTY.Text = ""
    End Sub
    'Private Sub LoadBalanceStock()
    '    Dim ctlSK As New StockController
    '    lblBalance.Text = ctlSK.Stock_GetBalance(ddlMed.SelectedValue, lblLocationID.Text)
    '    lblUOM.Text = ctlOrder.Medicine_GetUOM(ddlMed.SelectedValue)
    'End Sub

    'Protected Sub ddlMed_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlMed.SelectedIndexChanged
    '    lblUnit2.Visible = False
    '    txtMedName.Visible = False
    '    'lblBalance.Visible = True
    '    'lblBalanceLabel.Visible = True
    '    'LoadBalanceStock()


    '    Select Case ddlMed.SelectedValue
    '        Case "11"
    '            lblUnit1.Visible = False
    '            lblUnit2.Visible = True
    '        Case "12"
    '            txtMedName.Visible = True
    '            lblUnit1.Visible = True

    '            'lblBalance.Visible = False
    '            'lblBalanceLabel.Visible = False
    '    End Select
    'End Sub

    Private Sub grdMed_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdMed.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            e.Row.Cells(0).Text = e.Row.RowIndex + 1
            'Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            'Dim imgD As Image = e.Row.Cells(2).FindControl("imgDel")
            'imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

End Class