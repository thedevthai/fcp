﻿
Public Class vmiStockRefillManual
    Inherits System.Web.UI.Page

    Dim dt As New DataTable

    Dim acc As New UserController
    Dim ctlbase As New ApplicationBaseClass
    'Dim ctlOrder As New SmokingController
    Dim ctlSk As New StockController
    Dim ctlInv As New InventoryController


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Session("Username") Is Nothing Then
        '    Response.Redirect("Default.aspx?logout=YES")
        'End If
        If Not IsPostBack Then
            grdData.PageIndex = 0


            LoadProvince()
            LoadStock()
        End If

    End Sub

    Private Sub LoadProvince()
        dt = ctlbase.LoadProvince
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .DataSource = dt
                .DataTextField = "ProvinceName"
                .DataValueField = "ProvinceID"
                .DataBind()
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadStock()


        dt = ctlSk.Stock_GetSearch(txtMedName.Text.Trim(), txtLocationName.Text.Trim(), ddlProvince.SelectedValue)

        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                Try

                    Dim nrow As Integer = dt.Rows.Count
                    If .PageCount > 1 Then
                        If .PageIndex > 0 Then
                            If (.PageSize) < nrow - (.PageSize * .PageIndex) Then
                                For i = 0 To .PageSize - 1
                                    .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                Next
                            Else
                                For i = 0 To nrow - (.PageSize * .PageIndex) - 1
                                    .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                Next
                            End If
                        Else
                            For i = 0 To .PageSize - 1
                                .Rows(i).Cells(0).Text = i + 1
                            Next
                        End If
                    Else
                        For i = 0 To nrow - 1
                            .Rows(i).Cells(0).Text = i + 1
                        Next
                    End If
                Catch ex As Exception

                End Try

            End With
        End If
        dt = Nothing
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadStock()
    End Sub
    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    tabPageSearch.ActiveTabIndex = 1
                    EditData(e.CommandArgument())
                Case "imgDel"

                    ctlSk.Stock_Disable(e.CommandArgument, Session("Username"))
                    acc.User_GenLogfile(Session("Username"), ACTTYPE_DEL, "Stock", "Disable Stock UID:" & e.CommandArgument(), "")


                    DisplayMessage(Me, "ลบข้อมูลเรียบร้อย")
                    LoadStock()
            End Select


        End If
    End Sub


    Private Sub EditData(ByVal pID As Integer)

        dt = ctlSk.Stock_GetByUID(pID)

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                lblUID.Text = DBNull2Str(dt.Rows(0)("UID"))
                Me.txtMedName.Text = DBNull2Str(dt.Rows(0)("itemName"))
                lblLocationID.Text = DBNull2Str(dt.Rows(0)("LocationID"))
                lblLocationName.Text = DBNull2Str(dt.Rows(0)("LocationName"))
                ddlProvince.SelectedValue = DBNull2Str(dt.Rows(0)("ProvinceID"))
                lblOnHand.Text = DBNull2Zero(dt.Rows(0)("OnHand"))
                lblStock.Text = DBNull2Zero(dt.Rows(0)("Stock"))
                lblUOM.Text = DBNull2Str(dt.Rows(0)("UOM"))
                lblROP.Text = DBNull2Zero(dt.Rows(0)("ROP"))
                lblMedUID.Text = DBNull2Str(dt.Rows(0)("MedUID"))
                lblMedName.Text = DBNull2Str(dt.Rows(0)("itemName"))
                txtQTY.Focus()
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub ClearData()
        'Me.lblCode.Text = ""
        txtLocationName.Text = ""
        txtMedName.Text = ""
        lblLocationID.Text = ""
        txtLocationName.Text = ""
        txtQTY.Text = ""
        lblStock.Text = ""
        lblROP.Text = ""
        lblOnHand.Text = ""
        lblLocationName.Text = ""
        lblUOM.Text = ""
        lblUID.Text = ""
    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            'Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            'Dim imgD As Image = e.Row.Cells(8).FindControl("imgDel")
            'imgD.Attributes.Add("onClick", scriptString)

            If StrNull2Zero(e.Row.Cells(6).Text) <= StrNull2Zero(e.Row.Cells(5).Text) Then
                e.Row.ForeColor = Drawing.Color.Red
            End If


        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub
    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        Dim iQTY As Integer = DBNull2Zero(txtQTY.Text)

        ctlInv.Inventory_Transfer(StrNull2Zero(lblUID.Text), lblLocationID.Text, StrNull2Zero(lblMedUID.Text), iQTY, lblUOM.Text, "ISSCPA", "P", Session("Username"))

        acc.User_GenLogfile(Session("username"), ACTTYPE_ADD, "VMI_Transfer", "เติมยาให้ร้านยา: " & lblLocationID.Text & ">>" & lblMedUID.Text & ">>QTY:" & iQTY, "")


        LoadStock()
        DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อยแล้ว")

    End Sub


    Protected Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        ClearData()
    End Sub

    Protected Sub cmdSearch_Click(sender As Object, e As EventArgs) Handles cmdSearch.Click
        grdData.PageIndex = 0
        LoadStock()
    End Sub
End Class

