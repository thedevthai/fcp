﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="RegisterList.aspx.vb" Inherits=".RegisterList" %>
<%@ Import Namespace="System.Data" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
      <section class="content-header">
      <h1>Register List
        <small>(ข้อมูลผู้ลงทะเบียนเข้าโครงการ)</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">
          
        <div class="box box-solid">
             <div class="box-header">
              <i class="fa fa-filter"></i>
              <h3 class="box-title">ค้นหา</h3>   
                  <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>วันที่ลงทะเบียน</label>
                            <br />
                            <div class="input-group">
                                <asp:TextBox ID="txtStartDate" runat="server" CssClass="form-control text-center"
                                    autocomplete="off" data-date-format="dd/mm/yyyy"
                                    data-date-language="th-th" data-provide="datepicker"
                                    onkeyup="chkstr(this,this.value)"></asp:TextBox>
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa lnr-calendar-full"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>ถึง</label>
                            <br />
                            <div class="input-group">
                                <asp:TextBox ID="txtEndDate" runat="server" CssClass="form-control text-center"
                                    autocomplete="off" data-date-format="dd/mm/yyyy"
                                    data-date-language="th-th" data-provide="datepicker"
                                    onkeyup="chkstr(this,this.value)"></asp:TextBox>
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fa lnr-calendar-full"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                  <div class="col-md-3">
                        <div class="form-group">
                            <label>กลุ่ม</label>
                            <br />
                            <asp:DropDownList ID="ddlGroup" runat="server" CssClass="form-control select2" Width="100%" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>สถานะ</label>
                            <br />
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control select2" Width="100%" AutoPostBack="True">
                                <asp:ListItem Selected="True" Value="0">ทั้งหมด</asp:ListItem>
                                <asp:ListItem Value="1">Pendding</asp:ListItem>
                                <asp:ListItem Value="2">Approved</asp:ListItem>
                                <asp:ListItem Value="3">Reject</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-12 text-center"> 
                        <asp:LinkButton ID="cmdView" runat="server" CssClass="btn btn-success" Width="120px"><i class="fa fa-search"></i> ค้นหา</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <i class="header-icon lnr-list icon-gradient bg-success"></i>Register List
            <div class="btn-actions-pane-right">             
            </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table id="tbdata" class="table table-bordered table-hover">
                        <thead>
                            <tr>  
                                <th class="sorting_asc_disabled sorting_desc_disabled text-center"></th>
                                <th class="text-center" style="width: 30px">No.</th>
                                <th class="text-center">ชื่อร้านยา</th>
                                <th class="text-center">เลขที่ ขย.5</th>
                                <th class="text-center">สถานที่ตั้ง</th>
                                <th class="text-center">ประเภท</th>
                                <th class="text-center">วันที่ลงทะเบียน</th>
                                <th width="100" class="text-center">สถานะ</th>                             
                                <th class="text-left">Username</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <% For Each row As DataRow In dtReg.Rows %>
                            <tr> <td class="text-center" style="width: 50px">                                   
                                    <a href="RegisterDetail?id=<% =String.Concat(row("UID")) %>" class="text-primary" data-toggle="tooltip" data-placement="top" data-original-title="รายละเอียด"><i class="fa fa-edit" aria-hidden="true" style="font-size:20px"></i></a>
                                </td>
                                <td><% =String.Concat(row("nRow")) %></td>
                                <td> <a href="RegisterDetail?id=<% =String.Concat(row("UID")) %>" data-toggle="tooltip" data-placement="top" data-original-title="ดูรายละเอียด"><% =String.Concat(row("LocationName")) %></a></td>
                                <td class="text-center"><% =String.Concat(row("LicenseNo")) %></td>
                                <td><% =String.Concat(row("Address")) %></td>
                                <td class="text-center"><% =String.Concat(row("GroupName")) %></td> 
                                <td class="text-center"><% =String.Concat(row("RegisterDate")) %></td>
                                <td class="text-center"><% =String.Concat(row("StatusName")) %></td> 
                                <td class="text-center"><% =String.Concat(row("Username")) %></td>
                            </tr>
                            <%  Next %>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>

  </section>                  

</asp:Content>
