﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="LocationsEdit.aspx.vb" Inherits=".LocationsEdit" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
        </asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>ข้อมูลร้านยา
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">ข้อมูลร้านยา</li>
      </ol>
    </section>

<section class="content">    
  

          <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-users"></i>

              <h3 class="box-title">บันทึก/แก้ไข ข้อมูลร้านยา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
           <table  class="table table-bordered MenuSt">
                                                <tr>
                                                     <td Width="150px">รหัสในโครงการ :</td>
                                                  <td><asp:Label ID="lblID" runat="server" CssClass="form-control text-success text-bold text-center" BackColor="#D0E8FF"></asp:Label></td>   
                                                     <td Width="100px" class="text-right">เลขที่ ขย. 5</td>
                                                    <td><asp:TextBox ID="txtLicenseNo" runat="server" CssClass="form-control text-center" ></asp:TextBox></td>                                               
                                                    <td Width="200px"  class="text-right">รหัสหน่วยบริการ สปสช.</td>
                                                    <td><asp:TextBox ID="txtNHSOCode" runat="server" CssClass="form-control text-center" ></asp:TextBox></td>
                                                </tr>
    </table>    
    <table  border="0" width="100%">
        <tr>
            <td>  
    <table  class="table table-bordered" border="0">
                                                <tr>
                                                    <td>ประเภท :</td>
                                                    <td>
                                                        <asp:Label ID="lblTypeName" runat="server"></asp:Label>                                                    </td>
                                                  <td>จังหวัด : </td>
                                                    <td>
                                                        <asp:Label ID="lblProvinceName" runat="server"></asp:Label>                                                    </td>
                                                </tr>
                                                <tr>
                                                  <td>ชื่อร้านยา / เภสัชอาสา / ชื่อ รพ.</td>
                                                  <td><asp:TextBox ID="txtName" runat="server" CssClass="form-control" ></asp:TextBox></td>
  <td>ประเภทร้านยา:</td>
                                                      <td >
                                                           <table>
                                                             <tr>
                                                                 <td>
                                                        <asp:DropDownList ID="ddlTypeShop" runat="server" CssClass="form-control select2" Width="200px">
                                                            <asp:ListItem Selected="True">ร้านยาคุณภาพ</asp:ListItem>
                                                            <asp:ListItem Value="ร้านยา GPP">ร้านยา GPP</asp:ListItem>
                                                            <asp:ListItem Value="ร้านยาเภสัชกร"></asp:ListItem>
                                                             <asp:ListItem Value="โรงพยาบาล"></asp:ListItem>
                                                             <asp:ListItem Value="รพสต."></asp:ListItem>
                                                                     </asp:DropDownList>                                                    
                                                                 </td><td>ระบุ</td>
                                                                 <td>                                                    
                                                        <asp:TextBox ID="txtTypeName" runat="server" CssClass="form-control" Width="200px"></asp:TextBox>                                                    </td>
                                                             
                                                             </tr>
                                                         </table>
                                                      </td>
                                                </tr>
                                                  <tr>
                                                    <td align="left" valign="top" >ที่อยู่ :</td>
                                                    <td align="left" colspan="3" ><asp:TextBox ID="txtAddress" runat="server" TextMode="MultiLine" CssClass="form-control" Height="50px"></asp:TextBox></td>
                                                       
                                                </tr>
                                                <tr> 
                                                     <td align="left" valign="bottom" >รหัสไปรษณีย์:</td> 
                                                    <td align="left" > <asp:TextBox ID="txtZipCode" runat="server" MaxLength="5"></asp:TextBox></td>
                                                    
                                                    <td align="left" valign="top" >ละติจูด,ลองติจูด</td>
                                                  
                                                   
                                                    <td align="left" valign="bottom" >
                                                         <small class="text-blue">ละติจูด/ลองติจูด ในรูปแบบองศาทศนิยมเท่านั้น และคั่นชุดข้อมูลด้วยคอมม่า (,)</small>
                                                        <asp:TextBox ID="txtLat" runat="server" CssClass="form-control"></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                    <td>เบอร์โทรเพื่อการประชาสัมพันธ์ร้าน :</td>
                                                    <td><table width="100%" border="0">
                                                          <tr>
                                                            <td  width="350"><asp:TextBox ID="txtTel" runat="server" CssClass="form-control"></asp:TextBox></td>
                                                            <td width="80" align="right">Line ID :</td>
                                                            <td><asp:TextBox ID="txtFax" runat="server" CssClass="form-control"></asp:TextBox></td>
                                                          </tr>
                                                        </table></td>
                                                    <td>E-mail :</td>
                                                    <td><asp:TextBox ID="txtMail" runat="server" 
                                                            Width="200px"></asp:TextBox></td>
                                                </tr>
                                                  <tr>
                                                  <td>ชื่อเจ้าของ(คู่สัญญา)</td>
                                                  <td ><table width="100%" border="0">
                                                <tr>
                                                          <td width="350"><asp:TextBox ID="txtCoName" runat="server" CssClass="form-control"></asp:TextBox></td>
                                                          <td width="80"  align="right">E-mail :</td>
                                                          <td><asp:TextBox ID="txtCoMail" runat="server" CssClass="form-control"></asp:TextBox></td>
                                                    </tr>
                                                      </table></td>
                                                      <td >โทรศัพท์ : </td>
                                                      <td class="text-left"><asp:TextBox ID="txtCoTel" runat="server" Width="200px"></asp:TextBox></td>
          </tr>
                                 <!--                <tr>
                                                  <td>เลขที่บัญชี :</td>
                                                  <td>
                                                     <asp:TextBox ID="txtAccNo" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                                                  <td>ธนาคาร :</td>
                                                <td >
                                                   <asp:DropDownList ID="ddlBank" runat="server" CssClass="form-control select2">                                                    </asp:DropDownList>                                                    </td>
          </tr>
                                                <tr>
                                                  <td>ชื่อบัญชี :</td>
                                                  <td>
                                                     <asp:TextBox ID="txtAccName" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                                                  <td>สาขา :</td>
                                                <td>
                                                   <asp:TextBox ID="txtBrunch" runat="server" Width="200px"></asp:TextBox>                                                    </td>
                                                </tr>
                                                <tr>
                                                  <td>ประเภทบัญชี :</td>
                                                  <td colspan="3" >
                                                     <asp:RadioButtonList ID="optBankType" runat="server" 
                                                          RepeatDirection="Horizontal">
                                                    <asp:ListItem Selected="True" Value="SAV">ออมทรัพย์/สะสมทรัพย์/เผื่อเรียก</asp:ListItem>
                                                    <asp:ListItem Value="DDA">กระแสรายวัน/เดินสะพัด</asp:ListItem>
                                                    <asp:ListItem Value="BOC">ฝากประจำ</asp:ListItem>
                                                  </asp:RadioButtonList></td>
                                                 </tr>
                                                <tr>
                                                  <td>เลขบัตรประชาชน:</td>
                                                  <td >
                                                     <asp:TextBox ID="txtCardID" runat="server" Width="300px" MaxLength="13"></asp:TextBox>                                                    &nbsp;(เฉพาะตัวเลขเท่านั้น)</td>
                                                  <td>&nbsp;</td>
                                                  <td>&nbsp;</td>
                                                </tr>
        -->
     <tr>
                                                  <td>ปี พ.ศ.ที่สมัครเข้าโครงการ</td>
                                                  <td >       
                                                      <asp:TextBox ID="txtYear" runat="server" MaxLength="4"></asp:TextBox>
                                                    </td>
                                                  <td>&nbsp;</td>
                                                  <td>&nbsp;</td>
                                                </tr>

  </table>   
        
            </td>
      </tr>
       <tr>
          <td valign="top" class="MenuSt" >เภสัชกรที่ปฏิบัติหน้าที่
              </td>
      </tr>
       <tr>
          <td valign="top" >
<asp:GridView ID="grdData" 
                             runat="server" CellPadding="2" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" CssClass="table table-hover" 
                             Font-Bold="False">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                            <asp:BoundField >
                            <ItemStyle HorizontalAlign="Center" Width="30px" />
                            </asp:BoundField>
                        <asp:BoundField DataField="PrefixName" HeaderText="คำนำหน้า" >                            
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FirstName" HeaderText="ชื่อ" >
                            <HeaderStyle HorizontalAlign="Left" />                            
                            <ItemStyle Width="200px" />
                            </asp:BoundField>
                            <asp:BoundField DataField="LastName" HeaderText="นามสกุล" >
                            <HeaderStyle HorizontalAlign="Left" />                            
                            <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" 
                                                            />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView>              </td>
      </tr>
       <tr>
          <td align="center" valign="top"><asp:Label ID="lblValidate" runat="server" 
                  Visible="False" CssClass="validateAlert" Width="99%"></asp:Label></td>
      </tr>
       <tr>
          <td align="center" valign="top"><span >
            <asp:Button ID="cmdSave" runat="server" text="บันทึก" cssclass="buttonSave" Width="100px" ></asp:Button>
            <asp:Button ID="cmdClear" runat="server" 
                                                        text="ยกเลิก" cssclass="buttonCancle" Width="100px" ></asp:Button>
          </span></td>
      </tr>
        <tr>
          <td align="center" valign="top">&nbsp;
              </td>
      </tr>
    </table>

 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
  </section>      
</asp:Content>
