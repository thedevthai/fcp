﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FormList_Smoking
    
    '''<summary>
    '''ddlYear control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlYear As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''ddlForm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlForm As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''lblProv control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblProv As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ddlProvinceID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlProvinceID As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''txtSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtSearch As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''cmdSearch control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents cmdSearch As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''lblCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCount As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''lblCountForm control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblCountForm As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''grdData control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdData As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''lblNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNo As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Panel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Panel2 As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblPayCount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPayCount As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''grdDataPaymented control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdDataPaymented As Global.System.Web.UI.WebControls.GridView
    
    '''<summary>
    '''lblNo2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNo2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Panel1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Panel1 As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''lblNot control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblNot As Global.System.Web.UI.WebControls.Label
End Class
