﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Subgurim.Controles
Public Class H01
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlL As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlH As New HomeVisitController
    Dim ctlMTM As New MTMController

    Dim ServiceDate As Long
    Dim sAlert As String
    Dim isValid As Boolean


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Session("patientid") Is Nothing Then
            Response.Redirect("PatientSearch.aspx")
        End If

        If Not IsPostBack Then
            isAdd = True
            txtCGNo.Visible = False
            lblCGday.Visible = False
            txtCGYear.Visible = False
            lblCGYear.Visible = False
            lblCgType.Visible = False
            optCigarette.Visible = False
            lblAlcohol.Visible = False
            txtAlcoholFQ.Visible = False

            pnAlertM.Visible = False
            If Request("acttype") = "view" Then
                cmdSave.Visible = False
            Else
                cmdSave.Visible = True
            End If


            txtServiceDate.Text = DisplayShortDateTH(ctlL.GET_DATE_SERVER)
            txtBYear.Text = Right(txtServiceDate.Text, 4)

            If StrNull2Zero(txtBYear.Text) < 2500 Then
                txtBYear.Text = StrNull2Zero(txtBYear.Text) + 543
            Else
                txtBYear.Text = StrNull2Zero(txtBYear.Text)
            End If


            LoadProblemGroup()
            LoadDeseaseToDDL()
            LoadMedicine()
            LoadLocationData(Session("LocationID"))
            LoadPharmacist(Session("LocationID"))


            If Request("t") = "new" Or (Request("t") Is Nothing) Then
                lblNotic.Visible = True
                pnDetail.Visible = False
                lblSEQ.Text = (ctlH.H01_GetLastSEQ(Session("LocationID"), txtBYear.Text, Session("patientid")) + 1).ToString
            Else
                If Not Request("fid") Is Nothing Then
                    LoadMTMHeader()
                End If
            End If

            LoadLabResultToGrid()

        End If

        'If (FileUploaderAJAX1.IsPosting) Then
        '    ServiceDate = CLng(ConvertDate2DBString(Today.Date))
        '    'F01_sFile = Path.GetExtension(FileUploaderAJAX1.PostedFile.FileName)
        '    F01_DocFile = "Refer" & "_" & lblLocationID.Text & "_" & ServiceDate & "_" & ConvertTimeToString(Now())
        '    UploadFile(F01_DocFile)
        'End If


        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtPitting.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")

    End Sub
    Private Sub LoadDeseaseToDDL()
        cboDesease.Items.Clear()
        Dim dtD As New DataTable
        Dim ctlD As New DeseaseController

        dtD = ctlD.Desease_Get

        If dtD.Rows.Count > 0 Then
            cboDesease.Items.Clear()

            With cboDesease
                .DataSource = dtD
                .TextField = "Name"
                .ValueField = "UID"
                .DataBind()
            End With


        End If

    End Sub

    Private Sub LoadLocation()

        dt = ctlL.Location_GetByID(Session("LocationID"))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("ProvinceName"))
            End With
        End If
    End Sub
    Private Sub LoadPharmacist(LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With

                'With ddlPharmacist_D
                '    .Visible = True
                '    For i = 0 To dtP.Rows.Count - 1
                '        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                '        .Items(i).Value = dtP.Rows(i)("PersonID")
                '    Next
                '    .SelectedIndex = 0
                'End With



            End If
        End If
        dtP = Nothing
    End Sub

    Private Sub LoadMedicine()
        Dim dtM As New DataTable
        Dim ctlD As New DrugController
        cboDrug.Items.Clear()
        dtM = ctlD.Drug_Get
        If dtM.Rows.Count > 0 Then
            cboDrug.DataSource = dtM
            cboDrug.TextField = "Name"
            cboDrug.ValueField = "UID"
            cboDrug.DataBind()
        End If
        dtM = Nothing
    End Sub

    Private Sub LoadProblemGroup()
        Dim dtPG As New DataTable
        Dim ctlP As New ProblemController
        cboProblemGroup.Items.Clear()
        dtPG = ctlP.ProblemGroup_Get
        If dtPG.Rows.Count > 0 Then
            cboProblemGroup.DataSource = dtPG
            cboProblemGroup.TextField = "Descriptions"
            cboProblemGroup.ValueField = "Code"
            cboProblemGroup.DataBind()
        End If
        dtPG = Nothing
    End Sub
    Private Sub LoadProblem()
        Dim dtP As New DataTable
        Dim ctlP As New ProblemController
        cboProblemItem.Items.Clear()
        dtP = ctlP.ProblemItem_Get(cboProblemGroup.Value)
        If dtP.Rows.Count > 0 Then
            cboProblemItem.DataSource = dtP
            cboProblemItem.TextField = "Descriptions"
            cboProblemItem.ValueField = "Code"
            cboProblemItem.DataBind()
        End If
        dtP = Nothing
    End Sub

    Private Sub LoadLocationData(LocationID As String)
        dt = ctlL.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblLocationID.Text = .Item("LocationID")
                lblLocationName.Text = .Item("LocationName")
                lblLocationProvince.Text = .Item("ProvinceName")
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadMTMHeader()
        Dim BYear As Integer = 0

        BYear = ctlH.H01_GetBYear(StrNull2Zero(Request("fid")))

        If Request("t") = "new" Then
            dt = ctlH.H01_GetLastInfo(BYear, Session("patientid"))
        Else
            dt = ctlH.H01_GetByUID(StrNull2Zero(Request("fid")))
        End If


        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                lblNotic.Visible = False
                pnDetail.Visible = True
                txtBYear.Text = .Item("BYear")
                lblHUID.Text = .Item("UID")
                lblSEQ.Text = DBNull2Str(.Item("SEQ"))

                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                'lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                'lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                txtTime.Text = String.Concat(.Item("ServiceTime"))

                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))


                optSmoke.SelectedValue = String.Concat(.Item("Smoke"))
                txtCGYear.Text = String.Concat(.Item("SmokeYear"))
                txtCGNo.Text = String.Concat(.Item("SmokeCigarette"))
                optCigarette.SelectedValue = String.Concat(.Item("CigaretteType"))
                optAlcohol.SelectedValue = String.Concat(.Item("Alcohol"))
                txtAlcoholFQ.Text = String.Concat(.Item("AlcoholFQ"))



                chkPitting.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("isPitting")))
                chkWound.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("isWound")))
                chkPeripheral.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("isPeripheral")))

                txtPitting.Text = String.Concat(.Item("Pitting"))
                txtWound.Text = String.Concat(.Item("Wound"))
                txtPeripheral.Text = String.Concat(.Item("Peripheral"))

                chkStatus.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("Status")))

                If Request("t") = "new" Then
                    lblHUID.Text = ""
                    txtServiceDate.Text = DisplayShortDateTH(ctlL.GET_DATE_SERVER)
                    txtTime.Text = ""
                    ddlPerson.SelectedIndex = 0
                    pnDetail.Visible = False
                Else
                    pnDetail.Visible = True
                    LoadDrugProblemToGrid()
                    LoadDeseaseRelateToGrid()
                    LoadLabResultToGrid()
                End If





            End With
        Else
            LoadLocationData(Session("LocationID"))
        End If
        dt = Nothing

    End Sub

    Protected Sub optSmoke_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optSmoke.SelectedIndexChanged
        Select Case optSmoke.SelectedValue
            Case "2"
                txtCGYear.Visible = True
                lblCGYear.Visible = True
            Case "1"
                lblCGday.Visible = True
                txtCGNo.Visible = True
                lblCGYear.Visible = True
                txtCGYear.Visible = True
                optAlcohol.Visible = True
            Case Else
                txtCGNo.Visible = False
                lblCGday.Visible = False
                txtCGYear.Visible = False
                lblCGYear.Visible = False
                lblCgType.Visible = False
                optCigarette.Visible = False
        End Select
    End Sub

    Protected Sub optAlcohol_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optAlcohol.SelectedIndexChanged
        Select Case optAlcohol.SelectedValue
            Case "3"
                lblAlcohol.Visible = True
                txtAlcoholFQ.Visible = True
            Case Else
                lblAlcohol.Visible = False
                txtAlcoholFQ.Visible = False
        End Select
    End Sub


    Private Function ValidateData() As Boolean
        sAlert = ""
        isValid = True
        If txtInterventionDrug.Text = "" Then
            sAlert &= "- Intervention <br/>"
            isValid = False
        End If
        Return isValid
    End Function
    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If StrNull2Zero(txtTime.Text) = 0 Then
            DisplayMessage(Me.Page, "กรุณาป้อนระยะเวลาในการให้บริการก่อน")
            Exit Sub
        End If

        Dim objuser As New UserController
        Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))

        If lblHUID.Text = "" Then 'Add new

            If ctlH.H01_ChkDupCustomer(Session("patientid"), ServiceDate) = True Then
                DisplayMessage(Me.Page, "ท่านได้บันทึกการให้บริการของผู้รับบริการนี้ ในวันที่ท่านระบุแล้ว")
                LoadDataToComboBox()
                Exit Sub
            End If

            ctlH.H01_Add(lblLocationID.Text, txtBYear.Text, Session("patientid"), ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), StrNull2Zero(optSmoke.SelectedValue), StrNull2Zero(txtCGYear.Text), StrNull2Zero(txtCGNo.Text), StrNull2Zero(optCigarette.SelectedValue), StrNull2Zero(optAlcohol.SelectedValue), StrNull2Zero(txtAlcoholFQ.Text), Convert2Status(chkStatus.Checked), Session("username"), StrNull2Zero(lblSEQ.Text))

            objuser.User_GenLogfile(Session("Username"), ACTTYPE_ADD, "H01", "เพิ่มบันทึกการเยี่ยมบ้าน :<<PatientID:" & Session("patientid") & ">>", "Home Visit")

            lblHUID.Text = ctlH.H01_GetLastUID(lblLocationID.Text, txtBYear.Text, Session("patientid"), ServiceDate).ToString()

            LoadMedicine()
            LoadProblemGroup()
            LoadDeseaseToDDL()

            pnDetail.Visible = True
        Else
            ctlH.H01_Update(StrNull2Zero(lblHUID.Text), lblLocationID.Text, txtBYear.Text, Session("patientid"), ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), StrNull2Zero(optSmoke.SelectedValue), StrNull2Zero(txtCGYear.Text), StrNull2Zero(txtCGNo.Text), StrNull2Zero(optCigarette.SelectedValue), StrNull2Zero(optAlcohol.SelectedValue), StrNull2Zero(txtAlcoholFQ.Text), Convert2Status(chkStatus.Checked), Session("username"))

            objuser.User_GenLogfile(Session("Username"), ACTTYPE_UPD, "H01", "แก้ไขบันทึกการเยี่ยมบ้าน :<<PatientID:" & Session("patientid") & ">>", "Home Visit")
        End If
        'Response.Redirect("ResultPage.aspx")
        DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อย")
        LoadDataToComboBox()


    End Sub
#Region "Edit Data"
    Private Sub EditDrugProblem(UID As Integer)
        LoadMedicine()
        LoadProblemGroup()
        LoadDeseaseToDDL()

        dt = ctlMTM.MTM_DrugProblem_GetByUID(UID)
        If dt.Rows.Count > 0 Then
            lblUID_Drug.Text = dt.Rows(0)("UID")
            'txtFollowDate.Text = DisplayStr2ShortDateTH(dt.Rows(0)("FollowDate"))
            'ddlPharmacist_D.SelectedValue = dt.Rows(0)("PharmacistUID")
            cboDrug.Value = DBNull2Str(dt.Rows(0)("DrugUID"))
            'cboDesease.Value = DBNull2Str(dt.Rows(0)("DeseaseUID"))
            cboProblemGroup.Value = dt.Rows(0)("ProblemGroupUID")
            LoadProblem()
            cboProblemItem.Value = dt.Rows(0)("ProblemUID")

        End If
    End Sub
#End Region

    Protected Sub cboProblemGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboProblemGroup.SelectedIndexChanged
        LoadProblem()
    End Sub

#Region "Load Data to Grid"

    Private Sub LoadLabResultToGrid()
        dt = ctlH.LabResult1_Get(StrNull2Zero(lblHUID.Text))
        grdLab.DataSource = dt
        grdLab.DataBind()

        dt = ctlH.LabResult2_Get(StrNull2Zero(lblHUID.Text))
        grdLab2.DataSource = dt
        grdLab2.DataBind()
    End Sub
    Private Sub LoadDeseaseRelateToGrid()
        dt = ctlMTM.MTM_DeseaseRelate_Get(StrNull2Zero(lblHUID.Text), FORM_TYPE_ID_H01)
        grdDesease.DataSource = dt
        grdDesease.DataBind()
    End Sub
    Private Sub LoadDrugProblemToGrid()
        dt = ctlMTM.MTM_DrugProblem_GetByHeaderUID(StrNull2Zero(lblHUID.Text), FORM_TYPE_ID_H01)
        grdDrugProblem.DataSource = dt
        grdDrugProblem.DataBind()
    End Sub

#End Region
#Region "Grid Event"
    Protected Sub grdDrugProblem_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDrugProblem.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(6).FindControl("imgDel_D")
            imgD.Attributes.Add("onClick", scriptString)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub
    Private Sub grdDrugProblem_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDrugProblem.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit_D"
                    EditDrugProblem(e.CommandArgument())
                Case "imgDel_D"
                    ctlMTM.MTM_DrugProblem_Delete(StrNull2Zero(lblUID_Drug.Text))
                    LoadDrugProblemToGrid()
                    LoadDataToComboBox()
            End Select
        End If
    End Sub
    Protected Sub grdDesease_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDesease.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(2).FindControl("imgDel_DS")
            imgD.Attributes.Add("onClick", scriptString)

            e.Row.Cells(0).Text = e.Row.RowIndex + 1
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub
    Protected Sub grdBehavior_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdLab.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub
    Private Sub grdDesease_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDesease.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel_DS"
                    ctlMTM.MTM_DeseaseRelate_Delete(e.CommandArgument())
                    LoadDeseaseRelateToGrid()
                    LoadDataToComboBox()
            End Select
        End If
    End Sub

#End Region
    Protected Sub cmdAddDesease_Click(sender As Object, e As EventArgs) Handles cmdAddDesease.Click
        If cboDesease.Value = "" Then
            'DisplayMessage(Me.Page, "เลือกโรคที่ต้องการเพิ่มก่อน")
            LoadDataToComboBox()
            Exit Sub
        End If

        ctlMTM.MTM_DeseaseRelate_Add(StrNull2Zero(lblHUID.Text), FORM_TYPE_ID_H01, cboDesease.Value, txtDeseaseOther.Text, Session("userid"))
        LoadDeseaseRelateToGrid()
        LoadDataToComboBox()
    End Sub


    Protected Sub cmdAddLab_Click(sender As Object, e As EventArgs) Handles cmdAddLab.Click
        ServiceDate = CLng(ConvertStrDate2DBString(txtServiceDate.Text))
        For i = 0 To grdLab.Rows.Count - 1
            Dim txtV As TextBox = grdLab.Rows(i).Cells(1).FindControl("txtResultValue")
            ctlH.LabResult_Save(StrNull2Zero(lblHUID.Text), StrNull2Zero(grdLab.DataKeys(i).Value), ServiceDate, Session("patientid"), txtV.Text, Session("userid"))
        Next
        For i = 0 To grdLab2.Rows.Count - 1
            Dim txtV As TextBox = grdLab2.Rows(i).Cells(1).FindControl("txtResultValue0")
            ctlH.LabResult_Save(StrNull2Zero(lblHUID.Text), StrNull2Zero(grdLab2.DataKeys(i).Value), ServiceDate, Session("patientid"), txtV.Text, Session("userid"))
        Next
        DisplayMessage(Me.Page, "บันทึกผล Lab เรียบร้อย")
        LoadDataToComboBox()
    End Sub

    Protected Sub cmdAddDrugProblem_Click(sender As Object, e As EventArgs) Handles cmdAddDrugProblem.Click
        If Not ValidateData() Then
            lblAlertM.Text = "<b>กรุณากรอกข้อมูลให้ครบถ้วน</b> <br/>" & sAlert
            lblAlertM.Visible = True
            pnAlertM.Visible = True
            LoadDataToComboBox()
            Exit Sub
        Else
            pnAlertM.Visible = False

        End If

        ctlMTM.MTM_DrugProblem_Save(StrNull2Zero(lblHUID.Text), StrNull2Zero(lblUID_Drug.Text), FORM_TYPE_ID_H01, StrNull2Zero(cboDrug.Value), cboProblemGroup.Value, cboProblemItem.Value, txtProblemOtherDrug.Text, txtInterventionDrug.Text, optResult_D.SelectedValue, txtResultOther_D.Text, Session("username"))

        lblUID_Drug.Text = "0"

        cboDrug.Value = ""
        cboProblemGroup.Value = ""
        cboProblemItem.Value = "" +
        cboDesease.Value = ""
        'ddlPharmacist_D.SelectedIndex = 0

        LoadDrugProblemToGrid()
        'DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อย")

        pnAlertM.Visible = False
        LoadDataToComboBox()
    End Sub
    Private Sub LoadDataToComboBox()
        LoadDeseaseToDDL()
        LoadProblemGroup()
        LoadMedicine()
    End Sub

    Protected Sub cmdPE_Save_Click(sender As Object, e As EventArgs) Handles cmdPE_Save.Click
        ctlH.H01_UpdatePE(StrNull2Zero(lblHUID.Text), ConvertStatus2YN(chkPitting.Checked), ConvertStatus2YN(chkWound.Checked), ConvertStatus2YN(chkPeripheral.Checked), txtPitting.Text, txtWound.Text, txtPeripheral.Text)
        DisplayMessage(Me.Page, "บันทึกข้อมูลเรียบร้อย")
        LoadDataToComboBox()
    End Sub

    Protected Sub grdLab2_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdLab2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Protected Sub cmdSave2_Click(sender As Object, e As EventArgs) Handles cmdSave2.Click
        ctlH.H01_Update(StrNull2Zero(lblHUID.Text), lblLocationID.Text, txtBYear.Text, Session("patientid"), ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), StrNull2Zero(optSmoke.SelectedValue), StrNull2Zero(txtCGYear.Text), StrNull2Zero(txtCGNo.Text), StrNull2Zero(optCigarette.SelectedValue), StrNull2Zero(optAlcohol.SelectedValue), StrNull2Zero(txtAlcoholFQ.Text), Convert2Status(chkStatus.Checked), Session("username"))

        Response.Redirect("FormList_HomeVisit.aspx")
    End Sub
End Class