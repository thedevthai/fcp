﻿Imports System.Net
Imports System.Net.Mail
Public Class Users
    Inherits System.Web.UI.Page

    Dim dt As New DataTable

    Dim ctlUser As New UserController
    Dim ctlRole As New RoleController
    Dim objLct As New LocationInfo
    Dim ctlLct As New LocationController
    Dim objEn As New CryptographyEngine

    Dim objRole As New RoleInfo

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then
            grdData.PageIndex = 0
            cmdSendMail.Visible = False
            txtUsername.ReadOnly = False
            lblID.Text = 0
            LoadProvinceGroupToDDL()
            LoadUserAccountToGrid()
            LoadRolsToCheckList()
            LoadLocationToDDL()
            txtFindLocation.Visible = False
            ' txtFindPreceptor.visible = False
            ddlLocation.Visible = False

            lnkFindLocation.Visible = False
            '  lnkFindPreceptor.visible = False

            lblLocation.Visible = False
            imgArrowLocation.Visible = False

            lblRPTRole.Visible = False
            ddlRPTRole.Visible = False
            lblMNGRole.Visible = False
            ddlProjectRole.Visible = False
            ClearData()
        End If
    End Sub

    Private Sub LoadRolsToCheckList()
        dt = ctlRole.GetRoles
        With optRoles
            .Visible = True
            .DataSource = dt
            .DataTextField = objRole.tblField(objRole.fldPos.f01_RoleName).fldName
            .DataValueField = objRole.tblField(objRole.fldPos.f00_RoleID).fldName
            .DataBind()
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub LoadLocationToDDL()
        Dim dtL As New DataTable
        If txtFindLocation.Text = "" Then
            dtL = ctlLct.Location_Get
        Else
            dtL = ctlLct.Location_GetBySearch("0", txtFindLocation.Text)
        End If

        ddlLocation.Items.Clear()

        If dtL.Rows.Count > 0 Then
            With ddlLocation
                .Visible = True
                .Items.Add("---เลือกร้านยา---")
                .Items(0).Value = 0
                For i = 0 To dtL.Rows.Count - 1
                    .Items.Add(dtL.Rows(i)("LocationName"))
                    .Items(i + 1).Value = dtL.Rows(i)("LocationID")
                Next
                .SelectedIndex = 0
            End With
        End If

    End Sub

    Private Sub LoadUserAccountToGrid()

        dt = ctlUser.GetUsers_ByGroupSearch(1, ddlGroupFind.SelectedValue, txtSearch.Text)

        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                Try

                    Dim nrow As Integer = dt.Rows.Count

                    If .PageCount > 1 Then
                        If .PageIndex > 0 Then
                            If (.PageSize) < nrow - (.PageSize * .PageIndex) Then
                                For i = 0 To .PageSize - 1
                                    .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)

                                    If dt.Rows(i)("RoleID") = 1 Then
                                        .Rows(i).Cells(2).Text = ctlLct.Location_GetNameByID(String.Concat(dt.Rows((.PageSize * .PageIndex) + (i))("LocationID")))
                                    Else
                                        .Rows(i).Cells(2).Text = dt.Rows((.PageSize * .PageIndex) + (i))("FirstName") & " " & dt.Rows((.PageSize * .PageIndex) + (i))("LastName")
                                    End If
                                    If dt.Rows(i)("RoleID") = 2 Then
                                        .Rows(i).Cells(5).Text = ctlLct.ProvinceGroupName_GetByID(dt.Rows((.PageSize * .PageIndex) + (i))("ReportGroup"))
                                    End If

                                Next
                            Else
                                For i = 0 To nrow - (.PageSize * .PageIndex) - 1
                                    .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                    If dt.Rows(i)("RoleID") = 1 Then
                                        .Rows(i).Cells(2).Text = ctlLct.Location_GetNameByID(String.Concat(dt.Rows((.PageSize * .PageIndex) + (i))("LocationID")))
                                    Else
                                        .Rows(i).Cells(2).Text = dt.Rows((.PageSize * .PageIndex) + (i))("FirstName") & " " & dt.Rows((.PageSize * .PageIndex) + (i))("LastName")
                                    End If
                                    If dt.Rows(i)("RoleID") = 2 Then
                                        .Rows(i).Cells(5).Text = ctlLct.ProvinceGroupName_GetByID(dt.Rows((.PageSize * .PageIndex) + (i))("ReportGroup"))
                                    End If
                                Next
                            End If
                        Else
                            For i = 0 To .PageSize - 1
                                .Rows(i).Cells(0).Text = i + 1

                                If dt.Rows(i)("RoleID") = 1 Then
                                    .Rows(i).Cells(2).Text = ctlLct.Location_GetNameByID(String.Concat(dt.Rows(i)("LocationID")))
                                Else
                                    .Rows(i).Cells(2).Text = dt.Rows(i)("FirstName") & " " & dt.Rows(i)("LastName")
                                End If
                                If dt.Rows(i)("RoleID") = 2 Then
                                    .Rows(i).Cells(5).Text = ctlLct.ProvinceGroupName_GetByID(dt.Rows(i)("ReportGroup"))
                                End If
                            Next
                        End If
                    Else
                        For i = 0 To nrow - 1
                            .Rows(i).Cells(0).Text = i + 1

                            If dt.Rows(i)("RoleID") = 1 Then
                                .Rows(i).Cells(2).Text = ctlLct.Location_GetNameByID(String.Concat(dt.Rows(i)("LocationID")))
                            Else
                                .Rows(i).Cells(2).Text = dt.Rows(i)("FirstName") & " " & dt.Rows(i)("LastName")
                            End If
                            If dt.Rows(i)("RoleID") = 2 Then
                                .Rows(i).Cells(5).Text = ctlLct.ProvinceGroupName_GetByID(dt.Rows(i)("ReportGroup"))
                            End If

                        Next
                    End If


                Catch ex As Exception

                End Try


            End With
        Else
            grdData.DataSource = Nothing
            grdData.Visible = False
        End If
        dt = Nothing

    End Sub
    Protected Function validateData() As Boolean
        Dim result As Boolean = True
        lblvalidate1.Text = ""
        lblvalidate1.Text = ""
        lblvalidate1.Text = ""


        If lblID.Text = 0 Then

            If ddlUserGroup.SelectedIndex = 0 Then
                result = False
                lblvalidate1.Text = "กรุณาเลือกกลุ่มผู้ใช้"
                lblvalidate1.Visible = True
            Else
                Select Case ddlUserGroup.SelectedValue
                    Case 1 'Shop
                        If ddlLocation.SelectedValue = "0" Then
                            result = False
                            lblvalidate1.Text = "กรุณาเลือกร้านยา"
                            lblvalidate1.Visible = True
                        End If
                End Select
            End If

        End If

        If txtUsername.Text = "" Or txtPassword.Text = "" Then
            result = False
            lblvalidate2.Text = "กรุณากรอก Username และ Password ก่อน"
            lblvalidate2.Visible = True

        Else

            If lblID.Text = "0" Then
                dt = ctlUser.GetUsers_ByUsername(txtUsername.Text)
                If dt.Rows.Count > 0 Then
                    result = False
                    If ddlUserGroup.SelectedValue = 1 Then
                        lblvalidate2.Text = "ร้านยานี้มี Username ในระบบแล้ว"
                    Else
                        lblvalidate2.Text = "มี Username นี้ในระบบแล้ว กรุณาตั้ง Username ใหม่"
                    End If

                    lblvalidate2.Visible = True
                    imgAlert.Visible = True
                Else
                    imgAlert.Visible = False
                End If

            End If

        End If


        If chkProject.Items(0).Selected = False And chkProject.Items(1).Selected = False Then
            lblvalidate3.Text = "เลือกโครงการที่จะให้สิทธิ์ก่อน"
            lblvalidate3.Visible = True
        End If


        Dim n As Integer = 0
        For i = 0 To optRoles.Items.Count - 1
            If optRoles.Items(i).Selected Then
                n = n + 1
            End If
        Next


        If n = 0 Then
            lblvalidate3.Text &= vbCrLf & "กรุณากำหนดสิทธิ์ให้ผู้ใช้ก่อน"
            lblvalidate3.Visible = True
        End If


        Return result
    End Function


    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSave.Click

        'UserID = ctlUser.GetUsersID_ByUsername(txtUsername.Text)
        If validateData() Then

            Dim item, MngProj As Integer
            Dim sLocationID, RPTGRP As String

            Dim status As Integer = 0
            If chkStatus.Checked Then
                status = 1
            End If



            If ddlUserGroup.SelectedValue = 1 Then
                sLocationID = ddlLocation.SelectedValue
            Else
                sLocationID = LOCATID_CPA
            End If

            If optRoles.SelectedValue = isProjectManager Then
                MngProj = ddlProjectRole.SelectedValue
            ElseIf optRoles.SelectedValue = isReportViewerAccess Then
                MngProj = ddlProjectRole.SelectedValue
            Else
                MngProj = 0
            End If

            If optRoles.SelectedValue = isReportViewerAccess Then
                RPTGRP = ddlRPTRole.SelectedValue
            Else
                RPTGRP = ""
            End If

            If lblID.Text = 0 Then

                item = ctlUser.User_Add(txtUsername.Text, objEn.EncryptString(txtPassword.Text, True), txtFirstName.Text, txtLastName.Text, 0, status, optRoles.SelectedValue, sLocationID, RPTGRP, MngProj, txtMail.Text)

                ctlUser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Users", "add new user :" & txtUsername.Text, "Result:" & item)
            Else
                item = ctlUser.User_Update(txtUsername.Text, objEn.EncryptString(txtPassword.Text, True), txtFirstName.Text, txtLastName.Text, 0, status, optRoles.SelectedValue, sLocationID, RPTGRP, MngProj, txtMail.Text)

                ctlUser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Users", "update user :" & txtUsername.Text, "Result:" & item)

            End If

            For i = 0 To chkProject.Items.Count - 1

                If optRoles.SelectedValue = isReportViewerAccess Then
                    ctlUser.User_AddLocationProject(i + 1, txtUsername.Text, Boolean2StatusFlag(chkProject.Items(i).Selected))
                Else
                    ctlUser.User_AddLocationProject(i + 1, sLocationID, Boolean2StatusFlag(chkProject.Items(i).Selected))
                End If
            Next
            'If item = 1 Then 
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
            LoadUserAccountToGrid()
            ClearData()
            'End If

        Else
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถบันทึกข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง!');", True)
        End If
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadUserAccountToGrid()
    End Sub
    Private Sub grdData_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand

        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"
                    If ctlUser.User_Delete(e.CommandArgument) Then

                        ctlUser.User_GenLogfile(Request.Cookies("username").Value, "DEL", "User", "ลบ user :" & txtUsername.Text, "")

                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ลบข้อมูลเรียบร้อย');", True)
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    End If

                    LoadUserAccountToGrid()
                Case "imgMail"
                    Dim dtU As New DataTable
                    Dim ctlU As New UserController
                    dtU = ctlU.User_GetByID(e.CommandArgument)
                    If dtU.Rows.Count > 0 Then
                        SendEmailUsername(String.Concat(dtU.Rows(0)("FirstName")) & " " & String.Concat(dtU.Rows(0)("LastName")), String.Concat(dtU.Rows(0)("Username")), objEn.DecryptString(dtU.Rows(0)("Password"), True), String.Concat(dtU.Rows(0)("Email")))
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalSuccess(this,'Success','ส่งอีเมล์แจ้ง Username & Password เรียบร้อย');", True)
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','ไม่พบอีเมล์ที่จะส่ง');", True)
                    End If
            End Select

        End If

    End Sub
    Private Sub EditData(ByVal pID As String)
        ClearData()
        Dim dtD As New DataTable
        dtD = ctlUser.User_GetByID(pID)

        If dtD.Rows.Count > 0 Then
            With dtD.Rows(0)
                isAdd = False
                cmdSendMail.Visible = True
                Me.lblID.Text = .Item("UserID")
                txtUsername.Text = .Item("Username")
                Me.txtFirstName.Text = String.Concat(.Item("FirstName"))
                txtLastName.Text = String.Concat(.Item("LastName"))
                txtMail.Text = String.Concat(.Item("Email"))

                If DBNull2Zero(.Item("RoleID")) = 1 Then
                    ddlUserGroup.SelectedValue = 1
                Else
                    ddlUserGroup.SelectedValue = 2
                End If

                txtFindLocation.Visible = False

                txtFirstName.Enabled = True
                txtLastName.Enabled = True
                txtFirstName.BackColor = Drawing.Color.AliceBlue
                txtLastName.BackColor = Drawing.Color.AliceBlue

                ddlLocation.Items.Clear()
                Select Case DBNull2Zero(.Item("RoleID"))
                    Case 1 'Shop
                        lnkFindLocation.Visible = True
                        lblLocation.Visible = True
                        imgArrowLocation.Visible = True
                        txtFindLocation.Visible = True
                        LoadLocationToDDL()
                        ddlLocation.Visible = True
                        ddlLocation.SelectedValue = .Item("LocationID")
                    Case Else
                        ddlLocation.Visible = False
                        txtFindLocation.Visible = False
                        lnkFindLocation.Visible = False
                        lblLocation.Visible = False
                        imgArrowLocation.Visible = False
                End Select

                optRoles.SelectedValue = DBNull2Zero(.Item("RoleID"))
                ddlUserGroup.Enabled = False

                lblRPTRole.Visible = False
                ddlRPTRole.Visible = False
                lblMNGRole.Visible = False
                ddlProjectRole.Visible = False

                If DBNull2Zero(.Item("RoleID")) = isReportViewerAccess Then
                    ddlRPTRole.SelectedValue = DBNull2Str(.Item("ReportGroup"))
                    lblRPTRole.Visible = True
                    ddlRPTRole.Visible = True
                ElseIf DBNull2Zero(.Item("RoleID")) = isProjectManager Then
                    ddlProjectRole.SelectedValue = DBNull2Str(.Item("ProjectManager"))
                    lblMNGRole.Visible = True
                    ddlProjectRole.Visible = True
                Else
                    lblRPTRole.Visible = False
                    ddlRPTRole.Visible = False
                    lblMNGRole.Visible = False
                    ddlProjectRole.Visible = False
                End If

                If .Item("isPublic") = 0 Then
                    chkStatus.Checked = False
                Else
                    chkStatus.Checked = True
                End If

                txtPassword.Text = objEn.DecryptString(.Item("Password"), True)
            End With
            txtUsername.ReadOnly = True

            chkProject.ClearSelection()
            Dim dtLP As New DataTable

            If optRoles.SelectedValue <> isShopAccess Then
                dtLP = ctlUser.LocationProject_GetByUsername(txtUsername.Text)
            Else
                dtLP = ctlUser.LocationProject_GetByLocationID(ddlLocation.SelectedValue)
            End If

            If dtLP.Rows.Count > 0 Then
                For i = 0 To dtLP.Rows.Count - 1
                    Select Case dtLP.Rows(i)("ProjectID")
                        Case "1"
                            chkProject.Items(0).Selected = True
                        Case "2"
                            chkProject.Items(1).Selected = True
                        Case "3"
                            chkProject.Items(2).Selected = True
                        Case "4"
                            chkProject.Items(3).Selected = True
                    End Select
                Next
            End If
            dtLP = Nothing
        End If

        dtD = Nothing

    End Sub

    Protected Sub cmdClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdClear.Click
        ClearData()
        ddlUserGroup.SelectedIndex = 0
    End Sub

    Private Sub ClearData()
        Me.txtUsername.Text = ""
        txtPassword.Text = ""
        Me.txtFirstName.Text = ""
        txtLastName.Text = ""
        lblID.Text = 0
        txtMail.Text = ""
        chkStatus.Checked = True
        txtUsername.ReadOnly = False
        isAdd = True
        imgAlert.Visible = False
        cmdSendMail.Visible = False
        lblvalidate1.Visible = False
        lblvalidate2.Visible = False
        lblvalidate3.Visible = False

        txtFindLocation.Visible = False

        ddlUserGroup.Enabled = True
        ddlLocation.Visible = False

        lnkFindLocation.Visible = False

        lblLocation.Visible = False
        imgArrowLocation.Visible = False

        txtFirstName.Enabled = True
        txtLastName.Enabled = True
        txtUsername.Enabled = True

        txtFirstName.BackColor = Drawing.Color.White
        txtLastName.BackColor = Drawing.Color.White
        txtUsername.BackColor = Drawing.Color.White

    End Sub

    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            'e.Row.Cells(0).Text = e.Row.RowIndex + 1
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(9).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If


    End Sub

    Protected Sub ddlUserGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUserGroup.SelectedIndexChanged

        ClearData()

        txtFindLocation.Visible = False

        txtFirstName.BackColor = Drawing.Color.AliceBlue
        txtLastName.BackColor = Drawing.Color.AliceBlue

        ddlLocation.Visible = False
        lnkFindLocation.Visible = False
        ddlLocation.Items.Clear()
        lblLocation.Visible = False
        imgArrowLocation.Visible = False
        Select Case ddlUserGroup.SelectedValue
            Case 1 'Shop
                LoadLocationToDDL()
                ddlLocation.Visible = True
                lnkFindLocation.Visible = True
                txtFindLocation.Visible = True
                lblLocation.Visible = True
                imgArrowLocation.Visible = True
                optRoles.SelectedValue = 1

            Case Else 'เจ้าหน้าที่อื่นๆ
                optRoles.SelectedValue = 3
                txtFirstName.Enabled = True
                txtLastName.Enabled = True
                txtFirstName.BackColor = Drawing.Color.White
                txtLastName.BackColor = Drawing.Color.White
        End Select
    End Sub

    Protected Sub lnkFindLocation_Click(sender As Object, e As EventArgs) Handles lnkFindLocation.Click
        LoadLocationToDDL()
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadUserAccountToGrid()
    End Sub

    Protected Sub ddlGroupFind_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroupFind.SelectedIndexChanged
        grdData.PageIndex = 0
        LoadUserAccountToGrid()
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        txtUsername.Text = ddlLocation.SelectedValue
        txtFirstName.Text = ddlLocation.SelectedItem.Text

    End Sub

    Private Sub LoadProvinceGroupToDDL()
        dt = ctlLct.LoadProvinceGroup

        ddlRPTRole.Items.Clear()

        If dt.Rows.Count > 0 Then
            With ddlRPTRole
                .Visible = True
                .Items.Add("---ทั้งหมด---")
                .Items(0).Value = "ALL"
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)("ProvinceGroupName"))
                    .Items(i + 1).Value = dt.Rows(i)("ProvinceGroupID")
                Next
                .SelectedIndex = 0
            End With
        End If

    End Sub

    Protected Sub optRoles_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optRoles.SelectedIndexChanged
        lblRPTRole.Visible = False
        ddlRPTRole.Visible = False
        lblMNGRole.Visible = False
        ddlProjectRole.Visible = False

        If optRoles.SelectedValue = isReportViewerAccess Then
            lblRPTRole.Visible = True
            ddlRPTRole.Visible = True
        ElseIf optRoles.SelectedValue = isProjectManager Then
            lblMNGRole.Visible = True
            ddlProjectRole.Visible = True
        Else
            lblRPTRole.Visible = False
            ddlRPTRole.Visible = False
            lblMNGRole.Visible = False
            ddlProjectRole.Visible = False
        End If
    End Sub

    Private Sub cmdSendMail_Click(sender As Object, e As EventArgs) Handles cmdSendMail.Click
        SendEmailUsername(txtFirstName.Text & " " & txtLastName.Text, txtUsername.Text, txtPassword.Text, txtMail.Text)
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalSuccess(this,'Success','ส่งอีเมล์แจ้ง Username & Password เรียบร้อย');", True)
    End Sub
    Private Sub SendEmailUsername(PersonName As String, Username As String, Password As String, sTo As String)
        Dim CC As String = ""
        If sTo = "" Then
            Exit Sub
        End If

        Dim SenderDisplayName As String = "มูลนิธิเภสัชกรรมชุมชน"
        Dim MySubject As String = "แจ้งข้อมูลการเข้าใช้งาน FCPPROJECT : " & PersonName
        Dim MyMessageBody As String = ""

        MyMessageBody = " <font size='4'>
    
  <p> เรียน คุณ " & PersonName & "</p>
  <p> เรื่อง แจ้งข้อมูลการเข้าใช้งานระบบฐานข้อมูลกิจกรรมบริการสร้างเสริมสุขภาพและการดูแลการใช้ยาฯโดยเภสัชกรชุมชน (FCPPROJECT)</p> 
     <p>ท่านสามารถเข้าใช้งานระบบได้ที่ <a href='https://www.fcpproject.com'>https://www.fcpproject.com</p> 
  <p>ข้อมูลผู้ใช้งานของท่าน</p>
        ชื่อผู้ใช้ (Username) : " & Username & " <br />
        รหัสผ่าน (Password) : " & Password & "  <br /> <br />

        <font color='#ff0000'> 
**อีเมล์นี้เป็นระบบอัตโนมัติ ห้ามตอบกลับใดๆทั้งสิ้น** </font><br />
        หากท่านต้องการติดต่อสอบถามข้อมูลเพิ่มเติมสามารถติดต่อได้ตามช่องทางต่อไปนี้ <br />
       
        <a href='https://compharmfund.or.th/site/page/contact_us'>มูลนิธิเภสัชกรรมชุมชน</a> <br />
        อีเมลล์ <a href='mailto:support@compharmfund.or.th'>support@compharmfund.or.th</a> <br />       

        <br /><br />
 
        ขอแสดงความนับถือ <br />
       
มูลนิธิเภสัชกรรมชุมชน<br />
40 ซอย สุขุมวิท 38 แขวงคลองเตย เขตคลองเตย กรุงเทพมหานคร 10110<br /> 
    </font>"


        Dim mailMessage As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage()
        mailMessage.From = New MailAddress("papcinfo@gmail.com", SenderDisplayName)
        mailMessage.Subject = MySubject
        mailMessage.Body = MyMessageBody
        mailMessage.IsBodyHtml = True
        mailMessage.[To].Add(New MailAddress(sTo))
        If CC <> "" Then
            mailMessage.CC.Add(New MailAddress(CC))
        End If

        Dim smtp As SmtpClient = New SmtpClient()
        smtp.Host = "smtp.gmail.com"
        smtp.EnableSsl = True
        Dim NetworkCred As NetworkCredential = New NetworkCredential()
        NetworkCred.UserName = mailMessage.From.Address
        NetworkCred.Password = "qktzlmsamzknnizq"
        smtp.UseDefaultCredentials = True
        smtp.Credentials = NetworkCred
        smtp.Port = 587
        smtp.Send(mailMessage)
    End Sub
End Class

