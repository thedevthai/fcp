﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="PatientList.aspx.vb" Inherits=".PatientList" %>

<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <section class="content-header">
        <h1>รายชื่อผู้รับบริการที่อยากเลิกบุหรี่
        <small></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"></li>
        </ol>
    </section>

    <section class="content">
      
 <div class="box box-primary">
            <div class="box-header"> 
              <h3 class="box-title">&nbsp;</h3>             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>                 
            </div>
            <div class="box-body">
                <table id="tbdata" class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">PID</th>
                            <th class="text-left">ชื่อ-นามสกุล</th>
                            <th class="text-center">อายุ</th>
                            <th class="text-center">เพศ</th>
                            <th class="text-center">ที่อยู่ปัจจุบัน</th>
                            <th class="text-center">ผู้คัดกรอง</th>                     
                            <th class="text-center">วันที่คัดกรอง</th>
                            <% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then%>
                            <th class="sorting_asc_disabled sorting_desc_disabled text-center"></th>
                            <% End If %>
                        </tr>
                    </thead>
                    <tbody>
                        <% For Each row As DataRow In dtQ.Rows %>
                        <tr>
                            <td class="text-center"><% =String.Concat(row("PatientID")) %></td>
                            <td><a href="Patient?ActionType=pt&PatientID=<% =String.Concat(row("PatientID")) %>"><% =String.Concat(row("PatientName")) %> </a></td>
                            <td class="text-center"><% =String.Concat(row("Ages")) %></td>
                            <td class="text-center"><% =String.Concat(row("Gender")) %></td>
                            <td class="text-left"><% =String.Concat(row("PatientAddress")) %></td>
                            <td class="text-left"><% =String.Concat(row("LocationName")) %></td>
                            <td class="text-center"><% =String.Concat(row("CreateDate")) %> </td>
                            <% If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then%>
                            <td  class="text-center">
                                <a href="Patient?ActionType=pt&PatientID=<% =String.Concat(row("PatientID")) %>" class="btn btn-success" data-toggle="tooltip" data-placement="top" data-original-title="ดูข้อมูลโปรไฟล์"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;ดูโปรไฟล์</a> 
                                <a href="A1?ActionType=frm&t=new&pid=<% =String.Concat(row("PatientID")) %>" class="btn btn-primary" data-toggle="tooltip" data-placement="top" data-original-title="ให้บริการ A1-A4"><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;ให้บริการ</a> 
                            </td>
                            <% End If %>
                        </tr>
                        <%  Next %>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</asp:Content>
