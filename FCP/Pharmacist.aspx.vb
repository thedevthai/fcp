﻿
Public Class Pharmacist
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim ctlPs As New PersonController
    Dim objUser As New UserController
    Dim ctlLct As New LocationController
    Dim objLct As New LocationInfo


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            grdData.PageIndex = 0
            isAdd = True
            ClearData()
            UpdateProgress1.Visible = False

            LoadPrefixToDDL()
            LoadLocationToDDL()

            If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
                ddlLocation.SelectedValue = Request.Cookies("LocationID").Value
                ddlLocation.Enabled = False
            Else
                ddlLocation.Enabled = True
            End If

            LoadPharmacist()
        End If

     
    End Sub

    Private Sub LoadPrefixToDDL()
        dt = ctlPs.LoadPrefix
        If dt.Rows.Count > 0 Then
            With ddlPrefix
                .Enabled = True
                .DataSource = dt
                .DataTextField = "PrefixName"
                .DataValueField = "PrefixID"
                .DataBind()
                '.SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadLocationToDDL()

        dt = ctlLct.Location_Get
        ddlLocation.Items.Clear()

        If dt.Rows.Count > 0 Then
            With ddlLocation
                .Visible = True
                .Items.Add("---เลือกร้านยา---")
                .Items(0).Value = 0
                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)(objLct.tblField(objLct.fldPos.f01_LocationName).fldName))
                    .Items(i + 1).Value = dt.Rows(i)(objLct.tblField(objLct.fldPos.f00_LocationID).fldName)
                Next
                .SelectedIndex = 0
            End With
        End If

    End Sub
    Private Sub LoadPharmacist()

        If ddlLocation.SelectedValue <> "0" Then
            If Trim(txtSearch.Text) <> "" Then
                dt = ctlPs.GetPerson_ByLocationSearch(ddlLocation.SelectedValue, txtSearch.Text)
            Else
                dt = ctlPs.GetPerson_ByLocation(ddlLocation.SelectedValue)
            End If
        Else
            If Trim(txtSearch.Text) <> "" Then
                dt = ctlPs.GetPerson_BySearch(txtSearch.Text)
            Else
                dt = ctlPs.GetPerson
            End If

        End If
        If dt.Rows.Count > 0 Then
            lblStudentCount.Text = "พบรายชื่อทั้งหมด " & dt.Rows.Count & " คน"

            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                'For i = 0 To dt.Rows.Count - 1
                '    .Rows(i).Cells(0).Text = i + 1
                'Next



            End With
        Else
            grdData.Visible = False
            lblStudentCount.Text = "พบรายชื่อทั้งหมด 0 คน"
        End If
    End Sub

    Dim acc As New UserController

    'Function chkDup() As Boolean
    '    dt = ctlStd.GetPerson_ByID(StrNull2Zero(lblID.Text))
    '    If dt.Rows.Count > 0 Then
    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If txtFirstName.Text = "" Or txtLastName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณป้อนข้อมูลให้ครบถ้วน');", True)
            Exit Sub
        End If


        If lblID.Text = "" Then

            ctlPs.Person_Add(ddlPrefix.SelectedValue, txtFirstName.Text, txtLastName.Text, txtPositionName.Text, ddlLocation.SelectedValue, Request.Cookies("username").Value, txtLicenseNo.Text)
            acc.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Persons", "เพิ่มรายชื่อเภสัชกรประจำร้านยาใหม่ :" & txtFirstName.Text & " " & txtLastName.Text, "เพิ่มแบบทีละคน")
        Else
            ctlPs.Person_Update(StrNull2Zero(lblID.Text), ddlPrefix.SelectedValue, txtFirstName.Text, txtLastName.Text, txtPositionName.Text, ddlLocation.SelectedValue, Request.Cookies("username").Value, txtLicenseNo.Text)
            acc.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Persons", "แก้ไขชื่อเภสัชกรประจำร้านยาใหม่ :" & txtFirstName.Text & " " & txtLastName.Text, "เพิ่มแบบทีละคน")

        End If

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
        isAdd = True
        LoadPharmacist()
        ClearData()
    End Sub
    Private Sub ClearData()
        txtFirstName.Text = ""
        txtLastName.Text = ""
        ddlPrefix.SelectedIndex = 0
        lblID.Text = ""
        txtPositionName.Text = ""
        txtLicenseNo.Text = ""
    End Sub
    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadPharmacist()
    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
                Case "imgDel"
                    If ctlPs.Person_Delete(e.CommandArgument) Then

                        objUser.User_GenLogfile(Request.Cookies("username").Value, "DEL", "Persons", "ลบเภสัชกรประจำร้านยา :" & e.CommandArgument, "")

                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ลบข้อมูลเรียบร้อย');", True)
                    Else
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    End If

                    LoadPharmacist()

            End Select

        End If
    End Sub

    Private Sub grdData_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            'e.Row.Cells(0).Text = e.Row.RowIndex + 1
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(6).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub

    Private Sub EditData(ByVal pID As String)
        ClearData()

        dt = ctlPs.GetPerson_ByID(pID)

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                lblID.Text = .Item("PersonID")

                If String.Concat(.Item("PrefixID")) <> "" Then
                    Me.ddlPrefix.SelectedValue = String.Concat(.Item("PrefixID"))
                End If

                Me.txtFirstName.Text = String.Concat(.Item("FirstName"))
                txtLastName.Text = String.Concat(.Item("LastName"))
                txtPositionName.Text = String.Concat(.Item("PositionName"))
                ddlLocation.SelectedValue = String.Concat(.Item("LocationID"))
                txtLicenseNo.Text = String.Concat(.Item("LicenseNo"))
            End With

        End If

        dt = Nothing
    End Sub

    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadPharmacist()
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        grdData.PageIndex = 0
        LoadPharmacist()
    End Sub
End Class