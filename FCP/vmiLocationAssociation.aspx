﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="vmiLocationAssociation.aspx.vb" Inherits=".vmiLocationAssociation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/cpastyles.css">
    <link href="css/pagestyles.css" rel="stylesheet" type="text/css" />
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>Location Association
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
<div class="box box-success">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">จัดการคลังจ่ายยา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">




                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
        
          
          <tr>
            <td bgcolor="#FFFFFF">
            
            
            
            <TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" bgColor="#ffffff" border="0">
<TBODY>
					<TR>
						<TD>
						  <TABLE id="Table2" height="100%" cellSpacing="2" cellPadding="0" width="100%" border="0">
				  <TR>
									<TD  valign="top">
                                    
                                    <table border="0" cellspacing="2" cellpadding="2">
                                      <tr>
                                        <td width="100">Store</td>
                                        <td><asp:DropDownList ID="ddlStore" 
                                                    runat="server" CssClass="OptionControl"> </asp:DropDownList></td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                      </tr>
                                      <tr>
                                        <td><asp:Label ID="lblLocation" runat="server" Text="ร้านยา"></asp:Label></td>
                                        <td><asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                          <ContentTemplate>
                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="OptionControl"> </asp:DropDownList>
                                          </ContentTemplate>
                                          <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="lnkFindLocation" EventName="Click" />
                                          </Triggers>
                                        </asp:UpdatePanel></td>
                                        <td><asp:Image ID="imgArrowLocation" runat="server" ImageUrl="images/arrow-orange-icons.png" />
                                        <asp:TextBox ID="txtFindLocation" runat="server" Width="60px"></asp:TextBox></td>
                                        <td><asp:LinkButton ID="lnkFindLocation" runat="server" CssClass="buttonFind">ค้นหา</asp:LinkButton></td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                        <td colspan="3" align="left">
                  <asp:Button ID="cmdSave" runat="server" CssClass="buttonRedial" Text="Save" Width="90px" />
                                          </td>
                                      </tr>
                                      <tr>
                                        <td colspan="4"><asp:Label ID="lblvalidate1" runat="server" CssClass="validateAlert" 
                                                Visible="False" Width="99%"></asp:Label></td>
                                      </tr>
                                    </table></TD>
</TR>
				 
       <tr>
          <td  align="left" valign="top" class="skin_Search"><table border="0" cellspacing="1" cellpadding="0">
            <tr>
              <td width="50" >ค้นหา</td>
               <td width="50" > <asp:DropDownList ID="ddlStoreFind" 
                                                    runat="server"  AutoPostBack="True" 
                       CssClass="OptionControl">
                                                  </asp:DropDownList> </td>

              <td>
                  <asp:TextBox ID="txtSearch" runat="server" Width="150px"></asp:TextBox>
                </td>
              <td>
                  <asp:Button ID="cmdFind" runat="server" CssClass="buttonFind" Text="ค้นหา" />
                </td>
            </tr>
            <tr>
                  <td colspan="3" class="text10_nblue"><strong>&nbsp;*</strong>คำค้นหาสามารถค้นหาได้จาก ชื่อร้านยา</td>
              </tr>
          </table></td>
      </tr>
				  <TR>
				     <TD  valign="top" class="text12b_nblue"><asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" CssClass="txtcontent" 
                             Font-Bold="False">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                            <asp:BoundField HeaderText="No.">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />                            </asp:BoundField>
                            <asp:BoundField DataField="LocationID" HeaderText="รหัสร้านยา" />
                        <asp:BoundField DataField="LocationName" HeaderText="ร้านยา" />
                            <asp:BoundField DataField="ProvinceName" HeaderText="จังหวัด" />
                            <asp:BoundField HeaderText="รหัสคลัง" DataField="StoreCode" />
                            <asp:BoundField DataField="StoreName" HeaderText="ชื่อคลัง" />
                        <asp:TemplateField HeaderText="Edit">
                            <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID")%>' />                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgDel" runat="server" 
                                    ImageUrl="images/icon-delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID")%>' />                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Left" />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView>  </TD>
				     </TR>
				  <TR>
				     <TD  valign="top">&nbsp;</TD>
				     </TR>
								</TABLE>					  </TD>
		  </TR>
				</TBODY>
			</TABLE>            </td>
            </tr>
         
        </table>    
                 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    </section>   
</asp:Content>
