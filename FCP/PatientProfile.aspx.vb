﻿Public Class PatientProfile
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlType As New ServiceTypeController
    Dim ctlOrder As New OrderController
    Dim ctlP As New PatientController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Request("PatientID") Is Nothing Then
            Session("patientid") = Request("PatientID")
        End If
        If Not IsPostBack Then
            'ShowPatientInfo()
            LoadPatientProfileToGrid()
        End If
    End Sub
    
    Private Sub LoadPatientProfileToGrid()
        Dim dtS As New DataTable
        Dim sPage As String = ""

        dt = ctlType.ServiceType_GetProfile(Session("patientid"), Request.Cookies("LocationID").Value)
        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()
                For i = 0 To dt.Rows.Count - 1

                    If .DataKeys(i).Value = FORM_TYPE_ID_A4F Then
                        sPage = "A4"
                    ElseIf .DataKeys(i).Value = FORM_TYPE_ID_COVID Then
                        sPage = "COVID"
                    ElseIf .DataKeys(i).Value = FORM_TYPE_ID_A1 Then
                        sPage = "A1"
                    Else
                        sPage = .DataKeys(i).Value
                    End If

                    If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
                        If dt.Rows(i)("isHas") = 1 Then
                            If (dt.Rows(i)("ServiceTypeID") <> FORM_TYPE_ID_F02) And (dt.Rows(i)("ServiceTypeID") <> FORM_TYPE_ID_F03) Then

                                If (dt.Rows(i)("ServiceTypeID") = FORM_TYPE_ID_COVID) Then
                                    .Rows(i).Cells(2).Text = "<a  Class='buttonSave' href='COVID.aspx?yid=" & Year(ctlOrder.GET_DATE_SERVER) & "'><i class='fa fa-plus-circle'></i>&nbsp;เพิ่มกิจกรรม </a>"
                                Else
                                    .Rows(i).Cells(2).Text = "<a  Class='buttonSave' href='" & sPage & ".aspx?yid=" & Year(ctlOrder.GET_DATE_SERVER) & "'><i class='fa fa-plus-circle'></i>&nbsp;เพิ่มกิจกรรม </a>"
                                End If
                            Else
                                    .Rows(i).Cells(2).Text = "<a  Class='buttonSave' href='FormList.aspx?ActionType=frm'><i class='fa fa-plus-circle'></i>&nbsp;เพิ่มกิจกรรม </a>"
                            End If
                        End If
                    End If

                    dtS = ctlOrder.Service_GetPatinetProfile(sPage, Request("PatientID"))
                    If dtS.Rows.Count > 0 Then
                        For n = 0 To dtS.Rows.Count - 1
                            If (dtS.Rows(n)("LocationID") <> Request.Cookies("LocationID").Value) Then
                                If Convert.ToInt32(Request.Cookies("RoleID").Value) = isSuperAdminAccess Then
                                    .Rows(i).Cells(2).Text &= "&nbsp;<a Class='buttonFind' href='" & sPage & ".aspx?yid=25" & dtS.Rows(n)("nYear") & "'> " & dtS.Rows(n)("nYear") & "</a>"
                                Else
                                    .Rows(i).Cells(2).Text &= "&nbsp; <span Class='buttonDisable'>" & dtS.Rows(n)("nYear") & "</span>"
                                End If
                            Else
                                .Rows(i).Cells(2).Text &= "&nbsp;<a Class='buttonFind' href='" & sPage & ".aspx?yid=25" & dtS.Rows(n)("nYear") & "&fid=" & dtS.Rows(n)("UID") & "'> " & dtS.Rows(n)("nYear") & "</a>"
                            End If



                            ' " <a  target='popup' Class='buttonLogin' onclick='window.open('" & sPage & ".aspx?m=cyt','name','width=1000,height=800')'> " & dtS.Rows(n)("nYear") & "</a>"
                        Next
                    End If


                Next
            End With

        Else
            grdData.Visible = False
            grdData.DataSource = Nothing
        End If
        dt = Nothing
    End Sub

End Class