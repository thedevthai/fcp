﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Public Class F13
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlLct As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlOrder As New OrderController
    Dim ServiceDate As Long
    Public F01_DocFile, F01_sFile As String
    Private _dateDiff As Object

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then
            If Request("acttype") = "view" Then
                cmdSave.Visible = False
                cmdClear.Visible = False
            Else
                cmdSave.Visible = True
                cmdClear.Visible = True
            End If
            LoadPharmacist(Request.Cookies("LocationID").Value)
            txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)
            LoadFormData()


        End If

        'txtScreenBP1.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")
        'txtScreenBP2.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")
        'txtScreenBP3.Attributes.Add("onkeydown", "ikeyCode = event.keyCode;if(ikeyCode==13) {document.form1.HiddenField1.value = 1;}")

        'If HiddenField1.Value <> "" Then
        '    CalBPAVG()
        'End If
        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")


    End Sub
    Private Sub LoadPharmacist(ByVal LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With
            End If
        End If
        dtP = Nothing
    End Sub

    Private Sub LoadLocationData(LocationID As String)
        dt = ctlLct.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblLocationID.Text = .Item("LocationID")
                lblLocationName.Text = .Item("LocationName")
                lblLocationProvince.Text = .Item("ProvinceName")
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadFormData()
        Dim pYear As Integer
        If Not Request("fid") Is Nothing Then
            dt = ctlOrder.GetOrder_ByID(StrNull2Zero(Request("fid")))
        Else
            If Not Request("yid") Is Nothing Then
                pYear = CInt(Request("yid"))
            Else
                pYear = Year(ctlLct.GET_DATE_SERVER)
                If pYear < 2500 Then
                    pYear = pYear + 543
                End If

            End If
            dt = ctlOrder.LocationOrder_ByYear(pYear, FORM_TYPE_ID_F13, Request.Cookies("LocationID").Value, Session("patientid"))
        End If

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Session("patientid") = DBNull2Lng(.Item("PatientID"))
                lblID.Text = .Item("itemID")
                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                txtTime.Text = String.Concat(.Item("ServiceTime"))

                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))

                chkStatus.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("Status")))


                txtAllergy.Text = String.Concat(.Item("MedicinceDesc"))
                txtDisease.Text = String.Concat(.Item("ProblemRemark"))
                txtMedicineUsage.Text = String.Concat(.Item("EducateRemark"))
                txtUsageDay.Text = String.Concat(.Item("EducateCount"))
                optConsult.SelectedValue = String.Concat(.Item("isEducate"))


                txtB4.Text = String.Concat(.Item("ProbPro1"))
                txtV5.Text = String.Concat(.Item("ProbPro2"))

                Dim arMed(1), sStr() As String
                arMed(0) = ""
                arMed(1) = ""


                sStr = Split(String.Concat(.Item("ProbMed1")), "|")
                For i = 0 To sStr.Length - 1
                    arMed(i) = sStr(i)
                Next
                ddlMedicineGroup1.SelectedValue = arMed(0)
                txtMed1.Text = arMed(1)

                arMed(0) = ""
                arMed(1) = ""
                sStr = Split(String.Concat(.Item("ProbMed2")), "|")
                For i = 0 To sStr.Length - 1
                    arMed(i) = sStr(i)
                Next
                ddlMedicineGroup2.SelectedValue = arMed(0)
                txtMed2.Text = arMed(1)

                arMed(0) = ""
                arMed(1) = ""
                sStr = Split(String.Concat(.Item("ProbMed3")), "|")
                For i = 0 To sStr.Length - 1
                    arMed(i) = sStr(i)
                Next
                ddlMedicineGroup3.SelectedValue = arMed(0)
                txtMed3.Text = arMed(1)

                arMed(0) = ""
                arMed(1) = ""
                sStr = Split(String.Concat(.Item("EduMed1")), "|")
                For i = 0 To sStr.Length - 1
                    arMed(i) = sStr(i)
                Next
                ddlMedicineGroup4.SelectedValue = arMed(0)
                txtMed4.Text = arMed(1)

                arMed(0) = ""
                arMed(1) = ""
                sStr = Split(String.Concat(.Item("EduMed2")), "|")
                For i = 0 To sStr.Length - 1
                    arMed(i) = sStr(i)
                Next
                ddlMedicineGroup5.SelectedValue = arMed(0)
                txtMed5.Text = arMed(1)



                'txtMed2.Text = String.Concat(.Item("ProbMed2"))
                'txtMed3.Text = String.Concat(.Item("ProbMed3"))
                'txtMed4.Text = String.Concat(.Item("EduMed1"))
                'txtMed5.Text = String.Concat(.Item("EduMed2"))

                txtFollowDate.Text = String.Concat(.Item("DateCheck"))
                optChannel.SelectedValue = String.Concat(.Item("Follow_Channel"))
                optChannelTime.SelectedValue = String.Concat(.Item("isProblemOther"))
                optFollowup.SelectedValue = String.Concat(.Item("ServicePlan"))

                optCause.SelectedValue = String.Concat(.Item("EduPro1"))
                txtCauseRemark.Text = String.Concat(.Item("EduPro2"))

                If String.Concat(.Item("ServicePlan")) = "2" Then
                    optPlace.SelectedValue = String.Concat(.Item("Hospital_Type"))
                    txtHospitalName.Text = String.Concat(.Item("HospitalName"))
                    txtMedicineFromHospital.Text = String.Concat(.Item("Remark"))
                End If

                txtV5.Text = String.Concat(.Item("ProbPro1"))
                txtB4.Text = String.Concat(.Item("ProbPro2"))

                chkNHSO1.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO1")))
                chkNHSO2.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO2")))
                chkNHSO3.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO3")))
                chkNHSO4.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO4")))
                chkNHSO5.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO5")))
                chkNHSO6.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO6")))
                chkNHSO7.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO7")))
                chkNHSO8.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO8")))

                If DBNull2Zero(.Item("Status")) >= 3 Then
                    cmdSave.Visible = False
                    cmdClear.Visible = False
                Else
                    If Request.Cookies("LocationID").Value <> String.Concat(.Item("LocationID")) Then
                        LoadLocationData(String.Concat(.Item("LocationID")))
                        If Request("t") = "edit" Then
                            cmdSave.Visible = True
                            cmdClear.Visible = True
                        Else
                            cmdSave.Visible = False
                            cmdClear.Visible = False
                        End If
                    Else
                        LoadLocationData(Request.Cookies("LocationID").Value)
                    End If
                End If


                ShowDiagnosisResult(String.Concat(.Item("AbNormalRemark")))


            End With
        Else
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing

    End Sub

    Private Sub ShowDiagnosisResult(sResult As String)
        Dim pstr() As String
        pstr = Split(sResult, "|")

        For i = 0 To pstr.Length - 1

            Select Case pstr(i)
                Case "C1"
                    chkC1.Checked = True
                Case "C2"
                    chkC2.Checked = True
                Case "C3"
                    chkC3.Checked = True
                Case "C4"
                    chkC4.Checked = True
                Case "C5"
                    chkC5.Checked = True
                Case "C6"
                    chkC6.Checked = True
                Case "C7"
                    chkC7.Checked = True
                Case "C8"
                    chkC8.Checked = True
                Case "C9"
                    chkC9.Checked = True
                Case "P1"
                    chkP1.Checked = True
                Case "P2"
                    chkP2.Checked = True
                Case "P3"
                    chkP3.Checked = True
                Case "P4"
                    chkP4.Checked = True
                Case "P5"
                    chkP5.Checked = True
                Case "P6"
                    chkP6.Checked = True
                Case "P7"
                    chkP7.Checked = True
                Case "C8V"
                    chkC8v.Checked = True
                Case "C9B"
                    chkC9b.Checked = True
                Case "C10"
                    chkC10.Checked = True
                Case "C11"
                    chkC11.Checked = True
                Case "V1"
                    chkV1.Checked = True
                Case "V2"
                    chkV2.Checked = True
                Case "V3"
                    chkV3.Checked = True
                Case "V4"
                    chkV4.Checked = True
                Case "V5"
                    chkV5.Checked = True
                Case "B1"
                    chkB1.Checked = True
                Case "B2"
                    chkB2.Checked = True
                Case "B3"
                    chkB3.Checked = True
                Case "B3.1"
                    chkB3_1.Checked = True
                Case "B3.2"
                    chkB3_2.Checked = True
                Case "B4"
                    chkB4.Checked = True
                Case "O1"
                    chkO1.Checked = True
                Case "O2"
                    chkO2.Checked = True
                Case "O3"
                    chkO3.Checked = True
                Case "O4"
                    chkO4.Checked = True
                Case "V6"
                    chkV6.Checked = True
                Case "V7"
                    chkV7.Checked = True
                Case "B5"
                    chkB5.Checked = True
                Case "B6"
                    chkB6.Checked = True
                Case "B7"
                    chkB7.Checked = True
                Case "O5.1"
                    chkO5_1.Checked = True
                Case "O5.2"
                    chkO5_2.Checked = True
                Case "O5.3"
                    chkO5_3.Checked = True
                Case "O5.4"
                    chkO5_4.Checked = True
                Case "O5.5"
                    chkO5_5.Checked = True
                Case "O5.6"
                    chkO5_6.Checked = True
                Case "O5.2A"
                    chkO5_2a.Checked = True
                Case "O5.3A"
                    chkO5_3a.Checked = True
            End Select

        Next

    End Sub
    Protected Sub cmdClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdClear.Click
        lblID.Text = ""
        txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)
        txtTime.Text = ""
        ddlPerson.SelectedIndex = 0
        'txtFName.Text = ""
        'txtLName.Text = ""
        'optGender.SelectedIndex = 0
        'txtAges.Text = ""
        'txtCardID.Text = ""
        'txtBirthDate.Text = ""
        'txtTelephone.Text = ""
        'txtMobile.Text = ""
        'optAddressType.SelectedIndex = 0
        'txtAddress.Text = ""
        'txtRoad.Text = ""
        'txtDistrict.Text = ""
        'txtCity.Text = ""
        'ddlProvince.SelectedValue = "01"
        'optClaim.SelectedIndex = 0
        txtB4.Text = ""
        txtV5.Text = ""
        txtMed1.Text = ""
        txtMed2.Text = ""
        txtMed3.Text = ""
        txtMed4.Text = ""
        txtMed5.Text = ""
        txtHospitalName.Text = ""
        txtAllergy.Text = ""
        txtDisease.Text = ""
        txtUsageDay.Text = ""
        txtMedicineUsage.Text = ""
        txtFollowDate.Text = ""

    End Sub

    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSave.Click

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        'If Trim(txtFName.Text) = "" Or Trim(txtLName.Text) = "" Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนชื่อ-นามสกุลผู้รับบริการก่อน');", True)
        '    Exit Sub
        'End If
        'If Trim(txtMobile.Text) = "" Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนเบอร์มือถือผู้รับบริการก่อน หากไม่มีให้ใส่เบอร์บ้านในช่องเบอร์มือถือได้")
        '    Exit Sub
        'End If
        If StrNull2Zero(txtTime.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนระยะเวลาในการให้บริการก่อน');", True)
            Exit Sub
        End If
        'If StrNull2Zero(txtAges.Text) = 0 Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนอายุผู้รับบริการก่อน');", True)
        '    Exit Sub
        'End If
        Dim objuser As New UserController
        Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))
        ''Dim BirthDate As String = ConvertStrDate2DBString(txtBirthDate.Text)


        Dim strP As String = ""
        Dim AbNormalRemark As String = ""

        AbNormalRemark = GetDiagnosisResult()

        Dim Med1, Med2, Med3, Med4, Med5 As String
         
        If Trim(txtMed1.Text) <> "" Then
            Med1 = ddlMedicineGroup1.SelectedValue & "|" & txtMed1.Text
        Else
            Med1 = ""
        End If

        If Trim(txtMed2.Text) <> "" Then
            Med2 = ddlMedicineGroup2.SelectedValue & "|" & txtMed2.Text
        Else
            Med2 = ""
        End If

        If Trim(txtMed3.Text) <> "" Then
            Med3 = ddlMedicineGroup3.SelectedValue & "|" & txtMed3.Text
        Else
            Med3 = ""
        End If

        If Trim(txtMed4.Text) <> "" Then
            Med4 = ddlMedicineGroup4.SelectedValue & "|" & txtMed4.Text
        Else
            Med4 = ""
        End If

        If Trim(txtMed5.Text) <> "" Then
            Med5 = ddlMedicineGroup5.SelectedValue & "|" & txtMed5.Text
        Else
            Med5 = ""
        End If

        If lblID.Text = "" Then 'Add new

            If ctlOrder.Order_ChkDupCustomer(FORM_TYPE_ID_F13, Session("patientid"), StrNull2Zero(Left(Str(ServiceDate), 4))) = True Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ผู้รับบริการรายนี้เคยได้รับบริการในกิจกรรมนี้แล้ว');", True)
                Exit Sub
            End If

            ctlOrder.F13_Add(lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F13, Convert2Status(chkStatus.Checked),
            txtAllergy.Text _
          , txtDisease.Text _
          , txtMedicineUsage.Text _
          , StrNull2Zero(txtUsageDay.Text) _
          , optConsult.SelectedValue _
          , Med1 _
          , Med2 _
          , Med3 _
          , Med4 _
          , Med5 _
          , 1 _
          , optChannel.SelectedValue _
          , optChannelTime.SelectedValue _
          , txtFollowDate.Text _
          , optFollowup.SelectedValue _
          , optCause.SelectedValue _
          , txtCauseRemark.Text _
          , optPlace.SelectedValue _
          , txtHospitalName.Text _
          , txtMedicineFromHospital.Text _
          , AbNormalRemark _
          , txtV5.Text _
          , txtB4.Text _
          , Request.Cookies("username").Value, CLng(ConvertDate2DBString(ctlLct.GET_DATE_SERVER)), Boolean2Decimal(chkNHSO1.Checked), Boolean2Decimal(chkNHSO2.Checked), Boolean2Decimal(chkNHSO3.Checked), Boolean2Decimal(chkNHSO4.Checked), Boolean2Decimal(chkNHSO5.Checked), Boolean2Decimal(chkNHSO6.Checked), Boolean2Decimal(chkNHSO7.Checked), Boolean2Decimal(chkNHSO8.Checked), Session("patientid"), Session("sex"), Session("age"))


            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Service_Order", "บันทึกเพิ่มแบบคัดกรองการใช้ยาปฏิชีวนะในโรคติดเชื้อทางเดินหายใจส่วนบน (URIs) อย่างสมเหตุผล (F13):" & Session("patientname"), "F13")
        Else
            ctlOrder.F13_Update(StrNull2Zero(lblID.Text), lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F13, Convert2Status(chkStatus.Checked),
            txtAllergy.Text _
          , txtDisease.Text _
          , txtMedicineUsage.Text _
          , StrNull2Zero(txtUsageDay.Text) _
          , optConsult.SelectedValue _
          , Med1 _
          , Med2 _
          , Med3 _
          , Med4 _
          , Med5 _
          , 1 _
          , optChannel.SelectedValue _
          , optChannelTime.SelectedValue _
          , txtFollowDate.Text _
          , optFollowup.SelectedValue _
          , optCause.SelectedValue _
          , txtCauseRemark.Text _
          , optPlace.SelectedValue _
          , txtHospitalName.Text _
          , txtMedicineFromHospital.Text _
          , AbNormalRemark _
          , txtV5.Text _
          , txtB4.Text _
          , Request.Cookies("username").Value, CLng(ConvertDate2DBString(ctlLct.GET_DATE_SERVER)), Boolean2Decimal(chkNHSO1.Checked), Boolean2Decimal(chkNHSO2.Checked), Boolean2Decimal(chkNHSO3.Checked), Boolean2Decimal(chkNHSO4.Checked), Boolean2Decimal(chkNHSO5.Checked), Boolean2Decimal(chkNHSO6.Checked), Boolean2Decimal(chkNHSO7.Checked), Boolean2Decimal(chkNHSO8.Checked), Session("patientid"))

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Service_Order", "บันทึกแบบคัดกรองการใช้ยาปฏิชีวนะในโรคติดเชื้อทางเดินหายใจส่วนบน (URIs) อย่างสมเหตุผล (F13) :" & Session("patientname"), "F13")
        End If

        Response.Redirect("ResultPage.aspx?p=F13")

        ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย")
    End Sub
    Function GetDiagnosisResult() As String
        Dim result As String = ""
        If chkC1.Checked Then
            result &= "C1|"
        End If
        If chkC2.Checked Then
            result &= "C2|"
        End If
        If chkC3.Checked Then
            result &= "C3|"
        End If
        If chkC4.Checked Then
            result &= "C4|"
        End If
        If chkC5.Checked Then
            result &= "C5|"
        End If
        If chkC6.Checked Then
            result &= "C6|"
        End If
        If chkC7.Checked Then
            result &= "C7|"
        End If
        If chkC8.Checked Then
            result &= "C8|"
        End If
        If chkC9.Checked Then
            result &= "C9|"
        End If

        If chkP1.Checked Then
            result &= "P1|"
        End If
        If chkP2.Checked Then
            result &= "P2|"
        End If
        If chkP3.Checked Then
            result &= "P3|"
        End If
        If chkP4.Checked Then
            result &= "P4|"
        End If
        If chkP5.Checked Then
            result &= "P5|"
        End If
        If chkP6.Checked Then
            result &= "P6|"
        End If
        If chkP7.Checked Then
            result &= "P7|"
        End If


        If chkC8v.Checked Then
            result &= "C8V|"
        End If
        If chkC9b.Checked Then
            result &= "C9B|"
        End If

        If chkC10.Checked Then
            result &= "C10|"
        End If
        If chkC11.Checked Then
            result &= "C11|"
        End If


        If chkV1.Checked Then
            result &= "V1|"
        End If
        If chkV2.Checked Then
            result &= "V2|"
        End If
        If chkV3.Checked Then
            result &= "V3|"
        End If
        If chkV4.Checked Then
            result &= "V4|"
        End If
        If chkV5.Checked Then
            result &= "V5|"
        End If

        If chkB1.Checked Then
            result &= "B1|"
        End If
        If chkB2.Checked Then
            result &= "B2|"
        End If
        If chkB3.Checked Then
            result &= "B3|"
        End If
        If chkB3_1.Checked Then
            result &= "B3.1|"
        End If
        If chkB3_2.Checked Then
            result &= "B3.2|"
        End If

        If chkB4.Checked Then
            result &= "B4|"
        End If


        If chkO1.Checked Then
            result &= "O1|"
        End If
        If chkO2.Checked Then
            result &= "O2|"
        End If
        If chkO3.Checked Then
            result &= "O3|"
        End If
        If chkO4.Checked Then
            result &= "O4|"
        End If

        If chkV6.Checked Then
            result &= "V6|"
        End If
        If chkV7.Checked Then
            result &= "V7|"
        End If

        If chkB5.Checked Then
            result &= "B5|"
        End If
        If chkB6.Checked Then
            result &= "B6|"
        End If
        If chkB7.Checked Then
            result &= "B7|"
        End If

        If chkO5_1.Checked Then
            result &= "O5.1|"
        End If
        If chkO5_2.Checked Then
            result &= "O5.2|"
        End If
        If chkO5_3.Checked Then
            result &= "O5.3|"
        End If
        If chkO5_4.Checked Then
            result &= "O5.4|"
        End If
        If chkO5_5.Checked Then
            result &= "O5.5|"
        End If

        If chkO5_6.Checked Then
            result &= "O5.6|"
        End If

        If chkO5_2a.Checked Then
            result &= "O5.2A|"
        End If
        If chkO5_3a.Checked Then
            result &= "O5.3A"
        End If

        If Right(result, 1) = "|" Then
            result = Left(result, Len(result) - 1)
        End If

        Return result
    End Function

    'Private Sub LoadPersonalData()
    '    dt = ctlOrder.GetCustomer_ByName(txtFName.Text, txtLName.Text)
    '    If dt.Rows.Count > 0 Then
    '        With dt.Rows(0)
    '            optGender.SelectedValue = DBNull2Str(.Item("Gender"))
    '            txtAges.Text = DBNull2Str(.Item("Ages"))
    '            txtCardID.Text = DBNull2Str(.Item("CardID"))
    '            txtBirthDate.Text = DisplayStr2ShortDateTH(DBNull2Str(.Item("BirthDate")))
    '            txtTelephone.Text = DBNull2Str(.Item("Telephone"))
    '            txtMobile.Text = DBNull2Str(.Item("Mobile"))
    '            optAddressType.SelectedValue = DBNull2Str(.Item("AddressType"))
    '            txtAddress.Text = DBNull2Str(.Item("AddressNo"))
    '            txtRoad.Text = DBNull2Str(.Item("Road"))
    '            txtDistrict.Text = DBNull2Str(.Item("District"))
    '            txtCity.Text = DBNull2Str(.Item("City"))
    '            ddlProvince.SelectedValue = DBNull2Str(.Item("ProvinceID"))
    '            optClaim.SelectedValue = DBNull2Str(.Item("MainClaim"))

    '        End With
    '    Else
    '        ClearData()
    '    End If

    'End Sub

    Protected Sub optFollowup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optFollowup.SelectedIndexChanged
        If optFollowup.SelectedValue = "2" Then
            optPlace.Enabled = True
            txtHospitalName.Enabled = True
        Else
            optPlace.Enabled = False
            txtHospitalName.Enabled = False
        End If
    End Sub
End Class