﻿Imports System.Data
Imports System.Data.SqlClient
Public Class FormEdit
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlType As New ServiceTypeController
    Dim objUser As New UserController
    Dim ctlOrder As New OrderController
    Dim ctlSmk As New SmokingController
    Dim svTypeID As String
    '  Dim svSeqNo As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then

            lblNo.Visible = False
         
            LoadProvinceToDDL()

            LoadActivityTypeToDDL()

            grdData.PageIndex = 0
          
            LoadActivityListToGrid()

            If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
                grdData.Columns(2).Visible = False
                lblProv.Visible = False
                ddlProvinceID.Visible = False

            Else
                grdData.Columns(2).Visible = True
                lblProv.Visible = True
                ddlProvinceID.Visible = True

            End If
        End If
    End Sub
    Protected Function DateText(ByVal input As Date) As String
        Dim dStr As String
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/0544", "", input.ToString("dd/MM/yyyy"))
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/0001", "", input.ToString("dd/MM/yyyy"))
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/0544", "", input.ToString("dd/MM/yyyy"))
        dStr = IIf(input.ToString("dd/MM/yyyy") = "01/01/2443", "", input.ToString("dd/MM/yyyy"))
        Return dStr
    End Function
    Private Sub LoadProvinceToDDL()

        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
            dt = ctlOrder.Province_GetInGroup(Request.Cookies("RPTGRP").Value)
        Else
            dt = ctlOrder.Province_GetInLocation
        End If


        ddlProvinceID.Items.Clear()
        If dt.Rows.Count > 0 Then
            With ddlProvinceID
                .Enabled = True
                .Items.Add("---ทั้งหมด---")
                If Convert.ToInt32(Request.Cookies("RoleID").Value) = isReportViewerAccess And Request.Cookies("RPTGRP").Value <> "ALL" Then
                    .Items(0).Value = Request.Cookies("RPTGRP").Value
                Else
                    .Items(0).Value = "0"
                End If

                For i = 0 To dt.Rows.Count - 1
                    .Items.Add(dt.Rows(i)("ProvinceName"))
                    .Items(i + 1).Value = dt.Rows(i)("ProvinceID")
                Next
                .SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadActivityListToGrid()

        dt = ctlOrder.Smoking_Get4Edit("", ddlActivity.SelectedValue, Trim(txtSearch.Text), ddlProvinceID.SelectedValue, CInt(ddlYear.SelectedValue))

        If dt.Rows.Count > 0 Then
            lblCount.Text = dt.Rows.Count
            lblNo.Visible = False
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()

                Try

                    Dim nrow As Integer = dt.Rows.Count
                    Dim n05Count As Integer = 0

                    If .PageCount > 1 Then
                        If .PageIndex > 0 Then
                            If (.PageSize) < nrow - (.PageSize * .PageIndex) Then
                                For i = 0 To .PageSize - 1
                                    ' .Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                    .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows((.PageSize * .PageIndex) + i)("ServiceDate"))
                                    .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows((.PageSize * .PageIndex) + i)("Gender"))

        
                                     


                                Next
                            Else
                                For i = 0 To nrow - (.PageSize * .PageIndex) - 1
                                    '.Rows(i).Cells(0).Text = (.PageSize * .PageIndex) + (i + 1)
                                    .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows((.PageSize * .PageIndex) + i)("ServiceDate"))
                                    .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows((.PageSize * .PageIndex) + i)("Gender"))

                                Next
                            End If
                        Else
                            For i = 0 To .PageSize - 1
                                ' .Rows(i).Cells(0).Text = i + 1
                                .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows(i)("ServiceDate"))
                                .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows(i)("Gender"))


                            Next
                        End If
                    Else
                        For i = 0 To nrow - 1
                            ' .Rows(i).Cells(0).Text = i + 1
                            .Rows(i).Cells(1).Text = DisplayStr2ShortDateTH(dt.Rows(i)("ServiceDate"))
                            .Rows(i).Cells(5).Text = DisplayGenderName(dt.Rows(i)("Gender"))

                        Next
                    End If
                Catch ex As Exception
                    'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','" & ex.Message & "');", True)
                    DisplayMessage(Me.Page, ex.Message)
                End Try

            End With
        Else
            lblCount.Text = 0
            lblNo.Visible = True
            grdData.Visible = False
            grdData.DataSource = Nothing
        End If

        dt = Nothing
    End Sub
    Private Sub LoadActivityTypeToDDL()

        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            dt = ctlType.ServiceType_GetByLocationID(Request.Cookies("LocationID").Value)
        ElseIf Convert.ToInt32(Request.Cookies("RoleID").Value) = isProjectManager Then
            dt = ctlType.ServiceType_GetByProjectID(Request.Cookies("PRJMNG").Value)
        Else
            dt = ctlType.ServiceType_GetAll()
        End If

        If dt.Rows.Count > 0 Then
            ddlActivity.Items.Clear()
            ddlActivity.Items.Add("---ทั้งหมด---")
            ddlActivity.Items(0).Value = "0"
            For i = 0 To dt.Rows.Count - 1
                With ddlActivity
                    .Items.Add("" & dt.Rows(i)("ServiceTypeID") & " : " & dt.Rows(i)("ServiceName"))
                    .Items(i + 1).Value = dt.Rows(i)("ServiceTypeID")
                End With
            Next
        End If
        dt = Nothing
    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadActivityListToGrid()
    End Sub

    Private Sub grdData_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand

        Dim sArg(1), sStr() As String
        Dim iSID As Long
        Dim iPID As Integer
        sArg(0) = ""
        sArg(1) = ""
        sStr = Split(DBNull2Str(e.CommandArgument), "|")
        For i = 0 To sStr.Length - 1
            sArg(i) = sStr(i)
        Next
        iSID = StrNull2Long(sArg(0))
        iPID = StrNull2Zero(sArg(1))
        Session("patientid") = ctlOrder.ServiceOrder_GetPatientIDByItemID(iSID, iPID)

        If TypeOf e.CommandSource Is WebControls.LinkButton Then
            Dim ButtonPressed As WebControls.LinkButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "lnkEdu1"
                    Response.Redirect("F02.aspx?t=new&seq=1&fid=" & iSID)
                Case "lnkEdu2"
                    Response.Redirect("F03.aspx?t=new&seq=2&fid=" & iSID)
                Case "lnkFollow"
                    Response.Redirect("F11_Follow.aspx?t=new&fid=" & iSID)
                Case "lnkA04"
                    Response.Redirect("A4.aspx?t=new&fid=" & iSID)
                Case "lnkA05"
                    Response.Redirect("A5.aspx?t=new&fid=" & iSID)
            End Select
        End If
        svTypeID = ctlOrder.Order_GetTypeIDByItemID(iSID, iPID)
        'svSeqNo = ctlOrder.Order_GetSEQByItemID(isid)

        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"

                    Select Case svTypeID
                        Case FORM_TYPE_ID_F11F
                            Response.Redirect("F11_Follow.aspx?t=edit&fid=" & iSID)
                        Case FORM_TYPE_ID_F02
                            Response.Redirect("F02.aspx?t=edit&fid=" & iSID)
                        Case FORM_TYPE_ID_F03
                            Response.Redirect("F03.aspx?t=edit&fid=" & iSID)
                        Case FORM_TYPE_ID_A4F
                            Response.Redirect("A4.aspx?t=edit&fid=" & iSID)
                        Case FORM_TYPE_ID_A05
                            Response.Redirect("A5.aspx?t=edit&fid=" & iSID)
                        Case Else
                            Response.Redirect("" & svTypeID & ".aspx?t=edit&fid=" & iSID)
                    End Select

                Case "imgDel"
                    'Dim RID As Integer

                    'Select Case svTypeID
                    '    Case FORM_TYPE_ID_F01
                    '        ctlOrder.Order_F01DeleteByRefID(iSID)
                    '    Case FORM_TYPE_ID_F02
                    '        ctlOrder.F01_UpdateEducateCountByDelete(iSID, 0, Request.Cookies("username").Value)
                    '        ctlOrder.Order_F02DeleteByRefID(iSID)
                    '    Case FORM_TYPE_ID_F03
                    '        ctlOrder.F01_UpdateEducateCountByDelete(iSID, 1, Request.Cookies("username").Value)
                    '    Case FORM_TYPE_ID_F11F
                    '        RID = ctlOrder.F11_GetRefID(iSID)
                    '        ctlOrder.F11_UpdateFollowStatus(RID, 0, Request.Cookies("username").Value)
                    'End Select

                    If Left(svTypeID, 1) = "A" Then
                        ctlSmk.Smoking_Order_DeleteByUID(iSID)
                        objUser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_DEL, "Smoking", "ลบรายการกิจกรรมเฉพาะ UID :" & svTypeID, iSID)
                        'Else
                        '    ctlOrder.Order_Delete(iSID)
                        '    objUser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_DEL, "ServiceOrder", "ลบรายการกิจกรรมเฉพาะ UID :" & svTypeID, iSID)
                    End If


                    DisplayMessage(Me.Page, "ลบข้อมูลเรียบร้อย")
                    'ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ลบข้อมูลเรียบร้อย');", True)
                    grdData.PageIndex = 0
                    LoadActivityListToGrid()
                    'Else
                    '  ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ไม่สามารถลบข้อมูลได้ กรุณาตรวจสอบและลองใหม่อีกครั้ง');", True)
                    'End If
            End Select
        End If

    End Sub

    Private Sub grdData_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            'e.Row.Cells(0).Text = e.Row.RowIndex + 1
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(9).FindControl("imgDel")
            imgD.Attributes.Add("onClick", scriptString)

        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If

    End Sub


    Protected Sub cmdFind_Click(sender As Object, e As EventArgs) Handles cmdFind.Click
        grdData.PageIndex = 0
        LoadActivityListToGrid()

    End Sub

End Class

