﻿Public Class SiteCPA
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ctlS As New SystemConfigController

        If ctlS.SystemConfig_GetByCode("ALIVE") = "N" Then
            Response.Redirect("404.aspx")
        End If

        If Not IsPostBack Then
            lblVersion.Text = ctlS.SystemConfig_GetByCode("Version")
        End If
        hlnkUserName.Text = Request.Cookies("NameOfUser").Value
        If Convert.ToInt32(Request.Cookies("RoleID").Value) = isShopAccess Then
            hlnkUserName.NavigateUrl = "LocationsEdit.aspx?id=" & Request.Cookies("LocationID").Value
        Else
            hlnkUserName.NavigateUrl = "#"
        End If

    End Sub

End Class