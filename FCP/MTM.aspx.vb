﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Subgurim.Controles
Public Class MTM
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlL As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlMTM As New MTMController
    'Dim ctlICD As New ICDController
    Dim ctlMed As New DrugController
    Dim ctlD As New DeseaseController
    Dim sAlert As String
    Dim isValid As Boolean


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If Session("patientid") Is Nothing Then
            Response.Redirect("PatientSearch.aspx")
        End If

        If Not IsPostBack Then
            isAdd = True
            'txtCGNo.Visible = False
            'lblCGday.Visible = False
            'txtCGYear.Visible = False
            'lblCGYear.Visible = False
            'lblCgType.Visible = False
            'optCigarette.Visible = False
            'lblAlcohol.Visible = False
            'txtAlcoholFQ.Visible = False
            pnEating.Visible = False
            pnFat.Visible = False
            txtBegin.Enabled = True
            txtEnd.Enabled = True

            pnRefer.Visible = False
            pnDrugRefill.Visible = True

            pnDRP_Warning.Visible = False
            pnDRP_Success.Visible = False
            pnAlertB.Visible = False
            pnB_Success.Visible = False
            pnReferAlert.Visible = False
            pnDrugRemainAlert.Visible = False
            pnDrugRefillAlert.Visible = False

            cmdSaveDrug.Visible = False

            If Request("acttype") = "view" Then
                cmdSave.Visible = False
            Else
                cmdSave.Visible = True
            End If

            LoadDrugRemainReasonToDDL()


            txtServiceDate.Text = DisplayShortDateTH(ctlL.GET_DATE_SERVER)
            'txtBYear.Text = Right(txtServiceDate.Text, 4)

            'If StrNull2Zero(txtBYear.Text) < 2500 Then
            '    txtBYear.Text = StrNull2Zero(txtBYear.Text) + 543
            'Else
            '    txtBYear.Text = StrNull2Zero(txtBYear.Text)
            'End If

            LoadProblemGroup()
            'LoadDeseaseToDDL()
            LoadDrugToDDL()


            LoadLocationData(Request.Cookies("LocationID").Value)
            LoadPharmacist(Request.Cookies("LocationID").Value)
            LoadProblemRelate()

            If Request("t") = "new" Or (Request("t") Is Nothing) Then

                pnMTM.Visible = False
                optMTMType.SelectedIndex = 0
                'BindPatientFromList()
                lblSEQ.Text = (ctlMTM.MTM_Master_GetLastSEQ(Request.Cookies("LocationID").Value, Session("patientid"), optMTMType.SelectedValue) + 1).ToString
            Else
                If Not Request("fid") Is Nothing Then
                    LoadMTMHeader()
                End If
            End If

            LoadLabResultToGrid()
            LoadDrugUse()
            LoadHospital()
            LoadHospitalRefer()
        End If

        'If (FileUploaderAJAX1.IsPosting) Then
        '    ServiceDate = CLng(ConvertDate2DBString(Today.Date))
        '    'F01_sFile = Path.GetExtension(FileUploaderAJAX1.PostedFile.FileName)
        '    F01_DocFile = "Refer" & "_" & lblLocationID.Text & "_" & ServiceDate & "_" & ConvertTimeToString(Now())
        '    UploadFile(F01_DocFile)
        'End If


        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        ''txtCardID.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")

        Dim scriptString As String = "javascript:return confirm(""คุณแน่ใจที่จะยกเลิกการส่งต่อผู้รับบริการรายนี้ใช่หรือไม่?"");"
        cmdCancelRefer.Attributes.Add("onClick", scriptString)


    End Sub

    Private Sub LoadDrugToDDL()
        Dim dtM As New DataTable
        Dim ctlD As New DrugController

        dtM = ctlD.Drug_Get
        If dtM.Rows.Count > 0 Then
            ddlDrug.DataSource = dtM
            ddlDrug.DataTextField = "Name"
            ddlDrug.DataValueField = "UID"
            ddlDrug.DataBind()

            ddlDrugUse.DataSource = dtM
            ddlDrugUse.DataTextField = "Name"
            ddlDrugUse.DataValueField = "UID"
            ddlDrugUse.DataBind()

            ddlDrugRefill.DataSource = dtM
            ddlDrugRefill.DataTextField = "Name"
            ddlDrugRefill.DataValueField = "UID"
            ddlDrugRefill.DataBind()

            ddlDrugRemain.DataSource = dtM
            ddlDrugRemain.DataTextField = "Name"
            ddlDrugRemain.DataValueField = "UID"
            ddlDrugRemain.DataBind()
        End If

        dtM = Nothing
    End Sub
    'Private Sub LoadDeseaseToDDL()
    '    ddlDesease.Items.Clear()
    '    Dim dtD As New DataTable
    '    Dim ctlD As New DeseaseController

    '    dtD = ctlD.Desease_Get

    '    If dtD.Rows.Count > 0 Then
    '        ddlDesease.Items.Clear()

    '        With ddlDesease
    '            .DataSource = dtD
    '            .DataTextField = "Name"
    '            .DataValueField = "UID"
    '            .DataBind()
    '        End With

    '    End If

    'End Sub


    Private Sub BindFinalResultToRadioList()
        If chkNotFollow.Checked Then
            'optFat.Enabled = False
            optResultB.Enabled = False
            txtResultOtherB.Enabled = False
            txtBegin.Enabled = False
            txtEnd.Enabled = False
        Else
            optFat.Enabled = True
            optResultB.Enabled = True
            txtResultOtherB.Enabled = True
            txtBegin.Enabled = True
            txtEnd.Enabled = True
            optResultB.Items.Clear()
            pnFat.Visible = False
            pnEating.Visible = False
            Select Case ddlBehavior.SelectedValue
                Case "1" 'การสูบบุหรี่
                    optResultB.Items.Add("สูบลดลง")
                    optResultB.Items.Add("สูบเท่าเดิม")
                    optResultB.Items.Add("สูบเพิ่มขึ้น")
                    optResultB.Items.Add("เลิกสูบแล้ว")
                    optResultB.Items.Add("อื่นๆ")
                Case "2" 'การดื่มเหล้า
                    optResultB.Items.Add("ดื่มลดลง")
                    optResultB.Items.Add("ดื่มเพิ่มขึ้น")
                    optResultB.Items.Add("คงเดิม")
                    optResultB.Items.Add("อื่นๆ")
                Case "3" 'ความอ้วน
                    pnFat.Visible = True
                    optResultB.Items.Add("ลดลง")
                    optResultB.Items.Add("เพิ่มขึ้น")
                    optResultB.Items.Add("คงเดิม")
                    optResultB.Items.Add("อื่นๆ")
                Case "4" 'การออกกำลังกาย
                    optResultB.Items.Add("ไม่ได้ออกกำลังกายเพิ่ม(เหมือนเดิม)")
                    optResultB.Items.Add("ออกกำลังกายเพิ่มขึ้น")
                    optResultB.Items.Add("ออกกำลังกายลดลง")
                    optResultB.Items.Add("จำนวนครั้งเท่าเดิม แต่เพิ่มเวลาในแต่ละครั้ง")
                    optResultB.Items.Add("อื่นๆ")
                Case "5" 'การรับประทานอาหาร
                    pnEating.Visible = True
                    optResultB.Items.Add("ดีขึ้น")
                    optResultB.Items.Add("แย่ลง")
                    optResultB.Items.Add("เท่าเดิม")
                    optResultB.Items.Add("อื่นๆ")

                    txtBegin.Enabled = False
                    txtEnd.Enabled = False
                Case "6" 'ความเครียด
                    optResultB.Items.Add("ลดลง")
                    optResultB.Items.Add("เพิ่มขึ้น")
                    optResultB.Items.Add("คงเดิม")
                    optResultB.Items.Add("อื่นๆ")
                Case Else
                    optResultB.Items.Add("ลดลง")
                    optResultB.Items.Add("เพิ่มขึ้น")
                    optResultB.Items.Add("คงเดิม")
                    optResultB.Items.Add("อื่นๆ")
            End Select
        End If
        'LoadDataToComboBox()
    End Sub



    Private Sub LoadPharmacist(LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With

                'With ddlPharmacist_D
                '    .Visible = True
                '    For i = 0 To dtP.Rows.Count - 1
                '        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                '        .Items(i).Value = dtP.Rows(i)("PersonID")
                '    Next
                '    .SelectedIndex = 0
                'End With

                'With ddlPharmacistB
                '    .Visible = True
                '    For i = 0 To dtP.Rows.Count - 1
                '        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                '        .Items(i).Value = dtP.Rows(i)("PersonID")
                '    Next
                '    .SelectedIndex = 0
                'End With

            End If
        End If
        dtP = Nothing
    End Sub
    Private Sub LoadProblemRelate()
        Dim dtPG As New DataTable
        Dim ctlP As New ProblemController
        ddlBehavior.Items.Clear()
        dtPG = ctlP.BehaviorProblem_Get
        If dtPG.Rows.Count > 0 Then
            ddlBehavior.DataSource = dtPG
            ddlBehavior.DataTextField = "Descriptions"
            ddlBehavior.DataValueField = "UID"
            ddlBehavior.DataBind()
        End If
        dtPG = Nothing
    End Sub

    Private Sub LoadProblemGroup()
        Dim dtPG As New DataTable
        Dim ctlP As New ProblemController
        cboProblemGroup.Items.Clear()
        dtPG = ctlP.ProblemGroup_Get
        If dtPG.Rows.Count > 0 Then
            cboProblemGroup.DataSource = dtPG
            cboProblemGroup.DataTextField = "Descriptions"
            cboProblemGroup.DataValueField = "Code"
            cboProblemGroup.DataBind()
            cboProblemGroup.SelectedIndex = 0
            LoadProblem()
        End If

        dtPG = Nothing
    End Sub
    Private Sub LoadProblem()
        Dim dtP As New DataTable
        Dim ctlP As New ProblemController
        cboProblemItem.Items.Clear()
        dtP = ctlP.ProblemItem_Get(cboProblemGroup.SelectedValue)
        If dtP.Rows.Count > 0 Then
            cboProblemItem.DataSource = dtP
            cboProblemItem.DataTextField = "Descriptions"
            cboProblemItem.DataValueField = "Code"
            cboProblemItem.DataBind()
        End If
        dtP = Nothing
    End Sub

    Private Sub LoadLocationData(LocationID As String)
        dt = ctlL.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationCode.Text = DBNull2Str(.Item("LocationCode"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("ProvinceName"))
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadMTMHerb()
        Dim dtH As New DataTable

        dtH = ctlMTM.MTM_Master_GetHerbLastInfo(StrNull2Zero(Session("patientid")))
        If dtH.Rows.Count > 0 Then
            With dtH.Rows(0)
                txtDrug1.Text = String.Concat(.Item("MedicationUsed1"))
                txtDrug2.Text = String.Concat(.Item("MedicationUsed2"))
                txtDrug3.Text = String.Concat(.Item("MedicationUsed3"))
                txtDrug4.Text = String.Concat(.Item("MedicationUsed4"))
            End With
        End If

    End Sub

    Private Sub LoadMTMHeader()
        'Dim BYear As Integer = 0

        'BYear = ctlMTM.MTM_Master_GetBYear(StrNull2Zero(Request("fid")))

        If Request("t") = "New" Then
            dt = ctlMTM.MTM_Master_GetLastInfo(Session("patientid"))
        Else
            dt = ctlMTM.MTM_Master_GetByUID(StrNull2Zero(Request("fid")))
        End If


        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                isAdd = False
                cmdSaveDrug.Visible = True

                pnMTM.Visible = True
                'txtBYear.Text = .Item("BYear")
                lblMTMUID.Text = .Item("UID")
                lblSEQ.Text = DBNull2Str(.Item("SEQ"))

                optMTMType.SelectedValue = DBNull2Str(.Item("MTMTYPE"))
                'BindPatientFromList()

                optPatientFrom.SelectedValue = DBNull2Str(.Item("PFROM"))
                txtFromRemark.Text = DBNull2Str(.Item("FROMTXT"))

                optService.SelectedValue = DBNull2Str(.Item("MTMService"))
                txtServiceRemark.Text = DBNull2Str(.Item("ServiceRemark"))

                If DBNull2Str(.Item("MTMService")) = "2" Then
                    optTelepharm.SelectedValue = DBNull2Str(.Item("ServiceRef"))
                Else
                    optTelepharm.ClearSelection()
                End If

                optTelepharmacyMethod.SelectedValue = DBNull2Str(.Item("TelepharmacyMethod"))
                txtRecordMethod.Text = DBNull2Str(.Item("RecordMethod"))
                txtRecordLocation.Text = DBNull2Str(.Item("RecordLocation"))

                txtTelepharmacyRemark.Text = DBNull2Str(.Item("TelepharmacyRemark"))

                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                'lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                'lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                txtTime.Text = String.Concat(.Item("ServiceTime"))

                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))


                'optSmoke.SelectedValue = String.Concat(.Item("Smoke"))
                'txtCGYear.Text = String.Concat(.Item("SmokeYear"))
                'txtCGNo.Text = String.Concat(.Item("SmokeCigarette"))
                'optCigarette.SelectedValue = String.Concat(.Item("CigaretteType"))
                'optAlcohol.SelectedValue = String.Concat(.Item("Alcohol"))
                'txtAlcoholFQ.Text = String.Concat(.Item("AlcoholFQ"))

                LoadMTMHerb()

                'txtDrug1.Text = String.Concat(.Item("MedicationUsed1"))
                'txtDrug2.Text = String.Concat(.Item("MedicationUsed2"))
                'txtDrug3.Text = String.Concat(.Item("MedicationUsed3"))
                'txtDrug4.Text = String.Concat(.Item("MedicationUsed4"))
                'txtDrug5.Text = String.Concat(.Item("MedicationUsed5"))
                'txtMed6.Text = String.Concat(.Item("MedicationUsed6"))
                'txtMed7.Text = String.Concat(.Item("MedicationUsed7"))



                chkPitting.Checked = ConvertYesNo2Boolean(String.Concat(.Item("isPitting")))
                chkWound.Checked = ConvertYesNo2Boolean(String.Concat(.Item("isWound")))
                chkPeripheral.Checked = ConvertYesNo2Boolean(String.Concat(.Item("isPeripheral")))

                txtPitting.Text = String.Concat(.Item("Pitting"))
                txtWound.Text = String.Concat(.Item("Wound"))
                txtPeripheral.Text = String.Concat(.Item("Peripheral"))


                'optMedFrom.SelectedValue = String.Concat(.Item("HospitalType"))
                'txtHospital.Text = String.Concat(.Item("HospitalName"))
                chkStatus.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("Status")))

                If DBNull2Str(.Item("ReferStatus")) = "Y" Then
                    LoadReferDetail()
                End If

                If Request("t") = "New" Then
                    lblMTMUID.Text = ""
                    txtServiceDate.Text = DisplayShortDateTH(ctlL.GET_DATE_SERVER)
                    txtTime.Text = ""
                    ddlPerson.SelectedIndex = 0
                    pnMTM.Visible = False
                Else
                    pnMTM.Visible = True
                    LoadDrugProblemToGrid()
                    LoadDrugRemainToGrid()
                    LoadDrugRefillToGrid()
                    LoadDeseaseRelateToGrid()
                    LoadBehaviorProblemToGrid()
                End If

            End With
        Else
            cmdSaveDrug.Visible = False
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing

    End Sub

    Private Function ValidateData() As Boolean
        sAlert = ""
        isValid = True

        If ddlDrug.Text = "" Then
            sAlert &= "- กรุณาเลือกรายการยาก่อน <br/>"
            isValid = False
        End If

        If DBNull2Str(cboProblemGroup.SelectedValue) = "" Then
            sAlert &= "- กรุณาเลือกปัญหาก่อน <br/>"
            isValid = False
        End If

        If cboProblemItem.Text = "" Then
            sAlert &= "- กรุณาเลือกปัญหาย่อยก่อน <br/>"
            isValid = False
        End If
        If txtInterventionDrug.Text = "" Then
            sAlert &= "- Intervention <br/>"
            isValid = False
        End If
        Return isValid
    End Function
    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        If txtServiceDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนวันที่ให้บริการก่อน');", True)
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนวันที่ให้บริการก่อน');", True)
            Exit Sub
        End If
        If StrNull2Zero(txtTime.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนระยะเวลาในการให้บริการก่อน');", True)
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนระยะเวลาในการให้บริการก่อน');", True)
            Exit Sub
        End If

        If optMTMType.SelectedIndex = -1 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทของการให้บริการก่อน');", True)
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทของการให้บริการก่อน ")
            Exit Sub
        End If

        If optPatientFrom.SelectedIndex = -1 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทแหล่งที่มาก่อน');", True)
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทแหล่งที่มาก่อน ")
            Exit Sub
        End If

        If optPatientFrom.SelectedValue = "9" And txtFromRemark.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุประเภทแหล่งที่มาก่อน');", True)
            Exit Sub
        End If


        If optService.SelectedIndex = -1 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกการให้บริการมาก่อน');", True)
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกการให้บริการมาก่อน ")
            Exit Sub
        End If


        If optService.SelectedIndex = 1 Then
            If optTelepharm.SelectedIndex = -1 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทการให้บริการ Telepharmacy ก่อน ');", True)
                Exit Sub
            Else
                If optTelepharm.SelectedValue <> "1" Then
                    If txtTelepharmacyRemark.Text.Trim() = "" Then
                        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุชื่อ รพ. หรือ ชื่อโครงการอื่นๆ กรณี Telepharmacy ก่อน');", True)
                        Exit Sub
                    End If
                End If
            End If
        End If




        '-----------------------------------------------------
        If optPatientFrom.SelectedValue = "9" Then
            If txtFromRemark.Text.Trim() = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุแหล่งที่มาก่อน');", True)
                Exit Sub
            End If

        End If

        If optService.SelectedValue = "9" Then
            If txtServiceRemark.Text.Trim() = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุการให้บริการก่อน');", True)
                Exit Sub
            End If
        End If

        If optService.SelectedIndex = 1 Then 'case Telepharmacy
            If optTelepharm.SelectedIndex = -1 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทการให้บริการ Telepharmacy ก่อน');", True)
                Exit Sub
            End If

            If optTelepharmacyMethod.SelectedIndex = -1 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกวิธีการให้บริการ Telepharmacy ก่อน');", True)
                Exit Sub
            End If

            If optTelepharmacyMethod.SelectedIndex <> 0 Then 'case Telepharmacy
                If txtRecordMethod.Text.Trim() = "" Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุวิธีการบันทึก');", True)
                    Exit Sub
                End If
                If txtRecordLocation.Text.Trim() = "" Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุสถานที่เก็บข้อมูล');", True)
                    Exit Sub
                End If
            End If
        End If

        '------------------------------------------------------

        Dim objuser As New UserController
        Dim ServiceDate As Long
        ServiceDate = CLng(ConvertStrDate2DBString(txtServiceDate.Text))

        If lblMTMUID.Text = "" Then 'Add new

            If ctlMTM.MTM_Master_ChkDupCustomer(Session("patientid"), ServiceDate) = True Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านได้บันทึกการให้บริการของผู้รับบริการนี้ ในวันที่ท่านระบุแล้ว');", True)
                ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านได้บันทึกการให้บริการของผู้รับบริการนี้ ในวันที่ท่านระบุแล้ว")
                'LoadDataToComboBox()
                Exit Sub
            End If

            ctlMTM.MTM_Master_Add(lblLocationID.Text, Session("patientid"), ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), 0, 0, 0, 0, 0, 0, txtDrug1.Text, txtDrug2.Text, txtDrug3.Text, txtDrug4.Text, "", "", "", 0, "", Convert2Status(chkStatus.Checked), Request.Cookies("username").Value, StrNull2Zero(lblSEQ.Text), optMTMType.SelectedValue, optPatientFrom.SelectedValue, txtFromRemark.Text, optService.SelectedValue, txtServiceRemark.Text, optTelepharm.SelectedValue, optTelepharmacyMethod.SelectedValue, txtRecordMethod.Text, txtRecordLocation.Text, txtTelepharmacyRemark.Text)

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "MTM", "เพิ่มแบบฟอร์มMTM  การดูแลเรื่องการใช้ยา :  <<PatientID:" & Session("patientid") & ">>", "MTM")

            lblMTMUID.Text = ctlMTM.MTM_Master_GetLastUID(lblLocationID.Text, Session("patientid"), ServiceDate).ToString()

            LoadProblemGroup()

            pnMTM.Visible = True

            Response.Redirect("MTM.aspx?t=edit&fid=" & lblMTMUID.Text)
        Else
            ctlMTM.MTM_Master_Update(StrNull2Zero(lblMTMUID.Text), lblLocationID.Text, Session("patientid"), ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), 0, 0, 0, 0, 0, 0, txtDrug1.Text, txtDrug2.Text, txtDrug3.Text, txtDrug4.Text, "", "", "", 0, "", Convert2Status(chkStatus.Checked), Request.Cookies("username").Value, optMTMType.SelectedValue, optPatientFrom.SelectedValue, txtFromRemark.Text, optService.SelectedValue, txtServiceRemark.Text, optTelepharm.SelectedValue, optTelepharmacyMethod.SelectedValue, txtRecordMethod.Text, txtRecordLocation.Text, txtTelepharmacyRemark.Text)

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Service_MTM", "บันทึกเพิ่มแบบฟอร์มMTM  การดูแลเรื่องการใช้ยา :<<PatientID:" & Session("patientid") & ">>", "MTM")
        End If
        'Response.Redirect("ResultPage.aspx")   
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการทำงาน','บันทึกข้อมูลเรียบร้อย');", True)
        '' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย")
        'LoadDataToComboBox()


    End Sub
#Region "Edit Data"
    Private Sub EditDrugProblem(UID As Integer)

        LoadProblemGroup()

        dt = ctlMTM.MTM_DrugProblem_GetByUID(UID)
        If dt.Rows.Count > 0 Then
            lblUID_Drug.Text = dt.Rows(0)("UID")

            cboProblemGroup.SelectedValue = dt.Rows(0)("ProblemGroupUID")
            LoadProblem()
            cboProblemItem.SelectedValue = dt.Rows(0)("ProblemUID")
            txtInterventionDrug.Text = DBNull2Str(dt.Rows(0)("Interventions"))
            optResult_D.SelectedValue = DBNull2Str(dt.Rows(0)("FinalResult"))
            txtResultOther_D.Text = DBNull2Str(dt.Rows(0)("FinalResultOther"))
            txtProblemOtherDrug.Text = DBNull2Str(dt.Rows(0)("ProblemOther"))
            pnDRP_Warning.Visible = False
            pnDRP_Success.Visible = False
            cmdDrugProblemAdd.Text = "บันทึกการแก้ไข"

            'ddlDrug.SelectedValue = ctlMed.Drug_GetNameByUID(DBNull2Str(dt.Rows(0)("DrugUID")))

            ddlDrug.SelectedValue = dt.Rows(0)("DrugUID")

        End If
    End Sub
    Private Sub EditBehaviorProblem(UID As Integer)
        dt = ctlMTM.MTM_BehaviorProblem_GetByUID(UID)
        If dt.Rows.Count > 0 Then
            lblUID_B.Text = dt.Rows(0)("UID")
            ddlBehavior.SelectedValue = DBNull2Str(dt.Rows(0)("ProblemUID"))
            BindFinalResultToRadioList()

            txtProblemBehaviorOther.Text = DBNull2Str(dt.Rows(0)("ProblemOther"))

            txtInterventionB.Text = DBNull2Str(dt.Rows(0)("Interventions"))



            txtBegin.Text = DBNull2Str(dt.Rows(0)("ResultBegin"))
            txtEnd.Text = DBNull2Str(dt.Rows(0)("ResultEnd"))

            Select Case ddlBehavior.SelectedValue
                'Case "1" 'การสูบบุหรี่

                'Case "2" 'การดื่มเหล้า

                Case "3" 'ความอ้วน
                    pnFat.Visible = True
                    optFat.SelectedValue = DBNull2Str(dt.Rows(0)("FatFollow"))
                Case "4" 'การออกกำลังกาย 
                    txtResultOtherB.Text = DBNull2Str(dt.Rows(0)("Remark"))
                    txtBegin.Enabled = False
                    txtEnd.Enabled = False
                Case "5" 'การรับประทานอาหาร
                    pnEating.Visible = True
                    optTast.SelectedValue = DBNull2Str(dt.Rows(0)("TasteFollow"))
                    'Case "6" 'ความเครียด

            End Select

            If DBNull2Str(dt.Rows(0)("isFollow")) = "N" Then
                chkNotFollow.Checked = True
            Else
                optResultB.SelectedValue = DBNull2Str(dt.Rows(0)("FinalResult"))
                txtResultOtherB.Text = DBNull2Str(dt.Rows(0)("FinalResultOther"))
                chkNotFollow.Checked = False
            End If
            cmdBehaviorAdd.Text = "บันทึกการแก้ไข"

        End If
    End Sub
#End Region

    Protected Sub cboProblemGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboProblemGroup.SelectedIndexChanged
        LoadProblem()
    End Sub

#Region "Load Data to Grid"
    Private Sub LoadDeseaseRelateToGrid()
        dt = ctlMTM.MTM_DeseaseRelate_Get(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(Session("patientid")))
        grdDesease.DataSource = dt
        grdDesease.DataBind()
    End Sub
    Private Sub LoadDrugProblemToGrid()
        dt = ctlMTM.MTM_DrugProblem_Get(StrNull2Zero(Session("patientid")), Request.Cookies("UserID").Value)
        grdDrugProblem.DataSource = dt
        grdDrugProblem.DataBind()
    End Sub
    Private Sub LoadBehaviorProblemToGrid()
        dt = ctlMTM.MTM_BehaviorProblem_Get(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(Session("patientid")))
        grdBehavior.DataSource = dt
        grdBehavior.DataBind()
    End Sub
    Private Sub LoadLabResultToGrid()
        dt = ctlMTM.LabResult1_Get(StrNull2Zero(lblMTMUID.Text))
        grdLab.DataSource = dt
        grdLab.DataBind()

        dt = ctlMTM.LabResult2_Get(StrNull2Zero(lblMTMUID.Text))
        grdLab2.DataSource = dt
        grdLab2.DataBind()
    End Sub
#End Region
#Region "Grid Event"
    Protected Sub grdDrugProblem_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDrugProblem.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(7).FindControl("imgDel_D")
            imgD.Attributes.Add("onClick", scriptString)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub
    Private Sub grdDrugProblem_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDrugProblem.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit_D"
                    EditDrugProblem(e.CommandArgument())
                Case "imgDel_D"
                    ctlMTM.MTM_DrugProblem_Delete(StrNull2Zero(e.CommandArgument()))
                    LoadDrugProblemToGrid()
                    'LoadDataToComboBox()
            End Select
        End If
    End Sub
    Protected Sub grdDesease_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDesease.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(2).FindControl("imgDel_DS")
            imgD.Attributes.Add("onClick", scriptString)

            e.Row.Cells(0).Text = e.Row.RowIndex + 1
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub
    Protected Sub grdBehavior_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdBehavior.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(6).FindControl("imgDel_B")
            imgD.Attributes.Add("onClick", scriptString)
            'e.Row.Cells(0).Text = e.Row.RowIndex + 1
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub
    Private Sub grdDesease_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDesease.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel_DS"
                    ctlMTM.MTM_DeseaseRelate_Delete(e.CommandArgument())
                    LoadDeseaseRelateToGrid()
                    'LoadDataToComboBox()
            End Select
        End If
    End Sub
    Private Sub grdBehavior_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdBehavior.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit_B"
                    EditBehaviorProblem(e.CommandArgument())
                Case "imgDel_B"
                    ctlMTM.MTM_BehaviorProblem_Delete(e.CommandArgument())
                    ClearTextBehavior()
                    LoadBehaviorProblemToGrid()
                    'LoadDataToComboBox()
            End Select
        End If
    End Sub
#End Region

    'Dim strD() As String
    Protected Sub cmdDesease_Click(sender As Object, e As EventArgs) Handles cmdDesease.Click
        If txtDeseaseName.Text = "" Then
            '' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ระบุโรคที่ต้องการเพิ่มก่อน');", True)

            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ระบุโรคที่ต้องการเพิ่มก่อน');", True)
            'LoadDataToComboBox()
            Exit Sub
        End If

        'Dim strD() As String
        'strD = Split(txtICD10Code.Text, ":")

        'If Not ctlD.Desease_IsHas(strD(0).TrimEnd().ToUpper()) Then
        '    ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาตรวจสอบโรคที่เพิ่มก่อน');", True)
        '    LoadDataToComboBox()
        '    Exit Sub
        'End If


        ctlMTM.MTM_DeseaseRelate_Add2(StrNull2Zero(lblMTMUID.Text), FORM_TYPE_ID_MTM, txtDeseaseName.Text, txtDeseaseOther.Text, Session("patientid"), Request.Cookies("UserID").Value)
        txtDeseaseName.Text = ""
        txtDeseaseOther.Text = ""
        LoadDeseaseRelateToGrid()
        'LoadDataToComboBox()
    End Sub


    Protected Sub cmdAddBehavior_Click(sender As Object, e As EventArgs) Handles cmdBehaviorAdd.Click
        lblAlertB.Visible = True
        lblAlertB.Text = ""
        pnDRP_Warning.Visible = False
        pnDRP_Success.Visible = False
        pnB_Success.Visible = False
        pnAlertB.Visible = False

        If ddlBehavior.SelectedValue = "99" Then ' อื่นๆ
            If txtProblemBehaviorOther.Text.Trim = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุปัญหาเรื่องอื่นๆก่อน');", True)
                Exit Sub
            End If
        End If

        If chkNotFollow.Checked Then
            'optFat.Enabled = False
            optResultB.Enabled = False
            txtResultOtherB.Enabled = False
            txtBegin.Enabled = False
            txtEnd.Enabled = False
        Else
            optFat.Enabled = True
            optResultB.Enabled = True
            txtResultOtherB.Enabled = True
            txtBegin.Enabled = True
            txtEnd.Enabled = True
            'optResultB.Items.Clear()
            pnFat.Visible = False
            pnEating.Visible = False

            optResultB.Visible = True


            Select Case ddlBehavior.SelectedValue
                Case "1" 'การสูบบุหรี่

                Case "2" 'การดื่มเหล้า

                Case "3" 'ความอ้วน
                    pnFat.Visible = True
                    If optFat.SelectedIndex = -1 Then
                        lblAlertB.Text = "ท่านยังไม่ได้เลือกสิ่งที่พบ"
                        pnAlertB.Visible = True
                        Exit Sub
                    End If

                Case "4" 'การออกกำลังกาย

                Case "5" 'การรับประทานอาหาร
                    pnEating.Visible = True
                    If optTast.SelectedIndex = -1 Then
                        lblAlertB.Text = "ท่านยังไม่ได้เลือกรส"
                        pnAlertB.Visible = True
                        Exit Sub
                    End If




                Case "6" 'ความเครียด

                Case Else

            End Select

            If optResultB.SelectedIndex = -1 Then
                lblAlertB.Text = "ท่านยังไม่ได้เลือกผลการติดตาม"
                pnAlertB.Visible = True
                Exit Sub
            End If

        End If

        If txtInterventionB.Text = "" Then
            lblAlertB.Text = "ท่านยังไม่ได้ป้อน Interventions"
            pnAlertB.Visible = True

            Exit Sub
        End If

        Dim isFollow, ResultB, ResultOtherB, ResultBegin, ResultEnd As String
        If chkNotFollow.Checked Then ' not follow
            isFollow = "N"
            ResultB = ""
            ResultOtherB = ""
            ResultBegin = ""
            ResultEnd = ""
        Else
            isFollow = "Y"
            ResultB = optResultB.SelectedValue
            ResultOtherB = txtResultOtherB.Text
            ResultBegin = txtBegin.Text
            ResultEnd = txtEnd.Text
        End If

        ctlMTM.MTM_BehaviorProblem_Save(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(lblUID_B.Text), ddlBehavior.SelectedValue, txtProblemBehaviorOther.Text, txtInterventionB.Text, ResultB, ResultOtherB, ResultBegin, ResultEnd, optFat.SelectedValue, ResultOtherB, isFollow, Session("patientid"), Request.Cookies("UserID").Value, optTast.SelectedValue)

        ClearTextBehavior()

        LoadBehaviorProblemToGrid()
        'LoadDataToComboBox()

        pnAlertB.Visible = False
        pnB_Success.Visible = True

    End Sub
    Private Sub ClearTextBehavior()
        lblUID_B.Text = "0"
        ddlBehavior.SelectedIndex = 0
        BindFinalResultToRadioList()

        txtProblemBehaviorOther.Text = ""
        txtInterventionB.Text = ""
        txtResultOtherB.Text = ""
        txtBegin.Text = ""
        txtEnd.Text = ""
        chkNotFollow.Checked = False
        cmdBehaviorAdd.Text = "เพิ่มปัญหาพฤติกรรม"
    End Sub
    Protected Sub cmdAddDrugProblem_Click(sender As Object, e As EventArgs) Handles cmdDrugProblemAdd.Click
        lblDRP_Warning.Text = ""
        pnDRP_Warning.Visible = False
        pnDRP_Success.Visible = False
        pnB_Success.Visible = False
        pnAlertB.Visible = False

        If Not ValidateData() Then
            lblDRP_Warning.Text = "<b>กรุณากรอกข้อมูลให้ครบถ้วน</b> <br/>" & sAlert
            lblDRP_Warning.Visible = True
            pnDRP_Warning.Visible = True
            pnAlertB.Visible = False
            'LoadDataToComboBox()
            Exit Sub
        Else
            pnDRP_Warning.Visible = False
        End If

        If ddlDrug.SelectedValue = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกยาที่ใช้ก่อน');", True)
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกยาที่ใช้ก่อน');", True)
            'LoadDataToComboBox()
            Exit Sub
        End If

        If optResult_D.SelectedIndex = -1 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกสรุปการแก้ไขปัญหาก่อน');", True)
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกสรุปการแก้ไขปัญหาก่อน');", True)
            Exit Sub
        End If


        Dim ProblemGroup, ProblemItem As String
        ProblemGroup = cboProblemGroup.SelectedValue
        ProblemItem = cboProblemItem.SelectedValue

        Dim iDrugUID As Integer = ddlDrug.SelectedValue
        'strD = Split(txtDrug.Text, ":")

        'If Not ctlMed.Drug_IsHas(strD(0).TrimEnd()) Then
        '    ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาตรวจสอบยาที่เพิ่มก่อน');", True)
        '    LoadDataToComboBox()
        '    Exit Sub
        'End If

        'iDrugUID = ctlMed.Drug_GetUIDByCode(strD(0).TrimEnd())

        ctlMTM.MTM_DrugProblem_Save(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(lblUID_Drug.Text), FORM_TYPE_ID_MTM, iDrugUID, ProblemGroup, ProblemItem, txtProblemOtherDrug.Text, txtInterventionDrug.Text, optResult_D.SelectedValue, txtResultOther_D.Text, Session("patientid"), Request.Cookies("username").Value)

        'ddlPharmacist_D.SelectedIndex = 0
        ClearTextDrugProblem()
        LoadDrugProblemToGrid()
        '' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย")

        pnDRP_Warning.Visible = False
        pnAlertB.Visible = False
        'LoadDataToComboBox()
        pnDRP_Warning.Visible = False
        pnDRP_Success.Visible = True
    End Sub

    Private Sub ClearTextDrugProblem()
        lblUID_Drug.Text = "0"
        ddlDrug.SelectedIndex = 0
        cboProblemGroup.SelectedIndex = 0
        cboProblemItem.SelectedIndex = 0
        txtInterventionDrug.Text = ""
        cmdDrugProblemAdd.Text = "เพิ่มปัญหาจากการใช้ยา"
    End Sub

    Protected Sub cmdAddLab_Click(sender As Object, e As EventArgs) Handles cmdAddLab.Click
        If txtServiceDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุวันที่ให้บริการก่อน');", True)
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุวันที่ให้บริการก่อน');", True)
            Exit Sub
        End If
        Dim ServiceDate As Long

        ServiceDate = CLng(ConvertStrDate2DBString(txtServiceDate.Text))
        For i = 0 To grdLab.Rows.Count - 1
            Dim txtV As TextBox = grdLab.Rows(i).Cells(1).FindControl("txtResultValue")
            ctlMTM.LabResult_Save(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(grdLab.DataKeys(i).Value), ServiceDate, Session("patientid"), txtV.Text, Request.Cookies("UserID").Value)
        Next
        For i = 0 To grdLab2.Rows.Count - 1
            Dim txtV As TextBox = grdLab2.Rows(i).Cells(1).FindControl("txtResultValue0")
            ctlMTM.LabResult_Save(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(grdLab2.DataKeys(i).Value), ServiceDate, Session("patientid"), txtV.Text, Request.Cookies("UserID").Value)
        Next
        ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกผล Lab เรียบร้อย")
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกผล Lab เรียบร้อย');", True)
        'LoadDataToComboBox()
    End Sub
    Protected Sub cmdPE_Save_Click(sender As Object, e As EventArgs) Handles cmdPE_Save.Click
        ctlMTM.MTM_UpdatePE(StrNull2Zero(lblMTMUID.Text), ConvertStatus2YN(chkPitting.Checked), ConvertStatus2YN(chkWound.Checked), ConvertStatus2YN(chkPeripheral.Checked), txtPitting.Text, txtWound.Text, txtPeripheral.Text)
        ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย")
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย');", True)
        'LoadDataToComboBox()
    End Sub

    'Private Sub LoadDataToComboBox()
    '    LoadProblem()
    '    LoadHospitalRefer()
    'End Sub

    Protected Sub ddlBehavior_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlBehavior.SelectedIndexChanged
        BindFinalResultToRadioList()

    End Sub

    Protected Sub chkFollow_CheckedChanged(sender As Object, e As EventArgs) Handles chkNotFollow.CheckedChanged
        BindFinalResultToRadioList()
    End Sub

    Protected Sub cmdAddHospital_Click(sender As Object, e As EventArgs) Handles cmdAddHospital.Click
        If optMedFrom.SelectedIndex = -1 Then
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทก่อน ")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทก่อน');", True)
            Exit Sub
        End If
        If txtHospital.Text.Trim = "" Then
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุชื่อสถานพยาบาลก่อน');", True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาระบุชื่อสถานพยาบาลก่อน');", True)
            'LoadDataToComboBox()
            Exit Sub
        End If


        ctlMTM.MTM_Hospital_Add(lblMTMUID.Text, optMedFrom.SelectedValue, txtHospital.Text, Session("patientid"))
        LoadHospital()
        txtHospital.Text = ""

        'LoadDataToComboBox()
    End Sub
    Private Sub LoadHospital()
        dt = ctlMTM.MTM_Hospital_GetByPatient(StrNull2Zero(Session("patientid")))
        grdHospital.DataSource = dt
        grdHospital.DataBind()
    End Sub

    Protected Sub cmdAddDrug_Click(sender As Object, e As EventArgs) Handles cmdAddDrug.Click
        If ddlDrugUse.SelectedValue = "" Then
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกยาที่ใช้ก่อน');", True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกยาที่ใช้ก่อน');", True)
            'LoadDataToComboBox()
            Exit Sub
        End If

        Dim iDrugUID As Integer = ddlDrugUse.SelectedValue

        'strD = Split(txtDrugUse.Text, ":")
        'iDrugUID = ctlMed.Drug_GetUIDByCode(strD(0).TrimEnd())

        'If Not ctlMed.Drug_IsHas(strD(0).TrimEnd()) Then
        '    ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาตรวจสอบยาที่เพิ่มก่อน');", True)
        '    LoadDataToComboBox()
        '    Exit Sub
        'End If

        ctlMTM.MTM_DrugUse_Save(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(lblSEQ.Text), iDrugUID, "", txtFQ.Text, 0, Request.Cookies("username").Value, FORM_TYPE_ID_MTM, lblLocationID.Text, "A", StrNull2Long(Session("patientid")))

        txtFQ.Text = ""
        ' ddlDrugUse.Text = ""
        LoadDrugUse()
        'LoadDataToComboBox()
    End Sub
    Private Sub LoadDrugUse()
        dt = ctlMTM.MTM_DrugUse_Get(StrNull2Zero(Session("patientid")))
        grdDrugUse.DataSource = dt
        grdDrugUse.DataBind()
    End Sub

    Private Sub grdDrugUse_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDrugUse.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Private Sub grdHospital_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdHospital.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If
    End Sub

    Private Sub grdDrugUse_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDrugUse.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel_DU"
                    ctlMTM.MTM_DrugUse_Delete(e.CommandArgument())
                    LoadDrugUse()
                    'LoadDataToComboBox()
            End Select
        End If
    End Sub

    Private Sub grdHospital_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdHospital.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgDel_H"
                    ctlMTM.MTM_Hospital_Delete(e.CommandArgument())
                    LoadHospital()
                    'LoadDataToComboBox()
            End Select
        End If
    End Sub

    Protected Sub optMTMType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optMTMType.SelectedIndexChanged

        lblSEQ.Text = (ctlMTM.MTM_Master_GetLastSEQ(Request.Cookies("LocationID").Value, Session("patientid"), optMTMType.SelectedValue) + 1).ToString
        'BindPatientFromList()
        'LoadDataToComboBox()
    End Sub

    'Private Sub BindPatientFromList()
    '    optPatientFrom.Items.Clear()

    '    optPatientFrom.Items.Add("Walk in")
    '    optPatientFrom.Items(0).Value = "1"

    '    optPatientFrom.Items.Add("หน่วยบริการส่งมา")
    '    optPatientFrom.Items(1).Value = "2"

    '    optPatientFrom.Items.Add("ร้านเยี่ยมเอง")
    '    optPatientFrom.Items(2).Value = "3"

    '    optPatientFrom.Items.Add("โครงการลดความแออัด")
    '    optPatientFrom.Items(3).Value = "4"

    '    optPatientFrom.Items.Add("อื่นๆ")
    '    optPatientFrom.Items(4).Value = "9"

    'End Sub

    Protected Sub cmdSaveDrug_Click(sender As Object, e As EventArgs) Handles cmdSaveDrug.Click
        If optMTMType.SelectedIndex = -1 Then
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทของการให้บริการก่อน ")
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกประเภทของการให้บริการก่อน ');", True)
            Exit Sub
        End If

        ctlMTM.MTM_UpdateHerb(StrNull2Zero(lblMTMUID.Text), txtDrug1.Text, txtDrug2.Text, txtDrug3.Text, txtDrug4.Text, "", "", "")

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการทำงาน','บันทึกข้อมูลสมุนไพรเรียบร้อย');", True)
        'Response.Redirect("FormList.aspx")
        'LoadDataToComboBox()
    End Sub

    Protected Sub cmdBehaviorCancel_Click(sender As Object, e As EventArgs) Handles cmdBehaviorCancel.Click
        ClearTextBehavior()
        'LoadDataToComboBox()
    End Sub

    Protected Sub cmdDrugProblemCancel_Click(sender As Object, e As EventArgs) Handles cmdDrugProblemCancel.Click
        ClearTextDrugProblem()
        'LoadDataToComboBox()
    End Sub
    Private Function ValidateDataDrugRemain() As Boolean
        sAlert = ""
        isValid = True

        If ddlDrugRemain.Text = "" Then
            sAlert &= "- กรุณาเลือกรายการยาก่อน <br/>"
            isValid = False
        End If

        If StrNull2Zero(txtQTYRemain.Text) = 0 Then
            sAlert &= "- กรุณาระบุจำนวนก่อน <br/>"
            isValid = False
        End If

        If txtUOMRemain.Text = "" Then
            sAlert &= "- กรุณาระบุหน่วยนับก่อน <br/>"
            isValid = False
        End If

        Return isValid
    End Function
    Private Function ValidateDataDrugRefill() As Boolean
        sAlert = ""
        isValid = True

        If ddlDrugRefill.Text = "" Then
            sAlert &= "- กรุณาเลือกรายการยาก่อน <br/>"
            isValid = False
        End If

        If StrNull2Zero(txtQTYRefill.Text) = 0 Then
            sAlert &= "- กรุณาระบุจำนวนก่อน <br/>"
            isValid = False
        End If

        If txtUOMRefill.Text = "" Then
            sAlert &= "- กรุณาระบุหน่วยนับก่อน <br/>"
            isValid = False
        End If

        Return isValid
    End Function
    Protected Sub cmdSaveDrugRemain_Click(sender As Object, e As EventArgs) Handles cmdDrugRemain.Click
        If Not ValidateDataDrugRemain() Then
            lblRemainAlert.Text = "<b>กรุณากรอกข้อมูลให้ครบถ้วน</b> <br/>" & sAlert
            lblRemainAlert.Visible = True
            pnDrugRemainAlert.Visible = True
            'LoadDataToComboBox()
            Exit Sub
        Else
            pnDrugRemainAlert.Visible = False
        End If

        If ddlDrugRemain.Text = "" Then
            ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกยาที่ใช้ก่อน');", True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกยาที่ใช้ก่อน');", True)
            'LoadDataToComboBox()
            Exit Sub
        End If

        'strD = Split(txtDrugRemainCode.Text, ":")

        'If Not ctlMed.Drug_IsHas(strD(0).TrimEnd()) Then
        '    ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาตรวจสอบยาที่เพิ่มก่อน');", True)
        '    LoadDataToComboBox()
        '    Exit Sub
        'End If


        ctlMTM.MTM_DrugRemain_Save(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(lblUID_Drug.Text), FORM_TYPE_ID_MTM, StrNull2Zero(ddlDrugRemain.SelectedValue), txtQTYRemain.Text, txtUOMRemain.Text, txtHospitalRemain.Text, ddlRemainReason.SelectedValue, txtRemarkRemain.Text, Session("patientid"), Request.Cookies("username").Value)

        ClearTextDrugRemain()
        LoadDrugRemainToGrid()
        '' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย")

        pnDrugRemainAlert.Visible = False
        'LoadDataToComboBox()
    End Sub
    Private Sub ClearTextDrugRemain()
        lblDrugRemainID.Text = "0"
        ddlDrugRemain.ClearSelection()
        'lblDrugRemainName.Text = ""
        txtQTYRemain.Text = ""
        txtUOMRemain.Text = ""
        txtRemarkRemain.Text = ""
        txtHospitalRemain.Text = ""
    End Sub
    Private Sub ClearTextDrugRefill()
        lblDrugRefillID.Text = "0"
        ddlDrugRefill.Text = ""
        'lblDrugRefillName.Text = ""
        txtQTYRefill.Text = ""
        txtUOMRefill.Text = ""
    End Sub

    Private Sub LoadDrugRemainToGrid()
        dt = ctlMTM.MTM_DrugRemain_Get(StrNull2Zero(Session("patientid")))
        grdDrugRemain.DataSource = dt
        grdDrugRemain.DataBind()
        pnDrugRemainAlert.Visible = False
    End Sub


    Protected Sub grdDrugRemain_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDrugRemain.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(8).FindControl("imgDelRemain")
            imgD.Attributes.Add("onClick", scriptString)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub
    Protected Sub grdDrugRemain_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDrugRemain.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEditRemain"
                    EditDrugRemain(e.CommandArgument())
                Case "imgDelRemain"
                    ctlMTM.MTM_DrugRemain_Delete(StrNull2Zero(e.CommandArgument()))
                    LoadDrugRemainToGrid()
                    'LoadDataToComboBox()
            End Select
        End If
    End Sub
    Private Sub EditDrugRemain(UID As Integer)

        dt = ctlMTM.MTM_DrugRemain_GetByUID(UID)
        If dt.Rows.Count > 0 Then
            lblUID_Drug.Text = dt.Rows(0)("UID")
            'txtDrugRemainCode.Text = DBNull2Str(dt.Rows(0)("DrugUID"))

            txtQTYRemain.Text = DBNull2Str(dt.Rows(0)("QTY"))
            txtUOMRemain.Text = DBNull2Str(dt.Rows(0)("UOM"))
            If DBNull2Str(dt.Rows(0)("ReasonUID")) <> "" Then
                ddlRemainReason.SelectedValue = DBNull2Str(dt.Rows(0)("ReasonUID"))
            End If
            txtRemarkRemain.Text = DBNull2Str(dt.Rows(0)("ReasonRemark"))

            ddlDrugRemain.SelectedValue = DBNull2Zero(dt.Rows(0)("DrugUID"))
            txtHospitalRemain.Text = DBNull2Str(dt.Rows(0)("RemainFrom"))

            pnDrugRemainAlert.Visible = False
            cmdDrugRemain.Text = "บันทึกการแก้ไข"
        End If
    End Sub


    'Private Sub LoadICD10Name()
    '    If txtICD10Code.Text <> "" Then
    '        strD = Split(txtICD10Code.Text, ":")
    '        txtICD10Code.Text = ctlD.Desease_GetName(strD(0).TrimEnd().ToUpper())
    '    End If
    'End Sub

    'Private Sub LoadDrugRemainName()
    '    If txtDrugRemainCode.Text <> "" Then
    '        strD = Split(txtDrugRemainCode.Text, ":")
    '        txtDrugRemainCode.Text = ctlMed.Drug_GetName(strD(0).TrimEnd())
    '    End If
    'End Sub
    'Private Sub LoadDrugRefillName()
    '    If txtDrugRefillCode.Text <> "" Then
    '        strD = Split(txtDrugRefillCode.Text, ":")
    '        txtDrugRefillCode.Text = ctlMed.Drug_GetName(strD(0).TrimEnd())
    '    End If

    'End Sub

    'Private Sub LoadDrugName()
    '    If txtDrug.Text <> "" Then
    '        strD = Split(txtDrug.Text, ":")
    '        txtDrug.Text = ctlMed.Drug_GetName(strD(0).TrimEnd())
    '    End If

    'End Sub
    'Private Sub LoadDrugUseName()
    '    If txtDrugUse.Text <> "" Then
    '        strD = Split(txtDrugUse.Text, ":")
    '        txtDrugUse.Text = ctlMed.Drug_GetName(strD(0).TrimEnd())
    '    End If
    'End Sub


    Protected Sub optResult_D_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optResult_D.SelectedIndexChanged
        If optResult_D.SelectedValue = 2 Then
            pnRefer.Visible = True
            pnDrugRefill.Visible = False
            LoadHospitalRefer()
        Else
            pnRefer.Visible = False
            pnDrugRefill.Visible = True
        End If
    End Sub

    Protected Sub cmdDrugRefill_Click(sender As Object, e As EventArgs) Handles cmdDrugRefill.Click
        If Not ValidateDataDrugRefill() Then
            lblDrugRefillAlert.Text = "<b>กรุณากรอกข้อมูลให้ครบถ้วน</b> <br/>" & sAlert
            lblDrugRefillAlert.Visible = True
            pnDrugRefillAlert.Visible = True
            'LoadDataToComboBox()
            Exit Sub
        Else
            pnDrugRefillAlert.Visible = False
        End If

        If ddlDrugRefill.Text = "" Then
            '' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกยาที่ใช้ก่อน');", True)
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาเลือกยาที่ใช้ก่อน');", True)
            'LoadDataToComboBox()
            Exit Sub
        End If
        'Dim strD() As String
        Dim iDrugUID As Integer = ddlDrugRefill.SelectedValue

        'strD = Split(txtDrugRefillCode.Text, ":")
        'iDrugUID = ctlMed.Drug_GetUIDByCode(strD(0).TrimEnd())

        'If Not ctlMed.Drug_IsHas(strD(0).TrimEnd()) Then
        '    ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาตรวจสอบยาที่เพิ่มก่อน');", True)
        '    'LoadDataToComboBox()
        '    Exit Sub
        'End If

        If StrNull2Zero(hdRefillHeaderUID.Value) = 0 Then
            ctlMTM.DrugRefillHeader_Add(ConvertStrDate2InformDateString(txtServiceDate.Text), Session("patientid"), Request.Cookies("LocationID").Value, txtRefillDesc.Text, StrNull2Zero(lblMTMUID.Text), Request.Cookies("username").Value)
            hdRefillHeaderUID.Value = ctlMTM.DrugRefillHeader_GetUID(ConvertStrDate2InformDateString(txtServiceDate.Text), Session("patientid"), Request.Cookies("LocationID").Value)
        End If

        ctlMTM.DrugRefillItem_Save(StrNull2Zero(lblUID_Drug.Text), StrNull2Zero(hdRefillHeaderUID.Value.ToString()), iDrugUID, txtQTYRefill.Text, txtUOMRefill.Text, txtRefillDesc.Text, Request.Cookies("username").Value)


        'ctlMTM.MTM_DrugRefill_Save(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(lblUID_Drug.Text), FORM_TYPE_ID_MTM, strD(0).TrimEnd(), txtQTYRefill.Text, txtUOMRefill.Text, txtRefillDesc.Text, Session("patientid"), Request.Cookies("username").Value, ConvertStrDate2InformDateString(txtServiceDate.Text))

        ClearTextDrugRefill()
        LoadDrugRefillToGrid()
        '' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย")

        pnDrugRefillAlert.Visible = False
        'LoadDataToComboBox()
    End Sub
    Private Sub LoadDrugRefillToGrid()
        dt = ctlMTM.DrugRefill_Get(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(Session("patientid")))
        grdDrugRefill.DataSource = dt
        grdDrugRefill.DataBind()
        pnDrugRefillAlert.Visible = False
    End Sub

    Private Sub EditDrugRefill(UID As Integer)
        dt = ctlMTM.DrugRefill_GetByUID(UID)
        If dt.Rows.Count > 0 Then
            lblUID_Drug.Text = dt.Rows(0)("UID")
            ddlDrugRefill.SelectedValue = DBNull2Str(dt.Rows(0)("DrugUID"))
            txtQTYRefill.Text = DBNull2Str(dt.Rows(0)("QTY"))
            txtUOMRefill.Text = DBNull2Str(dt.Rows(0)("UOM"))
            txtRefillDesc.Text = DBNull2Str(dt.Rows(0)("UsedRemark"))

            pnDrugRefillAlert.Visible = False
            cmdDrugRefill.Text = "บันทึกการแก้ไข"
        End If
    End Sub
    Private Sub LoadHospitalRefer()
        Dim ctlH As New HospitalController
        dt = ctlH.Hospital_GetByStatus("A")
        ddlReferHospital.DataSource = dt
        ddlReferHospital.DataValueField = "HospitalUID"
        ddlReferHospital.DataTextField = "HospitalName"
        ddlReferHospital.DataBind()
        pnReferAlert.Visible = False
    End Sub

    Private Sub LoadDrugRemainReasonToDDL()
        Dim ctlRf As New ReferenceValueController
        dt = ctlRf.ReferenceValue_GetByDomainCode("REMREASON")
        ddlRemainReason.DataSource = dt
        ddlRemainReason.DataTextField = "DisplayName"
        ddlRemainReason.DataValueField = "UID"
        ddlRemainReason.DataBind()
        pnReferAlert.Visible = False
    End Sub


    Private Sub LoadReferDetail()
        LoadHospitalRefer()
        dt = ctlMTM.Refer_Get(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(Session("patientid")))
        If dt.Rows.Count > 0 Then
            ddlReferHospital.SelectedValue = dt.Rows(0)("HospitalUID")
            txtReferReason.Text = dt.Rows(0)("ReferReason")
        Else
            ddlReferHospital.Text = ""
            txtReferReason.Text = ""
        End If

        pnReferAlert.Visible = False
    End Sub

    Protected Sub cmdRefer_Click(sender As Object, e As EventArgs) Handles cmdRefer.Click
        If ddlReferHospital.Text = "" Or txtReferReason.Text = "" Then
            pnReferAlert.Visible = True
            lblReferAlert.Text = "กรุณาระบุสถานพยาบาลที่ส่งต่อ หรือ ระบุเหตุผลที่ส่งต่อก่อน"
            Exit Sub
        End If

        ctlMTM.Refer_Save(StrNull2Zero(lblMTMUID.Text), StrNull2Zero(lblUID_Drug.Text), ddlReferHospital.SelectedValue, txtReferReason.Text, Session("patientid"), Request.Cookies("username").Value)

        pnReferAlert.Visible = True
        lblReferAlert.Text = "บันทึกการส่งต่อเรียบร้อย"


    End Sub

    Protected Sub cmdCancelRefer_Click(sender As Object, e As EventArgs) Handles cmdCancelRefer.Click
        ctlMTM.Refer_Delete(StrNull2Zero(lblMTMUID.Text), Session("patientid"), Request.Cookies("username").Value)

        pnReferAlert.Visible = True
        lblReferAlert.Text = "ยกเลิกเรียบร้อย"

        ddlReferHospital.Text = ""
        txtReferReason.Text = ""
    End Sub

    Private Sub grdDrugRefill_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdDrugRefill.RowCommand
        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEditRefill"
                    EditDrugRefill(e.CommandArgument())
                Case "imgDelRefill"
                    ctlMTM.DrugRefill_Delete(StrNull2Zero(e.CommandArgument()))
                    LoadDrugRefillToGrid()
                    'LoadDataToComboBox()
            End Select
        End If

    End Sub

    Private Sub grdDrugRefill_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdDrugRefill.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
            Dim scriptString As String = "javascript:return confirm(""ต้องการลบ ข้อมูลนี้ ?"");"
            Dim imgD As Image = e.Row.Cells(6).FindControl("imgDelRefill")
            imgD.Attributes.Add("onClick", scriptString)
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#d0e8ff';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")
        End If
    End Sub

    'Protected Sub cmdDrugSearch_Click(sender As Object, e As EventArgs) Handles cmdDrugSearch.Click
    '    Dim dtM As New DataTable
    '    Dim ctlD As New DrugController
    '    cboDrug.Items.Clear()
    '    cboDrugUse.Items.Clear()
    '    dtM = ctlD.Drug_GetBySearch(txtDrugSearch.Text)
    '    If dtM.Rows.Count > 0 Then
    '        grdDrugTMT.DataSource = dtM
    '        grdDrugTMT.DataBind()

    '    End If
    '    dtM = Nothing
    'End Sub

End Class