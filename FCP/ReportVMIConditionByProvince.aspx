﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="ReportVMIConditionByProvince.aspx.vb" Inherits=".ReportVMIConditionByProvince" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1><asp:Label ID="lblReportHeader" runat="server" Text="รายงาน"></asp:Label>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
  
    <table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td height="350" valign="top">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td  valign="top"> <table border="0" align="center" cellpadding="0" cellspacing="3">
      <tr>
    <td>
        <asp:Label ID="lblProv" runat="server" Text="จังหวัด"></asp:Label>      </td>
    <td colspan="3" align="left">
        <asp:DropDownList ID="ddlProvince" runat="server" AutoPostBack="True">        </asp:DropDownList>      </td>
    </tr>
  <tr>
    <td>
        <asp:Label ID="lblStart" runat="server" Text="ตั้งแต่"></asp:Label>
      </td>
    <td>
        <asp:TextBox ID="txtStartDate" runat="server">        </asp:TextBox>
      </td>
    <td>
        <asp:Label ID="lblTo" runat="server" Text="ถึง"></asp:Label>
      </td>
    <td>
        <asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox>
      </td>
  </tr>
      <tr>
                                            <td>
                                                <asp:Label ID="lblLocation" runat="server" Text="ร้านยา"></asp:Label>                                            </td>
                                            <td align="left"><asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddlLocation" runat="server" CssClass="OptionControl">                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="lnkFindLocation" EventName="Click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>                                              </td>
                                            <td colspan="2"><asp:Image ID="imgArrowLocation" runat="server" ImageUrl="images/arrow-orange-icons.png" />
                                            <asp:TextBox ID="txtFindLocation" runat="server" Width="60px"></asp:TextBox>
                                              <asp:LinkButton ID="lnkFindLocation" runat="server">ค้นหา</asp:LinkButton>                                              </td>
    </tr>
  
</table></td>
    </tr>
    <tr>
      <td align="center">
          <asp:Button ID="cmdExcel" runat="server" Text="ส่งออก Excel" CssClass="buttonFind" />
        </td>
    </tr>
  </table>
  </td> 
      </tr>
    </table>
</section>
</asp:Content>
