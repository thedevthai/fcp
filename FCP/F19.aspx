﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="F19.aspx.vb" Inherits=".F19" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>
<%@ Register src="ucHistoryYear.ascx" tagname="ucHistoryYear" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/pagestyles.css">
<link rel="stylesheet" type="text/css" href="css/rajchasistyles.css">
<link rel="stylesheet" type="text/css" href="css/uidialog.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>F19
        <small>แบบคัดกรอง/แบบประเมิน ปัญหาการดื่มสุรา</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">   


    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลกิจกรรม</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body"> 
        <table class="table table-hover">
      <tr>
        <td width="100" class="texttopic">รหัสร้าน</td>
        <td width="120" class="LocationName">
            <asp:Label ID="lblLocationID" runat="server"></asp:Label>
          </td>
        <td width="50" class="texttopic">ชื่อร้าน</td>
        <td width="300" class="LocationName">
            <asp:Label ID="lblLocationName" runat="server"></asp:Label>          </td>
        <td width="50" class="texttopic">จังหวัด</td>
        <td class="LocationName">
            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>
          </td>
           <td  align="left"><span class="NameEN">Ref.ID :<asp:Label ID="lblID" 
                  runat="server"></asp:Label></span></td>
      </tr>
    </table>
                  
        
        <table  class="table table-hover">
      <tr>        
        
        <td width="100">วันที่ให้บริการ</td>
        <td>
            <asp:TextBox ID="txtServiceDate" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
(วว/ดด/ปปปป พ.ศ.) 
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="txtServiceDate" CssClass="text-red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="\d{2}/\d{2}/\d{4}"></asp:RegularExpressionValidator></td>
        <td><span class="pull-right">รวมระยะเวลา (นาที)<img src="images/star.png" width="10" height="10" /></span></td>
        <td  Width="70px">
            <asp:TextBox ID="txtTime" runat="server" Width="60px"></asp:TextBox></td>
        <td><span class="pull-right">เภสัชกรผู้ให้บริการ</span> </td>
        <td>
            <asp:DropDownList ID="ddlPerson" runat="server" CssClass="form-control select2">            </asp:DropDownList>          </td>
      </tr>
    </table>




 </div>
           
          </div>
   
  <div class="box box-primary">
    <div class="box-header">
      <h3 class="box-title">แบบประเมินปัญหาการดื่มสุรา
        </h3>
         <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">

 <div class="row">
     <div class="col-md-12">
         <p>คำชี้แจง : คำถามแต่ละข้อต่อไปนี้จะถามถึงประสบการณ์การดิ่มสุราในรอบ 1 ปีที่ผ่านมาโดย "สุรา" หมายถึงเครื่องดื่มที่มีแอลกอฮอล์ทุกชนิด ได้แก่ เบียร์ เหล้า สาโท กระแช่ วิสกี้ สปาย ไวน์ เป็นต้น ขอให้ตอบตามความจริง </p>
         </div>
     </div>

      <div class="row">

        <div class="col-md-12">
          <div class="form-group">
            <label>1. คุณดื่มสุราบ่อยเพียงไร</label>
            <asp:RadioButtonList ID="optQ1" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
 <asp:ListItem Value="0" Selected="True">ไม่เคยเลย</asp:ListItem>
              <asp:ListItem Value="1">เดือนละครั้งหรือน้อยกว่า</asp:ListItem>
             
                <asp:ListItem Value="2">2-4 ครั้งต่อเดือน</asp:ListItem>
                <asp:ListItem Value="3">2-3 ครั้งต่อสัปดาห์</asp:ListItem>
                <asp:ListItem Value="4">4 ครั้งขึ้นไปต่อสัปดาห์</asp:ListItem>
             
            </asp:RadioButtonList>
          </div>
        </div>

      


      </div>
      <div class="row">

        <div class="col-md-12">
          <div class="form-group">
            <label>2. เวลาที่คุณดื่มสุราโดยทั่วไปแล้วคุณดื่มประมาณเท่าไรต่อวัน หรือ</label>
            <asp:RadioButtonList ID="optQ2A" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
 <asp:ListItem Value="0" Selected="True">1-2 ดื่มมาตรฐาน</asp:ListItem>
              <asp:ListItem Value="1">3-4 ดื่มมาตรฐาน</asp:ListItem>
             
                <asp:ListItem Value="2">5-6 ดื่มมาตรฐาน</asp:ListItem>
                <asp:ListItem Value="3">7-9 ดื่มมาตรฐาน</asp:ListItem>
                <asp:ListItem Value="4">10 ดื่มมาตรฐานขึ้นไป</asp:ListItem>
             
            </asp:RadioButtonList>

          </div>
        </div>


      </div>
      
        
      <div class="row">

        <div class="col-md-12">
          <div class="form-group">
            <label>ถ้าโดยทั่วไปดื่มเบียร์ เช่น สิงห์ ไฮเนเกรน ลีโอ เชียร์ ไทเกอร์ ช้าง ดื่มประมาณเท่าไหร่ต่อวัน หรือ</label>
            <asp:RadioButtonList ID="optQ2B" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                <asp:ListItem Value="0" Selected="True">1-1.5 กระป๋อง / 1/2-3/4 ขวด</asp:ListItem>
                <asp:ListItem Value="1">2-3 กระป๋อง / 1-1.5 ขวด </asp:ListItem>             
                <asp:ListItem Value="2">3.5-4 กระป๋อง / 2 ขวด</asp:ListItem>
                <asp:ListItem Value="3">4.5-7 กระป๋อง / 3-4 ขวด</asp:ListItem>
                <asp:ListItem Value="4">7 กระป๋อง / 4 ขวดขึ้นไป</asp:ListItem>
             
            </asp:RadioButtonList>
          </div>
        </div>

      


      </div>
        
      <div class="row">

        <div class="col-md-12">
          <div class="form-group">
            <label>ถ้าโดยทั่วไปดื่มเหล้า เช่น แม่โขง หงส์ทอง หงส์ทิพย์ เหล้าขาว 40 ดีกรี ดื่มประมาณเท่าไรต่อวัน</label>
            <asp:RadioButtonList ID="optQ2C" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
 <asp:ListItem Value="0" Selected="True">2-3 ฝา</asp:ListItem>
              <asp:ListItem Value="1">1/4 แบน</asp:ListItem>
             
                <asp:ListItem Value="2">1/2 แบน</asp:ListItem>
                <asp:ListItem Value="3">3/4 แบน</asp:ListItem>
                <asp:ListItem Value="4">1 แบนขึ้นไป</asp:ListItem>
             
            </asp:RadioButtonList>
          </div>
        </div>

      


      </div>
        
      <div class="row">

        <div class="col-md-12">
          <div class="form-group">
            <label>3. บ่อยครั้งเพียงไรที่คุณดื่มตั้งแต่ 6 ดื่มมาตรฐานขึ้นไปหรือเบียร์ 4 กระป๋องหรือ 2 ขวดใหญ่ขึ้นไป หรือเหล้าวิสกี้ 3 เป๊กขึ้นไป</label>
            <asp:RadioButtonList ID="optQ3" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                <asp:ListItem Value="0" Selected="True">ไม่เคยเลย</asp:ListItem>
                <asp:ListItem Value="1">น้อยกว่าเดือนละครั้ง</asp:ListItem>             
                <asp:ListItem Value="2">เดือนละครั้ง</asp:ListItem>
                <asp:ListItem Value="3">สัปดาห์ละครั้ง</asp:ListItem>
                <asp:ListItem Value="4">ทุกวันหรือเกือบทุกวัน</asp:ListItem>
             
            </asp:RadioButtonList>
          </div>
        </div>

      


      </div>
        
      <div class="row">

        <div class="col-md-12">
          <div class="form-group">
            <label>4. ในช่วงหนึ่งปีที่แล้ว มีบ่อยครั้งเพียงไรที่คุณพบว่าคุณไม่สามารถหยุดดื่มได้หากคุณได้เริ่มดื่มไปแล้ว</label>
            <asp:RadioButtonList ID="optQ4" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                 <asp:ListItem Value="0" Selected="True">ไม่เคยเลย</asp:ListItem>
                <asp:ListItem Value="1">น้อยกว่าเดือนละครั้ง</asp:ListItem>             
                <asp:ListItem Value="2">เดือนละครั้ง</asp:ListItem>
                <asp:ListItem Value="3">สัปดาห์ละครั้ง</asp:ListItem>
                <asp:ListItem Value="4">ทุกวันหรือเกือบทุกวัน</asp:ListItem>
             
            </asp:RadioButtonList>
          </div>
        </div>

      


      </div>
        
      <div class="row">

        <div class="col-md-12">
          <div class="form-group">
            <label>5. ในช่วงหนึ่งปีที่แล้ว มีบ่อยเพียงไรที่คุณไม่ได้ทำสิ่งที่คุณควรจะทำตามปกติ เพราะคุณมัวแต่ไปดื่มสุราเสีย</label>
            <asp:RadioButtonList ID="optQ5" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                 <asp:ListItem Value="0" Selected="True">ไม่เคยเลย</asp:ListItem>
                <asp:ListItem Value="1">น้อยกว่าเดือนละครั้ง</asp:ListItem>             
                <asp:ListItem Value="2">เดือนละครั้ง</asp:ListItem>
                <asp:ListItem Value="3">สัปดาห์ละครั้ง</asp:ListItem>
                <asp:ListItem Value="4">ทุกวันหรือเกือบทุกวัน</asp:ListItem>
             
            </asp:RadioButtonList>
          </div>
        </div>

      


      </div>
        
      <div class="row">

        <div class="col-md-12">
          <div class="form-group">
            <label>6. ในช่วงหนึ่งปีที่แล้ว มีบ่อยเพียงไรที่คุณต้องรีบดื่มสุราทันทีในตอนเช้า เพื่อจะได้ดำเนินชีวิตตามปกติหรือถอนอาการเมาค้างจากการเมาหนักในเมื่อคืนที่ผ่านมา</label>
            <asp:RadioButtonList ID="optQ6" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
  <asp:ListItem Value="0" Selected="True">ไม่เคยเลย</asp:ListItem>
                <asp:ListItem Value="1">น้อยกว่าเดือนละครั้ง</asp:ListItem>             
                <asp:ListItem Value="2">เดือนละครั้ง</asp:ListItem>
                <asp:ListItem Value="3">สัปดาห์ละครั้ง</asp:ListItem>
                <asp:ListItem Value="4">ทุกวันหรือเกือบทุกวัน</asp:ListItem>
             
            </asp:RadioButtonList>
          </div>
        </div>

      


      </div>
        
      <div class="row">

        <div class="col-md-12">
          <div class="form-group">
            <label>7. ในช่วงหนึ่งปีที่แล้ว มีบ่อยเพียงไรที่คุณรู้สิกไม่ดี โกรธหรือเสียใจ เนื่องจากคุณได้ทำบางสิ่งบางอย่างลงไปขณะที่คุณดื่มสุราเข้าไป</label>
            <asp:RadioButtonList ID="optQ7" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                 <asp:ListItem Value="0" Selected="True">ไม่เคยเลย</asp:ListItem>
                <asp:ListItem Value="1">น้อยกว่าเดือนละครั้ง</asp:ListItem>             
                <asp:ListItem Value="2">เดือนละครั้ง</asp:ListItem>
                <asp:ListItem Value="3">สัปดาห์ละครั้ง</asp:ListItem>
                <asp:ListItem Value="4">ทุกวันหรือเกือบทุกวัน</asp:ListItem>
             
            </asp:RadioButtonList>
          </div>
        </div>

      


      </div>
        
      <div class="row">

        <div class="col-md-12">
          <div class="form-group">
            <label>8. ในช่วงหนึ่งปีที่แล้ว มีบ่อยเพียงไรที่คุณไม่สามาราถจำได้ว่าเกิดอะไรขึ้นในคืนที่ผ่านมาเพราะว่าคุณได้ดื่มสุราเข้าไป</label>
            <asp:RadioButtonList ID="optQ8" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                 <asp:ListItem Value="0" Selected="True">ไม่เคยเลย</asp:ListItem>
                <asp:ListItem Value="1">น้อยกว่าเดือนละครั้ง</asp:ListItem>             
                <asp:ListItem Value="2">เดือนละครั้ง</asp:ListItem>
                <asp:ListItem Value="3">สัปดาห์ละครั้ง</asp:ListItem>
                <asp:ListItem Value="4">ทุกวันหรือเกือบทุกวัน</asp:ListItem>
             
            </asp:RadioButtonList>
          </div>
        </div>

      


      </div>
        
      <div class="row">

        <div class="col-md-12">
          <div class="form-group">
            <label>9. ตัวคุณเองหรือคนอื่น เคยได้รับบาดเจ็บซึ่งเป็นผลจากการดื่มสุราของคุณหรือไม่</label>
            <asp:RadioButtonList ID="optQ9" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                <asp:ListItem Value="0" Selected="True">ไม่เคยเลย</asp:ListItem>          
                <asp:ListItem Value="2">เคยแต่ไม่ได้เกิดขึ้นในปีที่แล้ว</asp:ListItem> 
                <asp:ListItem Value="4">เคยเกิดขึ้นในช่วงหนึ่งปีที่แล้ว</asp:ListItem>
             
            </asp:RadioButtonList>
          </div>
        </div>

      


      </div>
        
      <div class="row">

        <div class="col-md-12">
          <div class="form-group">
            <label>10. เคยมีแพทย์ หรือบุคลากรทางการแพทย์หรือเพื่อนฝูงหรือญาติพี่น้องแสดงความเป็นห่วงเป็นใยต่อการดื่มสุราของคุณหรือไม่</label>
            <asp:RadioButtonList ID="optQ10" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
                <asp:ListItem Value="0" Selected="True">ไม่เคยเลย</asp:ListItem>          
                <asp:ListItem Value="2">เคยแต่ไม่ได้เกิดขึ้นในปีที่แล้ว</asp:ListItem> 
                <asp:ListItem Value="4">เคยเกิดขึ้นในช่วงหนึ่งปีที่แล้ว</asp:ListItem>             
            </asp:RadioButtonList>
          </div>
        </div>

      


      </div>


    </div>
        <div class="box-footer clearfix">
         <span class="text-primary text-bold">คะแนนรวมทั้งหมด</span>&nbsp;
              <asp:Label ID="lblQTotal" runat="server" cssclass="text-primary text-bold"></asp:Label>
               &nbsp;<asp:Label ID="lblQResultTxt" runat="server" Visible="False"  class="text-red text-bold"></asp:Label>
       
            </div>
  </div>
    
    
  
      
  <div class="box box-solid">   
    <div class="box-body">
      <table class="table table-hover">                 
        <tr>
          <td class="MenuSt text-center">คำยินยอม</td>
        </tr>
           <tr>
          <td> 
              <asp:CheckBox ID="chkAgree" runat="server" Text="ข้าพเจ้าได้รับทราบรายละเอียดของแผนงานฯและสมัครใจเข้ารับบริการ" CssClass="text-primary text-bold" /> โดยยินยอมให้เภสัชกรผู้ให้บริการตามแผนงานฯนี้เข้าถึงข้อมูลสุขภาพของข้าพเจ้าและอนุญาตให้ผู้ควบคุมข้อมูลของแผนงานฯ เก็บรวบรวมและบันทึกข้อมูลสุขภาพของข้าพเจ้าไว้เพื่อวัตถุประสงค์ในการสร้างเสริมสุขภาพของข้าพเจ้าเองและเพื่อประโยชน์ส่วนรวมของการพัฒนางานสร้างเสริมสุขภาพและการพัฒนาวิชาชีพเภสัชกรรมชุมชน โดยจะต้องเก็บข้อมูลของข้าพเจ้าเป็นความลับและอนุญตให้เปิดเผยหรือใช้ข้อมูลของข้าพเจ้าในรูปแบบที่ไม่ระบุตัวตนของข้าพเจ้าเท่านั้น
               </td>

        </tr>
           <tr>
          <td> <asp:CheckBox ID="chkClose" runat="server" Text="จบการทำงาน" Checked="True" /></td>
        </tr>      
      </table>
    </div>
  </div> 

      <div class="text-center"><asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="100px" /></div>
    </section>
</asp:Content>
 

