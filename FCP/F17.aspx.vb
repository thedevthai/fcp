﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO



Public Class F17
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlLct As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlOrder As New OrderController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then
            If Request("acttype") = "view" Then
                cmdSave.Visible = False
            Else
                cmdSave.Visible = True
            End If
            txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)

            LoadFormData()
            LoadPharmacist(Request.Cookies("LocationID").Value)
        End If

        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        'txtCardID.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")


    End Sub
    Private Sub LoadPharmacist(LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With
            End If
        End If
        dtP = Nothing
    End Sub
    Private Sub LoadLocationData(LocationID As String)
        dt = ctlLct.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblLocationID.Text = .Item("LocationID")
                lblLocationName.Text = .Item("LocationName")
                lblLocationProvince.Text = .Item("ProvinceName")
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub CalScore()
        Dim iScore As Integer = 0
        iScore = StrRadioOption2Zero(optQ1.SelectedValue) +
                 StrRadioOption2Zero(optQ2.SelectedValue) +
                 StrRadioOption2Zero(optQ3.SelectedValue) +
                 StrRadioOption2Zero(optQ4.SelectedValue) +
                 StrRadioOption2Zero(optQ5.SelectedValue) +
                 StrRadioOption2Zero(optQ6.SelectedValue) +
                 StrRadioOption2Zero(optQ7.SelectedValue) +
                 StrRadioOption2Zero(optQ8.SelectedValue)

        If iScore = 0 Then
            optAbnormal.SelectedIndex = 0
        ElseIf iScore > 0 And iScore <= 6 Then
            optAbnormal.SelectedIndex = 1
        ElseIf iScore >= 7 Then
            optAbnormal.SelectedIndex = 2
        End If

    End Sub


    'Private Sub BindDayToDDL()
    '    Dim sD As String = ""
    '    For i = 1 To 31
    '        If i < 10 Then
    '            sD = "0" & i
    '        Else
    '            sD = i
    '        End If

    '        With ddlDay
    '            .Items.Add(i)
    '            .Items(i - 1).Value = sD
    '            .SelectedIndex = 0
    '        End With
    '    Next
    'End Sub
    'Private Sub BindMonthToDDL()
    '    Dim sD As String = ""

    '    For i = 1 To 12
    '        If i < 10 Then
    '            sD = "0" & i
    '        Else
    '            sD = i
    '        End If

    '        With ddlMonth
    '            .Items.Add(DisplayNumber2Month(i))
    '            .Items(i - 1).Value = sD
    '            .SelectedIndex = 0
    '        End With
    '    Next
    'End Sub

    'Private Sub BindYearToDDL()
    '    Dim ctlb As New ApplicationBaseClass
    '    Dim sDate As Date
    '    Dim y As Integer

    '    sDate = ctlb.GET_DATE_SERVER()

    '    If sDate.Year < 2300 Then
    '        y = (sDate.Year + 543)
    '    Else
    '        y = sDate.Year
    '    End If
    '    Dim i As Integer = 0
    '    Dim n As Integer = y - 17

    '    For i = 0 To 9
    '        With ddlYear
    '            .Items.Add(n)
    '            .Items(i).Value = n
    '            .SelectedIndex = 0
    '        End With
    '        n = n - 1
    '    Next
    'End Sub

    Private Sub LoadFormData()
        Dim pYear As Integer
        If Not Request("fid") Is Nothing Then
            dt = ctlOrder.GetOrder_ByID(StrNull2Zero(Request("fid")))
        Else
            If Not Request("yid") Is Nothing Then
                pYear = CInt(Request("yid"))
            Else
                pYear = Year(ctlLct.GET_DATE_SERVER)
                If pYear < 2500 Then
                    pYear = pYear + 543
                End If

            End If
            dt = ctlOrder.LocationOrder_ByYear(pYear, FORM_TYPE_ID_F17, Request.Cookies("LocationID").Value, Session("patientid"))
        End If
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                Session("patientid") = DBNull2Lng(.Item("PatientID"))
                lblID.Text = .Item("itemID")
                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                txtTime.Text = String.Concat(.Item("ServiceTime"))
                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))
                chkClose.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("Status")))


                optQ1.SelectedValue = DBNull2Str(.Item("isProblem1"))
                optQ2.SelectedValue = DBNull2Str(.Item("isProblem2"))
                optQ3.SelectedValue = DBNull2Str(.Item("isProblem3"))
                optQ4.SelectedValue = DBNull2Str(.Item("isProblem4"))
                optQ5.SelectedValue = DBNull2Str(.Item("isProblem5"))
                optQ6.SelectedValue = DBNull2Str(.Item("isProblem6"))
                optQ7.SelectedValue = DBNull2Str(.Item("isProblem7"))
                optQ8.SelectedValue = DBNull2Str(.Item("isProblem8"))

                optAbnormal.SelectedValue = DBNull2Str(.Item("AbnormalRemark"))
                chkEdu1.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate1")))
                chkEdu2.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate2")))
                chkEdu3.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate3")))
                chkEdu4.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate4")))
                chkEdu5.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate5")))
                chkEduOther.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducateOther")))
                txtEduOther.Text = DBNull2Str(.Item("EducateRemark"))

                chkNHSO1.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO1")))
                chkNHSO2.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO2")))
                chkNHSO3.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO3")))
                chkNHSO4.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO4")))
                chkNHSO5.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO5")))
                chkNHSO6.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO6")))
                chkNHSO7.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO7")))
                chkNHSO8.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO8")))

                If DBNull2Zero(.Item("Status")) >= 3 Then
                    cmdSave.Visible = False
                Else
                    If Request.Cookies("LocationID").Value <> String.Concat(.Item("LocationID")) Then
                        LoadLocationData(String.Concat(.Item("LocationID")))
                        If Request("t") = "edit" Then
                            cmdSave.Visible = True
                        Else
                            cmdSave.Visible = False
                        End If
                    Else
                        LoadLocationData(Request.Cookies("LocationID").Value)
                    End If
                End If


            End With
        Else
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing

    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If


        If StrNull2Zero(txtTime.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนระยะเวลาในการให้บริการก่อน');", True)
            Exit Sub
        End If

        Dim objuser As New UserController
        Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))
        
        If lblID.Text = "" Then 'Add new

            If ctlOrder.Order_ChkDupCustomer(FORM_TYPE_ID_F17, Session("patientid"), StrNull2Zero(Left(Str(ServiceDate), 4))) = True Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ผู้รับบริการรายนี้เคยได้รับบริการในกิจกรรมนี้แล้ว');", True)
                Exit Sub
            End If

            ctlOrder.F17_Add(lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F17, Session("patientid"), optQ1.SelectedValue, optQ2.SelectedValue, optQ3.SelectedValue, optQ4.SelectedValue, optQ5.SelectedValue, optQ6.SelectedValue, optQ7.SelectedValue, optQ8.SelectedValue, optAbnormal.SelectedValue, Boolean2Decimal(chkEdu1.Checked), Boolean2Decimal(chkEdu2.Checked), Boolean2Decimal(chkEdu3.Checked), Boolean2Decimal(chkEdu4.Checked), Boolean2Decimal(chkEdu5.Checked), Boolean2Decimal(chkEduOther.Checked), txtEduOther.Text, Boolean2Decimal(chkNHSO1.Checked), Boolean2Decimal(chkNHSO2.Checked), Boolean2Decimal(chkNHSO3.Checked), Boolean2Decimal(chkNHSO4.Checked), Boolean2Decimal(chkNHSO5.Checked), Boolean2Decimal(chkNHSO6.Checked), Boolean2Decimal(chkNHSO7.Checked), Boolean2Decimal(chkNHSO8.Checked), Convert2Status(chkClose.Checked), Request.Cookies("username").Value, CLng(ConvertDate2DBString(ctlLct.GET_DATE_SERVER)))

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Service_Order", "แบบประเมินความเสี่ยงเบื้องต้นโรคต่อการเป็นหลอดเลือดสมอง (F17):" & Session("patientname"), "F17")
        Else
            ctlOrder.F17_Update(StrNull2Zero(lblID.Text), lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F17, Session("patientid"), optQ1.SelectedValue, optQ2.SelectedValue, optQ3.SelectedValue, optQ4.SelectedValue, optQ5.SelectedValue, optQ6.SelectedValue, optQ7.SelectedValue, optQ8.SelectedValue, optAbnormal.SelectedValue, Boolean2Decimal(chkEdu1.Checked), Boolean2Decimal(chkEdu2.Checked), Boolean2Decimal(chkEdu3.Checked), Boolean2Decimal(chkEdu4.Checked), Boolean2Decimal(chkEdu5.Checked), Boolean2Decimal(chkEduOther.Checked), txtEduOther.Text, Boolean2Decimal(chkNHSO1.Checked), Boolean2Decimal(chkNHSO2.Checked), Boolean2Decimal(chkNHSO3.Checked), Boolean2Decimal(chkNHSO4.Checked), Boolean2Decimal(chkNHSO5.Checked), Boolean2Decimal(chkNHSO6.Checked), Boolean2Decimal(chkNHSO7.Checked), Boolean2Decimal(chkNHSO8.Checked), Convert2Status(chkClose.Checked), Request.Cookies("username").Value)

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Service_Order", "แบบประเมินความเสี่ยงเบื้องต้นโรคต่อการเป็นหลอดเลือดสมอง (F17):" & Session("patientname"), "F17")
        End If

        Response.Redirect("ResultPage.aspx?p=F17")

        ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย")
    End Sub

    Protected Sub optQ1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ1.SelectedIndexChanged
        CalScore()
    End Sub

    Protected Sub optQ2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ2.SelectedIndexChanged
        CalScore()
    End Sub

    Protected Sub optQ3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ3.SelectedIndexChanged
        CalScore()
    End Sub

    Protected Sub optQ4_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ4.SelectedIndexChanged
        CalScore()
    End Sub

    Protected Sub optQ5_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ5.SelectedIndexChanged
        CalScore()
    End Sub

    Protected Sub optQ6_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ6.SelectedIndexChanged
        CalScore()
    End Sub

    Protected Sub optQ7_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ7.SelectedIndexChanged
        CalScore()
    End Sub

    Protected Sub optQ8_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optQ8.SelectedIndexChanged
        CalScore()
    End Sub
End Class