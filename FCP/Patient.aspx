﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="Patient.aspx.vb" Inherits=".Patient" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        function autoTab(obj) {
            /* กำหนดรูปแบบข้อความโดยให้ _ แทนค่าอะไรก็ได้ แล้วตามด้วยเครื่องหมาย 
            หรือสัญลักษณ์ที่ใช้แบ่ง เช่นกำหนดเป็น รูปแบบเลขที่บัตรประชาชน 
            4-2215-54125-6-12 ก็สามารถกำหนดเป็น _-____-_____-_-__ 
            รูปแบบเบอร์โทรศัพท์ 08-4521-6521 กำหนดเป็น __-____-____ 
            หรือกำหนดเวลาเช่น 12:45:30 กำหนดเป็น __:__:__ 
            ตัวอย่างข้างล่างเป็นการกำหนดรูปแบบรหัสสินค้า
            รหัสสินค้า 11-BRID-Y1207 
            */
            var pattern = new String("_-____-_____-___"); // กำหนดรูปแบบในนี้ 
            var pattern_ex = new String("-"); // กำหนดสัญลักษณ์หรือเครื่องหมายที่ใช้แบ่งในนี้ 
            var returnText = new String("");
            var obj_l = obj.value.length;
            var obj_l2 = obj_l - 1;
            for (i = 0; i < pattern.length; i++) {
                if (obj_l2 == i && pattern.charAt(i + 1) == pattern_ex) {
                    returnText += obj.value + pattern_ex;
                    obj.value = returnText;
                }
            }
            if (obj_l >= pattern.length) {
                obj.value = obj.value.substr(0, pattern.length);
            }
        }
</script> 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>ข้อมูลผู้รับบริการ
        <small>(Patient)</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    

 <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-users"></i>

              <h3 class="box-title">บันทึกข้อมูลผู้รับบริการ</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"> 
   <tr>
    <td>
      <table width="100%" border="0" cellspacing="2" cellpadding="0">
        <tr>
          <td></td>
          <td width="100" align="right"><span class="NameEN">ID :
              <asp:Label ID="lblID" 
                  runat="server"></asp:Label></span> &nbsp;</td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td valign="top"><table width="100%" border="0" cellspacing="2" cellpadding="0">
     <tr>
    <td colspan="4"><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td width="200" valign="top" class="texttopic">ชื่อ-นามสกุล<img src="images/star.png" width="10" height="10" /></td>
                       <td align="left" valign="top">
            <asp:TextBox ID="txtForeName" runat="server" Width="200"></asp:TextBox>&nbsp;<asp:TextBox ID="txtSurname" runat="server" Width="200"></asp:TextBox></td>
        <td width="100" valign="top" class="texttopic">เพศ</td>
        <td valign="top"><asp:RadioButtonList ID="optGender" runat="server" RepeatDirection="Horizontal">
          <asp:ListItem Selected="True" Value="M">ชาย</asp:ListItem>
          <asp:ListItem Value="F">หญิง</asp:ListItem>
        </asp:RadioButtonList></td>
        </tr>
        <tr>
         <td valign="top" class="texttopic">วัน/เดือน/ปีเกิด</td>
        <td align="left" valign="top">
            <asp:TextBox ID="txtBirthDate" runat="server" AutoPostBack="True" CssClass="form-control text-center" Width="200" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
          &nbsp;(วว/ดด/ปปปป{พ.ศ.})</td>
        <td valign="top" class="texttopic">อายุ<img src="images/star.png" width="10" height="10" /></td>
        <td valign="top"><asp:TextBox ID="txtAges" runat="server" CssClass="text-center"  Width="50px"></asp:TextBox> 
          &nbsp;ปี</td>
      </tr>
      <tr>
        <td valign="top" class="texttopic">เลขบัตร ปชช.</td>
        <td align="left" valign="top"><asp:TextBox ID="txtCardID" runat="server"  Width="200px" MaxLength="16"></asp:TextBox></td>
        <td valign="top" class="texttopic">&nbsp;</td>
        <td valign="top">&nbsp;</td>
        </tr>
      <tr>
        <td valign="top" class="texttopic">เบอร์โทรศัพท์บ้าน</td>
        <td align="left" valign="top" colspan="3">
            <table>
                <tr>
                    <td><asp:TextBox ID="txtTelephone" runat="server" Width="200px"></asp:TextBox></td>
                    <td>มือถือ<img src="images/star.png" width="10" height="10" /></td>
                    <td><asp:TextBox ID="txtMobile" runat="server" Width="200px"></asp:TextBox></td>
                    <td>เวลาที่สะดวกให้ติดต่อกลับ</td>
                    <td>
                        <asp:TextBox ID="txtTimeContact" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
          </td>
        </tr>
      <tr>
        <td valign="top" class="texttopic">ที่อยู่</td>
        <td colspan="3" align="left" valign="top"><asp:RadioButtonList ID="optAddressType" runat="server" 
                RepeatDirection="Horizontal" CellPadding="0" CellSpacing="0">
          <asp:ListItem Selected="True" Value="LIVE">ที่อยู่ปัจจุบัน</asp:ListItem>
          <asp:ListItem Value="CARD">ที่อยู่ตามบัตรประชาชน</asp:ListItem>
        </asp:RadioButtonList></td>
        </tr>
      <tr>
        <td valign="top" class="texttopic">บ้านเลขที่/หมู่บ้าน/อาคาร/ซอย</td>
        <td align="left" valign="top"><asp:TextBox ID="txtAddress" runat="server" Width="250px"></asp:TextBox></td>
        <td valign="top" class="texttopic">ถนน</td>
        <td valign="top"><asp:TextBox ID="txtRoad" runat="server"></asp:TextBox></td>
      </tr>
      <tr>
        <td valign="top" class="texttopic">ตำบล/แขวง</td>
        <td align="left" valign="top"><asp:TextBox ID="txtDistrict" runat="server" Width="200px"></asp:TextBox></td>
        <td valign="top" class="texttopic">อำเภอ/เขต</td>
        <td valign="top"><asp:TextBox ID="txtCity" runat="server"></asp:TextBox></td>
      </tr>
      <tr>
        <td valign="top" class="texttopic">จังหวัด</td>
        <td align="left" valign="top"><asp:DropDownList CssClass="OptionControl" ID="ddlProvince" runat="server"> </asp:DropDownList></td>
        <td valign="top" class="texttopic">รหัสไปรษณีย์</td>
        <td valign="top"><asp:TextBox ID="txtZipCode" runat="server"></asp:TextBox></td>
      </tr>           

       <tr>
        <td valign="top" class="texttopic">อาชีพ</td>
        <td colspan="3" align="left" valign="top"><asp:RadioButtonList ID="optOccupation" runat="server" RepeatColumns="2">
          <asp:ListItem Selected="True" Value="0">ไม่ระบุ</asp:ListItem>
          <asp:ListItem Value="1">ไม่มีอาชีพ</asp:ListItem>
          <asp:ListItem Value="2">เกษตรกรรม</asp:ListItem>
          <asp:ListItem Value="3">ข้าราชการ/รัฐวิสาหกิจ/พนักงานของรัฐ</asp:ListItem>
          <asp:ListItem Value="4"> พนักงานหน่วยงานเอกชนหรือลูกจ้าง/ค้าขาย/ธุรกิจส่วนตัว</asp:ListItem>
          <asp:ListItem Value="5"> นักเรียน/นักศึกษา</asp:ListItem>
          <asp:ListItem Value="6"> พระ/นักบวช</asp:ListItem>
         <asp:ListItem Value="7"> รับจ้าง/รับจ้างทั่วไป (ไม่มีประกันสังคม)</asp:ListItem>

        </asp:RadioButtonList>

        </td>
      </tr>
      <tr>
        <td valign="top" class="texttopic">สิทธิการรักษา</td>
        <td colspan="3" align="left" valign="top"><asp:RadioButtonList ID="optClaim" runat="server" RepeatDirection="Horizontal" RepeatColumns="2">
          <asp:ListItem Selected="True" Value="UCS">ประกันสุขภาพถ้วนหน้า</asp:ListItem>
          <asp:ListItem Value="SSS">ประกันสังคม</asp:ListItem>
          <asp:ListItem Value="OFC">ข้าราชการ/รัฐวิสาหกิจ</asp:ListItem>
          <asp:ListItem Value="NON">ทราบสิทธิแต่ประสงค์จ่ายเงินเอง</asp:ListItem>
            <asp:ListItem Value="INS">ประกันชีวิต</asp:ListItem>
            <asp:ListItem Value="0">ไม่ระบุ</asp:ListItem>
        </asp:RadioButtonList></td>
        </tr>
    
      
      <tr>
        <td valign="top" class="texttopic">สถานะ</td>
        <td align="left" valign="top"><asp:CheckBox ID="chkClose" runat="server" Text="Active" Checked="True" /></td>
        <td valign="top" class="texttopic">&nbsp;</td>
        <td valign="top">&nbsp;</td>
      </tr>
    </table></td>
  
            
    </table></td>
  </tr>
 
 </table>

                  </div>
           
          </div>

    
<div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">ประวัติแพ้ยา</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">   
          <table align="left" class="table">
        
              <tr>
                  <td width="100"> แพ้ยาหรือไม่</td>
                 
                  <td> 
                      <asp:RadioButtonList ID="optAllergy" runat="server" RepeatDirection="Horizontal">
                          <asp:ListItem Value="N">ไม่แพ้</asp:ListItem>
                          <asp:ListItem Value="Y">แพ้</asp:ListItem>
                      </asp:RadioButtonList>
                  </td>
                 
              </tr>
              
             
              
             
               <tr>
                  <td>
                      ระบุชื่อยา</td>
                 
                  <td>
                      <asp:TextBox ID="txtDrugAllergy" runat="server"   Width="100%" MaxLength="1000"></asp:TextBox>
                   </td>
                 
              </tr>
              <tr>
                  <td> </td>
                 
                  <td>
                      กรณีไม่ทราบชื่อยา โปรดระบุว่าเป็นยาใช้รักษาอาการ / โรคอะไร</td>
                 
              </tr>
              
             
        </table>
    
      </div>
</div>

<div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">IN HOMESSS (ข้อมูลสำหรับโครงการเยี่ยมบ้าน)</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">   
          <table align="left" class="table">
        
              <tr>
                  <td>I=Inmobility&nbsp;เขาเดินได้ไหม  &nbsp; N=Nutrition   เขากินอย่างไร  &nbsp; H=Housing     เขาอยู่อย่างไร&nbsp; O=Other people 
                      บุคคลอื่นที่เกี่ยวข้อง&nbsp;  M=Medication   กินอะไรเป็นยา &nbsp; E=Examination  มาตรวจร่างกาย&nbsp; S=Safety     ปลอดภัยหรือเปล่า&nbsp; S=Spirituality   เข้าใจความหมายชีวิต &nbsp; S=Service   ฉุกคิดสถานพยาบาลที่มี</td>
                 
              </tr>
               <tr>
                  <td>
                      <asp:TextBox ID="txtHomesss" runat="server" CssClass="OptionControl" Height="100px" TextMode="MultiLine" Width="100%" MaxLength="1000"></asp:TextBox>
                   </td>
                 
              </tr>
             
              
             
        </table>
    
      </div>
</div>


    
<div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">คัดกรองการสูบบุหรี่</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">    
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>1.ปัจจุบันสูบบุหรี่หรือไม่</label>
                  <asp:RadioButtonList ID="optSmoke" runat="server" RepeatDirection="Horizontal"> 
                    <asp:ListItem Value="Y">สูบ</asp:ListItem>
                    <asp:ListItem Value="N">ไม่สูบ</asp:ListItem>
                  </asp:RadioButtonList>
                </div>
              </div>
                <div class="col-md-6">
                <div class="form-group">
                  <label>2.อยากจะลดหรือเลิกสูบบุหรี่หรือไม่</label>
                  <asp:RadioButtonList ID="optSmokingQuit" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="1">อยากลด</asp:ListItem>
                    <asp:ListItem Value="2">อยากเลิก</asp:ListItem>
                    <asp:ListItem Value="3">ยังไม่คิด/เฉยๆ</asp:ListItem>
                    <asp:ListItem Value="0">ไม่สูบบุหรี่</asp:ListItem>
                  </asp:RadioButtonList>
                </div>
              </div>
            </div>
                <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label>3.Remark (ตย.เช่น ผป.อยากรับบริการที่ร้าน/ผป.สามารถรับบริการ Tele ได้ หรือรายละเอียดอื่นๆ กรณีเภสัชกรติดต่อไม่ได้/ไม่รับสาย)</label>
                    <asp:TextBox ID="txtSmokeRemark" runat="server" TextMode="MultiLine" Height="50" CssClass="form-control"></asp:TextBox>
                </div>
              </div>
            </div>
      </div>
</div>

    <div align="center">  
        <asp:Button ID="cmdSave" runat="server" CssClass="btn btn-primary" Text="บันทึก" Width="100px" />
&nbsp;<asp:Button ID="cmdDelete" runat="server" CssClass="btn btn-danger" Text="ลบ" Width="100px" />
&nbsp;<asp:Button ID="cmdClear" runat="server" CssClass="btn btn-success" Text="เพิ่มผู้รับบริการใหม่" />
     
  </div>   <br />
<div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Patient</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">   
          <table align="left" class="table">        
              <tr>
                  <td>
                               <table border="0" cellspacing="2" cellpadding="0">
            <tr>
              <td width="50" >ค้นหา</td>
                <td   > 
                    <asp:CheckBox ID="chkisLocation" runat="server" Text="เฉพาะผู้รับบริการที่ร้านเพิ่มเอง" />
                    </td>
              <td >
                  <asp:TextBox ID="txtSearch" runat="server" Width="200px"></asp:TextBox>                </td>
              <td >
                  <asp:Button ID="cmdFind" runat="server" CssClass="buttonFind" Text="ค้นหา" />
                </td>
            </tr>
            
          </table>

                  </td>                 
              </tr>
              <tr>
                  <td>
              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" CssClass="table table-hover">           
            <columns>
                <asp:BoundField HeaderText="PID" DataField="PatientID" >
                <ItemStyle HorizontalAlign="Center" Width="70px" />                </asp:BoundField>
                <asp:BoundField DataField="ForeName" HeaderText="ชื่อ" />
                <asp:BoundField DataField="Surname" HeaderText="นามสกุล" />
                <asp:BoundField DataField="Ages" HeaderText="อายุ" >
                <ItemStyle HorizontalAlign="Center" Width="30px" />                </asp:BoundField>
            <asp:BoundField HeaderText="เพศ" DataField="Gender">                      
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="70px" />                      </asp:BoundField>
                <asp:TemplateField HeaderText="เลขบัตร ปชช.">
                    <ItemTemplate>
                        <asp:Label ID="lblCardID" runat="server"> <%# FormatCardID(DBNull2Str(DataBinder.Eval(Container.DataItem, "CardID")))%></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="จังหวัด" DataField="ProvinceName" >
                </asp:BoundField>
                <asp:TemplateField>
                    <ItemTemplate> 
                     <asp:ImageButton     ID="imgEdit" runat="server" 
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PatientID")%>' 
                                        ImageUrl="images/icon-edit.png" Visible='<%# DataBinder.Eval(Container.DataItem, "isLocation")%>'  />
                                 <!--
                                     <asp:ImageButton ID="imgDel"  cssclass="gridbutton"  runat="server" 
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PatientID")%>' 
                                        ImageUrl="images/icon-delete.png" Visible='<%# DataBinder.Eval(Container.DataItem, "isLocation")%>'  />       -->             
                                     </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="45px" />
                </asp:TemplateField>
            </columns>          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />          
           
            <headerstyle  Font-Bold="True" 
                      VerticalAlign="Middle" HorizontalAlign="Center" />          
           
          </asp:GridView>
                  </td>                 
              </tr>
        </table>
    
      </div>
</div>

</section>

</asp:Content>
 

