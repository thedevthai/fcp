﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="vmiStockReceive.aspx.vb" Inherits=".vmiStockReceive" %>
<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>บันทึกรับสินค้า / Inventory Receive
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
  <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">บันทึกรับสินค้า</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">




 
     
                     <table width="100%" border="0" cellspacing="0" cellpadding="0">
        
          
          <tr>
            <td bgcolor="#FFFFFF">
            
            
            
            <TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" bgColor="#ffffff" border="0">
<TBODY>
					<TR>
						<TD>
						  <TABLE id="Table2" height="100%" cellSpacing="2" cellPadding="0" width="100%" border="0">
				  <TR>
									<TD  valign="top">
                                    
                                    <table border="0" cellspacing="2" cellpadding="2">
                                     
                                      <tr>
                                        <td width="100">Trans.ID</td>
                                        <td>
                                            <asp:Label ID="lblUID" runat="server"></asp:Label>
                                          </td>
                                      </tr>
                                     
                                      <tr>
                                        <td width="100">Item Name</td>
                                        <td>
                                            <asp:Label ID="lblMedName" runat="server"></asp:Label>
                                          </td>
                                      </tr>
                                      <tr>
                                        <td>QTY</td>
                                        <td>
                                            <asp:Label ID="lblQTY" runat="server"></asp:Label>
                                          </td>
                                      </tr> <tr>
                                        <td width="100">Date Receive</td>
                                        <td>
                                            <dx:ASPxDateEdit ID="dtpDate" runat="server" Theme="PlasticBlue">
                                            </dx:ASPxDateEdit>
                                          </td>
                                      </tr>
                                      <tr>
                                        <td>&nbsp;</td>
                                        <td align="left">
                                            <asp:Button ID="cmdSave" runat="server" CssClass="buttonRedial" Text="Save" Width="90px" />
                                          </td>
                                      </tr>
                                      <tr>
                                        <td colspan="2"><asp:Label ID="lblvalidate1" runat="server" CssClass="validateAlert" 
                                                Visible="False" Width="99%"></asp:Label></td>
                                      </tr>
                                    </table></TD>
</TR>
				 
       <tr>
          <td  align="left" valign="top" class="skin_Search">
              
              <table>
                  <tr>
                      <td>กรอง :</td>
                      <td><asp:RadioButtonList ID="optStatus" runat="server" RepeatDirection="Horizontal" AutoPostBack="True">
              <asp:ListItem Selected="True" Value="A">ทั้งหมด</asp:ListItem>
              <asp:ListItem Value="P">Pending</asp:ListItem>
              <asp:ListItem Value="R">Received</asp:ListItem>
              </asp:RadioButtonList>
                      </td>
                  </tr>
              </table>
              </td>
      </tr>
				  <TR>
				     <TD  valign="top" class="text12b_nblue"><asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" CssClass="txtcontent" 
                             Font-Bold="False" PageSize="20">
                        <RowStyle BackColor="#F7F7F7" />
                        <columns>
                            <asp:BoundField HeaderText="Trans. ID" DataField="UID">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />                            </asp:BoundField>
<asp:BoundField HeaderText="Inv. Date" DataField="INVDATE"></asp:BoundField>
                            <asp:BoundField DataField="itemName" HeaderText="Item Name" />
                            <asp:BoundField DataField="QTY" DataFormatString="{0:#,###}" HeaderText="QTY" />
                            <asp:BoundField HeaderText="Status" DataField="StatusName" />
                        <asp:TemplateField HeaderText="Received">
                            <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgEdit" runat="server" 
                                    ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID")%>' />                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                        </columns>
                        <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />                     
                        <pagerstyle HorizontalAlign="Center" 
                             CssClass="dc_pagination dc_paginationC dc_paginationC11" />                     
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Left" />                     
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                     </asp:GridView>  </TD>
				     </TR>
				  <TR>
				     <TD  valign="top">&nbsp;</TD>
				     </TR>
								</TABLE>					  </TD>
		  </TR>
				</TBODY>
			</TABLE>            </td>
            </tr>
         
        </table>    
                 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>   
</section>
</asp:Content>
