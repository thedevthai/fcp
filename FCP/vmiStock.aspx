﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="vmiStock.aspx.vb" Inherits=".vmiStock" %>


<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1> Medicine Stock 
        <small>: ตั้งค่า Stock และ ROP ของร้านยา</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
  
<div class="box box-success">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ตั้งค่า Stock และ ROP ของร้านยา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">




    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td align="left" valign="top">
      <table border="0" cellpadding="2" cellspacing="2">
                                      <tr>
                                          <td width="30">&nbsp;</td>
                                          <td width="70">ยา</td>
                                          <td width="200">
                                              <asp:Label ID="lblMedName" runat="server"></asp:Label>
                                          </td>
                                          <td width="70">หน่วย</td>
                                          <td>
                                              <asp:Label ID="lblUOM" runat="server"></asp:Label>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>&nbsp;</td>
                                          <td>Stock</td>
                                          <td>
                                              <asp:TextBox ID="txtStock" runat="server" Width="80px"></asp:TextBox>
                                          </td>
                                          <td>ROP</td>
                                          <td class="auto-style1">
                                              <asp:TextBox ID="txtROP" runat="server" Width="80px"></asp:TextBox>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>&nbsp;</td>
                                          <td>On hand</td>
                                          <td>
                                              <asp:TextBox ID="txtBalance" runat="server" Width="80px"></asp:TextBox>
                                          </td>
                                          <td>Ref.ID</td>
                                          <td>
                                              <asp:Label ID="lblUID" runat="server"></asp:Label>
                                              [<asp:Label ID="lblMedUID" runat="server"></asp:Label>
                                              ]</td>
                                      </tr>
                                      <tr>
                                          <td align="center">
                                              &nbsp;</td>
                                          <td align="center" colspan="4">
                                              <asp:Button ID="cmdSave" runat="server" CssClass="buttonRedial " Text="Save" Width="100px" />
                                              &nbsp;<asp:Button ID="cmdCancel" runat="server" CssClass="buttonRedial " Text="Cancel" Width="100px" />
                                          </td>
                                      </tr>
                                  </table>
 </td>
      </tr>
       <tr>
          <td align="left" valign="top" class="MenuSt"> &nbsp;&nbsp;&nbsp;&nbsp;รายการสต๊อกยา</td>
      </tr>
        <tr>
          <td align="center" valign="top">
              <asp:GridView ID="grdData" 
                             runat="server" CellPadding="0" ForeColor="#333333" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%">
            <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
            <columns>
            <asp:BoundField HeaderText="No." DataField="nRow">
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />                      </asp:BoundField>
                <asp:BoundField DataField="itemName" HeaderText="ชื่อยา" >
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="Stock" HeaderText="Stock" />
                <asp:BoundField DataField="ROP" HeaderText="ROP" />
                <asp:BoundField DataField="OnHand" HeaderText="On hand" />
            <asp:TemplateField HeaderText="Edit">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "MedUID")%>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Delete">
              <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgDel" runat="server" 
                                    ImageUrl="images/icon-delete.png" 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "MedUID")%>' />                        </itemtemplate>
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />          
            </asp:TemplateField>
            </columns>
            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />          
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC12" />          
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <headerstyle CssClass="th" Font-Bold="True" HorizontalAlign="Center" 
                      VerticalAlign="Middle" />          
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
          </asp:GridView></td>
      </tr>
       
    </table>
     </div>
            <div class="box-footer clearfix">
           
            </div>
          </div> 
    </section>
</asp:Content>
