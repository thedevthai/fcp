﻿Imports System.Net.Mail
Imports System.Web
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Net
Imports DevExpress.XtraPrinting.Export.Pdf

Public Class RegisterNew
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlM As New MasterController
    Dim ctlL As New LocationController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If IsNothing(Request.Cookies("CPAQA")) Then
        '    Response.Redirect("Default.aspx")
        'End If
        If Not IsPostBack Then
            LoadProvinceToDDL()
            LoadLocationGroupToDDL()
            LoadLocationTypeToCheckList()

            txtLicenseNo.Text = Session("LicenseRegister")

            If Request("t") = "1" Then
                lblLicensTxt.Text = "เลขที่ใบอนุญาต ขย.5"
                If txtLicenseNo.Text <> "" Then
                    LoadLocationDataFromFDA()
                End If
            ElseIf Request("t") = "2" Then
                lblLicensTxt.Text = "เลขที่ใบประกอบวิชาชีพ"
            End If
        End If
        txtQAYear.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        'txtArea1.Attributes.Add("OnKeyPress", "return AllowOnlyDouble();")
        'txtArea2.Attributes.Add("OnKeyPress", "return AllowOnlyDouble();")
        txtZipCode.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtEmail.Attributes.Add("OnKeyPress", "return NotAllowThai();")
    End Sub
    Function checkField(tD As DataTable, ColumnName As String) As String
        If dt.Columns(ColumnName) IsNot Nothing Then
            Return String.Concat(dt.Rows(0)(ColumnName))
        Else
            Return ""
        End If
    End Function

    Private Sub LoadLocationDataFromFDA()
        Dim ctlFDA As New FDAServiceController
        Dim NewCode As String
        NewCode = ctlFDA.ConvertLicenseToNewCode(Session("LicenseRegister"))
        lblNewCode.Text = NewCode
        Try
            dt = ctlFDA.GET_DRUG_LCN_INFORMATION(NewCode)
            If dt.Rows.Count > 0 Then
                With dt.Rows(0)
                    txtLicenseNo.Text = Replace(Replace(String.Concat(.Item("lcnno_no")), "ขย1 ", ""), " ", ".")
                    txtLocationName.Text = checkField(dt, "thanm")
                    txtAddressNo.Text = checkField(dt, "thanm_address") '& " " & checkField(dt, "tharoad")
                    ddlProvince.SelectedValue = checkField(dt, "pvncd")
                    txtZipCode.Text = checkField(dt, "zipcode")
                    txtTel.Text = checkField(dt, "tel")
                    txtCo_Name.Text = checkField(dt, "grannm_lo")
                    txtWorkTime.Text = checkField(dt, "licen_time")
                    'lcnno                   รหัสผู้ประกอบการ
                    'thanm                   ชื่อสถานที่(ร้าน)
                    'pvncd                   รหัสจังหวัด
                    'lcntpcd                 ประเภทใบอนุญาตสถานที่ด้าน ยา
                    'lcnsid                  รหัสผู้ประกอบการ
                    'GROUPNAME               ประเภทสถานที่
                    'lcnno_no                เลขใบอนุญาตสถานที่ด้านยา
                    'lcnno_noo               เลขใบอนุญาตสถานที่ด้านยา
                    'lcnno_not_pvnabbr       เลขใบอนุญาตสถานที่ด้านยา 
                    'typee                   ประเภทใบอนุญาตสถานที่ด้าน ยา
                    'licen                   ชื่อผู้รับอนุญาต
                    'licen_addr              -
                    'licen_address           -
                    'licen_time              เวลาทำการของร้าน
                    'grannm_lo               ชื่อผู้ดำเนินกิจการ
                    'grannm_addr              -
                    'grannm_address           -
                    'thaaddr                 ที่อยู่สถานที่ 
                    'tharoom                 ห้อง
                    'thafloor                ชั้น
                    'thabuilding             อาคาร
                    'thasoi                  ซอย
                    'tharoad                 ถนน
                    'thamu                   หมู่
                    'thathmblnm              ตำบล
                    'zipcode                 รหัสไปรษณีย์
                    'tel                     เบอร์โทรศัพท์
                    'fax                     เบอร์โทรสาร
                    'thanm_addr              ที่อยู่สถานที่ แบบเต็ม
                    'thanm_address           ที่อยู่สถานที่ แบบเต็ม
                    'thaamphrnm              อำเภอ
                    'thachngwtnm             จังหวัด
                    'cncnm                   สถานะใบอนุญาต
                    'appdate                 วันที่อนุญาต
                    'expyear                 ปีที่หมดอายุ
                    'lmdfdate                Last update
                    'grouptype               กลุ่มใบอนุญาต
                    'Newcode_not             รหัส Newcode
                End With
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub LoadProvinceToDDL()
        dt = ctlM.Province_Get
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .DataSource = dt
                .DataTextField = "ProvinceName"
                .DataValueField = "ProvinceID"
                .DataBind()
                '.SelectedIndex = 0
            End With
        End If
        dt = Nothing
    End Sub
    Private Sub LoadLocationGroupToDDL()
        dt = ctlL.LocationGroup_Get
        If dt.Rows.Count > 0 Then
            With ddlGroup
                .Enabled = True
                .DataSource = dt
                .DataTextField = "Name"
                .DataValueField = "Code"
                .DataBind()
            End With
        End If
        dt = Nothing
    End Sub


    Private Sub LoadLocationTypeToCheckList()
        dt = ctlL.LocationType_Get
        If dt.Rows.Count > 0 Then
            optLocationType.DataSource = dt
            optLocationType.DataTextField = "Name"
            optLocationType.DataValueField = "UID"
            optLocationType.DataBind()
        End If
        dt = Nothing
    End Sub

    Function Check_Email(str As String) As Boolean
        Return Regex.IsMatch(str, "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
    End Function
    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click
        If txtLicenseNo.Text <> "" And ctlL.Register_SearchByLicense(txtLicenseNo.Text) > 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','เลขที่ใบอนุญาต/ใบประกอบวิชาชีพนี้ได้ทำการลงทะเบียนแล้ว ไม่สามารถลงทะเบียนได้อีก');", True)
            Exit Sub
        End If
        If txtLicenseNoPharm.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุเลขที่ใบประกอบวิชาชีพ คู่สัญญาก่อน');", True)
            Exit Sub
        End If

        If Request("t") = "1" Then
            If txtLicenseNo.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรอกเลขที่เลขที่ใบอนุญาต (ขย.) ก่อน');", True)
                Exit Sub
            End If

            If ctlL.Location_SearchByLicense(txtLicenseNo.Text) > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','เลขที่ใบอนุญาตนี้มี User ในระบบแล้ว กรุณา Login ด้วยรหัสผู้ใช้งานของท่าน');", True)
                Exit Sub
            End If

            If IsNumeric(Left(txtLicenseNo.Text, 1)) Or IsNumeric(Mid(txtLicenseNo.Text, 2, 1)) Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุเลขที่ใบอนุญาตขายยา (ขย.5) 2 ตัวแรกด้วยรหัสจังหวัด');", True)
                Exit Sub
            End If

            If Len(Trim(txtLicenseNo.Text)) < 8 Or Len(Trim(txtLicenseNo.Text)) > 12 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุเลขที่ใบอนุญาตขายยา (ขย.5) ให้ถูกต้อง');", True)
                Exit Sub
            End If

        ElseIf Request("t") = "2" Then
            If txtLicenseNo.Text = "" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรอกเลขที่เลขที่ใบประกอบวิชาชีพ ก่อน');", True)
                Exit Sub
            End If

            If ctlL.Location_SearchByLicense(txtLicenseNo.Text) > 0 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','เลขที่ใบประกอบวิชาชีพนี้มี User ในระบบแล้ว กรุณา Login ด้วยรหัสผู้ใช้งานของท่าน');", True)
                Exit Sub
            End If

            If IsNumeric(Left(txtLicenseNo.Text, 1)) Or IsNumeric(Mid(txtLicenseNo.Text, 2, 1)) Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุเลขที่ใบประกอบวิชาชีพ 2 ตัวแรกด้วย ภ.');", True)
                Exit Sub
            End If

            If Len(Trim(txtLicenseNo.Text)) < 5 Or Len(Trim(txtLicenseNo.Text)) > 8 Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุเลขที่ใบประกอบวิชาชีพให้ถูกต้อง');", True)
                Exit Sub
            End If
        End If

        If txtLocationName.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุชื่อร้านยา');", True)
            Exit Sub
        End If
        If txtAddressNo.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุบ้านเลขที่/เลขที่ตั้ง');", True)
            Exit Sub
        End If
        If txtZipCode.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุรหัสไปรษณีย์');", True)
            Exit Sub
        End If
        If txtTel.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุเบอร์โทร');", True)
            Exit Sub
        End If
        If txtEmail.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุอีเมล');", True)
            Exit Sub
        End If
        If Check_Email(txtEmail.Text) = False Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','รูปแบบอีเมลไม่ถูกต้อง กรุณาตรวจสอบ');", True)
            Exit Sub
        End If

        If txtCo_Name.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningAlert(this,'ผลการตรวจสอบ','กรุณาระบุชื่อเจ้าของ(คู่สัญญา)');", True)
            Exit Sub
        End If

        Dim sLat, sLng, sL() As String
        sLat = ""
        sLng = ""
        If txtLat.Text <> "" Then
            sL = Split(Replace(txtLat.Text, " ", ""), ",")
            sLat = sL(0)
            If sL.Length > 1 Then
                sLng = sL(1)
            End If

            If IsNumeric(sLat) = False Or IsNumeric(sLng) = False Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModalWarningInfo(this,'ผลการตรวจสอบ','กรุณาระบุละติจูด/ลองติจูด เป็นรูปแบบองศาทศนิยมเท่านั้น ');", True)
                Exit Sub
            End If
        End If

        'Dim Generator As System.Random = New System.Random()
        'Dim sPassword As String = Generator.Next(1000, 9999).ToString()
        'Dim enc As New CryptographyEngine

        ctlL.Location_Register(txtLicenseNo.Text, txtLocationName.Text, txtNHSOCode.Text, optLocationType.SelectedValue, ddlGroup.SelectedValue, txtAddressNo.Text, ddlProvince.SelectedValue, ddlProvince.SelectedItem.Text, txtZipCode.Text, txtTel.Text, txtEmail.Text, txtLineID.Text, sLat, sLng, txtWorkTime.Text, StrNull2Zero(txtQAYear.Text), txtCo_Name.Text, txtCo_Tel.Text, txtCo_Mail.Text, txtLicenseNoPharm.Text)

        Dim ctlU As New UserController
        ctlU.User_GenLogfile(0, ACTTYPE_ADD, "Locations", "ลงทะเบียนใหม่", "[LicenseNo=" & txtLicenseNo.Text & "][LocationName=" & txtLocationName.Text & "]")
        Response.Redirect("RegisterComplete.aspx")
    End Sub

End Class