﻿Imports Microsoft.Reporting.WebForms
Imports System.IO
Public Class ReportSmokingCondition
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If
        UpdateProgress1.Visible = False
        If Not IsPostBack Then
            lblSEQ.Visible = False
            ddlFollowSEQ.Visible = False
            Select Case Request("itemType")
                Case "smsum1"
                    FagRPT = "SmokingSummary"
                    lblReportName.Text = "รายงานสรุปการให้บริการ"
                    ReportsName = "SmokingSummaryService2"
                    Reportskey = "XLS"
                Case "smfn1"
                    FagRPT = "SmokingFinance"
                    lblReportName.Text = "รายงานสรุปการให้บริการ"
                    ReportsName = "SmokingFinanceService"
                    Reportskey = "XLS"
                Case "smstop"
                    FagRPT = "SmokingFinanceStopDay"
                    lblReportName.Text = "รายงานสรุปจำนวนวันหยุดสูบ"
                    ReportsName = "SmokingFinanceStopDay"
                    Reportskey = "XLS"
                Case "smstopdate"
                    FagRPT = "SmokingFinanceStopDate"
                    lblReportName.Text = "รายงานวันที่หยุดสูบ"
                    ReportsName = "SmokingFinanceStopDate"
                    Reportskey = "XLS"

                Case "sm2"
                    FagRPT = "SmokingDetailA1"
                    lblReportName.Text = "รายงานการให้บริการ A1-A3"
                    ReportsName = "SmokingServiceA1A3"
                    Reportskey = "XLS"
                Case "sm3"
                    FagRPT = "SmokingDetailA4"
                    lblReportName.Text = "รายงานการให้บริการ A4"
                    ReportsName = "SmokingServiceA4"
                    Reportskey = "XLS"
                Case "sm4"
                    FagRPT = "SmokingDetailA5"
                    lblReportName.Text = "รายงานการให้บริการ A5"
                    ReportsName = "SmokingServiceA5"
                    Reportskey = "XLS"
                    lblSEQ.Visible = True
                    ddlFollowSEQ.Visible = True
                Case "smpq"
                    FagRPT = "PatientQuitline"
                    lblReportName.Text = "รายงานผู้ที่ต้องการเลิกบุหรี่"
                    ReportsName = "PatientQuitline"
                    Reportskey = "XLS"
            End Select


        End If

    End Sub

    Protected Sub cmdView_Click(sender As Object, e As EventArgs) Handles cmdView.Click

        'UpdateProgress1.DisplayAfter = 0
        'UpdateProgress1.Visible = True
        'LoadReport()
        If txtStartDate.Text = "" Or txtEndDate.Text = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ท่านยังไม่ได้ใส่วันที่ที่ต้องการ');", True)
            Exit Sub
        End If

        Dim dStart As String = ConvertStrDate2DBString(txtStartDate.Text)
        Dim dEnd As String = ConvertStrDate2DBString(txtEndDate.Text)

        Response.Redirect("ReportViewer.aspx?ex=1&b=" & dStart & "&e=" & dEnd & "&seq=" & ddlFollowSEQ.SelectedValue)

    End Sub

    'Private Sub LoadReport()

    '    'System.Threading.Thread.Sleep(1000)
    '    'UpdateProgress1.Visible = True

    '    Dim credential As New ReportServerCredentials
    '    ReportViewer1.Reset()
    '    ReportViewer1.ServerReport.ReportServerCredentials = credential
    '    ReportViewer1.ServerReport.ReportServerUrl = credential.ReportServerUrl
    '    ReportViewer1.ShowPrintButton = True

    '    'System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("th-TH")
    '    Dim xParam As New List(Of ReportParameter)

    '    ReportViewer1.ServerReport.ReportPath = credential.ReportPath(ReportsName)
    '    Dim xBeginDate As Long = ConvertStrDate2DBString(txtStartDate.Text)
    '    Dim xEndDate As Long = ConvertStrDate2DBString(txtEndDate.Text)
    '    xParam.Add(New ReportParameter("BYear", StrNull2Zero(ddlYear.SelectedValue)))
    '    xParam.Add(New ReportParameter("StartDate", xBeginDate))
    '    xParam.Add(New ReportParameter("EndDate", xEndDate))

    '    ReportViewer1.ServerReport.SetParameters(xParam)

    '    Dim warnings As Warning()
    '    Dim streamIds As String()
    '    Dim mimeType As String = String.Empty
    '    Dim encoding As String = String.Empty
    '    Dim extension As String = String.Empty

    '    Dim bytes As Byte() = ReportViewer1.ServerReport.Render("EXCEL", Nothing, mimeType, encoding, extension, streamIds, Nothing)

    '    Response.Buffer = True
    '    Response.Clear()
    '    Response.ContentType = mimeType
    '    Response.AddHeader("content-disposition", Convert.ToString("attachment; filename=" & ReportsName & "_" & ConvertStrDate2DBString(txtStartDate.Text) & "_" & ConvertStrDate2DBString(txtEndDate.Text) & "." & extension))
    '    Response.BinaryWrite(bytes)
    '    ' create the file
    '    Response.Flush()


    'End Sub

End Class

