﻿Public Class FormDirect
    Inherits System.Web.UI.Page
    Dim ctlS As New OrderController
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim svTypeID As String = Request("ServiceTypeID")

        Session("patientid") = ctlS.ServiceOrder_GetPatientIDByItemID(StrNull2Long(Request("itemId")), StrNull2Zero(Request("ProjID")))

        If svTypeID = FORM_TYPE_ID_F11F Then
            Response.Redirect("F11_Follow.aspx?t=edit&acttype=" & Request("acttype") & "&fid=" & Request("itemID"))
        ElseIf svTypeID = FORM_TYPE_ID_F02 Then
            Response.Redirect("F02.aspx?t=edit&acttype=" & Request("acttype") & "&fid=" & Request("itemID"))
        ElseIf svTypeID = FORM_TYPE_ID_F03 Then
            Response.Redirect("F03.aspx?t=edit&acttype=" & Request("acttype") & "&fid=" & Request("itemID"))
        ElseIf svTypeID = FORM_TYPE_ID_A4F Then
            Response.Redirect("A4.aspx?t=edit&acttype=" & Request("acttype") & "&fid=" & Request("itemID"))
        ElseIf svTypeID = FORM_TYPE_ID_A05 Then
            Response.Redirect("A5F.aspx?t=edit&acttype=" & Request("acttype") & "&fid=" & Request("itemID"))
        ElseIf svTypeID = FORM_TYPE_ID_A1 Then
            Response.Redirect("A1.aspx?t=edit&acttype=" & Request("acttype") & "&fid=" & Request("itemID"))
        ElseIf svTypeID = FORM_TYPE_ID_A5 Then
            Response.Redirect("A5.aspx?t=edit&acttype=" & Request("acttype") & "&fid=" & Request("itemID"))

        Else
            Response.Redirect("" & svTypeID & ".aspx?t=edit&acttype=" & Request("acttype") & "&fid=" & Request("itemID"))
        End If


    End Sub

End Class