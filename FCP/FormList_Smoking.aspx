﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteNoAjax.Master" CodeBehind="FormList_Smoking.aspx.vb" Inherits=".FormList_Smoking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">       
</asp:Content>
    
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>รายการกิจกรรมโครงการบุหรี่
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">รายการกิจกรรมโครงการบุหรี่</li>
      </ol>
    </section>

<section class="content">  


       <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-search"></i>

              <h3 class="box-title">ค้นหา</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">  
                <table border="0" align="center" cellPadding="1" cellSpacing="1">
<tr>
  <td align="left" class="texttopic">ปี :</td>
  <td align="left" class="texttopic">
                                                      <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" 
                                                          CssClass="OptionControl">
                                                          <asp:ListItem Selected="True" Value="0">-- ทั้งหมด --</asp:ListItem>
                                                          <asp:ListItem>2557</asp:ListItem>
                                                          <asp:ListItem>2558</asp:ListItem>
                                                          <asp:ListItem>2559</asp:ListItem>
                                                          <asp:ListItem>2560</asp:ListItem>
                                                          <asp:ListItem>2561</asp:ListItem>
                                                          <asp:ListItem>2562</asp:ListItem>
                                                          <asp:ListItem>2563</asp:ListItem>
                                                          <asp:ListItem>2564</asp:ListItem>
                                                          <asp:ListItem>2565</asp:ListItem>
                                                          <asp:ListItem>2566</asp:ListItem>
                                                          <asp:ListItem>2567</asp:ListItem>
                                                          <asp:ListItem>2568</asp:ListItem>
                                                      </asp:DropDownList>                                                    </td>
</tr>
<tr>
  <td align="left" class="texttopic">กิจกรรม :</td>
  <td align="left" class="texttopic">
                                                      <asp:DropDownList ID="ddlForm" runat="server" AutoPostBack="True" 
                                                          CssClass="form-control select2">                                                      </asp:DropDownList>                                                    </td>
</tr>
<tr>
  <td align="left" class="texttopic"><asp:Label ID="lblProv" runat="server" 
          Text="จังหวัด :"></asp:Label>
    </td>
  <td align="left" class="texttopic">
                                                      <asp:DropDownList ID="ddlProvinceID" runat="server" AutoPostBack="True" 
                                                          CssClass="OptionControl">                                                      </asp:DropDownList>                                                    </td>
</tr>

<tr>
  <td align="left" class="texttopic">ค้นหา :</td>
  <td align="left" class="texttopic">
                  <asp:TextBox ID="txtSearch" runat="server" Width="150px"></asp:TextBox>                                                    </td>
</tr>

<tr>
  <td align="left" class="texttopic">&nbsp;</td>
  <td align="left" class="texttopic">
                                                      <asp:Button ID="cmdSearch" runat="server" CssClass="buttonLogin" Text="ค้นหา" Width="100px" />
    </td>
</tr>

  </table>  
 </div>
          
          </div>
     <h3>รายการกิจกรรมที่พบทั้งหมด&nbsp;<asp:Label ID="lblCount" runat="server"></asp:Label>
           &nbsp;&nbsp;รายการ</h3> 
    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-edit"></i>

              <h3 class="box-title">1.ให้บริการแล้ว รอติดตาม ( <asp:Label 
                  ID="lblCountForm" runat="server"></asp:Label>
              &nbsp;)</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">


              <asp:GridView ID="grdData" CssClass="table table-hover"
                             runat="server" CellPadding="4" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True" > 
            <columns>
                <asp:BoundField HeaderText="Seq No." DataField="itemID" >
                <ItemStyle HorizontalAlign="Center" Width="70px" />                </asp:BoundField>
                <asp:BoundField HeaderText="วันที่บริการ" DataField="ServiceDateTXT" >
                <ItemStyle HorizontalAlign="Center" Width="90px" />                </asp:BoundField>
                <asp:TemplateField HeaderText="ร้านยา">
                 <ItemTemplate>
                        <asp:HyperLink ID="Hyper1" runat="server" 
                            NavigateUrl='<%# NavigateURL("FormDirect.aspx", "acttype", "view", "ServiceTypeID", DataBinder.Eval(Container.DataItem, "ServiceTypeID"), "ItemId", DataBinder.Eval(Container.DataItem, "ItemID"), "ProjID", DataBinder.Eval(Container.DataItem, "ProjectID"))%>' 
                            Target="_blank" 
                            Text='<%# DataBinder.Eval(Container.DataItem, "LocationName") %>' 
                            CssClass="grd_item"></asp:HyperLink>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle CssClass="grd_item" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ชื่อ-สกุล">
                 <ItemTemplate>
                        <asp:HyperLink ID="Hyper2" runat="server" 
                            NavigateUrl='<%# NavigateURL("FormDirect.aspx", "acttype", "view", "ServiceTypeID", DataBinder.Eval(Container.DataItem, "ServiceTypeID"), "ItemId", DataBinder.Eval(Container.DataItem, "ItemID"), "ProjID", DataBinder.Eval(Container.DataItem, "ProjectID"))%>' 
                            Target="_blank" 
                            Text='<%# DataBinder.Eval(Container.DataItem, "CustName") %>' 
                            CssClass="grd_item"></asp:HyperLink>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" CssClass="grd_item" />
                </asp:TemplateField>
                <asp:BoundField DataField="Ages" HeaderText="อายุ" >
                <ItemStyle HorizontalAlign="Center" Width="30px" />                </asp:BoundField>
            <asp:BoundField HeaderText="เพศ">                      
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />                      </asp:BoundField>
                <asp:BoundField DataField="ServiceTypeID" HeaderText="ฟอร์ม" >
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" />                </asp:BoundField>
                <asp:BoundField DataField="FollowSEQ">
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                </asp:BoundField>
            <asp:TemplateField  HeaderText="" >
                <ItemTemplate>
                     
                    <asp:LinkButton ID="lnkA04" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "itemID") & "|" & DataBinder.Eval(Container.DataItem, "ProjectID")%>' CssClass="buttonMenuA" Visible='<%# DataBinder.Eval(Container.DataItem, "isA4F") %>'>ติดตาม A4</asp:LinkButton>
                    <asp:LinkButton ID="lnkA05" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "itemID") & "|" & DataBinder.Eval(Container.DataItem, "ProjectID")%>' CssClass="buttonMenuB" Visible='<%# DataBinder.Eval(Container.DataItem, "isA5F") %>'> ติดตาม A5</asp:LinkButton>
                </ItemTemplate>
              <itemstyle HorizontalAlign="center" VerticalAlign="Middle" />          
            </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:ImageButton  cssclass="gridbutton"  ID="imgEdit" runat="server"
                            CommandArgument='<%# DataBinder.Eval(Container.DataItem, "itemID") & "|" & DataBinder.Eval(Container.DataItem, "ProjectID")%>' 
                                        ImageUrl="images/icon-edit.png" />
                                 
                                     <asp:ImageButton ID="imgDel"  cssclass="gridbutton"  runat="server" 
                                       CommandArgument='<%# DataBinder.Eval(Container.DataItem, "itemID") & "|" & DataBinder.Eval(Container.DataItem, "ProjectID")%>'
                                        ImageUrl="images/icon-delete.png" />                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                </asp:TemplateField>
            </columns>         
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />           
                  
          </asp:GridView>


 </div>
            <div class="box-footer clearfix">
           
              <asp:Label ID="lblNo" runat="server" CssClass="text_red" 
                  
                  Text="ยังไม่มีรายการกิจกรรม"></asp:Label>           
           
            </div>
          </div>
      <asp:Panel ID="Panel2" runat="server"> 
  <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-dollar"></i>

              <h3 class="box-title">ดำเนินการประมวลผลแล้ว ( <asp:Label            ID="lblPayCount" runat="server"></asp:Label> &nbsp;) </h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
               

                 
            </div>
            <div class="box-body">          

   
  
 
              <asp:GridView ID="grdDataPaymented" CssClass="table table-hover"
                             runat="server" CellPadding="4" 
                                                        GridLines="None" 
                      AutoGenerateColumns="False" Width="100%" AllowPaging="True"> 
            <columns>
                <asp:BoundField HeaderText="Seq No." DataField="itemID" >
                <ItemStyle HorizontalAlign="Center" Width="70px" />                </asp:BoundField>
                <asp:BoundField DataField="ServiceDateTXT" HeaderText="วันที่บริการ">
                <ItemStyle Width="80px" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="ร้านยา">
                 <ItemTemplate>
                        <asp:HyperLink ID="Hyper5" runat="server" 
                            NavigateUrl='<%# NavigateURL("FormDirect.aspx", "acttype", "view", "ServiceTypeID", DataBinder.Eval(Container.DataItem, "ServiceTypeID"), "ItemId", DataBinder.Eval(Container.DataItem, "ItemID"), "ProjID", DataBinder.Eval(Container.DataItem, "ProjectID"))%>' 
                            Target="_blank" 
                            Text='<%# DataBinder.Eval(Container.DataItem, "LocationName") %>' 
                            CssClass="grd_item"></asp:HyperLink>
                    </ItemTemplate>
                    
                    <HeaderStyle HorizontalAlign="Left" />
                    
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ชื่อ-สกุล">
                 <ItemTemplate>
                        <asp:HyperLink ID="Hyper6" runat="server" 
                            NavigateUrl='<%# NavigateURL("FormDirect.aspx", "acttype", "view", "ServiceTypeID", DataBinder.Eval(Container.DataItem, "ServiceTypeID"), "ItemId", DataBinder.Eval(Container.DataItem, "ItemID"), "ProjID", DataBinder.Eval(Container.DataItem, "ProjectID"))%>' 
                            Target="_blank" 
                            Text='<%# DataBinder.Eval(Container.DataItem, "CustName") %>' 
                            CssClass="grd_item"></asp:HyperLink>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle  HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:BoundField DataField="Ages" HeaderText="อายุ" >
                <ItemStyle HorizontalAlign="Center" Width="30px" />                </asp:BoundField>
            <asp:BoundField HeaderText="เพศ">                      
              <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />                      </asp:BoundField>
                <asp:BoundField DataField="ServiceTypeID" HeaderText="ฟอร์ม" >
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="50px" />                </asp:BoundField>
                <asp:BoundField DataField="FollowSEQ">
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="30px" />
                </asp:BoundField>
                <asp:TemplateField>
                    <ItemTemplate>
                          <asp:LinkButton ID="lnkA042" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "itemID") & "|" & DataBinder.Eval(Container.DataItem, "ProjectID")%>' CssClass="buttonMenuA"  Visible='<%# DataBinder.Eval(Container.DataItem, "isA4F") %>'>ติดตาม A4</asp:LinkButton>
                    <asp:LinkButton ID="lnkA052" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "itemID") & "|" & DataBinder.Eval(Container.DataItem, "ProjectID")%>' CssClass="buttonMenuB"  Visible='<%# DataBinder.Eval(Container.DataItem, "isA5F") %>'>ติดตาม A5</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate> 
                     <asp:ImageButton  cssclass="gridbutton"  ID="imgEdit2" runat="server" 
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "itemID") & "|" & DataBinder.Eval(Container.DataItem, "ProjectID")%>' 
                                        ImageUrl="images/icon-edit.png" />
                                    <asp:ImageButton ID="imgDel2" runat="server" 
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "itemID") & "|" & DataBinder.Eval(Container.DataItem, "ProjectID")%>' 
                                        ImageUrl="images/icon-delete.png" Visible="False" />                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                </asp:TemplateField>
                <asp:BoundField DataField="InvoiceNo" HeaderText="Invoice No." />
                <asp:BoundField DataField="PayDateTXT" HeaderText="Pay Date" />
            </columns>         
            <pagerstyle HorizontalAlign="Center" 
                      CssClass="dc_pagination dc_paginationC dc_paginationC11" />           
          </asp:GridView>
                  
 

                 </div>
            <div class="box-footer clearfix">
            <asp:Label ID="lblNo2" runat="server" CssClass="text_red" 
                  
                  Text="ยังไม่มีรายการกิจกรรม"></asp:Label> 
            </div>
          </div>

     </asp:Panel>
 <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Center">
      <asp:Label ID="lblNot" runat="server" CssClass="RateDisplay" Text="ท่านไม่ได้เข้าร่วมโครงการนี้"></asp:Label>
    </asp:Panel>

    </section>
</asp:Content>
