<%@ Page Title="" Language="vb" AutoEventWireup="false"  CodeBehind="ReportViewer.aspx.vb" Inherits=".ReportViewer" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

    <head>
        <style type="text/css">
            #form1 {
                text-align: center;
            }
        </style>
</head>

    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0">
            <ProgressTemplate>
                <asp:Image ID="Image2" runat="server" ImageUrl="images/loading2.gif" />
            </ProgressTemplate>
        </asp:UpdateProgress>

    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" Height="100%" ProcessingMode="Remote" ShowParameterPrompts="False" ShowPrintButton="true" DocumentMapWidth="100%" ZoomMode="PageWidth">
    </rsweb:ReportViewer>
</form>

