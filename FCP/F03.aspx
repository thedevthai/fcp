﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteSimple.Master" CodeBehind="F03.aspx.vb" Inherits=".F03" %>
<%@ Register src="ucHistoryYear.ascx" tagname="ucHistoryYear" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/pagestyles.css" /> 
<link rel="stylesheet" type="text/css" href="css/rajchasistyles.css" /> 
<link rel="stylesheet" type="text/css" href="css/uidialog.css" /> 
<link rel="stylesheet" type="text/css" href="css/dc_table.css" /> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>F03<small>การให้ความรู้และคำแนะนำปรึกษาในกลุ่ม Metabolic Syndrome ครั้งที่ 2 </small>
     </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">   
      <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลกิจกรรม</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">

 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="10"> <uc1:ucHistoryYear ID="ucHistoryYear1" runat="server" /></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="OptionPanels">
      <tr>
        <td width="100" class="texttopic">รหัสร้าน</td>
        <td width="120" class="LocationName">
            <asp:Label ID="lblLocationID" runat="server"></asp:Label>
          </td>
        <td width="50" class="texttopic">ชื่อร้าน</td>
        <td width="300" class="LocationName">
            <asp:Label ID="lblLocationName" runat="server"></asp:Label>          </td>
        <td width="50" class="texttopic">จังหวัด</td>
        <td class="LocationName">
            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
   <tr>
    <td align="center"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">
      <tr>
        <td width="100">วันที่ให้บริการ</td>
        <td>
            <asp:TextBox ID="txtServiceDate" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
(วว/ดด/ปปปป พ.ศ.) 
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="txtServiceDate" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="\d{2}/\d{2}/\d{4}"></asp:RegularExpressionValidator></td>
        <td width="100">รวมระยะเวลา<img src="images/star.png" width="10" height="10" /></td>
        <td>
            <asp:TextBox ID="txtTime" runat="server" Width="50px"></asp:TextBox>
&nbsp;นาที</td>
        <td>เภสัชกรผู้ให้บริการ</td>
        <td>
             <asp:DropDownList  CssClass="form-control select2"  ID="ddlPerson" runat="server">            </asp:DropDownList>          </td>
      </tr>
    </table></td>
  </tr>
 
   <tr>
    <td>
      <table width="100%" border="0" cellspacing="2" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
          <td width="100" align="right"><span class="NameEN">ID :
              <asp:Label ID="lblID" 
                  runat="server"></asp:Label></td>
          <td width="100"><span class="NameEN">Ref.ID :
              <asp:Label ID="lblRefID" 
                  runat="server"></asp:Label></span>
            </td>
        </tr>
      </table></td>
  </tr>
    <tr>
    <td class="MenuSt" align="center">แผนการให้บริการเพิ่มเติม</td>
  </tr>
   <tr>
    <td  align="left"><table border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td>แผนการให้บริการ :</td>
        <td>
            <asp:Label ID="lblServicePlan" runat="server"></asp:Label>
          </td>
      </tr>
      <tr>
        <td>สถานพยาบาล :</td>
        <td>
            <asp:Label ID="lblHospitalName" runat="server"></asp:Label>
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td class="MenuSt" align="center">
        <table width="100%" border="0" cellspacing="2" cellpadding="0">
          <tr>
            <td align="center">การให้ความรู้และคำแนะนำปรึกษา   ครั้งที่ 2
        </td>
          </tr>
          <tr>
            <td align="center">(  เว้นช่วง 2 สัปดาห์  กรณีพบความเสี่ยง และ ต้องติดตาม )  </td>
          </tr>
          <tr>
            <td align="center"><table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td><asp:RadioButtonList ID="optChannal" runat="server" 
                RepeatDirection="Horizontal">
                  <asp:ListItem Selected="True" Value="S">มารับบริการที่ร้าน</asp:ListItem>
                  <asp:ListItem Value="C">ให้บริการทางโทรศัพท์</asp:ListItem>
                </asp:RadioButtonList></td>
                <td width="40" align="center">วันที่</td>
                <td>
                    <asp:TextBox ID="txtRefDate" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                  </td>
                <td width="40" align="center">เวลา</td>
                <td>
                    <asp:TextBox ID="txtRefTime" runat="server" Width="50px"></asp:TextBox>
                  </td>
              </tr>
            </table></td>
          </tr>
        </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="dc_table_s3">
     <thead>  <tr>
        <th scope="col" width="50%" valign="top" class="texttopic">ปัญหาของผู้คัดกรอง</th>
        <th scope="col" colspan="3" valign="top">การให้ความรู้และคำแนะนำปรึกษา</th>
        </tr> </thead>
       <tbody><tr>
        <th valign="top" class="texttopic" align="left">
              <asp:CheckBox ID="chkProblem1" runat="server" Font-Bold="False" Text="ความรู้เกี่ยวกับความเสี่ยงโรคเบาหวาน" /> <br />
               <asp:CheckBox ID="chkProblem2" runat="server" Font-Bold="False" Text="ความรู้เกี่ยวกับความเสี่ยงความดันโลหิตสูง" />                  <br />
                <asp:CheckBox ID="chkProblem3" runat="server" Font-Bold="False" Text="ขาดการออกกำลังกาย" /> <br />
              <asp:CheckBox ID="chkProblem4" runat="server" Font-Bold="False" Text="ขาดการควบคุมอาหาร/โภชนาการ" /> <br />
                <asp:CheckBox ID="chkProblem5" runat="server" Font-Bold="False" Text="มีความเครียด" /> <br />
                <asp:CheckBox ID="chkProblem6" runat="server" Font-Bold="False" Text="สูบบุหรี่" /> <br />
                <asp:CheckBox ID="chkProblem7" runat="server" Font-Bold="False" Text="อื่นๆ(เฉพาะราย)" />
           
           &nbsp;<asp:TextBox ID="txtProblemOther" runat="server" Width="200px"></asp:TextBox>
           
           </th>
        <th colspan="3" valign="top" align="left">
             <asp:CheckBox ID="chkEdu1" runat="server" Font-Bold="False" Text="ให้คำแนะนำเกี่ยวกับความเสี่ยงโรคเบาหวาน" /> <br />
  <asp:CheckBox ID="chkEdu2" runat="server" Font-Bold="False" Text="ให้คำแนะนำเกี่ยวกับความเสี่ยงความดันโลหิตสูง" /> <br />
  <asp:CheckBox ID="chkEdu3" runat="server" Font-Bold="False" Text="ให้คำแนะนำออกกำลังกาย" /> <br />
  <asp:CheckBox ID="chkEdu4" runat="server" Font-Bold="False" Text="ให้คำแนะนำควบคุมอาหาร/โภชนาการ" /> <br />
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- DASH Diet (สำหรับผู้ที่เสี่ยง HTN) <br /> 
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- Plate Model (สำหรับผู้ที่เสี่ยง DM)<br /> 
  <asp:CheckBox ID="chkEdu5" runat="server" Font-Bold="False" Text="ให้คำแนะนำกิจกรรมคลายเครียด " /> <br />    
  <asp:CheckBox ID="chkEdu6" runat="server" Font-Bold="False" Text="หยุดสูบบุหรี่" /> <br />
  <asp:CheckBox ID="chkEdu7" runat="server" Font-Bold="False" Text="อื่นๆ(เฉพาะราย)" /> &nbsp;<asp:TextBox 
                 ID="txtEduOther" runat="server" Width="200px"></asp:TextBox>
             <br />           
            
            </th>
      </tr>
       </tbody>
    </table></td>
  </tr>
  
  <tr>
    <td>
        
        <table border="0" cellspacing="2" cellpadding="0" align="left">
        <tr>
        <td colspan="3">&nbsp;</td>
        </tr>
      <tr>
        <td>BP เฉลี่ย</td>
        <td><asp:TextBox ID="txtBP" runat="server" Width="40px"></asp:TextBox>&nbsp;/
            <asp:TextBox ID="txtBP2" runat="server" Width="40px"></asp:TextBox></td>
        <td align="left"><span>mmHg. <span class="text10_nblue"> ค่าปกติ <120/80 mmHg.</span></td>
      </tr>
      <tr>
        <td>FBS (อดอาหาร 8 ชั่วโมง)</td>
        <td><asp:TextBox ID="txtFBS" runat="server" Width="60px"></asp:TextBox></td>
        <td align="left">mg/dL. <span class="text10_nblue"> ค่าปกติ   70 – 110  mg./dL</span> </td>
      </tr>
       <tr>
        <td>Random</td>
         <td><asp:TextBox ID="txtRandom" runat="server" Width="60px"></asp:TextBox></td>
          <td align="left">mg/dL. หลังอาหาร 
                <asp:TextBox ID="txtHourAfter" runat="server" Width="60px"></asp:TextBox>
&nbsp;ชั่วโมง <span class="text10_nblue"> ค่าปกติ  &lt;140 mg./dL </span></td>
      </tr>
      </table> </td>
  </tr>
   <tr>
    <td class="MenuSt" align="center">ผลการ Refer</td>
  </tr>

 <tr><td>
    
 <table border="0" cellspacing="2" cellpadding="0" align="left">
           
          <tr>
              <td valign="top">
                  <asp:CheckBox ID="chkNotResponse" runat="server" Text="ติดตามไม่ได้ เพราะ" />
              </td>
              <td align="left" colspan="4">
                  <table>
                      <tr>
                          <td><asp:RadioButtonList ID="optCause" runat="server" AutoPostBack="True">
                    <asp:ListItem Value="1">โทรไม่ติด/ไม่มีผู้รับ</asp:ListItem>
                    <asp:ListItem Value="2">ผู้รับบริการไม่มีเวลาไปพบแพทย์</asp:ListItem>
                              <asp:ListItem Value="3">ผู้รับบริการคิดว่าตนเองไม่เป็นอะไร</asp:ListItem>
                              <asp:ListItem Selected="True" Value="4">อื่นๆ ระบุ</asp:ListItem>
                  </asp:RadioButtonList></td>
                          <td align="left" valign="bottom"><asp:TextBox ID="txtOtherCause" runat="server" Width="200px"></asp:TextBox></td>
                      </tr>
                  </table>
              </td>             
          </tr>  
           
          <tr>
              <td>ผลการวินิจฉัย</td>
              <td align="left" colspan="4"><table border="0" cellpadding="0" cellspacing="2">
                <tr>
                  <td><asp:RadioButtonList ID="optResultProcess" runat="server" 
                      RepeatDirection="Horizontal" AutoPostBack="True">
                    <asp:ListItem Value="0">ไม่เป็นโรค</asp:ListItem>
                    <asp:ListItem Value="1">เป็นโรค</asp:ListItem>
                  </asp:RadioButtonList></td>
                  <td>
                <asp:CheckBox ID="chkDiseases1" runat="server" Font-Bold="False" 
                      Text="โรคเบาหวาน" /></td>
                  <td><asp:CheckBox ID="chkDiseases2" runat="server" 
                      Font-Bold="False" Text="โรคความดันโลหิตสูง" /></td>
                  <td align="right"><asp:CheckBox 
                      ID="chkDiseases3" runat="server" Font-Bold="False" Text="โรคอื่นๆ" /></td>
                  <td align="left"><asp:TextBox ID="txtResultRemark" runat="server" Width="150px"></asp:TextBox></td>
                </tr>
            </table></td>             
          </tr>    
           
          <tr>
              <td>การ Refer</td>
              <td align="left" colspan="4">
                  <table>
                      <tr>
                          <td><asp:RadioButtonList ID="optRefer" runat="server" 
                      RepeatDirection="Horizontal">
                    <asp:ListItem Value="1">ออกใบ Refer</asp:ListItem>
                    <asp:ListItem Value="0" Selected="True">ไม่ได้ออกใบ Refer </asp:ListItem>
                  </asp:RadioButtonList></td>
                          <td>เพราะ </td>
                          <td><asp:TextBox ID="txtRemarkRefer" runat="server" Width="300px"></asp:TextBox></td>
                      </tr>
                  </table>
              </td>             
          </tr>  <tr>
              <td>ยาที่ได้รับ   </td>
              <td colspan="4" align="left">
                  <asp:RadioButtonList ID="optIsMed" runat="server" RepeatDirection="Horizontal">
                      <asp:ListItem Value="0">ไม่ได้รับยา</asp:ListItem>
                      <asp:ListItem Value="1">ได้รับยา</asp:ListItem>
                  </asp:RadioButtonList>                </td>
            </tr>
            <tr>
              <td align="right">1.</td>
              <td  align="left">
                  <asp:TextBox ID="txtMed1" runat="server" Width="300px"></asp:TextBox>                </td>
                    <td width="130" align="right">แนบไฟล์หลักฐาน 1</td>
            <td width="310">
            <asp:FileUpload ID="FileUpload1" runat="server" Width="300px" />              </td>
            <td>
                <asp:HyperLink ID="HyperLink1" runat="server" Target="_blank"></asp:HyperLink>
                </td>
            </tr>
             <tr>
              <td align="right">2.</td>
              <td  align="left">
            <asp:TextBox ID="txtMed2" runat="server" Width="300px"></asp:TextBox> </td>
             <td align="right">แนบไฟล์หลักฐาน 2</td>
            <td>
            <asp:FileUpload ID="FileUpload2" runat="server" Width="300px" />              </td>  
            <td>
                <asp:HyperLink ID="HyperLink2" runat="server" Target="_blank"></asp:HyperLink>
                 </td>
            </tr>
             <tr>
              <td align="right">3.</td>
              <td  align="left">
                 <asp:TextBox 
                      ID="txtMed3" runat="server" Width="300px"></asp:TextBox>     </td>
              <td align="right">แนบไฟล์หลักฐาน 3</td>
            <td>
            <asp:FileUpload ID="FileUpload3" runat="server" Width="300px" />              </td>
            <td>
                <asp:HyperLink ID="HyperLink3" runat="server" Target="_blank"></asp:HyperLink>
                 </td>
            </tr>
             <tr>
              <td align="right">4.</td>
              <td  align="left">
                 <asp:TextBox 
                      ID="txtMed4" runat="server" Width="300px"></asp:TextBox>      </td>
               <td width="130" align="right">แนบไฟล์หลักฐาน 4</td>
            <td width="310">
            <asp:FileUpload ID="FileUpload4" runat="server" Width="300px" />              </td>
            <td>
                <asp:HyperLink ID="HyperLink4" runat="server" Target="_blank"></asp:HyperLink>
                 </td>
            </tr>
             <tr>
              <td align="right">5.</td>
              <td  align="left">
                 <asp:TextBox 
                      ID="txtMed5" runat="server" Width="300px"></asp:TextBox>   </td>
              <td width="130" align="right">แนบไฟล์หลักฐาน 5</td>
            <td width="310">
            <asp:FileUpload ID="FileUpload5" runat="server" Width="300px" />              </td>
            <td>
                <asp:HyperLink ID="HyperLink5" runat="server" Target="_blank"></asp:HyperLink>
                 </td>
            </tr>
             <tr>
               <td align="right">&nbsp;</td>
               <td  align="left">&nbsp;</td>
               <td colspan="3" align="left">(ชื่อไฟล์ต้องเป็นภาษาอังกฤษเท่านั้น)</td>
             </tr>
        </table>
        </td></tr>
         <tr>
        <td>&nbsp;</td>
      </tr>
</table>
  
 </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
  <asp:Panel ID="pnBehavior" runat="server">
    <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">ปัญหาเรื่องการปรับเปลี่ยนพฤติกรรม</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">  
  <table width="100%">
                <tr>
                    <td width="150">เรื่อง</td>
                    <td>
                        <asp:DropDownList ID="ddlBehavior" runat="server" CssClass="OptionControl" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td width="150">กรณี อื่นๆ ระบุ</td>
                    <td>
                        <asp:TextBox ID="txtProblemBehaviorOther" runat="server" CssClass="OptionControl2"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Interventions</td>
                    <td>
                        <asp:TextBox ID="txtInterventionB" runat="server" CssClass="OptionControl2"></asp:TextBox>
                    </td>
                </tr>

                   <tr>
                    <td valign="top">ผลการติดตาม<br />
                        <asp:CheckBox ID="chkNotFollow" runat="server" AutoPostBack="True" Text="ไม่ใช่การติดตามผล" />
                    </td>
                    <td>
                   
                        

                        <asp:Panel ID="pnFat" runat="server">                       
                        <table>
                            <tr>
                                <td>สิ่งที่พบ</td>
                                <td> <asp:RadioButtonList ID="optFat" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Selected="True">น้ำหนัก</asp:ListItem>
                            <asp:ListItem>รอบเอว</asp:ListItem>
                        </asp:RadioButtonList></td>
                            </tr>
                           
                        </table>
                        </asp:Panel>

                             <asp:Panel ID="pnEating" runat="server">                       
                        <table>
                            <tr>
                                <td>รส</td>
                                <td> <asp:RadioButtonList ID="optTast" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Selected="True">รสจัด</asp:ListItem>
                            <asp:ListItem>หวาน</asp:ListItem>
                                    <asp:ListItem>เค็ม</asp:ListItem>
                                    <asp:ListItem>มัน</asp:ListItem>
                        </asp:RadioButtonList></td>
                            </tr>
                           
                        </table>
                        </asp:Panel>


                          <table  >
                            <tr>
                                <td>
                                    <asp:RadioButtonList ID="optResultB" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Selected="True" Value="สูบลดลง">สูบลดลง</asp:ListItem>
                                        <asp:ListItem>สูบเท่าเดิม</asp:ListItem>
                                        <asp:ListItem Value="สูบเพิ่มขึ้น">สูบเพิ่มขึ้น</asp:ListItem>
                                        <asp:ListItem Value="เลิกสูบแล้ว">เลิกสูบแล้ว</asp:ListItem>
                                        <asp:ListItem Value="อื่นๆ">อื่นๆ</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>ระบุ</td>
                                <td>
                                    <asp:TextBox ID="txtResultOtherB" runat="server" CssClass="OptionControl2" Width="150px"></asp:TextBox>
                                </td>
                            </tr>

                        </table> 
                        <table>
                            <tr>
                                <td>จาก</td>
                                <td>
                                    <asp:TextBox ID="txtBegin" runat="server"></asp:TextBox>
                                </td>
                                <td>เป็น</td>
                                <td>
                                    <asp:TextBox ID="txtEnd" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                                      
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                         <asp:Panel ID="pnAlertB" runat="server">
                  <div class="alert alert-danger alert-dismissible">
                
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <p><asp:Label ID="lblAlertB" runat="server"></asp:Label></p>
              </div>
                   </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>ID:<asp:Label ID="lblUID_B" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Button ID="cmdAddBehavior" runat="server" CssClass="buttonRedial" Text="เพิ่ม" Width="70px" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:GridView ID="grdBehavior" runat="server" AutoGenerateColumns="False" CellPadding="0" ForeColor="#333333" GridLines="None" Width="100%">
                            <RowStyle BackColor="White" VerticalAlign="Top" />
                            <columns>
                                <asp:BoundField DataField="nRow" HeaderText="No">
                                <HeaderStyle HorizontalAlign="Center" />
                                <itemstyle HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ServiceDate" HeaderText="วันที่">
                                <HeaderStyle HorizontalAlign="Center" />
                                <ItemStyle HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ProblemDesc" HeaderText="ปัญหาที่พบ" />
                                <asp:BoundField DataField="Interventions" HeaderText="Interventions">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FinalResult" HeaderText="ผลการติดตาม" />
                                <asp:TemplateField HeaderText="Edit">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgEdit_B" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID")%>' CssClass="gridbutton" ImageUrl="images/icon-edit.png" />
                                    </ItemTemplate>
                                    <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Del">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDel_B" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "UID") %>' CssClass="gridbutton" ImageUrl="images/icon-delete.png" />
                                    </ItemTemplate>
                                    <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                            </columns>
                            <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <headerstyle CssClass="th" Font-Bold="True" VerticalAlign="Middle" />
                            <EditRowStyle BackColor="#2461BF" />
                            <AlternatingRowStyle BackColor="#F7F7F7" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
       </div>
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
  </asp:Panel>    
    
    
 <table width="100%" border="0" cellspacing="2" cellpadding="0" align="center">
      <tr>
        
        <td >
            <asp:CheckBox ID="chkClose" runat="server" Text="จบการทำงาน" />
          </td>
      </tr>
   
  <tr>
    <td align="center">
        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="100px" />
        </td>
  </tr>
     </table> 
     
</section>
</asp:Content>
 

