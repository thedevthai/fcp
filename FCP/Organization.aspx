﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Organization.aspx.vb" Inherits="FCP.Organization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
           <section class="content-header">
      <h1><%: Title %>        
        <small>ข้อมูลบริษัท/องค์กร</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Company Profile</li>
      </ol>
    </section>
    <!-- Main content -->
  <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Organization
                </h3><asp:HiddenField ID="hdUID" runat="server" />
            </div>
            <div class="box-body">            

                 <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label>Business Unit</label>
                  <asp:TextBox ID="txtBU" runat="server" cssclass="form-control" placeholder="BU Code"></asp:TextBox>   
                 </div>
              <!-- /.form-group -->
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Name (ไทย)</label> <asp:TextBox ID="txtNameTH" runat="server" cssclass="form-control" placeholder="ชื่อภาษาไทย"></asp:TextBox>
                 </div>
              <!-- /.form-group -->
            </div>
                     <div class="col-md-6">
              <div class="form-group">
                <label>Name (English)</label> <asp:TextBox ID="txtNameEN" runat="server" cssclass="form-control" placeholder="English Name"></asp:TextBox>
                 </div>
              <!-- /.form-group -->
            </div>
</div>
                <div class="row">

 <div class="col-md-3">
              <div class="form-group">
                <label>Address no.</label> <asp:TextBox ID="txtAddressNo" runat="server" cssclass="form-control" placeholder="บ้านเลขที่"></asp:TextBox>
                 </div>
              <!-- /.form-group -->
            </div>

                     <div class="col-md-3">
              <div class="form-group">
                <label>Lane</label> <asp:TextBox ID="txtLane" runat="server" cssclass="form-control" placeholder="ซอย"></asp:TextBox>
                 </div>
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Road</label> <asp:TextBox ID="txtRoad" runat="server" cssclass="form-control" placeholder="ถนน"></asp:TextBox>
                 </div>
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Sub District</label> <asp:TextBox ID="txtSubDistrict" runat="server" cssclass="form-control" placeholder="ตำบล/แขวง"></asp:TextBox>
                 </div>
            </div>
            </div>
                   <div class="row">

 <div class="col-md-3">
              <div class="form-group">
                <label>district</label> <asp:TextBox ID="txtDistrict" runat="server" cssclass="form-control" placeholder="อำเภอ"></asp:TextBox>
                 </div>
              <!-- /.form-group -->
            </div>

                     <div class="col-md-3">
              <div class="form-group">
                <label>Province</label> 
                  <asp:DropDownList ID="ddlProvince" runat="server" cssclass="form-control select2" Width="100%" ></asp:DropDownList>
                   
                 </div>
            </div>
                     <div class="col-md-3">
              <div class="form-group">
                <label>Zip Code</label> <asp:TextBox ID="txtZipcode" runat="server" cssclass="form-control" placeholder="รหัสไปรษณีย์"></asp:TextBox>
                 </div>
            </div>
                 <div class="col-md-3">
              <div class="form-group">
                <label>Country</label> 
                  <asp:DropDownList ID="ddlCountry" runat="server" cssclass="form-control select2" Width="100%" >
                      <asp:ListItem Selected="True" Value="TH">ประเทศไทย</asp:ListItem>
                  </asp:DropDownList>
                   
                 </div>
            </div>     
            </div>
            <div class="row">             
          
               <div class="col-md-3">
              <div class="form-group">
                <label>Telephone</label> <asp:TextBox ID="txtTel" runat="server" cssclass="form-control" placeholder="เบอร์โทร"></asp:TextBox>
                 </div>
            </div>
                 <div class="col-md-3">
              <div class="form-group">
                <label>Fax</label> <asp:TextBox ID="txtFax" runat="server" cssclass="form-control" placeholder="แฟกซ์"></asp:TextBox>
                 </div>
            </div>

                     <div class="col-md-3">
              <div class="form-group">
                <label>E-mail</label> <asp:TextBox ID="txtEmail" runat="server" cssclass="form-control" placeholder="อีเมล์"></asp:TextBox>
                 </div>
            </div>
 
                     <div class="col-md-3">
              <div class="form-group">
                <label>Website</label> <asp:TextBox ID="txtWebsite" runat="server" cssclass="form-control" placeholder="เว็บไซต์"></asp:TextBox>
                 </div>
            </div>
 
            </div>
          <h5 class="text-info">Address display (for report)</h5>
    <div class="row">             
          
               <div class="col-md-6">
              <div class="form-group">
                <label>ภาษาไทย</label> <asp:TextBox ID="txtAddressTha" runat="server" cssclass="form-control" placeholder="ที่อยู่ภาษาไทย"></asp:TextBox>
                 </div>
            </div>
                 <div class="col-md-6">
              <div class="form-group">
                <label>English</label> <asp:TextBox ID="txtAddressEng" runat="server" cssclass="form-control" placeholder="ที่อยู่ภาษาอังกฤษ"></asp:TextBox>
                 </div>
            </div>

               
 
            </div>
                
            </div>
                <!-- /.box-body -->
             <div class="box-footer"> 
                <button type="submit" class="btn btn-primary pull-right" data-dismiss="modal"><i class="fa fa-save"></i> Save</button>

              </div>
              <!-- /.box-footer -->
          
          <!-- /.box -->
</div>
   <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">รายการข้อมูลบริษัท/องค์กร</h3>
            </div>
 
              <div class="box-body">
                   <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="False" CellPadding="0" CssClass="table table-bordered table-hover"  Width="100%" OnRowCommand="grdData_RowCommand" OnRowDataBound="grdData_RowDataBound">
                          <RowStyle BackColor="#F7F7F7" />
                          <columns>
                              <asp:BoundField DataField="OrganizationUID" HeaderText="ID">
                              <HeaderStyle HorizontalAlign="Center" />
                              </asp:BoundField>
                              <asp:BoundField DataField="OrganizationCode" HeaderText="Business Unit" />
                              <asp:BoundField DataField="OrganizationName" HeaderText="Name (TH)" />
                              <asp:BoundField DataField="AddressTH" HeaderText="Address" />
                              <asp:TemplateField HeaderText="Active">
                                  <itemtemplate>
                                      <asp:Image ID="imgStatus" runat="server" ImageUrl="images/icon-ok.png" Visible='<%# Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsPublic")) %>' />
                                  </itemtemplate>
                              </asp:TemplateField>
                              <asp:TemplateField>
                                  <itemtemplate>
                                      <asp:ImageButton ID="imgEdit" runat="server" CommandName="Select" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "OrganizationUID") %>' ImageUrl="images/icon-edit.png" />
                                  </itemtemplate>
                                  <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />
                              </asp:TemplateField>
                              <asp:TemplateField>
                                  <itemtemplate>
                                      <asp:ImageButton ID="imgDel" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "OrganizationUID") %>' ImageUrl="images/delete.png" />
                                  </itemtemplate>
                                  <itemstyle HorizontalAlign="Center" VerticalAlign="Middle" />
                              </asp:TemplateField>
                          </columns>
                          <footerstyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                          <pagerstyle CssClass="dc_pagination dc_paginationC dc_paginationC11" HorizontalAlign="Center" />
                          <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                          <headerstyle VerticalAlign="Middle" />
                          <EditRowStyle BackColor="#2461BF" />
                          <AlternatingRowStyle BackColor="White" />
                      </asp:GridView>
                 
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
              
              </div>
              <!-- /.box-footer -->
           
 </div>
  
      <div class="modal fade" id="modal-default" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span id="spnTitle"></span></h4>
              </div>
              <div class="modal-body">
                <p><span id="spnMsg"></span>&hellip;</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Confirm</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

  </section>    
</asp:Content>
