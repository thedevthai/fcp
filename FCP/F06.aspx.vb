﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO



Public Class F06
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlLct As New LocationController
    Dim ctlPs As New PersonController
    Dim ctlOrder As New OrderController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then
            If Request("acttype") = "view" Then
                cmdSave.Visible = False
                cmdClear.Visible = False
            Else
                cmdSave.Visible = True
                cmdClear.Visible = True
            End If
            txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)

            LoadFormData() 
        LoadPharmacist(Request.Cookies("LocationID").Value)

        End If

        txtTime.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        'txtCardID.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")


    End Sub
   Private Sub LoadPharmacist(LID As String)
        Dim dtP As New DataTable
        dtP = ctlPs.GetPerson_ByLocation(LID)
        If dtP.Rows.Count > 0 Then
            ddlPerson.Items.Clear()
            If dtP.Rows.Count > 0 Then
                With ddlPerson
                    .Visible = True
                    For i = 0 To dtP.Rows.Count - 1
                        .Items.Add(dtP.Rows(i)("FirstName") & " " & dtP.Rows(i)("LastName"))
                        .Items(i).Value = dtP.Rows(i)("PersonID")
                    Next
                    .SelectedIndex = 0
                End With
            End If
        End If
        dtP = Nothing
    End Sub

    Private Sub LoadLocationData(LocationID As String)
        dt = ctlLct.Location_GetByID(LocationID)
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblLocationID.Text = .Item("LocationID")
                lblLocationName.Text = .Item("LocationName")
                lblLocationProvince.Text = .Item("ProvinceName")
            End With
        End If
        dt = Nothing
    End Sub

    Private Sub LoadFormData()
        Dim pYear As Integer
        If Not Request("fid") Is Nothing Then
            dt = ctlOrder.GetOrder_ByID(StrNull2Zero(Request("fid")))
        Else
            If Not Request("yid") Is Nothing Then
                pYear = CInt(Request("yid"))
            Else
                pYear = Year(ctlLct.GET_DATE_SERVER)
                If pYear < 2500 Then
                    pYear = pYear + 543
                End If

            End If
            dt = ctlOrder.LocationOrder_ByYear(pYear, FORM_TYPE_ID_F06, Request.Cookies("LocationID").Value, Session("patientid"))
        End If
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)

                lblID.Text = .Item("itemID")
                Session("LocationForm") = DBNull2Str(.Item("LocationID"))
                lblLocationID.Text = DBNull2Str(.Item("LocationID"))
                lblLocationName.Text = DBNull2Str(.Item("LocationName"))
                lblLocationProvince.Text = DBNull2Str(.Item("LocationProvinceName"))
                txtServiceDate.Text = DisplayStr2ShortDateTH(.Item("ServiceDate"))
                txtTime.Text = String.Concat(.Item("ServiceTime"))
                LoadPharmacist(lblLocationID.Text)
                ddlPerson.SelectedValue = DBNull2Str(.Item("PersonID"))
                Session("patientid") = DBNull2Lng(.Item("PatientID"))
                'Dim sName(1) As String
                'sName(0) = ""
                'sName(1) = ""
                'sName = Split(DBNull2Str(.Item("CustName")), " ")
                'txtFName.Text = sName(0)
                'If sName.Length > 1 Then
                '    txtLName.Text = sName(1)
                'End If

                'optGender.SelectedValue = DBNull2Str(.Item("Gender"))
                'txtAges.Text = DBNull2Str(.Item("Ages"))
                'txtCardID.Text = DBNull2Str(.Item("CardID"))
                'txtBirthDate.Text = DisplayStr2ShortDateTH(DBNull2Str(.Item("BirthDate")))
                'txtTelephone.Text = DBNull2Str(.Item("Telephone"))
                'txtMobile.Text = DBNull2Str(.Item("Mobile"))
                'optAddressType.SelectedValue = DBNull2Str(.Item("AddressType"))
                'txtAddress.Text = DBNull2Str(.Item("AddressNo"))
                'txtRoad.Text = DBNull2Str(.Item("Road"))
                'txtDistrict.Text = DBNull2Str(.Item("District"))
                'txtCity.Text = DBNull2Str(.Item("City"))
                'ddlProvince.SelectedValue = DBNull2Str(.Item("ProvinceID"))
                'optClaim.SelectedValue = DBNull2Str(.Item("MainClaim"))
                chkClose.Checked = ConvertStatus2Boolean(DBNull2Zero(.Item("Status")))


                chkProblem1.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem1")))
                chkProblem2.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem2")))
                chkProblem3.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem3")))
                chkProblem4.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem4")))
                chkProblem5.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem5")))
                chkProblem6.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem6")))
                chkProblem7.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblem7")))
                chkProblemOther.Checked = Decimal2Boolean(DBNull2Zero(.Item("isProblemOther")))
                txtProblemOther.Text = DBNull2Str(.Item("ProblemRemark"))
                chkEdu1.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate1")))
                chkEdu2.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate2")))
                chkEdu3.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate3")))
                chkEdu4.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate4")))
                chkEdu5.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducate5")))
                chkEduOther.Checked = Decimal2Boolean(DBNull2Zero(.Item("isEducateOther")))
                txtEduOther.Text = DBNull2Str(.Item("EducateRemark"))

                chkNHSO1.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO1")))
                chkNHSO2.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO2")))
                chkNHSO3.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO3")))
                chkNHSO4.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO4")))
                chkNHSO5.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO5")))
                chkNHSO6.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO6")))
                chkNHSO7.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO7")))
                chkNHSO8.Checked = Decimal2Boolean(DBNull2Zero(.Item("isNHSO8")))

                If DBNull2Zero(.Item("Status")) >= 3 Then
                    cmdSave.Visible = False
                    cmdClear.Visible = False
                Else
                    If Request.Cookies("LocationID").Value <> String.Concat(.Item("LocationID")) Then
                        LoadLocationData(String.Concat(.Item("LocationID")))
                        If Request("t") = "edit" Then
                            cmdSave.Visible = True
                            cmdClear.Visible = True
                        Else
                            cmdSave.Visible = False
                            cmdClear.Visible = False
                        End If
                    Else
                        LoadLocationData(Request.Cookies("LocationID").Value)
                    End If
                End If


            End With
        Else
            LoadLocationData(Request.Cookies("LocationID").Value)
        End If
        dt = Nothing
    End Sub
    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click

        lblID.Text = ""
        txtServiceDate.Text = DisplayShortDateTH(ctlLct.GET_DATE_SERVER)
        txtTime.Text = ""
        ddlPerson.SelectedIndex = 0
        'txtFName.Text = ""
        'optGender.SelectedIndex = 0
        'txtAges.Text = ""
        'txtCardID.Text = ""
        'txtBirthDate.Text = ""
        'txtTelephone.Text = ""
        'txtMobile.Text = ""
        'optAddressType.SelectedIndex = 0
        'txtAddress.Text = ""
        'txtRoad.Text = ""
        'txtDistrict.Text = ""
        'txtCity.Text = ""
        'ddlProvince.SelectedValue = "01"
        'optClaim.SelectedIndex = 0
        chkClose.Checked = False



    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If


        If StrNull2Zero(txtTime.Text) = 0 Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนระยะเวลาในการให้บริการก่อน');", True)
            Exit Sub
        End If


        Dim objuser As New UserController
        Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))
        'Dim BirthDate As String = ConvertStrDate2DBString(txtBirthDate.Text)
        'Dim CustName As String
        'CustName = Trim(txtFName.Text) & " " & Trim(txtLName.Text)
        If lblID.Text = "" Then 'Add new

            If ctlOrder.Order_ChkDupCustomer(FORM_TYPE_ID_F06, Session("patientid"), StrNull2Zero(Left(Str(ServiceDate), 4))) = True Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ผู้รับบริการรายนี้เคยได้รับบริการในกิจกรรมนี้แล้ว');", True)
                Exit Sub
            End If

            ctlOrder.F06_Add(lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F06, Convert2Status(chkClose.Checked), Boolean2Decimal(chkProblem1.Checked), Boolean2Decimal(chkProblem2.Checked), Boolean2Decimal(chkProblem3.Checked), Boolean2Decimal(chkProblem4.Checked), Boolean2Decimal(chkProblem5.Checked), Boolean2Decimal(chkProblem6.Checked), Boolean2Decimal(chkProblem7.Checked), Boolean2Decimal(chkProblemOther.Checked), txtProblemOther.Text, Boolean2Decimal(chkEdu1.Checked), Boolean2Decimal(chkEdu2.Checked), Boolean2Decimal(chkEdu3.Checked), Boolean2Decimal(chkEdu4.Checked), Boolean2Decimal(chkEdu5.Checked), Boolean2Decimal(chkEduOther.Checked), txtEduOther.Text, Request.Cookies("username").Value, CLng(ConvertDate2DBString(ctlLct.GET_DATE_SERVER)), Boolean2Decimal(chkNHSO1.Checked), Boolean2Decimal(chkNHSO2.Checked), Boolean2Decimal(chkNHSO3.Checked), Boolean2Decimal(chkNHSO4.Checked), Boolean2Decimal(chkNHSO5.Checked), Boolean2Decimal(chkNHSO6.Checked), Boolean2Decimal(chkNHSO7.Checked), Boolean2Decimal(chkNHSO8.Checked), Session("patientid"), Session("sex"), Session("age"))

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Service_Order", "บันทึกเพิ่มกิจกรรมการการสร้างเสริมสุขภาพและป้องกันโรคโดยการฝากครรภ์ (F06) :" & Session("patientname"), "F06")
        Else
            ctlOrder.F06_Update(StrNull2Zero(lblID.Text), lblLocationID.Text, ServiceDate, StrNull2Zero(txtTime.Text), StrNull2Zero(ddlPerson.SelectedValue), FORM_TYPE_ID_F06, Convert2Status(chkClose.Checked), Boolean2Decimal(chkProblem1.Checked), Boolean2Decimal(chkProblem2.Checked), Boolean2Decimal(chkProblem3.Checked), Boolean2Decimal(chkProblem4.Checked), Boolean2Decimal(chkProblem5.Checked), Boolean2Decimal(chkProblem6.Checked), Boolean2Decimal(chkProblem7.Checked), Boolean2Decimal(chkProblemOther.Checked), txtProblemOther.Text, Boolean2Decimal(chkEdu1.Checked), Boolean2Decimal(chkEdu2.Checked), Boolean2Decimal(chkEdu3.Checked), Boolean2Decimal(chkEdu4.Checked), Boolean2Decimal(chkEdu5.Checked), Boolean2Decimal(chkEduOther.Checked), txtEduOther.Text, Request.Cookies("username").Value, Boolean2Decimal(chkNHSO1.Checked), Boolean2Decimal(chkNHSO2.Checked), Boolean2Decimal(chkNHSO3.Checked), Boolean2Decimal(chkNHSO4.Checked), Boolean2Decimal(chkNHSO5.Checked), Boolean2Decimal(chkNHSO6.Checked), Boolean2Decimal(chkNHSO7.Checked), Boolean2Decimal(chkNHSO8.Checked), Session("patientid"))

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Service_Order", "บันทึกแก้ไขกิจกรรมการการสร้างเสริมสุขภาพและป้องกันโรคโดยการฝากครรภ์(F06) :" & Session("patientname"), "F06")
        End If

        Response.Redirect("ResultPage.aspx?p=F06")

        ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย")
    End Sub

    'Protected Sub txtBirthDate_TextChanged(sender As Object, e As EventArgs) Handles txtBirthDate.TextChanged
    '    Try
    '        txtAges.Text = DateDiff(DateInterval.Year, CDate(txtBirthDate.Text), ctlLct.GET_DATE_SERVER.Date)
    '        txtCardID.Focus()
    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Protected Sub txtLName_TextChanged(sender As Object, e As EventArgs) Handles txtLName.TextChanged
    '    LoadPersonalData()
    'End Sub
    'Private Sub LoadPersonalData()
    '    dt = ctlOrder.GetCustomer_ByName(txtFName.Text, txtLName.Text)
    '    If dt.Rows.Count > 0 Then
    '        With dt.Rows(0)
    '            optGender.SelectedValue = DBNull2Str(.Item("Gender"))
    '            txtAges.Text = DBNull2Str(.Item("Ages"))
    '            txtCardID.Text = DBNull2Str(.Item("CardID"))
    '            txtBirthDate.Text = DisplayStr2ShortDateTH(DBNull2Str(.Item("BirthDate")))
    '            txtTelephone.Text = DBNull2Str(.Item("Telephone"))
    '            txtMobile.Text = DBNull2Str(.Item("Mobile"))
    '            optAddressType.SelectedValue = DBNull2Str(.Item("AddressType"))
    '            txtAddress.Text = DBNull2Str(.Item("AddressNo"))
    '            txtRoad.Text = DBNull2Str(.Item("Road"))
    '            txtDistrict.Text = DBNull2Str(.Item("District"))
    '            txtCity.Text = DBNull2Str(.Item("City"))
    '            ddlProvince.SelectedValue = DBNull2Str(.Item("ProvinceID"))
    '            optClaim.SelectedValue = DBNull2Str(.Item("MainClaim"))


    '        End With
    '    Else
    '        ClearData()
    '    End If

    'End Sub
    'Private Sub ClearData()
    '    txtAges.Text = 0
    '    txtCardID.Text = ""
    '    txtBirthDate.Text = ""
    '    txtTelephone.Text = ""
    '    txtMobile.Text = ""
    '    txtAddress.Text = ""
    '    txtRoad.Text = ""
    '    txtDistrict.Text = ""
    '    txtCity.Text = ""
    'End Sub
End Class