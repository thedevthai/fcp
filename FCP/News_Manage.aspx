﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteCPA.Master" CodeBehind="News_Manage.aspx.vb" Inherits=".News_Manage" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register assembly="DevExpress.Web.ASPxHtmlEditor.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxHtmlEditor" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.ASPxSpellChecker.v17.2, Version=17.2.13.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.Web.ASPxSpellChecker" tagprefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>จัดการข่าวประกาศ
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">     

    <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">เพิ่ม/แก้ไข ข่าวประกาศ</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">






<table width="100%" border="0" align="left" cellpadding="0" cellspacing="2">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="2">
      <tr>
        <td width="120">ID :</td>
        <td><table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="200"><asp:Label ID="lblID" runat="server">0</asp:Label></td>
                <td width="50" align="center">ลำดับ</td>
                <td>
                    <asp:TextBox ID="txtOrder" runat="server" Width="50px">99</asp:TextBox>
                  </td>
              </tr>
            </table></td>
      </tr>
      <tr>
        <td>หัวเรื่อง :</td>
        <td>
            <asp:TextBox ID="txtTitle" runat="server" Width="500px"></asp:TextBox>
          </td>
      </tr>
      <tr>
        <td>ประกาศไว้ที่หน้าแรก :</td>
        <td>
            <asp:CheckBox ID="chkFistPage" runat="server" Text="ประกาศไว้ที่หน้าแรก" />
          </td>
      </tr>
      <tr>
        <td>ประเภท :</td>
        <td>
            <asp:RadioButtonList ID="optType" runat="server" RepeatDirection="Horizontal" 
                AutoPostBack="True">
                <asp:ListItem Value="URL">URL Link</asp:ListItem>
                <asp:ListItem Value="UPL">File Upload</asp:ListItem>
                <asp:ListItem Selected="True" Value="CON">เนื้อหาข่าว</asp:ListItem>
            </asp:RadioButtonList>
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>
        <asp:Panel ID="pnURL" runat="server">
       
    
    <table width="100%" border="0" cellpadding="0" cellspacing="2">
      <tr>
        <td width="120">URL : </td>
        <td>
            <asp:TextBox ID="txtURL" runat="server" Width="500px"></asp:TextBox>
          </td>
      </tr>
    </table>
     </asp:Panel>
        <asp:Panel ID="pnFile" runat="server">
     
    <table width="100%" border="0" cellpadding="0" cellspacing="2">
      <tr>
        <td width="120">Link File : </td>
        <td>
            <asp:HyperLink ID="hlnkNews" runat="server" Target="_blank">[hlnkNews]</asp:HyperLink>
          </td>
      </tr>
      <tr>
        <td>Upload File :</td>
        <td>
            <asp:FileUpload ID="FileUpload" runat="server" Width="300px" />
            &nbsp; (ชื่อไฟล์ต้องเป็นภาษาอังกฤษเท่านั้น)</td>
      </tr>
    </table>
       </asp:Panel>
        <asp:Panel ID="pnContent" runat="server">
       
    <table width="100%" border="0" cellpadding="0" cellspacing="2">
      <tr>
        <td align="left">เนื้อหาข่าว : </td>
        </tr>
      <tr>
        <td>
            <dx:ASPxHtmlEditor ID="xHtmlNews" runat="server" Theme="Office2010Blue" Width="100%">
            </dx:ASPxHtmlEditor>
          </td>
        </tr>
      <tr>
        <td>
            <asp:CheckBox ID="chkStatus" runat="server" Checked="True" 
                Text="Public (แสดงบนหน้าเว็บ)" />
          </td>
      </tr>
    </table>
     </asp:Panel>
    
    
    
    </td>
  </tr>
  <tr>
    <td align="center">
        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="100px" />
        </td>
  </tr>
  <tr>
    <td align="center">
        <asp:LinkButton ID="lnkNew" runat="server" CssClass="buttonModal" 
            PostBackUrl="News_List.aspx?m=news&amp;p=news-lst">เพิ่มข่าวใหม่</asp:LinkButton>
&nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CssClass="buttonModal" 
            PostBackUrl="News_List.aspx?m=news&amp;p=news-lst">หน้ารายการข่าว</asp:LinkButton>
&nbsp;<asp:HyperLink ID="HyperLink3" runat="server" CssClass="buttonModal" 
            NavigateUrl="Default.aspx" Target="_blank">หน้าแรก</asp:HyperLink>
      </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>

     </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    </section>
</asp:Content>
