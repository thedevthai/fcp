﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="F01.aspx.vb" Inherits=".F01" %>
<%@ Register assembly="FUA" namespace="Subgurim.Controles" tagprefix="cc1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc2" %>
<%@ Register src="ucHistoryYear.ascx" tagname="ucHistoryYear" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/pagestyles.css">
<link rel="stylesheet" type="text/css" href="css/rajchasistyles.css">
<link rel="stylesheet" type="text/css" href="css/uidialog.css">
<link rel="stylesheet" type="text/css" href="css/dc_table.css">
    
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <section class="content-header">
      <h1>F01
        <small>แบบคัดกรองความเสี่ยงในกลุ่มภาวะโรคเมตาบอลิก ( เบาหวาน   ความดันโลหิตสูง   และ โรคอ้วน)</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"></li>
      </ol>
    </section>

<section class="content">    
   
<asp:HiddenField ID="HiddenField1" runat="server" />

<div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-info-circle"></i>

              <h3 class="box-title">ข้อมูลกิจกรรม</h3>
             
                 <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>

                 
            </div>
            <div class="box-body">
 <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="10"> <uc1:ucHistoryYear ID="ucHistoryYear1" runat="server" /></td>
  </tr>
  <tr>
    <td align="left"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="2" class="OptionPanels">
      <tr>
        <td width="100" class="texttopic">รหัสร้าน</td>
        <td width="120" class="LocationName">
            <asp:Label ID="lblLocationID" runat="server"></asp:Label>          </td>
        <td width="50" class="texttopic">ชื่อร้าน</td>
        <td width="300" class="LocationName">
            <asp:Label ID="lblLocationName" runat="server"></asp:Label>          </td>
        <td width="50" class="texttopic">จังหวัด</td>
        <td class="LocationName">
            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
   <tr>
    <td align="center"><table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">
      <tr>
        <td align="left">
             <table  border="0" cellspacing="2" cellpadding="0">
        <tr>
        
          <td class="NameEN">Item ID : </td>
          <td class="NameEN">
              <asp:Label ID="lblID" 
                  runat="server"></asp:Label>            </td>
        </tr>
      </table>

        </td>
        <td width="100">วันที่ให้บริการ</td>
        <td>
            <asp:TextBox ID="txtServiceDate" runat="server" AutoPostBack="True" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
(วว/ดด/ปปปป พ.ศ.) 
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                ControlToValidate="txtServiceDate" CssClass="text_red" 
                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="\d{2}/\d{2}/\d{4}"></asp:RegularExpressionValidator></td>
        <td width="100">รวมระยะเวลา<img src="images/star.png" width="10" height="10" /></td>
        <td>
            <asp:TextBox ID="txtTime" runat="server" Width="50px"></asp:TextBox>
&nbsp;นาที</td>
        <td>เภสัชกรผู้ให้บริการ</td>
        <td>
             <asp:DropDownList  CssClass="form-control select2"  ID="ddlPerson" runat="server">            </asp:DropDownList>          </td>
      </tr>
    </table></td>
  </tr>
   
   <tr>
    <td align="center" class="MenuSt">ประวัติครอบครัว</td>
  </tr>
   <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td class="texttopic">ประวัติการเจ็บป่วยในครอบครัว (พ่อ, แม่ และพี่น้องสายตรง)</td>
      </tr>
      <tr>
        <td>
            <table cellpadding="0">
                <tr>
                    <td><asp:CheckBox ID="chkFamDisease1" runat="server" Text="เบาหวาน" 
                            AutoPostBack="True" />
                    </td> 
                    <td><asp:CheckBox ID="chkFamDisease2" runat="server" Text="ความดันโลหิตสูง" /></td>
                    <td><asp:CheckBox ID="chkFamDisease3" runat="server" Text="ไขมันในหลอดเลือดผิดปกติ" /></td>
                    <td><asp:CheckBox ID="chkFamDisease4" runat="server" Text="หลอดเลือดหัวใจ" /></td>
                    <td><asp:CheckBox ID="chkFamDisease5" runat="server" Text="อัมพาต" /></td>
                     <td><asp:CheckBox ID="chkFamDisease6" runat="server" Text="ไม่มี/ไม่ทราบ" /></td>
                </tr>
            </table>
          </td>
      </tr>
    </table></td>
  </tr>
   <tr>
    <td align="center" class="MenuSt">พฤติกรรมสุขภาพ</td>
  </tr>
   <tr>
    <td><table border="0" cellpadding="0" cellspacing="2">
      <tr>
        <td>การดื่มเครื่องดื่มที่มีแอลกอฮอล์</td>
        <td align="left"><table border="0" cellspacing="2" cellpadding="0">
              <tr>
                <td align="left">
                    <asp:RadioButtonList ID="optAlcohol" runat="server" 
                RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="3">ดื่มประจำ</asp:ListItem>
                     <asp:ListItem Value="2">ดื่มครั้งคราว</asp:ListItem>
                  <asp:ListItem Value="1">เคยดื่มแต่เลิกแล้ว</asp:ListItem>
                  <asp:ListItem Selected="True" Value="0">ไม่ดื่ม</asp:ListItem>
                </asp:RadioButtonList></td>
                <td align="left"><asp:TextBox ID="txtAlcoholFQ" runat="server" Width="50px"></asp:TextBox>
                  &nbsp;<asp:Label ID="lblAlcohol" runat="server" Text=" ครั้ง/สัปดาห์"></asp:Label>
                   </td>
              </tr>
            </table>
            </td>
      </tr>
      <tr>
        <td>การสูบบุหรี่</td>
        <td><table border="0" cellspacing="2" cellpadding="0">
              <tr>
                <td align="left"><asp:RadioButtonList ID="optSmoke" runat="server" 
                RepeatDirection="Horizontal" AutoPostBack="True">
                  <asp:ListItem Value="2">เลิกสูบแล้ว</asp:ListItem>
                  <asp:ListItem Value="1">สูบประจำ</asp:ListItem>
                  <asp:ListItem Selected="True" Value="0">ไม่สูบ</asp:ListItem>
                </asp:RadioButtonList></td>
                <td><asp:TextBox ID="txtCGNo" runat="server" Width="40px"></asp:TextBox>
&nbsp;<asp:Label ID="lblCGday" runat="server" Text="มวน/วัน"></asp:Label>
            &nbsp;<asp:TextBox ID="txtCGYear" runat="server" Width="40px"></asp:TextBox>
&nbsp;<asp:Label ID="lblCGYear" runat="server" Text="ปี"></asp:Label></td>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td>&nbsp; <asp:Label ID="lblCgType" runat="server" Text="ชนิดของบุหรี่ที่สูบ"></asp:Label></td>
                      <td> <asp:RadioButtonList ID="optCigarette" runat="server" 
                RepeatDirection="Horizontal">
                  <asp:ListItem Value="1">มวนเอง</asp:ListItem>
                  <asp:ListItem Selected="True" Value="2">บุหรี่ซอง</asp:ListItem>
                  <asp:ListItem Value="3">บุหรี่ไฟฟ้า</asp:ListItem>
                
                  </asp:RadioButtonList></td>
                    </tr>
                  </table>

                   
                   </td>
                </tr>
            </table>          </td>
      </tr>
     
      <tr>
        <td>การออกกำลังกาย</td>
        <td>
            <asp:RadioButtonList ID="optExercise" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Value="2">ออกกำลังกาย &gt; สัปดาห์ละ 3 ครั้ง ครั้งละ 30 นาที</asp:ListItem>
                <asp:ListItem Value="1">ออกกำลังกาย &lt; สัปดาห์ละ 3 ครั้ง</asp:ListItem>
                <asp:ListItem Selected="True" Value="0">ไม่ออกกำลังกาย</asp:ListItem>
            </asp:RadioButtonList>          </td>
      </tr>
      <tr>
        <td>อาหารที่รับประทานเป็นประจำ</td>
        <td align="left">
            <table>
                <tr>
                    <td>
                        <asp:CheckBox ID="chkFood_Sweet" runat="server" Text="หวาน" />
                    </td>
                    <td>
                        <asp:CheckBox ID="chkFood_Salt" runat="server" Text="เค็ม" />
                    </td>
                    <td>
                        <asp:CheckBox ID="chkFood_Fat" runat="server" Text="มัน" />
                    </td>
                    <td>
                        <asp:CheckBox ID="chkFood_Other" runat="server" Text="อื่นๆ เช่น เผ็ด เปรี้ยว จืด" />
                    </td>
                </tr>
            </table>
          </td>
      </tr>
      <tr>
        <td>การพักผ่อน</td>
        <td>
            <asp:RadioButtonList ID="optSleep" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Value="7">&gt; 6 ชั่วโมง</asp:ListItem>
                <asp:ListItem Selected="True" Value="5">&lt; 6 ชั่วโมง</asp:ListItem>
            </asp:RadioButtonList>          </td>
      </tr>
    </table></td> 
  </tr>
  <tr>
    <td align="center" class="MenuSt">แบบคัดกรอง/ประเมินความเสี่ยง</td>
  </tr>
  <tr>
    <td>
    <table width="100%" border="0">
      <tr>
        <td  valign="top">
          <table width="100%" align="center" class="dc_table_s3">
    <thead>
      <tr>
        <th scope="col">ปัจจัยเสี่ยง</th>
        <th align="center" scope="col">คะแนน</th>
        </tr>
    </thead>
    <tfoot>
    </tfoot>
    <tbody>
      <tr >
        <th align="left" scope="row">1. เพศ</th>
        <th align="left"><span>
            <asp:RadioButtonList ID="optRiskGender" runat="server" 
                    RepeatDirection="Horizontal" CssClass="nono" AutoPostBack="True">
          <asp:ListItem Selected="True" Value="2">ชาย</asp:ListItem>
          <asp:ListItem Value="0">หญิง</asp:ListItem>
        </asp:RadioButtonList></span></th>
        </tr>
      <tr class="odd">
        <th align="left" scope="row">2.อายุ</th>
        <th align="left">
            <asp:RadioButtonList ID="optRiskAge" runat="server" 
                RepeatDirection="Horizontal" CssClass="nono" BackColor="#F7F9FC" 
                AutoPostBack="True">
                <asp:ListItem Value="0" Selected="True">34-44 ปี</asp:ListItem>
                <asp:ListItem Value="1">45-49 ปี</asp:ListItem>
                <asp:ListItem Value="2">&gt;= 50 ปี</asp:ListItem>
            </asp:RadioButtonList>
          </th>
        </tr>
      <tr >
        <th align="left" scope="row">3.ประวัติคนในครอบครัวเบาหวาน</th>
        <th align="left">
            <asp:RadioButtonList ID="optRiskFam" runat="server" 
                RepeatDirection="Horizontal" AutoPostBack="True">
                <asp:ListItem Value="4">มี</asp:ListItem>
                <asp:ListItem Selected="True" Value="0">ไม่มี</asp:ListItem>
            </asp:RadioButtonList>
          </th>
        </tr>
      <tr class="odd">
        <th align="left" scope="row">4.BMI</th>
        <th align="left">
            <asp:RadioButtonList ID="optRiskBMI" runat="server" 
                RepeatDirection="Horizontal" AutoPostBack="True">
                <asp:ListItem Selected="True" Value="0">&lt;23</asp:ListItem>
                <asp:ListItem Value="3">23-27.5</asp:ListItem>
                <asp:ListItem Value="5">&gt;27.5</asp:ListItem>
            </asp:RadioButtonList>
          </th>
      </tr>
      <tr>
        <th align="left" scope="row" valign="top">5.  เส้นรอบเอว</th>
        <th align="left">
            <asp:RadioButtonList ID="optRiskShape" runat="server" AutoPostBack="True">
                <asp:ListItem Selected="True" Value="0">&lt;90 (ชาย) , &lt;80 (หญิง) ซม.</asp:ListItem>
                <asp:ListItem Value="1">&gt;=90 (ชาย) ,&gt;=80 (หญิง) ซม.</asp:ListItem>
            </asp:RadioButtonList>
          </th>
      </tr>
      <tr class="odd">
        <th align="left" scope="row"> 6.  ความดันโลหิตสูง(เฉลี่ย) </th>
        <th align="left">
            <asp:RadioButtonList ID="optRiskBloodAVG" runat="server" 
                RepeatDirection="Horizontal" AutoPostBack="True">
                <asp:ListItem Selected="True" Value="0">BP&lt;140/90 mmHg</asp:ListItem>
                <asp:ListItem Value="2">BP&gt;=140/90 mmHg</asp:ListItem>
            </asp:RadioButtonList>
          </th>
      </tr>
    </tbody>
  </table>      
    </td>
        <td align="left" valign="top">
        <table width="100%" border="0"   class="dc_table_s1">
          <thead>
      <tr>
        <th scope="col">ข้อมูลผู้คัดกรอง</th>
        </tr>
    </thead>
    <tfoot>
    </tfoot>
    <tbody>
          <tr>
            <td align="left"><table border="0" cellspacing="2" cellpadding="0">
              <tr>
                <td>อายุ</td>
                <td>
                    <asp:TextBox ID="txtScreenAge" runat="server" Width="50px" AutoPostBack="True"></asp:TextBox>                  </td>
                <td>ปี </td>
                <td>
                    เส้นรอบเอว
                    <asp:TextBox ID="txtScreenShape" runat="server" Width="50px" 
                        AutoPostBack="True"></asp:TextBox>
&nbsp;ซม.</td>
              </tr>
              <tr>
                <td>น้ำหนัก</td>
                <td>
                    <asp:TextBox ID="txtScreenWeight" runat="server" Width="50px" AutoPostBack="True"></asp:TextBox>                  </td>
                <td>kg. </td>
                <td>
                    ส่วนสูง
                    <asp:TextBox ID="txtScreenHeigh" runat="server" Width="50px" AutoPostBack="True"></asp:TextBox>
&nbsp;ซม.</td>
              </tr>
              <tr>
                <td>BMI</td>
                <td>
                    <asp:TextBox ID="txtScreenBMI" runat="server" Width="50px" AutoPostBack="True"></asp:TextBox>                  </td>
                <td colspan="2">ค่าปกติ 18.5-23</td>
                </tr>
              <tr>
                <td>BP ครั้งที่ 1</td>
                <td valign="top">
                    <asp:TextBox ID="txtScreenBP1" runat="server" Width="30px" AutoPostBack="True"></asp:TextBox>
                  &nbsp;/ &nbsp;
                    <asp:TextBox ID="txtScreenBP1_2" runat="server" Width="30px" 
                        AutoPostBack="True"></asp:TextBox>                  </td>
                <td>mmHg.</td>
                <td rowspan="2" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="20"><img src="images/red-brackets.png" width="16" height="50" align="absmiddle" /></td>
                    <td>กรณีวัด 3 ครั้งให้เลือก 2 ค่าที่ใกล้เคียงที่สุด</td>
                  </tr>
                </table>
                  </td>
              </tr>
              <tr>
                 <td>BP ครั้งที่ 2</td>
                <td valign="top">
                    <asp:TextBox ID="txtScreenBP2" runat="server" Width="30px" AutoPostBack="True"></asp:TextBox> 
                    &nbsp; /  &nbsp;<asp:TextBox ID="txtScreenBP2_2" runat="server" Width="30px" 
                        AutoPostBack="True"></asp:TextBox>                  </td>
                <td>mmHg.</td>
                </tr>
              <tr>
                <td>BP เฉลี่ย</td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtScreenBPAVG" runat="server" Width="30px"></asp:TextBox>
                            &nbsp;/ &nbsp;
                            <asp:TextBox ID="txtScreenBPAVG2" runat="server" Width="30px"></asp:TextBox>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="txtScreenBP1" EventName="TextChanged" />
                            <asp:AsyncPostBackTrigger ControlID="txtScreenBP2" EventName="TextChanged" />
                            <asp:AsyncPostBackTrigger ControlID="txtScreenBP1_2" EventName="TextChanged" />
                             <asp:AsyncPostBackTrigger ControlID="txtScreenBP2_2" EventName="TextChanged" /> 
                        </Triggers>
                    </asp:UpdatePanel>                  </td>
                <td colspan="2">mmHg. ค่าปกติ &lt;120/80 mmHg.</td>
                </tr>
              <tr>
                <td>HR</td>
                <td>
                    <asp:TextBox ID="txtScreenHR" runat="server" Width="50px"></asp:TextBox>                  </td>
                <td colspan="2">Bpm. ค่าปกติ 60-80 Bpm.</td>
                </tr>
            </table></td>
          </tr>
          </tbody>
        </table></td>
        </tr>        
        <tr>
        <td colspan="2" valign="top">&nbsp;</td>
        </tr>
      
    </table></td>
  </tr>
  <tr>
    <td align="center" class="MenuSt">สรุปผลการคัดกรอง</td>
  </tr>
   <tr>
    <td><table width="100%" border="0"   class="table table-hover">
     <tr>
        <th align="left" valign="top">ความเสี่ยงเบาหวาน (ใช้ คะแนน ข้อ 1- 6)</th>
        <th align="left" valign="top"> <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
              <asp:TextBox ID="txtDiabetes" runat="server" ReadOnly="True" Width="50px"></asp:TextBox>
              &nbsp;คะแนน </ContentTemplate>
            <Triggers>
              <asp:AsyncPostBackTrigger ControlID="optRiskGender" 
                            EventName="SelectedIndexChanged" />
              <asp:AsyncPostBackTrigger ControlID="optRiskAge" 
                            EventName="SelectedIndexChanged" />
              <asp:AsyncPostBackTrigger ControlID="optRiskFam" 
                            EventName="SelectedIndexChanged" />
              <asp:AsyncPostBackTrigger ControlID="optRiskBMI" 
                            EventName="SelectedIndexChanged" />
              <asp:AsyncPostBackTrigger ControlID="optRiskShape" 
                            EventName="SelectedIndexChanged" />
              <asp:AsyncPostBackTrigger ControlID="optRiskBloodAVG" 
                            EventName="SelectedIndexChanged" />
            </Triggers>
          </asp:UpdatePanel>
          </th>
        <th align="left"><table width="100%" border="0" cellspacing="1" cellpadding="0">
            <tr>
              <td align="left"><asp:UpdatePanel ID="UpdatePanel7" runat="server">
                <ContentTemplate>
                  <asp:RadioButtonList ID="optRiskBlood" runat="server" 
                        RepeatDirection="Horizontal">
                    <asp:ListItem Selected="True" Value="2">เสี่ยงต่ำ</asp:ListItem>
                    <asp:ListItem Value="5">เสี่ยงปานกลาง</asp:ListItem>
                    <asp:ListItem Value="8">เสี่ยงสูง</asp:ListItem>
                    <asp:ListItem Value="9">เสี่ยงสูงมาก</asp:ListItem>
                  </asp:RadioButtonList>
                </ContentTemplate>
                <Triggers>
                  <asp:AsyncPostBackTrigger ControlID="optRiskGender" 
                            EventName="SelectedIndexChanged" />
                  <asp:AsyncPostBackTrigger ControlID="optRiskAge" 
                            EventName="SelectedIndexChanged" />
                  <asp:AsyncPostBackTrigger ControlID="optRiskFam" 
                            EventName="SelectedIndexChanged" />
                  <asp:AsyncPostBackTrigger ControlID="optRiskBMI" 
                            EventName="SelectedIndexChanged" />
                  <asp:AsyncPostBackTrigger ControlID="optRiskShape" 
                            EventName="SelectedIndexChanged" />
                  <asp:AsyncPostBackTrigger ControlID="optRiskBloodAVG" 
                            EventName="SelectedIndexChanged" />
                </Triggers>
              </asp:UpdatePanel></td>
            </tr>
            <tr>
              <td><span class="text10_nblue">** ให้เจาะระดับน้ำตาลในเลือด</span></td>
            </tr>
        </table></th>
      </tr>
     
      <tr>
        <th align="left">ความเสี่ยงภาวะอ้วนลงพุง (ใช้คะแนน ข้อ 5)</th>
        <th align="left"> <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
              <asp:TextBox ID="txtFat" runat="server" ReadOnly="True" Width="50px"></asp:TextBox>
              &nbsp;คะแนน </ContentTemplate>
            <Triggers>
              <asp:AsyncPostBackTrigger ControlID="optRiskShape" 
                            EventName="SelectedIndexChanged" />
            </Triggers>
          </asp:UpdatePanel>
        </th>
        <th align="left"> <asp:UpdatePanel ID="UpdatePanel5" runat="server">
            <ContentTemplate>
              <asp:RadioButtonList ID="optRiskFat" runat="server" 
                            RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="0">ไม่เสี่ยง</asp:ListItem>
                <asp:ListItem Value="1">เสี่ยง</asp:ListItem>
              </asp:RadioButtonList>
            </ContentTemplate>
            <Triggers>
              <asp:AsyncPostBackTrigger ControlID="optRiskShape" 
                            EventName="SelectedIndexChanged" />
            </Triggers>
          </asp:UpdatePanel>
        </th>
      </tr>
      <tr>
        <th align="left">ความเสี่ยงความดันโลหิตสูง (ใช้คะแนน ข้อ 6)</th>
        <th align="left"> <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
              <asp:TextBox ID="txtBlood" runat="server" ReadOnly="True" 
    Width="50px"></asp:TextBox>
              &nbsp;คะแนน </ContentTemplate>
            <Triggers>
              <asp:AsyncPostBackTrigger ControlID="optRiskBloodAVG" 
                            EventName="SelectedIndexChanged" />
            </Triggers>
          </asp:UpdatePanel>
        </th>
        <th align="left"> <asp:UpdatePanel ID="UpdatePanel6" runat="server">
            <ContentTemplate>
              <asp:RadioButtonList ID="optRiskBloodRisk" runat="server" 
                            RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="0">ความเสี่ยงต่ำ</asp:ListItem>
                <asp:ListItem Value="2">เสี่ยง</asp:ListItem>
              </asp:RadioButtonList>
            </ContentTemplate>
            <Triggers>
              <asp:AsyncPostBackTrigger ControlID="optRiskBloodAVG" 
                            EventName="SelectedIndexChanged" />
            </Triggers>
          </asp:UpdatePanel>
        </th>
      </tr>
      
    </table></td>
  </tr>
   <tr>
    <td align="center" class="MenuSt">ผลการเจาะวัดระดับน้ำตาลในเลือด</td>
  </tr>
   <tr>
    <td>
    <table width="100%" border="0" cellspacing="2" cellpadding="0">
          <tr>
            <td>FBS (อดอาหาร&#8805; 8 ชั่วโมง)  
                <asp:TextBox ID="txtFBS" runat="server" Width="60px"></asp:TextBox>
              </td>
            <td>หรือ Random 
                <asp:TextBox ID="txtRandom" runat="server" Width="60px"></asp:TextBox>
&nbsp;หลังอาหาร 
                <asp:TextBox ID="txtHourAfter" runat="server" Width="60px"></asp:TextBox>
&nbsp;ชั่วโมง</td>
          </tr>
          <tr>
            <td colspan="2"></td>
            </tr>
        </table>
    </td>
  </tr>
   <tr>
    <td align="center" class="MenuSt">แผนการให้บริการเพิ่มเติม</td>
  </tr>
   <tr>
    <td align="left">
        <asp:CheckBoxList ID="chkPlan" runat="server">
            <asp:ListItem Selected="True" Value="1">ให้ความรู้เรื่องโรคและพฤติกรรมความเสี่ยงครั้งที่ 1</asp:ListItem>
            <asp:ListItem Value="2">ให้ความรู้เรื่องโรคและพฤติกรรมความเสี่ยงครั้งที่ 2</asp:ListItem>
            <asp:ListItem Value="3">Refer เพื่อConfirm โดยแพทย์ </asp:ListItem>
        </asp:CheckBoxList>
       </td>
  </tr>
   <tr>
    <td align="left"><table border="0" cellspacing="2" cellpadding="0">
      <tr>
        <td>สถานพยาบาล</td>
        <td>
            <asp:TextBox ID="txtPlanHospital" runat="server" Width="300px"></asp:TextBox>          </td>
      </tr>
      <tr>
        <td>สำเนาเอกสารการส่งต่อ</td>
        <td>
            <cc1:FileUploaderAJAX ID="FileUploaderAJAX1" runat="server" />              </td>
      </tr>
      <tr>
        <td colspan="2"><span class="text10_nblue">หมายเหตุ การ Refer เพื่อConfirm โดยแพทย์ : FBS &#8805; 126 mg./dL  ,  Random &#8805; 200 mg./dL ,  BP &#8805; 140 / 90 mmHg  </span>
     </td></tr>
      <tr>
        <td>&nbsp;</td>     
        <td>
            <asp:HyperLink ID="hlnkReferFile" runat="server" Target="_blank">[hlnkReferFile]</asp:HyperLink>
          </td>
        </tr>  
      </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="2" cellpadding="0" class="Section_Header">
      <tr>
        <td width="100">สถานะ</td>
        <td>
            <asp:CheckBox ID="chkStatus" runat="server" Text="จบการทำงาน" />
          </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center">
        <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="100px" />
        </td>
  </tr>
</table>
  </div>
            <div class="box-footer clearfix">
           
            </div>
          </div>
    
      </section>
</asp:Content>