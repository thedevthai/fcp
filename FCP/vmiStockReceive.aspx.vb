﻿
Public Class vmiStockReceive
    Inherits System.Web.UI.Page

    Dim dt As New DataTable
    Dim ctlUser As New UserController
    Dim ctlInv As New InventoryController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("Username") Is Nothing Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then
            grdData.PageIndex = 0
            dtpDate.Value = Now.Date()
            LoadTransactionLine()
        End If
    End Sub
    Private Sub LoadTransactionLine()

        dt = ctlInv.Inventory_GetTransferByLocation(Session("LocationID"), optStatus.SelectedValue)

        If dt.Rows.Count > 0 Then
            With grdData
                .Visible = True
                .DataSource = dt
                .DataBind()
            End With
        Else
            grdData.DataSource = Nothing
            grdData.Visible = False
        End If
        dt = Nothing

    End Sub

    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdSave.Click
        If dtpDate.Text = "" Then
            DisplayMessage(Me.Page, "ท่านต้องเลือกวันที่รับสินค้าก่อน")
            Exit Sub
        End If


        Dim item As Integer
        Dim invDate As String
        invDate = ConvertFormateDate(dtpDate.Value)

        item = ctlInv.Inventory_Receive(lblUID.Text, invDate, Session("Username"))

        ctlUser.User_GenLogfile(Session("Username"), ACTTYPE_ADD, "Receive", "บันทึกรับสินค้า :" & lblMedName.Text, ">>QTY:" & lblQTY.Text & ">>TransID:" & lblUID.Text)


        DisplayMessage(Me.Page, "บันทึกรับสินค้าเรียบร้อย")
        LoadTransactionLine()

    End Sub

    Private Sub grdData_PageIndexChanging(sender As Object, e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdData.PageIndexChanging
        grdData.PageIndex = e.NewPageIndex
        LoadTransactionLine()
    End Sub
    Private Sub grdData_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdData.RowCommand

        If TypeOf e.CommandSource Is WebControls.ImageButton Then
            Dim ButtonPressed As WebControls.ImageButton = e.CommandSource
            Select Case ButtonPressed.ID
                Case "imgEdit"
                    EditData(e.CommandArgument())
            End Select

        End If

    End Sub
    Private Sub EditData(ByVal pID As String)

        dt = ctlInv.Inventory_GetTransferByUID(pID)

        If dt.Rows.Count > 0 Then
            With dt.Rows(0)
                lblMedName.Text = .Item("itemName")
                lblQTY.Text = .Item("QTY")
                lblUID.Text = .Item("UID")
                dtpDate.Value = Now.Date()
            End With
        End If

        dt = Nothing
    End Sub


    Private Sub grdData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdData.RowDataBound
        If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then

            'e.Row.Cells(0).Text = e.Row.RowIndex + 1            

        End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.Attributes.Add("onmouseover", "this.originalcolor=this.style.backgroundColor;" + " this.style.backgroundColor='#dfffae';")
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalcolor;")

        End If


    End Sub

    Protected Sub optStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles optStatus.SelectedIndexChanged
        grdData.PageIndex = 0
        LoadTransactionLine()
    End Sub
End Class

