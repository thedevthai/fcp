﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Public Class PatientRegister
    Inherits System.Web.UI.Page
    Dim dt As New DataTable
    Dim ctlP As New PatientController
    Dim objuser As New UserController

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Not IsPostBack Then
            If Request("acttype") = "view" Then
                cmdSave.Visible = False
                cmdClear.Visible = False
            Else
                cmdSave.Visible = True
                cmdClear.Visible = True
            End If
           
            LoadProvinceToDDL()
            ClearData()
            If Not Request("PatientID") Is Nothing Then
                EditData(Request("PatientID"))
            End If
        End If
        txtAges.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtZipCode.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtCardID.Attributes.Add("OnKeyPress", "return AllowOnlyIntegers();")
        txtCardID.Attributes.Add("OnKeyPress", "return autoTab(this);")
    End Sub

    Private Sub LoadProvinceToDDL()
        dt = ctlP.LoadProvince
        If dt.Rows.Count > 0 Then
            With ddlProvince
                .Enabled = True
                .DataSource = dt
                .DataTextField = "ProvinceName"
                .DataValueField = "ProvinceID"
                .DataBind()
                .SelectedIndex = 1
            End With
        End If
        dt = Nothing
    End Sub
    'Private Sub LockControls()
    '    txtForeName.ReadOnly = True
    '    txtSurname.ReadOnly = True
    '    optGender.Enabled = False
    '    txtAges.ReadOnly = True
    '    txtCardID.ReadOnly = True
    '    txtBirthDate.ReadOnly = True
    '    txtTelephone.ReadOnly = True
    '    txtMobile.ReadOnly = True
    '    optAddressType.Enabled = False
    '    txtAddress.ReadOnly = True
    '    txtRoad.ReadOnly = True
    '    txtDistrict.ReadOnly = True
    '    txtCity.ReadOnly = True
    '    ddlProvince.Enabled = False
    '    optClaim.Enabled = False
    '    txtZipCode.ReadOnly = True
    '    chkClose.Enabled = False
    'End Sub

    Private Sub EditData(pID As String)
        ClearData()
        dt = ctlP.Patient_GetAllByID(StrNull2Zero(pID))
        If dt.Rows.Count > 0 Then
            With dt.Rows(0)

                If Request("t") = "new" Then
                    lblID.Text = ""
                ElseIf Request("t") = "edit" Then
                    lblID.Text = String.Concat(.Item("itemID"))
                ElseIf Request("t") Is Nothing Then
                    lblID.Text = ""
                End If

                lblID.Text = DBNull2Str(.Item("PatientID"))
                Session("patientid") = DBNull2Str(.Item("PatientID"))
                txtForeName.Text = DBNull2Str(.Item("ForeName"))
                txtSurname.Text = DBNull2Str(.Item("SurName"))
                optGender.SelectedValue = DBNull2Str(.Item("Gender"))
                txtAges.Text = DBNull2Str(.Item("Ages"))
                txtCardID.Text = FormatCardID(DBNull2Str(.Item("CardID")))
                txtBirthDate.Text = DisplayStr2ShortDateTH(DBNull2Str(.Item("BirthDate")))
                txtTelephone.Text = DBNull2Str(.Item("Telephone"))
                txtMobile.Text = DBNull2Str(.Item("Mobile"))
                optAddressType.SelectedValue = DBNull2Str(.Item("AddressType"))
                txtAddress.Text = DBNull2Str(.Item("AddressNo"))
                txtRoad.Text = DBNull2Str(.Item("Road"))
                txtDistrict.Text = DBNull2Str(.Item("District"))
                txtCity.Text = DBNull2Str(.Item("City"))
                ddlProvince.SelectedValue = DBNull2Str(.Item("ProvinceID"))
                txtZipCode.Text = DBNull2Str(.Item("ZipCode"))
                optClaim.SelectedValue = DBNull2Str(.Item("MainClaim"))

                txtTimeContact.Text = DBNull2Str(.Item("TimeContact"))

                optAllergy.SelectedValue = DBNull2Str(.Item("isAllergy"))
                txtDrugAllergy.Text = DBNull2Str(.Item("DrugAllergy"))
                txtHomesss.Text = DBNull2Str(.Item("Homesss"))

                optSmoke.SelectedValue = String.Concat(.Item("isSmoke"))
                optSmokingQuit.SelectedValue = String.Concat(.Item("SmokingQuit"))

                chkClose.Checked = ConvertActive2Boolean(DBNull2Zero(.Item("Status")))

                If (Request.Cookies("LocationID").Value <> DBNull2Str(.Item("CreateBy"))) Then
                    cmdSave.Visible = False
                Else
                    cmdSave.Visible = True
                End If

            End With
        End If
        dt = Nothing

    End Sub
    Private Sub ClearData()
        lblID.Text = ""
        txtForeName.Text = ""
        txtSurname.Text = ""
        optGender.SelectedIndex = 0
        txtAges.Text = ""
        txtCardID.Text = ""
        txtBirthDate.Text = ""
        txtTelephone.Text = ""
        txtMobile.Text = ""
        optAddressType.SelectedIndex = 0
        txtAddress.Text = ""
        txtRoad.Text = ""
        txtDistrict.Text = ""
        txtCity.Text = ""
        ddlProvince.SelectedIndex = 0
        optClaim.SelectedIndex = 0
        txtZipCode.Text = ""
        txtTimeContact.Text = ""
        txtHomesss.Text = ""
        optAllergy.SelectedValue = "N"
        txtDrugAllergy.Text = ""

        chkClose.Checked = True
        cmdSave.Visible = True
    End Sub
    Private Sub txtBirthDate_TextChanged(sender As Object, e As System.EventArgs) Handles txtBirthDate.TextChanged

        Try
            Dim iAge As Integer

            'txtAges.Text = DateDiff(DateInterval.Year, ConvertStrDate2InformDate(txtBirthDate.Text), ctlP.GET_DATE_SERVER.Date)
            iAge = DateDiff(DateInterval.Year, CDate(txtBirthDate.Text), ctlP.GET_DATE_SERVER.Date)

            If iAge < 0 Then
                iAge = iAge + 543
            ElseIf iAge > 543 Then
                iAge = iAge - 543
            End If

            txtAges.Text = iAge.ToString()

            txtCardID.Focus()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub cmdSave_Click(sender As Object, e As EventArgs) Handles cmdSave.Click

        If IsNothing(Request.Cookies("FCPCPA")) Then
            Response.Redirect("Default.aspx?logout=YES")
        End If

        If Trim(txtForeName.Text) = "" Then
             ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนชื่อผู้รับบริการก่อน');", True)
            Exit Sub
        End If
        If Trim(txtSurname.Text) = "" Then
             ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนนามสกุลผู้รับบริการก่อน');", True)
            Exit Sub
        End If
        'If Trim(txtCardID.Text) = "" Then
        '     ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนเลขบัตรประชาชนก่อน');", True)
        '    Exit Sub
        'End If
        If txtAges.Text = "" Then
             ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนอายุผู้รับบริการก่อน');", True)
            Exit Sub
        End If

        If Trim(txtMobile.Text) = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาป้อนเบอร์มือถือผู้รับบริการก่อน หากไม่มีให้ใส่เครื่องหมาย (-)');", True)
            Exit Sub
        End If

        Dim isSmoke, SmokingQuit As String
        isSmoke = optSmoke.SelectedValue
        SmokingQuit = optSmokingQuit.SelectedValue

        If isSmoke = "" Then
            ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาทำการคัดกรองการสูบบุหรี่ให้ครบถ้วนก่อน');", True)
            Exit Sub
        End If
        If isSmoke = "Y" Then
            If SmokingQuit = "" Or SmokingQuit = "0" Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','กรุณาทำการคัดกรองความต้องการเลิกบุหรี่ก่อน');", True)
                Exit Sub
            End If
        Else
            SmokingQuit = "0"
        End If

        'Dim ServiceDate As Long = CLng(ConvertStrDate2DBString(txtServiceDate.Text))
        Dim BirthDate As String = ConvertStrDate2DBString(txtBirthDate.Text)

        If lblID.Text = "" Then 'Add new

            If Trim(Replace(txtCardID.Text, "-", "")) <> "" Then
                If ctlP.Patient_ChkDupPatientByIDCard(Trim(Replace(txtCardID.Text, "-", ""))) = True Then
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ผู้รับบริการรายนี้มีอยู่ในฐานข้อมูลแล้ว');", True)
                    Exit Sub
                End If
            End If

            If ctlP.Patient_ChkDupPatient(Trim(txtForeName.Text), Trim(txtSurname.Text)) = True Then
                ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','ผู้รับบริการรายนี้มีอยู่ในฐานข้อมูลแล้ว');", True)
                Exit Sub
            End If

            ctlP.Patient_Add(txtForeName.Text, txtSurname.Text, optGender.SelectedValue, BirthDate, StrNull2Zero(txtAges.Text), Replace(txtCardID.Text, "-", ""), txtTelephone.Text, txtMobile.Text, txtTimeContact.Text, optAddressType.SelectedValue, txtAddress.Text, txtRoad.Text, txtDistrict.Text, txtCity.Text, ddlProvince.SelectedValue, ddlProvince.SelectedItem.Text, txtZipCode.Text, "", optOccupation.SelectedValue, optClaim.SelectedValue, Convert2Active(chkClose.Checked), Request.Cookies("username").Value, CLng(ConvertStrDate2DBString(ctlP.GET_DATE_SERVER.Date)), Request.Cookies("LocationID").Value, txtHomesss.Text, optAllergy.SelectedValue.ToString(), txtDrugAllergy.Text, isSmoke, SmokingQuit, txtSmokeRemark.Text)

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_ADD, "Patient", "เพิ่ม Patient: " & txtForeName.Text & " " & txtSurname.Text, "")

        Else
            ctlP.Patient_Update(lblID.Text, txtForeName.Text, txtSurname.Text, optGender.SelectedValue, BirthDate, StrNull2Zero(txtAges.Text), Replace(txtCardID.Text, "-", ""), txtTelephone.Text, txtMobile.Text, txtTimeContact.Text, optAddressType.SelectedValue, txtAddress.Text, txtRoad.Text, txtDistrict.Text, txtCity.Text, ddlProvince.SelectedValue, ddlProvince.SelectedItem.Text, txtZipCode.Text, "", optOccupation.SelectedValue, optClaim.SelectedValue, Convert2Active(chkClose.Checked), Request.Cookies("username").Value, txtHomesss.Text, optAllergy.SelectedValue.ToString(), txtDrugAllergy.Text, isSmoke, SmokingQuit, txtSmokeRemark.Text)

            objuser.User_GenLogfile(Request.Cookies("username").Value, ACTTYPE_UPD, "Patient", "แก้ไข Patient: " & txtForeName.Text & " " & txtSurname.Text & " ID:" & lblID.Text, "")
        End If

        Session("patientid") = ctlP.Patient_GetPatientID(Trim(txtForeName.Text), Trim(txtSurname.Text))
        Response.Redirect("PatientProfile.aspx")
     
        ' ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "MessageAlert", "openModals(this,'ผลการตรวจสอบ','บันทึกข้อมูลเรียบร้อย")
    End Sub

    Protected Sub cmdClear_Click(sender As Object, e As EventArgs) Handles cmdClear.Click
        ClearData()
    End Sub
End Class