﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/SitePatient.Master" CodeBehind="A5.aspx.vb" Inherits=".A5" %>
<%@ Register Src="ucHistoryYear.ascx" TagName="ucHistoryYear" TagPrefix="uc1" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="stylesheet" type="text/css" href="css/pagestyles.css">
    <link rel="stylesheet" type="text/css" href="css/cpastyles.css">
    <link rel="stylesheet" type="text/css" href="css/uidialog.css">
    <link rel="stylesheet" type="text/css" href="css/dc_table.css">     
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <section class="content-header">
        <h1>A5 (ติดตามผล)
        <small>โครงการสนับสนุนการทำกิจกรรมปรับเปลี่ยนพฤติกรรมผู้เสพยาสูบโดยเภสัชกรชุมชน </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="Home.aspx?actionType=h"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"></li>
        </ol>
    </section>

    <section class="content">
        <div align="center">
            <uc1:ucHistoryYear ID="ucHistoryYear1" runat="server" />
        </div>
        <br />
        <div class="pull-right">   
             <asp:HiddenField ID="hdServiceUID" runat="server" />
            <asp:HiddenField ID="hdA1UID" runat="server" />
        </div>

        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">ข้อมูลร้านยาที่ให้บริการ</h3>
                <div class="box-tools pull-right">


                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <table width="100%" border="0" align="left" cellpadding="0" cellspacing="2" class="OptionPanels">
                    <tr>
                        <td width="50">รหัสร้าน</td>
                        <td width="120" class="LocationName">
                            <asp:Label ID="lblLocationID" runat="server"></asp:Label>
                        </td>
                        <td width="50">ชื่อร้าน</td>
                        <td width="300" class="LocationName">
                            <asp:Label ID="lblLocationName" runat="server"></asp:Label>
                        </td>
                        <td width="100">รหัสหน่วยบริการ</td>
                        <td class="LocationName">
                            <asp:Label ID="lblLocationCode" runat="server"></asp:Label>
                        </td>
                        <td width="50">จังหวัด</td>
                        <td class="LocationName">
                            <asp:Label ID="lblLocationProvince" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>



                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">
                    <tr>
                        <td>ปี</td>
                        <td>
                            <asp:TextBox ID="txtYear" runat="server" Width="50px" CssClass="text-center"></asp:TextBox>
                        </td>
                        <td>วันที่ให้บริการ</td>
                        <td>
                            <asp:TextBox ID="txtServiceDate" runat="server" CssClass="form-control text-center" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                        </td>
                        <td align="left">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                ControlToValidate="txtServiceDate" CssClass="text_red"
                                ErrorMessage="*รูปแบบวันที่ไม่ถูกต้อง" ValidationExpression="(\d{1}|\d{2})/(\d{1}|\d{2})/\d{4}"></asp:RegularExpressionValidator></td>
                        <td>ระยะเวลาที่ให้บริการ</td>
                        <td>
                            <asp:TextBox ID="txtTime" runat="server" Width="50px" CssClass="text-center"></asp:TextBox>
                            &nbsp;นาที</td>
                        <td>เภสัชกรผู้ให้บริการ</td>
                        <td>
                            <asp:DropDownList CssClass="form-control select2" ID="ddlPerson" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                </table>


            </div>

        </div>


        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">วิธีการติดตาม</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">

                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="Section_Date">

                    <tr>
                        <td>แหล่งที่มา</td>
                        <td align="left" colspan="5">

                            <table>
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="optPatientFrom" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="W">Walk in</asp:ListItem>
                                            <asp:ListItem Value="H">Home visit</asp:ListItem>
                                            <asp:ListItem Value="R">หน่วยบริการส่งมา</asp:ListItem>
                                            <asp:ListItem Value="C">โครงการลดความแออัด</asp:ListItem>
                                            <asp:ListItem Value="9">อื่นๆ</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>ระบุ</td>
                                    <td>
                                        <asp:TextBox ID="txtFromRemark" runat="server" Width="300px"></asp:TextBox></td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                    <tr>
                        <td valign="top">การให้บริการ</td>
                        <td align="left" colspan="5">
                            <table>
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="optService" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="1">Face to face</asp:ListItem>
                                            <asp:ListItem Value="2">Telepharmacy</asp:ListItem>
                                            <asp:ListItem Value="9">อื่นๆ</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtServiceRemark" runat="server" Width="300px" placeholder="กรณีอื่นๆ ระบุ"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">กรณี Telepharmacy</td>
                        <td align="left" colspan="5">
                            <table>
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="optTelepharm" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="1">ผป.ของร้านเอง</asp:ListItem>
                                            <asp:ListItem Value="2">ผป.โครงการลดแอัด (ระบุ รพ.แม่ข่าย)</asp:ListItem>
                                            <asp:ListItem Value="3">ผป.รับยาทาง ปณ.จาก รพ. (ระบุ รพ.)</asp:ListItem>
                                            <asp:ListItem Value="9">ผป.ในโครงการอื่นๆ (ระบุ)</asp:ListItem>
                                        </asp:RadioButtonList></td>
                                    <td width="60" class="text-right">ระบุ</td>
                                    <td>
                                        <asp:TextBox ID="txtTelepharmacyRemark" runat="server" Width="300px"></asp:TextBox></td>
                                </tr>
                            </table>


                        </td>
                    </tr>
                    <tr>
                        <td valign="top">วิธีการสื่อสาร Telepharmacy</td>
                        <td align="left" colspan="5">

                            <table>
                                <tr>
                                    <td>
                                        <asp:RadioButtonList ID="optTelepharmacyMethod" runat="server">
                                            <asp:ListItem Value="1">โทรศัพท์ หรือ  line call  <u><b>ไม่มีการบันทึกทั้งภาพและเสียง</b></u>        </asp:ListItem>
                                            <asp:ListItem Value="2">โทรศัพท์  หรือ  Line call <u><b>บันทึกแต่เสียง</b></u></asp:ListItem>
                                            <asp:ListItem Value="3">มี soft ware หรือ program หรือ  Line Call  หรือ อื่นๆ  และ<u><b>มีการบันทึก ภาพและเสียง</b></u></asp:ListItem>
                                        </asp:RadioButtonList></td>
                                    <td>
                                        <table class="nav-justified">
                                            <tr>
                                                <td>ระบุ วิธีการบันทึก</td>
                                                <td>
                                                    <asp:TextBox ID="txtRecordMethod" runat="server" Width="300px"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td>ระบุ  สถานที่เก็บข้อมูล</td>
                                                <td>
                                                    <asp:TextBox ID="txtRecordLocation" runat="server" Width="300px"></asp:TextBox></td>
                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                            </table>


                        </td>
                        <td colspan="2" valign="top"></td>
                    </tr>
                </table>

            </div>
        </div>
        <div class="box box-success">
            <div class="box-header">
               
                <h3 class="box-title">ข้อมูลกิจกรรม</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>


            </div>
            <div class="box-body">

                <table class="table" border="0" align="center">
                    <tr>
                        <td align="left" colspan="4">
                            <table border="0" cellspacing="0" cellpadding="2">
                                <tr>
                                    <td align="left">เภสัชอาสาพาเลิกบุหรี่ ทำการติดตามผ่าน  </td>
                                    <td>
                                        <asp:RadioButtonList ID="optFollowChannel" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                            <asp:ListItem Value="CALL" Selected="True">โทรศัพท์</asp:ListItem>
                                            <asp:ListItem Value="WALK">มาที่ร้าน</asp:ListItem>
                                            <asp:ListItem Value="ONLINE">ออนไลน์ โดย</asp:ListItem>
                                        </asp:RadioButtonList></td>
                                    <td>
                                        <asp:RadioButtonList ID="optOnline" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="1" Selected="True">เห็นหน้า</asp:ListItem>
                                            <asp:ListItem Value="2">เทเลคอล(เสียง)</asp:ListItem>
                                            <asp:ListItem Value="3">พิมพ์ตอบ</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4" class="MenuSt">1. การติดตาม ครั้งที่&nbsp;
        <asp:Label ID="lblSEQ" runat="server" Width="50px" CssClass="form-control text-blue"></asp:Label>

                        </td>
                    </tr>

                    <tr>
                        <td align="left">2. วันที่ติดตาม</td>
                        <td>
                            <asp:TextBox ID="txtFollowDate" runat="server" CssClass="form-control text-center" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="240">3. อาการถอนนิโคติน</td>
                        <td width="200">
                            <asp:RadioButtonList ID="optNicotineEffect" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="0">ไม่มี</asp:ListItem> 
                                <asp:ListItem Value="1">มี</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td width="50">ระบุ</td>
                        <td> <asp:TextBox ID="txtNicotineRemark" runat="server" CssClass="form-control"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>4. อาการไม่พึงประสงค์จากการใช้ยา </td>
                        <td>
                            <asp:RadioButtonList ID="optMedEffect" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="0">ไม่มี</asp:ListItem>
                                <asp:ListItem Value="1">มี</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>ระบุ</td>
                        <td>
                            <asp:TextBox ID="txtMedEffRemark" runat="server" CssClass="form-control"></asp:TextBox></td>
                    </tr>
                         <tr>
         <td valign="top">5. ปริมาณบุหรี่ที่สูบ</td>
         <td colspan="3">
             <table border="0" cellspacing="2" cellpadding="0">                
                 <tr>
                    
                     <td>
                         กรณีสูบบุหรี่ <u>ซอง </u>
                     </td>
                     <td>
                         <asp:TextBox ID="txtSmokeQTY" runat="server" CssClass="text-center" Width="80px"></asp:TextBox>
                     </td>
                     <td>
                         มวน/วัน</td>
                 </tr>
                
                 <tr>
                     <td>กรณีสูบบุหรี่ <u>ไฟฟ้า</u>  ระบุความเข้มข้นของนิโคติน</td>
                     <td>
                         <asp:TextBox ID="txtNicotinRate" runat="server" CssClass="text-center" Width="80px"></asp:TextBox></td>
                     <td>จำนวนครั้งที่สูบต่อวัน</td>
                     <td>
                         <asp:TextBox ID="txtOften" runat="server" CssClass="text-center" Width="50px"></asp:TextBox>ครั้ง/วัน</td>
                     <td>&nbsp;</td>
                     <td>ในแต่ละครั้งที่สูบบุหรี่ไฟฟ้าสูดนาน</td>
                     <td>
                         <asp:TextBox ID="txtMinutePerTime" runat="server" CssClass="text-center" Width="80px"></asp:TextBox></td>
                     <td>&nbsp;นาที/ครั้ง</td>
                 </tr>
             </table>
         </td>
     </tr>

                    <tr>                      
                        <td align="left" scope="row">6. เทียบกับ A1-A4</td>
                         <td colspan="3">
     <asp:RadioButtonList ID="optChangeRate" runat="server" RepeatDirection="Horizontal">
         <asp:ListItem Value="L" Selected="True">ลดลง</asp:ListItem>
         <asp:ListItem Value="E">เท่าเดิม</asp:ListItem>
         <asp:ListItem Value="H">เพิ่มขึ้น</asp:ListItem>
     </asp:RadioButtonList>
 </td>

                                                </tr>
                    <tr>    
                          <td align="left" scope="row">7. วันที่เลิกได้สำเร็จ</td>
                        <td>
                                                                    <asp:TextBox ID="dtpFinalStopDate" runat="server" CssClass="form-control text-center" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox></td>

                                                                 <td></td>
                                                    <td align="left" scope="row" valign="top">
                                                    จำนวนวันที่เลิกได้สำเร็จ
                                                                    <asp:TextBox ID="txtQuitDay" runat="server"  Width="50px" CssClass="text-center"></asp:TextBox>
                                                                    
                                                                </td> 
                                                </tr>

                     <tr>
     <td  colspan="4">8.ผลตรวจร่างกาย</td>   
 </tr>

                              <tr>
  <td></td>
              <td colspan="3">

                  <table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                          <td>น้ำหนัก</td>
                          <td>
                              <asp:TextBox ID="txtWeight" runat="server" Width="80px" CssClass="text-center"></asp:TextBox>
                          </td>
                          <td>&nbsp; กก.&nbsp; ส่วนสูง</td>
                          <td>
                              <asp:TextBox ID="txtHeigh" runat="server" AutoPostBack="True" Width="80px" CssClass="text-center"></asp:TextBox>
                              &nbsp;ซม.</td>
                            <td>ความดันโลหิต </td>
  <td>
      <asp:TextBox ID="txtBP" runat="server" CssClass="text-center" Width="80px"></asp:TextBox>
  </td>
  <td>&nbsp;mmHg</td>
                      </tr>
                      <tr>
                          <td>PEFR</td>
                          <td>
                              <asp:TextBox ID="txtPEFR_Value" runat="server" AutoPostBack="True" CssClass="text-center" Width="80px" >0</asp:TextBox></td>
                          <td>ค่า % PEFR</td>
                          <td>
                              <asp:TextBox ID="txtPEFR_Rate" runat="server" CssClass="text-center" Width="80px" ></asp:TextBox></td>
                      </tr>
                      <tr>
                          <td colspan="3">ปริมาณก๊าซคาร์บอนมอนอกไซด์ในลมหายใจ </td>
                          <td>
                              <asp:TextBox ID="txtCo_Value" runat="server" Width="80px" CssClass="text-center"></asp:TextBox>
                              &nbsp;ppm.</td>
                      </tr>
                  </table>
              </td>
          </tr>
                    <tr>
                        <td align="left">9. วิธีการเลิกบุหรี่</td>
                           <td align="left" colspan="4"> 
                               <table>
                                   <tr>
                                       <td >
       <asp:RadioButtonList ID="optStopPlane1" runat="server" AutoPostBack="True" RepeatDirection="Horizontal">
                                                                        <asp:ListItem Selected="True" Value="1">ปรับเปลี่ยนพฤติกรรม และสร้างแรงจูงใจ</asp:ListItem>
                                                                        <asp:ListItem Value="2">ปรับเปลี่ยนพฤติกรรม และสร้างแรงจูงใจ + ใช้ยาช่วยเลิกบุหรี่ </asp:ListItem>
             <asp:ListItem Value="3">อื่นๆ ระบุ</asp:ListItem>
                                                                    </asp:RadioButtonList>
                                       </td>
  
                              <td>
                                                                    <asp:TextBox ID="txtStopPlaneOther" runat="server" CssClass="form-control" Width="400"></asp:TextBox></td>
                                   </tr>
                               </table>
                        
                                                                </td>
                                                             
                                                  </tr>
                                              
                                        
               <tr class="MenuSt">
                   <th align="left" scope="row" colspan="4"> ยาช่วยเลิกบุหรี่ที่จ่าย (กรณีบรรจุแผงให้นับ เม็ด)</th>
                                                </tr>

                                                <tr>
                                                    <td></td>
                                                    <td colspan="3">

                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td>

                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td width="120">ยาที่จ่าย</td>
                                                                            <td>
                                                                                <asp:DropDownList CssClass="form-control select2" ID="ddlMed" runat="server">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td>อื่นๆ ระบุ</td>
                                                                            <td colspan="2">
                                                                                <asp:TextBox ID="txtMed" runat="server" Width="200px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>ขนาดรับประทาน</td>
                                                                            <td colspan="4">
                                                                                <asp:TextBox ID="txtFrequency" runat="server" Width="100%"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>จำนวน</td>
                                                                            <td>
                                                                                <asp:TextBox ID="txtMedQTY" runat="server" Width="60px" CssClass="form-control text-center"></asp:TextBox>
                                                                                &nbsp;&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblBalanceLabel" runat="server" CssClass="small text-blue"   Text="Balance = "></asp:Label>
                                                                            </td>
                                                                            <td colspan="4"  class="small text-blue">1.กรณี จ่ายยาเป็นแผงให้ระบุจำนวนเม็ด &nbsp;
                                 2.กรณี สมุนไพรชงหญ้าดอกขาว ให้ระบุเป็นซอง ใน 1 ห่อมี 10 ซอง 
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblBalance" runat="server" Font-Size="8pt" ForeColor="Blue"></asp:Label>
                                                                            </td>
                                                                            <td colspan="3">

                                                                                <asp:Button ID="cmdAddMed" runat="server" Text="เพิ่มยา/บันทึกชื่อยา"  CssClass="btn btn-primary">
                                                                                </asp:Button>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblNoMed" runat="server" Font-Size="12pt" ForeColor="Red" Text="ไม่มีรายการจ่ายยาช่วยเลิกบุหรี่"></asp:Label>
                                                                    <asp:GridView ID="grdData"
                                                                        runat="server" CellPadding="0" ForeColor="#333333"
                                                                        GridLines="None"
                                                                        AutoGenerateColumns="False" Font-Bold="False" DataKeyNames="itemID">
                                                                        <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
                                                                        <Columns>
                                                                            <asp:BoundField HeaderText="No.">
                                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="ItemName" HeaderText="ยาที่จ่าย">
                                                                                <HeaderStyle HorizontalAlign="Left" />
                                                                                <ItemStyle HorizontalAlign="Left" Width="300px" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField HeaderText="จำนวน" DataField="QTY">
                                                                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                                            </asp:BoundField>
                                                                            <asp:BoundField DataField="FrequencyDescription" HeaderText="ขนาดรับประทาน" />
                                                                            <asp:TemplateField HeaderText="ลบ">
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton CssClass="gridbutton" ID="imgDel" runat="server"
                                                                                        ImageUrl="images/icon-delete.png"
                                                                                        CommandArgument='<%# Container.DataItemIndex %>' />
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                                        <PagerStyle HorizontalAlign="Center"
                                                                            CssClass="dc_pagination dc_paginationC dc_paginationC11" />
                                                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                                        <HeaderStyle CssClass="th" Font-Bold="True" HorizontalAlign="Center"
                                                                            VerticalAlign="Middle" />
                                                                        <EditRowStyle BackColor="#2461BF" />
                                                                        <AlternatingRowStyle BackColor="White" />
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                     
                    <tr>    
                        <td>10. วันที่นัดหมายครั้งต่อไป </td> 
                        <td>
                                        <asp:TextBox ID="xDateNextService" runat="server" CssClass="form-control" autocomplete="off" data-date-format="dd/mm/yyyy" data-date-language="th-th" data-provide="datepicker" onkeyup="chkstr(this,this.value)"></asp:TextBox></td>
                        <td></td>
                        <td></td>                                   
                    </tr>
                    <tr> 
                        <td>สถานะ</td>  <td>
                                        <asp:CheckBox ID="chkStatus" runat="server" Text="จบการทำงาน" />
                                    </td>                   
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Button ID="cmdSave" runat="server" Text="บันทึก" CssClass="buttonSave" Width="100px" />
                        </td>
                    </tr>
                  
                </table>
            </div>
            <div class="box-footer clearfix">
            </div>
        </div>
  
    
             <div class="box box-success">
         <div class="box-header">
             <i class="fa fa-info-circle"></i>

             <h3 class="box-title">ประวัติการติดตาม A5</h3>

             <div class="box-tools pull-right">
                 <button type="button" class="btn btn-box-tool" data-widget="collapse">
                     <i class="fa fa-minus"></i>
                 </button>
                 <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
             </div>


         </div>
         <div class="box-body">

                            <asp:GridView ID="grdA05"
                                runat="server" CellPadding="0" ForeColor="#333333"
                                GridLines="None"
                                AutoGenerateColumns="False" Width="100%">
                                <RowStyle BackColor="#F7F7F7" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:BoundField HeaderText="ครั้งที่" DataField="FollowSEQ">
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FollowDate" HeaderText="วันที่">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FollowChannelTXT" HeaderText="วิธีติดตาม">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>                                 
                                    <asp:BoundField DataField="NicotineEffectTXT" HeaderText="อาการถอนนิโคติน" />
                                    <asp:BoundField HeaderText="อาการไม่พึงประสงค์" DataField="MedEffectTXT" />
                                    <asp:BoundField HeaderText="เทียบกับ A1-A4" DataField="ChangeRateTXT" />                          
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton CssClass="gridbutton" ID="imgEdit" runat="server" ImageUrl="images/icon-edit.png"
                                                CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ServiceUID")%>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton CssClass="gridbutton" ID="imgDelete" runat="server"
                                                ImageUrl="images/icon-delete.png"
                                                CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ServiceUID") %>' />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle HorizontalAlign="Center"
                                    CssClass="dc_pagination dc_paginationC dc_paginationC11" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <HeaderStyle CssClass="th" Font-Bold="True" HorizontalAlign="Center"
                                    VerticalAlign="Middle" />
                                <EditRowStyle BackColor="#2461BF" />
                                <AlternatingRowStyle BackColor="White" />
                            </asp:GridView>

             </div>
                 </div>
    </section>
</asp:Content>
